/*
               File: ReferenciaINMAnexosWC
        Description: Referencia INMAnexos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:3:7.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciainmanexoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public referenciainmanexoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public referenciainmanexoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ReferenciaINM_Codigo )
      {
         this.AV7ReferenciaINM_Codigo = aP0_ReferenciaINM_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ReferenciaINM_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_23 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_23_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_23_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV7ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
                  AV28Pgmname = GetNextPar( );
                  A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n645TipoDocumento_Codigo = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAT92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV28Pgmname = "ReferenciaINMAnexosWC";
               context.Gx_err = 0;
               WST92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Referencia INMAnexos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428233748");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("referenciainmanexoswc.aspx") + "?" + UrlEncode("" +AV7ReferenciaINM_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_23", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_23), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV28Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"REFERENCIAINMANEXOS_ARQUIVO", A2139ReferenciaINMAnexos_Arquivo);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"UCCONFIRMEPANEL_Title", StringUtil.RTrim( Ucconfirmepanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"UCCONFIRMEPANEL_Confirmationtext", StringUtil.RTrim( Ucconfirmepanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"UCCONFIRMEPANEL_Yesbuttoncaption", StringUtil.RTrim( Ucconfirmepanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"UCCONFIRMEPANEL_Nobuttoncaption", StringUtil.RTrim( Ucconfirmepanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"UCCONFIRMEPANEL_Result", StringUtil.RTrim( Ucconfirmepanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormT92( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("referenciainmanexoswc.js", "?2020428233789");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ReferenciaINMAnexosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Referencia INMAnexos WC" ;
      }

      protected void WBT90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "referenciainmanexoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            wb_table1_2_T92( true) ;
         }
         else
         {
            wb_table1_2_T92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_T92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaINM_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaINM_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtReferenciaINM_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaINMAnexosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTT92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Referencia INMAnexos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPT90( ) ;
            }
         }
      }

      protected void WST92( )
      {
         STARTT92( ) ;
         EVTT92( ) ;
      }

      protected void EVTT92( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11T92 */
                                    E11T92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "UCCONFIRMEPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12T92 */
                                    E12T92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBNTANEXOS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13T92 */
                                    E13T92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "VBNTVISUALISAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "VBNTVISUALISAR.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPT90( ) ;
                              }
                              nGXsfl_23_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_23_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_23_idx), 4, 0)), 4, "0");
                              SubsflControlProps_232( ) ;
                              AV19BntVisualisar = cgiGet( edtavBntvisualisar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBntvisualisar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19BntVisualisar)) ? AV26Bntvisualisar_GXI : context.convertURL( context.PathToRelativeUrl( AV19BntVisualisar))));
                              AV20BntRemover = cgiGet( edtavBntremover_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBntremover_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV20BntRemover)) ? AV27Bntremover_GXI : context.convertURL( context.PathToRelativeUrl( AV20BntRemover))));
                              A2134ReferenciaINMAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINMAnexos_Codigo_Internalname), ",", "."));
                              A2138ReferenciaINMAnexos_Descricao = cgiGet( edtReferenciaINMAnexos_Descricao_Internalname);
                              A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                              A2142ReferenciaINMAnexos_Data = context.localUtil.CToT( cgiGet( edtReferenciaINMAnexos_Data_Internalname), 0);
                              n2142ReferenciaINMAnexos_Data = false;
                              A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoDocumento_Codigo_Internalname), ",", "."));
                              n645TipoDocumento_Codigo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E14T92 */
                                          E14T92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E15T92 */
                                          E15T92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E16T92 */
                                          E16T92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBNTVISUALISAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E17T92 */
                                          E17T92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPT90( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WET92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormT92( ) ;
            }
         }
      }

      protected void PAT92( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_232( ) ;
         while ( nGXsfl_23_idx <= nRC_GXsfl_23 )
         {
            sendrow_232( ) ;
            nGXsfl_23_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_23_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_23_idx+1));
            sGXsfl_23_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_23_idx), 4, 0)), 4, "0");
            SubsflControlProps_232( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV7ReferenciaINM_Codigo ,
                                       String AV28Pgmname ,
                                       int A645TipoDocumento_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFT92( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2134ReferenciaINMAnexos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REFERENCIAINMANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_DESCRICAO", GetSecureSignedToken( sPrefix, A2138ReferenciaINMAnexos_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"REFERENCIAINMANEXOS_DESCRICAO", A2138ReferenciaINMAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2142ReferenciaINMAnexos_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REFERENCIAINMANEXOS_DATA", context.localUtil.TToC( A2142ReferenciaINMAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODOCUMENTO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFT92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV28Pgmname = "ReferenciaINMAnexosWC";
         context.Gx_err = 0;
      }

      protected void RFT92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 23;
         /* Execute user event: E15T92 */
         E15T92 ();
         nGXsfl_23_idx = 1;
         sGXsfl_23_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_23_idx), 4, 0)), 4, "0");
         SubsflControlProps_232( ) ;
         nGXsfl_23_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_232( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00T92 */
            pr_default.execute(0, new Object[] {AV7ReferenciaINM_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_23_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2141ReferenciaINMAnexos_ArquivoNome = H00T92_A2141ReferenciaINMAnexos_ArquivoNome[0];
               n2141ReferenciaINMAnexos_ArquivoNome = H00T92_n2141ReferenciaINMAnexos_ArquivoNome[0];
               A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
               A2140ReferenciaINMAnexos_ArquivoTipo = H00T92_A2140ReferenciaINMAnexos_ArquivoTipo[0];
               n2140ReferenciaINMAnexos_ArquivoTipo = H00T92_n2140ReferenciaINMAnexos_ArquivoTipo[0];
               A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
               A709ReferenciaINM_Codigo = H00T92_A709ReferenciaINM_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               A645TipoDocumento_Codigo = H00T92_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00T92_n645TipoDocumento_Codigo[0];
               A2142ReferenciaINMAnexos_Data = H00T92_A2142ReferenciaINMAnexos_Data[0];
               n2142ReferenciaINMAnexos_Data = H00T92_n2142ReferenciaINMAnexos_Data[0];
               A646TipoDocumento_Nome = H00T92_A646TipoDocumento_Nome[0];
               A2138ReferenciaINMAnexos_Descricao = H00T92_A2138ReferenciaINMAnexos_Descricao[0];
               A2134ReferenciaINMAnexos_Codigo = H00T92_A2134ReferenciaINMAnexos_Codigo[0];
               A2139ReferenciaINMAnexos_Arquivo = H00T92_A2139ReferenciaINMAnexos_Arquivo[0];
               n2139ReferenciaINMAnexos_Arquivo = H00T92_n2139ReferenciaINMAnexos_Arquivo[0];
               A646TipoDocumento_Nome = H00T92_A646TipoDocumento_Nome[0];
               /* Execute user event: E16T92 */
               E16T92 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 23;
            WBT90( ) ;
         }
         nGXsfl_23_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00T93 */
         pr_default.execute(1, new Object[] {AV7ReferenciaINM_Codigo});
         GRID_nRecordCount = H00T93_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7ReferenciaINM_Codigo, AV28Pgmname, A645TipoDocumento_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPT90( )
      {
         /* Before Start, stand alone formulas. */
         AV28Pgmname = "ReferenciaINMAnexosWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14T92 */
         E14T92 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_23 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_23"), ",", "."));
            AV17GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV18GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ReferenciaINM_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ucconfirmepanel_Title = cgiGet( sPrefix+"UCCONFIRMEPANEL_Title");
            Ucconfirmepanel_Confirmationtext = cgiGet( sPrefix+"UCCONFIRMEPANEL_Confirmationtext");
            Ucconfirmepanel_Yesbuttoncaption = cgiGet( sPrefix+"UCCONFIRMEPANEL_Yesbuttoncaption");
            Ucconfirmepanel_Nobuttoncaption = cgiGet( sPrefix+"UCCONFIRMEPANEL_Nobuttoncaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ucconfirmepanel_Result = cgiGet( sPrefix+"UCCONFIRMEPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14T92 */
         E14T92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14T92( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtReferenciaINM_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtReferenciaINM_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15T92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV17GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GridCurrentPage), 10, 0)));
         AV18GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridPageCount), 10, 0)));
      }

      protected void E11T92( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV16PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV16PageToGo) ;
         }
      }

      private void E16T92( )
      {
         /* Grid_Load Routine */
         AV19BntVisualisar = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavBntvisualisar_Internalname, AV19BntVisualisar);
         AV26Bntvisualisar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavBntvisualisar_Tooltiptext = "Mostrar";
         AV20BntRemover = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavBntremover_Internalname, AV20BntRemover);
         AV27Bntremover_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavBntremover_Tooltiptext = "";
         edtTipoDocumento_Nome_Link = formatLink("viewtipodocumento.aspx") + "?" + UrlEncode("" +A645TipoDocumento_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 23;
         }
         sendrow_232( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_23_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(23, GridRow);
         }
      }

      protected void E13T92( )
      {
         /* 'DoBntAnexos' Routine */
         context.PopUp(formatLink("wp_referenciainmanexos.aspx") + "?" + UrlEncode("" +AV7ReferenciaINM_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV28Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV28Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV28Pgmname+"GridState"), "");
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV28Pgmname+"GridState"), "");
         new wwpbaseobjects.savegridstate(context ).execute(  AV28Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV28Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ReferenciaINM";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ReferenciaINM_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E17T92( )
      {
         /* Bntvisualisar_Click Routine */
         context.wjLoc = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(A2139ReferenciaINMAnexos_Arquivo)) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim(StringUtil.Trim( A2141ReferenciaINMAnexos_ArquivoNome)+"."+StringUtil.Trim( A2140ReferenciaINMAnexos_ArquivoTipo)));
         context.wjLocDisableFrm = 0;
      }

      protected void E12T92( )
      {
         /* Ucconfirmepanel_Close Routine */
         if ( StringUtil.StrCmp(Ucconfirmepanel_Result, "Yes") == 0 )
         {
            AV21BC_ReferenciaINM.Load(AV7ReferenciaINM_Codigo);
            if ( AV21BC_ReferenciaINM.Success() )
            {
               AV23Item = 1;
               while ( AV23Item <= AV21BC_ReferenciaINM.gxTpr_Anexos.Count )
               {
                  if ( ((SdtReferenciaINM_Anexos)AV21BC_ReferenciaINM.gxTpr_Anexos.Item(AV23Item)).gxTpr_Referenciainmanexos_codigo == A2134ReferenciaINMAnexos_Codigo )
                  {
                     AV21BC_ReferenciaINM.gxTpr_Anexos.RemoveItem(AV23Item);
                     if (true) break;
                  }
                  AV23Item = (short)(AV23Item+1);
               }
               AV21BC_ReferenciaINM.Save();
               if ( AV21BC_ReferenciaINM.Success() )
               {
                  context.CommitDataStores( "ReferenciaINMAnexosWC");
                  context.DoAjaxRefreshCmp(sPrefix);
               }
               else
               {
                  GX_msglist.addItem("Erro ao relizar a exclus�o do documento.");
                  context.RollbackDataStores( "ReferenciaINMAnexosWC");
               }
            }
         }
      }

      protected void wb_table1_2_T92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_T92( true) ;
         }
         else
         {
            wb_table2_8_T92( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_T92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"UCCONFIRMEPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_T92e( true) ;
         }
         else
         {
            wb_table1_2_T92e( false) ;
         }
      }

      protected void wb_table2_8_T92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(1000), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTablegrid_Internalname, tblTablegrid_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_11_T92( true) ;
         }
         else
         {
            wb_table3_11_T92( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_T92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_20_T92( true) ;
         }
         else
         {
            wb_table4_20_T92( false) ;
         }
         return  ;
      }

      protected void wb_table4_20_T92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_T92e( true) ;
         }
         else
         {
            wb_table2_8_T92e( false) ;
         }
      }

      protected void wb_table4_20_T92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"23\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo do Anexo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo do Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Upload") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19BntVisualisar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBntvisualisar_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV20BntRemover));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBntremover_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2138ReferenciaINMAnexos_Descricao);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2141ReferenciaINMAnexos_ArquivoNome));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTipoDocumento_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2140ReferenciaINMAnexos_ArquivoTipo));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A2142ReferenciaINMAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 23 )
         {
            wbEnd = 0;
            nRC_GXsfl_23 = (short)(nGXsfl_23_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_20_T92e( true) ;
         }
         else
         {
            wb_table4_20_T92e( false) ;
         }
      }

      protected void wb_table3_11_T92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table5_14_T92( true) ;
         }
         else
         {
            wb_table5_14_T92( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_T92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_T92e( true) ;
         }
         else
         {
            wb_table3_11_T92e( false) ;
         }
      }

      protected void wb_table5_14_T92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBntanexos_Internalname, context.GetImagePath( "35d9ce8e-0cd2-4075-b379-43e1e9bf02ad", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBntanexos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOBNTANEXOS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMAnexosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_T92e( true) ;
         }
         else
         {
            wb_table5_14_T92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAT92( ) ;
         WST92( ) ;
         WET92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ReferenciaINM_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAT92( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "referenciainmanexoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAT92( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
         }
         wcpOAV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ReferenciaINM_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ReferenciaINM_Codigo != wcpOAV7ReferenciaINM_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ReferenciaINM_Codigo = AV7ReferenciaINM_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ReferenciaINM_Codigo = cgiGet( sPrefix+"AV7ReferenciaINM_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ReferenciaINM_Codigo) > 0 )
         {
            AV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ReferenciaINM_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
         }
         else
         {
            AV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ReferenciaINM_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAT92( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WST92( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WST92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ReferenciaINM_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ReferenciaINM_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ReferenciaINM_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ReferenciaINM_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WET92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428233925");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("referenciainmanexoswc.js", "?2020428233925");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_232( )
      {
         edtavBntvisualisar_Internalname = sPrefix+"vBNTVISUALISAR_"+sGXsfl_23_idx;
         edtavBntremover_Internalname = sPrefix+"vBNTREMOVER_"+sGXsfl_23_idx;
         edtReferenciaINMAnexos_Codigo_Internalname = sPrefix+"REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_23_idx;
         edtReferenciaINMAnexos_Descricao_Internalname = sPrefix+"REFERENCIAINMANEXOS_DESCRICAO_"+sGXsfl_23_idx;
         edtReferenciaINMAnexos_ArquivoNome_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVONOME_"+sGXsfl_23_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_23_idx;
         edtReferenciaINMAnexos_ArquivoTipo_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVOTIPO_"+sGXsfl_23_idx;
         edtReferenciaINMAnexos_Data_Internalname = sPrefix+"REFERENCIAINMANEXOS_DATA_"+sGXsfl_23_idx;
         edtTipoDocumento_Codigo_Internalname = sPrefix+"TIPODOCUMENTO_CODIGO_"+sGXsfl_23_idx;
      }

      protected void SubsflControlProps_fel_232( )
      {
         edtavBntvisualisar_Internalname = sPrefix+"vBNTVISUALISAR_"+sGXsfl_23_fel_idx;
         edtavBntremover_Internalname = sPrefix+"vBNTREMOVER_"+sGXsfl_23_fel_idx;
         edtReferenciaINMAnexos_Codigo_Internalname = sPrefix+"REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_23_fel_idx;
         edtReferenciaINMAnexos_Descricao_Internalname = sPrefix+"REFERENCIAINMANEXOS_DESCRICAO_"+sGXsfl_23_fel_idx;
         edtReferenciaINMAnexos_ArquivoNome_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVONOME_"+sGXsfl_23_fel_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_23_fel_idx;
         edtReferenciaINMAnexos_ArquivoTipo_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVOTIPO_"+sGXsfl_23_fel_idx;
         edtReferenciaINMAnexos_Data_Internalname = sPrefix+"REFERENCIAINMANEXOS_DATA_"+sGXsfl_23_fel_idx;
         edtTipoDocumento_Codigo_Internalname = sPrefix+"TIPODOCUMENTO_CODIGO_"+sGXsfl_23_fel_idx;
      }

      protected void sendrow_232( )
      {
         SubsflControlProps_232( ) ;
         WBT90( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_23_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_23_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_23_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBntvisualisar_Enabled!=0)&&(edtavBntvisualisar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'"+sPrefix+"',false,'',23)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV19BntVisualisar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19BntVisualisar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV26Bntvisualisar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19BntVisualisar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBntvisualisar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19BntVisualisar)) ? AV26Bntvisualisar_GXI : context.PathToRelativeUrl( AV19BntVisualisar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBntvisualisar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBntvisualisar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVBNTVISUALISAR.CLICK."+sGXsfl_23_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV19BntVisualisar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBntremover_Enabled!=0)&&(edtavBntremover_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'"+sPrefix+"',false,'',23)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV20BntRemover_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV20BntRemover))&&String.IsNullOrEmpty(StringUtil.RTrim( AV27Bntremover_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV20BntRemover)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBntremover_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV20BntRemover)) ? AV27Bntremover_GXI : context.PathToRelativeUrl( AV20BntRemover)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBntremover_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBntremover_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e18t92_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV20BntRemover_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2134ReferenciaINMAnexos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_Descricao_Internalname,(String)A2138ReferenciaINMAnexos_Descricao,(String)A2138ReferenciaINMAnexos_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)23,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_ArquivoNome_Internalname,StringUtil.RTrim( A2141ReferenciaINMAnexos_ArquivoNome),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_ArquivoNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtTipoDocumento_Nome_Link,(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_ArquivoTipo_Internalname,StringUtil.RTrim( A2140ReferenciaINMAnexos_ArquivoTipo),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_ArquivoTipo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_Data_Internalname,context.localUtil.TToC( A2142ReferenciaINMAnexos_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A2142ReferenciaINMAnexos_Data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)23,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_CODIGO"+"_"+sGXsfl_23_idx, GetSecureSignedToken( sPrefix+sGXsfl_23_idx, context.localUtil.Format( (decimal)(A2134ReferenciaINMAnexos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_DESCRICAO"+"_"+sGXsfl_23_idx, GetSecureSignedToken( sPrefix+sGXsfl_23_idx, A2138ReferenciaINMAnexos_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINMANEXOS_DATA"+"_"+sGXsfl_23_idx, GetSecureSignedToken( sPrefix+sGXsfl_23_idx, context.localUtil.Format( A2142ReferenciaINMAnexos_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODOCUMENTO_CODIGO"+"_"+sGXsfl_23_idx, GetSecureSignedToken( sPrefix+sGXsfl_23_idx, context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_23_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_23_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_23_idx+1));
            sGXsfl_23_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_23_idx), 4, 0)), 4, "0");
            SubsflControlProps_232( ) ;
         }
         /* End function sendrow_232 */
      }

      protected void init_default_properties( )
      {
         imgBntanexos_Internalname = sPrefix+"BNTANEXOS";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavBntvisualisar_Internalname = sPrefix+"vBNTVISUALISAR";
         edtavBntremover_Internalname = sPrefix+"vBNTREMOVER";
         edtReferenciaINMAnexos_Codigo_Internalname = sPrefix+"REFERENCIAINMANEXOS_CODIGO";
         edtReferenciaINMAnexos_Descricao_Internalname = sPrefix+"REFERENCIAINMANEXOS_DESCRICAO";
         edtReferenciaINMAnexos_ArquivoNome_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVONOME";
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME";
         edtReferenciaINMAnexos_ArquivoTipo_Internalname = sPrefix+"REFERENCIAINMANEXOS_ARQUIVOTIPO";
         edtReferenciaINMAnexos_Data_Internalname = sPrefix+"REFERENCIAINMANEXOS_DATA";
         edtTipoDocumento_Codigo_Internalname = sPrefix+"TIPODOCUMENTO_CODIGO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegrid_Internalname = sPrefix+"TABLEGRID";
         Ucconfirmepanel_Internalname = sPrefix+"UCCONFIRMEPANEL";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtReferenciaINM_Codigo_Internalname = sPrefix+"REFERENCIAINM_CODIGO";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtTipoDocumento_Codigo_Jsonclick = "";
         edtReferenciaINMAnexos_Data_Jsonclick = "";
         edtReferenciaINMAnexos_ArquivoTipo_Jsonclick = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtReferenciaINMAnexos_ArquivoNome_Jsonclick = "";
         edtReferenciaINMAnexos_Descricao_Jsonclick = "";
         edtReferenciaINMAnexos_Codigo_Jsonclick = "";
         edtavBntremover_Jsonclick = "";
         edtavBntremover_Visible = -1;
         edtavBntremover_Enabled = 1;
         edtavBntvisualisar_Jsonclick = "";
         edtavBntvisualisar_Visible = -1;
         edtavBntvisualisar_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTipoDocumento_Nome_Link = "";
         edtavBntremover_Tooltiptext = "";
         edtavBntvisualisar_Tooltiptext = "Mostrar";
         subGrid_Class = "WorkWithBorder WorkWith";
         subGrid_Backcolorstyle = 3;
         edtReferenciaINM_Codigo_Jsonclick = "";
         edtReferenciaINM_Codigo_Visible = 1;
         Ucconfirmepanel_Nobuttoncaption = "Cancelar";
         Ucconfirmepanel_Yesbuttoncaption = "Confirmar";
         Ucconfirmepanel_Confirmationtext = "Confirma apagar o arquivo/linnk anexado?";
         Ucconfirmepanel_Title = "Aten��o";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV28Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV17GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11T92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E16T92',iparms:[{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV19BntVisualisar',fld:'vBNTVISUALISAR',pic:'',nv:''},{av:'edtavBntvisualisar_Tooltiptext',ctrl:'vBNTVISUALISAR',prop:'Tooltiptext'},{av:'AV20BntRemover',fld:'vBNTREMOVER',pic:'',nv:''},{av:'edtavBntremover_Tooltiptext',ctrl:'vBNTREMOVER',prop:'Tooltiptext'},{av:'edtTipoDocumento_Nome_Link',ctrl:'TIPODOCUMENTO_NOME',prop:'Link'}]}");
         setEventMetadata("'DOBNTANEXOS'","{handler:'E13T92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VBNTVISUALISAR.CLICK","{handler:'E17T92',iparms:[{av:'A2139ReferenciaINMAnexos_Arquivo',fld:'REFERENCIAINMANEXOS_ARQUIVO',pic:'',nv:''},{av:'A2141ReferenciaINMAnexos_ArquivoNome',fld:'REFERENCIAINMANEXOS_ARQUIVONOME',pic:'',nv:''},{av:'A2140ReferenciaINMAnexos_ArquivoTipo',fld:'REFERENCIAINMANEXOS_ARQUIVOTIPO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("VBNTREMOVER.CLICK","{handler:'E18T92',iparms:[],oparms:[]}");
         setEventMetadata("UCCONFIRMEPANEL.CLOSE","{handler:'E12T92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ucconfirmepanel_Result',ctrl:'UCCONFIRMEPANEL',prop:'Result'},{av:'A2134ReferenciaINMAnexos_Codigo',fld:'REFERENCIAINMANEXOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ucconfirmepanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV28Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2139ReferenciaINMAnexos_Arquivo = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19BntVisualisar = "";
         AV26Bntvisualisar_GXI = "";
         AV20BntRemover = "";
         AV27Bntremover_GXI = "";
         A2138ReferenciaINMAnexos_Descricao = "";
         A646TipoDocumento_Nome = "";
         A2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00T92_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         H00T92_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         H00T92_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         H00T92_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         H00T92_A709ReferenciaINM_Codigo = new int[1] ;
         H00T92_A645TipoDocumento_Codigo = new int[1] ;
         H00T92_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00T92_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         H00T92_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         H00T92_A646TipoDocumento_Nome = new String[] {""} ;
         H00T92_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         H00T92_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         H00T92_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         H00T92_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         A2141ReferenciaINMAnexos_ArquivoNome = "";
         A2139ReferenciaINMAnexos_Arquivo_Filename = "";
         A2140ReferenciaINMAnexos_ArquivoTipo = "";
         A2139ReferenciaINMAnexos_Arquivo_Filetype = "";
         H00T93_AGRID_nRecordCount = new long[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV21BC_ReferenciaINM = new SdtReferenciaINM(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         imgBntanexos_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ReferenciaINM_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciainmanexoswc__default(),
            new Object[][] {
                new Object[] {
               H00T92_A2141ReferenciaINMAnexos_ArquivoNome, H00T92_n2141ReferenciaINMAnexos_ArquivoNome, H00T92_A2140ReferenciaINMAnexos_ArquivoTipo, H00T92_n2140ReferenciaINMAnexos_ArquivoTipo, H00T92_A709ReferenciaINM_Codigo, H00T92_A645TipoDocumento_Codigo, H00T92_n645TipoDocumento_Codigo, H00T92_A2142ReferenciaINMAnexos_Data, H00T92_n2142ReferenciaINMAnexos_Data, H00T92_A646TipoDocumento_Nome,
               H00T92_A2138ReferenciaINMAnexos_Descricao, H00T92_A2134ReferenciaINMAnexos_Codigo, H00T92_A2139ReferenciaINMAnexos_Arquivo, H00T92_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               H00T93_AGRID_nRecordCount
               }
            }
         );
         AV28Pgmname = "ReferenciaINMAnexosWC";
         /* GeneXus formulas. */
         AV28Pgmname = "ReferenciaINMAnexosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_23 ;
      private short nGXsfl_23_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_23_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV23Item ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ReferenciaINM_Codigo ;
      private int wcpOAV7ReferenciaINM_Codigo ;
      private int subGrid_Rows ;
      private int A645TipoDocumento_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A709ReferenciaINM_Codigo ;
      private int edtReferenciaINM_Codigo_Visible ;
      private int A2134ReferenciaINMAnexos_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV16PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavBntvisualisar_Enabled ;
      private int edtavBntvisualisar_Visible ;
      private int edtavBntremover_Enabled ;
      private int edtavBntremover_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV17GridCurrentPage ;
      private long AV18GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ucconfirmepanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_23_idx="0001" ;
      private String AV28Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ucconfirmepanel_Title ;
      private String Ucconfirmepanel_Confirmationtext ;
      private String Ucconfirmepanel_Yesbuttoncaption ;
      private String Ucconfirmepanel_Nobuttoncaption ;
      private String GX_FocusControl ;
      private String edtReferenciaINM_Codigo_Internalname ;
      private String edtReferenciaINM_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBntvisualisar_Internalname ;
      private String edtavBntremover_Internalname ;
      private String edtReferenciaINMAnexos_Codigo_Internalname ;
      private String edtReferenciaINMAnexos_Descricao_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtReferenciaINMAnexos_Data_Internalname ;
      private String edtTipoDocumento_Codigo_Internalname ;
      private String scmdbuf ;
      private String A2141ReferenciaINMAnexos_ArquivoNome ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filename ;
      private String A2140ReferenciaINMAnexos_ArquivoTipo ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filetype ;
      private String edtavBntvisualisar_Tooltiptext ;
      private String edtavBntremover_Tooltiptext ;
      private String edtTipoDocumento_Nome_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegrid_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String imgBntanexos_Internalname ;
      private String imgBntanexos_Jsonclick ;
      private String sCtrlAV7ReferenciaINM_Codigo ;
      private String edtReferenciaINMAnexos_ArquivoNome_Internalname ;
      private String edtReferenciaINMAnexos_ArquivoTipo_Internalname ;
      private String sGXsfl_23_fel_idx="0001" ;
      private String edtavBntvisualisar_Jsonclick ;
      private String edtavBntremover_Jsonclick ;
      private String ROClassString ;
      private String edtReferenciaINMAnexos_Codigo_Jsonclick ;
      private String edtReferenciaINMAnexos_Descricao_Jsonclick ;
      private String edtReferenciaINMAnexos_ArquivoNome_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String edtReferenciaINMAnexos_ArquivoTipo_Jsonclick ;
      private String edtReferenciaINMAnexos_Data_Jsonclick ;
      private String edtTipoDocumento_Codigo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Ucconfirmepanel_Internalname ;
      private DateTime A2142ReferenciaINMAnexos_Data ;
      private bool entryPointCalled ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2142ReferenciaINMAnexos_Data ;
      private bool n2141ReferenciaINMAnexos_ArquivoNome ;
      private bool n2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool n2139ReferenciaINMAnexos_Arquivo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV19BntVisualisar_IsBlob ;
      private bool AV20BntRemover_IsBlob ;
      private String A2138ReferenciaINMAnexos_Descricao ;
      private String AV26Bntvisualisar_GXI ;
      private String AV27Bntremover_GXI ;
      private String AV19BntVisualisar ;
      private String AV20BntRemover ;
      private String A2139ReferenciaINMAnexos_Arquivo ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00T92_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] H00T92_n2141ReferenciaINMAnexos_ArquivoNome ;
      private String[] H00T92_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] H00T92_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private int[] H00T92_A709ReferenciaINM_Codigo ;
      private int[] H00T92_A645TipoDocumento_Codigo ;
      private bool[] H00T92_n645TipoDocumento_Codigo ;
      private DateTime[] H00T92_A2142ReferenciaINMAnexos_Data ;
      private bool[] H00T92_n2142ReferenciaINMAnexos_Data ;
      private String[] H00T92_A646TipoDocumento_Nome ;
      private String[] H00T92_A2138ReferenciaINMAnexos_Descricao ;
      private int[] H00T92_A2134ReferenciaINMAnexos_Codigo ;
      private String[] H00T92_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] H00T92_n2139ReferenciaINMAnexos_Arquivo ;
      private long[] H00T93_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private SdtReferenciaINM AV21BC_ReferenciaINM ;
   }

   public class referenciainmanexoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00T92 ;
          prmH00T92 = new Object[] {
          new Object[] {"@AV7ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00T93 ;
          prmH00T93 = new Object[] {
          new Object[] {"@AV7ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00T92", "SELECT * FROM (SELECT  T1.[ReferenciaINMAnexos_ArquivoNome], T1.[ReferenciaINMAnexos_ArquivoTipo], T1.[ReferenciaINM_Codigo], T1.[TipoDocumento_Codigo], T1.[ReferenciaINMAnexos_Data], T2.[TipoDocumento_Nome], T1.[ReferenciaINMAnexos_Descricao], T1.[ReferenciaINMAnexos_Codigo], T1.[ReferenciaINMAnexos_Arquivo], ROW_NUMBER() OVER ( ORDER BY T1.[ReferenciaINM_Codigo] ) AS GX_ROW_NUMBER FROM ([ReferenciaINMAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[ReferenciaINM_Codigo] = @AV7ReferenciaINM_Codigo) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T92,11,0,true,false )
             ,new CursorDef("H00T93", "SELECT COUNT(*) FROM ([ReferenciaINMAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[ReferenciaINM_Codigo] = @AV7ReferenciaINM_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T93,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(9, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
