/*
               File: WP_AlteraContrato
        Description: Alterar contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:26:31.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_alteracontrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_alteracontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_alteracontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_UserId )
      {
         this.AV10AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9UserId = aP1_UserId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavContrato_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV10AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10AreaTrabalho_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV9UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOT2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOT2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621626321");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"caption=\"Alterar contrato\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"\" novalidate action=\""+formatLink("wp_alteracontrato.aspx") + "?" + UrlEncode("" +AV10AreaTrabalho_Codigo) + "," + UrlEncode("" +AV9UserId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vSERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body");
            context.WriteHtmlText( ">") ;
            WEOT2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOT2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_alteracontrato.aspx") + "?" + UrlEncode("" +AV10AreaTrabalho_Codigo) + "," + UrlEncode("" +AV9UserId) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AlteraContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar contrato" ;
      }

      protected void WBOT0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_OT2( true) ;
         }
         else
         {
            wb_table1_2_OT2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OT2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTOT2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOT0( ) ;
      }

      protected void WSOT2( )
      {
         STARTOT2( ) ;
         EVTOT2( ) ;
      }

      protected void EVTOT2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OT2 */
                              E11OT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12OT2 */
                                    E12OT2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CANCELAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13OT2 */
                              E13OT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14OT2 */
                              E14OT2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOT2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOT2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavContrato_codigo.Name = "vCONTRATO_CODIGO";
            cmbavContrato_codigo.WebTags = "";
            cmbavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContrato_codigo.ItemCount > 0 )
            {
               AV5Contrato_Codigo = (int)(NumberUtil.Val( cmbavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavServico_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavContrato_codigo.ItemCount > 0 )
         {
            AV5Contrato_Codigo = (int)(NumberUtil.Val( cmbavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOT2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
      }

      protected void RFOT2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14OT2 */
            E14OT2 ();
            WBOT0( ) ;
         }
      }

      protected void STRUPOT0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OT2 */
         E11OT2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV14Servico_Sigla = StringUtil.Upper( cgiGet( edtavServico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Sigla", AV14Servico_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Servico_Sigla, "@!"))));
            cmbavContrato_codigo.CurrentValue = cgiGet( cmbavContrato_codigo_Internalname);
            AV5Contrato_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContrato_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OT2 */
         E11OT2 ();
         if (returnInSub) return;
      }

      protected void E11OT2( )
      {
         /* Start Routine */
         AV11Codigos.FromXml(AV7WebSession.Get("Codigos"), "Collection");
         bttBtnenter_Visible = (AV15WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         /* Using cursor H00OT2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00OT2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00OT2_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00OT2_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = H00OT2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00OT2_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00OT2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00OT2_n801ContagemResultado_ServicoSigla[0];
            A490ContagemResultado_ContratadaCod = H00OT2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00OT2_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00OT2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00OT2_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00OT2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00OT2_n801ContagemResultado_ServicoSigla[0];
            if ( A456ContagemResultado_Codigo == AV11Codigos.GetNumeric(1) )
            {
               AV12Servico = A601ContagemResultado_Servico;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Servico), 6, 0)));
               AV14Servico_Sigla = A801ContagemResultado_ServicoSigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Sigla", AV14Servico_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Servico_Sigla, "@!"))));
               AV13Contratada = A490ContagemResultado_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor H00OT3 */
         pr_default.execute(1, new Object[] {AV12Servico, AV13Contratada});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A77Contrato_Numero = H00OT3_A77Contrato_Numero[0];
            A74Contrato_Codigo = H00OT3_A74Contrato_Codigo[0];
            A155Servico_Codigo = H00OT3_A155Servico_Codigo[0];
            A39Contratada_Codigo = H00OT3_A39Contratada_Codigo[0];
            A77Contrato_Numero = H00OT3_A77Contrato_Numero[0];
            A39Contratada_Codigo = H00OT3_A39Contratada_Codigo[0];
            /* Using cursor H00OT4 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo, AV9UserId});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00OT4_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00OT4_A1079ContratoGestor_UsuarioCod[0];
               cmbavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)), A77Contrato_Numero, 0);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12OT2 */
         E12OT2 ();
         if (returnInSub) return;
      }

      protected void E12OT2( )
      {
         /* Enter Routine */
         if ( (0==AV5Contrato_Codigo) )
         {
            GX_msglist.addItem("Informe o novo contrato das demandas!");
         }
         else
         {
            new prc_alteracontrato(context ).execute(  AV5Contrato_Codigo,  AV12Servico,  AV9UserId) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Servico), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZZZ9")));
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13OT2( )
      {
         /* 'Cancelar' Routine */
         AV7WebSession.Remove("Codigos");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E14OT2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_OT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:40px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Servi�o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlteraContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 7,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla_Internalname, StringUtil.RTrim( AV14Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV14Servico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,7);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtavServico_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_WP_AlteraContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:40px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Contratos geridos:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlteraContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContrato_codigo, cmbavContrato_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)), 1, cmbavContrato_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"", "", true, "HLP_WP_AlteraContrato.htm");
            cmbavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_codigo_Internalname, "Values", (String)(cmbavContrato_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlteraContrato.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CANCELAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlteraContrato.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OT2e( true) ;
         }
         else
         {
            wb_table1_2_OT2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV10AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10AreaTrabalho_Codigo), "ZZZZZ9")));
         AV9UserId = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9UserId), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOT2( ) ;
         WSOT2( ) ;
         WEOT2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216263221");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_alteracontrato.js", "?20206216263221");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavServico_sigla_Internalname = "vSERVICO_SIGLA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         bttBtnenter_Visible = 1;
         cmbavContrato_codigo_Jsonclick = "";
         edtavServico_sigla_Jsonclick = "";
         edtavServico_sigla_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar contrato";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12OT2',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Servico',fld:'vSERVICO',pic:'ZZZZZ9',nv:0},{av:'AV9UserId',fld:'vUSERID',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'CANCELAR'","{handler:'E13OT2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV14Servico_Sigla = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11Codigos = new GxSimpleCollection();
         AV7WebSession = context.GetSession();
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H00OT2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00OT2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00OT2_A456ContagemResultado_Codigo = new int[1] ;
         H00OT2_A601ContagemResultado_Servico = new int[1] ;
         H00OT2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00OT2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00OT2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00OT2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00OT2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         H00OT3_A160ContratoServicos_Codigo = new int[1] ;
         H00OT3_A77Contrato_Numero = new String[] {""} ;
         H00OT3_A74Contrato_Codigo = new int[1] ;
         H00OT3_A155Servico_Codigo = new int[1] ;
         H00OT3_A39Contratada_Codigo = new int[1] ;
         A77Contrato_Numero = "";
         H00OT4_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00OT4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         sStyleString = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_alteracontrato__default(),
            new Object[][] {
                new Object[] {
               H00OT2_A1553ContagemResultado_CntSrvCod, H00OT2_n1553ContagemResultado_CntSrvCod, H00OT2_A456ContagemResultado_Codigo, H00OT2_A601ContagemResultado_Servico, H00OT2_n601ContagemResultado_Servico, H00OT2_A801ContagemResultado_ServicoSigla, H00OT2_n801ContagemResultado_ServicoSigla, H00OT2_A490ContagemResultado_ContratadaCod, H00OT2_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00OT3_A160ContratoServicos_Codigo, H00OT3_A77Contrato_Numero, H00OT3_A74Contrato_Codigo, H00OT3_A155Servico_Codigo, H00OT3_A39Contratada_Codigo
               }
               , new Object[] {
               H00OT4_A1078ContratoGestor_ContratoCod, H00OT4_A1079ContratoGestor_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV10AreaTrabalho_Codigo ;
      private int AV9UserId ;
      private int wcpOAV10AreaTrabalho_Codigo ;
      private int wcpOAV9UserId ;
      private int AV12Servico ;
      private int AV5Contrato_Codigo ;
      private int edtavServico_sigla_Enabled ;
      private int bttBtnenter_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV13Contratada ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV14Servico_Sigla ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavServico_sigla_Internalname ;
      private String cmbavContrato_codigo_Internalname ;
      private String bttBtnenter_Internalname ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A77Contrato_Numero ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String edtavServico_sigla_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavContrato_codigo_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n490ContagemResultado_ContratadaCod ;
      private IGxSession AV7WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavContrato_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00OT2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00OT2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00OT2_A456ContagemResultado_Codigo ;
      private int[] H00OT2_A601ContagemResultado_Servico ;
      private bool[] H00OT2_n601ContagemResultado_Servico ;
      private String[] H00OT2_A801ContagemResultado_ServicoSigla ;
      private bool[] H00OT2_n801ContagemResultado_ServicoSigla ;
      private int[] H00OT2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00OT2_n490ContagemResultado_ContratadaCod ;
      private int[] H00OT3_A160ContratoServicos_Codigo ;
      private String[] H00OT3_A77Contrato_Numero ;
      private int[] H00OT3_A74Contrato_Codigo ;
      private int[] H00OT3_A155Servico_Codigo ;
      private int[] H00OT3_A39Contratada_Codigo ;
      private int[] H00OT4_A1078ContratoGestor_ContratoCod ;
      private int[] H00OT4_A1079ContratoGestor_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV11Codigos ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class wp_alteracontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OT2 ;
          prmH00OT2 = new Object[] {
          } ;
          Object[] prmH00OT3 ;
          prmH00OT3 = new Object[] {
          new Object[] {"@AV12Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00OT4 ;
          prmH00OT4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9UserId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OT2", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_ContratadaCod] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OT2,100,0,false,false )
             ,new CursorDef("H00OT3", "SELECT T1.[ContratoServicos_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[Servico_Codigo], T2.[Contratada_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV12Servico) AND (T2.[Contratada_Codigo] = @AV13Contratada) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OT3,100,0,true,false )
             ,new CursorDef("H00OT4", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo and [ContratoGestor_UsuarioCod] = @AV9UserId ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OT4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
