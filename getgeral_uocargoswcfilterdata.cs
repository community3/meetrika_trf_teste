/*
               File: GetGeral_UOCargosWCFilterData
        Description: Get Geral_UOCargos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:3.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getgeral_uocargoswcfilterdata : GXProcedure
   {
      public getgeral_uocargoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getgeral_uocargoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getgeral_uocargoswcfilterdata objgetgeral_uocargoswcfilterdata;
         objgetgeral_uocargoswcfilterdata = new getgeral_uocargoswcfilterdata();
         objgetgeral_uocargoswcfilterdata.AV16DDOName = aP0_DDOName;
         objgetgeral_uocargoswcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetgeral_uocargoswcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetgeral_uocargoswcfilterdata.AV20OptionsJson = "" ;
         objgetgeral_uocargoswcfilterdata.AV23OptionsDescJson = "" ;
         objgetgeral_uocargoswcfilterdata.AV25OptionIndexesJson = "" ;
         objgetgeral_uocargoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetgeral_uocargoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetgeral_uocargoswcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getgeral_uocargoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_GRUPOCARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGRUPOCARGO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("Geral_UOCargosWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Geral_UOCargosWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("Geral_UOCargosWCGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "CARGO_ATIVO") == 0 )
            {
               AV32Cargo_Ativo = BooleanUtil.Val( AV30GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV10TFCargo_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV11TFCargo_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME") == 0 )
            {
               AV12TFGrupoCargo_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME_SEL") == 0 )
            {
               AV13TFGrupoCargo_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&CARGO_UOCOD") == 0 )
            {
               AV44Cargo_UOCod = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV34Cargo_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
            {
               AV35GrupoCargo_Codigo1 = (int)(NumberUtil.Val( AV31GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV38Cargo_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
               {
                  AV39GrupoCargo_Codigo2 = (int)(NumberUtil.Val( AV31GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV42Cargo_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
                  {
                     AV43GrupoCargo_Codigo3 = (int)(NumberUtil.Val( AV31GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCARGO_NOMEOPTIONS' Routine */
         AV10TFCargo_Nome = AV14SearchTxt;
         AV11TFCargo_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Cargo_Nome1 ,
                                              AV35GrupoCargo_Codigo1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Cargo_Nome2 ,
                                              AV39GrupoCargo_Codigo2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Cargo_Nome3 ,
                                              AV43GrupoCargo_Codigo3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV13TFGrupoCargo_Nome_Sel ,
                                              AV12TFGrupoCargo_Nome ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A616GrupoCargo_Nome ,
                                              A628Cargo_Ativo ,
                                              AV44Cargo_UOCod ,
                                              A1002Cargo_UOCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV34Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Cargo_Nome1), "%", "");
         lV38Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Cargo_Nome2), "%", "");
         lV42Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Cargo_Nome3), "%", "");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV12TFGrupoCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV12TFGrupoCargo_Nome), "%", "");
         /* Using cursor P00NQ2 */
         pr_default.execute(0, new Object[] {AV44Cargo_UOCod, lV34Cargo_Nome1, AV35GrupoCargo_Codigo1, lV38Cargo_Nome2, AV39GrupoCargo_Codigo2, lV42Cargo_Nome3, AV43GrupoCargo_Codigo3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, lV12TFGrupoCargo_Nome, AV13TFGrupoCargo_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNQ2 = false;
            A1002Cargo_UOCod = P00NQ2_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NQ2_n1002Cargo_UOCod[0];
            A628Cargo_Ativo = P00NQ2_A628Cargo_Ativo[0];
            A618Cargo_Nome = P00NQ2_A618Cargo_Nome[0];
            A616GrupoCargo_Nome = P00NQ2_A616GrupoCargo_Nome[0];
            A615GrupoCargo_Codigo = P00NQ2_A615GrupoCargo_Codigo[0];
            A617Cargo_Codigo = P00NQ2_A617Cargo_Codigo[0];
            A616GrupoCargo_Nome = P00NQ2_A616GrupoCargo_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00NQ2_A1002Cargo_UOCod[0] == A1002Cargo_UOCod ) && ( StringUtil.StrCmp(P00NQ2_A618Cargo_Nome[0], A618Cargo_Nome) == 0 ) )
            {
               BRKNQ2 = false;
               A617Cargo_Codigo = P00NQ2_A617Cargo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKNQ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A618Cargo_Nome)) )
            {
               AV18Option = A618Cargo_Nome;
               AV21OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")));
               AV19Options.Add(AV18Option, 0);
               AV22OptionsDesc.Add(AV21OptionDesc, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNQ2 )
            {
               BRKNQ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADGRUPOCARGO_NOMEOPTIONS' Routine */
         AV12TFGrupoCargo_Nome = AV14SearchTxt;
         AV13TFGrupoCargo_Nome_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Cargo_Nome1 ,
                                              AV35GrupoCargo_Codigo1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Cargo_Nome2 ,
                                              AV39GrupoCargo_Codigo2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Cargo_Nome3 ,
                                              AV43GrupoCargo_Codigo3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV13TFGrupoCargo_Nome_Sel ,
                                              AV12TFGrupoCargo_Nome ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A616GrupoCargo_Nome ,
                                              A628Cargo_Ativo ,
                                              AV44Cargo_UOCod ,
                                              A1002Cargo_UOCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV34Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Cargo_Nome1), "%", "");
         lV38Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Cargo_Nome2), "%", "");
         lV42Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Cargo_Nome3), "%", "");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV12TFGrupoCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV12TFGrupoCargo_Nome), "%", "");
         /* Using cursor P00NQ3 */
         pr_default.execute(1, new Object[] {AV44Cargo_UOCod, lV34Cargo_Nome1, AV35GrupoCargo_Codigo1, lV38Cargo_Nome2, AV39GrupoCargo_Codigo2, lV42Cargo_Nome3, AV43GrupoCargo_Codigo3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, lV12TFGrupoCargo_Nome, AV13TFGrupoCargo_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNQ4 = false;
            A1002Cargo_UOCod = P00NQ3_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NQ3_n1002Cargo_UOCod[0];
            A628Cargo_Ativo = P00NQ3_A628Cargo_Ativo[0];
            A616GrupoCargo_Nome = P00NQ3_A616GrupoCargo_Nome[0];
            A615GrupoCargo_Codigo = P00NQ3_A615GrupoCargo_Codigo[0];
            A618Cargo_Nome = P00NQ3_A618Cargo_Nome[0];
            A617Cargo_Codigo = P00NQ3_A617Cargo_Codigo[0];
            A616GrupoCargo_Nome = P00NQ3_A616GrupoCargo_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NQ3_A1002Cargo_UOCod[0] == A1002Cargo_UOCod ) && ( StringUtil.StrCmp(P00NQ3_A616GrupoCargo_Nome[0], A616GrupoCargo_Nome) == 0 ) )
            {
               BRKNQ4 = false;
               A615GrupoCargo_Codigo = P00NQ3_A615GrupoCargo_Codigo[0];
               A617Cargo_Codigo = P00NQ3_A617Cargo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKNQ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A616GrupoCargo_Nome)) )
            {
               AV18Option = A616GrupoCargo_Nome;
               AV21OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A616GrupoCargo_Nome, "@!")));
               AV19Options.Add(AV18Option, 0);
               AV22OptionsDesc.Add(AV21OptionDesc, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNQ4 )
            {
               BRKNQ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV32Cargo_Ativo = true;
         AV10TFCargo_Nome = "";
         AV11TFCargo_Nome_Sel = "";
         AV12TFGrupoCargo_Nome = "";
         AV13TFGrupoCargo_Nome_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Cargo_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Cargo_Nome2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42Cargo_Nome3 = "";
         scmdbuf = "";
         lV34Cargo_Nome1 = "";
         lV38Cargo_Nome2 = "";
         lV42Cargo_Nome3 = "";
         lV10TFCargo_Nome = "";
         lV12TFGrupoCargo_Nome = "";
         A618Cargo_Nome = "";
         A616GrupoCargo_Nome = "";
         P00NQ2_A1002Cargo_UOCod = new int[1] ;
         P00NQ2_n1002Cargo_UOCod = new bool[] {false} ;
         P00NQ2_A628Cargo_Ativo = new bool[] {false} ;
         P00NQ2_A618Cargo_Nome = new String[] {""} ;
         P00NQ2_A616GrupoCargo_Nome = new String[] {""} ;
         P00NQ2_A615GrupoCargo_Codigo = new int[1] ;
         P00NQ2_A617Cargo_Codigo = new int[1] ;
         AV18Option = "";
         AV21OptionDesc = "";
         P00NQ3_A1002Cargo_UOCod = new int[1] ;
         P00NQ3_n1002Cargo_UOCod = new bool[] {false} ;
         P00NQ3_A628Cargo_Ativo = new bool[] {false} ;
         P00NQ3_A616GrupoCargo_Nome = new String[] {""} ;
         P00NQ3_A615GrupoCargo_Codigo = new int[1] ;
         P00NQ3_A618Cargo_Nome = new String[] {""} ;
         P00NQ3_A617Cargo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getgeral_uocargoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NQ2_A1002Cargo_UOCod, P00NQ2_n1002Cargo_UOCod, P00NQ2_A628Cargo_Ativo, P00NQ2_A618Cargo_Nome, P00NQ2_A616GrupoCargo_Nome, P00NQ2_A615GrupoCargo_Codigo, P00NQ2_A617Cargo_Codigo
               }
               , new Object[] {
               P00NQ3_A1002Cargo_UOCod, P00NQ3_n1002Cargo_UOCod, P00NQ3_A628Cargo_Ativo, P00NQ3_A616GrupoCargo_Nome, P00NQ3_A615GrupoCargo_Codigo, P00NQ3_A618Cargo_Nome, P00NQ3_A617Cargo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV47GXV1 ;
      private int AV44Cargo_UOCod ;
      private int AV35GrupoCargo_Codigo1 ;
      private int AV39GrupoCargo_Codigo2 ;
      private int AV43GrupoCargo_Codigo3 ;
      private int A615GrupoCargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int A617Cargo_Codigo ;
      private long AV26count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV32Cargo_Ativo ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool A628Cargo_Ativo ;
      private bool BRKNQ2 ;
      private bool n1002Cargo_UOCod ;
      private bool BRKNQ4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFCargo_Nome ;
      private String AV11TFCargo_Nome_Sel ;
      private String AV12TFGrupoCargo_Nome ;
      private String AV13TFGrupoCargo_Nome_Sel ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV34Cargo_Nome1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV38Cargo_Nome2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV42Cargo_Nome3 ;
      private String lV34Cargo_Nome1 ;
      private String lV38Cargo_Nome2 ;
      private String lV42Cargo_Nome3 ;
      private String lV10TFCargo_Nome ;
      private String lV12TFGrupoCargo_Nome ;
      private String A618Cargo_Nome ;
      private String A616GrupoCargo_Nome ;
      private String AV18Option ;
      private String AV21OptionDesc ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NQ2_A1002Cargo_UOCod ;
      private bool[] P00NQ2_n1002Cargo_UOCod ;
      private bool[] P00NQ2_A628Cargo_Ativo ;
      private String[] P00NQ2_A618Cargo_Nome ;
      private String[] P00NQ2_A616GrupoCargo_Nome ;
      private int[] P00NQ2_A615GrupoCargo_Codigo ;
      private int[] P00NQ2_A617Cargo_Codigo ;
      private int[] P00NQ3_A1002Cargo_UOCod ;
      private bool[] P00NQ3_n1002Cargo_UOCod ;
      private bool[] P00NQ3_A628Cargo_Ativo ;
      private String[] P00NQ3_A616GrupoCargo_Nome ;
      private int[] P00NQ3_A615GrupoCargo_Codigo ;
      private String[] P00NQ3_A618Cargo_Nome ;
      private int[] P00NQ3_A617Cargo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getgeral_uocargoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NQ2( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Cargo_Nome1 ,
                                             int AV35GrupoCargo_Codigo1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Cargo_Nome2 ,
                                             int AV39GrupoCargo_Codigo2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Cargo_Nome3 ,
                                             int AV43GrupoCargo_Codigo3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             String AV13TFGrupoCargo_Nome_Sel ,
                                             String AV12TFGrupoCargo_Nome ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             String A616GrupoCargo_Nome ,
                                             bool A628Cargo_Ativo ,
                                             int AV44Cargo_UOCod ,
                                             int A1002Cargo_UOCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod], T1.[Cargo_Ativo], T1.[Cargo_Nome], T2.[GrupoCargo_Nome], T1.[GrupoCargo_Codigo], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) INNER JOIN [Geral_Grupo_Cargo] T2 WITH (NOLOCK) ON T2.[GrupoCargo_Codigo] = T1.[GrupoCargo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Cargo_UOCod] = @AV44Cargo_UOCod)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV34Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV35GrupoCargo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV35GrupoCargo_Codigo1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV38Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV39GrupoCargo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV39GrupoCargo_Codigo2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV42Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV43GrupoCargo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV43GrupoCargo_Codigo3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFGrupoCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFGrupoCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] like @lV12TFGrupoCargo_Nome)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFGrupoCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] = @AV13TFGrupoCargo_Nome_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_UOCod], T1.[Cargo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NQ3( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Cargo_Nome1 ,
                                             int AV35GrupoCargo_Codigo1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Cargo_Nome2 ,
                                             int AV39GrupoCargo_Codigo2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Cargo_Nome3 ,
                                             int AV43GrupoCargo_Codigo3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             String AV13TFGrupoCargo_Nome_Sel ,
                                             String AV12TFGrupoCargo_Nome ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             String A616GrupoCargo_Nome ,
                                             bool A628Cargo_Ativo ,
                                             int AV44Cargo_UOCod ,
                                             int A1002Cargo_UOCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod], T1.[Cargo_Ativo], T2.[GrupoCargo_Nome], T1.[GrupoCargo_Codigo], T1.[Cargo_Nome], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) INNER JOIN [Geral_Grupo_Cargo] T2 WITH (NOLOCK) ON T2.[GrupoCargo_Codigo] = T1.[GrupoCargo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Cargo_UOCod] = @AV44Cargo_UOCod)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV34Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV35GrupoCargo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV35GrupoCargo_Codigo1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV38Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV39GrupoCargo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV39GrupoCargo_Codigo2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV42Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV43GrupoCargo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV43GrupoCargo_Codigo3)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFGrupoCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFGrupoCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] like @lV12TFGrupoCargo_Nome)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFGrupoCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] = @AV13TFGrupoCargo_Nome_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_UOCod], T2.[GrupoCargo_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NQ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
               case 1 :
                     return conditional_P00NQ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NQ2 ;
          prmP00NQ2 = new Object[] {
          new Object[] {"@AV44Cargo_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV35GrupoCargo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV38Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV39GrupoCargo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV42Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV43GrupoCargo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV12TFGrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV13TFGrupoCargo_Nome_Sel",SqlDbType.VarChar,80,0}
          } ;
          Object[] prmP00NQ3 ;
          prmP00NQ3 = new Object[] {
          new Object[] {"@AV44Cargo_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV35GrupoCargo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV38Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV39GrupoCargo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV42Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV43GrupoCargo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV12TFGrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV13TFGrupoCargo_Nome_Sel",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NQ2,100,0,true,false )
             ,new CursorDef("P00NQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NQ3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getgeral_uocargoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getgeral_uocargoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getgeral_uocargoswcfilterdata") )
          {
             return  ;
          }
          getgeral_uocargoswcfilterdata worker = new getgeral_uocargoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
