/*
               File: PRC_NotificaoesParaDestNome
        Description: Filtro Notificacoes enviadas para Destinatario Nome
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:13.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_notificaoesparadestnome : GXProcedure
   {
      public prc_notificaoesparadestnome( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_notificaoesparadestnome( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_Operador1 ,
                           String aP1_DestNome1 ,
                           short aP2_Operador2 ,
                           String aP3_DestNome2 ,
                           short aP4_Operador3 ,
                           String aP5_DestNome3 ,
                           short aP6_Operador4 ,
                           String aP7_DestNome4 ,
                           out IGxCollection aP8_Codigos )
      {
         this.AV10Operador1 = aP0_Operador1;
         this.AV9DestNome1 = aP1_DestNome1;
         this.AV13Operador2 = aP2_Operador2;
         this.AV12DestNome2 = aP3_DestNome2;
         this.AV15Operador3 = aP4_Operador3;
         this.AV14DestNome3 = aP5_DestNome3;
         this.AV17Operador4 = aP6_Operador4;
         this.AV16DestNome4 = aP7_DestNome4;
         this.AV8Codigos = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP8_Codigos=this.AV8Codigos;
      }

      public IGxCollection executeUdp( short aP0_Operador1 ,
                                       String aP1_DestNome1 ,
                                       short aP2_Operador2 ,
                                       String aP3_DestNome2 ,
                                       short aP4_Operador3 ,
                                       String aP5_DestNome3 ,
                                       short aP6_Operador4 ,
                                       String aP7_DestNome4 )
      {
         this.AV10Operador1 = aP0_Operador1;
         this.AV9DestNome1 = aP1_DestNome1;
         this.AV13Operador2 = aP2_Operador2;
         this.AV12DestNome2 = aP3_DestNome2;
         this.AV15Operador3 = aP4_Operador3;
         this.AV14DestNome3 = aP5_DestNome3;
         this.AV17Operador4 = aP6_Operador4;
         this.AV16DestNome4 = aP7_DestNome4;
         this.AV8Codigos = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP8_Codigos=this.AV8Codigos;
         return AV8Codigos ;
      }

      public void executeSubmit( short aP0_Operador1 ,
                                 String aP1_DestNome1 ,
                                 short aP2_Operador2 ,
                                 String aP3_DestNome2 ,
                                 short aP4_Operador3 ,
                                 String aP5_DestNome3 ,
                                 short aP6_Operador4 ,
                                 String aP7_DestNome4 ,
                                 out IGxCollection aP8_Codigos )
      {
         prc_notificaoesparadestnome objprc_notificaoesparadestnome;
         objprc_notificaoesparadestnome = new prc_notificaoesparadestnome();
         objprc_notificaoesparadestnome.AV10Operador1 = aP0_Operador1;
         objprc_notificaoesparadestnome.AV9DestNome1 = aP1_DestNome1;
         objprc_notificaoesparadestnome.AV13Operador2 = aP2_Operador2;
         objprc_notificaoesparadestnome.AV12DestNome2 = aP3_DestNome2;
         objprc_notificaoesparadestnome.AV15Operador3 = aP4_Operador3;
         objprc_notificaoesparadestnome.AV14DestNome3 = aP5_DestNome3;
         objprc_notificaoesparadestnome.AV17Operador4 = aP6_Operador4;
         objprc_notificaoesparadestnome.AV16DestNome4 = aP7_DestNome4;
         objprc_notificaoesparadestnome.AV8Codigos = new GxSimpleCollection() ;
         objprc_notificaoesparadestnome.context.SetSubmitInitialConfig(context);
         objprc_notificaoesparadestnome.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_notificaoesparadestnome);
         aP8_Codigos=this.AV8Codigos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_notificaoesparadestnome)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9DestNome1 ,
                                              AV10Operador1 ,
                                              AV12DestNome2 ,
                                              AV13Operador2 ,
                                              AV14DestNome3 ,
                                              AV15Operador3 ,
                                              AV16DestNome4 ,
                                              AV17Operador4 ,
                                              A1421ContagemResultadoNotificacao_DestNome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV9DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV9DestNome1), 50, "%");
         lV9DestNome1 = StringUtil.PadR( StringUtil.RTrim( AV9DestNome1), 50, "%");
         lV12DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV12DestNome2), 50, "%");
         lV12DestNome2 = StringUtil.PadR( StringUtil.RTrim( AV12DestNome2), 50, "%");
         lV14DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV14DestNome3), 50, "%");
         lV14DestNome3 = StringUtil.PadR( StringUtil.RTrim( AV14DestNome3), 50, "%");
         lV16DestNome4 = StringUtil.PadR( StringUtil.RTrim( AV16DestNome4), 50, "%");
         lV16DestNome4 = StringUtil.PadR( StringUtil.RTrim( AV16DestNome4), 50, "%");
         /* Using cursor P00VF2 */
         pr_default.execute(0, new Object[] {lV9DestNome1, lV9DestNome1, lV12DestNome2, lV12DestNome2, lV14DestNome3, lV14DestNome3, lV16DestNome4, lV16DestNome4});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1414ContagemResultadoNotificacao_DestCod = P00VF2_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VF2_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VF2_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VF2_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VF2_n1421ContagemResultadoNotificacao_DestNome[0];
            A1412ContagemResultadoNotificacao_Codigo = P00VF2_A1412ContagemResultadoNotificacao_Codigo[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VF2_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VF2_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VF2_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VF2_n1421ContagemResultadoNotificacao_DestNome[0];
            AV8Codigos.Add(A1412ContagemResultadoNotificacao_Codigo, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         lV9DestNome1 = "";
         lV12DestNome2 = "";
         lV14DestNome3 = "";
         lV16DestNome4 = "";
         A1421ContagemResultadoNotificacao_DestNome = "";
         P00VF2_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         P00VF2_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         P00VF2_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         P00VF2_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         P00VF2_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         P00VF2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_notificaoesparadestnome__default(),
            new Object[][] {
                new Object[] {
               P00VF2_A1414ContagemResultadoNotificacao_DestCod, P00VF2_A1426ContagemResultadoNotificacao_DestPesCod, P00VF2_n1426ContagemResultadoNotificacao_DestPesCod, P00VF2_A1421ContagemResultadoNotificacao_DestNome, P00VF2_n1421ContagemResultadoNotificacao_DestNome, P00VF2_A1412ContagemResultadoNotificacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Operador1 ;
      private short AV13Operador2 ;
      private short AV15Operador3 ;
      private short AV17Operador4 ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private String AV9DestNome1 ;
      private String AV12DestNome2 ;
      private String AV14DestNome3 ;
      private String AV16DestNome4 ;
      private String scmdbuf ;
      private String lV9DestNome1 ;
      private String lV12DestNome2 ;
      private String lV14DestNome3 ;
      private String lV16DestNome4 ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VF2_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] P00VF2_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] P00VF2_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] P00VF2_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] P00VF2_n1421ContagemResultadoNotificacao_DestNome ;
      private long[] P00VF2_A1412ContagemResultadoNotificacao_Codigo ;
      private IGxCollection aP8_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
   }

   public class prc_notificaoesparadestnome__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VF2( IGxContext context ,
                                             String AV9DestNome1 ,
                                             short AV10Operador1 ,
                                             String AV12DestNome2 ,
                                             short AV13Operador2 ,
                                             String AV14DestNome3 ,
                                             short AV15Operador3 ,
                                             String AV16DestNome4 ,
                                             short AV17Operador4 ,
                                             String A1421ContagemResultadoNotificacao_DestNome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_DestCod, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_DestPesCod, T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_DestNome, T1.[ContagemResultadoNotificacao_Codigo] FROM (([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9DestNome1)) && ( AV10Operador1 == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV9DestNome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV9DestNome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9DestNome1)) && ( AV10Operador1 == 1 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV9DestNome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV9DestNome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12DestNome2)) && ( AV13Operador2 == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12DestNome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV12DestNome2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12DestNome2)) && ( AV13Operador2 == 1 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV12DestNome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV12DestNome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14DestNome3)) && ( AV15Operador3 == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14DestNome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV14DestNome3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14DestNome3)) && ( AV15Operador3 == 1 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV14DestNome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV14DestNome3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16DestNome4)) && ( AV17Operador4 == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV16DestNome4)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV16DestNome4)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16DestNome4)) && ( AV17Operador4 == 1 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV16DestNome4)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV16DestNome4)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00VF2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VF2 ;
          prmP00VF2 = new Object[] {
          new Object[] {"@lV9DestNome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV9DestNome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12DestNome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12DestNome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14DestNome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14DestNome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16DestNome4",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16DestNome4",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VF2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
