/*
               File: ProjetoEquipeConversion
        Description: Conversion for table ProjetoEquipe
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/5/2020 18:40:51.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
namespace GeneXus.Programs {
   public class projetoequipeconversion : GXProcedure
   {
      public projetoequipeconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public projetoequipeconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         projetoequipeconversion objprojetoequipeconversion;
         objprojetoequipeconversion = new projetoequipeconversion();
         objprojetoequipeconversion.context.SetSubmitInitialConfig(context);
         objprojetoequipeconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprojetoequipeconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((projetoequipeconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor PROJETOEQU2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2155Projeto_SistemaCodigo = PROJETOEQU2_A2155Projeto_SistemaCodigo[0];
            n2155Projeto_SistemaCodigo = PROJETOEQU2_n2155Projeto_SistemaCodigo[0];
            A1399Sistema_ImpUserCod = PROJETOEQU2_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = PROJETOEQU2_n1399Sistema_ImpUserCod[0];
            A648Projeto_Codigo = PROJETOEQU2_A648Projeto_Codigo[0];
            A1399Sistema_ImpUserCod = PROJETOEQU2_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = PROJETOEQU2_n1399Sistema_ImpUserCod[0];
            /*
               INSERT RECORD ON TABLE GXA0236

            */
            AV2Projeto_Codigo = A648Projeto_Codigo;
            if ( PROJETOEQU2_n1399Sistema_ImpUserCod[0] )
            {
               AV3Usuario_Codigo = 0;
            }
            else
            {
               AV3Usuario_Codigo = A1399Sistema_ImpUserCod;
            }
            AV4FuncaoUsuario_Codigo = 0;
            /* Using cursor PROJETOEQU3 */
            pr_default.execute(1, new Object[] {AV2Projeto_Codigo, AV3Usuario_Codigo, AV4FuncaoUsuario_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0236") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PROJETOEQU2_A2155Projeto_SistemaCodigo = new int[1] ;
         PROJETOEQU2_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         PROJETOEQU2_A1399Sistema_ImpUserCod = new int[1] ;
         PROJETOEQU2_n1399Sistema_ImpUserCod = new bool[] {false} ;
         PROJETOEQU2_A648Projeto_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetoequipeconversion__default(),
            new Object[][] {
                new Object[] {
               PROJETOEQU2_A2155Projeto_SistemaCodigo, PROJETOEQU2_n2155Projeto_SistemaCodigo, PROJETOEQU2_A1399Sistema_ImpUserCod, PROJETOEQU2_n1399Sistema_ImpUserCod, PROJETOEQU2_A648Projeto_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2155Projeto_SistemaCodigo ;
      private int A1399Sistema_ImpUserCod ;
      private int A648Projeto_Codigo ;
      private int GIGXA0236 ;
      private int AV2Projeto_Codigo ;
      private int AV3Usuario_Codigo ;
      private int AV4FuncaoUsuario_Codigo ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool n2155Projeto_SistemaCodigo ;
      private bool n1399Sistema_ImpUserCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] PROJETOEQU2_A2155Projeto_SistemaCodigo ;
      private bool[] PROJETOEQU2_n2155Projeto_SistemaCodigo ;
      private int[] PROJETOEQU2_A1399Sistema_ImpUserCod ;
      private bool[] PROJETOEQU2_n1399Sistema_ImpUserCod ;
      private int[] PROJETOEQU2_A648Projeto_Codigo ;
   }

   public class projetoequipeconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmPROJETOEQU2 ;
          prmPROJETOEQU2 = new Object[] {
          } ;
          Object[] prmPROJETOEQU3 ;
          prmPROJETOEQU3 = new Object[] {
          new Object[] {"@AV2Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV3Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV4FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("PROJETOEQU2", "SELECT T1.[Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, T2.[Sistema_ImpUserCod], T1.[Projeto_Codigo] FROM ([Projeto] T1 LEFT JOIN [Sistema] T2 ON T2.[Sistema_Codigo] = T1.[Projeto_SistemaCodigo]) ORDER BY T1.[Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmPROJETOEQU2,100,0,true,false )
             ,new CursorDef("PROJETOEQU3", "INSERT INTO [GXA0236]([Projeto_Codigo], [Usuario_Codigo], [FuncaoUsuario_Codigo]) VALUES(@AV2Projeto_Codigo, @AV3Usuario_Codigo, @AV4FuncaoUsuario_Codigo)", GxErrorMask.GX_NOMASK,prmPROJETOEQU3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
