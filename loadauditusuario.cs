/*
               File: LoadAuditUsuario
        Description: Load Audit Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:52.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditusuario : GXProcedure
   {
      public loadauditusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Usuario_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Usuario_Codigo = aP2_Usuario_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Usuario_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditusuario objloadauditusuario;
         objloadauditusuario = new loadauditusuario();
         objloadauditusuario.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditusuario.AV10AuditingObject = aP1_AuditingObject;
         objloadauditusuario.AV16Usuario_Codigo = aP2_Usuario_Codigo;
         objloadauditusuario.AV14ActualMode = aP3_ActualMode;
         objloadauditusuario.context.SetSubmitInitialConfig(context);
         objloadauditusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditusuario);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A02 */
         pr_default.execute(0, new Object[] {AV16Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1073Usuario_CargoCod = P00A02_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P00A02_n1073Usuario_CargoCod[0];
            A1074Usuario_CargoNom = P00A02_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00A02_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = P00A02_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P00A02_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = P00A02_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = P00A02_n1076Usuario_CargoUONom[0];
            A57Usuario_PessoaCod = P00A02_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00A02_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00A02_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = P00A02_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = P00A02_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = P00A02_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00A02_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = P00A02_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = P00A02_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = P00A02_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = P00A02_A2Usuario_Nome[0];
            n2Usuario_Nome = P00A02_n2Usuario_Nome[0];
            A289Usuario_EhContador = P00A02_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = P00A02_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = P00A02_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = P00A02_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = P00A02_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = P00A02_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = P00A02_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = P00A02_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = P00A02_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = P00A02_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = P00A02_A54Usuario_Ativo[0];
            A1647Usuario_Email = P00A02_A1647Usuario_Email[0];
            n1647Usuario_Email = P00A02_n1647Usuario_Email[0];
            A1908Usuario_DeFerias = P00A02_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = P00A02_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = P00A02_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = P00A02_n2016Usuario_UltimaArea[0];
            A1Usuario_Codigo = P00A02_A1Usuario_Codigo[0];
            A1074Usuario_CargoNom = P00A02_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00A02_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = P00A02_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P00A02_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = P00A02_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = P00A02_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = P00A02_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00A02_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = P00A02_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = P00A02_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = P00A02_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00A02_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = P00A02_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = P00A02_n2095Usuario_PessoaTelefone[0];
            GXt_boolean1 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean1) ;
            A290Usuario_EhAuditorFM = GXt_boolean1;
            GXt_char2 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char2) ;
            A1083Usuario_Entidade = GXt_char2;
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Usuario";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cargo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cargo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1074Usuario_CargoNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoUOCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade organizacional";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoUONom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade organizacional";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1076Usuario_CargoUONom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Entidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Entidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1083Usuario_Entidade;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A58Usuario_PessoaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaTip";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A59Usuario_PessoaTip;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaDoc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Documento(CPF)";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A325Usuario_PessoaDoc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaTelefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2095Usuario_PessoaTelefone;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_UserGamGuid";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Guid";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A341Usuario_UserGamGuid;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2Usuario_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contador?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A289Usuario_EhContador);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhAuditorFM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Auditor FM?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A290Usuario_EhAuditorFM);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A291Usuario_EhContratada);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContratante";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A292Usuario_EhContratante);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhFinanceiro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Financeiro?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A293Usuario_EhFinanceiro);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhGestor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "� Gestor?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A538Usuario_EhGestor);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhPreposto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1093Usuario_EhPreposto);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CrtfPath";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Certificado Digital";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1017Usuario_CrtfPath;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Notificar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Notifica��es (inativo)";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1235Usuario_Notificar;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A54Usuario_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Email";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "E-mail do Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1647Usuario_Email;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_DeFerias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "De f�rias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1908Usuario_DeFerias);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_UltimaArea";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ultima �rea";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2016Usuario_UltimaArea), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A03 */
         pr_default.execute(1, new Object[] {AV16Usuario_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1073Usuario_CargoCod = P00A03_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P00A03_n1073Usuario_CargoCod[0];
            A1074Usuario_CargoNom = P00A03_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00A03_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = P00A03_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P00A03_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = P00A03_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = P00A03_n1076Usuario_CargoUONom[0];
            A57Usuario_PessoaCod = P00A03_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00A03_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00A03_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = P00A03_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = P00A03_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = P00A03_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00A03_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = P00A03_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = P00A03_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = P00A03_A341Usuario_UserGamGuid[0];
            A2Usuario_Nome = P00A03_A2Usuario_Nome[0];
            n2Usuario_Nome = P00A03_n2Usuario_Nome[0];
            A289Usuario_EhContador = P00A03_A289Usuario_EhContador[0];
            A291Usuario_EhContratada = P00A03_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = P00A03_A292Usuario_EhContratante[0];
            A293Usuario_EhFinanceiro = P00A03_A293Usuario_EhFinanceiro[0];
            A538Usuario_EhGestor = P00A03_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = P00A03_A1093Usuario_EhPreposto[0];
            A1017Usuario_CrtfPath = P00A03_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = P00A03_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = P00A03_A1235Usuario_Notificar[0];
            n1235Usuario_Notificar = P00A03_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = P00A03_A54Usuario_Ativo[0];
            A1647Usuario_Email = P00A03_A1647Usuario_Email[0];
            n1647Usuario_Email = P00A03_n1647Usuario_Email[0];
            A1908Usuario_DeFerias = P00A03_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = P00A03_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = P00A03_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = P00A03_n2016Usuario_UltimaArea[0];
            A1Usuario_Codigo = P00A03_A1Usuario_Codigo[0];
            A1074Usuario_CargoNom = P00A03_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = P00A03_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = P00A03_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P00A03_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = P00A03_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = P00A03_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = P00A03_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00A03_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = P00A03_A59Usuario_PessoaTip[0];
            n59Usuario_PessoaTip = P00A03_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = P00A03_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P00A03_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = P00A03_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = P00A03_n2095Usuario_PessoaTelefone[0];
            GXt_boolean1 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean1) ;
            A290Usuario_EhAuditorFM = GXt_boolean1;
            GXt_char2 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char2) ;
            A1083Usuario_Entidade = GXt_char2;
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Usuario";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cargo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cargo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1074Usuario_CargoNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoUOCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade organizacional";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CargoUONom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade organizacional";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1076Usuario_CargoUONom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Entidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Entidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1083Usuario_Entidade;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A58Usuario_PessoaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaTip";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A59Usuario_PessoaTip;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaDoc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Documento(CPF)";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A325Usuario_PessoaDoc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_PessoaTelefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2095Usuario_PessoaTelefone;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_UserGamGuid";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Guid";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A341Usuario_UserGamGuid;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2Usuario_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contador?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A289Usuario_EhContador);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhAuditorFM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Auditor FM?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A290Usuario_EhAuditorFM);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A291Usuario_EhContratada);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhContratante";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A292Usuario_EhContratante);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhFinanceiro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Financeiro?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A293Usuario_EhFinanceiro);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhGestor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "� Gestor?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A538Usuario_EhGestor);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_EhPreposto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1093Usuario_EhPreposto);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_CrtfPath";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Certificado Digital";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1017Usuario_CrtfPath;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Notificar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Notifica��es (inativo)";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1235Usuario_Notificar;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A54Usuario_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_Email";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "E-mail do Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1647Usuario_Email;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_DeFerias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "De f�rias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1908Usuario_DeFerias);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_UltimaArea";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ultima �rea";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2016Usuario_UltimaArea), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A03_A1Usuario_Codigo[0] == A1Usuario_Codigo ) )
                  {
                     A1073Usuario_CargoCod = P00A03_A1073Usuario_CargoCod[0];
                     n1073Usuario_CargoCod = P00A03_n1073Usuario_CargoCod[0];
                     A1074Usuario_CargoNom = P00A03_A1074Usuario_CargoNom[0];
                     n1074Usuario_CargoNom = P00A03_n1074Usuario_CargoNom[0];
                     A1075Usuario_CargoUOCod = P00A03_A1075Usuario_CargoUOCod[0];
                     n1075Usuario_CargoUOCod = P00A03_n1075Usuario_CargoUOCod[0];
                     A1076Usuario_CargoUONom = P00A03_A1076Usuario_CargoUONom[0];
                     n1076Usuario_CargoUONom = P00A03_n1076Usuario_CargoUONom[0];
                     A57Usuario_PessoaCod = P00A03_A57Usuario_PessoaCod[0];
                     A58Usuario_PessoaNom = P00A03_A58Usuario_PessoaNom[0];
                     n58Usuario_PessoaNom = P00A03_n58Usuario_PessoaNom[0];
                     A59Usuario_PessoaTip = P00A03_A59Usuario_PessoaTip[0];
                     n59Usuario_PessoaTip = P00A03_n59Usuario_PessoaTip[0];
                     A325Usuario_PessoaDoc = P00A03_A325Usuario_PessoaDoc[0];
                     n325Usuario_PessoaDoc = P00A03_n325Usuario_PessoaDoc[0];
                     A2095Usuario_PessoaTelefone = P00A03_A2095Usuario_PessoaTelefone[0];
                     n2095Usuario_PessoaTelefone = P00A03_n2095Usuario_PessoaTelefone[0];
                     A341Usuario_UserGamGuid = P00A03_A341Usuario_UserGamGuid[0];
                     A2Usuario_Nome = P00A03_A2Usuario_Nome[0];
                     n2Usuario_Nome = P00A03_n2Usuario_Nome[0];
                     A289Usuario_EhContador = P00A03_A289Usuario_EhContador[0];
                     A291Usuario_EhContratada = P00A03_A291Usuario_EhContratada[0];
                     A292Usuario_EhContratante = P00A03_A292Usuario_EhContratante[0];
                     A293Usuario_EhFinanceiro = P00A03_A293Usuario_EhFinanceiro[0];
                     A538Usuario_EhGestor = P00A03_A538Usuario_EhGestor[0];
                     A1093Usuario_EhPreposto = P00A03_A1093Usuario_EhPreposto[0];
                     A1017Usuario_CrtfPath = P00A03_A1017Usuario_CrtfPath[0];
                     n1017Usuario_CrtfPath = P00A03_n1017Usuario_CrtfPath[0];
                     A1235Usuario_Notificar = P00A03_A1235Usuario_Notificar[0];
                     n1235Usuario_Notificar = P00A03_n1235Usuario_Notificar[0];
                     A54Usuario_Ativo = P00A03_A54Usuario_Ativo[0];
                     A1647Usuario_Email = P00A03_A1647Usuario_Email[0];
                     n1647Usuario_Email = P00A03_n1647Usuario_Email[0];
                     A1908Usuario_DeFerias = P00A03_A1908Usuario_DeFerias[0];
                     n1908Usuario_DeFerias = P00A03_n1908Usuario_DeFerias[0];
                     A2016Usuario_UltimaArea = P00A03_A2016Usuario_UltimaArea[0];
                     n2016Usuario_UltimaArea = P00A03_n2016Usuario_UltimaArea[0];
                     A1074Usuario_CargoNom = P00A03_A1074Usuario_CargoNom[0];
                     n1074Usuario_CargoNom = P00A03_n1074Usuario_CargoNom[0];
                     A1075Usuario_CargoUOCod = P00A03_A1075Usuario_CargoUOCod[0];
                     n1075Usuario_CargoUOCod = P00A03_n1075Usuario_CargoUOCod[0];
                     A1076Usuario_CargoUONom = P00A03_A1076Usuario_CargoUONom[0];
                     n1076Usuario_CargoUONom = P00A03_n1076Usuario_CargoUONom[0];
                     A58Usuario_PessoaNom = P00A03_A58Usuario_PessoaNom[0];
                     n58Usuario_PessoaNom = P00A03_n58Usuario_PessoaNom[0];
                     A59Usuario_PessoaTip = P00A03_A59Usuario_PessoaTip[0];
                     n59Usuario_PessoaTip = P00A03_n59Usuario_PessoaTip[0];
                     A325Usuario_PessoaDoc = P00A03_A325Usuario_PessoaDoc[0];
                     n325Usuario_PessoaDoc = P00A03_n325Usuario_PessoaDoc[0];
                     A2095Usuario_PessoaTelefone = P00A03_A2095Usuario_PessoaTelefone[0];
                     n2095Usuario_PessoaTelefone = P00A03_n2095Usuario_PessoaTelefone[0];
                     GXt_boolean1 = A290Usuario_EhAuditorFM;
                     new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean1) ;
                     A290Usuario_EhAuditorFM = GXt_boolean1;
                     GXt_char2 = A1083Usuario_Entidade;
                     new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char2) ;
                     A1083Usuario_Entidade = GXt_char2;
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_CargoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_CargoNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1074Usuario_CargoNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_CargoUOCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_CargoUONom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1076Usuario_CargoUONom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Entidade") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1083Usuario_Entidade;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_PessoaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A58Usuario_PessoaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_PessoaTip") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A59Usuario_PessoaTip;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_PessoaDoc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A325Usuario_PessoaDoc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_PessoaTelefone") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2095Usuario_PessoaTelefone;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_UserGamGuid") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A341Usuario_UserGamGuid;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2Usuario_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhContador") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A289Usuario_EhContador);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhAuditorFM") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A290Usuario_EhAuditorFM);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhContratada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A291Usuario_EhContratada);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhContratante") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A292Usuario_EhContratante);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhFinanceiro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A293Usuario_EhFinanceiro);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhGestor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A538Usuario_EhGestor);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_EhPreposto") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1093Usuario_EhPreposto);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_CrtfPath") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1017Usuario_CrtfPath;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Notificar") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1235Usuario_Notificar;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A54Usuario_Ativo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_Email") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1647Usuario_Email;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_DeFerias") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1908Usuario_DeFerias);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Usuario_UltimaArea") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2016Usuario_UltimaArea), 6, 0);
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A02_A1073Usuario_CargoCod = new int[1] ;
         P00A02_n1073Usuario_CargoCod = new bool[] {false} ;
         P00A02_A1074Usuario_CargoNom = new String[] {""} ;
         P00A02_n1074Usuario_CargoNom = new bool[] {false} ;
         P00A02_A1075Usuario_CargoUOCod = new int[1] ;
         P00A02_n1075Usuario_CargoUOCod = new bool[] {false} ;
         P00A02_A1076Usuario_CargoUONom = new String[] {""} ;
         P00A02_n1076Usuario_CargoUONom = new bool[] {false} ;
         P00A02_A57Usuario_PessoaCod = new int[1] ;
         P00A02_A58Usuario_PessoaNom = new String[] {""} ;
         P00A02_n58Usuario_PessoaNom = new bool[] {false} ;
         P00A02_A59Usuario_PessoaTip = new String[] {""} ;
         P00A02_n59Usuario_PessoaTip = new bool[] {false} ;
         P00A02_A325Usuario_PessoaDoc = new String[] {""} ;
         P00A02_n325Usuario_PessoaDoc = new bool[] {false} ;
         P00A02_A2095Usuario_PessoaTelefone = new String[] {""} ;
         P00A02_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         P00A02_A341Usuario_UserGamGuid = new String[] {""} ;
         P00A02_A2Usuario_Nome = new String[] {""} ;
         P00A02_n2Usuario_Nome = new bool[] {false} ;
         P00A02_A289Usuario_EhContador = new bool[] {false} ;
         P00A02_A291Usuario_EhContratada = new bool[] {false} ;
         P00A02_A292Usuario_EhContratante = new bool[] {false} ;
         P00A02_A293Usuario_EhFinanceiro = new bool[] {false} ;
         P00A02_A538Usuario_EhGestor = new bool[] {false} ;
         P00A02_A1093Usuario_EhPreposto = new bool[] {false} ;
         P00A02_A1017Usuario_CrtfPath = new String[] {""} ;
         P00A02_n1017Usuario_CrtfPath = new bool[] {false} ;
         P00A02_A1235Usuario_Notificar = new String[] {""} ;
         P00A02_n1235Usuario_Notificar = new bool[] {false} ;
         P00A02_A54Usuario_Ativo = new bool[] {false} ;
         P00A02_A1647Usuario_Email = new String[] {""} ;
         P00A02_n1647Usuario_Email = new bool[] {false} ;
         P00A02_A1908Usuario_DeFerias = new bool[] {false} ;
         P00A02_n1908Usuario_DeFerias = new bool[] {false} ;
         P00A02_A2016Usuario_UltimaArea = new int[1] ;
         P00A02_n2016Usuario_UltimaArea = new bool[] {false} ;
         P00A02_A1Usuario_Codigo = new int[1] ;
         A1074Usuario_CargoNom = "";
         A1076Usuario_CargoUONom = "";
         A58Usuario_PessoaNom = "";
         A59Usuario_PessoaTip = "";
         A325Usuario_PessoaDoc = "";
         A2095Usuario_PessoaTelefone = "";
         A341Usuario_UserGamGuid = "";
         A2Usuario_Nome = "";
         A1017Usuario_CrtfPath = "";
         A1235Usuario_Notificar = "";
         A1647Usuario_Email = "";
         A1083Usuario_Entidade = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A03_A1073Usuario_CargoCod = new int[1] ;
         P00A03_n1073Usuario_CargoCod = new bool[] {false} ;
         P00A03_A1074Usuario_CargoNom = new String[] {""} ;
         P00A03_n1074Usuario_CargoNom = new bool[] {false} ;
         P00A03_A1075Usuario_CargoUOCod = new int[1] ;
         P00A03_n1075Usuario_CargoUOCod = new bool[] {false} ;
         P00A03_A1076Usuario_CargoUONom = new String[] {""} ;
         P00A03_n1076Usuario_CargoUONom = new bool[] {false} ;
         P00A03_A57Usuario_PessoaCod = new int[1] ;
         P00A03_A58Usuario_PessoaNom = new String[] {""} ;
         P00A03_n58Usuario_PessoaNom = new bool[] {false} ;
         P00A03_A59Usuario_PessoaTip = new String[] {""} ;
         P00A03_n59Usuario_PessoaTip = new bool[] {false} ;
         P00A03_A325Usuario_PessoaDoc = new String[] {""} ;
         P00A03_n325Usuario_PessoaDoc = new bool[] {false} ;
         P00A03_A2095Usuario_PessoaTelefone = new String[] {""} ;
         P00A03_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         P00A03_A341Usuario_UserGamGuid = new String[] {""} ;
         P00A03_A2Usuario_Nome = new String[] {""} ;
         P00A03_n2Usuario_Nome = new bool[] {false} ;
         P00A03_A289Usuario_EhContador = new bool[] {false} ;
         P00A03_A291Usuario_EhContratada = new bool[] {false} ;
         P00A03_A292Usuario_EhContratante = new bool[] {false} ;
         P00A03_A293Usuario_EhFinanceiro = new bool[] {false} ;
         P00A03_A538Usuario_EhGestor = new bool[] {false} ;
         P00A03_A1093Usuario_EhPreposto = new bool[] {false} ;
         P00A03_A1017Usuario_CrtfPath = new String[] {""} ;
         P00A03_n1017Usuario_CrtfPath = new bool[] {false} ;
         P00A03_A1235Usuario_Notificar = new String[] {""} ;
         P00A03_n1235Usuario_Notificar = new bool[] {false} ;
         P00A03_A54Usuario_Ativo = new bool[] {false} ;
         P00A03_A1647Usuario_Email = new String[] {""} ;
         P00A03_n1647Usuario_Email = new bool[] {false} ;
         P00A03_A1908Usuario_DeFerias = new bool[] {false} ;
         P00A03_n1908Usuario_DeFerias = new bool[] {false} ;
         P00A03_A2016Usuario_UltimaArea = new int[1] ;
         P00A03_n2016Usuario_UltimaArea = new bool[] {false} ;
         P00A03_A1Usuario_Codigo = new int[1] ;
         GXt_char2 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditusuario__default(),
            new Object[][] {
                new Object[] {
               P00A02_A1073Usuario_CargoCod, P00A02_n1073Usuario_CargoCod, P00A02_A1074Usuario_CargoNom, P00A02_n1074Usuario_CargoNom, P00A02_A1075Usuario_CargoUOCod, P00A02_n1075Usuario_CargoUOCod, P00A02_A1076Usuario_CargoUONom, P00A02_n1076Usuario_CargoUONom, P00A02_A57Usuario_PessoaCod, P00A02_A58Usuario_PessoaNom,
               P00A02_n58Usuario_PessoaNom, P00A02_A59Usuario_PessoaTip, P00A02_n59Usuario_PessoaTip, P00A02_A325Usuario_PessoaDoc, P00A02_n325Usuario_PessoaDoc, P00A02_A2095Usuario_PessoaTelefone, P00A02_n2095Usuario_PessoaTelefone, P00A02_A341Usuario_UserGamGuid, P00A02_A2Usuario_Nome, P00A02_n2Usuario_Nome,
               P00A02_A289Usuario_EhContador, P00A02_A291Usuario_EhContratada, P00A02_A292Usuario_EhContratante, P00A02_A293Usuario_EhFinanceiro, P00A02_A538Usuario_EhGestor, P00A02_A1093Usuario_EhPreposto, P00A02_A1017Usuario_CrtfPath, P00A02_n1017Usuario_CrtfPath, P00A02_A1235Usuario_Notificar, P00A02_n1235Usuario_Notificar,
               P00A02_A54Usuario_Ativo, P00A02_A1647Usuario_Email, P00A02_n1647Usuario_Email, P00A02_A1908Usuario_DeFerias, P00A02_n1908Usuario_DeFerias, P00A02_A2016Usuario_UltimaArea, P00A02_n2016Usuario_UltimaArea, P00A02_A1Usuario_Codigo
               }
               , new Object[] {
               P00A03_A1073Usuario_CargoCod, P00A03_n1073Usuario_CargoCod, P00A03_A1074Usuario_CargoNom, P00A03_n1074Usuario_CargoNom, P00A03_A1075Usuario_CargoUOCod, P00A03_n1075Usuario_CargoUOCod, P00A03_A1076Usuario_CargoUONom, P00A03_n1076Usuario_CargoUONom, P00A03_A57Usuario_PessoaCod, P00A03_A58Usuario_PessoaNom,
               P00A03_n58Usuario_PessoaNom, P00A03_A59Usuario_PessoaTip, P00A03_n59Usuario_PessoaTip, P00A03_A325Usuario_PessoaDoc, P00A03_n325Usuario_PessoaDoc, P00A03_A2095Usuario_PessoaTelefone, P00A03_n2095Usuario_PessoaTelefone, P00A03_A341Usuario_UserGamGuid, P00A03_A2Usuario_Nome, P00A03_n2Usuario_Nome,
               P00A03_A289Usuario_EhContador, P00A03_A291Usuario_EhContratada, P00A03_A292Usuario_EhContratante, P00A03_A293Usuario_EhFinanceiro, P00A03_A538Usuario_EhGestor, P00A03_A1093Usuario_EhPreposto, P00A03_A1017Usuario_CrtfPath, P00A03_n1017Usuario_CrtfPath, P00A03_A1235Usuario_Notificar, P00A03_n1235Usuario_Notificar,
               P00A03_A54Usuario_Ativo, P00A03_A1647Usuario_Email, P00A03_n1647Usuario_Email, P00A03_A1908Usuario_DeFerias, P00A03_n1908Usuario_DeFerias, P00A03_A2016Usuario_UltimaArea, P00A03_n2016Usuario_UltimaArea, P00A03_A1Usuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV16Usuario_Codigo ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A57Usuario_PessoaCod ;
      private int A2016Usuario_UltimaArea ;
      private int A1Usuario_Codigo ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1076Usuario_CargoUONom ;
      private String A58Usuario_PessoaNom ;
      private String A59Usuario_PessoaTip ;
      private String A2095Usuario_PessoaTelefone ;
      private String A341Usuario_UserGamGuid ;
      private String A2Usuario_Nome ;
      private String A1235Usuario_Notificar ;
      private String A1083Usuario_Entidade ;
      private String GXt_char2 ;
      private bool returnInSub ;
      private bool n1073Usuario_CargoCod ;
      private bool n1074Usuario_CargoNom ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1076Usuario_CargoUONom ;
      private bool n58Usuario_PessoaNom ;
      private bool n59Usuario_PessoaTip ;
      private bool n325Usuario_PessoaDoc ;
      private bool n2095Usuario_PessoaTelefone ;
      private bool n2Usuario_Nome ;
      private bool A289Usuario_EhContador ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool A293Usuario_EhFinanceiro ;
      private bool A538Usuario_EhGestor ;
      private bool A1093Usuario_EhPreposto ;
      private bool n1017Usuario_CrtfPath ;
      private bool n1235Usuario_Notificar ;
      private bool A54Usuario_Ativo ;
      private bool n1647Usuario_Email ;
      private bool A1908Usuario_DeFerias ;
      private bool n1908Usuario_DeFerias ;
      private bool n2016Usuario_UltimaArea ;
      private bool A290Usuario_EhAuditorFM ;
      private bool GXt_boolean1 ;
      private String A1074Usuario_CargoNom ;
      private String A325Usuario_PessoaDoc ;
      private String A1017Usuario_CrtfPath ;
      private String A1647Usuario_Email ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00A02_A1073Usuario_CargoCod ;
      private bool[] P00A02_n1073Usuario_CargoCod ;
      private String[] P00A02_A1074Usuario_CargoNom ;
      private bool[] P00A02_n1074Usuario_CargoNom ;
      private int[] P00A02_A1075Usuario_CargoUOCod ;
      private bool[] P00A02_n1075Usuario_CargoUOCod ;
      private String[] P00A02_A1076Usuario_CargoUONom ;
      private bool[] P00A02_n1076Usuario_CargoUONom ;
      private int[] P00A02_A57Usuario_PessoaCod ;
      private String[] P00A02_A58Usuario_PessoaNom ;
      private bool[] P00A02_n58Usuario_PessoaNom ;
      private String[] P00A02_A59Usuario_PessoaTip ;
      private bool[] P00A02_n59Usuario_PessoaTip ;
      private String[] P00A02_A325Usuario_PessoaDoc ;
      private bool[] P00A02_n325Usuario_PessoaDoc ;
      private String[] P00A02_A2095Usuario_PessoaTelefone ;
      private bool[] P00A02_n2095Usuario_PessoaTelefone ;
      private String[] P00A02_A341Usuario_UserGamGuid ;
      private String[] P00A02_A2Usuario_Nome ;
      private bool[] P00A02_n2Usuario_Nome ;
      private bool[] P00A02_A289Usuario_EhContador ;
      private bool[] P00A02_A291Usuario_EhContratada ;
      private bool[] P00A02_A292Usuario_EhContratante ;
      private bool[] P00A02_A293Usuario_EhFinanceiro ;
      private bool[] P00A02_A538Usuario_EhGestor ;
      private bool[] P00A02_A1093Usuario_EhPreposto ;
      private String[] P00A02_A1017Usuario_CrtfPath ;
      private bool[] P00A02_n1017Usuario_CrtfPath ;
      private String[] P00A02_A1235Usuario_Notificar ;
      private bool[] P00A02_n1235Usuario_Notificar ;
      private bool[] P00A02_A54Usuario_Ativo ;
      private String[] P00A02_A1647Usuario_Email ;
      private bool[] P00A02_n1647Usuario_Email ;
      private bool[] P00A02_A1908Usuario_DeFerias ;
      private bool[] P00A02_n1908Usuario_DeFerias ;
      private int[] P00A02_A2016Usuario_UltimaArea ;
      private bool[] P00A02_n2016Usuario_UltimaArea ;
      private int[] P00A02_A1Usuario_Codigo ;
      private int[] P00A03_A1073Usuario_CargoCod ;
      private bool[] P00A03_n1073Usuario_CargoCod ;
      private String[] P00A03_A1074Usuario_CargoNom ;
      private bool[] P00A03_n1074Usuario_CargoNom ;
      private int[] P00A03_A1075Usuario_CargoUOCod ;
      private bool[] P00A03_n1075Usuario_CargoUOCod ;
      private String[] P00A03_A1076Usuario_CargoUONom ;
      private bool[] P00A03_n1076Usuario_CargoUONom ;
      private int[] P00A03_A57Usuario_PessoaCod ;
      private String[] P00A03_A58Usuario_PessoaNom ;
      private bool[] P00A03_n58Usuario_PessoaNom ;
      private String[] P00A03_A59Usuario_PessoaTip ;
      private bool[] P00A03_n59Usuario_PessoaTip ;
      private String[] P00A03_A325Usuario_PessoaDoc ;
      private bool[] P00A03_n325Usuario_PessoaDoc ;
      private String[] P00A03_A2095Usuario_PessoaTelefone ;
      private bool[] P00A03_n2095Usuario_PessoaTelefone ;
      private String[] P00A03_A341Usuario_UserGamGuid ;
      private String[] P00A03_A2Usuario_Nome ;
      private bool[] P00A03_n2Usuario_Nome ;
      private bool[] P00A03_A289Usuario_EhContador ;
      private bool[] P00A03_A291Usuario_EhContratada ;
      private bool[] P00A03_A292Usuario_EhContratante ;
      private bool[] P00A03_A293Usuario_EhFinanceiro ;
      private bool[] P00A03_A538Usuario_EhGestor ;
      private bool[] P00A03_A1093Usuario_EhPreposto ;
      private String[] P00A03_A1017Usuario_CrtfPath ;
      private bool[] P00A03_n1017Usuario_CrtfPath ;
      private String[] P00A03_A1235Usuario_Notificar ;
      private bool[] P00A03_n1235Usuario_Notificar ;
      private bool[] P00A03_A54Usuario_Ativo ;
      private String[] P00A03_A1647Usuario_Email ;
      private bool[] P00A03_n1647Usuario_Email ;
      private bool[] P00A03_A1908Usuario_DeFerias ;
      private bool[] P00A03_n1908Usuario_DeFerias ;
      private int[] P00A03_A2016Usuario_UltimaArea ;
      private bool[] P00A03_n2016Usuario_UltimaArea ;
      private int[] P00A03_A1Usuario_Codigo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A02 ;
          prmP00A02 = new Object[] {
          new Object[] {"@AV16Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A03 ;
          prmP00A03 = new Object[] {
          new Object[] {"@AV16Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A02", "SELECT T1.[Usuario_CargoCod] AS Usuario_CargoCod, T2.[Cargo_Nome] AS Usuario_CargoNom, T2.[Cargo_UOCod] AS Usuario_CargoUOCod, T3.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T4.[Pessoa_Docto] AS Usuario_PessoaDoc, T4.[Pessoa_Telefone] AS Usuario_PessoaTelefone, T1.[Usuario_UserGamGuid], T1.[Usuario_Nome], T1.[Usuario_EhContador], T1.[Usuario_EhContratada], T1.[Usuario_EhContratante], T1.[Usuario_EhFinanceiro], T1.[Usuario_EhGestor], T1.[Usuario_EhPreposto], T1.[Usuario_CrtfPath], T1.[Usuario_Notificar], T1.[Usuario_Ativo], T1.[Usuario_Email], T1.[Usuario_DeFerias], T1.[Usuario_UltimaArea], T1.[Usuario_Codigo] FROM ((([Usuario] T1 WITH (NOLOCK) LEFT JOIN [Geral_Cargo] T2 WITH (NOLOCK) ON T2.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T2.[Cargo_UOCod]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV16Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A02,1,0,true,true )
             ,new CursorDef("P00A03", "SELECT T1.[Usuario_CargoCod] AS Usuario_CargoCod, T2.[Cargo_Nome] AS Usuario_CargoNom, T2.[Cargo_UOCod] AS Usuario_CargoUOCod, T3.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T4.[Pessoa_Docto] AS Usuario_PessoaDoc, T4.[Pessoa_Telefone] AS Usuario_PessoaTelefone, T1.[Usuario_UserGamGuid], T1.[Usuario_Nome], T1.[Usuario_EhContador], T1.[Usuario_EhContratada], T1.[Usuario_EhContratante], T1.[Usuario_EhFinanceiro], T1.[Usuario_EhGestor], T1.[Usuario_EhPreposto], T1.[Usuario_CrtfPath], T1.[Usuario_Notificar], T1.[Usuario_Ativo], T1.[Usuario_Email], T1.[Usuario_DeFerias], T1.[Usuario_UltimaArea], T1.[Usuario_Codigo] FROM ((([Usuario] T1 WITH (NOLOCK) LEFT JOIN [Geral_Cargo] T2 WITH (NOLOCK) ON T2.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T2.[Cargo_UOCod]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV16Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A03,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 40) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.getBool(14) ;
                ((bool[]) buf[23])[0] = rslt.getBool(15) ;
                ((bool[]) buf[24])[0] = rslt.getBool(16) ;
                ((bool[]) buf[25])[0] = rslt.getBool(17) ;
                ((String[]) buf[26])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((String[]) buf[28])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((bool[]) buf[30])[0] = rslt.getBool(20) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((bool[]) buf[33])[0] = rslt.getBool(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 40) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.getBool(14) ;
                ((bool[]) buf[23])[0] = rslt.getBool(15) ;
                ((bool[]) buf[24])[0] = rslt.getBool(16) ;
                ((bool[]) buf[25])[0] = rslt.getBool(17) ;
                ((String[]) buf[26])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((String[]) buf[28])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((bool[]) buf[30])[0] = rslt.getBool(20) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((bool[]) buf[33])[0] = rslt.getBool(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
