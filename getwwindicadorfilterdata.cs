/*
               File: GetWWIndicadorFilterData
        Description: Get WWIndicador Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:14.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwindicadorfilterdata : GXProcedure
   {
      public getwwindicadorfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwindicadorfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwindicadorfilterdata objgetwwindicadorfilterdata;
         objgetwwindicadorfilterdata = new getwwindicadorfilterdata();
         objgetwwindicadorfilterdata.AV14DDOName = aP0_DDOName;
         objgetwwindicadorfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetwwindicadorfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetwwindicadorfilterdata.AV18OptionsJson = "" ;
         objgetwwindicadorfilterdata.AV21OptionsDescJson = "" ;
         objgetwwindicadorfilterdata.AV23OptionIndexesJson = "" ;
         objgetwwindicadorfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwindicadorfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwindicadorfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwindicadorfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV2WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_INDICADOR_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADINDICADOR_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("WWIndicadorGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWIndicadorGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("WWIndicadorGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "INDICADOR_AREATRABALHOCOD") == 0 )
            {
               AV47Indicador_AreaTrabalhoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NOME") == 0 )
            {
               AV10TFIndicador_Nome = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NOME_SEL") == 0 )
            {
               AV11TFIndicador_Nome_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFINDICADOR_REFER_SEL") == 0 )
            {
               AV41TFIndicador_Refer_SelsJson = AV28GridStateFilterValue.gxTpr_Value;
               AV42TFIndicador_Refer_Sels.FromJSonString(AV41TFIndicador_Refer_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFINDICADOR_APLICA_SEL") == 0 )
            {
               AV43TFIndicador_Aplica_SelsJson = AV28GridStateFilterValue.gxTpr_Value;
               AV44TFIndicador_Aplica_Sels.FromJSonString(AV43TFIndicador_Aplica_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NAOCNFCOD") == 0 )
            {
               AV45TFIndicador_NaoCnfCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
               AV46TFIndicador_NaoCnfCod_To = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "INDICADOR_NOME") == 0 )
            {
               AV31DynamicFiltersOperator1 = AV29GridStateDynamicFilter.gxTpr_Operator;
               AV32Indicador_Nome1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "INDICADOR_NOME") == 0 )
               {
                  AV35DynamicFiltersOperator2 = AV29GridStateDynamicFilter.gxTpr_Operator;
                  AV36Indicador_Nome2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV37DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV38DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "INDICADOR_NOME") == 0 )
                  {
                     AV39DynamicFiltersOperator3 = AV29GridStateDynamicFilter.gxTpr_Operator;
                     AV40Indicador_Nome3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADINDICADOR_NOMEOPTIONS' Routine */
         AV10TFIndicador_Nome = AV12SearchTxt;
         AV11TFIndicador_Nome_Sel = "";
         AV52WWIndicadorDS_1_Indicador_areatrabalhocod = AV47Indicador_AreaTrabalhoCod;
         AV53WWIndicadorDS_2_Dynamicfiltersselector1 = AV30DynamicFiltersSelector1;
         AV54WWIndicadorDS_3_Dynamicfiltersoperator1 = AV31DynamicFiltersOperator1;
         AV55WWIndicadorDS_4_Indicador_nome1 = AV32Indicador_Nome1;
         AV56WWIndicadorDS_5_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV57WWIndicadorDS_6_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV58WWIndicadorDS_7_Dynamicfiltersoperator2 = AV35DynamicFiltersOperator2;
         AV59WWIndicadorDS_8_Indicador_nome2 = AV36Indicador_Nome2;
         AV60WWIndicadorDS_9_Dynamicfiltersenabled3 = AV37DynamicFiltersEnabled3;
         AV61WWIndicadorDS_10_Dynamicfiltersselector3 = AV38DynamicFiltersSelector3;
         AV62WWIndicadorDS_11_Dynamicfiltersoperator3 = AV39DynamicFiltersOperator3;
         AV63WWIndicadorDS_12_Indicador_nome3 = AV40Indicador_Nome3;
         AV64WWIndicadorDS_13_Tfindicador_nome = AV10TFIndicador_Nome;
         AV65WWIndicadorDS_14_Tfindicador_nome_sel = AV11TFIndicador_Nome_Sel;
         AV66WWIndicadorDS_15_Tfindicador_refer_sels = AV42TFIndicador_Refer_Sels;
         AV67WWIndicadorDS_16_Tfindicador_aplica_sels = AV44TFIndicador_Aplica_Sels;
         AV68WWIndicadorDS_17_Tfindicador_naocnfcod = AV45TFIndicador_NaoCnfCod;
         AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV46TFIndicador_NaoCnfCod_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A2063Indicador_Refer ,
                                              AV66WWIndicadorDS_15_Tfindicador_refer_sels ,
                                              A2064Indicador_Aplica ,
                                              AV67WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                              AV53WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                              AV54WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                              AV55WWIndicadorDS_4_Indicador_nome1 ,
                                              AV56WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                              AV57WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                              AV58WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                              AV59WWIndicadorDS_8_Indicador_nome2 ,
                                              AV60WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                              AV61WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                              AV62WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                              AV63WWIndicadorDS_12_Indicador_nome3 ,
                                              AV65WWIndicadorDS_14_Tfindicador_nome_sel ,
                                              AV64WWIndicadorDS_13_Tfindicador_nome ,
                                              AV66WWIndicadorDS_15_Tfindicador_refer_sels.Count ,
                                              AV67WWIndicadorDS_16_Tfindicador_aplica_sels.Count ,
                                              AV68WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                              AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                              A2059Indicador_Nome ,
                                              A2062Indicador_NaoCnfCod ,
                                              A2061Indicador_AreaTrabalhoCod ,
                                              AV2WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV55WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWIndicadorDS_4_Indicador_nome1), 50, "%");
         lV55WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWIndicadorDS_4_Indicador_nome1), 50, "%");
         lV59WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV59WWIndicadorDS_8_Indicador_nome2), 50, "%");
         lV59WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV59WWIndicadorDS_8_Indicador_nome2), 50, "%");
         lV63WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_12_Indicador_nome3), 50, "%");
         lV63WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_12_Indicador_nome3), 50, "%");
         lV64WWIndicadorDS_13_Tfindicador_nome = StringUtil.PadR( StringUtil.RTrim( AV64WWIndicadorDS_13_Tfindicador_nome), 50, "%");
         /* Using cursor P00WW2 */
         pr_default.execute(0, new Object[] {AV2WWPContext.gxTpr_Areatrabalho_codigo, lV55WWIndicadorDS_4_Indicador_nome1, lV55WWIndicadorDS_4_Indicador_nome1, lV59WWIndicadorDS_8_Indicador_nome2, lV59WWIndicadorDS_8_Indicador_nome2, lV63WWIndicadorDS_12_Indicador_nome3, lV63WWIndicadorDS_12_Indicador_nome3, lV64WWIndicadorDS_13_Tfindicador_nome, AV65WWIndicadorDS_14_Tfindicador_nome_sel, AV68WWIndicadorDS_17_Tfindicador_naocnfcod, AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKWW2 = false;
            A2061Indicador_AreaTrabalhoCod = P00WW2_A2061Indicador_AreaTrabalhoCod[0];
            n2061Indicador_AreaTrabalhoCod = P00WW2_n2061Indicador_AreaTrabalhoCod[0];
            A2059Indicador_Nome = P00WW2_A2059Indicador_Nome[0];
            A2062Indicador_NaoCnfCod = P00WW2_A2062Indicador_NaoCnfCod[0];
            n2062Indicador_NaoCnfCod = P00WW2_n2062Indicador_NaoCnfCod[0];
            A2064Indicador_Aplica = P00WW2_A2064Indicador_Aplica[0];
            n2064Indicador_Aplica = P00WW2_n2064Indicador_Aplica[0];
            A2063Indicador_Refer = P00WW2_A2063Indicador_Refer[0];
            n2063Indicador_Refer = P00WW2_n2063Indicador_Refer[0];
            A2058Indicador_Codigo = P00WW2_A2058Indicador_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00WW2_A2059Indicador_Nome[0], A2059Indicador_Nome) == 0 ) )
            {
               BRKWW2 = false;
               A2058Indicador_Codigo = P00WW2_A2058Indicador_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKWW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2059Indicador_Nome)) )
            {
               AV16Option = A2059Indicador_Nome;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKWW2 )
            {
               BRKWW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV2WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV47Indicador_AreaTrabalhoCod = AV2WWPContext.gxTpr_Areatrabalho_codigo;
         AV10TFIndicador_Nome = "";
         AV11TFIndicador_Nome_Sel = "";
         AV41TFIndicador_Refer_SelsJson = "";
         AV42TFIndicador_Refer_Sels = new GxSimpleCollection();
         AV43TFIndicador_Aplica_SelsJson = "";
         AV44TFIndicador_Aplica_Sels = new GxSimpleCollection();
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV32Indicador_Nome1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV36Indicador_Nome2 = "";
         AV38DynamicFiltersSelector3 = "";
         AV40Indicador_Nome3 = "";
         AV53WWIndicadorDS_2_Dynamicfiltersselector1 = "";
         AV55WWIndicadorDS_4_Indicador_nome1 = "";
         AV57WWIndicadorDS_6_Dynamicfiltersselector2 = "";
         AV59WWIndicadorDS_8_Indicador_nome2 = "";
         AV61WWIndicadorDS_10_Dynamicfiltersselector3 = "";
         AV63WWIndicadorDS_12_Indicador_nome3 = "";
         AV64WWIndicadorDS_13_Tfindicador_nome = "";
         AV65WWIndicadorDS_14_Tfindicador_nome_sel = "";
         AV66WWIndicadorDS_15_Tfindicador_refer_sels = new GxSimpleCollection();
         AV67WWIndicadorDS_16_Tfindicador_aplica_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV55WWIndicadorDS_4_Indicador_nome1 = "";
         lV59WWIndicadorDS_8_Indicador_nome2 = "";
         lV63WWIndicadorDS_12_Indicador_nome3 = "";
         lV64WWIndicadorDS_13_Tfindicador_nome = "";
         A2059Indicador_Nome = "";
         P00WW2_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         P00WW2_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         P00WW2_A2059Indicador_Nome = new String[] {""} ;
         P00WW2_A2062Indicador_NaoCnfCod = new int[1] ;
         P00WW2_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         P00WW2_A2064Indicador_Aplica = new short[1] ;
         P00WW2_n2064Indicador_Aplica = new bool[] {false} ;
         P00WW2_A2063Indicador_Refer = new short[1] ;
         P00WW2_n2063Indicador_Refer = new bool[] {false} ;
         P00WW2_A2058Indicador_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwindicadorfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00WW2_A2061Indicador_AreaTrabalhoCod, P00WW2_n2061Indicador_AreaTrabalhoCod, P00WW2_A2059Indicador_Nome, P00WW2_A2062Indicador_NaoCnfCod, P00WW2_n2062Indicador_NaoCnfCod, P00WW2_A2064Indicador_Aplica, P00WW2_n2064Indicador_Aplica, P00WW2_A2063Indicador_Refer, P00WW2_n2063Indicador_Refer, P00WW2_A2058Indicador_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV31DynamicFiltersOperator1 ;
      private short AV35DynamicFiltersOperator2 ;
      private short AV39DynamicFiltersOperator3 ;
      private short AV54WWIndicadorDS_3_Dynamicfiltersoperator1 ;
      private short AV58WWIndicadorDS_7_Dynamicfiltersoperator2 ;
      private short AV62WWIndicadorDS_11_Dynamicfiltersoperator3 ;
      private short A2063Indicador_Refer ;
      private short A2064Indicador_Aplica ;
      private int AV50GXV1 ;
      private int AV47Indicador_AreaTrabalhoCod ;
      private int AV45TFIndicador_NaoCnfCod ;
      private int AV46TFIndicador_NaoCnfCod_To ;
      private int AV52WWIndicadorDS_1_Indicador_areatrabalhocod ;
      private int AV68WWIndicadorDS_17_Tfindicador_naocnfcod ;
      private int AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to ;
      private int AV66WWIndicadorDS_15_Tfindicador_refer_sels_Count ;
      private int AV67WWIndicadorDS_16_Tfindicador_aplica_sels_Count ;
      private int AV2WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A2062Indicador_NaoCnfCod ;
      private int A2061Indicador_AreaTrabalhoCod ;
      private int A2058Indicador_Codigo ;
      private long AV24count ;
      private String AV10TFIndicador_Nome ;
      private String AV11TFIndicador_Nome_Sel ;
      private String AV32Indicador_Nome1 ;
      private String AV36Indicador_Nome2 ;
      private String AV40Indicador_Nome3 ;
      private String AV55WWIndicadorDS_4_Indicador_nome1 ;
      private String AV59WWIndicadorDS_8_Indicador_nome2 ;
      private String AV63WWIndicadorDS_12_Indicador_nome3 ;
      private String AV64WWIndicadorDS_13_Tfindicador_nome ;
      private String AV65WWIndicadorDS_14_Tfindicador_nome_sel ;
      private String scmdbuf ;
      private String lV55WWIndicadorDS_4_Indicador_nome1 ;
      private String lV59WWIndicadorDS_8_Indicador_nome2 ;
      private String lV63WWIndicadorDS_12_Indicador_nome3 ;
      private String lV64WWIndicadorDS_13_Tfindicador_nome ;
      private String A2059Indicador_Nome ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV37DynamicFiltersEnabled3 ;
      private bool AV56WWIndicadorDS_5_Dynamicfiltersenabled2 ;
      private bool AV60WWIndicadorDS_9_Dynamicfiltersenabled3 ;
      private bool BRKWW2 ;
      private bool n2061Indicador_AreaTrabalhoCod ;
      private bool n2062Indicador_NaoCnfCod ;
      private bool n2064Indicador_Aplica ;
      private bool n2063Indicador_Refer ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV41TFIndicador_Refer_SelsJson ;
      private String AV43TFIndicador_Aplica_SelsJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV38DynamicFiltersSelector3 ;
      private String AV53WWIndicadorDS_2_Dynamicfiltersselector1 ;
      private String AV57WWIndicadorDS_6_Dynamicfiltersselector2 ;
      private String AV61WWIndicadorDS_10_Dynamicfiltersselector3 ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00WW2_A2061Indicador_AreaTrabalhoCod ;
      private bool[] P00WW2_n2061Indicador_AreaTrabalhoCod ;
      private String[] P00WW2_A2059Indicador_Nome ;
      private int[] P00WW2_A2062Indicador_NaoCnfCod ;
      private bool[] P00WW2_n2062Indicador_NaoCnfCod ;
      private short[] P00WW2_A2064Indicador_Aplica ;
      private bool[] P00WW2_n2064Indicador_Aplica ;
      private short[] P00WW2_A2063Indicador_Refer ;
      private bool[] P00WW2_n2063Indicador_Refer ;
      private int[] P00WW2_A2058Indicador_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV42TFIndicador_Refer_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV44TFIndicador_Aplica_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV66WWIndicadorDS_15_Tfindicador_refer_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV67WWIndicadorDS_16_Tfindicador_aplica_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV2WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getwwindicadorfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00WW2( IGxContext context ,
                                             short A2063Indicador_Refer ,
                                             IGxCollection AV66WWIndicadorDS_15_Tfindicador_refer_sels ,
                                             short A2064Indicador_Aplica ,
                                             IGxCollection AV67WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                             String AV53WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                             short AV54WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                             String AV55WWIndicadorDS_4_Indicador_nome1 ,
                                             bool AV56WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                             String AV57WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                             short AV58WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                             String AV59WWIndicadorDS_8_Indicador_nome2 ,
                                             bool AV60WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                             String AV61WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                             short AV62WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                             String AV63WWIndicadorDS_12_Indicador_nome3 ,
                                             String AV65WWIndicadorDS_14_Tfindicador_nome_sel ,
                                             String AV64WWIndicadorDS_13_Tfindicador_nome ,
                                             int AV66WWIndicadorDS_15_Tfindicador_refer_sels_Count ,
                                             int AV67WWIndicadorDS_16_Tfindicador_aplica_sels_Count ,
                                             int AV68WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                             int AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                             String A2059Indicador_Nome ,
                                             int A2062Indicador_NaoCnfCod ,
                                             int A2061Indicador_AreaTrabalhoCod ,
                                             int AV2WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Indicador_AreaTrabalhoCod], [Indicador_Nome], [Indicador_NaoCnfCod], [Indicador_Aplica], [Indicador_Refer], [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Indicador_AreaTrabalhoCod] = @AV2WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV53WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV54WWIndicadorDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV55WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV54WWIndicadorDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV55WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV56WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV58WWIndicadorDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV59WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV56WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV58WWIndicadorDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV59WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV60WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV61WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV63WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV60WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV61WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV63WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWIndicadorDS_14_Tfindicador_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWIndicadorDS_13_Tfindicador_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV64WWIndicadorDS_13_Tfindicador_nome)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWIndicadorDS_14_Tfindicador_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] = @AV65WWIndicadorDS_14_Tfindicador_nome_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV66WWIndicadorDS_15_Tfindicador_refer_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV66WWIndicadorDS_15_Tfindicador_refer_sels, "[Indicador_Refer] IN (", ")") + ")";
         }
         if ( AV67WWIndicadorDS_16_Tfindicador_aplica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV67WWIndicadorDS_16_Tfindicador_aplica_sels, "[Indicador_Aplica] IN (", ")") + ")";
         }
         if ( ! (0==AV68WWIndicadorDS_17_Tfindicador_naocnfcod) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] >= @AV68WWIndicadorDS_17_Tfindicador_naocnfcod)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] <= @AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Indicador_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00WW2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WW2 ;
          prmP00WW2 = new Object[] {
          new Object[] {"@AV2WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV55WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWIndicadorDS_13_Tfindicador_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65WWIndicadorDS_14_Tfindicador_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWIndicadorDS_17_Tfindicador_naocnfcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWIndicadorDS_18_Tfindicador_naocnfcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WW2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwindicadorfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwindicadorfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwindicadorfilterdata") )
          {
             return  ;
          }
          getwwindicadorfilterdata worker = new getwwindicadorfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
