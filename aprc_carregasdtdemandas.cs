/*
               File: PRC_CarregaSDTDemandas
        Description: Carrega SDTDemandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:8.12
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_carregasdtdemandas : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV42PraLinha = (short)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV15Arquivo = GetNextPar( );
                  AV65Aba = GetNextPar( );
                  AV8ColOSFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV9ColNomeSisteman = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV99ColModulon = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV10ColOSFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11ColDataDmnn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV12ColDescricaon = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14ColLinkn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV20Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV82ContratadaSrv_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV19EhValidacao = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV21OSFM = GetNextPar( );
                  AV28DataOSFM = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV18Observacao = GetNextPar( );
                  AV17ContadorFSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV34ContadorFSNom = GetNextPar( );
                  AV16Link = GetNextPar( );
                  AV48ColPFBFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV49ColPFLFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV51ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV50ColContadorFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV109ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV75ServicoVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV66ColFiltron = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV67TipoFiltro = GetNextPar( );
                  AV74Filtro = GetNextPar( );
                  AV77ColPFBFSDmnn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV78ColPFLFSDmnn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV80ColContadorFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV90DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV91ColDataCntn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV96FileName = GetNextPar( );
                  AV104ColProjeton = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV102ColTipon = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV106ColObservacaon = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV114PrazoEntrega = context.localUtil.ParseDTimeParm( GetNextPar( ));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_carregasdtdemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_carregasdtdemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_PraLinha ,
                           String aP1_Arquivo ,
                           String aP2_Aba ,
                           short aP3_ColOSFSn ,
                           short aP4_ColNomeSisteman ,
                           short aP5_ColModulon ,
                           short aP6_ColOSFMn ,
                           short aP7_ColDataDmnn ,
                           short aP8_ColDescricaon ,
                           short aP9_ColLinkn ,
                           int aP10_Contratada_Codigo ,
                           int aP11_ContratadaSrv_Codigo ,
                           bool aP12_EhValidacao ,
                           String aP13_OSFM ,
                           ref DateTime aP14_DataOSFM ,
                           String aP15_Observacao ,
                           int aP16_ContadorFSCod ,
                           String aP17_ContadorFSNom ,
                           String aP18_Link ,
                           short aP19_ColPFBFMn ,
                           short aP20_ColPFLFMn ,
                           int aP21_ContadorFMCod ,
                           short aP22_ColContadorFMn ,
                           int aP23_ContratoServicos_Codigo ,
                           int aP24_ServicoVnc_Codigo ,
                           short aP25_ColFiltron ,
                           String aP26_TipoFiltro ,
                           String aP27_Filtro ,
                           short aP28_ColPFBFSDmnn ,
                           short aP29_ColPFLFSDmnn ,
                           short aP30_ColContadorFSn ,
                           DateTime aP31_DataCnt ,
                           short aP32_ColDataCntn ,
                           String aP33_FileName ,
                           short aP34_ColProjeton ,
                           short aP35_ColTipon ,
                           short aP36_ColObservacaon ,
                           DateTime aP37_PrazoEntrega )
      {
         this.AV42PraLinha = aP0_PraLinha;
         this.AV15Arquivo = aP1_Arquivo;
         this.AV65Aba = aP2_Aba;
         this.AV8ColOSFSn = aP3_ColOSFSn;
         this.AV9ColNomeSisteman = aP4_ColNomeSisteman;
         this.AV99ColModulon = aP5_ColModulon;
         this.AV10ColOSFMn = aP6_ColOSFMn;
         this.AV11ColDataDmnn = aP7_ColDataDmnn;
         this.AV12ColDescricaon = aP8_ColDescricaon;
         this.AV14ColLinkn = aP9_ColLinkn;
         this.AV20Contratada_Codigo = aP10_Contratada_Codigo;
         this.AV82ContratadaSrv_Codigo = aP11_ContratadaSrv_Codigo;
         this.AV19EhValidacao = aP12_EhValidacao;
         this.AV21OSFM = aP13_OSFM;
         this.AV28DataOSFM = aP14_DataOSFM;
         this.AV18Observacao = aP15_Observacao;
         this.AV17ContadorFSCod = aP16_ContadorFSCod;
         this.AV34ContadorFSNom = aP17_ContadorFSNom;
         this.AV16Link = aP18_Link;
         this.AV48ColPFBFMn = aP19_ColPFBFMn;
         this.AV49ColPFLFMn = aP20_ColPFLFMn;
         this.AV51ContadorFMCod = aP21_ContadorFMCod;
         this.AV50ColContadorFMn = aP22_ColContadorFMn;
         this.AV109ContratoServicos_Codigo = aP23_ContratoServicos_Codigo;
         this.AV75ServicoVnc_Codigo = aP24_ServicoVnc_Codigo;
         this.AV66ColFiltron = aP25_ColFiltron;
         this.AV67TipoFiltro = aP26_TipoFiltro;
         this.AV74Filtro = aP27_Filtro;
         this.AV77ColPFBFSDmnn = aP28_ColPFBFSDmnn;
         this.AV78ColPFLFSDmnn = aP29_ColPFLFSDmnn;
         this.AV80ColContadorFSn = aP30_ColContadorFSn;
         this.AV90DataCnt = aP31_DataCnt;
         this.AV91ColDataCntn = aP32_ColDataCntn;
         this.AV96FileName = aP33_FileName;
         this.AV104ColProjeton = aP34_ColProjeton;
         this.AV102ColTipon = aP35_ColTipon;
         this.AV106ColObservacaon = aP36_ColObservacaon;
         this.AV114PrazoEntrega = aP37_PrazoEntrega;
         initialize();
         executePrivate();
         aP14_DataOSFM=this.AV28DataOSFM;
      }

      public void executeSubmit( short aP0_PraLinha ,
                                 String aP1_Arquivo ,
                                 String aP2_Aba ,
                                 short aP3_ColOSFSn ,
                                 short aP4_ColNomeSisteman ,
                                 short aP5_ColModulon ,
                                 short aP6_ColOSFMn ,
                                 short aP7_ColDataDmnn ,
                                 short aP8_ColDescricaon ,
                                 short aP9_ColLinkn ,
                                 int aP10_Contratada_Codigo ,
                                 int aP11_ContratadaSrv_Codigo ,
                                 bool aP12_EhValidacao ,
                                 String aP13_OSFM ,
                                 ref DateTime aP14_DataOSFM ,
                                 String aP15_Observacao ,
                                 int aP16_ContadorFSCod ,
                                 String aP17_ContadorFSNom ,
                                 String aP18_Link ,
                                 short aP19_ColPFBFMn ,
                                 short aP20_ColPFLFMn ,
                                 int aP21_ContadorFMCod ,
                                 short aP22_ColContadorFMn ,
                                 int aP23_ContratoServicos_Codigo ,
                                 int aP24_ServicoVnc_Codigo ,
                                 short aP25_ColFiltron ,
                                 String aP26_TipoFiltro ,
                                 String aP27_Filtro ,
                                 short aP28_ColPFBFSDmnn ,
                                 short aP29_ColPFLFSDmnn ,
                                 short aP30_ColContadorFSn ,
                                 DateTime aP31_DataCnt ,
                                 short aP32_ColDataCntn ,
                                 String aP33_FileName ,
                                 short aP34_ColProjeton ,
                                 short aP35_ColTipon ,
                                 short aP36_ColObservacaon ,
                                 DateTime aP37_PrazoEntrega )
      {
         aprc_carregasdtdemandas objaprc_carregasdtdemandas;
         objaprc_carregasdtdemandas = new aprc_carregasdtdemandas();
         objaprc_carregasdtdemandas.AV42PraLinha = aP0_PraLinha;
         objaprc_carregasdtdemandas.AV15Arquivo = aP1_Arquivo;
         objaprc_carregasdtdemandas.AV65Aba = aP2_Aba;
         objaprc_carregasdtdemandas.AV8ColOSFSn = aP3_ColOSFSn;
         objaprc_carregasdtdemandas.AV9ColNomeSisteman = aP4_ColNomeSisteman;
         objaprc_carregasdtdemandas.AV99ColModulon = aP5_ColModulon;
         objaprc_carregasdtdemandas.AV10ColOSFMn = aP6_ColOSFMn;
         objaprc_carregasdtdemandas.AV11ColDataDmnn = aP7_ColDataDmnn;
         objaprc_carregasdtdemandas.AV12ColDescricaon = aP8_ColDescricaon;
         objaprc_carregasdtdemandas.AV14ColLinkn = aP9_ColLinkn;
         objaprc_carregasdtdemandas.AV20Contratada_Codigo = aP10_Contratada_Codigo;
         objaprc_carregasdtdemandas.AV82ContratadaSrv_Codigo = aP11_ContratadaSrv_Codigo;
         objaprc_carregasdtdemandas.AV19EhValidacao = aP12_EhValidacao;
         objaprc_carregasdtdemandas.AV21OSFM = aP13_OSFM;
         objaprc_carregasdtdemandas.AV28DataOSFM = aP14_DataOSFM;
         objaprc_carregasdtdemandas.AV18Observacao = aP15_Observacao;
         objaprc_carregasdtdemandas.AV17ContadorFSCod = aP16_ContadorFSCod;
         objaprc_carregasdtdemandas.AV34ContadorFSNom = aP17_ContadorFSNom;
         objaprc_carregasdtdemandas.AV16Link = aP18_Link;
         objaprc_carregasdtdemandas.AV48ColPFBFMn = aP19_ColPFBFMn;
         objaprc_carregasdtdemandas.AV49ColPFLFMn = aP20_ColPFLFMn;
         objaprc_carregasdtdemandas.AV51ContadorFMCod = aP21_ContadorFMCod;
         objaprc_carregasdtdemandas.AV50ColContadorFMn = aP22_ColContadorFMn;
         objaprc_carregasdtdemandas.AV109ContratoServicos_Codigo = aP23_ContratoServicos_Codigo;
         objaprc_carregasdtdemandas.AV75ServicoVnc_Codigo = aP24_ServicoVnc_Codigo;
         objaprc_carregasdtdemandas.AV66ColFiltron = aP25_ColFiltron;
         objaprc_carregasdtdemandas.AV67TipoFiltro = aP26_TipoFiltro;
         objaprc_carregasdtdemandas.AV74Filtro = aP27_Filtro;
         objaprc_carregasdtdemandas.AV77ColPFBFSDmnn = aP28_ColPFBFSDmnn;
         objaprc_carregasdtdemandas.AV78ColPFLFSDmnn = aP29_ColPFLFSDmnn;
         objaprc_carregasdtdemandas.AV80ColContadorFSn = aP30_ColContadorFSn;
         objaprc_carregasdtdemandas.AV90DataCnt = aP31_DataCnt;
         objaprc_carregasdtdemandas.AV91ColDataCntn = aP32_ColDataCntn;
         objaprc_carregasdtdemandas.AV96FileName = aP33_FileName;
         objaprc_carregasdtdemandas.AV104ColProjeton = aP34_ColProjeton;
         objaprc_carregasdtdemandas.AV102ColTipon = aP35_ColTipon;
         objaprc_carregasdtdemandas.AV106ColObservacaon = aP36_ColObservacaon;
         objaprc_carregasdtdemandas.AV114PrazoEntrega = aP37_PrazoEntrega;
         objaprc_carregasdtdemandas.context.SetSubmitInitialConfig(context);
         objaprc_carregasdtdemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_carregasdtdemandas);
         aP14_DataOSFM=this.AV28DataOSFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_carregasdtdemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV32WWPContext) ;
            /* Execute user subroutine: 'OPENFILE' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV64NomeArq = "Arquivo: " + AV96FileName;
            AV64NomeArq = AV64NomeArq + (String.IsNullOrEmpty(StringUtil.RTrim( AV65Aba)) ? "" : " ("+AV65Aba+")");
            AV81ColPFLFS = AV78ColPFLFSDmnn;
            if ( AV78ColPFLFSDmnn < 0 )
            {
               AV81ColPFLFS = AV77ColPFBFSDmnn;
            }
            AV61Servico_Percentual = (decimal)(1);
            /* Using cursor P004D2 */
            pr_default.execute(0, new Object[] {AV109ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = P004D2_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P004D2_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P004D2_A52Contratada_AreaTrabalhoCod[0];
               A29Contratante_Codigo = P004D2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P004D2_n29Contratante_Codigo[0];
               A160ContratoServicos_Codigo = P004D2_A160ContratoServicos_Codigo[0];
               A558Servico_Percentual = P004D2_A558Servico_Percentual[0];
               n558Servico_Percentual = P004D2_n558Servico_Percentual[0];
               A1454ContratoServicos_PrazoTpDias = P004D2_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P004D2_n1454ContratoServicos_PrazoTpDias[0];
               A1649ContratoServicos_PrazoInicio = P004D2_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P004D2_n1649ContratoServicos_PrazoInicio[0];
               A593Contratante_OSAutomatica = P004D2_A593Contratante_OSAutomatica[0];
               A39Contratada_Codigo = P004D2_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P004D2_A52Contratada_AreaTrabalhoCod[0];
               A29Contratante_Codigo = P004D2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P004D2_n29Contratante_Codigo[0];
               A593Contratante_OSAutomatica = P004D2_A593Contratante_OSAutomatica[0];
               OV116PrazoInicio = AV116PrazoInicio;
               AV61Servico_Percentual = A558Servico_Percentual;
               AV115TipoDias = A1454ContratoServicos_PrazoTpDias;
               AV116PrazoInicio = A1649ContratoServicos_PrazoInicio;
               AV117OSAutomatica = A593Contratante_OSAutomatica;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74Filtro)) )
            {
               AV70FiltroNum = NumberUtil.Val( AV68FiltroChar, ".");
               AV69FiltroDate = context.localUtil.CToD( AV68FiltroChar, 2);
               AV68FiltroChar = StringUtil.Trim( StringUtil.Upper( AV74Filtro));
               AV71TemFiltro = true;
            }
            if ( (0==AV51ContadorFMCod) )
            {
               new prc_cstuntprdnrm(context ).execute( ref  AV109ContratoServicos_Codigo, ref  AV53ContadorCodigo, ref  AV100CstUntNrm, out  AV101CalculoPFinal) ;
            }
            AV24Ln = AV42PraLinha;
            AV63TxtLinha = "Fila ";
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23ExcelDocument.get_Cells(AV24Ln, AV8ColOSFSn, 1, 1).Text)) )
            {
               if ( ( AV24Ln /  ( decimal )( 75 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV24Ln/ (decimal)(75))) )) )
               {
                  AV22ErrCod = AV23ExcelDocument.Close();
                  /* Execute user subroutine: 'OPENFILE' */
                  S121 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               AV72Ok = true;
               if ( AV71TemFiltro )
               {
                  if ( StringUtil.StrCmp(AV67TipoFiltro, "C") == 0 )
                  {
                     AV94String = StringUtil.Upper( AV23ExcelDocument.get_Cells(AV24Ln, AV66ColFiltron, 1, 1).Text);
                     AV72Ok = (bool)(((StringUtil.StrCmp(AV94String, AV68FiltroChar)==0))||((StringUtil.StringSearch( AV94String, AV68FiltroChar, 1)==1)));
                  }
                  else if ( StringUtil.StrCmp(AV67TipoFiltro, "N") == 0 )
                  {
                     AV72Ok = (bool)(((Convert.ToDecimal(AV23ExcelDocument.get_Cells(AV24Ln, AV66ColFiltron, 1, 1).Number)==AV70FiltroNum)));
                  }
                  else if ( StringUtil.StrCmp(AV67TipoFiltro, "D") == 0 )
                  {
                     AV72Ok = (bool)(((AV23ExcelDocument.get_Cells(AV24Ln, AV66ColFiltron, 1, 1).Date==AV69FiltroDate)));
                  }
               }
               if ( AV72Ok )
               {
                  AV33Linha = AV63TxtLinha + StringUtil.Trim( StringUtil.Str( (decimal)(AV24Ln), 4, 0)) + " - ";
                  AV63TxtLinha = "     ";
                  AV22ErrCod = 0;
                  if ( AV48ColPFBFMn > 0 )
                  {
                     if ( (0==AV51ContadorFMCod) )
                     {
                        AV52ContadorNome = StringUtil.Upper( StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV50ColContadorFMn, 1, 1).Text));
                        AV57FMFlag = true;
                        /* Execute user subroutine: 'PROCURACONTADOR' */
                        S151 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                     }
                     else
                     {
                        AV53ContadorCodigo = AV51ContadorFMCod;
                     }
                     if ( AV22ErrCod == 0 )
                     {
                        AV26SDT_Demanda.gxTpr_Pfbfm = (decimal)(AV23ExcelDocument.get_Cells(AV24Ln, AV48ColPFBFMn, 1, 1).Number);
                        AV26SDT_Demanda.gxTpr_Pflfm = (decimal)(AV23ExcelDocument.get_Cells(AV24Ln, AV49ColPFLFMn, 1, 1).Number);
                        AV26SDT_Demanda.gxTpr_Deflator = AV61Servico_Percentual;
                        AV26SDT_Demanda.gxTpr_Contadorfmcod = AV53ContadorCodigo;
                        if ( AV51ContadorFMCod > 0 )
                        {
                           new prc_cstuntprdnrm(context ).execute( ref  AV109ContratoServicos_Codigo, ref  AV53ContadorCodigo, ref  AV100CstUntNrm, out  AV101CalculoPFinal) ;
                        }
                        AV26SDT_Demanda.gxTpr_Custocrfm = AV100CstUntNrm;
                     }
                  }
                  if ( AV22ErrCod == 0 )
                  {
                     if ( AV9ColNomeSisteman > 0 )
                     {
                        AV30Sistema_Sigla = StringUtil.Upper( StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV9ColNomeSisteman, 1, 1).Text));
                        AV124GXLvl93 = 0;
                        /* Using cursor P004D3 */
                        pr_default.execute(1, new Object[] {AV30Sistema_Sigla, AV32WWPContext.gxTpr_Areatrabalho_codigo});
                        while ( (pr_default.getStatus(1) != 101) )
                        {
                           A130Sistema_Ativo = P004D3_A130Sistema_Ativo[0];
                           A135Sistema_AreaTrabalhoCod = P004D3_A135Sistema_AreaTrabalhoCod[0];
                           A129Sistema_Sigla = P004D3_A129Sistema_Sigla[0];
                           A127Sistema_Codigo = P004D3_A127Sistema_Codigo[0];
                           AV124GXLvl93 = 1;
                           AV26SDT_Demanda.gxTpr_Sistemacod = A127Sistema_Codigo;
                           AV26SDT_Demanda.gxTpr_Sistemanom = A129Sistema_Sigla;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(1);
                        }
                        pr_default.close(1);
                        if ( AV124GXLvl93 == 0 )
                        {
                           AV22ErrCod = 1;
                           AV33Linha = AV33Linha + "Sistema " + AV30Sistema_Sigla + " desconhecido. ";
                        }
                     }
                     else
                     {
                        AV105OrigemDsc = StringUtil.Upper( StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV9ColNomeSisteman, 1, 1).Text));
                        AV125GXLvl107 = 0;
                        /* Using cursor P004D4 */
                        pr_default.execute(2, new Object[] {AV105OrigemDsc});
                        while ( (pr_default.getStatus(2) != 101) )
                        {
                           A1461SistemaDePara_Origem = P004D4_A1461SistemaDePara_Origem[0];
                           A1463SistemaDePara_OrigemDsc = P004D4_A1463SistemaDePara_OrigemDsc[0];
                           n1463SistemaDePara_OrigemDsc = P004D4_n1463SistemaDePara_OrigemDsc[0];
                           A127Sistema_Codigo = P004D4_A127Sistema_Codigo[0];
                           A1460SistemaDePara_Codigo = P004D4_A1460SistemaDePara_Codigo[0];
                           AV125GXLvl107 = 1;
                           AV26SDT_Demanda.gxTpr_Sistemacod = A127Sistema_Codigo;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(2);
                        }
                        pr_default.close(2);
                        if ( AV125GXLvl107 == 0 )
                        {
                           AV22ErrCod = 1;
                           AV33Linha = AV33Linha + "Sistema " + AV105OrigemDsc + " desconhecido. ";
                        }
                     }
                  }
                  if ( ( AV22ErrCod == 0 ) && ( AV99ColModulon > 0 ) )
                  {
                     AV98Modulo_Sigla = StringUtil.Upper( StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV99ColModulon, 1, 1).Text));
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98Modulo_Sigla)) )
                     {
                        AV126GXLvl123 = 0;
                        /* Using cursor P004D5 */
                        pr_default.execute(3, new Object[] {AV98Modulo_Sigla, AV26SDT_Demanda.gxTpr_Sistemacod});
                        while ( (pr_default.getStatus(3) != 101) )
                        {
                           A145Modulo_Sigla = P004D5_A145Modulo_Sigla[0];
                           A127Sistema_Codigo = P004D5_A127Sistema_Codigo[0];
                           A146Modulo_Codigo = P004D5_A146Modulo_Codigo[0];
                           AV126GXLvl123 = 1;
                           AV26SDT_Demanda.gxTpr_Modulocod = A146Modulo_Codigo;
                           AV26SDT_Demanda.gxTpr_Modulosigla = A145Modulo_Sigla;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(3);
                        }
                        pr_default.close(3);
                        if ( AV126GXLvl123 == 0 )
                        {
                           AV22ErrCod = 1;
                           AV33Linha = AV33Linha + "M�dulo " + AV98Modulo_Sigla + " desconhecido. ";
                        }
                     }
                  }
                  if ( AV22ErrCod == 0 )
                  {
                     /* Execute user subroutine: 'LERDEMANDA' */
                     S111 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV127GXLvl140 = 0;
                     /* Using cursor P004D7 */
                     pr_default.execute(4, new Object[] {AV26SDT_Demanda.gxTpr_Demanda, AV20Contratada_Codigo, AV26SDT_Demanda.gxTpr_Sistemacod, AV109ContratoServicos_Codigo});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A1553ContagemResultado_CntSrvCod = P004D7_A1553ContagemResultado_CntSrvCod[0];
                        n1553ContagemResultado_CntSrvCod = P004D7_n1553ContagemResultado_CntSrvCod[0];
                        A489ContagemResultado_SistemaCod = P004D7_A489ContagemResultado_SistemaCod[0];
                        n489ContagemResultado_SistemaCod = P004D7_n489ContagemResultado_SistemaCod[0];
                        A490ContagemResultado_ContratadaCod = P004D7_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P004D7_n490ContagemResultado_ContratadaCod[0];
                        A457ContagemResultado_Demanda = P004D7_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P004D7_n457ContagemResultado_Demanda[0];
                        A493ContagemResultado_DemandaFM = P004D7_A493ContagemResultado_DemandaFM[0];
                        n493ContagemResultado_DemandaFM = P004D7_n493ContagemResultado_DemandaFM[0];
                        A471ContagemResultado_DataDmn = P004D7_A471ContagemResultado_DataDmn[0];
                        A456ContagemResultado_Codigo = P004D7_A456ContagemResultado_Codigo[0];
                        A566ContagemResultado_DataUltCnt = P004D7_A566ContagemResultado_DataUltCnt[0];
                        n566ContagemResultado_DataUltCnt = P004D7_n566ContagemResultado_DataUltCnt[0];
                        A566ContagemResultado_DataUltCnt = P004D7_A566ContagemResultado_DataUltCnt[0];
                        n566ContagemResultado_DataUltCnt = P004D7_n566ContagemResultado_DataUltCnt[0];
                        AV127GXLvl140 = 1;
                        AV33Linha = AV33Linha + "Demanda: " + AV26SDT_Demanda.gxTpr_Demanda + " j� cadastrada " + ((DateTime.MinValue==A566ContagemResultado_DataUltCnt) ? context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") : context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 2, "/")) + " OS FM: " + StringUtil.Trim( A493ContagemResultado_DemandaFM);
                        if ( (0==AV75ServicoVnc_Codigo) )
                        {
                           AV22ErrCod = 1;
                        }
                        else
                        {
                           AV76ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
                           /* Execute user subroutine: 'BUSCASERVICOCADASTRADO' */
                           S171 ();
                           if ( returnInSub )
                           {
                              pr_default.close(4);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                        pr_default.readNext(4);
                     }
                     pr_default.close(4);
                     if ( AV127GXLvl140 == 0 )
                     {
                        AV26SDT_Demanda.gxTpr_Naocadastrada = true;
                        /* Execute user subroutine: 'CARREGASDT' */
                        S141 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                     }
                  }
                  if ( AV22ErrCod == 0 )
                  {
                     GXt_int1 = AV110Dias;
                     new prc_diasparaentrega(context ).execute( ref  AV109ContratoServicos_Codigo,  AV26SDT_Demanda.gxTpr_Pfbfs,  0, out  GXt_int1) ;
                     AV110Dias = GXt_int1;
                     AV111Entrega = DateTimeUtil.ResetTime( AV26SDT_Demanda.gxTpr_Datadmn ) ;
                     if ( AV116PrazoInicio == 2 )
                     {
                        new prc_datadeinicioutil(context ).execute( ref  AV111Entrega) ;
                     }
                     new prc_gethorasentrega(context ).execute(  AV32WWPContext.gxTpr_Areatrabalho_codigo,  0,  AV109ContratoServicos_Codigo, out  AV112Horas, out  AV113Minutos) ;
                     GXt_dtime2 = AV111Entrega;
                     new prc_adddiasuteis(context ).execute(  AV111Entrega,  AV110Dias,  AV115TipoDias, out  GXt_dtime2) ;
                     AV111Entrega = GXt_dtime2;
                     AV111Entrega = DateTimeUtil.TAdd( AV111Entrega, 3600*(AV112Horas));
                     AV111Entrega = DateTimeUtil.TAdd( AV111Entrega, 60*(AV113Minutos));
                     if ( (DateTime.MinValue==AV114PrazoEntrega) )
                     {
                        AV26SDT_Demanda.gxTpr_Dataent = AV111Entrega;
                     }
                     else
                     {
                        if ( AV114PrazoEntrega < AV111Entrega )
                        {
                           AV33Linha = AV33Linha + "Prazo de entrega informado " + context.localUtil.TToC( AV114PrazoEntrega, 8, 5, 0, 3, "/", ":", " ") + " menor ao SLA calculado " + context.localUtil.TToC( AV111Entrega, 8, 5, 0, 3, "/", ":", " ");
                           AV22ErrCod = 1;
                        }
                        else
                        {
                           AV26SDT_Demanda.gxTpr_Dataent = AV114PrazoEntrega;
                        }
                     }
                  }
                  if ( AV22ErrCod == 0 )
                  {
                     /* Execute user subroutine: 'CHECARREPETIDAS' */
                     S131 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV25SDT_Demandas.Add(AV26SDT_Demanda, 0);
                     AV26SDT_Demanda = new SdtSDT_Demandas_Demanda(context);
                     if ( AV95Warnings )
                     {
                        H4D0( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV33Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else
                  {
                     H4D0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV33Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                     AV47Erros = (short)(AV47Erros+1);
                  }
                  AV46Lidas = (short)(AV46Lidas+1);
               }
               AV24Ln = (short)(AV24Ln+1);
            }
            AV45Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV46Lidas), 4, 0)) + " linhas procesadas - Erros: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV47Erros), 4, 0)) + " linhas n�o ser�o importadas.";
            H4D0( false, 35) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV45Totais, "")), 25, Gx_line+17, 755, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
            AV23ExcelDocument.Close();
            AV27WebSession.Set("SDTDemandas", AV25SDT_Demandas.ToXml(false, true, "SDT_Demandas", "GxEv3Up14_MeetrikaVs3"));
            context.nUserReturn = 1;
            this.cleanup();
            if (true) return;
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H4D0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LERDEMANDA' Routine */
         AV97Demanda = StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV8ColOSFSn, 1, 1).Text);
         AV97Demanda = StringUtil.Trim( AV97Demanda);
         AV97Demanda = StringUtil.StringReplace( AV97Demanda, "'", "");
         AV97Demanda = StringUtil.StringReplace( AV97Demanda, "\"", "");
         AV97Demanda = StringUtil.StringReplace( AV97Demanda, ";", "");
         AV97Demanda = StringUtil.StringReplace( AV97Demanda, ",", "");
         AV97Demanda = StringUtil.StringReplace( AV97Demanda, "�", "");
         AV26SDT_Demanda.gxTpr_Demanda = AV97Demanda;
      }

      protected void S121( )
      {
         /* 'OPENFILE' Routine */
         AV22ErrCod = AV23ExcelDocument.Open(AV15Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Aba)) )
         {
            AV22ErrCod = AV23ExcelDocument.SelectSheet(AV65Aba);
         }
      }

      protected void S131( )
      {
         /* 'CHECARREPETIDAS' Routine */
         AV128GXV1 = 1;
         while ( AV128GXV1 <= AV25SDT_Demandas.Count )
         {
            AV88SDT_DmnChkRpt = ((SdtSDT_Demandas_Demanda)AV25SDT_Demandas.Item(AV128GXV1));
            if ( StringUtil.StrCmp(AV88SDT_DmnChkRpt.gxTpr_Demanda, AV26SDT_Demanda.gxTpr_Demanda) == 0 )
            {
               AV33Linha = AV33Linha + "Demanda: " + AV26SDT_Demanda.gxTpr_Demanda + " repetida na planilha.";
               AV22ErrCod = 1;
               if (true) break;
            }
            AV128GXV1 = (int)(AV128GXV1+1);
         }
      }

      protected void S141( )
      {
         /* 'CARREGASDT' Routine */
         AV26SDT_Demanda.gxTpr_Cntsrvcod = AV109ContratoServicos_Codigo;
         if ( (DateTime.MinValue==AV28DataOSFM) )
         {
            AV95Warnings = false;
            if ( (DateTime.MinValue==AV23ExcelDocument.get_Cells(AV24Ln, AV11ColDataDmnn, 1, 1).Date) )
            {
               AV26SDT_Demanda.gxTpr_Datadmn = DateTimeUtil.ServerNow( context, "DEFAULT");
               AV33Linha = AV33Linha + " Sem data, usou-se " + context.localUtil.DToC( AV26SDT_Demanda.gxTpr_Datadmn, 2, "/");
               AV95Warnings = true;
            }
            else
            {
               AV26SDT_Demanda.gxTpr_Datadmn = AV23ExcelDocument.get_Cells(AV24Ln, AV11ColDataDmnn, 1, 1).Date;
               if ( (DateTime.MinValue==AV26SDT_Demanda.gxTpr_Datadmn) )
               {
                  AV26SDT_Demanda.gxTpr_Datadmn = DateTimeUtil.ServerNow( context, "DEFAULT");
                  AV33Linha = AV33Linha + " Data n�o reconhecida, usou-se " + context.localUtil.DToC( AV26SDT_Demanda.gxTpr_Datadmn, 2, "/");
                  AV95Warnings = true;
               }
            }
         }
         else
         {
            AV26SDT_Demanda.gxTpr_Datadmn = AV28DataOSFM;
         }
         if ( (DateTime.MinValue==AV90DataCnt) )
         {
            if ( (0==AV91ColDataCntn) )
            {
               AV26SDT_Demanda.gxTpr_Datacnt = DateTimeUtil.ServerNow( context, "DEFAULT");
            }
            else
            {
               AV26SDT_Demanda.gxTpr_Datacnt = AV23ExcelDocument.get_Cells(AV24Ln, AV91ColDataCntn, 1, 1).Date;
            }
         }
         else
         {
            AV26SDT_Demanda.gxTpr_Datacnt = AV90DataCnt;
         }
         if ( ! AV117OSAutomatica )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21OSFM)) )
            {
               AV92OSFMLida = AV23ExcelDocument.get_Cells(AV24Ln, AV10ColOSFMn, 1, 1).Text;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV92OSFMLida)) )
               {
                  AV26SDT_Demanda.gxTpr_Demandafm = AV93OSFMAnterior;
               }
               else
               {
                  AV26SDT_Demanda.gxTpr_Demandafm = AV92OSFMLida;
                  AV93OSFMAnterior = AV92OSFMLida;
               }
            }
            else
            {
               AV26SDT_Demanda.gxTpr_Demandafm = AV21OSFM;
            }
         }
         if ( AV12ColDescricaon > 0 )
         {
            AV26SDT_Demanda.gxTpr_Descricao = AV23ExcelDocument.get_Cells(AV24Ln, AV12ColDescricaon, 1, 1).Text;
         }
         if ( AV106ColObservacaon > 0 )
         {
            AV26SDT_Demanda.gxTpr_Observacao = AV23ExcelDocument.get_Cells(AV24Ln, AV106ColObservacaon, 1, 1).Text;
         }
         else
         {
            AV26SDT_Demanda.gxTpr_Observacao = AV18Observacao;
         }
         if ( ! (DateTime.MinValue==AV114PrazoEntrega) )
         {
            AV26SDT_Demanda.gxTpr_Observacao = AV26SDT_Demanda.gxTpr_Observacao+StringUtil.NewLine( )+"(Prazo informado pelo usu�rio)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16Link)) )
         {
            AV26SDT_Demanda.gxTpr_Link = AV23ExcelDocument.get_Cells(AV24Ln, AV14ColLinkn, 1, 1).Text;
         }
         else
         {
            AV26SDT_Demanda.gxTpr_Link = AV16Link;
         }
         AV36p = 1;
         if ( (0==AV17ContadorFSCod) )
         {
            if ( AV19EhValidacao )
            {
               AV52ContadorNome = StringUtil.Upper( StringUtil.Trim( AV23ExcelDocument.get_Cells(AV24Ln, AV80ColContadorFSn, 1, 1).Text));
               AV56FSFlag = true;
               /* Execute user subroutine: 'PROCURACONTADOR' */
               S151 ();
               if (returnInSub) return;
               AV26SDT_Demanda.gxTpr_Contadorfscod = AV53ContadorCodigo;
               AV26SDT_Demanda.gxTpr_Contadorfsnom = AV52ContadorNome;
            }
            else
            {
               /* Execute user subroutine: 'CARREGAPFFS' */
               S161 ();
               if (returnInSub) return;
            }
         }
         else
         {
            if ( AV19EhValidacao )
            {
               AV26SDT_Demanda.gxTpr_Contadorfscod = AV17ContadorFSCod;
               AV26SDT_Demanda.gxTpr_Contadorfsnom = AV34ContadorFSNom;
            }
            else
            {
               AV26SDT_Demanda.gxTpr_Contadorfscod = 0;
               AV26SDT_Demanda.gxTpr_Contadorfsnom = "";
               /* Execute user subroutine: 'CARREGAPFFS' */
               S161 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S161( )
      {
         /* 'CARREGAPFFS' Routine */
         if ( AV77ColPFBFSDmnn > 0 )
         {
            AV26SDT_Demanda.gxTpr_Pfbfs = (decimal)(AV23ExcelDocument.get_Cells(AV24Ln, AV77ColPFBFSDmnn, 1, 1).Number);
            AV26SDT_Demanda.gxTpr_Pflfs = (decimal)(AV23ExcelDocument.get_Cells(AV24Ln, AV81ColPFLFS, 1, 1).Number);
         }
      }

      protected void S171( )
      {
         /* 'BUSCASERVICOCADASTRADO' Routine */
         AV129GXLvl334 = 0;
         /* Using cursor P004D8 */
         pr_default.execute(5, new Object[] {AV76ContagemResultado_OSVinculada, AV82ContratadaSrv_Codigo, AV26SDT_Demanda.gxTpr_Sistemacod, AV109ContratoServicos_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004D8_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004D8_n1553ContagemResultado_CntSrvCod[0];
            A489ContagemResultado_SistemaCod = P004D8_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P004D8_n489ContagemResultado_SistemaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P004D8_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P004D8_n805ContagemResultado_ContratadaOrigemCod[0];
            A602ContagemResultado_OSVinculada = P004D8_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P004D8_n602ContagemResultado_OSVinculada[0];
            A471ContagemResultado_DataDmn = P004D8_A471ContagemResultado_DataDmn[0];
            A456ContagemResultado_Codigo = P004D8_A456ContagemResultado_Codigo[0];
            AV129GXLvl334 = 1;
            AV33Linha = AV33Linha + "Servi�o vinculado j� cadastrado " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/");
            AV22ErrCod = 1;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( AV129GXLvl334 == 0 )
         {
            H4D0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV33Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            /* Execute user subroutine: 'CARREGASDT' */
            S141 ();
            if (returnInSub) return;
         }
      }

      protected void S151( )
      {
         /* 'PROCURACONTADOR' Routine */
         AV53ContadorCodigo = 0;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52ContadorNome)) )
         {
            AV52ContadorNome = "<desconhecido>";
         }
         else
         {
            AV37i = 1;
            AV36p = 1;
            AV39LastSpace = (short)(StringUtil.Len( AV52ContadorNome));
            GX_I = 1;
            while ( GX_I <= 4 )
            {
               AV60Nomes[GX_I-1] = "";
               GX_I = (int)(GX_I+1);
            }
            AV35Flag = true;
            while ( AV35Flag )
            {
               AV43s = (short)(StringUtil.StringSearch( AV52ContadorNome, " ", AV36p)-1);
               if ( AV43s < 0 )
               {
                  AV43s = (short)(StringUtil.Len( AV52ContadorNome));
               }
               AV60Nomes[AV37i-1] = "%" + StringUtil.Substring( AV52ContadorNome, AV36p, AV43s) + "%";
               AV36p = (short)(AV43s+2);
               AV37i = (short)(AV37i+1);
               AV35Flag = (bool)((AV36p<AV39LastSpace)&&(AV37i<=4));
            }
            AV37i = 0;
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 AV56FSFlag ,
                                                 AV57FMFlag ,
                                                 A66ContratadaUsuario_ContratadaCod ,
                                                 AV20Contratada_Codigo ,
                                                 A516Contratada_TipoFabrica ,
                                                 A1394ContratadaUsuario_UsuarioAtivo ,
                                                 A71ContratadaUsuario_UsuarioPessoaNom ,
                                                 AV60Nomes ,
                                                 A1228ContratadaUsuario_AreaTrabalhoCod ,
                                                 AV32WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor P004D9 */
            pr_default.execute(6, new Object[] {AV32WWPContext.gxTpr_Areatrabalho_codigo, AV20Contratada_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = P004D9_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P004D9_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = P004D9_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = P004D9_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1394ContratadaUsuario_UsuarioAtivo = P004D9_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P004D9_n1394ContratadaUsuario_UsuarioAtivo[0];
               A516Contratada_TipoFabrica = P004D9_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P004D9_n516Contratada_TipoFabrica[0];
               A66ContratadaUsuario_ContratadaCod = P004D9_A66ContratadaUsuario_ContratadaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P004D9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P004D9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = P004D9_A69ContratadaUsuario_UsuarioCod[0];
               A516Contratada_TipoFabrica = P004D9_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P004D9_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P004D9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P004D9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P004D9_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P004D9_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = P004D9_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P004D9_n1394ContratadaUsuario_UsuarioAtivo[0];
               A71ContratadaUsuario_UsuarioPessoaNom = P004D9_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = P004D9_n71ContratadaUsuario_UsuarioPessoaNom[0];
               if ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV60Nomes[1-1] , 100 , "%"),  ' ' ) )
               {
                  if ( ( StringUtil.StrCmp(AV60Nomes[2-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV60Nomes[2-1] , 100 , "%"),  ' ' ) ) )
                  {
                     if ( ( StringUtil.StrCmp(AV60Nomes[3-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV60Nomes[3-1] , 100 , "%"),  ' ' ) ) )
                     {
                        if ( ( StringUtil.StrCmp(AV60Nomes[4-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV60Nomes[4-1] , 100 , "%"),  ' ' ) ) )
                        {
                           AV37i = (short)(AV37i+1);
                           AV53ContadorCodigo = A69ContratadaUsuario_UsuarioCod;
                           AV55NomeAchado = A71ContratadaUsuario_UsuarioPessoaNom;
                        }
                     }
                  }
               }
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         if ( (0==AV53ContadorCodigo) || ( AV37i > 1 ) )
         {
            AV53ContadorCodigo = 0;
            AV33Linha = AV33Linha + "Contador " + (AV56FSFlag ? "FS " : "FM ") + AV52ContadorNome + " n�o identificado" + ((AV37i>1) ? " de "+StringUtil.Trim( StringUtil.Str( (decimal)(AV37i), 4, 0))+"existentes." : ".");
            AV52ContadorNome = "";
            AV22ErrCod = 1;
         }
         else
         {
            AV52ContadorNome = AV55NomeAchado;
         }
      }

      protected void H4D0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 675, Gx_line+9, 724, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 733, Gx_line+9, 826, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV64NomeArq, "")), 17, Gx_line+33, 817, Gx_line+51, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("An�lise de demandas para importar", 258, Gx_line+0, 569, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+70);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV32WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV64NomeArq = "";
         scmdbuf = "";
         P004D2_A74Contrato_Codigo = new int[1] ;
         P004D2_A39Contratada_Codigo = new int[1] ;
         P004D2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P004D2_A29Contratante_Codigo = new int[1] ;
         P004D2_n29Contratante_Codigo = new bool[] {false} ;
         P004D2_A160ContratoServicos_Codigo = new int[1] ;
         P004D2_A558Servico_Percentual = new decimal[1] ;
         P004D2_n558Servico_Percentual = new bool[] {false} ;
         P004D2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P004D2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P004D2_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P004D2_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P004D2_A593Contratante_OSAutomatica = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         AV116PrazoInicio = 1;
         AV115TipoDias = "";
         AV68FiltroChar = "";
         AV69FiltroDate = DateTime.MinValue;
         AV101CalculoPFinal = "";
         AV63TxtLinha = "";
         AV23ExcelDocument = new ExcelDocumentI();
         AV94String = "";
         AV33Linha = "";
         AV52ContadorNome = "";
         AV26SDT_Demanda = new SdtSDT_Demandas_Demanda(context);
         AV30Sistema_Sigla = "";
         P004D3_A130Sistema_Ativo = new bool[] {false} ;
         P004D3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P004D3_A129Sistema_Sigla = new String[] {""} ;
         P004D3_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         AV105OrigemDsc = "";
         P004D4_A1461SistemaDePara_Origem = new String[] {""} ;
         P004D4_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         P004D4_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         P004D4_A127Sistema_Codigo = new int[1] ;
         P004D4_A1460SistemaDePara_Codigo = new int[1] ;
         A1461SistemaDePara_Origem = "";
         A1463SistemaDePara_OrigemDsc = "";
         AV98Modulo_Sigla = "";
         P004D5_A145Modulo_Sigla = new String[] {""} ;
         P004D5_A127Sistema_Codigo = new int[1] ;
         P004D5_A146Modulo_Codigo = new int[1] ;
         A145Modulo_Sigla = "";
         P004D7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004D7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004D7_A489ContagemResultado_SistemaCod = new int[1] ;
         P004D7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P004D7_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004D7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004D7_A457ContagemResultado_Demanda = new String[] {""} ;
         P004D7_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004D7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P004D7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P004D7_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P004D7_A456ContagemResultado_Codigo = new int[1] ;
         P004D7_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P004D7_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV111Entrega = (DateTime)(DateTime.MinValue);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         AV25SDT_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         AV45Totais = "";
         AV27WebSession = context.GetSession();
         AV97Demanda = "";
         AV88SDT_DmnChkRpt = new SdtSDT_Demandas_Demanda(context);
         AV92OSFMLida = "";
         AV93OSFMAnterior = "";
         P004D8_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004D8_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004D8_A489ContagemResultado_SistemaCod = new int[1] ;
         P004D8_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P004D8_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P004D8_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P004D8_A602ContagemResultado_OSVinculada = new int[1] ;
         P004D8_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P004D8_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P004D8_A456ContagemResultado_Codigo = new int[1] ;
         AV60Nomes = new String [4] ;
         GX_I = 1;
         while ( GX_I <= 4 )
         {
            AV60Nomes[GX_I-1] = "";
            GX_I = (int)(GX_I+1);
         }
         A516Contratada_TipoFabrica = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         P004D9_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P004D9_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P004D9_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P004D9_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P004D9_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P004D9_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P004D9_A516Contratada_TipoFabrica = new String[] {""} ;
         P004D9_n516Contratada_TipoFabrica = new bool[] {false} ;
         P004D9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P004D9_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P004D9_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P004D9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV55NomeAchado = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_carregasdtdemandas__default(),
            new Object[][] {
                new Object[] {
               P004D2_A74Contrato_Codigo, P004D2_A39Contratada_Codigo, P004D2_A52Contratada_AreaTrabalhoCod, P004D2_A29Contratante_Codigo, P004D2_n29Contratante_Codigo, P004D2_A160ContratoServicos_Codigo, P004D2_A558Servico_Percentual, P004D2_n558Servico_Percentual, P004D2_A1454ContratoServicos_PrazoTpDias, P004D2_n1454ContratoServicos_PrazoTpDias,
               P004D2_A1649ContratoServicos_PrazoInicio, P004D2_n1649ContratoServicos_PrazoInicio, P004D2_A593Contratante_OSAutomatica
               }
               , new Object[] {
               P004D3_A130Sistema_Ativo, P004D3_A135Sistema_AreaTrabalhoCod, P004D3_A129Sistema_Sigla, P004D3_A127Sistema_Codigo
               }
               , new Object[] {
               P004D4_A1461SistemaDePara_Origem, P004D4_A1463SistemaDePara_OrigemDsc, P004D4_n1463SistemaDePara_OrigemDsc, P004D4_A127Sistema_Codigo, P004D4_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               P004D5_A145Modulo_Sigla, P004D5_A127Sistema_Codigo, P004D5_A146Modulo_Codigo
               }
               , new Object[] {
               P004D7_A1553ContagemResultado_CntSrvCod, P004D7_n1553ContagemResultado_CntSrvCod, P004D7_A489ContagemResultado_SistemaCod, P004D7_n489ContagemResultado_SistemaCod, P004D7_A490ContagemResultado_ContratadaCod, P004D7_n490ContagemResultado_ContratadaCod, P004D7_A457ContagemResultado_Demanda, P004D7_n457ContagemResultado_Demanda, P004D7_A493ContagemResultado_DemandaFM, P004D7_n493ContagemResultado_DemandaFM,
               P004D7_A471ContagemResultado_DataDmn, P004D7_A456ContagemResultado_Codigo, P004D7_A566ContagemResultado_DataUltCnt, P004D7_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P004D8_A1553ContagemResultado_CntSrvCod, P004D8_n1553ContagemResultado_CntSrvCod, P004D8_A489ContagemResultado_SistemaCod, P004D8_n489ContagemResultado_SistemaCod, P004D8_A805ContagemResultado_ContratadaOrigemCod, P004D8_n805ContagemResultado_ContratadaOrigemCod, P004D8_A602ContagemResultado_OSVinculada, P004D8_n602ContagemResultado_OSVinculada, P004D8_A471ContagemResultado_DataDmn, P004D8_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P004D9_A70ContratadaUsuario_UsuarioPessoaCod, P004D9_n70ContratadaUsuario_UsuarioPessoaCod, P004D9_A71ContratadaUsuario_UsuarioPessoaNom, P004D9_n71ContratadaUsuario_UsuarioPessoaNom, P004D9_A1394ContratadaUsuario_UsuarioAtivo, P004D9_n1394ContratadaUsuario_UsuarioAtivo, P004D9_A516Contratada_TipoFabrica, P004D9_n516Contratada_TipoFabrica, P004D9_A66ContratadaUsuario_ContratadaCod, P004D9_A1228ContratadaUsuario_AreaTrabalhoCod,
               P004D9_n1228ContratadaUsuario_AreaTrabalhoCod, P004D9_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV42PraLinha ;
      private short AV8ColOSFSn ;
      private short AV9ColNomeSisteman ;
      private short AV99ColModulon ;
      private short AV10ColOSFMn ;
      private short AV11ColDataDmnn ;
      private short AV12ColDescricaon ;
      private short AV14ColLinkn ;
      private short AV48ColPFBFMn ;
      private short AV49ColPFLFMn ;
      private short AV50ColContadorFMn ;
      private short AV66ColFiltron ;
      private short AV77ColPFBFSDmnn ;
      private short AV78ColPFLFSDmnn ;
      private short AV80ColContadorFSn ;
      private short AV91ColDataCntn ;
      private short AV104ColProjeton ;
      private short AV102ColTipon ;
      private short AV106ColObservacaon ;
      private short GxWebError ;
      private short AV81ColPFLFS ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short OV116PrazoInicio ;
      private short AV116PrazoInicio ;
      private short AV24Ln ;
      private short AV22ErrCod ;
      private short AV124GXLvl93 ;
      private short AV125GXLvl107 ;
      private short AV126GXLvl123 ;
      private short AV127GXLvl140 ;
      private short AV110Dias ;
      private short GXt_int1 ;
      private short AV112Horas ;
      private short AV113Minutos ;
      private short AV47Erros ;
      private short AV46Lidas ;
      private short AV36p ;
      private short AV129GXLvl334 ;
      private short AV37i ;
      private short AV39LastSpace ;
      private short AV43s ;
      private int AV20Contratada_Codigo ;
      private int AV82ContratadaSrv_Codigo ;
      private int AV17ContadorFSCod ;
      private int AV51ContadorFMCod ;
      private int AV109ContratoServicos_Codigo ;
      private int AV75ServicoVnc_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A29Contratante_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV53ContadorCodigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A1460SistemaDePara_Codigo ;
      private int A146Modulo_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV76ContagemResultado_OSVinculada ;
      private int Gx_OldLine ;
      private int AV128GXV1 ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int GX_I ;
      private int AV32WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private decimal AV61Servico_Percentual ;
      private decimal A558Servico_Percentual ;
      private decimal AV70FiltroNum ;
      private decimal AV100CstUntNrm ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV15Arquivo ;
      private String AV65Aba ;
      private String AV34ContadorFSNom ;
      private String AV67TipoFiltro ;
      private String AV74Filtro ;
      private String AV96FileName ;
      private String AV64NomeArq ;
      private String scmdbuf ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV115TipoDias ;
      private String AV68FiltroChar ;
      private String AV101CalculoPFinal ;
      private String AV63TxtLinha ;
      private String AV94String ;
      private String AV33Linha ;
      private String AV52ContadorNome ;
      private String AV30Sistema_Sigla ;
      private String A129Sistema_Sigla ;
      private String A1461SistemaDePara_Origem ;
      private String AV98Modulo_Sigla ;
      private String A145Modulo_Sigla ;
      private String AV45Totais ;
      private String [] AV60Nomes ;
      private String A516Contratada_TipoFabrica ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String AV55NomeAchado ;
      private String Gx_time ;
      private DateTime AV114PrazoEntrega ;
      private DateTime AV111Entrega ;
      private DateTime GXt_dtime2 ;
      private DateTime AV28DataOSFM ;
      private DateTime AV90DataCnt ;
      private DateTime AV69FiltroDate ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV19EhValidacao ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool n558Servico_Percentual ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool A593Contratante_OSAutomatica ;
      private bool AV117OSAutomatica ;
      private bool AV71TemFiltro ;
      private bool AV72Ok ;
      private bool AV57FMFlag ;
      private bool A130Sistema_Ativo ;
      private bool n1463SistemaDePara_OrigemDsc ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool AV95Warnings ;
      private bool AV56FSFlag ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool AV35Flag ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n516Contratada_TipoFabrica ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String AV18Observacao ;
      private String AV16Link ;
      private String AV105OrigemDsc ;
      private String A1463SistemaDePara_OrigemDsc ;
      private String AV21OSFM ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV97Demanda ;
      private String AV92OSFMLida ;
      private String AV93OSFMAnterior ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private DateTime aP14_DataOSFM ;
      private IDataStoreProvider pr_default ;
      private int[] P004D2_A74Contrato_Codigo ;
      private int[] P004D2_A39Contratada_Codigo ;
      private int[] P004D2_A52Contratada_AreaTrabalhoCod ;
      private int[] P004D2_A29Contratante_Codigo ;
      private bool[] P004D2_n29Contratante_Codigo ;
      private int[] P004D2_A160ContratoServicos_Codigo ;
      private decimal[] P004D2_A558Servico_Percentual ;
      private bool[] P004D2_n558Servico_Percentual ;
      private String[] P004D2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P004D2_n1454ContratoServicos_PrazoTpDias ;
      private short[] P004D2_A1649ContratoServicos_PrazoInicio ;
      private bool[] P004D2_n1649ContratoServicos_PrazoInicio ;
      private bool[] P004D2_A593Contratante_OSAutomatica ;
      private bool[] P004D3_A130Sistema_Ativo ;
      private int[] P004D3_A135Sistema_AreaTrabalhoCod ;
      private String[] P004D3_A129Sistema_Sigla ;
      private int[] P004D3_A127Sistema_Codigo ;
      private String[] P004D4_A1461SistemaDePara_Origem ;
      private String[] P004D4_A1463SistemaDePara_OrigemDsc ;
      private bool[] P004D4_n1463SistemaDePara_OrigemDsc ;
      private int[] P004D4_A127Sistema_Codigo ;
      private int[] P004D4_A1460SistemaDePara_Codigo ;
      private String[] P004D5_A145Modulo_Sigla ;
      private int[] P004D5_A127Sistema_Codigo ;
      private int[] P004D5_A146Modulo_Codigo ;
      private int[] P004D7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004D7_n1553ContagemResultado_CntSrvCod ;
      private int[] P004D7_A489ContagemResultado_SistemaCod ;
      private bool[] P004D7_n489ContagemResultado_SistemaCod ;
      private int[] P004D7_A490ContagemResultado_ContratadaCod ;
      private bool[] P004D7_n490ContagemResultado_ContratadaCod ;
      private String[] P004D7_A457ContagemResultado_Demanda ;
      private bool[] P004D7_n457ContagemResultado_Demanda ;
      private String[] P004D7_A493ContagemResultado_DemandaFM ;
      private bool[] P004D7_n493ContagemResultado_DemandaFM ;
      private DateTime[] P004D7_A471ContagemResultado_DataDmn ;
      private int[] P004D7_A456ContagemResultado_Codigo ;
      private DateTime[] P004D7_A566ContagemResultado_DataUltCnt ;
      private bool[] P004D7_n566ContagemResultado_DataUltCnt ;
      private int[] P004D8_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004D8_n1553ContagemResultado_CntSrvCod ;
      private int[] P004D8_A489ContagemResultado_SistemaCod ;
      private bool[] P004D8_n489ContagemResultado_SistemaCod ;
      private int[] P004D8_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P004D8_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P004D8_A602ContagemResultado_OSVinculada ;
      private bool[] P004D8_n602ContagemResultado_OSVinculada ;
      private DateTime[] P004D8_A471ContagemResultado_DataDmn ;
      private int[] P004D8_A456ContagemResultado_Codigo ;
      private int[] P004D9_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P004D9_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P004D9_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P004D9_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P004D9_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P004D9_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] P004D9_A516Contratada_TipoFabrica ;
      private bool[] P004D9_n516Contratada_TipoFabrica ;
      private int[] P004D9_A66ContratadaUsuario_ContratadaCod ;
      private int[] P004D9_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P004D9_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P004D9_A69ContratadaUsuario_UsuarioCod ;
      private IGxSession AV27WebSession ;
      private ExcelDocumentI AV23ExcelDocument ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV25SDT_Demandas ;
      private SdtSDT_Demandas_Demanda AV26SDT_Demanda ;
      private SdtSDT_Demandas_Demanda AV88SDT_DmnChkRpt ;
      private wwpbaseobjects.SdtWWPContext AV32WWPContext ;
   }

   public class aprc_carregasdtdemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P004D9( IGxContext context ,
                                             bool AV56FSFlag ,
                                             bool AV57FMFlag ,
                                             int A66ContratadaUsuario_ContratadaCod ,
                                             int AV20Contratada_Codigo ,
                                             String A516Contratada_TipoFabrica ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String []  aP0_Nomes ,
                                             int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                             int AV32WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [2] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String [] AV60Nomes = aP0_Nomes ;
         scmdbuf = "SELECT T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Contratada_TipoFabrica], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Usuario_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Contratada_AreaTrabalhoCod] = @AV32WWPC_1Areatrabalho_codigo)";
         if ( AV56FSFlag )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_ContratadaCod] = @AV20Contratada_Codigo)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV57FMFlag )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_TipoFabrica] = 'M')";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_P004D9(context, (bool)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String[])dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004D2 ;
          prmP004D2 = new Object[] {
          new Object[] {"@AV109ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004D3 ;
          prmP004D3 = new Object[] {
          new Object[] {"@AV30Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV32WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004D4 ;
          prmP004D4 = new Object[] {
          new Object[] {"@AV105OrigemDsc",SqlDbType.VarChar,500,0}
          } ;
          Object[] prmP004D5 ;
          prmP004D5 = new Object[] {
          new Object[] {"@AV98Modulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV26SDT_Demanda__Sistemacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004D7 ;
          prmP004D7 = new Object[] {
          new Object[] {"@AV26SDT_Demanda__Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV20Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SDT_Demanda__Sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV109ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004D8 ;
          prmP004D8 = new Object[] {
          new Object[] {"@AV76ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82ContratadaSrv_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SDT_Demanda__Sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV109ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004D9 ;
          prmP004D9 = new Object[] {
          new Object[] {"@AV32WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004D2", "SELECT TOP 1 T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Percentual], T1.[ContratoServicos_PrazoTpDias], T1.[ContratoServicos_PrazoInicio], T5.[Contratante_OSAutomatica] FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV109ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D2,1,0,false,true )
             ,new CursorDef("P004D3", "SELECT TOP 1 [Sistema_Ativo], [Sistema_AreaTrabalhoCod], [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Sigla] = @AV30Sistema_Sigla) AND ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV32WWPC_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D3,1,0,false,true )
             ,new CursorDef("P004D4", "SELECT TOP 1 [SistemaDePara_Origem], [SistemaDePara_OrigemDsc], [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [SistemaDePara_Origem] = 'R' and [SistemaDePara_OrigemDsc] = @AV105OrigemDsc ORDER BY [SistemaDePara_Origem], [SistemaDePara_OrigemDsc] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D4,1,0,false,true )
             ,new CursorDef("P004D5", "SELECT TOP 1 [Modulo_Sigla], [Sistema_Codigo], [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE ([Modulo_Sigla] = @AV98Modulo_Sigla) AND ([Sistema_Codigo] = @AV26SDT_Demanda__Sistemacod) ORDER BY [Modulo_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D5,1,0,false,true )
             ,new CursorDef("P004D7", "SELECT T1.[ContagemResultado_CntSrvCod], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Demanda] = @AV26SDT_Demanda__Demanda) AND (T1.[ContagemResultado_ContratadaCod] = @AV20Contratada_Codigo) AND (T1.[ContagemResultado_SistemaCod] = @AV26SDT_Demanda__Sistemacod) AND (T1.[ContagemResultado_CntSrvCod] = @AV109ContratoServicos_Codigo) ORDER BY T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D7,100,0,true,false )
             ,new CursorDef("P004D8", "SELECT TOP 1 [ContagemResultado_CntSrvCod], [ContagemResultado_SistemaCod], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_OSVinculada], [ContagemResultado_DataDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_OSVinculada] = @AV76ContagemResultado_OSVinculada) AND ([ContagemResultado_ContratadaOrigemCod] = @AV82ContratadaSrv_Codigo) AND ([ContagemResultado_SistemaCod] = @AV26SDT_Demanda__Sistemacod) AND ([ContagemResultado_CntSrvCod] = @AV109ContratoServicos_Codigo) ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D8,1,0,false,true )
             ,new CursorDef("P004D9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004D9,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((short[]) buf[10])[0] = rslt.getShort(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 25) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
