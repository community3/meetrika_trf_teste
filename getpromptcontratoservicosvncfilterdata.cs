/*
               File: GetPromptContratoServicosVncFilterData
        Description: Get Prompt Contrato Servicos Vnc Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:55.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoservicosvncfilterdata : GXProcedure
   {
      public getpromptcontratoservicosvncfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoservicosvncfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoservicosvncfilterdata objgetpromptcontratoservicosvncfilterdata;
         objgetpromptcontratoservicosvncfilterdata = new getpromptcontratoservicosvncfilterdata();
         objgetpromptcontratoservicosvncfilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptcontratoservicosvncfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoservicosvncfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoservicosvncfilterdata.AV22OptionsJson = "" ;
         objgetpromptcontratoservicosvncfilterdata.AV25OptionsDescJson = "" ;
         objgetpromptcontratoservicosvncfilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptcontratoservicosvncfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoservicosvncfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoservicosvncfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoservicosvncfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOSRVVNC_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SERVICOSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptContratoServicosVncGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoServicosVncGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptContratoServicosVncGridState"), "");
         }
         AV36GXV1 = 1;
         while ( AV36GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV36GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA") == 0 )
            {
               AV10TFContratoSrvVnc_ServicoSigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA_SEL") == 0 )
            {
               AV11TFContratoSrvVnc_ServicoSigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_STATUSDMN_SEL") == 0 )
            {
               AV12TFContratoSrvVnc_StatusDmn_SelsJson = AV32GridStateFilterValue.gxTpr_Value;
               AV13TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV12TFContratoSrvVnc_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
            {
               AV14TFContratoSrvVnc_ServicoVncSigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL") == 0 )
            {
               AV15TFContratoSrvVnc_ServicoVncSigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV36GXV1 = (int)(AV36GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSRVVNC_SERVICOSIGLAOPTIONS' Routine */
         AV10TFContratoSrvVnc_ServicoSigla = AV16SearchTxt;
         AV11TFContratoSrvVnc_ServicoSigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                              AV11TFContratoSrvVnc_ServicoSigla_Sel ,
                                              AV10TFContratoSrvVnc_ServicoSigla ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoVncSigla ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A924ContratoSrvVnc_ServicoVncSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratoSrvVnc_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoSrvVnc_ServicoSigla), 15, "%");
         lV14TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         /* Using cursor P00PE2 */
         pr_default.execute(0, new Object[] {lV10TFContratoSrvVnc_ServicoSigla, AV11TFContratoSrvVnc_ServicoSigla_Sel, lV14TFContratoSrvVnc_ServicoVncSigla, AV15TFContratoSrvVnc_ServicoVncSigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPE2 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PE2_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PE2_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PE2_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE2_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PE2_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PE2_n923ContratoSrvVnc_ServicoVncCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PE2_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PE2_n922ContratoSrvVnc_ServicoSigla[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PE2_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PE2_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PE2_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PE2_n1084ContratoSrvVnc_StatusDmn[0];
            A917ContratoSrvVnc_Codigo = P00PE2_A917ContratoSrvVnc_Codigo[0];
            A921ContratoSrvVnc_ServicoCod = P00PE2_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PE2_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PE2_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PE2_n922ContratoSrvVnc_ServicoSigla[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PE2_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PE2_n923ContratoSrvVnc_ServicoVncCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PE2_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PE2_n924ContratoSrvVnc_ServicoVncSigla[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PE2_A922ContratoSrvVnc_ServicoSigla[0], A922ContratoSrvVnc_ServicoSigla) == 0 ) )
            {
               BRKPE2 = false;
               A915ContratoSrvVnc_CntSrvCod = P00PE2_A915ContratoSrvVnc_CntSrvCod[0];
               A921ContratoSrvVnc_ServicoCod = P00PE2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = P00PE2_n921ContratoSrvVnc_ServicoCod[0];
               A917ContratoSrvVnc_Codigo = P00PE2_A917ContratoSrvVnc_Codigo[0];
               A921ContratoSrvVnc_ServicoCod = P00PE2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = P00PE2_n921ContratoSrvVnc_ServicoCod[0];
               AV28count = (long)(AV28count+1);
               BRKPE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla)) )
            {
               AV20Option = A922ContratoSrvVnc_ServicoSigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPE2 )
            {
               BRKPE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' Routine */
         AV14TFContratoSrvVnc_ServicoVncSigla = AV16SearchTxt;
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                              AV11TFContratoSrvVnc_ServicoSigla_Sel ,
                                              AV10TFContratoSrvVnc_ServicoSigla ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoVncSigla ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A924ContratoSrvVnc_ServicoVncSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratoSrvVnc_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoSrvVnc_ServicoSigla), 15, "%");
         lV14TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         /* Using cursor P00PE3 */
         pr_default.execute(1, new Object[] {lV10TFContratoSrvVnc_ServicoSigla, AV11TFContratoSrvVnc_ServicoSigla_Sel, lV14TFContratoSrvVnc_ServicoVncSigla, AV15TFContratoSrvVnc_ServicoVncSigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKPE4 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PE3_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PE3_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PE3_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PE3_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PE3_n923ContratoSrvVnc_ServicoVncCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PE3_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PE3_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PE3_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PE3_n1084ContratoSrvVnc_StatusDmn[0];
            A922ContratoSrvVnc_ServicoSigla = P00PE3_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PE3_n922ContratoSrvVnc_ServicoSigla[0];
            A917ContratoSrvVnc_Codigo = P00PE3_A917ContratoSrvVnc_Codigo[0];
            A921ContratoSrvVnc_ServicoCod = P00PE3_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PE3_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PE3_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PE3_n922ContratoSrvVnc_ServicoSigla[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PE3_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PE3_n923ContratoSrvVnc_ServicoVncCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PE3_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PE3_n924ContratoSrvVnc_ServicoVncSigla[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00PE3_A924ContratoSrvVnc_ServicoVncSigla[0], A924ContratoSrvVnc_ServicoVncSigla) == 0 ) )
            {
               BRKPE4 = false;
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PE3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A923ContratoSrvVnc_ServicoVncCod = P00PE3_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00PE3_n923ContratoSrvVnc_ServicoVncCod[0];
               A917ContratoSrvVnc_Codigo = P00PE3_A917ContratoSrvVnc_Codigo[0];
               A923ContratoSrvVnc_ServicoVncCod = P00PE3_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00PE3_n923ContratoSrvVnc_ServicoVncCod[0];
               AV28count = (long)(AV28count+1);
               BRKPE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla)) )
            {
               AV20Option = A924ContratoSrvVnc_ServicoVncSigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPE4 )
            {
               BRKPE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoSrvVnc_ServicoSigla = "";
         AV11TFContratoSrvVnc_ServicoSigla_Sel = "";
         AV12TFContratoSrvVnc_StatusDmn_SelsJson = "";
         AV13TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV14TFContratoSrvVnc_ServicoVncSigla = "";
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         scmdbuf = "";
         lV10TFContratoSrvVnc_ServicoSigla = "";
         lV14TFContratoSrvVnc_ServicoVncSigla = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A922ContratoSrvVnc_ServicoSigla = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         P00PE2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PE2_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PE2_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PE2_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PE2_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PE2_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PE2_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PE2_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PE2_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PE2_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PE2_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PE2_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PE2_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PE2_A917ContratoSrvVnc_Codigo = new int[1] ;
         AV20Option = "";
         P00PE3_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PE3_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PE3_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PE3_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PE3_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PE3_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PE3_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PE3_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PE3_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PE3_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PE3_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PE3_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PE3_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PE3_A917ContratoSrvVnc_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoservicosvncfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PE2_A915ContratoSrvVnc_CntSrvCod, P00PE2_A921ContratoSrvVnc_ServicoCod, P00PE2_n921ContratoSrvVnc_ServicoCod, P00PE2_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PE2_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PE2_A923ContratoSrvVnc_ServicoVncCod, P00PE2_n923ContratoSrvVnc_ServicoVncCod, P00PE2_A922ContratoSrvVnc_ServicoSigla, P00PE2_n922ContratoSrvVnc_ServicoSigla, P00PE2_A924ContratoSrvVnc_ServicoVncSigla,
               P00PE2_n924ContratoSrvVnc_ServicoVncSigla, P00PE2_A1084ContratoSrvVnc_StatusDmn, P00PE2_n1084ContratoSrvVnc_StatusDmn, P00PE2_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               P00PE3_A915ContratoSrvVnc_CntSrvCod, P00PE3_A921ContratoSrvVnc_ServicoCod, P00PE3_n921ContratoSrvVnc_ServicoCod, P00PE3_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PE3_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PE3_A923ContratoSrvVnc_ServicoVncCod, P00PE3_n923ContratoSrvVnc_ServicoVncCod, P00PE3_A924ContratoSrvVnc_ServicoVncSigla, P00PE3_n924ContratoSrvVnc_ServicoVncSigla, P00PE3_A1084ContratoSrvVnc_StatusDmn,
               P00PE3_n1084ContratoSrvVnc_StatusDmn, P00PE3_A922ContratoSrvVnc_ServicoSigla, P00PE3_n922ContratoSrvVnc_ServicoSigla, P00PE3_A917ContratoSrvVnc_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV36GXV1 ;
      private int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private long AV28count ;
      private String AV10TFContratoSrvVnc_ServicoSigla ;
      private String AV11TFContratoSrvVnc_ServicoSigla_Sel ;
      private String AV14TFContratoSrvVnc_ServicoVncSigla ;
      private String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String scmdbuf ;
      private String lV10TFContratoSrvVnc_ServicoSigla ;
      private String lV14TFContratoSrvVnc_ServicoVncSigla ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private bool returnInSub ;
      private bool BRKPE2 ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool BRKPE4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV12TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00PE2_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PE2_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PE2_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PE2_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PE2_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PE2_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PE2_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] P00PE2_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PE2_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00PE2_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PE2_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PE2_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PE2_n1084ContratoSrvVnc_StatusDmn ;
      private int[] P00PE2_A917ContratoSrvVnc_Codigo ;
      private int[] P00PE3_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PE3_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PE3_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PE3_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PE3_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PE3_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PE3_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] P00PE3_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PE3_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PE3_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PE3_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PE3_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PE3_n922ContratoSrvVnc_ServicoSigla ;
      private int[] P00PE3_A917ContratoSrvVnc_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getpromptcontratoservicosvncfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PE2( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                             String AV11TFContratoSrvVnc_ServicoSigla_Sel ,
                                             String AV10TFContratoSrvVnc_ServicoSigla ,
                                             int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoVncSigla ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A924ContratoSrvVnc_ServicoVncSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T5.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_Codigo] FROM (((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoSrvVnc_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoSrvVnc_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV10TFContratoSrvVnc_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV10TFContratoSrvVnc_ServicoSigla)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV11TFContratoSrvVnc_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV11TFContratoSrvVnc_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV13TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00PE3( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                             String AV11TFContratoSrvVnc_ServicoSigla_Sel ,
                                             String AV10TFContratoSrvVnc_ServicoSigla ,
                                             int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoVncSigla ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A924ContratoSrvVnc_ServicoVncSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [4] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T5.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T1.[ContratoSrvVnc_Codigo] FROM (((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoSrvVnc_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoSrvVnc_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV10TFContratoSrvVnc_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV10TFContratoSrvVnc_ServicoSigla)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV11TFContratoSrvVnc_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV11TFContratoSrvVnc_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV13TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PE2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 1 :
                     return conditional_P00PE3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PE2 ;
          prmP00PE2 = new Object[] {
          new Object[] {"@lV10TFContratoSrvVnc_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11TFContratoSrvVnc_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00PE3 ;
          prmP00PE3 = new Object[] {
          new Object[] {"@lV10TFContratoSrvVnc_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11TFContratoSrvVnc_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PE2,100,0,true,false )
             ,new CursorDef("P00PE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PE3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoservicosvncfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoservicosvncfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoservicosvncfilterdata") )
          {
             return  ;
          }
          getpromptcontratoservicosvncfilterdata worker = new getpromptcontratoservicosvncfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
