/*
               File: PRC_UsadoDoContrato
        Description: Usado Do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:26.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usadodocontrato : GXProcedure
   {
      public prc_usadodocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usadodocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_CntCod ,
                           out decimal aP1_UsadoUnd )
      {
         this.A1603ContagemResultado_CntCod = aP0_ContagemResultado_CntCod;
         this.AV8UsadoUnd = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_CntCod=this.A1603ContagemResultado_CntCod;
         aP1_UsadoUnd=this.AV8UsadoUnd;
      }

      public decimal executeUdp( ref int aP0_ContagemResultado_CntCod )
      {
         this.A1603ContagemResultado_CntCod = aP0_ContagemResultado_CntCod;
         this.AV8UsadoUnd = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_CntCod=this.A1603ContagemResultado_CntCod;
         aP1_UsadoUnd=this.AV8UsadoUnd;
         return AV8UsadoUnd ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_CntCod ,
                                 out decimal aP1_UsadoUnd )
      {
         prc_usadodocontrato objprc_usadodocontrato;
         objprc_usadodocontrato = new prc_usadodocontrato();
         objprc_usadodocontrato.A1603ContagemResultado_CntCod = aP0_ContagemResultado_CntCod;
         objprc_usadodocontrato.AV8UsadoUnd = 0 ;
         objprc_usadodocontrato.context.SetSubmitInitialConfig(context);
         objprc_usadodocontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usadodocontrato);
         aP0_ContagemResultado_CntCod=this.A1603ContagemResultado_CntCod;
         aP1_UsadoUnd=this.AV8UsadoUnd;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usadodocontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WK2 */
         pr_default.execute(0, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00WK2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00WK2_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P00WK2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00WK2_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00WK2_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV8UsadoUnd = (decimal)(AV8UsadoUnd+A574ContagemResultado_PFFinal);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WK2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00WK2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00WK2_A1603ContagemResultado_CntCod = new int[1] ;
         P00WK2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00WK2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00WK2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00WK2_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usadodocontrato__default(),
            new Object[][] {
                new Object[] {
               P00WK2_A1553ContagemResultado_CntSrvCod, P00WK2_n1553ContagemResultado_CntSrvCod, P00WK2_A1603ContagemResultado_CntCod, P00WK2_n1603ContagemResultado_CntCod, P00WK2_A484ContagemResultado_StatusDmn, P00WK2_n484ContagemResultado_StatusDmn, P00WK2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1603ContagemResultado_CntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private decimal AV8UsadoUnd ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_CntCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00WK2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00WK2_n1553ContagemResultado_CntSrvCod ;
      private int[] P00WK2_A1603ContagemResultado_CntCod ;
      private bool[] P00WK2_n1603ContagemResultado_CntCod ;
      private String[] P00WK2_A484ContagemResultado_StatusDmn ;
      private bool[] P00WK2_n484ContagemResultado_StatusDmn ;
      private int[] P00WK2_A456ContagemResultado_Codigo ;
      private decimal aP1_UsadoUnd ;
   }

   public class prc_usadodocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WK2 ;
          prmP00WK2 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WK2", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T2.[Contrato_Codigo] = @ContagemResultado_CntCod) AND (T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L') ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WK2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
