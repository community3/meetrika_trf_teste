/*
               File: type_SdtAreaTrabalho_Guias
        Description: Area de Trabalho x Guias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 18:56:13.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AreaTrabalho_Guias" )]
   [XmlType(TypeName =  "AreaTrabalho_Guias" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtAreaTrabalho_Guias : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAreaTrabalho_Guias( )
      {
         /* Constructor for serialization */
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_nome = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_versao = "";
         gxTv_SdtAreaTrabalho_Guias_Mode = "";
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z = "";
      }

      public SdtAreaTrabalho_Guias( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV5AreaTrabalho_Codigo ,
                        int AV93Guia_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV5AreaTrabalho_Codigo,(int)AV93Guia_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"AreaTrabalho_Codigo", typeof(int)}, new Object[]{"Guia_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "AreaTrabalho_Guias");
         metadata.Set("BT", "AreaTrabalho_Guias");
         metadata.Set("PK", "[ \"AreaTrabalho_Codigo\",\"Guia_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Guia_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Guia_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Guia_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Guia_versao_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAreaTrabalho_Guias deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAreaTrabalho_Guias)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAreaTrabalho_Guias obj ;
         obj = this;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Areatrabalho_descricao = deserialized.gxTpr_Areatrabalho_descricao;
         obj.gxTpr_Guia_codigo = deserialized.gxTpr_Guia_codigo;
         obj.gxTpr_Guia_nome = deserialized.gxTpr_Guia_nome;
         obj.gxTpr_Guia_versao = deserialized.gxTpr_Guia_versao;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Areatrabalho_codigo_Z = deserialized.gxTpr_Areatrabalho_codigo_Z;
         obj.gxTpr_Areatrabalho_descricao_Z = deserialized.gxTpr_Areatrabalho_descricao_Z;
         obj.gxTpr_Guia_codigo_Z = deserialized.gxTpr_Guia_codigo_Z;
         obj.gxTpr_Guia_nome_Z = deserialized.gxTpr_Guia_nome_Z;
         obj.gxTpr_Guia_versao_Z = deserialized.gxTpr_Guia_versao_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Nome") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Versao") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_versao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao_Z") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Nome_Z") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Guia_Versao_Z") )
               {
                  gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "AreaTrabalho_Guias";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AreaTrabalho_Descricao", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Guia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Guias_Guia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Guia_Nome", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Guia_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Guia_Versao", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Guia_versao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Guias_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AreaTrabalho_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AreaTrabalho_Descricao_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Guia_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Guia_Nome_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Guia_Versao_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo, false);
         AddObjectProperty("AreaTrabalho_Descricao", gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao, false);
         AddObjectProperty("Guia_Codigo", gxTv_SdtAreaTrabalho_Guias_Guia_codigo, false);
         AddObjectProperty("Guia_Nome", gxTv_SdtAreaTrabalho_Guias_Guia_nome, false);
         AddObjectProperty("Guia_Versao", gxTv_SdtAreaTrabalho_Guias_Guia_versao, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAreaTrabalho_Guias_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAreaTrabalho_Guias_Initialized, false);
            AddObjectProperty("AreaTrabalho_Codigo_Z", gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z, false);
            AddObjectProperty("AreaTrabalho_Descricao_Z", gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z, false);
            AddObjectProperty("Guia_Codigo_Z", gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z, false);
            AddObjectProperty("Guia_Nome_Z", gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z, false);
            AddObjectProperty("Guia_Versao_Z", gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo ;
         }

         set {
            if ( gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo != value )
            {
               gxTv_SdtAreaTrabalho_Guias_Mode = "INS";
               this.gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z_SetNull( );
            }
            gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao"   )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Guia_Codigo" )]
      [  XmlElement( ElementName = "Guia_Codigo"   )]
      public int gxTpr_Guia_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_codigo ;
         }

         set {
            if ( gxTv_SdtAreaTrabalho_Guias_Guia_codigo != value )
            {
               gxTv_SdtAreaTrabalho_Guias_Mode = "INS";
               this.gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z_SetNull( );
            }
            gxTv_SdtAreaTrabalho_Guias_Guia_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Guia_Nome" )]
      [  XmlElement( ElementName = "Guia_Nome"   )]
      public String gxTpr_Guia_nome
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_nome ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Guia_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Guia_Versao" )]
      [  XmlElement( ElementName = "Guia_Versao"   )]
      public String gxTpr_Guia_versao
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_versao ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Guia_versao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Mode ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Mode_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Initialized ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Initialized_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo_Z"   )]
      public int gxTpr_Areatrabalho_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao_Z"   )]
      public String gxTpr_Areatrabalho_descricao_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Guia_Codigo_Z" )]
      [  XmlElement( ElementName = "Guia_Codigo_Z"   )]
      public int gxTpr_Guia_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Guia_Nome_Z" )]
      [  XmlElement( ElementName = "Guia_Nome_Z"   )]
      public String gxTpr_Guia_nome_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Guia_Versao_Z" )]
      [  XmlElement( ElementName = "Guia_Versao_Z"   )]
      public String gxTpr_Guia_versao_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_nome = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_versao = "";
         gxTv_SdtAreaTrabalho_Guias_Mode = "";
         gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z = "";
         gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "areatrabalho_guias", "GeneXus.Programs.areatrabalho_guias_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAreaTrabalho_Guias_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo ;
      private int gxTv_SdtAreaTrabalho_Guias_Guia_codigo ;
      private int gxTv_SdtAreaTrabalho_Guias_Areatrabalho_codigo_Z ;
      private int gxTv_SdtAreaTrabalho_Guias_Guia_codigo_Z ;
      private String gxTv_SdtAreaTrabalho_Guias_Guia_nome ;
      private String gxTv_SdtAreaTrabalho_Guias_Guia_versao ;
      private String gxTv_SdtAreaTrabalho_Guias_Mode ;
      private String gxTv_SdtAreaTrabalho_Guias_Guia_nome_Z ;
      private String gxTv_SdtAreaTrabalho_Guias_Guia_versao_Z ;
      private String sTagName ;
      private String gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao ;
      private String gxTv_SdtAreaTrabalho_Guias_Areatrabalho_descricao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"AreaTrabalho_Guias", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtAreaTrabalho_Guias_RESTInterface : GxGenericCollectionItem<SdtAreaTrabalho_Guias>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAreaTrabalho_Guias_RESTInterface( ) : base()
      {
      }

      public SdtAreaTrabalho_Guias_RESTInterface( SdtAreaTrabalho_Guias psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_Descricao" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return sdt.gxTpr_Areatrabalho_descricao ;
         }

         set {
            sdt.gxTpr_Areatrabalho_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Guia_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Guia_codigo
      {
         get {
            return sdt.gxTpr_Guia_codigo ;
         }

         set {
            sdt.gxTpr_Guia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Guia_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Guia_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Guia_nome) ;
         }

         set {
            sdt.gxTpr_Guia_nome = (String)(value);
         }

      }

      [DataMember( Name = "Guia_Versao" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Guia_versao
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Guia_versao) ;
         }

         set {
            sdt.gxTpr_Guia_versao = (String)(value);
         }

      }

      public SdtAreaTrabalho_Guias sdt
      {
         get {
            return (SdtAreaTrabalho_Guias)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAreaTrabalho_Guias() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 12 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
