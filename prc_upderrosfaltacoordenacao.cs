/*
               File: PRC_UpdErrosFaltaCoordenacao
        Description: Atualiza Erros Falta Coordenacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:13.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_upderrosfaltacoordenacao : GXProcedure
   {
      public prc_upderrosfaltacoordenacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_upderrosfaltacoordenacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_SistemaCod )
      {
         this.AV8ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         initialize();
         executePrivate();
         aP0_ContagemResultado_SistemaCod=this.AV8ContagemResultado_SistemaCod;
      }

      public int executeUdp( )
      {
         this.AV8ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         initialize();
         executePrivate();
         aP0_ContagemResultado_SistemaCod=this.AV8ContagemResultado_SistemaCod;
         return AV8ContagemResultado_SistemaCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_SistemaCod )
      {
         prc_upderrosfaltacoordenacao objprc_upderrosfaltacoordenacao;
         objprc_upderrosfaltacoordenacao = new prc_upderrosfaltacoordenacao();
         objprc_upderrosfaltacoordenacao.AV8ContagemResultado_SistemaCod = aP0_ContagemResultado_SistemaCod;
         objprc_upderrosfaltacoordenacao.context.SetSubmitInitialConfig(context);
         objprc_upderrosfaltacoordenacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_upderrosfaltacoordenacao);
         aP0_ContagemResultado_SistemaCod=this.AV8ContagemResultado_SistemaCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_upderrosfaltacoordenacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00482 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00482_A456ContagemResultado_Codigo[0];
            A581ContagemResultadoErro_Status = P00482_A581ContagemResultadoErro_Status[0];
            A579ContagemResultadoErro_Tipo = P00482_A579ContagemResultadoErro_Tipo[0];
            A489ContagemResultado_SistemaCod = P00482_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00482_n489ContagemResultado_SistemaCod[0];
            A489ContagemResultado_SistemaCod = P00482_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00482_n489ContagemResultado_SistemaCod[0];
            A581ContagemResultadoErro_Status = "C";
            BatchSize = 100;
            pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp00483");
            /* Using cursor P00483 */
            pr_default.addRecord(1, new Object[] {A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
            {
               Executebatchp00483( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            pr_default.readNext(0);
         }
         if ( pr_default.getBatchSize(1) > 0 )
         {
            Executebatchp00483( ) ;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp00483( )
      {
         /* Using cursor P00483 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdErrosFaltaCoordenacao");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00482_A456ContagemResultado_Codigo = new int[1] ;
         P00482_A581ContagemResultadoErro_Status = new String[] {""} ;
         P00482_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         P00482_A489ContagemResultado_SistemaCod = new int[1] ;
         P00482_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         A581ContagemResultadoErro_Status = "";
         A579ContagemResultadoErro_Tipo = "";
         P00483_A581ContagemResultadoErro_Status = new String[] {""} ;
         P00483_A456ContagemResultado_Codigo = new int[1] ;
         P00483_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_upderrosfaltacoordenacao__default(),
            new Object[][] {
                new Object[] {
               P00482_A456ContagemResultado_Codigo, P00482_A581ContagemResultadoErro_Status, P00482_A579ContagemResultadoErro_Tipo, P00482_A489ContagemResultado_SistemaCod, P00482_n489ContagemResultado_SistemaCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int BatchSize ;
      private String scmdbuf ;
      private String A581ContagemResultadoErro_Status ;
      private String A579ContagemResultadoErro_Tipo ;
      private bool n489ContagemResultado_SistemaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_SistemaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00482_A456ContagemResultado_Codigo ;
      private String[] P00482_A581ContagemResultadoErro_Status ;
      private String[] P00482_A579ContagemResultadoErro_Tipo ;
      private int[] P00482_A489ContagemResultado_SistemaCod ;
      private bool[] P00482_n489ContagemResultado_SistemaCod ;
      private String[] P00483_A581ContagemResultadoErro_Status ;
      private int[] P00483_A456ContagemResultado_Codigo ;
      private String[] P00483_A579ContagemResultadoErro_Tipo ;
   }

   public class prc_upderrosfaltacoordenacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00482 ;
          prmP00482 = new Object[] {
          new Object[] {"@AV8ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00483 ;
          prmP00483 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00482", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultadoErro_Status], T1.[ContagemResultadoErro_Tipo], T2.[ContagemResultado_SistemaCod] FROM ([ContagemResultadoErro] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T2.[ContagemResultado_SistemaCod] = @AV8ContagemResultado_SistemaCod) AND (T1.[ContagemResultadoErro_Tipo] = 'SC') AND (T1.[ContagemResultadoErro_Status] = 'P') ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultadoErro_Tipo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00482,1,0,true,false )
             ,new CursorDef("P00483", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00483)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
