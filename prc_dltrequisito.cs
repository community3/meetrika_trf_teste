/*
               File: PRC_DLTRequisito
        Description: PRC_DLTRequisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:56.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltrequisito : GXProcedure
   {
      public prc_dltrequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltrequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoRequisito_OSCod ,
                           ref int aP1_ContagemResultadoRequisito_ReqCod )
      {
         this.A2003ContagemResultadoRequisito_OSCod = aP0_ContagemResultadoRequisito_OSCod;
         this.A2004ContagemResultadoRequisito_ReqCod = aP1_ContagemResultadoRequisito_ReqCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoRequisito_OSCod=this.A2003ContagemResultadoRequisito_OSCod;
         aP1_ContagemResultadoRequisito_ReqCod=this.A2004ContagemResultadoRequisito_ReqCod;
      }

      public int executeUdp( ref int aP0_ContagemResultadoRequisito_OSCod )
      {
         this.A2003ContagemResultadoRequisito_OSCod = aP0_ContagemResultadoRequisito_OSCod;
         this.A2004ContagemResultadoRequisito_ReqCod = aP1_ContagemResultadoRequisito_ReqCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoRequisito_OSCod=this.A2003ContagemResultadoRequisito_OSCod;
         aP1_ContagemResultadoRequisito_ReqCod=this.A2004ContagemResultadoRequisito_ReqCod;
         return A2004ContagemResultadoRequisito_ReqCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoRequisito_OSCod ,
                                 ref int aP1_ContagemResultadoRequisito_ReqCod )
      {
         prc_dltrequisito objprc_dltrequisito;
         objprc_dltrequisito = new prc_dltrequisito();
         objprc_dltrequisito.A2003ContagemResultadoRequisito_OSCod = aP0_ContagemResultadoRequisito_OSCod;
         objprc_dltrequisito.A2004ContagemResultadoRequisito_ReqCod = aP1_ContagemResultadoRequisito_ReqCod;
         objprc_dltrequisito.context.SetSubmitInitialConfig(context);
         objprc_dltrequisito.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltrequisito);
         aP0_ContagemResultadoRequisito_OSCod=this.A2003ContagemResultadoRequisito_OSCod;
         aP1_ContagemResultadoRequisito_ReqCod=this.A2004ContagemResultadoRequisito_ReqCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltrequisito)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P00VR2 */
         pr_default.execute(0, new Object[] {A2004ContagemResultadoRequisito_ReqCod, A2003ContagemResultadoRequisito_OSCod});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
         /* End optimized DELETE. */
         /* Using cursor P00VR3 */
         pr_default.execute(1, new Object[] {A2004ContagemResultadoRequisito_ReqCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1919Requisito_Codigo = P00VR3_A1919Requisito_Codigo[0];
            A1931Requisito_Ordem = P00VR3_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00VR3_n1931Requisito_Ordem[0];
            AV9Requisito_Ordem = A1931Requisito_Ordem;
            /* Using cursor P00VR4 */
            pr_default.execute(2, new Object[] {A1919Requisito_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         new prc_alterarordem(context ).execute(  A2003ContagemResultadoRequisito_OSCod,  AV9Requisito_Ordem,  0,  "Req",  "-") ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DLTRequisito");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VR3_A1919Requisito_Codigo = new int[1] ;
         P00VR3_A1931Requisito_Ordem = new short[1] ;
         P00VR3_n1931Requisito_Ordem = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltrequisito__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               P00VR3_A1919Requisito_Codigo, P00VR3_A1931Requisito_Ordem, P00VR3_n1931Requisito_Ordem
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1931Requisito_Ordem ;
      private short AV9Requisito_Ordem ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A1919Requisito_Codigo ;
      private String scmdbuf ;
      private bool n1931Requisito_Ordem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoRequisito_OSCod ;
      private int aP1_ContagemResultadoRequisito_ReqCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00VR3_A1919Requisito_Codigo ;
      private short[] P00VR3_A1931Requisito_Ordem ;
      private bool[] P00VR3_n1931Requisito_Ordem ;
   }

   public class prc_dltrequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VR2 ;
          prmP00VR2 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VR3 ;
          prmP00VR3 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VR4 ;
          prmP00VR4 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VR2", "DELETE FROM [ContagemResultadoRequisito]  WHERE ([ContagemResultadoRequisito_ReqCod] = @ContagemResultadoRequisito_ReqCod) AND ([ContagemResultadoRequisito_OSCod] = @ContagemResultadoRequisito_OSCod)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VR2)
             ,new CursorDef("P00VR3", "SELECT TOP 1 [Requisito_Codigo], [Requisito_Ordem] FROM [Requisito] WITH (UPDLOCK) WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod ORDER BY [Requisito_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VR3,1,0,true,true )
             ,new CursorDef("P00VR4", "DELETE FROM [Requisito]  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VR4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
