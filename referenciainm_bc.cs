/*
               File: ReferenciaINM_BC
        Description: Referencia INM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:30.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciainm_bc : GXHttpHandler, IGxSilentTrn
   {
      public referenciainm_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public referenciainm_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2C90( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2C90( ) ;
         standaloneModal( ) ;
         AddRow2C90( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E112C2 */
            E112C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_2C0( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2C90( ) ;
            }
            else
            {
               CheckExtendedTable2C90( ) ;
               if ( AnyError == 0 )
               {
                  ZM2C90( 6) ;
               }
               CloseExtendedTableCursors2C90( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode90 = Gx_mode;
            CONFIRM_2C234( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode90;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode90;
         }
      }

      protected void CONFIRM_2C234( )
      {
         nGXsfl_234_idx = 0;
         while ( nGXsfl_234_idx < bcReferenciaINM.gxTpr_Anexos.Count )
         {
            ReadRow2C234( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound234 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_234 != 0 ) )
            {
               GetKey2C234( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound234 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate2C234( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable2C234( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM2C234( 8) ;
                        }
                        CloseExtendedTableCursors2C234( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound234 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey2C234( ) ;
                        Load2C234( ) ;
                        BeforeValidate2C234( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls2C234( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_234 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate2C234( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable2C234( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM2C234( 8) ;
                              }
                              CloseExtendedTableCursors2C234( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow234( ((SdtReferenciaINM_Anexos)bcReferenciaINM.gxTpr_Anexos.Item(nGXsfl_234_idx))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void E122C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ReferenciaINM_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
            }
         }
      }

      protected void E112C2( )
      {
         /* After Trn Routine */
      }

      protected void ZM2C90( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
            Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
            Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            Z2141ReferenciaINMAnexos_ArquivoNome = A2141ReferenciaINMAnexos_ArquivoNome;
            Z2140ReferenciaINMAnexos_ArquivoTipo = A2140ReferenciaINMAnexos_ArquivoTipo;
            Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z713ReferenciaINM_AreaTrabalhoDes = A713ReferenciaINM_AreaTrabalhoDes;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            Z2141ReferenciaINMAnexos_ArquivoNome = A2141ReferenciaINMAnexos_ArquivoNome;
            Z2140ReferenciaINMAnexos_ArquivoTipo = A2140ReferenciaINMAnexos_ArquivoTipo;
            Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( GX_JID == -5 )
         {
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
            Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
            Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            Z713ReferenciaINM_AreaTrabalhoDes = A713ReferenciaINM_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV13Pgmname = "ReferenciaINM_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A711ReferenciaINM_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A711ReferenciaINM_Ativo = true;
         }
      }

      protected void Load2C90( )
      {
         /* Using cursor BC002C8 */
         pr_default.execute(6, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound90 = 1;
            A710ReferenciaINM_Descricao = BC002C8_A710ReferenciaINM_Descricao[0];
            A713ReferenciaINM_AreaTrabalhoDes = BC002C8_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = BC002C8_n713ReferenciaINM_AreaTrabalhoDes[0];
            A711ReferenciaINM_Ativo = BC002C8_A711ReferenciaINM_Ativo[0];
            A712ReferenciaINM_AreaTrabalhoCod = BC002C8_A712ReferenciaINM_AreaTrabalhoCod[0];
            ZM2C90( -5) ;
         }
         pr_default.close(6);
         OnLoadActions2C90( ) ;
      }

      protected void OnLoadActions2C90( )
      {
      }

      protected void CheckExtendedTable2C90( )
      {
         standaloneModal( ) ;
         /* Using cursor BC002C7 */
         pr_default.execute(5, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Referencia INM_Area Trabalho'.", "ForeignKeyNotFound", 1, "REFERENCIAINM_AREATRABALHOCOD");
            AnyError = 1;
         }
         A713ReferenciaINM_AreaTrabalhoDes = BC002C7_A713ReferenciaINM_AreaTrabalhoDes[0];
         n713ReferenciaINM_AreaTrabalhoDes = BC002C7_n713ReferenciaINM_AreaTrabalhoDes[0];
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors2C90( )
      {
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2C90( )
      {
         /* Using cursor BC002C9 */
         pr_default.execute(7, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound90 = 1;
         }
         else
         {
            RcdFound90 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC002C6 */
         pr_default.execute(4, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM2C90( 5) ;
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = BC002C6_A709ReferenciaINM_Codigo[0];
            A710ReferenciaINM_Descricao = BC002C6_A710ReferenciaINM_Descricao[0];
            A711ReferenciaINM_Ativo = BC002C6_A711ReferenciaINM_Ativo[0];
            A712ReferenciaINM_AreaTrabalhoCod = BC002C6_A712ReferenciaINM_AreaTrabalhoCod[0];
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            sMode90 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2C90( ) ;
            if ( AnyError == 1 )
            {
               RcdFound90 = 0;
               InitializeNonKey2C90( ) ;
            }
            Gx_mode = sMode90;
         }
         else
         {
            RcdFound90 = 0;
            InitializeNonKey2C90( ) ;
            sMode90 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode90;
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey2C90( ) ;
         if ( RcdFound90 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_2C0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2C90( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC002C5 */
            pr_default.execute(3, new Object[] {A709ReferenciaINM_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINM"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z710ReferenciaINM_Descricao, BC002C5_A710ReferenciaINM_Descricao[0]) != 0 ) || ( Z711ReferenciaINM_Ativo != BC002C5_A711ReferenciaINM_Ativo[0] ) || ( Z712ReferenciaINM_AreaTrabalhoCod != BC002C5_A712ReferenciaINM_AreaTrabalhoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReferenciaINM"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2C90( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2C90( 0) ;
            CheckOptimisticConcurrency2C90( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C90( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2C90( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002C10 */
                     pr_default.execute(8, new Object[] {A710ReferenciaINM_Descricao, A711ReferenciaINM_Ativo, A712ReferenciaINM_AreaTrabalhoCod});
                     A709ReferenciaINM_Codigo = BC002C10_A709ReferenciaINM_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2C90( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2C90( ) ;
            }
            EndLevel2C90( ) ;
         }
         CloseExtendedTableCursors2C90( ) ;
      }

      protected void Update2C90( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C90( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C90( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2C90( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002C11 */
                     pr_default.execute(9, new Object[] {A710ReferenciaINM_Descricao, A711ReferenciaINM_Ativo, A712ReferenciaINM_AreaTrabalhoCod, A709ReferenciaINM_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINM"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2C90( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2C90( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2C90( ) ;
         }
         CloseExtendedTableCursors2C90( ) ;
      }

      protected void DeferredUpdate2C90( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2C90( ) ;
            AfterConfirm2C90( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2C90( ) ;
               if ( AnyError == 0 )
               {
                  ScanKeyStart2C234( ) ;
                  while ( RcdFound234 != 0 )
                  {
                     getByPrimaryKey2C234( ) ;
                     Delete2C234( ) ;
                     ScanKeyNext2C234( ) ;
                  }
                  ScanKeyEnd2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002C12 */
                     pr_default.execute(10, new Object[] {A709ReferenciaINM_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode90 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2C90( ) ;
         Gx_mode = sMode90;
      }

      protected void OnDeleteControls2C90( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002C13 */
            pr_default.execute(11, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
            A713ReferenciaINM_AreaTrabalhoDes = BC002C13_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = BC002C13_n713ReferenciaINM_AreaTrabalhoDes[0];
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC002C14 */
            pr_default.execute(12, new Object[] {A709ReferenciaINM_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item N�o Mensuravel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void ProcessNestedLevel2C234( )
      {
         nGXsfl_234_idx = 0;
         while ( nGXsfl_234_idx < bcReferenciaINM.gxTpr_Anexos.Count )
         {
            ReadRow2C234( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound234 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_234 != 0 ) )
            {
               standaloneNotModal2C234( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert2C234( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete2C234( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update2C234( ) ;
                  }
               }
            }
            KeyVarsToRow234( ((SdtReferenciaINM_Anexos)bcReferenciaINM.gxTpr_Anexos.Item(nGXsfl_234_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_234_idx = 0;
            while ( nGXsfl_234_idx < bcReferenciaINM.gxTpr_Anexos.Count )
            {
               ReadRow2C234( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound234 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcReferenciaINM.gxTpr_Anexos.RemoveElement(nGXsfl_234_idx);
                  nGXsfl_234_idx = (short)(nGXsfl_234_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey2C234( ) ;
                  VarsToRow234( ((SdtReferenciaINM_Anexos)bcReferenciaINM.gxTpr_Anexos.Item(nGXsfl_234_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll2C234( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_234 = 0;
         nIsMod_234 = 0;
         Gxremove234 = 0;
      }

      protected void ProcessLevel2C90( )
      {
         /* Save parent mode. */
         sMode90 = Gx_mode;
         ProcessNestedLevel2C234( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode90;
         /* ' Update level parameters */
      }

      protected void EndLevel2C90( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(3);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2C90( )
      {
         /* Scan By routine */
         /* Using cursor BC002C15 */
         pr_default.execute(13, new Object[] {A709ReferenciaINM_Codigo});
         RcdFound90 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = BC002C15_A709ReferenciaINM_Codigo[0];
            A710ReferenciaINM_Descricao = BC002C15_A710ReferenciaINM_Descricao[0];
            A713ReferenciaINM_AreaTrabalhoDes = BC002C15_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = BC002C15_n713ReferenciaINM_AreaTrabalhoDes[0];
            A711ReferenciaINM_Ativo = BC002C15_A711ReferenciaINM_Ativo[0];
            A712ReferenciaINM_AreaTrabalhoCod = BC002C15_A712ReferenciaINM_AreaTrabalhoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2C90( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound90 = 0;
         ScanKeyLoad2C90( ) ;
      }

      protected void ScanKeyLoad2C90( )
      {
         sMode90 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = BC002C15_A709ReferenciaINM_Codigo[0];
            A710ReferenciaINM_Descricao = BC002C15_A710ReferenciaINM_Descricao[0];
            A713ReferenciaINM_AreaTrabalhoDes = BC002C15_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = BC002C15_n713ReferenciaINM_AreaTrabalhoDes[0];
            A711ReferenciaINM_Ativo = BC002C15_A711ReferenciaINM_Ativo[0];
            A712ReferenciaINM_AreaTrabalhoCod = BC002C15_A712ReferenciaINM_AreaTrabalhoCod[0];
         }
         Gx_mode = sMode90;
      }

      protected void ScanKeyEnd2C90( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm2C90( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2C90( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2C90( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2C90( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2C90( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2C90( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2C90( )
      {
      }

      protected void ZM2C234( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
            Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            Z713ReferenciaINM_AreaTrabalhoDes = A713ReferenciaINM_AreaTrabalhoDes;
            Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
            Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            Z713ReferenciaINM_AreaTrabalhoDes = A713ReferenciaINM_AreaTrabalhoDes;
            Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
         }
         if ( GX_JID == -7 )
         {
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            Z2138ReferenciaINMAnexos_Descricao = A2138ReferenciaINMAnexos_Descricao;
            Z2139ReferenciaINMAnexos_Arquivo = A2139ReferenciaINMAnexos_Arquivo;
            Z2143ReferenciaINMAnexos_Link = A2143ReferenciaINMAnexos_Link;
            Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
            Z2140ReferenciaINMAnexos_ArquivoTipo = A2140ReferenciaINMAnexos_ArquivoTipo;
            Z2141ReferenciaINMAnexos_ArquivoNome = A2141ReferenciaINMAnexos_ArquivoNome;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal2C234( )
      {
      }

      protected void standaloneModal2C234( )
      {
      }

      protected void Load2C234( )
      {
         /* Using cursor BC002C16 */
         pr_default.execute(14, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound234 = 1;
            A2138ReferenciaINMAnexos_Descricao = BC002C16_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = BC002C16_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = BC002C16_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = BC002C16_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = BC002C16_n2142ReferenciaINMAnexos_Data[0];
            A646TipoDocumento_Nome = BC002C16_A646TipoDocumento_Nome[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = BC002C16_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = BC002C16_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = BC002C16_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = BC002C16_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002C16_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002C16_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = BC002C16_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = BC002C16_n2139ReferenciaINMAnexos_Arquivo[0];
            ZM2C234( -7) ;
         }
         pr_default.close(14);
         OnLoadActions2C234( ) ;
      }

      protected void OnLoadActions2C234( )
      {
      }

      protected void CheckExtendedTable2C234( )
      {
         Gx_BScreen = 1;
         standaloneModal2C234( ) ;
         Gx_BScreen = 0;
         if ( ! ( (DateTime.MinValue==A2142ReferenciaINMAnexos_Data) || ( A2142ReferenciaINMAnexos_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC002C4 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC002C4_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2C234( )
      {
         pr_default.close(2);
      }

      protected void enableDisable2C234( )
      {
      }

      protected void GetKey2C234( )
      {
         /* Using cursor BC002C17 */
         pr_default.execute(15, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound234 = 1;
         }
         else
         {
            RcdFound234 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey2C234( )
      {
         /* Using cursor BC002C3 */
         pr_default.execute(1, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2C234( 7) ;
            RcdFound234 = 1;
            InitializeNonKey2C234( ) ;
            A2134ReferenciaINMAnexos_Codigo = BC002C3_A2134ReferenciaINMAnexos_Codigo[0];
            A2138ReferenciaINMAnexos_Descricao = BC002C3_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = BC002C3_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = BC002C3_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = BC002C3_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = BC002C3_n2142ReferenciaINMAnexos_Data[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = BC002C3_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = BC002C3_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = BC002C3_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = BC002C3_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002C3_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002C3_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = BC002C3_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = BC002C3_n2139ReferenciaINMAnexos_Arquivo[0];
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            sMode234 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal2C234( ) ;
            Load2C234( ) ;
            Gx_mode = sMode234;
         }
         else
         {
            RcdFound234 = 0;
            InitializeNonKey2C234( ) ;
            sMode234 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal2C234( ) ;
            Gx_mode = sMode234;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes2C234( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency2C234( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC002C2 */
            pr_default.execute(0, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINMAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2142ReferenciaINMAnexos_Data != BC002C2_A2142ReferenciaINMAnexos_Data[0] ) || ( Z645TipoDocumento_Codigo != BC002C2_A645TipoDocumento_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReferenciaINMAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2C234( )
      {
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2C234( 0) ;
            CheckOptimisticConcurrency2C234( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C234( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002C18 */
                     pr_default.execute(16, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo, A2138ReferenciaINMAnexos_Descricao, n2139ReferenciaINMAnexos_Arquivo, A2139ReferenciaINMAnexos_Arquivo, n2143ReferenciaINMAnexos_Link, A2143ReferenciaINMAnexos_Link, n2142ReferenciaINMAnexos_Data, A2142ReferenciaINMAnexos_Data, n2140ReferenciaINMAnexos_ArquivoTipo, A2140ReferenciaINMAnexos_ArquivoTipo, n2141ReferenciaINMAnexos_ArquivoNome, A2141ReferenciaINMAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2C234( ) ;
            }
            EndLevel2C234( ) ;
         }
         CloseExtendedTableCursors2C234( ) ;
      }

      protected void Update2C234( )
      {
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C234( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C234( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002C19 */
                     pr_default.execute(17, new Object[] {A2138ReferenciaINMAnexos_Descricao, n2143ReferenciaINMAnexos_Link, A2143ReferenciaINMAnexos_Link, n2142ReferenciaINMAnexos_Data, A2142ReferenciaINMAnexos_Data, n2140ReferenciaINMAnexos_ArquivoTipo, A2140ReferenciaINMAnexos_ArquivoTipo, n2141ReferenciaINMAnexos_ArquivoNome, A2141ReferenciaINMAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINMAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2C234( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey2C234( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2C234( ) ;
         }
         CloseExtendedTableCursors2C234( ) ;
      }

      protected void DeferredUpdate2C234( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC002C20 */
            pr_default.execute(18, new Object[] {n2139ReferenciaINMAnexos_Arquivo, A2139ReferenciaINMAnexos_Arquivo, A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
            pr_default.close(18);
            dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
         }
      }

      protected void Delete2C234( )
      {
         Gx_mode = "DLT";
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2C234( ) ;
            AfterConfirm2C234( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2C234( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002C21 */
                  pr_default.execute(19, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
                  pr_default.close(19);
                  dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode234 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2C234( ) ;
         Gx_mode = sMode234;
      }

      protected void OnDeleteControls2C234( )
      {
         standaloneModal2C234( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002C22 */
            pr_default.execute(20, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC002C22_A646TipoDocumento_Nome[0];
            pr_default.close(20);
         }
      }

      protected void EndLevel2C234( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2C234( )
      {
         /* Scan By routine */
         /* Using cursor BC002C23 */
         pr_default.execute(21, new Object[] {A709ReferenciaINM_Codigo});
         RcdFound234 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound234 = 1;
            A2134ReferenciaINMAnexos_Codigo = BC002C23_A2134ReferenciaINMAnexos_Codigo[0];
            A2138ReferenciaINMAnexos_Descricao = BC002C23_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = BC002C23_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = BC002C23_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = BC002C23_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = BC002C23_n2142ReferenciaINMAnexos_Data[0];
            A646TipoDocumento_Nome = BC002C23_A646TipoDocumento_Nome[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = BC002C23_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = BC002C23_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = BC002C23_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = BC002C23_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002C23_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002C23_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = BC002C23_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = BC002C23_n2139ReferenciaINMAnexos_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2C234( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound234 = 0;
         ScanKeyLoad2C234( ) ;
      }

      protected void ScanKeyLoad2C234( )
      {
         sMode234 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound234 = 1;
            A2134ReferenciaINMAnexos_Codigo = BC002C23_A2134ReferenciaINMAnexos_Codigo[0];
            A2138ReferenciaINMAnexos_Descricao = BC002C23_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = BC002C23_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = BC002C23_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = BC002C23_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = BC002C23_n2142ReferenciaINMAnexos_Data[0];
            A646TipoDocumento_Nome = BC002C23_A646TipoDocumento_Nome[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = BC002C23_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = BC002C23_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = BC002C23_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = BC002C23_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002C23_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002C23_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = BC002C23_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = BC002C23_n2139ReferenciaINMAnexos_Arquivo[0];
         }
         Gx_mode = sMode234;
      }

      protected void ScanKeyEnd2C234( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm2C234( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2C234( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2C234( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2C234( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2C234( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2C234( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2C234( )
      {
      }

      protected void AddRow2C90( )
      {
         VarsToRow90( bcReferenciaINM) ;
      }

      protected void ReadRow2C90( )
      {
         RowToVars90( bcReferenciaINM, 1) ;
      }

      protected void AddRow2C234( )
      {
         SdtReferenciaINM_Anexos obj234 ;
         obj234 = new SdtReferenciaINM_Anexos(context);
         VarsToRow234( obj234) ;
         bcReferenciaINM.gxTpr_Anexos.Add(obj234, 0);
         obj234.gxTpr_Mode = "UPD";
         obj234.gxTpr_Modified = 0;
      }

      protected void ReadRow2C234( )
      {
         nGXsfl_234_idx = (short)(nGXsfl_234_idx+1);
         RowToVars234( ((SdtReferenciaINM_Anexos)bcReferenciaINM.gxTpr_Anexos.Item(nGXsfl_234_idx)), 1) ;
      }

      protected void InitializeNonKey2C90( )
      {
         A710ReferenciaINM_Descricao = "";
         A712ReferenciaINM_AreaTrabalhoCod = 0;
         A713ReferenciaINM_AreaTrabalhoDes = "";
         n713ReferenciaINM_AreaTrabalhoDes = false;
         A711ReferenciaINM_Ativo = true;
         Z710ReferenciaINM_Descricao = "";
         Z711ReferenciaINM_Ativo = false;
         Z712ReferenciaINM_AreaTrabalhoCod = 0;
      }

      protected void InitAll2C90( )
      {
         A709ReferenciaINM_Codigo = 0;
         InitializeNonKey2C90( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A711ReferenciaINM_Ativo = i711ReferenciaINM_Ativo;
      }

      protected void InitializeNonKey2C234( )
      {
         A2138ReferenciaINMAnexos_Descricao = "";
         A2139ReferenciaINMAnexos_Arquivo = "";
         n2139ReferenciaINMAnexos_Arquivo = false;
         A2143ReferenciaINMAnexos_Link = "";
         n2143ReferenciaINMAnexos_Link = false;
         A2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         n2142ReferenciaINMAnexos_Data = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A2140ReferenciaINMAnexos_ArquivoTipo = "";
         n2140ReferenciaINMAnexos_ArquivoTipo = false;
         A2141ReferenciaINMAnexos_ArquivoNome = "";
         n2141ReferenciaINMAnexos_ArquivoNome = false;
         Z2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2C234( )
      {
         A2134ReferenciaINMAnexos_Codigo = 0;
         InitializeNonKey2C234( ) ;
      }

      protected void StandaloneModalInsert2C234( )
      {
      }

      public void VarsToRow90( SdtReferenciaINM obj90 )
      {
         obj90.gxTpr_Mode = Gx_mode;
         obj90.gxTpr_Referenciainm_descricao = A710ReferenciaINM_Descricao;
         obj90.gxTpr_Referenciainm_areatrabalhocod = A712ReferenciaINM_AreaTrabalhoCod;
         obj90.gxTpr_Referenciainm_areatrabalhodes = A713ReferenciaINM_AreaTrabalhoDes;
         obj90.gxTpr_Referenciainm_ativo = A711ReferenciaINM_Ativo;
         obj90.gxTpr_Referenciainm_codigo = A709ReferenciaINM_Codigo;
         obj90.gxTpr_Referenciainm_codigo_Z = Z709ReferenciaINM_Codigo;
         obj90.gxTpr_Referenciainm_descricao_Z = Z710ReferenciaINM_Descricao;
         obj90.gxTpr_Referenciainm_areatrabalhocod_Z = Z712ReferenciaINM_AreaTrabalhoCod;
         obj90.gxTpr_Referenciainm_areatrabalhodes_Z = Z713ReferenciaINM_AreaTrabalhoDes;
         obj90.gxTpr_Referenciainm_ativo_Z = Z711ReferenciaINM_Ativo;
         obj90.gxTpr_Referenciainm_areatrabalhodes_N = (short)(Convert.ToInt16(n713ReferenciaINM_AreaTrabalhoDes));
         obj90.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow90( SdtReferenciaINM obj90 )
      {
         obj90.gxTpr_Referenciainm_codigo = A709ReferenciaINM_Codigo;
         return  ;
      }

      public void RowToVars90( SdtReferenciaINM obj90 ,
                               int forceLoad )
      {
         Gx_mode = obj90.gxTpr_Mode;
         A710ReferenciaINM_Descricao = obj90.gxTpr_Referenciainm_descricao;
         A712ReferenciaINM_AreaTrabalhoCod = obj90.gxTpr_Referenciainm_areatrabalhocod;
         A713ReferenciaINM_AreaTrabalhoDes = obj90.gxTpr_Referenciainm_areatrabalhodes;
         n713ReferenciaINM_AreaTrabalhoDes = false;
         A711ReferenciaINM_Ativo = obj90.gxTpr_Referenciainm_ativo;
         A709ReferenciaINM_Codigo = obj90.gxTpr_Referenciainm_codigo;
         Z709ReferenciaINM_Codigo = obj90.gxTpr_Referenciainm_codigo_Z;
         Z710ReferenciaINM_Descricao = obj90.gxTpr_Referenciainm_descricao_Z;
         Z712ReferenciaINM_AreaTrabalhoCod = obj90.gxTpr_Referenciainm_areatrabalhocod_Z;
         Z713ReferenciaINM_AreaTrabalhoDes = obj90.gxTpr_Referenciainm_areatrabalhodes_Z;
         Z711ReferenciaINM_Ativo = obj90.gxTpr_Referenciainm_ativo_Z;
         n713ReferenciaINM_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj90.gxTpr_Referenciainm_areatrabalhodes_N));
         Gx_mode = obj90.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow234( SdtReferenciaINM_Anexos obj234 )
      {
         obj234.gxTpr_Mode = Gx_mode;
         obj234.gxTpr_Referenciainmanexos_descricao = A2138ReferenciaINMAnexos_Descricao;
         obj234.gxTpr_Referenciainmanexos_arquivo = A2139ReferenciaINMAnexos_Arquivo;
         obj234.gxTpr_Referenciainmanexos_link = A2143ReferenciaINMAnexos_Link;
         obj234.gxTpr_Referenciainmanexos_data = A2142ReferenciaINMAnexos_Data;
         obj234.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj234.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj234.gxTpr_Referenciainmanexos_arquivotipo = A2140ReferenciaINMAnexos_ArquivoTipo;
         obj234.gxTpr_Referenciainmanexos_arquivonome = A2141ReferenciaINMAnexos_ArquivoNome;
         obj234.gxTpr_Referenciainmanexos_codigo = A2134ReferenciaINMAnexos_Codigo;
         obj234.gxTpr_Referenciainmanexos_codigo_Z = Z2134ReferenciaINMAnexos_Codigo;
         obj234.gxTpr_Referenciainmanexos_arquivonome_Z = Z2141ReferenciaINMAnexos_ArquivoNome;
         obj234.gxTpr_Referenciainmanexos_arquivotipo_Z = Z2140ReferenciaINMAnexos_ArquivoTipo;
         obj234.gxTpr_Referenciainmanexos_data_Z = Z2142ReferenciaINMAnexos_Data;
         obj234.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj234.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj234.gxTpr_Referenciainmanexos_arquivo_N = (short)(Convert.ToInt16(n2139ReferenciaINMAnexos_Arquivo));
         obj234.gxTpr_Referenciainmanexos_arquivonome_N = (short)(Convert.ToInt16(n2141ReferenciaINMAnexos_ArquivoNome));
         obj234.gxTpr_Referenciainmanexos_arquivotipo_N = (short)(Convert.ToInt16(n2140ReferenciaINMAnexos_ArquivoTipo));
         obj234.gxTpr_Referenciainmanexos_link_N = (short)(Convert.ToInt16(n2143ReferenciaINMAnexos_Link));
         obj234.gxTpr_Referenciainmanexos_data_N = (short)(Convert.ToInt16(n2142ReferenciaINMAnexos_Data));
         obj234.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj234.gxTpr_Modified = nIsMod_234;
         return  ;
      }

      public void KeyVarsToRow234( SdtReferenciaINM_Anexos obj234 )
      {
         obj234.gxTpr_Referenciainmanexos_codigo = A2134ReferenciaINMAnexos_Codigo;
         return  ;
      }

      public void RowToVars234( SdtReferenciaINM_Anexos obj234 ,
                                int forceLoad )
      {
         Gx_mode = obj234.gxTpr_Mode;
         A2138ReferenciaINMAnexos_Descricao = obj234.gxTpr_Referenciainmanexos_descricao;
         A2139ReferenciaINMAnexos_Arquivo = obj234.gxTpr_Referenciainmanexos_arquivo;
         n2139ReferenciaINMAnexos_Arquivo = false;
         A2143ReferenciaINMAnexos_Link = obj234.gxTpr_Referenciainmanexos_link;
         n2143ReferenciaINMAnexos_Link = false;
         A2142ReferenciaINMAnexos_Data = obj234.gxTpr_Referenciainmanexos_data;
         n2142ReferenciaINMAnexos_Data = false;
         A645TipoDocumento_Codigo = obj234.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj234.gxTpr_Tipodocumento_nome;
         A2140ReferenciaINMAnexos_ArquivoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( obj234.gxTpr_Referenciainmanexos_arquivotipo)) ? FileUtil.GetFileType( A2139ReferenciaINMAnexos_Arquivo) : obj234.gxTpr_Referenciainmanexos_arquivotipo);
         n2140ReferenciaINMAnexos_ArquivoTipo = false;
         A2141ReferenciaINMAnexos_ArquivoNome = (String.IsNullOrEmpty(StringUtil.RTrim( obj234.gxTpr_Referenciainmanexos_arquivonome)) ? FileUtil.GetFileName( A2139ReferenciaINMAnexos_Arquivo) : obj234.gxTpr_Referenciainmanexos_arquivonome);
         n2141ReferenciaINMAnexos_ArquivoNome = false;
         A2134ReferenciaINMAnexos_Codigo = obj234.gxTpr_Referenciainmanexos_codigo;
         Z2134ReferenciaINMAnexos_Codigo = obj234.gxTpr_Referenciainmanexos_codigo_Z;
         Z2141ReferenciaINMAnexos_ArquivoNome = obj234.gxTpr_Referenciainmanexos_arquivonome_Z;
         Z2140ReferenciaINMAnexos_ArquivoTipo = obj234.gxTpr_Referenciainmanexos_arquivotipo_Z;
         Z2142ReferenciaINMAnexos_Data = obj234.gxTpr_Referenciainmanexos_data_Z;
         Z645TipoDocumento_Codigo = obj234.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj234.gxTpr_Tipodocumento_nome_Z;
         n2139ReferenciaINMAnexos_Arquivo = (bool)(Convert.ToBoolean(obj234.gxTpr_Referenciainmanexos_arquivo_N));
         n2141ReferenciaINMAnexos_ArquivoNome = (bool)(Convert.ToBoolean(obj234.gxTpr_Referenciainmanexos_arquivonome_N));
         n2140ReferenciaINMAnexos_ArquivoTipo = (bool)(Convert.ToBoolean(obj234.gxTpr_Referenciainmanexos_arquivotipo_N));
         n2143ReferenciaINMAnexos_Link = (bool)(Convert.ToBoolean(obj234.gxTpr_Referenciainmanexos_link_N));
         n2142ReferenciaINMAnexos_Data = (bool)(Convert.ToBoolean(obj234.gxTpr_Referenciainmanexos_data_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj234.gxTpr_Tipodocumento_codigo_N));
         nIsMod_234 = obj234.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A709ReferenciaINM_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2C90( ) ;
         ScanKeyStart2C90( ) ;
         if ( RcdFound90 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
         }
         ZM2C90( -5) ;
         OnLoadActions2C90( ) ;
         AddRow2C90( ) ;
         bcReferenciaINM.gxTpr_Anexos.ClearCollection();
         if ( RcdFound90 == 1 )
         {
            ScanKeyStart2C234( ) ;
            nGXsfl_234_idx = 1;
            while ( RcdFound234 != 0 )
            {
               Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
               Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
               ZM2C234( -7) ;
               OnLoadActions2C234( ) ;
               nRcdExists_234 = 1;
               nIsMod_234 = 0;
               AddRow2C234( ) ;
               nGXsfl_234_idx = (short)(nGXsfl_234_idx+1);
               ScanKeyNext2C234( ) ;
            }
            ScanKeyEnd2C234( ) ;
         }
         ScanKeyEnd2C90( ) ;
         if ( RcdFound90 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars90( bcReferenciaINM, 0) ;
         ScanKeyStart2C90( ) ;
         if ( RcdFound90 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
         }
         ZM2C90( -5) ;
         OnLoadActions2C90( ) ;
         AddRow2C90( ) ;
         bcReferenciaINM.gxTpr_Anexos.ClearCollection();
         if ( RcdFound90 == 1 )
         {
            ScanKeyStart2C234( ) ;
            nGXsfl_234_idx = 1;
            while ( RcdFound234 != 0 )
            {
               Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
               Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
               ZM2C234( -7) ;
               OnLoadActions2C234( ) ;
               nRcdExists_234 = 1;
               nIsMod_234 = 0;
               AddRow2C234( ) ;
               nGXsfl_234_idx = (short)(nGXsfl_234_idx+1);
               ScanKeyNext2C234( ) ;
            }
            ScanKeyEnd2C234( ) ;
         }
         ScanKeyEnd2C90( ) ;
         if ( RcdFound90 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars90( bcReferenciaINM, 0) ;
         nKeyPressed = 1;
         GetKey2C90( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2C90( ) ;
         }
         else
         {
            if ( RcdFound90 == 1 )
            {
               if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
               {
                  A709ReferenciaINM_Codigo = Z709ReferenciaINM_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2C90( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2C90( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2C90( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow90( bcReferenciaINM) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars90( bcReferenciaINM, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2C90( ) ;
         if ( RcdFound90 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
            {
               A709ReferenciaINM_Codigo = Z709ReferenciaINM_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(4);
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(20);
         context.RollbackDataStores( "ReferenciaINM_BC");
         VarsToRow90( bcReferenciaINM) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcReferenciaINM.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcReferenciaINM.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcReferenciaINM )
         {
            bcReferenciaINM = (SdtReferenciaINM)(sdt);
            if ( StringUtil.StrCmp(bcReferenciaINM.gxTpr_Mode, "") == 0 )
            {
               bcReferenciaINM.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow90( bcReferenciaINM) ;
            }
            else
            {
               RowToVars90( bcReferenciaINM, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcReferenciaINM.gxTpr_Mode, "") == 0 )
            {
               bcReferenciaINM.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars90( bcReferenciaINM, 1) ;
         return  ;
      }

      public SdtReferenciaINM ReferenciaINM_BC
      {
         get {
            return bcReferenciaINM ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(20);
         pr_default.close(4);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode90 = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z710ReferenciaINM_Descricao = "";
         A710ReferenciaINM_Descricao = "";
         Z2141ReferenciaINMAnexos_ArquivoNome = "";
         A2141ReferenciaINMAnexos_ArquivoNome = "";
         Z2140ReferenciaINMAnexos_ArquivoTipo = "";
         A2140ReferenciaINMAnexos_ArquivoTipo = "";
         Z2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         A2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z713ReferenciaINM_AreaTrabalhoDes = "";
         A713ReferenciaINM_AreaTrabalhoDes = "";
         BC002C8_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C8_A710ReferenciaINM_Descricao = new String[] {""} ;
         BC002C8_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         BC002C8_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         BC002C8_A711ReferenciaINM_Ativo = new bool[] {false} ;
         BC002C8_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         BC002C7_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         BC002C7_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         BC002C9_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C6_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C6_A710ReferenciaINM_Descricao = new String[] {""} ;
         BC002C6_A711ReferenciaINM_Ativo = new bool[] {false} ;
         BC002C6_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         BC002C5_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C5_A710ReferenciaINM_Descricao = new String[] {""} ;
         BC002C5_A711ReferenciaINM_Ativo = new bool[] {false} ;
         BC002C5_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         BC002C10_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C13_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         BC002C13_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         BC002C14_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         BC002C14_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         BC002C15_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C15_A710ReferenciaINM_Descricao = new String[] {""} ;
         BC002C15_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         BC002C15_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         BC002C15_A711ReferenciaINM_Ativo = new bool[] {false} ;
         BC002C15_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         Z2138ReferenciaINMAnexos_Descricao = "";
         A2138ReferenciaINMAnexos_Descricao = "";
         Z2139ReferenciaINMAnexos_Arquivo = "";
         A2139ReferenciaINMAnexos_Arquivo = "";
         Z2143ReferenciaINMAnexos_Link = "";
         A2143ReferenciaINMAnexos_Link = "";
         BC002C16_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C16_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         BC002C16_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         BC002C16_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         BC002C16_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         BC002C16_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002C16_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         BC002C16_A646TipoDocumento_Nome = new String[] {""} ;
         BC002C16_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         BC002C16_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         BC002C16_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         BC002C16_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         BC002C16_A645TipoDocumento_Codigo = new int[1] ;
         BC002C16_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002C16_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         BC002C16_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         A2139ReferenciaINMAnexos_Arquivo_Filetype = "";
         A2139ReferenciaINMAnexos_Arquivo_Filename = "";
         BC002C4_A646TipoDocumento_Nome = new String[] {""} ;
         BC002C17_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C17_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         BC002C3_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C3_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         BC002C3_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         BC002C3_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         BC002C3_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         BC002C3_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002C3_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         BC002C3_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         BC002C3_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         BC002C3_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         BC002C3_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         BC002C3_A645TipoDocumento_Codigo = new int[1] ;
         BC002C3_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002C3_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         BC002C3_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         sMode234 = "";
         BC002C2_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C2_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         BC002C2_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         BC002C2_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         BC002C2_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         BC002C2_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002C2_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         BC002C2_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         BC002C2_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         BC002C2_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         BC002C2_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         BC002C2_A645TipoDocumento_Codigo = new int[1] ;
         BC002C2_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002C2_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         BC002C2_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         BC002C22_A646TipoDocumento_Nome = new String[] {""} ;
         BC002C23_A709ReferenciaINM_Codigo = new int[1] ;
         BC002C23_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         BC002C23_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         BC002C23_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         BC002C23_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         BC002C23_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002C23_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         BC002C23_A646TipoDocumento_Nome = new String[] {""} ;
         BC002C23_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         BC002C23_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         BC002C23_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         BC002C23_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         BC002C23_A645TipoDocumento_Codigo = new int[1] ;
         BC002C23_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002C23_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         BC002C23_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciainm_bc__default(),
            new Object[][] {
                new Object[] {
               BC002C2_A709ReferenciaINM_Codigo, BC002C2_A2134ReferenciaINMAnexos_Codigo, BC002C2_A2138ReferenciaINMAnexos_Descricao, BC002C2_A2143ReferenciaINMAnexos_Link, BC002C2_n2143ReferenciaINMAnexos_Link, BC002C2_A2142ReferenciaINMAnexos_Data, BC002C2_n2142ReferenciaINMAnexos_Data, BC002C2_A2140ReferenciaINMAnexos_ArquivoTipo, BC002C2_n2140ReferenciaINMAnexos_ArquivoTipo, BC002C2_A2141ReferenciaINMAnexos_ArquivoNome,
               BC002C2_n2141ReferenciaINMAnexos_ArquivoNome, BC002C2_A645TipoDocumento_Codigo, BC002C2_n645TipoDocumento_Codigo, BC002C2_A2139ReferenciaINMAnexos_Arquivo, BC002C2_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               BC002C3_A709ReferenciaINM_Codigo, BC002C3_A2134ReferenciaINMAnexos_Codigo, BC002C3_A2138ReferenciaINMAnexos_Descricao, BC002C3_A2143ReferenciaINMAnexos_Link, BC002C3_n2143ReferenciaINMAnexos_Link, BC002C3_A2142ReferenciaINMAnexos_Data, BC002C3_n2142ReferenciaINMAnexos_Data, BC002C3_A2140ReferenciaINMAnexos_ArquivoTipo, BC002C3_n2140ReferenciaINMAnexos_ArquivoTipo, BC002C3_A2141ReferenciaINMAnexos_ArquivoNome,
               BC002C3_n2141ReferenciaINMAnexos_ArquivoNome, BC002C3_A645TipoDocumento_Codigo, BC002C3_n645TipoDocumento_Codigo, BC002C3_A2139ReferenciaINMAnexos_Arquivo, BC002C3_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               BC002C4_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002C5_A709ReferenciaINM_Codigo, BC002C5_A710ReferenciaINM_Descricao, BC002C5_A711ReferenciaINM_Ativo, BC002C5_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               BC002C6_A709ReferenciaINM_Codigo, BC002C6_A710ReferenciaINM_Descricao, BC002C6_A711ReferenciaINM_Ativo, BC002C6_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               BC002C7_A713ReferenciaINM_AreaTrabalhoDes, BC002C7_n713ReferenciaINM_AreaTrabalhoDes
               }
               , new Object[] {
               BC002C8_A709ReferenciaINM_Codigo, BC002C8_A710ReferenciaINM_Descricao, BC002C8_A713ReferenciaINM_AreaTrabalhoDes, BC002C8_n713ReferenciaINM_AreaTrabalhoDes, BC002C8_A711ReferenciaINM_Ativo, BC002C8_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               BC002C9_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               BC002C10_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002C13_A713ReferenciaINM_AreaTrabalhoDes, BC002C13_n713ReferenciaINM_AreaTrabalhoDes
               }
               , new Object[] {
               BC002C14_A718ItemNaoMensuravel_AreaTrabalhoCod, BC002C14_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               BC002C15_A709ReferenciaINM_Codigo, BC002C15_A710ReferenciaINM_Descricao, BC002C15_A713ReferenciaINM_AreaTrabalhoDes, BC002C15_n713ReferenciaINM_AreaTrabalhoDes, BC002C15_A711ReferenciaINM_Ativo, BC002C15_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               BC002C16_A709ReferenciaINM_Codigo, BC002C16_A2134ReferenciaINMAnexos_Codigo, BC002C16_A2138ReferenciaINMAnexos_Descricao, BC002C16_A2143ReferenciaINMAnexos_Link, BC002C16_n2143ReferenciaINMAnexos_Link, BC002C16_A2142ReferenciaINMAnexos_Data, BC002C16_n2142ReferenciaINMAnexos_Data, BC002C16_A646TipoDocumento_Nome, BC002C16_A2140ReferenciaINMAnexos_ArquivoTipo, BC002C16_n2140ReferenciaINMAnexos_ArquivoTipo,
               BC002C16_A2141ReferenciaINMAnexos_ArquivoNome, BC002C16_n2141ReferenciaINMAnexos_ArquivoNome, BC002C16_A645TipoDocumento_Codigo, BC002C16_n645TipoDocumento_Codigo, BC002C16_A2139ReferenciaINMAnexos_Arquivo, BC002C16_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               BC002C17_A709ReferenciaINM_Codigo, BC002C17_A2134ReferenciaINMAnexos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002C22_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002C23_A709ReferenciaINM_Codigo, BC002C23_A2134ReferenciaINMAnexos_Codigo, BC002C23_A2138ReferenciaINMAnexos_Descricao, BC002C23_A2143ReferenciaINMAnexos_Link, BC002C23_n2143ReferenciaINMAnexos_Link, BC002C23_A2142ReferenciaINMAnexos_Data, BC002C23_n2142ReferenciaINMAnexos_Data, BC002C23_A646TipoDocumento_Nome, BC002C23_A2140ReferenciaINMAnexos_ArquivoTipo, BC002C23_n2140ReferenciaINMAnexos_ArquivoTipo,
               BC002C23_A2141ReferenciaINMAnexos_ArquivoNome, BC002C23_n2141ReferenciaINMAnexos_ArquivoNome, BC002C23_A645TipoDocumento_Codigo, BC002C23_n645TipoDocumento_Codigo, BC002C23_A2139ReferenciaINMAnexos_Arquivo, BC002C23_n2139ReferenciaINMAnexos_Arquivo
               }
            }
         );
         Z711ReferenciaINM_Ativo = true;
         A711ReferenciaINM_Ativo = true;
         i711ReferenciaINM_Ativo = true;
         AV13Pgmname = "ReferenciaINM_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E122C2 */
         E122C2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short nGXsfl_234_idx=1 ;
      private short nIsMod_234 ;
      private short RcdFound234 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound90 ;
      private short nRcdExists_234 ;
      private short Gxremove234 ;
      private int trnEnded ;
      private int Z709ReferenciaINM_Codigo ;
      private int A709ReferenciaINM_Codigo ;
      private int AV14GXV1 ;
      private int AV11Insert_ReferenciaINM_AreaTrabalhoCod ;
      private int Z712ReferenciaINM_AreaTrabalhoCod ;
      private int A712ReferenciaINM_AreaTrabalhoCod ;
      private int Z2134ReferenciaINMAnexos_Codigo ;
      private int A2134ReferenciaINMAnexos_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode90 ;
      private String AV13Pgmname ;
      private String Z2141ReferenciaINMAnexos_ArquivoNome ;
      private String A2141ReferenciaINMAnexos_ArquivoNome ;
      private String Z2140ReferenciaINMAnexos_ArquivoTipo ;
      private String A2140ReferenciaINMAnexos_ArquivoTipo ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filetype ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filename ;
      private String sMode234 ;
      private DateTime Z2142ReferenciaINMAnexos_Data ;
      private DateTime A2142ReferenciaINMAnexos_Data ;
      private bool Z711ReferenciaINM_Ativo ;
      private bool A711ReferenciaINM_Ativo ;
      private bool n713ReferenciaINM_AreaTrabalhoDes ;
      private bool n2143ReferenciaINMAnexos_Link ;
      private bool n2142ReferenciaINMAnexos_Data ;
      private bool n2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool n2141ReferenciaINMAnexos_ArquivoNome ;
      private bool n645TipoDocumento_Codigo ;
      private bool n2139ReferenciaINMAnexos_Arquivo ;
      private bool i711ReferenciaINM_Ativo ;
      private String Z2138ReferenciaINMAnexos_Descricao ;
      private String A2138ReferenciaINMAnexos_Descricao ;
      private String Z2143ReferenciaINMAnexos_Link ;
      private String A2143ReferenciaINMAnexos_Link ;
      private String Z710ReferenciaINM_Descricao ;
      private String A710ReferenciaINM_Descricao ;
      private String Z713ReferenciaINM_AreaTrabalhoDes ;
      private String A713ReferenciaINM_AreaTrabalhoDes ;
      private String Z2139ReferenciaINMAnexos_Arquivo ;
      private String A2139ReferenciaINMAnexos_Arquivo ;
      private IGxSession AV10WebSession ;
      private SdtReferenciaINM bcReferenciaINM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC002C8_A709ReferenciaINM_Codigo ;
      private String[] BC002C8_A710ReferenciaINM_Descricao ;
      private String[] BC002C8_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C8_n713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C8_A711ReferenciaINM_Ativo ;
      private int[] BC002C8_A712ReferenciaINM_AreaTrabalhoCod ;
      private String[] BC002C7_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C7_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] BC002C9_A709ReferenciaINM_Codigo ;
      private int[] BC002C6_A709ReferenciaINM_Codigo ;
      private String[] BC002C6_A710ReferenciaINM_Descricao ;
      private bool[] BC002C6_A711ReferenciaINM_Ativo ;
      private int[] BC002C6_A712ReferenciaINM_AreaTrabalhoCod ;
      private int[] BC002C5_A709ReferenciaINM_Codigo ;
      private String[] BC002C5_A710ReferenciaINM_Descricao ;
      private bool[] BC002C5_A711ReferenciaINM_Ativo ;
      private int[] BC002C5_A712ReferenciaINM_AreaTrabalhoCod ;
      private int[] BC002C10_A709ReferenciaINM_Codigo ;
      private String[] BC002C13_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C13_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] BC002C14_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] BC002C14_A715ItemNaoMensuravel_Codigo ;
      private int[] BC002C15_A709ReferenciaINM_Codigo ;
      private String[] BC002C15_A710ReferenciaINM_Descricao ;
      private String[] BC002C15_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C15_n713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] BC002C15_A711ReferenciaINM_Ativo ;
      private int[] BC002C15_A712ReferenciaINM_AreaTrabalhoCod ;
      private int[] BC002C16_A709ReferenciaINM_Codigo ;
      private int[] BC002C16_A2134ReferenciaINMAnexos_Codigo ;
      private String[] BC002C16_A2138ReferenciaINMAnexos_Descricao ;
      private String[] BC002C16_A2143ReferenciaINMAnexos_Link ;
      private bool[] BC002C16_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] BC002C16_A2142ReferenciaINMAnexos_Data ;
      private bool[] BC002C16_n2142ReferenciaINMAnexos_Data ;
      private String[] BC002C16_A646TipoDocumento_Nome ;
      private String[] BC002C16_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] BC002C16_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] BC002C16_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] BC002C16_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] BC002C16_A645TipoDocumento_Codigo ;
      private bool[] BC002C16_n645TipoDocumento_Codigo ;
      private String[] BC002C16_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] BC002C16_n2139ReferenciaINMAnexos_Arquivo ;
      private String[] BC002C4_A646TipoDocumento_Nome ;
      private int[] BC002C17_A709ReferenciaINM_Codigo ;
      private int[] BC002C17_A2134ReferenciaINMAnexos_Codigo ;
      private int[] BC002C3_A709ReferenciaINM_Codigo ;
      private int[] BC002C3_A2134ReferenciaINMAnexos_Codigo ;
      private String[] BC002C3_A2138ReferenciaINMAnexos_Descricao ;
      private String[] BC002C3_A2143ReferenciaINMAnexos_Link ;
      private bool[] BC002C3_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] BC002C3_A2142ReferenciaINMAnexos_Data ;
      private bool[] BC002C3_n2142ReferenciaINMAnexos_Data ;
      private String[] BC002C3_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] BC002C3_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] BC002C3_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] BC002C3_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] BC002C3_A645TipoDocumento_Codigo ;
      private bool[] BC002C3_n645TipoDocumento_Codigo ;
      private String[] BC002C3_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] BC002C3_n2139ReferenciaINMAnexos_Arquivo ;
      private int[] BC002C2_A709ReferenciaINM_Codigo ;
      private int[] BC002C2_A2134ReferenciaINMAnexos_Codigo ;
      private String[] BC002C2_A2138ReferenciaINMAnexos_Descricao ;
      private String[] BC002C2_A2143ReferenciaINMAnexos_Link ;
      private bool[] BC002C2_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] BC002C2_A2142ReferenciaINMAnexos_Data ;
      private bool[] BC002C2_n2142ReferenciaINMAnexos_Data ;
      private String[] BC002C2_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] BC002C2_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] BC002C2_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] BC002C2_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] BC002C2_A645TipoDocumento_Codigo ;
      private bool[] BC002C2_n645TipoDocumento_Codigo ;
      private String[] BC002C2_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] BC002C2_n2139ReferenciaINMAnexos_Arquivo ;
      private String[] BC002C22_A646TipoDocumento_Nome ;
      private int[] BC002C23_A709ReferenciaINM_Codigo ;
      private int[] BC002C23_A2134ReferenciaINMAnexos_Codigo ;
      private String[] BC002C23_A2138ReferenciaINMAnexos_Descricao ;
      private String[] BC002C23_A2143ReferenciaINMAnexos_Link ;
      private bool[] BC002C23_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] BC002C23_A2142ReferenciaINMAnexos_Data ;
      private bool[] BC002C23_n2142ReferenciaINMAnexos_Data ;
      private String[] BC002C23_A646TipoDocumento_Nome ;
      private String[] BC002C23_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] BC002C23_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] BC002C23_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] BC002C23_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] BC002C23_A645TipoDocumento_Codigo ;
      private bool[] BC002C23_n645TipoDocumento_Codigo ;
      private String[] BC002C23_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] BC002C23_n2139ReferenciaINMAnexos_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class referenciainm_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC002C8 ;
          prmBC002C8 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C7 ;
          prmBC002C7 = new Object[] {
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C9 ;
          prmBC002C9 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C6 ;
          prmBC002C6 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C5 ;
          prmBC002C5 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C10 ;
          prmBC002C10 = new Object[] {
          new Object[] {"@ReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ReferenciaINM_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C11 ;
          prmBC002C11 = new Object[] {
          new Object[] {"@ReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ReferenciaINM_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C12 ;
          prmBC002C12 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C13 ;
          prmBC002C13 = new Object[] {
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C14 ;
          prmBC002C14 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C15 ;
          prmBC002C15 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C16 ;
          prmBC002C16 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C4 ;
          prmBC002C4 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C17 ;
          prmBC002C17 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C3 ;
          prmBC002C3 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C2 ;
          prmBC002C2 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C18 ;
          prmBC002C18 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaINMAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ReferenciaINMAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ReferenciaINMAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C19 ;
          prmBC002C19 = new Object[] {
          new Object[] {"@ReferenciaINMAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaINMAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ReferenciaINMAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C20 ;
          prmBC002C20 = new Object[] {
          new Object[] {"@ReferenciaINMAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C21 ;
          prmBC002C21 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C22 ;
          prmBC002C22 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002C23 ;
          prmBC002C23 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC002C2", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo], [ReferenciaINMAnexos_Arquivo] FROM [ReferenciaINMAnexos] WITH (UPDLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C2,1,0,true,false )
             ,new CursorDef("BC002C3", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo], [ReferenciaINMAnexos_Arquivo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C3,1,0,true,false )
             ,new CursorDef("BC002C4", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C4,1,0,true,false )
             ,new CursorDef("BC002C5", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM [ReferenciaINM] WITH (UPDLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C5,1,0,true,false )
             ,new CursorDef("BC002C6", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C6,1,0,true,false )
             ,new CursorDef("BC002C7", "SELECT [AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ReferenciaINM_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C7,1,0,true,false )
             ,new CursorDef("BC002C8", "SELECT TM1.[ReferenciaINM_Codigo], TM1.[ReferenciaINM_Descricao], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, TM1.[ReferenciaINM_Ativo], TM1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM ([ReferenciaINM] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[ReferenciaINM_AreaTrabalhoCod]) WHERE TM1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ORDER BY TM1.[ReferenciaINM_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C8,100,0,true,false )
             ,new CursorDef("BC002C9", "SELECT [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C9,1,0,true,false )
             ,new CursorDef("BC002C10", "INSERT INTO [ReferenciaINM]([ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod]) VALUES(@ReferenciaINM_Descricao, @ReferenciaINM_Ativo, @ReferenciaINM_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC002C10)
             ,new CursorDef("BC002C11", "UPDATE [ReferenciaINM] SET [ReferenciaINM_Descricao]=@ReferenciaINM_Descricao, [ReferenciaINM_Ativo]=@ReferenciaINM_Ativo, [ReferenciaINM_AreaTrabalhoCod]=@ReferenciaINM_AreaTrabalhoCod  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo", GxErrorMask.GX_NOMASK,prmBC002C11)
             ,new CursorDef("BC002C12", "DELETE FROM [ReferenciaINM]  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo", GxErrorMask.GX_NOMASK,prmBC002C12)
             ,new CursorDef("BC002C13", "SELECT [AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ReferenciaINM_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C13,1,0,true,false )
             ,new CursorDef("BC002C14", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C14,1,0,true,true )
             ,new CursorDef("BC002C15", "SELECT TM1.[ReferenciaINM_Codigo], TM1.[ReferenciaINM_Descricao], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, TM1.[ReferenciaINM_Ativo], TM1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM ([ReferenciaINM] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[ReferenciaINM_AreaTrabalhoCod]) WHERE TM1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ORDER BY TM1.[ReferenciaINM_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C15,100,0,true,false )
             ,new CursorDef("BC002C16", "SELECT T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo], T1.[ReferenciaINMAnexos_Descricao], T1.[ReferenciaINMAnexos_Link], T1.[ReferenciaINMAnexos_Data], T2.[TipoDocumento_Nome], T1.[ReferenciaINMAnexos_ArquivoTipo], T1.[ReferenciaINMAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ReferenciaINMAnexos_Arquivo] FROM ([ReferenciaINMAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo and T1.[ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ORDER BY T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C16,11,0,true,false )
             ,new CursorDef("BC002C17", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C17,1,0,true,false )
             ,new CursorDef("BC002C18", "INSERT INTO [ReferenciaINMAnexos]([ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Arquivo], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo]) VALUES(@ReferenciaINM_Codigo, @ReferenciaINMAnexos_Codigo, @ReferenciaINMAnexos_Descricao, @ReferenciaINMAnexos_Arquivo, @ReferenciaINMAnexos_Link, @ReferenciaINMAnexos_Data, @ReferenciaINMAnexos_ArquivoTipo, @ReferenciaINMAnexos_ArquivoNome, @TipoDocumento_Codigo)", GxErrorMask.GX_NOMASK,prmBC002C18)
             ,new CursorDef("BC002C19", "UPDATE [ReferenciaINMAnexos] SET [ReferenciaINMAnexos_Descricao]=@ReferenciaINMAnexos_Descricao, [ReferenciaINMAnexos_Link]=@ReferenciaINMAnexos_Link, [ReferenciaINMAnexos_Data]=@ReferenciaINMAnexos_Data, [ReferenciaINMAnexos_ArquivoTipo]=@ReferenciaINMAnexos_ArquivoTipo, [ReferenciaINMAnexos_ArquivoNome]=@ReferenciaINMAnexos_ArquivoNome, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002C19)
             ,new CursorDef("BC002C20", "UPDATE [ReferenciaINMAnexos] SET [ReferenciaINMAnexos_Arquivo]=@ReferenciaINMAnexos_Arquivo  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002C20)
             ,new CursorDef("BC002C21", "DELETE FROM [ReferenciaINMAnexos]  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002C21)
             ,new CursorDef("BC002C22", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C22,1,0,true,false )
             ,new CursorDef("BC002C23", "SELECT T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo], T1.[ReferenciaINMAnexos_Descricao], T1.[ReferenciaINMAnexos_Link], T1.[ReferenciaINMAnexos_Data], T2.[TipoDocumento_Nome], T1.[ReferenciaINMAnexos_ArquivoTipo], T1.[ReferenciaINMAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ReferenciaINMAnexos_Arquivo] FROM ([ReferenciaINMAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ORDER BY T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002C23,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
