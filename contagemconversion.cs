/*
               File: ContagemConversion
        Description: Conversion for table Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 17:53:55.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
namespace GeneXus.Programs {
   public class contagemconversion : GXProcedure
   {
      public contagemconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public contagemconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         contagemconversion objcontagemconversion;
         objcontagemconversion = new contagemconversion();
         objcontagemconversion.context.SetSubmitInitialConfig(context);
         objcontagemconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objcontagemconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((contagemconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         cmdBuffer=" SET IDENTITY_INSERT [GXA0043] ON "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         /* Using cursor CONTAGEMCO2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2200Contagem_DataAtualizacao = CONTAGEMCO2_A2200Contagem_DataAtualizacao[0];
            n2200Contagem_DataAtualizacao = CONTAGEMCO2_n2200Contagem_DataAtualizacao[0];
            A2199Contagem_DemandaCod = CONTAGEMCO2_A2199Contagem_DemandaCod[0];
            n2199Contagem_DemandaCod = CONTAGEMCO2_n2199Contagem_DemandaCod[0];
            A2198Contagem_valorLocPF = CONTAGEMCO2_A2198Contagem_valorLocPF[0];
            n2198Contagem_valorLocPF = CONTAGEMCO2_n2198Contagem_valorLocPF[0];
            A2197Contagem_ValorTaxaEntregaMlhr = CONTAGEMCO2_A2197Contagem_ValorTaxaEntregaMlhr[0];
            n2197Contagem_ValorTaxaEntregaMlhr = CONTAGEMCO2_n2197Contagem_ValorTaxaEntregaMlhr[0];
            A2196Contagem_ValorTaxaEntregaDsnv = CONTAGEMCO2_A2196Contagem_ValorTaxaEntregaDsnv[0];
            n2196Contagem_ValorTaxaEntregaDsnv = CONTAGEMCO2_n2196Contagem_ValorTaxaEntregaDsnv[0];
            A2195Contagem_LocPF = CONTAGEMCO2_A2195Contagem_LocPF[0];
            n2195Contagem_LocPF = CONTAGEMCO2_n2195Contagem_LocPF[0];
            A2194Contagem_TaxaEntregaMlhr = CONTAGEMCO2_A2194Contagem_TaxaEntregaMlhr[0];
            n2194Contagem_TaxaEntregaMlhr = CONTAGEMCO2_n2194Contagem_TaxaEntregaMlhr[0];
            A2193Contagem_TaxaEntregaDsnv = CONTAGEMCO2_A2193Contagem_TaxaEntregaDsnv[0];
            n2193Contagem_TaxaEntregaDsnv = CONTAGEMCO2_n2193Contagem_TaxaEntregaDsnv[0];
            A2192Contagem_PFLAImp = CONTAGEMCO2_A2192Contagem_PFLAImp[0];
            n2192Contagem_PFLAImp = CONTAGEMCO2_n2192Contagem_PFLAImp[0];
            A2191Contagem_fatorImp = CONTAGEMCO2_A2191Contagem_fatorImp[0];
            n2191Contagem_fatorImp = CONTAGEMCO2_n2191Contagem_fatorImp[0];
            A2190Contagem_PFLImp = CONTAGEMCO2_A2190Contagem_PFLImp[0];
            n2190Contagem_PFLImp = CONTAGEMCO2_n2190Contagem_PFLImp[0];
            A2189Contagem_PFBRetrabalho = CONTAGEMCO2_A2189Contagem_PFBRetrabalho[0];
            n2189Contagem_PFBRetrabalho = CONTAGEMCO2_n2189Contagem_PFBRetrabalho[0];
            A2188Contagem_Excluidos = CONTAGEMCO2_A2188Contagem_Excluidos[0];
            n2188Contagem_Excluidos = CONTAGEMCO2_n2188Contagem_Excluidos[0];
            A2187Contagem_Agrupador = CONTAGEMCO2_A2187Contagem_Agrupador[0];
            n2187Contagem_Agrupador = CONTAGEMCO2_n2187Contagem_Agrupador[0];
            A2186Contagem_Fases = CONTAGEMCO2_A2186Contagem_Fases[0];
            n2186Contagem_Fases = CONTAGEMCO2_n2186Contagem_Fases[0];
            A2185Contagem_Metodologia = CONTAGEMCO2_A2185Contagem_Metodologia[0];
            n2185Contagem_Metodologia = CONTAGEMCO2_n2185Contagem_Metodologia[0];
            A2184Contagem_UnidadeOrganizacional = CONTAGEMCO2_A2184Contagem_UnidadeOrganizacional[0];
            n2184Contagem_UnidadeOrganizacional = CONTAGEMCO2_n2184Contagem_UnidadeOrganizacional[0];
            A2183Contagem_TipoUnidade = CONTAGEMCO2_A2183Contagem_TipoUnidade[0];
            n2183Contagem_TipoUnidade = CONTAGEMCO2_n2183Contagem_TipoUnidade[0];
            A2182Contagem_Pendencia = CONTAGEMCO2_A2182Contagem_Pendencia[0];
            n2182Contagem_Pendencia = CONTAGEMCO2_n2182Contagem_Pendencia[0];
            A2181Contagem_CntSrvCod = CONTAGEMCO2_A2181Contagem_CntSrvCod[0];
            n2181Contagem_CntSrvCod = CONTAGEMCO2_n2181Contagem_CntSrvCod[0];
            A1814Contagem_AmbienteTecnologico = CONTAGEMCO2_A1814Contagem_AmbienteTecnologico[0];
            n1814Contagem_AmbienteTecnologico = CONTAGEMCO2_n1814Contagem_AmbienteTecnologico[0];
            A1813Contagem_ReferenciaINM = CONTAGEMCO2_A1813Contagem_ReferenciaINM[0];
            n1813Contagem_ReferenciaINM = CONTAGEMCO2_n1813Contagem_ReferenciaINM[0];
            A1812Contagem_ArquivoImp = CONTAGEMCO2_A1812Contagem_ArquivoImp[0];
            n1812Contagem_ArquivoImp = CONTAGEMCO2_n1812Contagem_ArquivoImp[0];
            A1811Contagem_ServicoCod = CONTAGEMCO2_A1811Contagem_ServicoCod[0];
            n1811Contagem_ServicoCod = CONTAGEMCO2_n1811Contagem_ServicoCod[0];
            A1810Contagem_Versao = CONTAGEMCO2_A1810Contagem_Versao[0];
            n1810Contagem_Versao = CONTAGEMCO2_n1810Contagem_Versao[0];
            A1809Contagem_Aplicabilidade = CONTAGEMCO2_A1809Contagem_Aplicabilidade[0];
            n1809Contagem_Aplicabilidade = CONTAGEMCO2_n1809Contagem_Aplicabilidade[0];
            A1120Contagem_Descricao = CONTAGEMCO2_A1120Contagem_Descricao[0];
            n1120Contagem_Descricao = CONTAGEMCO2_n1120Contagem_Descricao[0];
            A1111Contagem_DataHomologacao = CONTAGEMCO2_A1111Contagem_DataHomologacao[0];
            n1111Contagem_DataHomologacao = CONTAGEMCO2_n1111Contagem_DataHomologacao[0];
            A1119Contagem_Divergencia = CONTAGEMCO2_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = CONTAGEMCO2_n1119Contagem_Divergencia[0];
            A1118Contagem_ContratadaCod = CONTAGEMCO2_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = CONTAGEMCO2_n1118Contagem_ContratadaCod[0];
            A1117Contagem_Deflator = CONTAGEMCO2_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = CONTAGEMCO2_n1117Contagem_Deflator[0];
            A1116Contagem_PFLD = CONTAGEMCO2_A1116Contagem_PFLD[0];
            n1116Contagem_PFLD = CONTAGEMCO2_n1116Contagem_PFLD[0];
            A1115Contagem_PFBD = CONTAGEMCO2_A1115Contagem_PFBD[0];
            n1115Contagem_PFBD = CONTAGEMCO2_n1115Contagem_PFBD[0];
            A1114Contagem_PFLA = CONTAGEMCO2_A1114Contagem_PFLA[0];
            n1114Contagem_PFLA = CONTAGEMCO2_n1114Contagem_PFLA[0];
            A1113Contagem_PFBA = CONTAGEMCO2_A1113Contagem_PFBA[0];
            n1113Contagem_PFBA = CONTAGEMCO2_n1113Contagem_PFBA[0];
            A1112Contagem_Consideracoes = CONTAGEMCO2_A1112Contagem_Consideracoes[0];
            n1112Contagem_Consideracoes = CONTAGEMCO2_n1112Contagem_Consideracoes[0];
            A1059Contagem_Notas = CONTAGEMCO2_A1059Contagem_Notas[0];
            n1059Contagem_Notas = CONTAGEMCO2_n1059Contagem_Notas[0];
            A948Contagem_Lock = CONTAGEMCO2_A948Contagem_Lock[0];
            A939Contagem_ProjetoCod = CONTAGEMCO2_A939Contagem_ProjetoCod[0];
            n939Contagem_ProjetoCod = CONTAGEMCO2_n939Contagem_ProjetoCod[0];
            A940Contagem_SistemaCod = CONTAGEMCO2_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = CONTAGEMCO2_n940Contagem_SistemaCod[0];
            A947Contagem_Fator = CONTAGEMCO2_A947Contagem_Fator[0];
            n947Contagem_Fator = CONTAGEMCO2_n947Contagem_Fator[0];
            A946Contagem_Link = CONTAGEMCO2_A946Contagem_Link[0];
            n946Contagem_Link = CONTAGEMCO2_n946Contagem_Link[0];
            A945Contagem_Demanda = CONTAGEMCO2_A945Contagem_Demanda[0];
            n945Contagem_Demanda = CONTAGEMCO2_n945Contagem_Demanda[0];
            A944Contagem_PFL = CONTAGEMCO2_A944Contagem_PFL[0];
            n944Contagem_PFL = CONTAGEMCO2_n944Contagem_PFL[0];
            A943Contagem_PFB = CONTAGEMCO2_A943Contagem_PFB[0];
            n943Contagem_PFB = CONTAGEMCO2_n943Contagem_PFB[0];
            A262Contagem_Status = CONTAGEMCO2_A262Contagem_Status[0];
            n262Contagem_Status = CONTAGEMCO2_n262Contagem_Status[0];
            A213Contagem_UsuarioContadorCod = CONTAGEMCO2_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = CONTAGEMCO2_n213Contagem_UsuarioContadorCod[0];
            A202Contagem_Observacao = CONTAGEMCO2_A202Contagem_Observacao[0];
            n202Contagem_Observacao = CONTAGEMCO2_n202Contagem_Observacao[0];
            A201Contagem_Fronteira = CONTAGEMCO2_A201Contagem_Fronteira[0];
            n201Contagem_Fronteira = CONTAGEMCO2_n201Contagem_Fronteira[0];
            A200Contagem_Escopo = CONTAGEMCO2_A200Contagem_Escopo[0];
            n200Contagem_Escopo = CONTAGEMCO2_n200Contagem_Escopo[0];
            A199Contagem_Proposito = CONTAGEMCO2_A199Contagem_Proposito[0];
            n199Contagem_Proposito = CONTAGEMCO2_n199Contagem_Proposito[0];
            A197Contagem_DataCriacao = CONTAGEMCO2_A197Contagem_DataCriacao[0];
            A196Contagem_Tipo = CONTAGEMCO2_A196Contagem_Tipo[0];
            n196Contagem_Tipo = CONTAGEMCO2_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = CONTAGEMCO2_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = CONTAGEMCO2_n195Contagem_Tecnica[0];
            A193Contagem_AreaTrabalhoCod = CONTAGEMCO2_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = CONTAGEMCO2_A192Contagem_Codigo[0];
            /*
               INSERT RECORD ON TABLE GXA0043

            */
            AV2Contagem_Codigo = A192Contagem_Codigo;
            AV3Contagem_AreaTrabalhoCod = A193Contagem_AreaTrabalhoCod;
            if ( CONTAGEMCO2_n195Contagem_Tecnica[0] )
            {
               AV4Contagem_Tecnica = "";
               nV4Contagem_Tecnica = false;
               nV4Contagem_Tecnica = true;
            }
            else
            {
               AV4Contagem_Tecnica = A195Contagem_Tecnica;
               nV4Contagem_Tecnica = false;
            }
            if ( CONTAGEMCO2_n196Contagem_Tipo[0] )
            {
               AV5Contagem_Tipo = "";
               nV5Contagem_Tipo = false;
               nV5Contagem_Tipo = true;
            }
            else
            {
               AV5Contagem_Tipo = A196Contagem_Tipo;
               nV5Contagem_Tipo = false;
            }
            AV6Contagem_DataCriacao = A197Contagem_DataCriacao;
            if ( CONTAGEMCO2_n199Contagem_Proposito[0] )
            {
               AV7Contagem_Proposito = "";
               nV7Contagem_Proposito = false;
               nV7Contagem_Proposito = true;
            }
            else
            {
               AV7Contagem_Proposito = A199Contagem_Proposito;
               nV7Contagem_Proposito = false;
            }
            if ( CONTAGEMCO2_n200Contagem_Escopo[0] )
            {
               AV8Contagem_Escopo = "";
               nV8Contagem_Escopo = false;
               nV8Contagem_Escopo = true;
            }
            else
            {
               AV8Contagem_Escopo = A200Contagem_Escopo;
               nV8Contagem_Escopo = false;
            }
            if ( CONTAGEMCO2_n201Contagem_Fronteira[0] )
            {
               AV9Contagem_Fronteira = "";
               nV9Contagem_Fronteira = false;
               nV9Contagem_Fronteira = true;
            }
            else
            {
               AV9Contagem_Fronteira = A201Contagem_Fronteira;
               nV9Contagem_Fronteira = false;
            }
            if ( CONTAGEMCO2_n202Contagem_Observacao[0] )
            {
               AV10Contagem_Observacao = "";
               nV10Contagem_Observacao = false;
               nV10Contagem_Observacao = true;
            }
            else
            {
               AV10Contagem_Observacao = A202Contagem_Observacao;
               nV10Contagem_Observacao = false;
            }
            if ( CONTAGEMCO2_n213Contagem_UsuarioContadorCod[0] )
            {
               AV11Contagem_UsuarioContadorCod = 0;
               nV11Contagem_UsuarioContadorCod = false;
               nV11Contagem_UsuarioContadorCod = true;
            }
            else
            {
               AV11Contagem_UsuarioContadorCod = A213Contagem_UsuarioContadorCod;
               nV11Contagem_UsuarioContadorCod = false;
            }
            if ( CONTAGEMCO2_n262Contagem_Status[0] )
            {
               AV12Contagem_Status = "";
               nV12Contagem_Status = false;
               nV12Contagem_Status = true;
            }
            else
            {
               AV12Contagem_Status = A262Contagem_Status;
               nV12Contagem_Status = false;
            }
            if ( CONTAGEMCO2_n943Contagem_PFB[0] )
            {
               AV13Contagem_PFB = 0;
               nV13Contagem_PFB = false;
               nV13Contagem_PFB = true;
            }
            else
            {
               AV13Contagem_PFB = A943Contagem_PFB;
               nV13Contagem_PFB = false;
            }
            if ( CONTAGEMCO2_n944Contagem_PFL[0] )
            {
               AV14Contagem_PFL = 0;
               nV14Contagem_PFL = false;
               nV14Contagem_PFL = true;
            }
            else
            {
               AV14Contagem_PFL = A944Contagem_PFL;
               nV14Contagem_PFL = false;
            }
            if ( CONTAGEMCO2_n945Contagem_Demanda[0] )
            {
               AV15Contagem_Demanda = "";
               nV15Contagem_Demanda = false;
               nV15Contagem_Demanda = true;
            }
            else
            {
               AV15Contagem_Demanda = A945Contagem_Demanda;
               nV15Contagem_Demanda = false;
            }
            if ( CONTAGEMCO2_n946Contagem_Link[0] )
            {
               AV16Contagem_Link = "";
               nV16Contagem_Link = false;
               nV16Contagem_Link = true;
            }
            else
            {
               AV16Contagem_Link = A946Contagem_Link;
               nV16Contagem_Link = false;
            }
            if ( CONTAGEMCO2_n947Contagem_Fator[0] )
            {
               AV17Contagem_Fator = 0;
               nV17Contagem_Fator = false;
               nV17Contagem_Fator = true;
            }
            else
            {
               AV17Contagem_Fator = A947Contagem_Fator;
               nV17Contagem_Fator = false;
            }
            if ( CONTAGEMCO2_n940Contagem_SistemaCod[0] )
            {
               AV18Contagem_SistemaCod = 0;
               nV18Contagem_SistemaCod = false;
               nV18Contagem_SistemaCod = true;
            }
            else
            {
               AV18Contagem_SistemaCod = A940Contagem_SistemaCod;
               nV18Contagem_SistemaCod = false;
            }
            if ( CONTAGEMCO2_n939Contagem_ProjetoCod[0] )
            {
               AV19Contagem_ProjetoCod = 0;
               nV19Contagem_ProjetoCod = false;
               nV19Contagem_ProjetoCod = true;
            }
            else
            {
               AV19Contagem_ProjetoCod = A939Contagem_ProjetoCod;
               nV19Contagem_ProjetoCod = false;
            }
            AV20Contagem_Lock = A948Contagem_Lock;
            if ( CONTAGEMCO2_n1059Contagem_Notas[0] )
            {
               AV21Contagem_Notas = "";
               nV21Contagem_Notas = false;
               nV21Contagem_Notas = true;
            }
            else
            {
               AV21Contagem_Notas = A1059Contagem_Notas;
               nV21Contagem_Notas = false;
            }
            if ( CONTAGEMCO2_n1112Contagem_Consideracoes[0] )
            {
               AV22Contagem_Consideracoes = "";
               nV22Contagem_Consideracoes = false;
               nV22Contagem_Consideracoes = true;
            }
            else
            {
               AV22Contagem_Consideracoes = A1112Contagem_Consideracoes;
               nV22Contagem_Consideracoes = false;
            }
            if ( CONTAGEMCO2_n1113Contagem_PFBA[0] )
            {
               AV23Contagem_PFBA = 0;
               nV23Contagem_PFBA = false;
               nV23Contagem_PFBA = true;
            }
            else
            {
               AV23Contagem_PFBA = A1113Contagem_PFBA;
               nV23Contagem_PFBA = false;
            }
            if ( CONTAGEMCO2_n1114Contagem_PFLA[0] )
            {
               AV24Contagem_PFLA = 0;
               nV24Contagem_PFLA = false;
               nV24Contagem_PFLA = true;
            }
            else
            {
               AV24Contagem_PFLA = A1114Contagem_PFLA;
               nV24Contagem_PFLA = false;
            }
            if ( CONTAGEMCO2_n1115Contagem_PFBD[0] )
            {
               AV25Contagem_PFBD = 0;
               nV25Contagem_PFBD = false;
               nV25Contagem_PFBD = true;
            }
            else
            {
               AV25Contagem_PFBD = A1115Contagem_PFBD;
               nV25Contagem_PFBD = false;
            }
            if ( CONTAGEMCO2_n1116Contagem_PFLD[0] )
            {
               AV26Contagem_PFLD = 0;
               nV26Contagem_PFLD = false;
               nV26Contagem_PFLD = true;
            }
            else
            {
               AV26Contagem_PFLD = A1116Contagem_PFLD;
               nV26Contagem_PFLD = false;
            }
            if ( CONTAGEMCO2_n1117Contagem_Deflator[0] )
            {
               AV27Contagem_Deflator = 0;
               nV27Contagem_Deflator = false;
               nV27Contagem_Deflator = true;
            }
            else
            {
               AV27Contagem_Deflator = A1117Contagem_Deflator;
               nV27Contagem_Deflator = false;
            }
            if ( CONTAGEMCO2_n1118Contagem_ContratadaCod[0] )
            {
               AV28Contagem_ContratadaCod = 0;
               nV28Contagem_ContratadaCod = false;
               nV28Contagem_ContratadaCod = true;
            }
            else
            {
               AV28Contagem_ContratadaCod = A1118Contagem_ContratadaCod;
               nV28Contagem_ContratadaCod = false;
            }
            if ( CONTAGEMCO2_n1119Contagem_Divergencia[0] )
            {
               AV29Contagem_Divergencia = 0;
               nV29Contagem_Divergencia = false;
               nV29Contagem_Divergencia = true;
            }
            else
            {
               AV29Contagem_Divergencia = A1119Contagem_Divergencia;
               nV29Contagem_Divergencia = false;
            }
            if ( CONTAGEMCO2_n1111Contagem_DataHomologacao[0] )
            {
               AV30Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
               nV30Contagem_DataHomologacao = false;
               nV30Contagem_DataHomologacao = true;
            }
            else
            {
               AV30Contagem_DataHomologacao = A1111Contagem_DataHomologacao;
               nV30Contagem_DataHomologacao = false;
            }
            if ( CONTAGEMCO2_n1120Contagem_Descricao[0] )
            {
               AV31Contagem_Descricao = "";
               nV31Contagem_Descricao = false;
               nV31Contagem_Descricao = true;
            }
            else
            {
               AV31Contagem_Descricao = A1120Contagem_Descricao;
               nV31Contagem_Descricao = false;
            }
            if ( CONTAGEMCO2_n1809Contagem_Aplicabilidade[0] )
            {
               AV32Contagem_Aplicabilidade = "";
               nV32Contagem_Aplicabilidade = false;
               nV32Contagem_Aplicabilidade = true;
            }
            else
            {
               AV32Contagem_Aplicabilidade = A1809Contagem_Aplicabilidade;
               nV32Contagem_Aplicabilidade = false;
            }
            if ( CONTAGEMCO2_n1810Contagem_Versao[0] )
            {
               AV33Contagem_Versao = "";
               nV33Contagem_Versao = false;
               nV33Contagem_Versao = true;
            }
            else
            {
               AV33Contagem_Versao = A1810Contagem_Versao;
               nV33Contagem_Versao = false;
            }
            if ( CONTAGEMCO2_n1811Contagem_ServicoCod[0] )
            {
               AV34Contagem_ServicoCod = 0;
               nV34Contagem_ServicoCod = false;
               nV34Contagem_ServicoCod = true;
            }
            else
            {
               AV34Contagem_ServicoCod = A1811Contagem_ServicoCod;
               nV34Contagem_ServicoCod = false;
            }
            if ( CONTAGEMCO2_n1812Contagem_ArquivoImp[0] )
            {
               AV35Contagem_ArquivoImp = "";
               nV35Contagem_ArquivoImp = false;
               nV35Contagem_ArquivoImp = true;
            }
            else
            {
               AV35Contagem_ArquivoImp = A1812Contagem_ArquivoImp;
               nV35Contagem_ArquivoImp = false;
            }
            if ( CONTAGEMCO2_n1813Contagem_ReferenciaINM[0] )
            {
               AV36Contagem_ReferenciaINM = 0;
               nV36Contagem_ReferenciaINM = false;
               nV36Contagem_ReferenciaINM = true;
            }
            else
            {
               AV36Contagem_ReferenciaINM = A1813Contagem_ReferenciaINM;
               nV36Contagem_ReferenciaINM = false;
            }
            if ( CONTAGEMCO2_n1814Contagem_AmbienteTecnologico[0] )
            {
               AV37Contagem_AmbienteTecnologico = 0;
               nV37Contagem_AmbienteTecnologico = false;
               nV37Contagem_AmbienteTecnologico = true;
            }
            else
            {
               AV37Contagem_AmbienteTecnologico = A1814Contagem_AmbienteTecnologico;
               nV37Contagem_AmbienteTecnologico = false;
            }
            if ( CONTAGEMCO2_n2181Contagem_CntSrvCod[0] )
            {
               AV38Contagem_CntSrvCod = 0;
               nV38Contagem_CntSrvCod = false;
               nV38Contagem_CntSrvCod = true;
            }
            else
            {
               AV38Contagem_CntSrvCod = A2181Contagem_CntSrvCod;
               nV38Contagem_CntSrvCod = false;
            }
            if ( CONTAGEMCO2_n2182Contagem_Pendencia[0] )
            {
               AV39Contagem_Pendencia = "";
               nV39Contagem_Pendencia = false;
               nV39Contagem_Pendencia = true;
            }
            else
            {
               AV39Contagem_Pendencia = A2182Contagem_Pendencia;
               nV39Contagem_Pendencia = false;
            }
            if ( CONTAGEMCO2_n2183Contagem_TipoUnidade[0] )
            {
               AV40Contagem_TipoUnidade = 0;
               nV40Contagem_TipoUnidade = false;
               nV40Contagem_TipoUnidade = true;
            }
            else
            {
               AV40Contagem_TipoUnidade = A2183Contagem_TipoUnidade;
               nV40Contagem_TipoUnidade = false;
            }
            if ( CONTAGEMCO2_n2184Contagem_UnidadeOrganizacional[0] )
            {
               AV41Contagem_UnidadeOrganizacional = 0;
               nV41Contagem_UnidadeOrganizacional = false;
               nV41Contagem_UnidadeOrganizacional = true;
            }
            else
            {
               AV41Contagem_UnidadeOrganizacional = A2184Contagem_UnidadeOrganizacional;
               nV41Contagem_UnidadeOrganizacional = false;
            }
            if ( CONTAGEMCO2_n2185Contagem_Metodologia[0] )
            {
               AV42Contagem_Metodologia = 0;
               nV42Contagem_Metodologia = false;
               nV42Contagem_Metodologia = true;
            }
            else
            {
               AV42Contagem_Metodologia = A2185Contagem_Metodologia;
               nV42Contagem_Metodologia = false;
            }
            if ( CONTAGEMCO2_n2186Contagem_Fases[0] )
            {
               AV43Contagem_Fases = 0;
               nV43Contagem_Fases = false;
               nV43Contagem_Fases = true;
            }
            else
            {
               AV43Contagem_Fases = A2186Contagem_Fases;
               nV43Contagem_Fases = false;
            }
            if ( CONTAGEMCO2_n2187Contagem_Agrupador[0] )
            {
               AV44Contagem_Agrupador = "";
               nV44Contagem_Agrupador = false;
               nV44Contagem_Agrupador = true;
            }
            else
            {
               AV44Contagem_Agrupador = A2187Contagem_Agrupador;
               nV44Contagem_Agrupador = false;
            }
            if ( CONTAGEMCO2_n2188Contagem_Excluidos[0] )
            {
               AV45Contagem_Excluidos = "";
               nV45Contagem_Excluidos = false;
               nV45Contagem_Excluidos = true;
            }
            else
            {
               AV45Contagem_Excluidos = A2188Contagem_Excluidos;
               nV45Contagem_Excluidos = false;
            }
            if ( CONTAGEMCO2_n2189Contagem_PFBRetrabalho[0] )
            {
               AV46Contagem_PFBRetrabalho = 0;
               nV46Contagem_PFBRetrabalho = false;
               nV46Contagem_PFBRetrabalho = true;
            }
            else
            {
               AV46Contagem_PFBRetrabalho = A2189Contagem_PFBRetrabalho;
               nV46Contagem_PFBRetrabalho = false;
            }
            if ( CONTAGEMCO2_n2190Contagem_PFLImp[0] )
            {
               AV47Contagem_PFLImp = 0;
               nV47Contagem_PFLImp = false;
               nV47Contagem_PFLImp = true;
            }
            else
            {
               AV47Contagem_PFLImp = A2190Contagem_PFLImp;
               nV47Contagem_PFLImp = false;
            }
            if ( CONTAGEMCO2_n2191Contagem_fatorImp[0] )
            {
               AV48Contagem_fatorImp = 0;
               nV48Contagem_fatorImp = false;
               nV48Contagem_fatorImp = true;
            }
            else
            {
               AV48Contagem_fatorImp = A2191Contagem_fatorImp;
               nV48Contagem_fatorImp = false;
            }
            if ( CONTAGEMCO2_n2192Contagem_PFLAImp[0] )
            {
               AV49Contagem_PFLAImp = 0;
               nV49Contagem_PFLAImp = false;
               nV49Contagem_PFLAImp = true;
            }
            else
            {
               AV49Contagem_PFLAImp = A2192Contagem_PFLAImp;
               nV49Contagem_PFLAImp = false;
            }
            if ( CONTAGEMCO2_n2193Contagem_TaxaEntregaDsnv[0] )
            {
               AV50Contagem_TaxaEntregaDsnv = 0;
               nV50Contagem_TaxaEntregaDsnv = false;
               nV50Contagem_TaxaEntregaDsnv = true;
            }
            else
            {
               AV50Contagem_TaxaEntregaDsnv = (short)(A2193Contagem_TaxaEntregaDsnv);
               nV50Contagem_TaxaEntregaDsnv = false;
            }
            if ( CONTAGEMCO2_n2194Contagem_TaxaEntregaMlhr[0] )
            {
               AV51Contagem_TaxaEntregaMlhr = 0;
               nV51Contagem_TaxaEntregaMlhr = false;
               nV51Contagem_TaxaEntregaMlhr = true;
            }
            else
            {
               AV51Contagem_TaxaEntregaMlhr = (short)(A2194Contagem_TaxaEntregaMlhr);
               nV51Contagem_TaxaEntregaMlhr = false;
            }
            if ( CONTAGEMCO2_n2195Contagem_LocPF[0] )
            {
               AV52Contagem_LocPF = 0;
               nV52Contagem_LocPF = false;
               nV52Contagem_LocPF = true;
            }
            else
            {
               AV52Contagem_LocPF = (short)(A2195Contagem_LocPF);
               nV52Contagem_LocPF = false;
            }
            if ( CONTAGEMCO2_n2196Contagem_ValorTaxaEntregaDsnv[0] )
            {
               AV53Contagem_ValorTaxaEntregaDsnv = 0;
               nV53Contagem_ValorTaxaEntregaDsnv = false;
               nV53Contagem_ValorTaxaEntregaDsnv = true;
            }
            else
            {
               AV53Contagem_ValorTaxaEntregaDsnv = (decimal)(A2196Contagem_ValorTaxaEntregaDsnv);
               nV53Contagem_ValorTaxaEntregaDsnv = false;
            }
            if ( CONTAGEMCO2_n2197Contagem_ValorTaxaEntregaMlhr[0] )
            {
               AV54Contagem_ValorTaxaEntregaMlhr = 0;
               nV54Contagem_ValorTaxaEntregaMlhr = false;
               nV54Contagem_ValorTaxaEntregaMlhr = true;
            }
            else
            {
               AV54Contagem_ValorTaxaEntregaMlhr = (decimal)(A2197Contagem_ValorTaxaEntregaMlhr);
               nV54Contagem_ValorTaxaEntregaMlhr = false;
            }
            if ( CONTAGEMCO2_n2198Contagem_valorLocPF[0] )
            {
               AV55Contagem_valorLocPF = 0;
               nV55Contagem_valorLocPF = false;
               nV55Contagem_valorLocPF = true;
            }
            else
            {
               AV55Contagem_valorLocPF = (decimal)(A2198Contagem_valorLocPF);
               nV55Contagem_valorLocPF = false;
            }
            if ( CONTAGEMCO2_n2199Contagem_DemandaCod[0] )
            {
               AV56Contagem_DemandaCod = 0;
               nV56Contagem_DemandaCod = false;
               nV56Contagem_DemandaCod = true;
            }
            else
            {
               AV56Contagem_DemandaCod = A2199Contagem_DemandaCod;
               nV56Contagem_DemandaCod = false;
            }
            if ( CONTAGEMCO2_n2200Contagem_DataAtualizacao[0] )
            {
               AV57Contagem_DataAtualizacao = (DateTime)(DateTime.MinValue);
               nV57Contagem_DataAtualizacao = false;
               nV57Contagem_DataAtualizacao = true;
            }
            else
            {
               AV57Contagem_DataAtualizacao = A2200Contagem_DataAtualizacao;
               nV57Contagem_DataAtualizacao = false;
            }
            /* Using cursor CONTAGEMCO3 */
            pr_default.execute(1, new Object[] {AV2Contagem_Codigo, AV3Contagem_AreaTrabalhoCod, nV4Contagem_Tecnica, AV4Contagem_Tecnica, nV5Contagem_Tipo, AV5Contagem_Tipo, AV6Contagem_DataCriacao, nV7Contagem_Proposito, AV7Contagem_Proposito, nV8Contagem_Escopo, AV8Contagem_Escopo, nV9Contagem_Fronteira, AV9Contagem_Fronteira, nV10Contagem_Observacao, AV10Contagem_Observacao, nV11Contagem_UsuarioContadorCod, AV11Contagem_UsuarioContadorCod, nV12Contagem_Status, AV12Contagem_Status, nV13Contagem_PFB, AV13Contagem_PFB, nV14Contagem_PFL, AV14Contagem_PFL, nV15Contagem_Demanda, AV15Contagem_Demanda, nV16Contagem_Link, AV16Contagem_Link, nV17Contagem_Fator, AV17Contagem_Fator, nV18Contagem_SistemaCod, AV18Contagem_SistemaCod, nV19Contagem_ProjetoCod, AV19Contagem_ProjetoCod, AV20Contagem_Lock, nV21Contagem_Notas, AV21Contagem_Notas, nV22Contagem_Consideracoes, AV22Contagem_Consideracoes, nV23Contagem_PFBA, AV23Contagem_PFBA, nV24Contagem_PFLA, AV24Contagem_PFLA, nV25Contagem_PFBD, AV25Contagem_PFBD, nV26Contagem_PFLD, AV26Contagem_PFLD, nV27Contagem_Deflator, AV27Contagem_Deflator, nV28Contagem_ContratadaCod, AV28Contagem_ContratadaCod, nV29Contagem_Divergencia, AV29Contagem_Divergencia, nV30Contagem_DataHomologacao, AV30Contagem_DataHomologacao, nV31Contagem_Descricao, AV31Contagem_Descricao, nV32Contagem_Aplicabilidade, AV32Contagem_Aplicabilidade, nV33Contagem_Versao, AV33Contagem_Versao, nV34Contagem_ServicoCod, AV34Contagem_ServicoCod, nV35Contagem_ArquivoImp, AV35Contagem_ArquivoImp, nV36Contagem_ReferenciaINM, AV36Contagem_ReferenciaINM, nV37Contagem_AmbienteTecnologico, AV37Contagem_AmbienteTecnologico, nV38Contagem_CntSrvCod, AV38Contagem_CntSrvCod, nV39Contagem_Pendencia, AV39Contagem_Pendencia, nV40Contagem_TipoUnidade, AV40Contagem_TipoUnidade, nV41Contagem_UnidadeOrganizacional, AV41Contagem_UnidadeOrganizacional, nV42Contagem_Metodologia, AV42Contagem_Metodologia, nV43Contagem_Fases, AV43Contagem_Fases, nV44Contagem_Agrupador, AV44Contagem_Agrupador, nV45Contagem_Excluidos, AV45Contagem_Excluidos, nV46Contagem_PFBRetrabalho, AV46Contagem_PFBRetrabalho, nV47Contagem_PFLImp, AV47Contagem_PFLImp, nV48Contagem_fatorImp, AV48Contagem_fatorImp, nV49Contagem_PFLAImp, AV49Contagem_PFLAImp, nV50Contagem_TaxaEntregaDsnv, AV50Contagem_TaxaEntregaDsnv, nV51Contagem_TaxaEntregaMlhr, AV51Contagem_TaxaEntregaMlhr, nV52Contagem_LocPF, AV52Contagem_LocPF, nV53Contagem_ValorTaxaEntregaDsnv, AV53Contagem_ValorTaxaEntregaDsnv, nV54Contagem_ValorTaxaEntregaMlhr, AV54Contagem_ValorTaxaEntregaMlhr, nV55Contagem_valorLocPF, AV55Contagem_valorLocPF, nV56Contagem_DemandaCod, AV56Contagem_DemandaCod, nV57Contagem_DataAtualizacao, AV57Contagem_DataAtualizacao});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0043") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         cmdBuffer=" SET IDENTITY_INSERT [GXA0043] OFF "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         cmdBuffer = "";
         scmdbuf = "";
         CONTAGEMCO2_A2200Contagem_DataAtualizacao = new DateTime[] {DateTime.MinValue} ;
         CONTAGEMCO2_n2200Contagem_DataAtualizacao = new bool[] {false} ;
         CONTAGEMCO2_A2199Contagem_DemandaCod = new int[1] ;
         CONTAGEMCO2_n2199Contagem_DemandaCod = new bool[] {false} ;
         CONTAGEMCO2_A2198Contagem_valorLocPF = new int[1] ;
         CONTAGEMCO2_n2198Contagem_valorLocPF = new bool[] {false} ;
         CONTAGEMCO2_A2197Contagem_ValorTaxaEntregaMlhr = new int[1] ;
         CONTAGEMCO2_n2197Contagem_ValorTaxaEntregaMlhr = new bool[] {false} ;
         CONTAGEMCO2_A2196Contagem_ValorTaxaEntregaDsnv = new int[1] ;
         CONTAGEMCO2_n2196Contagem_ValorTaxaEntregaDsnv = new bool[] {false} ;
         CONTAGEMCO2_A2195Contagem_LocPF = new int[1] ;
         CONTAGEMCO2_n2195Contagem_LocPF = new bool[] {false} ;
         CONTAGEMCO2_A2194Contagem_TaxaEntregaMlhr = new int[1] ;
         CONTAGEMCO2_n2194Contagem_TaxaEntregaMlhr = new bool[] {false} ;
         CONTAGEMCO2_A2193Contagem_TaxaEntregaDsnv = new int[1] ;
         CONTAGEMCO2_n2193Contagem_TaxaEntregaDsnv = new bool[] {false} ;
         CONTAGEMCO2_A2192Contagem_PFLAImp = new decimal[1] ;
         CONTAGEMCO2_n2192Contagem_PFLAImp = new bool[] {false} ;
         CONTAGEMCO2_A2191Contagem_fatorImp = new decimal[1] ;
         CONTAGEMCO2_n2191Contagem_fatorImp = new bool[] {false} ;
         CONTAGEMCO2_A2190Contagem_PFLImp = new decimal[1] ;
         CONTAGEMCO2_n2190Contagem_PFLImp = new bool[] {false} ;
         CONTAGEMCO2_A2189Contagem_PFBRetrabalho = new decimal[1] ;
         CONTAGEMCO2_n2189Contagem_PFBRetrabalho = new bool[] {false} ;
         CONTAGEMCO2_A2188Contagem_Excluidos = new String[] {""} ;
         CONTAGEMCO2_n2188Contagem_Excluidos = new bool[] {false} ;
         CONTAGEMCO2_A2187Contagem_Agrupador = new String[] {""} ;
         CONTAGEMCO2_n2187Contagem_Agrupador = new bool[] {false} ;
         CONTAGEMCO2_A2186Contagem_Fases = new int[1] ;
         CONTAGEMCO2_n2186Contagem_Fases = new bool[] {false} ;
         CONTAGEMCO2_A2185Contagem_Metodologia = new int[1] ;
         CONTAGEMCO2_n2185Contagem_Metodologia = new bool[] {false} ;
         CONTAGEMCO2_A2184Contagem_UnidadeOrganizacional = new int[1] ;
         CONTAGEMCO2_n2184Contagem_UnidadeOrganizacional = new bool[] {false} ;
         CONTAGEMCO2_A2183Contagem_TipoUnidade = new int[1] ;
         CONTAGEMCO2_n2183Contagem_TipoUnidade = new bool[] {false} ;
         CONTAGEMCO2_A2182Contagem_Pendencia = new String[] {""} ;
         CONTAGEMCO2_n2182Contagem_Pendencia = new bool[] {false} ;
         CONTAGEMCO2_A2181Contagem_CntSrvCod = new int[1] ;
         CONTAGEMCO2_n2181Contagem_CntSrvCod = new bool[] {false} ;
         CONTAGEMCO2_A1814Contagem_AmbienteTecnologico = new int[1] ;
         CONTAGEMCO2_n1814Contagem_AmbienteTecnologico = new bool[] {false} ;
         CONTAGEMCO2_A1813Contagem_ReferenciaINM = new int[1] ;
         CONTAGEMCO2_n1813Contagem_ReferenciaINM = new bool[] {false} ;
         CONTAGEMCO2_A1812Contagem_ArquivoImp = new String[] {""} ;
         CONTAGEMCO2_n1812Contagem_ArquivoImp = new bool[] {false} ;
         CONTAGEMCO2_A1811Contagem_ServicoCod = new int[1] ;
         CONTAGEMCO2_n1811Contagem_ServicoCod = new bool[] {false} ;
         CONTAGEMCO2_A1810Contagem_Versao = new String[] {""} ;
         CONTAGEMCO2_n1810Contagem_Versao = new bool[] {false} ;
         CONTAGEMCO2_A1809Contagem_Aplicabilidade = new String[] {""} ;
         CONTAGEMCO2_n1809Contagem_Aplicabilidade = new bool[] {false} ;
         CONTAGEMCO2_A1120Contagem_Descricao = new String[] {""} ;
         CONTAGEMCO2_n1120Contagem_Descricao = new bool[] {false} ;
         CONTAGEMCO2_A1111Contagem_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         CONTAGEMCO2_n1111Contagem_DataHomologacao = new bool[] {false} ;
         CONTAGEMCO2_A1119Contagem_Divergencia = new decimal[1] ;
         CONTAGEMCO2_n1119Contagem_Divergencia = new bool[] {false} ;
         CONTAGEMCO2_A1118Contagem_ContratadaCod = new int[1] ;
         CONTAGEMCO2_n1118Contagem_ContratadaCod = new bool[] {false} ;
         CONTAGEMCO2_A1117Contagem_Deflator = new decimal[1] ;
         CONTAGEMCO2_n1117Contagem_Deflator = new bool[] {false} ;
         CONTAGEMCO2_A1116Contagem_PFLD = new decimal[1] ;
         CONTAGEMCO2_n1116Contagem_PFLD = new bool[] {false} ;
         CONTAGEMCO2_A1115Contagem_PFBD = new decimal[1] ;
         CONTAGEMCO2_n1115Contagem_PFBD = new bool[] {false} ;
         CONTAGEMCO2_A1114Contagem_PFLA = new decimal[1] ;
         CONTAGEMCO2_n1114Contagem_PFLA = new bool[] {false} ;
         CONTAGEMCO2_A1113Contagem_PFBA = new decimal[1] ;
         CONTAGEMCO2_n1113Contagem_PFBA = new bool[] {false} ;
         CONTAGEMCO2_A1112Contagem_Consideracoes = new String[] {""} ;
         CONTAGEMCO2_n1112Contagem_Consideracoes = new bool[] {false} ;
         CONTAGEMCO2_A1059Contagem_Notas = new String[] {""} ;
         CONTAGEMCO2_n1059Contagem_Notas = new bool[] {false} ;
         CONTAGEMCO2_A948Contagem_Lock = new bool[] {false} ;
         CONTAGEMCO2_A939Contagem_ProjetoCod = new int[1] ;
         CONTAGEMCO2_n939Contagem_ProjetoCod = new bool[] {false} ;
         CONTAGEMCO2_A940Contagem_SistemaCod = new int[1] ;
         CONTAGEMCO2_n940Contagem_SistemaCod = new bool[] {false} ;
         CONTAGEMCO2_A947Contagem_Fator = new decimal[1] ;
         CONTAGEMCO2_n947Contagem_Fator = new bool[] {false} ;
         CONTAGEMCO2_A946Contagem_Link = new String[] {""} ;
         CONTAGEMCO2_n946Contagem_Link = new bool[] {false} ;
         CONTAGEMCO2_A945Contagem_Demanda = new String[] {""} ;
         CONTAGEMCO2_n945Contagem_Demanda = new bool[] {false} ;
         CONTAGEMCO2_A944Contagem_PFL = new decimal[1] ;
         CONTAGEMCO2_n944Contagem_PFL = new bool[] {false} ;
         CONTAGEMCO2_A943Contagem_PFB = new decimal[1] ;
         CONTAGEMCO2_n943Contagem_PFB = new bool[] {false} ;
         CONTAGEMCO2_A262Contagem_Status = new String[] {""} ;
         CONTAGEMCO2_n262Contagem_Status = new bool[] {false} ;
         CONTAGEMCO2_A213Contagem_UsuarioContadorCod = new int[1] ;
         CONTAGEMCO2_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         CONTAGEMCO2_A202Contagem_Observacao = new String[] {""} ;
         CONTAGEMCO2_n202Contagem_Observacao = new bool[] {false} ;
         CONTAGEMCO2_A201Contagem_Fronteira = new String[] {""} ;
         CONTAGEMCO2_n201Contagem_Fronteira = new bool[] {false} ;
         CONTAGEMCO2_A200Contagem_Escopo = new String[] {""} ;
         CONTAGEMCO2_n200Contagem_Escopo = new bool[] {false} ;
         CONTAGEMCO2_A199Contagem_Proposito = new String[] {""} ;
         CONTAGEMCO2_n199Contagem_Proposito = new bool[] {false} ;
         CONTAGEMCO2_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         CONTAGEMCO2_A196Contagem_Tipo = new String[] {""} ;
         CONTAGEMCO2_n196Contagem_Tipo = new bool[] {false} ;
         CONTAGEMCO2_A195Contagem_Tecnica = new String[] {""} ;
         CONTAGEMCO2_n195Contagem_Tecnica = new bool[] {false} ;
         CONTAGEMCO2_A193Contagem_AreaTrabalhoCod = new int[1] ;
         CONTAGEMCO2_A192Contagem_Codigo = new int[1] ;
         A2200Contagem_DataAtualizacao = (DateTime)(DateTime.MinValue);
         A2188Contagem_Excluidos = "";
         A2187Contagem_Agrupador = "";
         A2182Contagem_Pendencia = "";
         A1812Contagem_ArquivoImp = "";
         A1810Contagem_Versao = "";
         A1809Contagem_Aplicabilidade = "";
         A1120Contagem_Descricao = "";
         A1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         A1112Contagem_Consideracoes = "";
         A1059Contagem_Notas = "";
         A946Contagem_Link = "";
         A945Contagem_Demanda = "";
         A262Contagem_Status = "";
         A202Contagem_Observacao = "";
         A201Contagem_Fronteira = "";
         A200Contagem_Escopo = "";
         A199Contagem_Proposito = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A196Contagem_Tipo = "";
         A195Contagem_Tecnica = "";
         AV4Contagem_Tecnica = "";
         AV5Contagem_Tipo = "";
         AV6Contagem_DataCriacao = DateTime.MinValue;
         AV7Contagem_Proposito = "";
         AV8Contagem_Escopo = "";
         AV9Contagem_Fronteira = "";
         AV10Contagem_Observacao = "";
         AV12Contagem_Status = "";
         AV15Contagem_Demanda = "";
         AV16Contagem_Link = "";
         AV21Contagem_Notas = "";
         AV22Contagem_Consideracoes = "";
         AV30Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         AV31Contagem_Descricao = "";
         AV32Contagem_Aplicabilidade = "";
         AV33Contagem_Versao = "";
         AV35Contagem_ArquivoImp = "";
         AV39Contagem_Pendencia = "";
         AV44Contagem_Agrupador = "";
         AV45Contagem_Excluidos = "";
         AV57Contagem_DataAtualizacao = (DateTime)(DateTime.MinValue);
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemconversion__default(),
            new Object[][] {
                new Object[] {
               CONTAGEMCO2_A2200Contagem_DataAtualizacao, CONTAGEMCO2_n2200Contagem_DataAtualizacao, CONTAGEMCO2_A2199Contagem_DemandaCod, CONTAGEMCO2_n2199Contagem_DemandaCod, CONTAGEMCO2_A2198Contagem_valorLocPF, CONTAGEMCO2_n2198Contagem_valorLocPF, CONTAGEMCO2_A2197Contagem_ValorTaxaEntregaMlhr, CONTAGEMCO2_n2197Contagem_ValorTaxaEntregaMlhr, CONTAGEMCO2_A2196Contagem_ValorTaxaEntregaDsnv, CONTAGEMCO2_n2196Contagem_ValorTaxaEntregaDsnv,
               CONTAGEMCO2_A2195Contagem_LocPF, CONTAGEMCO2_n2195Contagem_LocPF, CONTAGEMCO2_A2194Contagem_TaxaEntregaMlhr, CONTAGEMCO2_n2194Contagem_TaxaEntregaMlhr, CONTAGEMCO2_A2193Contagem_TaxaEntregaDsnv, CONTAGEMCO2_n2193Contagem_TaxaEntregaDsnv, CONTAGEMCO2_A2192Contagem_PFLAImp, CONTAGEMCO2_n2192Contagem_PFLAImp, CONTAGEMCO2_A2191Contagem_fatorImp, CONTAGEMCO2_n2191Contagem_fatorImp,
               CONTAGEMCO2_A2190Contagem_PFLImp, CONTAGEMCO2_n2190Contagem_PFLImp, CONTAGEMCO2_A2189Contagem_PFBRetrabalho, CONTAGEMCO2_n2189Contagem_PFBRetrabalho, CONTAGEMCO2_A2188Contagem_Excluidos, CONTAGEMCO2_n2188Contagem_Excluidos, CONTAGEMCO2_A2187Contagem_Agrupador, CONTAGEMCO2_n2187Contagem_Agrupador, CONTAGEMCO2_A2186Contagem_Fases, CONTAGEMCO2_n2186Contagem_Fases,
               CONTAGEMCO2_A2185Contagem_Metodologia, CONTAGEMCO2_n2185Contagem_Metodologia, CONTAGEMCO2_A2184Contagem_UnidadeOrganizacional, CONTAGEMCO2_n2184Contagem_UnidadeOrganizacional, CONTAGEMCO2_A2183Contagem_TipoUnidade, CONTAGEMCO2_n2183Contagem_TipoUnidade, CONTAGEMCO2_A2182Contagem_Pendencia, CONTAGEMCO2_n2182Contagem_Pendencia, CONTAGEMCO2_A2181Contagem_CntSrvCod, CONTAGEMCO2_n2181Contagem_CntSrvCod,
               CONTAGEMCO2_A1814Contagem_AmbienteTecnologico, CONTAGEMCO2_n1814Contagem_AmbienteTecnologico, CONTAGEMCO2_A1813Contagem_ReferenciaINM, CONTAGEMCO2_n1813Contagem_ReferenciaINM, CONTAGEMCO2_A1812Contagem_ArquivoImp, CONTAGEMCO2_n1812Contagem_ArquivoImp, CONTAGEMCO2_A1811Contagem_ServicoCod, CONTAGEMCO2_n1811Contagem_ServicoCod, CONTAGEMCO2_A1810Contagem_Versao, CONTAGEMCO2_n1810Contagem_Versao,
               CONTAGEMCO2_A1809Contagem_Aplicabilidade, CONTAGEMCO2_n1809Contagem_Aplicabilidade, CONTAGEMCO2_A1120Contagem_Descricao, CONTAGEMCO2_n1120Contagem_Descricao, CONTAGEMCO2_A1111Contagem_DataHomologacao, CONTAGEMCO2_n1111Contagem_DataHomologacao, CONTAGEMCO2_A1119Contagem_Divergencia, CONTAGEMCO2_n1119Contagem_Divergencia, CONTAGEMCO2_A1118Contagem_ContratadaCod, CONTAGEMCO2_n1118Contagem_ContratadaCod,
               CONTAGEMCO2_A1117Contagem_Deflator, CONTAGEMCO2_n1117Contagem_Deflator, CONTAGEMCO2_A1116Contagem_PFLD, CONTAGEMCO2_n1116Contagem_PFLD, CONTAGEMCO2_A1115Contagem_PFBD, CONTAGEMCO2_n1115Contagem_PFBD, CONTAGEMCO2_A1114Contagem_PFLA, CONTAGEMCO2_n1114Contagem_PFLA, CONTAGEMCO2_A1113Contagem_PFBA, CONTAGEMCO2_n1113Contagem_PFBA,
               CONTAGEMCO2_A1112Contagem_Consideracoes, CONTAGEMCO2_n1112Contagem_Consideracoes, CONTAGEMCO2_A1059Contagem_Notas, CONTAGEMCO2_n1059Contagem_Notas, CONTAGEMCO2_A948Contagem_Lock, CONTAGEMCO2_A939Contagem_ProjetoCod, CONTAGEMCO2_n939Contagem_ProjetoCod, CONTAGEMCO2_A940Contagem_SistemaCod, CONTAGEMCO2_n940Contagem_SistemaCod, CONTAGEMCO2_A947Contagem_Fator,
               CONTAGEMCO2_n947Contagem_Fator, CONTAGEMCO2_A946Contagem_Link, CONTAGEMCO2_n946Contagem_Link, CONTAGEMCO2_A945Contagem_Demanda, CONTAGEMCO2_n945Contagem_Demanda, CONTAGEMCO2_A944Contagem_PFL, CONTAGEMCO2_n944Contagem_PFL, CONTAGEMCO2_A943Contagem_PFB, CONTAGEMCO2_n943Contagem_PFB, CONTAGEMCO2_A262Contagem_Status,
               CONTAGEMCO2_n262Contagem_Status, CONTAGEMCO2_A213Contagem_UsuarioContadorCod, CONTAGEMCO2_n213Contagem_UsuarioContadorCod, CONTAGEMCO2_A202Contagem_Observacao, CONTAGEMCO2_n202Contagem_Observacao, CONTAGEMCO2_A201Contagem_Fronteira, CONTAGEMCO2_n201Contagem_Fronteira, CONTAGEMCO2_A200Contagem_Escopo, CONTAGEMCO2_n200Contagem_Escopo, CONTAGEMCO2_A199Contagem_Proposito,
               CONTAGEMCO2_n199Contagem_Proposito, CONTAGEMCO2_A197Contagem_DataCriacao, CONTAGEMCO2_A196Contagem_Tipo, CONTAGEMCO2_n196Contagem_Tipo, CONTAGEMCO2_A195Contagem_Tecnica, CONTAGEMCO2_n195Contagem_Tecnica, CONTAGEMCO2_A193Contagem_AreaTrabalhoCod, CONTAGEMCO2_A192Contagem_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV50Contagem_TaxaEntregaDsnv ;
      private short AV51Contagem_TaxaEntregaMlhr ;
      private short AV52Contagem_LocPF ;
      private int A2199Contagem_DemandaCod ;
      private int A2198Contagem_valorLocPF ;
      private int A2197Contagem_ValorTaxaEntregaMlhr ;
      private int A2196Contagem_ValorTaxaEntregaDsnv ;
      private int A2195Contagem_LocPF ;
      private int A2194Contagem_TaxaEntregaMlhr ;
      private int A2193Contagem_TaxaEntregaDsnv ;
      private int A2186Contagem_Fases ;
      private int A2185Contagem_Metodologia ;
      private int A2184Contagem_UnidadeOrganizacional ;
      private int A2183Contagem_TipoUnidade ;
      private int A2181Contagem_CntSrvCod ;
      private int A1814Contagem_AmbienteTecnologico ;
      private int A1813Contagem_ReferenciaINM ;
      private int A1811Contagem_ServicoCod ;
      private int A1118Contagem_ContratadaCod ;
      private int A939Contagem_ProjetoCod ;
      private int A940Contagem_SistemaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A192Contagem_Codigo ;
      private int GIGXA0043 ;
      private int AV2Contagem_Codigo ;
      private int AV3Contagem_AreaTrabalhoCod ;
      private int AV11Contagem_UsuarioContadorCod ;
      private int AV18Contagem_SistemaCod ;
      private int AV19Contagem_ProjetoCod ;
      private int AV28Contagem_ContratadaCod ;
      private int AV34Contagem_ServicoCod ;
      private int AV36Contagem_ReferenciaINM ;
      private int AV37Contagem_AmbienteTecnologico ;
      private int AV38Contagem_CntSrvCod ;
      private int AV40Contagem_TipoUnidade ;
      private int AV41Contagem_UnidadeOrganizacional ;
      private int AV42Contagem_Metodologia ;
      private int AV43Contagem_Fases ;
      private int AV56Contagem_DemandaCod ;
      private decimal A2192Contagem_PFLAImp ;
      private decimal A2191Contagem_fatorImp ;
      private decimal A2190Contagem_PFLImp ;
      private decimal A2189Contagem_PFBRetrabalho ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A1117Contagem_Deflator ;
      private decimal A1116Contagem_PFLD ;
      private decimal A1115Contagem_PFBD ;
      private decimal A1114Contagem_PFLA ;
      private decimal A1113Contagem_PFBA ;
      private decimal A947Contagem_Fator ;
      private decimal A944Contagem_PFL ;
      private decimal A943Contagem_PFB ;
      private decimal AV13Contagem_PFB ;
      private decimal AV14Contagem_PFL ;
      private decimal AV17Contagem_Fator ;
      private decimal AV23Contagem_PFBA ;
      private decimal AV24Contagem_PFLA ;
      private decimal AV25Contagem_PFBD ;
      private decimal AV26Contagem_PFLD ;
      private decimal AV27Contagem_Deflator ;
      private decimal AV29Contagem_Divergencia ;
      private decimal AV46Contagem_PFBRetrabalho ;
      private decimal AV47Contagem_PFLImp ;
      private decimal AV48Contagem_fatorImp ;
      private decimal AV49Contagem_PFLAImp ;
      private decimal AV53Contagem_ValorTaxaEntregaDsnv ;
      private decimal AV54Contagem_ValorTaxaEntregaMlhr ;
      private decimal AV55Contagem_valorLocPF ;
      private String cmdBuffer ;
      private String scmdbuf ;
      private String A262Contagem_Status ;
      private String A196Contagem_Tipo ;
      private String A195Contagem_Tecnica ;
      private String AV4Contagem_Tecnica ;
      private String AV5Contagem_Tipo ;
      private String AV12Contagem_Status ;
      private String Gx_emsg ;
      private DateTime A2200Contagem_DataAtualizacao ;
      private DateTime A1111Contagem_DataHomologacao ;
      private DateTime AV30Contagem_DataHomologacao ;
      private DateTime AV57Contagem_DataAtualizacao ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime AV6Contagem_DataCriacao ;
      private bool n2200Contagem_DataAtualizacao ;
      private bool n2199Contagem_DemandaCod ;
      private bool n2198Contagem_valorLocPF ;
      private bool n2197Contagem_ValorTaxaEntregaMlhr ;
      private bool n2196Contagem_ValorTaxaEntregaDsnv ;
      private bool n2195Contagem_LocPF ;
      private bool n2194Contagem_TaxaEntregaMlhr ;
      private bool n2193Contagem_TaxaEntregaDsnv ;
      private bool n2192Contagem_PFLAImp ;
      private bool n2191Contagem_fatorImp ;
      private bool n2190Contagem_PFLImp ;
      private bool n2189Contagem_PFBRetrabalho ;
      private bool n2188Contagem_Excluidos ;
      private bool n2187Contagem_Agrupador ;
      private bool n2186Contagem_Fases ;
      private bool n2185Contagem_Metodologia ;
      private bool n2184Contagem_UnidadeOrganizacional ;
      private bool n2183Contagem_TipoUnidade ;
      private bool n2182Contagem_Pendencia ;
      private bool n2181Contagem_CntSrvCod ;
      private bool n1814Contagem_AmbienteTecnologico ;
      private bool n1813Contagem_ReferenciaINM ;
      private bool n1812Contagem_ArquivoImp ;
      private bool n1811Contagem_ServicoCod ;
      private bool n1810Contagem_Versao ;
      private bool n1809Contagem_Aplicabilidade ;
      private bool n1120Contagem_Descricao ;
      private bool n1111Contagem_DataHomologacao ;
      private bool n1119Contagem_Divergencia ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n1117Contagem_Deflator ;
      private bool n1116Contagem_PFLD ;
      private bool n1115Contagem_PFBD ;
      private bool n1114Contagem_PFLA ;
      private bool n1113Contagem_PFBA ;
      private bool n1112Contagem_Consideracoes ;
      private bool n1059Contagem_Notas ;
      private bool A948Contagem_Lock ;
      private bool n939Contagem_ProjetoCod ;
      private bool n940Contagem_SistemaCod ;
      private bool n947Contagem_Fator ;
      private bool n946Contagem_Link ;
      private bool n945Contagem_Demanda ;
      private bool n944Contagem_PFL ;
      private bool n943Contagem_PFB ;
      private bool n262Contagem_Status ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n202Contagem_Observacao ;
      private bool n201Contagem_Fronteira ;
      private bool n200Contagem_Escopo ;
      private bool n199Contagem_Proposito ;
      private bool n196Contagem_Tipo ;
      private bool n195Contagem_Tecnica ;
      private bool nV4Contagem_Tecnica ;
      private bool nV5Contagem_Tipo ;
      private bool nV7Contagem_Proposito ;
      private bool nV8Contagem_Escopo ;
      private bool nV9Contagem_Fronteira ;
      private bool nV10Contagem_Observacao ;
      private bool nV11Contagem_UsuarioContadorCod ;
      private bool nV12Contagem_Status ;
      private bool nV13Contagem_PFB ;
      private bool nV14Contagem_PFL ;
      private bool nV15Contagem_Demanda ;
      private bool nV16Contagem_Link ;
      private bool nV17Contagem_Fator ;
      private bool nV18Contagem_SistemaCod ;
      private bool nV19Contagem_ProjetoCod ;
      private bool AV20Contagem_Lock ;
      private bool nV21Contagem_Notas ;
      private bool nV22Contagem_Consideracoes ;
      private bool nV23Contagem_PFBA ;
      private bool nV24Contagem_PFLA ;
      private bool nV25Contagem_PFBD ;
      private bool nV26Contagem_PFLD ;
      private bool nV27Contagem_Deflator ;
      private bool nV28Contagem_ContratadaCod ;
      private bool nV29Contagem_Divergencia ;
      private bool nV30Contagem_DataHomologacao ;
      private bool nV31Contagem_Descricao ;
      private bool nV32Contagem_Aplicabilidade ;
      private bool nV33Contagem_Versao ;
      private bool nV34Contagem_ServicoCod ;
      private bool nV35Contagem_ArquivoImp ;
      private bool nV36Contagem_ReferenciaINM ;
      private bool nV37Contagem_AmbienteTecnologico ;
      private bool nV38Contagem_CntSrvCod ;
      private bool nV39Contagem_Pendencia ;
      private bool nV40Contagem_TipoUnidade ;
      private bool nV41Contagem_UnidadeOrganizacional ;
      private bool nV42Contagem_Metodologia ;
      private bool nV43Contagem_Fases ;
      private bool nV44Contagem_Agrupador ;
      private bool nV45Contagem_Excluidos ;
      private bool nV46Contagem_PFBRetrabalho ;
      private bool nV47Contagem_PFLImp ;
      private bool nV48Contagem_fatorImp ;
      private bool nV49Contagem_PFLAImp ;
      private bool nV50Contagem_TaxaEntregaDsnv ;
      private bool nV51Contagem_TaxaEntregaMlhr ;
      private bool nV52Contagem_LocPF ;
      private bool nV53Contagem_ValorTaxaEntregaDsnv ;
      private bool nV54Contagem_ValorTaxaEntregaMlhr ;
      private bool nV55Contagem_valorLocPF ;
      private bool nV56Contagem_DemandaCod ;
      private bool nV57Contagem_DataAtualizacao ;
      private String A2188Contagem_Excluidos ;
      private String A1809Contagem_Aplicabilidade ;
      private String A1112Contagem_Consideracoes ;
      private String A1059Contagem_Notas ;
      private String A202Contagem_Observacao ;
      private String A201Contagem_Fronteira ;
      private String A200Contagem_Escopo ;
      private String A199Contagem_Proposito ;
      private String AV7Contagem_Proposito ;
      private String AV8Contagem_Escopo ;
      private String AV9Contagem_Fronteira ;
      private String AV10Contagem_Observacao ;
      private String AV21Contagem_Notas ;
      private String AV22Contagem_Consideracoes ;
      private String AV32Contagem_Aplicabilidade ;
      private String AV45Contagem_Excluidos ;
      private String A2187Contagem_Agrupador ;
      private String A2182Contagem_Pendencia ;
      private String A1812Contagem_ArquivoImp ;
      private String A1810Contagem_Versao ;
      private String A1120Contagem_Descricao ;
      private String A946Contagem_Link ;
      private String A945Contagem_Demanda ;
      private String AV15Contagem_Demanda ;
      private String AV16Contagem_Link ;
      private String AV31Contagem_Descricao ;
      private String AV33Contagem_Versao ;
      private String AV35Contagem_ArquivoImp ;
      private String AV39Contagem_Pendencia ;
      private String AV44Contagem_Agrupador ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private DateTime[] CONTAGEMCO2_A2200Contagem_DataAtualizacao ;
      private bool[] CONTAGEMCO2_n2200Contagem_DataAtualizacao ;
      private int[] CONTAGEMCO2_A2199Contagem_DemandaCod ;
      private bool[] CONTAGEMCO2_n2199Contagem_DemandaCod ;
      private int[] CONTAGEMCO2_A2198Contagem_valorLocPF ;
      private bool[] CONTAGEMCO2_n2198Contagem_valorLocPF ;
      private int[] CONTAGEMCO2_A2197Contagem_ValorTaxaEntregaMlhr ;
      private bool[] CONTAGEMCO2_n2197Contagem_ValorTaxaEntregaMlhr ;
      private int[] CONTAGEMCO2_A2196Contagem_ValorTaxaEntregaDsnv ;
      private bool[] CONTAGEMCO2_n2196Contagem_ValorTaxaEntregaDsnv ;
      private int[] CONTAGEMCO2_A2195Contagem_LocPF ;
      private bool[] CONTAGEMCO2_n2195Contagem_LocPF ;
      private int[] CONTAGEMCO2_A2194Contagem_TaxaEntregaMlhr ;
      private bool[] CONTAGEMCO2_n2194Contagem_TaxaEntregaMlhr ;
      private int[] CONTAGEMCO2_A2193Contagem_TaxaEntregaDsnv ;
      private bool[] CONTAGEMCO2_n2193Contagem_TaxaEntregaDsnv ;
      private decimal[] CONTAGEMCO2_A2192Contagem_PFLAImp ;
      private bool[] CONTAGEMCO2_n2192Contagem_PFLAImp ;
      private decimal[] CONTAGEMCO2_A2191Contagem_fatorImp ;
      private bool[] CONTAGEMCO2_n2191Contagem_fatorImp ;
      private decimal[] CONTAGEMCO2_A2190Contagem_PFLImp ;
      private bool[] CONTAGEMCO2_n2190Contagem_PFLImp ;
      private decimal[] CONTAGEMCO2_A2189Contagem_PFBRetrabalho ;
      private bool[] CONTAGEMCO2_n2189Contagem_PFBRetrabalho ;
      private String[] CONTAGEMCO2_A2188Contagem_Excluidos ;
      private bool[] CONTAGEMCO2_n2188Contagem_Excluidos ;
      private String[] CONTAGEMCO2_A2187Contagem_Agrupador ;
      private bool[] CONTAGEMCO2_n2187Contagem_Agrupador ;
      private int[] CONTAGEMCO2_A2186Contagem_Fases ;
      private bool[] CONTAGEMCO2_n2186Contagem_Fases ;
      private int[] CONTAGEMCO2_A2185Contagem_Metodologia ;
      private bool[] CONTAGEMCO2_n2185Contagem_Metodologia ;
      private int[] CONTAGEMCO2_A2184Contagem_UnidadeOrganizacional ;
      private bool[] CONTAGEMCO2_n2184Contagem_UnidadeOrganizacional ;
      private int[] CONTAGEMCO2_A2183Contagem_TipoUnidade ;
      private bool[] CONTAGEMCO2_n2183Contagem_TipoUnidade ;
      private String[] CONTAGEMCO2_A2182Contagem_Pendencia ;
      private bool[] CONTAGEMCO2_n2182Contagem_Pendencia ;
      private int[] CONTAGEMCO2_A2181Contagem_CntSrvCod ;
      private bool[] CONTAGEMCO2_n2181Contagem_CntSrvCod ;
      private int[] CONTAGEMCO2_A1814Contagem_AmbienteTecnologico ;
      private bool[] CONTAGEMCO2_n1814Contagem_AmbienteTecnologico ;
      private int[] CONTAGEMCO2_A1813Contagem_ReferenciaINM ;
      private bool[] CONTAGEMCO2_n1813Contagem_ReferenciaINM ;
      private String[] CONTAGEMCO2_A1812Contagem_ArquivoImp ;
      private bool[] CONTAGEMCO2_n1812Contagem_ArquivoImp ;
      private int[] CONTAGEMCO2_A1811Contagem_ServicoCod ;
      private bool[] CONTAGEMCO2_n1811Contagem_ServicoCod ;
      private String[] CONTAGEMCO2_A1810Contagem_Versao ;
      private bool[] CONTAGEMCO2_n1810Contagem_Versao ;
      private String[] CONTAGEMCO2_A1809Contagem_Aplicabilidade ;
      private bool[] CONTAGEMCO2_n1809Contagem_Aplicabilidade ;
      private String[] CONTAGEMCO2_A1120Contagem_Descricao ;
      private bool[] CONTAGEMCO2_n1120Contagem_Descricao ;
      private DateTime[] CONTAGEMCO2_A1111Contagem_DataHomologacao ;
      private bool[] CONTAGEMCO2_n1111Contagem_DataHomologacao ;
      private decimal[] CONTAGEMCO2_A1119Contagem_Divergencia ;
      private bool[] CONTAGEMCO2_n1119Contagem_Divergencia ;
      private int[] CONTAGEMCO2_A1118Contagem_ContratadaCod ;
      private bool[] CONTAGEMCO2_n1118Contagem_ContratadaCod ;
      private decimal[] CONTAGEMCO2_A1117Contagem_Deflator ;
      private bool[] CONTAGEMCO2_n1117Contagem_Deflator ;
      private decimal[] CONTAGEMCO2_A1116Contagem_PFLD ;
      private bool[] CONTAGEMCO2_n1116Contagem_PFLD ;
      private decimal[] CONTAGEMCO2_A1115Contagem_PFBD ;
      private bool[] CONTAGEMCO2_n1115Contagem_PFBD ;
      private decimal[] CONTAGEMCO2_A1114Contagem_PFLA ;
      private bool[] CONTAGEMCO2_n1114Contagem_PFLA ;
      private decimal[] CONTAGEMCO2_A1113Contagem_PFBA ;
      private bool[] CONTAGEMCO2_n1113Contagem_PFBA ;
      private String[] CONTAGEMCO2_A1112Contagem_Consideracoes ;
      private bool[] CONTAGEMCO2_n1112Contagem_Consideracoes ;
      private String[] CONTAGEMCO2_A1059Contagem_Notas ;
      private bool[] CONTAGEMCO2_n1059Contagem_Notas ;
      private bool[] CONTAGEMCO2_A948Contagem_Lock ;
      private int[] CONTAGEMCO2_A939Contagem_ProjetoCod ;
      private bool[] CONTAGEMCO2_n939Contagem_ProjetoCod ;
      private int[] CONTAGEMCO2_A940Contagem_SistemaCod ;
      private bool[] CONTAGEMCO2_n940Contagem_SistemaCod ;
      private decimal[] CONTAGEMCO2_A947Contagem_Fator ;
      private bool[] CONTAGEMCO2_n947Contagem_Fator ;
      private String[] CONTAGEMCO2_A946Contagem_Link ;
      private bool[] CONTAGEMCO2_n946Contagem_Link ;
      private String[] CONTAGEMCO2_A945Contagem_Demanda ;
      private bool[] CONTAGEMCO2_n945Contagem_Demanda ;
      private decimal[] CONTAGEMCO2_A944Contagem_PFL ;
      private bool[] CONTAGEMCO2_n944Contagem_PFL ;
      private decimal[] CONTAGEMCO2_A943Contagem_PFB ;
      private bool[] CONTAGEMCO2_n943Contagem_PFB ;
      private String[] CONTAGEMCO2_A262Contagem_Status ;
      private bool[] CONTAGEMCO2_n262Contagem_Status ;
      private int[] CONTAGEMCO2_A213Contagem_UsuarioContadorCod ;
      private bool[] CONTAGEMCO2_n213Contagem_UsuarioContadorCod ;
      private String[] CONTAGEMCO2_A202Contagem_Observacao ;
      private bool[] CONTAGEMCO2_n202Contagem_Observacao ;
      private String[] CONTAGEMCO2_A201Contagem_Fronteira ;
      private bool[] CONTAGEMCO2_n201Contagem_Fronteira ;
      private String[] CONTAGEMCO2_A200Contagem_Escopo ;
      private bool[] CONTAGEMCO2_n200Contagem_Escopo ;
      private String[] CONTAGEMCO2_A199Contagem_Proposito ;
      private bool[] CONTAGEMCO2_n199Contagem_Proposito ;
      private DateTime[] CONTAGEMCO2_A197Contagem_DataCriacao ;
      private String[] CONTAGEMCO2_A196Contagem_Tipo ;
      private bool[] CONTAGEMCO2_n196Contagem_Tipo ;
      private String[] CONTAGEMCO2_A195Contagem_Tecnica ;
      private bool[] CONTAGEMCO2_n195Contagem_Tecnica ;
      private int[] CONTAGEMCO2_A193Contagem_AreaTrabalhoCod ;
      private int[] CONTAGEMCO2_A192Contagem_Codigo ;
   }

   public class contagemconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmCONTAGEMCO2 ;
          prmCONTAGEMCO2 = new Object[] {
          } ;
          Object[] prmCONTAGEMCO3 ;
          prmCONTAGEMCO3 = new Object[] {
          new Object[] {"@AV2Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV3Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV4Contagem_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@AV5Contagem_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@AV6Contagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV7Contagem_Proposito",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV8Contagem_Escopo",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV9Contagem_Fronteira",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV10Contagem_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV11Contagem_UsuarioContadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@AV13Contagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV14Contagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV15Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV16Contagem_Link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV17Contagem_Fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV18Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Contagem_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20Contagem_Lock",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV21Contagem_Notas",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV22Contagem_Consideracoes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV23Contagem_PFBA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV24Contagem_PFLA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV25Contagem_PFBD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26Contagem_PFLD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV27Contagem_Deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV28Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV30Contagem_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31Contagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV32Contagem_Aplicabilidade",SqlDbType.VarChar,1048576,0} ,
          new Object[] {"@AV33Contagem_Versao",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV34Contagem_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35Contagem_ArquivoImp",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV36Contagem_ReferenciaINM",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37Contagem_AmbienteTecnologico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38Contagem_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39Contagem_Pendencia",SqlDbType.VarChar,1,0} ,
          new Object[] {"@AV40Contagem_TipoUnidade",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Contagem_UnidadeOrganizacional",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42Contagem_Metodologia",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43Contagem_Fases",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44Contagem_Agrupador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV45Contagem_Excluidos",SqlDbType.VarChar,16,0} ,
          new Object[] {"@AV46Contagem_PFBRetrabalho",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV47Contagem_PFLImp",SqlDbType.Decimal,10,4} ,
          new Object[] {"@AV48Contagem_fatorImp",SqlDbType.Decimal,10,4} ,
          new Object[] {"@AV49Contagem_PFLAImp",SqlDbType.Decimal,10,4} ,
          new Object[] {"@AV50Contagem_TaxaEntregaDsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51Contagem_TaxaEntregaMlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV52Contagem_LocPF",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV53Contagem_ValorTaxaEntregaDsnv",SqlDbType.Decimal,18,6} ,
          new Object[] {"@AV54Contagem_ValorTaxaEntregaMlhr",SqlDbType.Decimal,18,6} ,
          new Object[] {"@AV55Contagem_valorLocPF",SqlDbType.Decimal,18,6} ,
          new Object[] {"@AV56Contagem_DemandaCod",SqlDbType.Int,9,0} ,
          new Object[] {"@AV57Contagem_DataAtualizacao",SqlDbType.DateTime,10,8}
          } ;
          String cmdBufferCONTAGEMCO3 ;
          cmdBufferCONTAGEMCO3=" INSERT INTO [GXA0043]([Contagem_Codigo], [Contagem_AreaTrabalhoCod], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_DataCriacao], [Contagem_Proposito], [Contagem_Escopo], [Contagem_Fronteira], [Contagem_Observacao], [Contagem_UsuarioContadorCod], [Contagem_Status], [Contagem_PFB], [Contagem_PFL], [Contagem_Demanda], [Contagem_Link], [Contagem_Fator], [Contagem_SistemaCod], [Contagem_ProjetoCod], [Contagem_Lock], [Contagem_Notas], [Contagem_Consideracoes], [Contagem_PFBA], [Contagem_PFLA], [Contagem_PFBD], [Contagem_PFLD], [Contagem_Deflator], [Contagem_ContratadaCod], [Contagem_Divergencia], [Contagem_DataHomologacao], [Contagem_Descricao], [Contagem_Aplicabilidade], [Contagem_Versao], [Contagem_ServicoCod], [Contagem_ArquivoImp], [Contagem_ReferenciaINM], [Contagem_AmbienteTecnologico], [Contagem_CntSrvCod], [Contagem_Pendencia], [Contagem_TipoUnidade], [Contagem_UnidadeOrganizacional], [Contagem_Metodologia], [Contagem_Fases], [Contagem_Agrupador], [Contagem_Excluidos], [Contagem_PFBRetrabalho], [Contagem_PFLImp], [Contagem_fatorImp], [Contagem_PFLAImp], [Contagem_TaxaEntregaDsnv], [Contagem_TaxaEntregaMlhr], [Contagem_LocPF], [Contagem_ValorTaxaEntregaDsnv], [Contagem_ValorTaxaEntregaMlhr], [Contagem_valorLocPF], [Contagem_DemandaCod], [Contagem_DataAtualizacao]) VALUES(@AV2Contagem_Codigo, @AV3Contagem_AreaTrabalhoCod, @AV4Contagem_Tecnica, @AV5Contagem_Tipo, @AV6Contagem_DataCriacao, @AV7Contagem_Proposito, @AV8Contagem_Escopo, @AV9Contagem_Fronteira, @AV10Contagem_Observacao, @AV11Contagem_UsuarioContadorCod, @AV12Contagem_Status, @AV13Contagem_PFB, @AV14Contagem_PFL, @AV15Contagem_Demanda, @AV16Contagem_Link, @AV17Contagem_Fator, @AV18Contagem_SistemaCod, @AV19Contagem_ProjetoCod, @AV20Contagem_Lock, @AV21Contagem_Notas, @AV22Contagem_Consideracoes, @AV23Contagem_PFBA, "
          + " @AV24Contagem_PFLA, @AV25Contagem_PFBD, @AV26Contagem_PFLD, @AV27Contagem_Deflator, @AV28Contagem_ContratadaCod, @AV29Contagem_Divergencia, @AV30Contagem_DataHomologacao, @AV31Contagem_Descricao, @AV32Contagem_Aplicabilidade, @AV33Contagem_Versao, @AV34Contagem_ServicoCod, @AV35Contagem_ArquivoImp, @AV36Contagem_ReferenciaINM, @AV37Contagem_AmbienteTecnologico, @AV38Contagem_CntSrvCod, @AV39Contagem_Pendencia, @AV40Contagem_TipoUnidade, @AV41Contagem_UnidadeOrganizacional, @AV42Contagem_Metodologia, @AV43Contagem_Fases, @AV44Contagem_Agrupador, @AV45Contagem_Excluidos, @AV46Contagem_PFBRetrabalho, @AV47Contagem_PFLImp, @AV48Contagem_fatorImp, @AV49Contagem_PFLAImp, @AV50Contagem_TaxaEntregaDsnv, @AV51Contagem_TaxaEntregaMlhr, @AV52Contagem_LocPF, @AV53Contagem_ValorTaxaEntregaDsnv, @AV54Contagem_ValorTaxaEntregaMlhr, @AV55Contagem_valorLocPF, @AV56Contagem_DemandaCod, @AV57Contagem_DataAtualizacao)" ;
          def= new CursorDef[] {
              new CursorDef("CONTAGEMCO2", "SELECT [Contagem_DataAtualizacao], [Contagem_DemandaCod], [Contagem_valorLocPF], [Contagem_ValorTaxaEntregaMlhr], [Contagem_ValorTaxaEntregaDsnv], [Contagem_LocPF], [Contagem_TaxaEntregaMlhr], [Contagem_TaxaEntregaDsnv], [Contagem_PFLAImp], [Contagem_fatorImp], [Contagem_PFLImp], [Contagem_PFBRetrabalho], [Contagem_Excluidos], [Contagem_Agrupador], [Contagem_Fases], [Contagem_Metodologia], [Contagem_UnidadeOrganizacional], [Contagem_TipoUnidade], [Contagem_Pendencia], [Contagem_CntSrvCod], [Contagem_AmbienteTecnologico], [Contagem_ReferenciaINM], [Contagem_ArquivoImp], [Contagem_ServicoCod], [Contagem_Versao], [Contagem_Aplicabilidade], [Contagem_Descricao], [Contagem_DataHomologacao], [Contagem_Divergencia], [Contagem_ContratadaCod], [Contagem_Deflator], [Contagem_PFLD], [Contagem_PFBD], [Contagem_PFLA], [Contagem_PFBA], [Contagem_Consideracoes], [Contagem_Notas], [Contagem_Lock], [Contagem_ProjetoCod], [Contagem_SistemaCod], [Contagem_Fator], [Contagem_Link], [Contagem_Demanda], [Contagem_PFL], [Contagem_PFB], [Contagem_Status], [Contagem_UsuarioContadorCod], [Contagem_Observacao], [Contagem_Fronteira], [Contagem_Escopo], [Contagem_Proposito], [Contagem_DataCriacao], [Contagem_Tipo], [Contagem_Tecnica], [Contagem_AreaTrabalhoCod], [Contagem_Codigo] FROM [Contagem] ORDER BY [Contagem_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmCONTAGEMCO2,100,0,true,false )
             ,new CursorDef("CONTAGEMCO3", cmdBufferCONTAGEMCO3, GxErrorMask.GX_NOMASK,prmCONTAGEMCO3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((int[]) buf[32])[0] = rslt.getInt(17) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(17);
                ((int[]) buf[34])[0] = rslt.getInt(18) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(18);
                ((String[]) buf[36])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(19);
                ((int[]) buf[38])[0] = rslt.getInt(20) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(20);
                ((int[]) buf[40])[0] = rslt.getInt(21) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(21);
                ((int[]) buf[42])[0] = rslt.getInt(22) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(22);
                ((String[]) buf[44])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(23);
                ((int[]) buf[46])[0] = rslt.getInt(24) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(24);
                ((String[]) buf[48])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(25);
                ((String[]) buf[50])[0] = rslt.getLongVarchar(26) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(26);
                ((String[]) buf[52])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(27);
                ((DateTime[]) buf[54])[0] = rslt.getGXDateTime(28) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(28);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(29) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(29);
                ((int[]) buf[58])[0] = rslt.getInt(30) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(30);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(31);
                ((decimal[]) buf[62])[0] = rslt.getDecimal(32) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(32);
                ((decimal[]) buf[64])[0] = rslt.getDecimal(33) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(33);
                ((decimal[]) buf[66])[0] = rslt.getDecimal(34) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(34);
                ((decimal[]) buf[68])[0] = rslt.getDecimal(35) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(35);
                ((String[]) buf[70])[0] = rslt.getLongVarchar(36) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(36);
                ((String[]) buf[72])[0] = rslt.getLongVarchar(37) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(37);
                ((bool[]) buf[74])[0] = rslt.getBool(38) ;
                ((int[]) buf[75])[0] = rslt.getInt(39) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(39);
                ((int[]) buf[77])[0] = rslt.getInt(40) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(40);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(41);
                ((String[]) buf[81])[0] = rslt.getVarchar(42) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(42);
                ((String[]) buf[83])[0] = rslt.getVarchar(43) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(43);
                ((decimal[]) buf[85])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(44);
                ((decimal[]) buf[87])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(45);
                ((String[]) buf[89])[0] = rslt.getString(46, 1) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(46);
                ((int[]) buf[91])[0] = rslt.getInt(47) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(47);
                ((String[]) buf[93])[0] = rslt.getLongVarchar(48) ;
                ((bool[]) buf[94])[0] = rslt.wasNull(48);
                ((String[]) buf[95])[0] = rslt.getLongVarchar(49) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(49);
                ((String[]) buf[97])[0] = rslt.getLongVarchar(50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(50);
                ((String[]) buf[99])[0] = rslt.getLongVarchar(51) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(51);
                ((DateTime[]) buf[101])[0] = rslt.getGXDate(52) ;
                ((String[]) buf[102])[0] = rslt.getString(53, 1) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(53);
                ((String[]) buf[104])[0] = rslt.getString(54, 1) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(54);
                ((int[]) buf[106])[0] = rslt.getInt(55) ;
                ((int[]) buf[107])[0] = rslt.getInt(56) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (DateTime)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[32]);
                }
                stmt.SetParameter(19, (bool)parms[33]);
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 26 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(26, (decimal)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 28 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(28, (decimal)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 29 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(29, (DateTime)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 30 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(30, (String)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 31 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(31, (String)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 32 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(32, (String)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 34 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(34, (String)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 35 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(35, (int)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 36 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(36, (int)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 37 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(37, (int)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 38 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(38, (String)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 39 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(39, (int)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 40 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(40, (int)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 41 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(41, (int)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 42 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(42, (int)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 43 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(43, (String)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 44 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(44, (String)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 45 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(45, (decimal)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 46 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(46, (decimal)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 47 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(47, (decimal)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 48 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(48, (decimal)parms[91]);
                }
                if ( (bool)parms[92] )
                {
                   stmt.setNull( 49 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(49, (short)parms[93]);
                }
                if ( (bool)parms[94] )
                {
                   stmt.setNull( 50 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(50, (short)parms[95]);
                }
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 51 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(51, (short)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 52 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(52, (decimal)parms[99]);
                }
                if ( (bool)parms[100] )
                {
                   stmt.setNull( 53 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(53, (decimal)parms[101]);
                }
                if ( (bool)parms[102] )
                {
                   stmt.setNull( 54 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(54, (decimal)parms[103]);
                }
                if ( (bool)parms[104] )
                {
                   stmt.setNull( 55 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(55, (int)parms[105]);
                }
                if ( (bool)parms[106] )
                {
                   stmt.setNull( 56 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(56, (DateTime)parms[107]);
                }
                return;
       }
    }

 }

}
