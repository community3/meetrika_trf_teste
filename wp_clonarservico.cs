/*
               File: WP_ClonarServico
        Description: Clonar Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:41:42.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_clonarservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_clonarservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_clonarservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServico_escolhido = new GXCombobox();
         cmbavServico_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8ContratoServicos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoServicos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicos_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAK92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTK92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299414281");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_clonarservico.aspx") + "?" + UrlEncode("" +AV8ContratoServicos_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_ALIAS", StringUtil.RTrim( A1858ContratoServicos_Alias));
         GxWebStd.gx_boolean_hidden_field( context, "vEXISTE", AV11Existe);
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicos_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEK92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTK92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_clonarservico.aspx") + "?" + UrlEncode("" +AV8ContratoServicos_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ClonarServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Clonar Servi�o" ;
      }

      protected void WBK90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_K92( true) ;
         }
         else
         {
            wb_table1_2_K92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_K92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_ClonarServico.htm");
         }
         wbLoad = true;
      }

      protected void STARTK92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Clonar Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPK90( ) ;
      }

      protected void WSK92( )
      {
         STARTK92( ) ;
         EVTK92( ) ;
      }

      protected void EVTK92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11K92 */
                              E11K92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VALIAS.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12K92 */
                              E12K92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13K92 */
                                    E13K92 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14K92 */
                              E14K92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15K92 */
                              E15K92 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEK92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAK92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavServico_escolhido.Name = "vSERVICO_ESCOLHIDO";
            dynavServico_escolhido.WebTags = "";
            dynavServico_escolhido.removeAllItems();
            /* Using cursor H00K92 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavServico_escolhido.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00K92_A155Servico_Codigo[0]), 6, 0)), H00K92_A605Servico_Sigla[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavServico_escolhido.ItemCount > 0 )
            {
               AV7Servico_Escolhido = (int)(NumberUtil.Val( dynavServico_escolhido.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Escolhido", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
            }
            cmbavServico_codigo.Name = "vSERVICO_CODIGO";
            cmbavServico_codigo.WebTags = "";
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavServico_codigo.ItemCount > 0 )
            {
               AV6Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavServico_escolhido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICO_ESCOLHIDOK91( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_ESCOLHIDO_dataK91( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_ESCOLHIDO_htmlK91( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_ESCOLHIDO_dataK91( ) ;
         gxdynajaxindex = 1;
         dynavServico_escolhido.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_escolhido.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_escolhido.ItemCount > 0 )
         {
            AV7Servico_Escolhido = (int)(NumberUtil.Val( dynavServico_escolhido.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Escolhido", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
         }
      }

      protected void GXDLVvSERVICO_ESCOLHIDO_dataK91( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00K93 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K93_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K93_A605Servico_Sigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServico_escolhido.ItemCount > 0 )
         {
            AV7Servico_Escolhido = (int)(NumberUtil.Val( dynavServico_escolhido.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Escolhido", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
         }
         if ( cmbavServico_codigo.ItemCount > 0 )
         {
            AV6Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFK92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavServico_escolhido.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_escolhido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_escolhido.Enabled), 5, 0)));
      }

      protected void RFK92( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15K92 */
            E15K92 ();
            WBK90( ) ;
         }
      }

      protected void STRUPK90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         dynavServico_escolhido.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_escolhido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_escolhido.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11K92 */
         E11K92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavServico_escolhido.CurrentValue = cgiGet( dynavServico_escolhido_Internalname);
            AV7Servico_Escolhido = (int)(NumberUtil.Val( cgiGet( dynavServico_escolhido_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Escolhido", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
            cmbavServico_codigo.CurrentValue = cgiGet( cmbavServico_codigo_Internalname);
            AV6Servico_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0)));
            AV10Alias = StringUtil.Upper( cgiGet( edtavAlias_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Alias", AV10Alias);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11K92 */
         E11K92 ();
         if (returnInSub) return;
      }

      protected void E11K92( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden';"+"</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         /* Using cursor H00K94 */
         pr_default.execute(2, new Object[] {AV8ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A160ContratoServicos_Codigo = H00K94_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = H00K94_A74Contrato_Codigo[0];
            A155Servico_Codigo = H00K94_A155Servico_Codigo[0];
            AV9Contrato_Codigo = A74Contrato_Codigo;
            AV7Servico_Escolhido = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Escolhido", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_ESCOLHIDO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Escolhido), "ZZZZZ9")));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor H00K95 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            A632Servico_Ativo = H00K95_A632Servico_Ativo[0];
            A155Servico_Codigo = H00K95_A155Servico_Codigo[0];
            A605Servico_Sigla = H00K95_A605Servico_Sigla[0];
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), A605Servico_Sigla, 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void E12K92( )
      {
         /* Alias_Isvalid Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Alias)) )
         {
            AV16GXLvl24 = 0;
            /* Using cursor H00K96 */
            pr_default.execute(4, new Object[] {AV6Servico_Codigo, AV10Alias});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1858ContratoServicos_Alias = H00K96_A1858ContratoServicos_Alias[0];
               n1858ContratoServicos_Alias = H00K96_n1858ContratoServicos_Alias[0];
               A155Servico_Codigo = H00K96_A155Servico_Codigo[0];
               AV16GXLvl24 = 1;
               AV11Existe = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Existe", AV11Existe);
               pr_default.readNext(4);
            }
            pr_default.close(4);
            if ( AV16GXLvl24 == 0 )
            {
               AV11Existe = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Existe", AV11Existe);
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E13K92 */
         E13K92 ();
         if (returnInSub) return;
      }

      protected void E13K92( )
      {
         /* Enter Routine */
         if ( (0==AV6Servico_Codigo) )
         {
            GX_msglist.addItem("Selecione o servi�o que deseja alocar neste contrato!");
         }
         else if ( AV11Existe )
         {
            GX_msglist.addItem("J� existe esse servi�o com mesmo alias!");
         }
         else
         {
            new prc_clonarservico(context ).execute(  AV8ContratoServicos_Codigo,  AV6Servico_Codigo,  AV10Alias) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicos_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Alias", AV10Alias);
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14K92( )
      {
         /* 'Fechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E15K92( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_K92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(25), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(35), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class=''>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Usando", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class=''>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_escolhido, dynavServico_escolhido_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0)), 1, dynavServico_escolhido_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavServico_escolhido.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_WP_ClonarServico.htm");
            dynavServico_escolhido.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7Servico_Escolhido), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_escolhido_Internalname, "Values", (String)(dynavServico_escolhido.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom2_Internalname, "Criar", "", "", lblTextblockcontratada_pessoanom2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class=''>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_codigo, cmbavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0)), 1, cmbavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_WP_ClonarServico.htm");
            cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", (String)(cmbavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom3_Internalname, "Com alias", "", "", lblTextblockcontratada_pessoanom3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAlias_Internalname, StringUtil.RTrim( AV10Alias), StringUtil.RTrim( context.localUtil.Format( AV10Alias, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ClonarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ClonarServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'FECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ClonarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_K92e( true) ;
         }
         else
         {
            wb_table1_2_K92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicos_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAK92( ) ;
         WSK92( ) ;
         WEK92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299414324");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_clonarservico.js", "?20205299414324");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         dynavServico_escolhido_Internalname = "vSERVICO_ESCOLHIDO";
         lblTextblockcontratada_pessoanom2_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM2";
         cmbavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblockcontratada_pessoanom3_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM3";
         edtavAlias_Internalname = "vALIAS";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavAlias_Jsonclick = "";
         cmbavServico_codigo_Jsonclick = "";
         dynavServico_escolhido_Jsonclick = "";
         dynavServico_escolhido.Enabled = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Clonar Servi�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VALIAS.ISVALID","{handler:'E12K92',iparms:[{av:'AV10Alias',fld:'vALIAS',pic:'@!',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1858ContratoServicos_Alias',fld:'CONTRATOSERVICOS_ALIAS',pic:'@!',nv:''}],oparms:[{av:'AV11Existe',fld:'vEXISTE',pic:'',nv:false}]}");
         setEventMetadata("ENTER","{handler:'E13K92',iparms:[{av:'AV6Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11Existe',fld:'vEXISTE',pic:'',nv:false},{av:'AV8ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV10Alias',fld:'vALIAS',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'FECHAR'","{handler:'E14K92',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1858ContratoServicos_Alias = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00K92_A155Servico_Codigo = new int[1] ;
         H00K92_A605Servico_Sigla = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00K93_A155Servico_Codigo = new int[1] ;
         H00K93_A605Servico_Sigla = new String[] {""} ;
         AV10Alias = "";
         H00K94_A160ContratoServicos_Codigo = new int[1] ;
         H00K94_A74Contrato_Codigo = new int[1] ;
         H00K94_A155Servico_Codigo = new int[1] ;
         H00K95_A632Servico_Ativo = new bool[] {false} ;
         H00K95_A155Servico_Codigo = new int[1] ;
         H00K95_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         H00K96_A160ContratoServicos_Codigo = new int[1] ;
         H00K96_A1858ContratoServicos_Alias = new String[] {""} ;
         H00K96_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00K96_A155Servico_Codigo = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         TempTags = "";
         lblTextblockcontratada_pessoanom2_Jsonclick = "";
         lblTextblockcontratada_pessoanom3_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_clonarservico__default(),
            new Object[][] {
                new Object[] {
               H00K92_A155Servico_Codigo, H00K92_A605Servico_Sigla
               }
               , new Object[] {
               H00K93_A155Servico_Codigo, H00K93_A605Servico_Sigla
               }
               , new Object[] {
               H00K94_A160ContratoServicos_Codigo, H00K94_A74Contrato_Codigo, H00K94_A155Servico_Codigo
               }
               , new Object[] {
               H00K95_A632Servico_Ativo, H00K95_A155Servico_Codigo, H00K95_A605Servico_Sigla
               }
               , new Object[] {
               H00K96_A160ContratoServicos_Codigo, H00K96_A1858ContratoServicos_Alias, H00K96_n1858ContratoServicos_Alias, H00K96_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavServico_escolhido.Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV16GXLvl24 ;
      private short nGXWrapped ;
      private int AV8ContratoServicos_Codigo ;
      private int wcpOAV8ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int AV7Servico_Escolhido ;
      private int lblTbjava_Visible ;
      private int AV6Servico_Codigo ;
      private int gxdynajaxindex ;
      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV9Contrato_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1858ContratoServicos_Alias ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String dynavServico_escolhido_Internalname ;
      private String gxwrpcisep ;
      private String cmbavServico_codigo_Internalname ;
      private String AV10Alias ;
      private String edtavAlias_Internalname ;
      private String A605Servico_Sigla ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String TempTags ;
      private String dynavServico_escolhido_Jsonclick ;
      private String lblTextblockcontratada_pessoanom2_Internalname ;
      private String lblTextblockcontratada_pessoanom2_Jsonclick ;
      private String cmbavServico_codigo_Jsonclick ;
      private String lblTextblockcontratada_pessoanom3_Internalname ;
      private String lblTextblockcontratada_pessoanom3_Jsonclick ;
      private String edtavAlias_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV11Existe ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A632Servico_Ativo ;
      private bool n1858ContratoServicos_Alias ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavServico_escolhido ;
      private GXCombobox cmbavServico_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00K92_A155Servico_Codigo ;
      private String[] H00K92_A605Servico_Sigla ;
      private int[] H00K93_A155Servico_Codigo ;
      private String[] H00K93_A605Servico_Sigla ;
      private int[] H00K94_A160ContratoServicos_Codigo ;
      private int[] H00K94_A74Contrato_Codigo ;
      private int[] H00K94_A155Servico_Codigo ;
      private bool[] H00K95_A632Servico_Ativo ;
      private int[] H00K95_A155Servico_Codigo ;
      private String[] H00K95_A605Servico_Sigla ;
      private int[] H00K96_A160ContratoServicos_Codigo ;
      private String[] H00K96_A1858ContratoServicos_Alias ;
      private bool[] H00K96_n1858ContratoServicos_Alias ;
      private int[] H00K96_A155Servico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_clonarservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00K92 ;
          prmH00K92 = new Object[] {
          } ;
          Object[] prmH00K93 ;
          prmH00K93 = new Object[] {
          } ;
          Object[] prmH00K94 ;
          prmH00K94 = new Object[] {
          new Object[] {"@AV8ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K95 ;
          prmH00K95 = new Object[] {
          } ;
          Object[] prmH00K96 ;
          prmH00K96 = new Object[] {
          new Object[] {"@AV6Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Alias",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00K92", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K92,0,0,true,false )
             ,new CursorDef("H00K93", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K93,0,0,true,false )
             ,new CursorDef("H00K94", "SELECT TOP 1 [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV8ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K94,1,0,false,true )
             ,new CursorDef("H00K95", "SELECT [Servico_Ativo], [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Ativo] = 1 ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K95,100,0,false,false )
             ,new CursorDef("H00K96", "SELECT [ContratoServicos_Codigo], [ContratoServicos_Alias], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Servico_Codigo] = @AV6Servico_Codigo) AND ([ContratoServicos_Alias] = @AV10Alias) ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K96,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
