/*
               File: PRC_SetUltimaArea
        Description: Set Ultima Area do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:8.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setultimaarea : GXProcedure
   {
      public prc_setultimaarea( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setultimaarea( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo ,
                           int aP1_UltimaArea )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8UltimaArea = aP1_UltimaArea;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo ,
                                 int aP1_UltimaArea )
      {
         prc_setultimaarea objprc_setultimaarea;
         objprc_setultimaarea = new prc_setultimaarea();
         objprc_setultimaarea.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_setultimaarea.AV8UltimaArea = aP1_UltimaArea;
         objprc_setultimaarea.context.SetSubmitInitialConfig(context);
         objprc_setultimaarea.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setultimaarea);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setultimaarea)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00W72 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2016Usuario_UltimaArea = P00W72_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = P00W72_n2016Usuario_UltimaArea[0];
            A2016Usuario_UltimaArea = AV8UltimaArea;
            n2016Usuario_UltimaArea = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00W73 */
            pr_default.execute(1, new Object[] {n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A1Usuario_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
            if (true) break;
            /* Using cursor P00W74 */
            pr_default.execute(2, new Object[] {n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A1Usuario_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SetUltimaArea");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00W72_A1Usuario_Codigo = new int[1] ;
         P00W72_A2016Usuario_UltimaArea = new int[1] ;
         P00W72_n2016Usuario_UltimaArea = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setultimaarea__default(),
            new Object[][] {
                new Object[] {
               P00W72_A1Usuario_Codigo, P00W72_A2016Usuario_UltimaArea, P00W72_n2016Usuario_UltimaArea
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private int AV8UltimaArea ;
      private int A2016Usuario_UltimaArea ;
      private String scmdbuf ;
      private bool n2016Usuario_UltimaArea ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00W72_A1Usuario_Codigo ;
      private int[] P00W72_A2016Usuario_UltimaArea ;
      private bool[] P00W72_n2016Usuario_UltimaArea ;
   }

   public class prc_setultimaarea__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W72 ;
          prmP00W72 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W73 ;
          prmP00W73 = new Object[] {
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W74 ;
          prmP00W74 = new Object[] {
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W72", "SELECT TOP 1 [Usuario_Codigo], [Usuario_UltimaArea] FROM [Usuario] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W72,1,0,true,true )
             ,new CursorDef("P00W73", "UPDATE [Usuario] SET [Usuario_UltimaArea]=@Usuario_UltimaArea  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W73)
             ,new CursorDef("P00W74", "UPDATE [Usuario] SET [Usuario_UltimaArea]=@Usuario_UltimaArea  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W74)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
