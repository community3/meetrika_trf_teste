/*
               File: PRC_ImportarContagens
        Description: Importar Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:14.7
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_importarcontagens : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV92OQImportar = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV78ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV94Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV8Arquivo = GetNextPar( );
                  AV84Aba = GetNextPar( );
                  AV16ColDmnn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV95DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV60ColDataCntn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV44PraLinha = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV9ColContadorFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV87ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV57ColPFBFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV58ColPFLFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV17ColPFBFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV18ColPFLFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV98ColParecern = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV85Final = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV90FileName = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_importarcontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_importarcontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_OQImportar ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           int aP2_Servico ,
                           String aP3_Arquivo ,
                           String aP4_Aba ,
                           short aP5_ColDmnn ,
                           DateTime aP6_DataCnt ,
                           short aP7_ColDataCntn ,
                           short aP8_PraLinha ,
                           short aP9_ColContadorFMn ,
                           int aP10_ContagemResultado_ContadorFMCod ,
                           short aP11_ColPFBFSn ,
                           short aP12_ColPFLFSn ,
                           short aP13_ColPFBFMn ,
                           short aP14_ColPFLFMn ,
                           short aP15_ColParecern ,
                           bool aP16_Final ,
                           String aP17_FileName )
      {
         this.AV92OQImportar = aP0_OQImportar;
         this.AV78ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV94Servico = aP2_Servico;
         this.AV8Arquivo = aP3_Arquivo;
         this.AV84Aba = aP4_Aba;
         this.AV16ColDmnn = aP5_ColDmnn;
         this.AV95DataCnt = aP6_DataCnt;
         this.AV60ColDataCntn = aP7_ColDataCntn;
         this.AV44PraLinha = aP8_PraLinha;
         this.AV9ColContadorFMn = aP9_ColContadorFMn;
         this.AV87ContagemResultado_ContadorFMCod = aP10_ContagemResultado_ContadorFMCod;
         this.AV57ColPFBFSn = aP11_ColPFBFSn;
         this.AV58ColPFLFSn = aP12_ColPFLFSn;
         this.AV17ColPFBFMn = aP13_ColPFBFMn;
         this.AV18ColPFLFMn = aP14_ColPFLFMn;
         this.AV98ColParecern = aP15_ColParecern;
         this.AV85Final = aP16_Final;
         this.AV90FileName = aP17_FileName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_OQImportar ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 int aP2_Servico ,
                                 String aP3_Arquivo ,
                                 String aP4_Aba ,
                                 short aP5_ColDmnn ,
                                 DateTime aP6_DataCnt ,
                                 short aP7_ColDataCntn ,
                                 short aP8_PraLinha ,
                                 short aP9_ColContadorFMn ,
                                 int aP10_ContagemResultado_ContadorFMCod ,
                                 short aP11_ColPFBFSn ,
                                 short aP12_ColPFLFSn ,
                                 short aP13_ColPFBFMn ,
                                 short aP14_ColPFLFMn ,
                                 short aP15_ColParecern ,
                                 bool aP16_Final ,
                                 String aP17_FileName )
      {
         aprc_importarcontagens objaprc_importarcontagens;
         objaprc_importarcontagens = new aprc_importarcontagens();
         objaprc_importarcontagens.AV92OQImportar = aP0_OQImportar;
         objaprc_importarcontagens.AV78ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objaprc_importarcontagens.AV94Servico = aP2_Servico;
         objaprc_importarcontagens.AV8Arquivo = aP3_Arquivo;
         objaprc_importarcontagens.AV84Aba = aP4_Aba;
         objaprc_importarcontagens.AV16ColDmnn = aP5_ColDmnn;
         objaprc_importarcontagens.AV95DataCnt = aP6_DataCnt;
         objaprc_importarcontagens.AV60ColDataCntn = aP7_ColDataCntn;
         objaprc_importarcontagens.AV44PraLinha = aP8_PraLinha;
         objaprc_importarcontagens.AV9ColContadorFMn = aP9_ColContadorFMn;
         objaprc_importarcontagens.AV87ContagemResultado_ContadorFMCod = aP10_ContagemResultado_ContadorFMCod;
         objaprc_importarcontagens.AV57ColPFBFSn = aP11_ColPFBFSn;
         objaprc_importarcontagens.AV58ColPFLFSn = aP12_ColPFLFSn;
         objaprc_importarcontagens.AV17ColPFBFMn = aP13_ColPFBFMn;
         objaprc_importarcontagens.AV18ColPFLFMn = aP14_ColPFLFMn;
         objaprc_importarcontagens.AV98ColParecern = aP15_ColParecern;
         objaprc_importarcontagens.AV85Final = aP16_Final;
         objaprc_importarcontagens.AV90FileName = aP17_FileName;
         objaprc_importarcontagens.context.SetSubmitInitialConfig(context);
         objaprc_importarcontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_importarcontagens);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_importarcontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV51WWPContext) ;
            /* Execute user subroutine: 'OPENFILE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV61ArqTitulo = "Arquivo: " + AV90FileName;
            AV20ContadorFMCod = AV87ContagemResultado_ContadorFMCod;
            AV39Ln = AV44PraLinha;
            AV24Contratada_Codigo = AV78ContagemResultado_ContratadaCod;
            AV39Ln = AV44PraLinha;
            if ( StringUtil.StrCmp(AV92OQImportar, "ICNT") == 0 )
            {
               AV101Titulo = "Importa��o de Contagens";
               while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text)) )
               {
                  AV37Linha = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Ln), 4, 0)) + " - ";
                  AV28ErrCod = 0;
                  if ( AV17ColPFBFMn > 0 )
                  {
                     AV64PFBFM = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV17ColPFBFMn, 1, 1).Number);
                     if ( ( AV64PFBFM == Convert.ToDecimal( 0 )) )
                     {
                        AV64PFBFM = NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV17ColPFBFMn, 1, 1).Value, ".");
                     }
                     AV65PFLFM = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV18ColPFLFMn, 1, 1).Number);
                     if ( ( AV65PFLFM == Convert.ToDecimal( 0 )) )
                     {
                        AV65PFLFM = NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV18ColPFLFMn, 1, 1).Value, ".");
                     }
                     if ( ! AV85Final )
                     {
                        if ( (0==AV87ContagemResultado_ContadorFMCod) )
                        {
                           AV23ContadorNome = StringUtil.Upper( StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV9ColContadorFMn, 1, 1).Text));
                           AV32FMFlag = true;
                           /* Execute user subroutine: 'PROCURACONTADOR' */
                           S121 ();
                           if ( returnInSub )
                           {
                              this.cleanup();
                              if (true) return;
                           }
                           AV20ContadorFMCod = AV19ContadorCodigo;
                        }
                     }
                  }
                  if ( AV57ColPFBFSn > 0 )
                  {
                     AV62PFBFS = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV57ColPFBFSn, 1, 1).Number);
                     if ( ( AV62PFBFS == Convert.ToDecimal( 0 )) )
                     {
                        AV62PFBFS = NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV57ColPFBFSn, 1, 1).Value, ".");
                     }
                     AV63PFLFS = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV58ColPFLFSn, 1, 1).Number);
                     if ( ( AV63PFLFS == Convert.ToDecimal( 0 )) )
                     {
                        AV63PFLFS = NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV58ColPFLFSn, 1, 1).Value, ".");
                     }
                  }
                  if ( AV28ErrCod == 0 )
                  {
                     AV79Demanda = StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text);
                     AV59ContagemResultado_Codigo = 0;
                     AV112GXLvl65 = 0;
                     /* Using cursor P004M2 */
                     pr_default.execute(0, new Object[] {AV79Demanda, AV24Contratada_Codigo});
                     while ( (pr_default.getStatus(0) != 101) )
                     {
                        A456ContagemResultado_Codigo = P004M2_A456ContagemResultado_Codigo[0];
                        A490ContagemResultado_ContratadaCod = P004M2_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P004M2_n490ContagemResultado_ContratadaCod[0];
                        A457ContagemResultado_Demanda = P004M2_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P004M2_n457ContagemResultado_Demanda[0];
                        A484ContagemResultado_StatusDmn = P004M2_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P004M2_n484ContagemResultado_StatusDmn[0];
                        A1596ContagemResultado_CntSrvPrc = P004M2_A1596ContagemResultado_CntSrvPrc[0];
                        n1596ContagemResultado_CntSrvPrc = P004M2_n1596ContagemResultado_CntSrvPrc[0];
                        A1553ContagemResultado_CntSrvCod = P004M2_A1553ContagemResultado_CntSrvCod[0];
                        n1553ContagemResultado_CntSrvCod = P004M2_n1553ContagemResultado_CntSrvCod[0];
                        A1596ContagemResultado_CntSrvPrc = P004M2_A1596ContagemResultado_CntSrvPrc[0];
                        n1596ContagemResultado_CntSrvPrc = P004M2_n1596ContagemResultado_CntSrvPrc[0];
                        AV112GXLvl65 = 1;
                        AV59ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                        AV88ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
                        if ( ( A1596ContagemResultado_CntSrvPrc > Convert.ToDecimal( 0 )) )
                        {
                           AV105Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
                        }
                        else
                        {
                           AV105Servico_Percentual = (decimal)(1);
                        }
                        if ( AV17ColPFBFMn > 0 )
                        {
                           A484ContagemResultado_StatusDmn = "R";
                           n484ContagemResultado_StatusDmn = false;
                           /* Using cursor P004M3 */
                           pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
                           while ( (pr_default.getStatus(1) != 101) )
                           {
                              A517ContagemResultado_Ultima = P004M3_A517ContagemResultado_Ultima[0];
                              A470ContagemResultado_ContadorFMCod = P004M3_A470ContagemResultado_ContadorFMCod[0];
                              A473ContagemResultado_DataCnt = P004M3_A473ContagemResultado_DataCnt[0];
                              A511ContagemResultado_HoraCnt = P004M3_A511ContagemResultado_HoraCnt[0];
                              if ( AV85Final )
                              {
                                 if ( StringUtil.StrCmp(AV88ContagemResultado_StatusDmn, "A") == 0 )
                                 {
                                    AV20ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                                    A517ContagemResultado_Ultima = false;
                                 }
                              }
                              else
                              {
                                 A517ContagemResultado_Ultima = false;
                              }
                              /* Using cursor P004M4 */
                              pr_default.execute(2, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                              pr_default.close(2);
                              dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                              pr_default.readNext(1);
                           }
                           pr_default.close(1);
                           new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  AV20ContadorFMCod, ref  AV89ContagemResultado_CstUntPrd, out  AV91CalculoPFinal) ;
                        }
                        /* Using cursor P004M5 */
                        pr_default.execute(3, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                        pr_default.close(3);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                        pr_default.readNext(0);
                     }
                     pr_default.close(0);
                     if ( AV112GXLvl65 == 0 )
                     {
                        AV37Linha = AV37Linha + "Demanda " + AV79Demanda + " n�o achada.";
                        AV28ErrCod = 1;
                     }
                     if ( AV28ErrCod == 0 )
                     {
                        if ( AV17ColPFBFMn > 0 )
                        {
                           if ( ! AV85Final || ( AV85Final && ( StringUtil.StrCmp(AV88ContagemResultado_StatusDmn, "A") == 0 ) ) )
                           {
                              /*
                                 INSERT RECORD ON TABLE ContagemResultadoContagens

                              */
                              A456ContagemResultado_Codigo = AV59ContagemResultado_Codigo;
                              A470ContagemResultado_ContadorFMCod = AV20ContadorFMCod;
                              A833ContagemResultado_CstUntPrd = AV89ContagemResultado_CstUntPrd;
                              n833ContagemResultado_CstUntPrd = false;
                              A511ContagemResultado_HoraCnt = context.localUtil.ServerTime( context, "DEFAULT");
                              if ( AV85Final )
                              {
                                 A458ContagemResultado_PFBFS = AV62PFBFS;
                                 n458ContagemResultado_PFBFS = false;
                                 A459ContagemResultado_PFLFS = AV63PFLFS;
                                 n459ContagemResultado_PFLFS = false;
                                 A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
                              }
                              else
                              {
                                 if ( AV60ColDataCntn > 0 )
                                 {
                                    A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(AV30ExcelDocument.get_Cells(AV39Ln, AV60ColDataCntn, 1, 1).Date);
                                 }
                                 else
                                 {
                                    A473ContagemResultado_DataCnt = AV95DataCnt;
                                 }
                              }
                              A460ContagemResultado_PFBFM = AV64PFBFM;
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = AV65PFLFM;
                              n461ContagemResultado_PFLFM = false;
                              A482ContagemResultadoContagens_Esforco = 0;
                              A462ContagemResultado_Divergencia = 0;
                              A483ContagemResultado_StatusCnt = 5;
                              A800ContagemResultado_Deflator = AV105Servico_Percentual;
                              n800ContagemResultado_Deflator = false;
                              AV83Aprovadas = (short)(AV83Aprovadas+1);
                              A469ContagemResultado_NaoCnfCntCod = 0;
                              n469ContagemResultado_NaoCnfCntCod = false;
                              n469ContagemResultado_NaoCnfCntCod = true;
                              A517ContagemResultado_Ultima = true;
                              AV37Linha = AV37Linha + "Nova ";
                              /* Using cursor P004M6 */
                              pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
                              pr_default.close(4);
                              dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                              if ( (pr_default.getStatus(4) == 1) )
                              {
                                 context.Gx_err = 1;
                                 Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                              }
                              else
                              {
                                 context.Gx_err = 0;
                                 Gx_emsg = "";
                              }
                              /* End Insert */
                              AV37Linha = AV37Linha + "Contagem da " + AV79Demanda + " inserida (";
                              if ( AV85Final )
                              {
                                 AV37Linha = AV37Linha + StringUtil.Trim( StringUtil.Str( AV62PFBFS, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV63PFLFS, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV64PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV65PFLFM, 14, 5)) + ") " + context.localUtil.TToC( DateTimeUtil.ServerNow( context, "DEFAULT"), 10, 8, 0, 3, "/", ":", " ");
                              }
                              else
                              {
                                 AV37Linha = AV37Linha + StringUtil.Trim( StringUtil.Str( AV64PFBFM, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV65PFLFM, 14, 5)) + ") " + context.localUtil.TToC( AV30ExcelDocument.get_Cells(AV39Ln, AV60ColDataCntn, 1, 1).Date, 8, 5, 0, 3, "/", ":", " ");
                              }
                           }
                           else
                           {
                              AV37Linha = AV37Linha + "Contagem da " + AV79Demanda + " N�O inserida pelo Status da Demanda";
                           }
                        }
                        if ( ( AV57ColPFBFSn > 0 ) && ! AV85Final )
                        {
                           AV114GXLvl144 = 0;
                           /* Using cursor P004M7 */
                           pr_default.execute(5, new Object[] {AV59ContagemResultado_Codigo});
                           while ( (pr_default.getStatus(5) != 101) )
                           {
                              A456ContagemResultado_Codigo = P004M7_A456ContagemResultado_Codigo[0];
                              A517ContagemResultado_Ultima = P004M7_A517ContagemResultado_Ultima[0];
                              A460ContagemResultado_PFBFM = P004M7_A460ContagemResultado_PFBFM[0];
                              n460ContagemResultado_PFBFM = P004M7_n460ContagemResultado_PFBFM[0];
                              A461ContagemResultado_PFLFM = P004M7_A461ContagemResultado_PFLFM[0];
                              n461ContagemResultado_PFLFM = P004M7_n461ContagemResultado_PFLFM[0];
                              A1553ContagemResultado_CntSrvCod = P004M7_A1553ContagemResultado_CntSrvCod[0];
                              n1553ContagemResultado_CntSrvCod = P004M7_n1553ContagemResultado_CntSrvCod[0];
                              A458ContagemResultado_PFBFS = P004M7_A458ContagemResultado_PFBFS[0];
                              n458ContagemResultado_PFBFS = P004M7_n458ContagemResultado_PFBFS[0];
                              A459ContagemResultado_PFLFS = P004M7_A459ContagemResultado_PFLFS[0];
                              n459ContagemResultado_PFLFS = P004M7_n459ContagemResultado_PFLFS[0];
                              A483ContagemResultado_StatusCnt = P004M7_A483ContagemResultado_StatusCnt[0];
                              A462ContagemResultado_Divergencia = P004M7_A462ContagemResultado_Divergencia[0];
                              A473ContagemResultado_DataCnt = P004M7_A473ContagemResultado_DataCnt[0];
                              A511ContagemResultado_HoraCnt = P004M7_A511ContagemResultado_HoraCnt[0];
                              A1553ContagemResultado_CntSrvCod = P004M7_A1553ContagemResultado_CntSrvCod[0];
                              n1553ContagemResultado_CntSrvCod = P004M7_n1553ContagemResultado_CntSrvCod[0];
                              AV114GXLvl144 = 1;
                              AV76PFFM = A460ContagemResultado_PFBFM;
                              AV75PFFS = AV62PFBFS;
                              AV69ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                              AV68ContagemResultado_PFLFS = AV63PFLFS;
                              AV106ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                              /* Execute user subroutine: 'CALCULADIVERGENCIA' */
                              S131 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(5);
                                 this.cleanup();
                                 if (true) return;
                              }
                              A458ContagemResultado_PFBFS = AV62PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV63PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A483ContagemResultado_StatusCnt = AV70ContagemResultado_StatusCnt;
                              A462ContagemResultado_Divergencia = AV71ContagemResultado_Divergencia;
                              /* Using cursor P004M8 */
                              pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
                              while ( (pr_default.getStatus(6) != 101) )
                              {
                                 A484ContagemResultado_StatusDmn = P004M8_A484ContagemResultado_StatusDmn[0];
                                 n484ContagemResultado_StatusDmn = P004M8_n484ContagemResultado_StatusDmn[0];
                                 if ( AV70ContagemResultado_StatusCnt == 5 )
                                 {
                                    A484ContagemResultado_StatusDmn = "R";
                                    n484ContagemResultado_StatusDmn = false;
                                    AV83Aprovadas = (short)(AV83Aprovadas+1);
                                 }
                                 else
                                 {
                                    A484ContagemResultado_StatusDmn = "A";
                                    n484ContagemResultado_StatusDmn = false;
                                    AV82Divergencias = (short)(AV82Divergencias+1);
                                 }
                                 /* Using cursor P004M9 */
                                 pr_default.execute(7, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                                 pr_default.close(7);
                                 dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                                 /* Exiting from a For First loop. */
                                 if (true) break;
                              }
                              pr_default.close(6);
                              AV37Linha = AV37Linha + "Contagem da " + AV79Demanda + " inserida (" + StringUtil.Trim( StringUtil.Str( AV62PFBFS, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV63PFLFS, 14, 5)) + ") Status: " + gxdomainstatuscontagem.getDescription(context,AV70ContagemResultado_StatusCnt);
                              /* Using cursor P004M10 */
                              pr_default.execute(8, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                              pr_default.close(8);
                              dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                              pr_default.readNext(5);
                           }
                           pr_default.close(5);
                           if ( AV114GXLvl144 == 0 )
                           {
                              AV37Linha = AV37Linha + "Contagem FM da " + AV79Demanda + " N�O achada.";
                              AV28ErrCod = 1;
                           }
                        }
                     }
                  }
                  context.CommitDataStores( "PRC_ImportarContagens");
                  new prc_disparoservicovinculado(context ).execute(  AV59ContagemResultado_Codigo,  AV51WWPContext.gxTpr_Userid) ;
                  if ( AV28ErrCod == 1 )
                  {
                     AV29Erros = (short)(AV29Erros+1);
                  }
                  H4M0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV37Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
                  AV39Ln = (short)(AV39Ln+1);
                  AV36Lidas = (short)(AV36Lidas+1);
               }
            }
            else if ( StringUtil.StrCmp(AV92OQImportar, "ICCT") == 0 )
            {
               AV101Titulo = "Importa��o de Contagens CATIs";
               while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text)) )
               {
                  AV37Linha = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Ln), 4, 0)) + " - ";
                  AV28ErrCod = 0;
                  AV79Demanda = StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text);
                  AV64PFBFM = 0;
                  AV65PFLFM = 0;
                  if ( AV60ColDataCntn > 0 )
                  {
                     AV96ContagemResultado_DataCnt = DateTimeUtil.ResetTime(AV30ExcelDocument.get_Cells(AV39Ln, AV60ColDataCntn, 1, 1).Date);
                  }
                  else
                  {
                     AV96ContagemResultado_DataCnt = AV95DataCnt;
                  }
                  AV97ContagemResultado_ParecerTcn = StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV98ColParecern, 1, 1).Text);
                  if ( AV9ColContadorFMn > 0 )
                  {
                     AV23ContadorNome = StringUtil.Upper( StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV9ColContadorFMn, 1, 1).Text));
                     AV32FMFlag = true;
                     /* Execute user subroutine: 'PROCURACONTADOR' */
                     S121 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV20ContadorFMCod = AV19ContadorCodigo;
                  }
                  if ( AV28ErrCod == 0 )
                  {
                     AV59ContagemResultado_Codigo = 0;
                     AV116GXLvl219 = 0;
                     /* Using cursor P004M12 */
                     pr_default.execute(9, new Object[] {AV79Demanda, AV24Contratada_Codigo, AV94Servico});
                     while ( (pr_default.getStatus(9) != 101) )
                     {
                        A601ContagemResultado_Servico = P004M12_A601ContagemResultado_Servico[0];
                        n601ContagemResultado_Servico = P004M12_n601ContagemResultado_Servico[0];
                        A490ContagemResultado_ContratadaCod = P004M12_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P004M12_n490ContagemResultado_ContratadaCod[0];
                        A457ContagemResultado_Demanda = P004M12_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P004M12_n457ContagemResultado_Demanda[0];
                        A456ContagemResultado_Codigo = P004M12_A456ContagemResultado_Codigo[0];
                        A484ContagemResultado_StatusDmn = P004M12_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P004M12_n484ContagemResultado_StatusDmn[0];
                        A1596ContagemResultado_CntSrvPrc = P004M12_A1596ContagemResultado_CntSrvPrc[0];
                        n1596ContagemResultado_CntSrvPrc = P004M12_n1596ContagemResultado_CntSrvPrc[0];
                        A798ContagemResultado_PFBFSImp = P004M12_A798ContagemResultado_PFBFSImp[0];
                        n798ContagemResultado_PFBFSImp = P004M12_n798ContagemResultado_PFBFSImp[0];
                        A799ContagemResultado_PFLFSImp = P004M12_A799ContagemResultado_PFLFSImp[0];
                        n799ContagemResultado_PFLFSImp = P004M12_n799ContagemResultado_PFLFSImp[0];
                        A1553ContagemResultado_CntSrvCod = P004M12_A1553ContagemResultado_CntSrvCod[0];
                        n1553ContagemResultado_CntSrvCod = P004M12_n1553ContagemResultado_CntSrvCod[0];
                        A684ContagemResultado_PFBFSUltima = P004M12_A684ContagemResultado_PFBFSUltima[0];
                        A685ContagemResultado_PFLFSUltima = P004M12_A685ContagemResultado_PFLFSUltima[0];
                        A682ContagemResultado_PFBFMUltima = P004M12_A682ContagemResultado_PFBFMUltima[0];
                        A684ContagemResultado_PFBFSUltima = P004M12_A684ContagemResultado_PFBFSUltima[0];
                        A685ContagemResultado_PFLFSUltima = P004M12_A685ContagemResultado_PFLFSUltima[0];
                        A682ContagemResultado_PFBFMUltima = P004M12_A682ContagemResultado_PFBFMUltima[0];
                        A601ContagemResultado_Servico = P004M12_A601ContagemResultado_Servico[0];
                        n601ContagemResultado_Servico = P004M12_n601ContagemResultado_Servico[0];
                        A1596ContagemResultado_CntSrvPrc = P004M12_A1596ContagemResultado_CntSrvPrc[0];
                        n1596ContagemResultado_CntSrvPrc = P004M12_n1596ContagemResultado_CntSrvPrc[0];
                        AV116GXLvl219 = 1;
                        AV59ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                        AV88ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
                        if ( ( A1596ContagemResultado_CntSrvPrc > Convert.ToDecimal( 0 )) )
                        {
                           AV105Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
                        }
                        else
                        {
                           AV105Servico_Percentual = (decimal)(1);
                        }
                        if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
                        {
                           AV62PFBFS = A684ContagemResultado_PFBFSUltima;
                           AV63PFLFS = A685ContagemResultado_PFLFSUltima;
                        }
                        else
                        {
                           AV62PFBFS = A798ContagemResultado_PFBFSImp;
                           AV63PFLFS = A799ContagemResultado_PFLFSImp;
                        }
                        AV104TemContagens = (bool)(((A682ContagemResultado_PFBFMUltima+A684ContagemResultado_PFBFSUltima)>Convert.ToDecimal(0)));
                        new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  AV20ContadorFMCod, ref  AV89ContagemResultado_CstUntPrd, out  AV91CalculoPFinal) ;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(9);
                     }
                     pr_default.close(9);
                     if ( AV116GXLvl219 == 0 )
                     {
                        AV37Linha = AV37Linha + "Demanda " + AV79Demanda + " n�o achada.";
                        AV28ErrCod = 1;
                     }
                     if ( AV28ErrCod == 0 )
                     {
                        while ( ( StringUtil.StrCmp(AV79Demanda, StringUtil.Trim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text)) == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ExcelDocument.get_Cells(AV39Ln, AV16ColDmnn, 1, 1).Text)) )
                        {
                           AV93PF = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV17ColPFBFMn, 1, 1).Number);
                           if ( ( AV93PF == Convert.ToDecimal( 0 )) )
                           {
                              AV64PFBFM = (decimal)(AV64PFBFM+(NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV17ColPFBFMn, 1, 1).Value, ".")));
                           }
                           else
                           {
                              AV64PFBFM = (decimal)(AV64PFBFM+AV93PF);
                           }
                           AV93PF = (decimal)(AV30ExcelDocument.get_Cells(AV39Ln, AV18ColPFLFMn, 1, 1).Number);
                           if ( ( AV93PF == Convert.ToDecimal( 0 )) )
                           {
                              AV65PFLFM = (decimal)(AV65PFLFM+(NumberUtil.Val( AV30ExcelDocument.get_Cells(AV39Ln, AV18ColPFLFMn, 1, 1).Value, ".")));
                           }
                           else
                           {
                              AV65PFLFM = (decimal)(AV65PFLFM+AV93PF);
                           }
                           AV39Ln = (short)(AV39Ln+1);
                           AV36Lidas = (short)(AV36Lidas+1);
                        }
                        if ( ( AV64PFBFM + AV65PFLFM <= Convert.ToDecimal( 0 )) )
                        {
                           AV37Linha = AV37Linha + "Sem valores na planilha da " + AV79Demanda;
                           AV28ErrCod = 1;
                        }
                        else
                        {
                           if ( AV104TemContagens )
                           {
                              /* Using cursor P004M13 */
                              pr_default.execute(10, new Object[] {AV59ContagemResultado_Codigo, AV96ContagemResultado_DataCnt});
                              while ( (pr_default.getStatus(10) != 101) )
                              {
                                 A473ContagemResultado_DataCnt = P004M13_A473ContagemResultado_DataCnt[0];
                                 A456ContagemResultado_Codigo = P004M13_A456ContagemResultado_Codigo[0];
                                 A470ContagemResultado_ContadorFMCod = P004M13_A470ContagemResultado_ContadorFMCod[0];
                                 A459ContagemResultado_PFLFS = P004M13_A459ContagemResultado_PFLFS[0];
                                 n459ContagemResultado_PFLFS = P004M13_n459ContagemResultado_PFLFS[0];
                                 A458ContagemResultado_PFBFS = P004M13_A458ContagemResultado_PFBFS[0];
                                 n458ContagemResultado_PFBFS = P004M13_n458ContagemResultado_PFBFS[0];
                                 A461ContagemResultado_PFLFM = P004M13_A461ContagemResultado_PFLFM[0];
                                 n461ContagemResultado_PFLFM = P004M13_n461ContagemResultado_PFLFM[0];
                                 A460ContagemResultado_PFBFM = P004M13_A460ContagemResultado_PFBFM[0];
                                 n460ContagemResultado_PFBFM = P004M13_n460ContagemResultado_PFBFM[0];
                                 A511ContagemResultado_HoraCnt = P004M13_A511ContagemResultado_HoraCnt[0];
                                 if ( ( A460ContagemResultado_PFBFM == AV64PFBFM ) && ( A461ContagemResultado_PFLFM == AV65PFLFM ) && ( A458ContagemResultado_PFBFS == AV62PFBFS ) && ( A459ContagemResultado_PFLFS == AV63PFLFS ) && ( A470ContagemResultado_ContadorFMCod == AV20ContadorFMCod ) )
                                 {
                                    AV37Linha = AV37Linha + "Contagem da " + AV79Demanda + " j� existe.";
                                    AV28ErrCod = 1;
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                                 pr_default.readNext(10);
                              }
                              pr_default.close(10);
                              if ( AV28ErrCod == 0 )
                              {
                                 /* Optimized UPDATE. */
                                 /* Using cursor P004M14 */
                                 pr_default.execute(11, new Object[] {AV59ContagemResultado_Codigo});
                                 pr_default.close(11);
                                 dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                                 dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                                 /* End optimized UPDATE. */
                              }
                           }
                           if ( AV28ErrCod == 0 )
                           {
                              AV76PFFM = AV64PFBFM;
                              AV75PFFS = AV62PFBFS;
                              AV69ContagemResultado_PFLFM = AV65PFLFM;
                              AV68ContagemResultado_PFLFS = AV63PFLFS;
                              AV106ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                              /* Execute user subroutine: 'CALCULADIVERGENCIA' */
                              S131 ();
                              if ( returnInSub )
                              {
                                 this.cleanup();
                                 if (true) return;
                              }
                              /* Using cursor P004M15 */
                              pr_default.execute(12, new Object[] {AV59ContagemResultado_Codigo});
                              while ( (pr_default.getStatus(12) != 101) )
                              {
                                 A490ContagemResultado_ContratadaCod = P004M15_A490ContagemResultado_ContratadaCod[0];
                                 n490ContagemResultado_ContratadaCod = P004M15_n490ContagemResultado_ContratadaCod[0];
                                 A1553ContagemResultado_CntSrvCod = P004M15_A1553ContagemResultado_CntSrvCod[0];
                                 n1553ContagemResultado_CntSrvCod = P004M15_n1553ContagemResultado_CntSrvCod[0];
                                 A456ContagemResultado_Codigo = P004M15_A456ContagemResultado_Codigo[0];
                                 A484ContagemResultado_StatusDmn = P004M15_A484ContagemResultado_StatusDmn[0];
                                 n484ContagemResultado_StatusDmn = P004M15_n484ContagemResultado_StatusDmn[0];
                                 A890ContagemResultado_Responsavel = P004M15_A890ContagemResultado_Responsavel[0];
                                 n890ContagemResultado_Responsavel = P004M15_n890ContagemResultado_Responsavel[0];
                                 A602ContagemResultado_OSVinculada = P004M15_A602ContagemResultado_OSVinculada[0];
                                 n602ContagemResultado_OSVinculada = P004M15_n602ContagemResultado_OSVinculada[0];
                                 A457ContagemResultado_Demanda = P004M15_A457ContagemResultado_Demanda[0];
                                 n457ContagemResultado_Demanda = P004M15_n457ContagemResultado_Demanda[0];
                                 A805ContagemResultado_ContratadaOrigemCod = P004M15_A805ContagemResultado_ContratadaOrigemCod[0];
                                 n805ContagemResultado_ContratadaOrigemCod = P004M15_n805ContagemResultado_ContratadaOrigemCod[0];
                                 A601ContagemResultado_Servico = P004M15_A601ContagemResultado_Servico[0];
                                 n601ContagemResultado_Servico = P004M15_n601ContagemResultado_Servico[0];
                                 A52Contratada_AreaTrabalhoCod = P004M15_A52Contratada_AreaTrabalhoCod[0];
                                 n52Contratada_AreaTrabalhoCod = P004M15_n52Contratada_AreaTrabalhoCod[0];
                                 A472ContagemResultado_DataEntrega = P004M15_A472ContagemResultado_DataEntrega[0];
                                 n472ContagemResultado_DataEntrega = P004M15_n472ContagemResultado_DataEntrega[0];
                                 A912ContagemResultado_HoraEntrega = P004M15_A912ContagemResultado_HoraEntrega[0];
                                 n912ContagemResultado_HoraEntrega = P004M15_n912ContagemResultado_HoraEntrega[0];
                                 A52Contratada_AreaTrabalhoCod = P004M15_A52Contratada_AreaTrabalhoCod[0];
                                 n52Contratada_AreaTrabalhoCod = P004M15_n52Contratada_AreaTrabalhoCod[0];
                                 A601ContagemResultado_Servico = P004M15_A601ContagemResultado_Servico[0];
                                 n601ContagemResultado_Servico = P004M15_n601ContagemResultado_Servico[0];
                                 if ( AV70ContagemResultado_StatusCnt == 5 )
                                 {
                                    A484ContagemResultado_StatusDmn = "R";
                                    n484ContagemResultado_StatusDmn = false;
                                    A890ContagemResultado_Responsavel = AV20ContadorFMCod;
                                    n890ContagemResultado_Responsavel = false;
                                    AV99Acao = "V";
                                    AV83Aprovadas = (short)(AV83Aprovadas+1);
                                 }
                                 else
                                 {
                                    new prc_nadivergenciaficacom(context ).execute(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A457ContagemResultado_Demanda,  A805ContagemResultado_ContratadaOrigemCod,  A601ContagemResultado_Servico,  A52Contratada_AreaTrabalhoCod,  AV20ContadorFMCod,  AV97ContagemResultado_ParecerTcn) ;
                                    AV99Acao = "D";
                                    AV82Divergencias = (short)(AV82Divergencias+1);
                                 }
                                 AV102Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
                                 AV102Prazo = DateTimeUtil.TAdd( AV102Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
                                 AV102Prazo = DateTimeUtil.TAdd( AV102Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
                                 AV100NovoStatus = A484ContagemResultado_StatusDmn;
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 /* Using cursor P004M16 */
                                 pr_default.execute(13, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
                                 pr_default.close(13);
                                 dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                                 if (true) break;
                                 /* Using cursor P004M17 */
                                 pr_default.execute(14, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
                                 pr_default.close(14);
                                 dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                                 /* Exiting from a For First loop. */
                                 if (true) break;
                              }
                              pr_default.close(12);
                              AV37Linha = AV37Linha + "Nova contagem da " + AV79Demanda;
                              /*
                                 INSERT RECORD ON TABLE ContagemResultadoContagens

                              */
                              A456ContagemResultado_Codigo = AV59ContagemResultado_Codigo;
                              A470ContagemResultado_ContadorFMCod = AV20ContadorFMCod;
                              A833ContagemResultado_CstUntPrd = AV89ContagemResultado_CstUntPrd;
                              n833ContagemResultado_CstUntPrd = false;
                              A473ContagemResultado_DataCnt = AV96ContagemResultado_DataCnt;
                              A511ContagemResultado_HoraCnt = context.localUtil.ServerTime( context, "DEFAULT");
                              A483ContagemResultado_StatusCnt = AV70ContagemResultado_StatusCnt;
                              A460ContagemResultado_PFBFM = AV64PFBFM;
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = AV65PFLFM;
                              n461ContagemResultado_PFLFM = false;
                              A458ContagemResultado_PFBFS = AV62PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV63PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A482ContagemResultadoContagens_Esforco = 0;
                              A462ContagemResultado_Divergencia = AV71ContagemResultado_Divergencia;
                              A463ContagemResultado_ParecerTcn = AV97ContagemResultado_ParecerTcn;
                              n463ContagemResultado_ParecerTcn = false;
                              A800ContagemResultado_Deflator = AV105Servico_Percentual;
                              n800ContagemResultado_Deflator = false;
                              A469ContagemResultado_NaoCnfCntCod = 0;
                              n469ContagemResultado_NaoCnfCntCod = false;
                              n469ContagemResultado_NaoCnfCntCod = true;
                              A517ContagemResultado_Ultima = true;
                              AV37Linha = AV37Linha + " inserida (" + StringUtil.Trim( StringUtil.Str( AV62PFBFS, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV63PFLFS, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV64PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV65PFLFM, 14, 5)) + ") " + gxdomainstatuscontagem.getDescription(context,AV70ContagemResultado_StatusCnt);
                              /* Using cursor P004M18 */
                              pr_default.execute(15, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
                              pr_default.close(15);
                              dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                              if ( (pr_default.getStatus(15) == 1) )
                              {
                                 context.Gx_err = 1;
                                 Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                                 AV37Linha = AV37Linha + " N�O INSERIDA";
                              }
                              else
                              {
                                 context.Gx_err = 0;
                                 Gx_emsg = "";
                              }
                              /* End Insert */
                              if ( AV70ContagemResultado_StatusCnt == 5 )
                              {
                                 new prc_inslogresponsavel(context ).execute( ref  AV59ContagemResultado_Codigo,  0,  AV99Acao,  "D",  AV20ContadorFMCod,  0,  AV88ContagemResultado_StatusDmn,  AV100NovoStatus,  AV97ContagemResultado_ParecerTcn,  AV102Prazo,  true) ;
                              }
                              new prc_disparoservicovinculado(context ).execute(  AV59ContagemResultado_Codigo,  AV51WWPContext.gxTpr_Userid) ;
                           }
                        }
                     }
                     else
                     {
                        AV39Ln = (short)(AV39Ln+1);
                        AV36Lidas = (short)(AV36Lidas+1);
                     }
                  }
                  else
                  {
                     AV39Ln = (short)(AV39Ln+1);
                     AV36Lidas = (short)(AV36Lidas+1);
                  }
                  if ( AV28ErrCod == 1 )
                  {
                     AV29Erros = (short)(AV29Erros+1);
                  }
                  H4M0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV37Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            AV49Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Lidas), 4, 0)) + " linhas processadas  -  " + StringUtil.Trim( StringUtil.Str( (decimal)(AV29Erros), 4, 0)) + " n�o importadas" + ((AV57ColPFBFSn==0) ? "" : " - "+StringUtil.Trim( StringUtil.Str( (decimal)(AV82Divergencias), 4, 0))+" divergentes - "+StringUtil.Trim( StringUtil.Str( (decimal)(AV83Aprovadas), 4, 0))+" aprovadas");
            H4M0( false, 35) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49Totais, "")), 25, Gx_line+17, 755, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
            AV30ExcelDocument.Close();
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H4M0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENFILE' Routine */
         AV28ErrCod = AV30ExcelDocument.Open(AV8Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84Aba)) )
         {
            AV28ErrCod = AV30ExcelDocument.SelectSheet(AV84Aba);
         }
      }

      protected void S121( )
      {
         /* 'PROCURACONTADOR' Routine */
         AV19ContadorCodigo = 0;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23ContadorNome)) )
         {
            AV23ContadorNome = "<desconhecido>";
         }
         else
         {
            AV34i = 1;
            AV43p = 1;
            AV35LastSpace = (short)(StringUtil.Len( AV23ContadorNome));
            GX_I = 1;
            while ( GX_I <= 4 )
            {
               AV54Nomes[GX_I-1] = "";
               GX_I = (int)(GX_I+1);
            }
            AV31Flag = true;
            while ( AV31Flag )
            {
               AV45s = (short)(StringUtil.StringSearch( AV23ContadorNome, " ", AV43p)-1);
               if ( AV45s < 0 )
               {
                  AV45s = (short)(StringUtil.Len( AV23ContadorNome));
               }
               AV54Nomes[AV34i-1] = "%" + StringUtil.Substring( AV23ContadorNome, AV43p, AV45s) + "%";
               AV43p = (short)(AV45s+2);
               AV34i = (short)(AV34i+1);
               AV31Flag = (bool)((AV43p<AV35LastSpace)&&(AV34i<=4));
            }
            AV34i = 0;
            pr_default.dynParam(16, new Object[]{ new Object[]{
                                                 AV33FSFlag ,
                                                 AV32FMFlag ,
                                                 A66ContratadaUsuario_ContratadaCod ,
                                                 AV24Contratada_Codigo ,
                                                 A516Contratada_TipoFabrica ,
                                                 A1394ContratadaUsuario_UsuarioAtivo ,
                                                 A71ContratadaUsuario_UsuarioPessoaNom ,
                                                 AV54Nomes ,
                                                 A1228ContratadaUsuario_AreaTrabalhoCod ,
                                                 AV51WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor P004M19 */
            pr_default.execute(16, new Object[] {AV51WWPContext.gxTpr_Areatrabalho_codigo, AV24Contratada_Codigo});
            while ( (pr_default.getStatus(16) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = P004M19_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P004M19_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = P004M19_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = P004M19_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1394ContratadaUsuario_UsuarioAtivo = P004M19_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P004M19_n1394ContratadaUsuario_UsuarioAtivo[0];
               A516Contratada_TipoFabrica = P004M19_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P004M19_n516Contratada_TipoFabrica[0];
               A66ContratadaUsuario_ContratadaCod = P004M19_A66ContratadaUsuario_ContratadaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P004M19_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P004M19_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = P004M19_A69ContratadaUsuario_UsuarioCod[0];
               A516Contratada_TipoFabrica = P004M19_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P004M19_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P004M19_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P004M19_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P004M19_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P004M19_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = P004M19_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P004M19_n1394ContratadaUsuario_UsuarioAtivo[0];
               A71ContratadaUsuario_UsuarioPessoaNom = P004M19_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = P004M19_n71ContratadaUsuario_UsuarioPessoaNom[0];
               if ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV54Nomes[1-1] , 100 , "%"),  ' ' ) )
               {
                  if ( ( StringUtil.StrCmp(AV54Nomes[2-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV54Nomes[2-1] , 100 , "%"),  ' ' ) ) )
                  {
                     if ( ( StringUtil.StrCmp(AV54Nomes[3-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV54Nomes[3-1] , 100 , "%"),  ' ' ) ) )
                     {
                        if ( ( StringUtil.StrCmp(AV54Nomes[4-1], "") == 0 ) || ( StringUtil.Like( A71ContratadaUsuario_UsuarioPessoaNom , StringUtil.PadR( AV54Nomes[4-1] , 100 , "%"),  ' ' ) ) )
                        {
                           AV34i = (short)(AV34i+1);
                           AV19ContadorCodigo = A69ContratadaUsuario_UsuarioCod;
                           AV41NomeAchado = A71ContratadaUsuario_UsuarioPessoaNom;
                        }
                     }
                  }
               }
               pr_default.readNext(16);
            }
            pr_default.close(16);
         }
         if ( (0==AV19ContadorCodigo) || ( AV34i > 1 ) )
         {
            AV19ContadorCodigo = 0;
            AV37Linha = AV37Linha + "Contador " + (AV33FSFlag ? "FS " : "FM ") + AV23ContadorNome + " n�o identificado" + ((AV34i>1) ? " de "+StringUtil.Trim( StringUtil.Str( (decimal)(AV34i), 4, 0))+"existentes." : ".");
            AV23ContadorNome = "";
            AV28ErrCod = 1;
         }
         else
         {
            AV23ContadorNome = AV41NomeAchado;
         }
      }

      protected void S131( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         new prc_getparmindicedivergencia(context ).execute( ref  AV106ContratoServicos_Codigo, out  AV77IndiceDivergencia, out  AV81CalculoDivergencia, out  AV80ContagemResultado_ValorPF) ;
         GXt_decimal1 = AV71ContagemResultado_Divergencia;
         new prc_calculardivergencia(context ).execute(  AV81CalculoDivergencia,  AV75PFFS,  AV76PFFM,  AV68ContagemResultado_PFLFS,  AV69ContagemResultado_PFLFM, out  GXt_decimal1) ;
         AV71ContagemResultado_Divergencia = GXt_decimal1;
         if ( AV71ContagemResultado_Divergencia > AV77IndiceDivergencia )
         {
            AV70ContagemResultado_StatusCnt = 7;
         }
         else
         {
            AV70ContagemResultado_StatusCnt = 5;
         }
      }

      protected void H4M0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 675, Gx_line+9, 724, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 733, Gx_line+9, 826, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV61ArqTitulo, "")), 13, Gx_line+33, 813, Gx_line+51, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV101Titulo, "")), 204, Gx_line+0, 622, Gx_line+26, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+76);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ImportarContagens");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV51WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV61ArqTitulo = "";
         AV101Titulo = "";
         AV30ExcelDocument = new ExcelDocumentI();
         AV37Linha = "";
         AV23ContadorNome = "";
         AV79Demanda = "";
         scmdbuf = "";
         P004M2_A456ContagemResultado_Codigo = new int[1] ;
         P004M2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004M2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004M2_A457ContagemResultado_Demanda = new String[] {""} ;
         P004M2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004M2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004M2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004M2_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P004M2_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P004M2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004M2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A484ContagemResultado_StatusDmn = "";
         AV88ContagemResultado_StatusDmn = "";
         P004M3_A456ContagemResultado_Codigo = new int[1] ;
         P004M3_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004M3_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004M3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004M3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV91CalculoPFinal = "";
         Gx_emsg = "";
         P004M7_A456ContagemResultado_Codigo = new int[1] ;
         P004M7_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004M7_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004M7_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004M7_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004M7_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004M7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004M7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004M7_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004M7_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004M7_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004M7_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004M7_A483ContagemResultado_StatusCnt = new short[1] ;
         P004M7_A462ContagemResultado_Divergencia = new decimal[1] ;
         P004M7_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004M7_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004M8_A456ContagemResultado_Codigo = new int[1] ;
         P004M8_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004M8_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         AV96ContagemResultado_DataCnt = DateTime.MinValue;
         AV97ContagemResultado_ParecerTcn = "";
         P004M12_A601ContagemResultado_Servico = new int[1] ;
         P004M12_n601ContagemResultado_Servico = new bool[] {false} ;
         P004M12_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004M12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004M12_A457ContagemResultado_Demanda = new String[] {""} ;
         P004M12_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004M12_A456ContagemResultado_Codigo = new int[1] ;
         P004M12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004M12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004M12_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P004M12_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P004M12_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P004M12_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P004M12_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P004M12_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P004M12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004M12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004M12_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P004M12_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P004M12_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P004M13_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004M13_A456ContagemResultado_Codigo = new int[1] ;
         P004M13_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004M13_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004M13_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004M13_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004M13_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004M13_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004M13_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004M13_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004M13_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004M13_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004M15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004M15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004M15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004M15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004M15_A456ContagemResultado_Codigo = new int[1] ;
         P004M15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004M15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004M15_A890ContagemResultado_Responsavel = new int[1] ;
         P004M15_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P004M15_A602ContagemResultado_OSVinculada = new int[1] ;
         P004M15_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P004M15_A457ContagemResultado_Demanda = new String[] {""} ;
         P004M15_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004M15_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P004M15_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P004M15_A601ContagemResultado_Servico = new int[1] ;
         P004M15_n601ContagemResultado_Servico = new bool[] {false} ;
         P004M15_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P004M15_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P004M15_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P004M15_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P004M15_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P004M15_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV99Acao = "";
         AV102Prazo = (DateTime)(DateTime.MinValue);
         AV100NovoStatus = "";
         A463ContagemResultado_ParecerTcn = "";
         AV49Totais = "";
         AV54Nomes = new String [4] ;
         GX_I = 1;
         while ( GX_I <= 4 )
         {
            AV54Nomes[GX_I-1] = "";
            GX_I = (int)(GX_I+1);
         }
         A516Contratada_TipoFabrica = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         P004M19_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P004M19_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P004M19_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P004M19_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P004M19_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P004M19_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P004M19_A516Contratada_TipoFabrica = new String[] {""} ;
         P004M19_n516Contratada_TipoFabrica = new bool[] {false} ;
         P004M19_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P004M19_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P004M19_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P004M19_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV41NomeAchado = "";
         AV81CalculoDivergencia = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_importarcontagens__default(),
            new Object[][] {
                new Object[] {
               P004M2_A456ContagemResultado_Codigo, P004M2_A490ContagemResultado_ContratadaCod, P004M2_n490ContagemResultado_ContratadaCod, P004M2_A457ContagemResultado_Demanda, P004M2_n457ContagemResultado_Demanda, P004M2_A484ContagemResultado_StatusDmn, P004M2_n484ContagemResultado_StatusDmn, P004M2_A1596ContagemResultado_CntSrvPrc, P004M2_n1596ContagemResultado_CntSrvPrc, P004M2_A1553ContagemResultado_CntSrvCod,
               P004M2_n1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               P004M3_A456ContagemResultado_Codigo, P004M3_A517ContagemResultado_Ultima, P004M3_A470ContagemResultado_ContadorFMCod, P004M3_A473ContagemResultado_DataCnt, P004M3_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004M7_A456ContagemResultado_Codigo, P004M7_A517ContagemResultado_Ultima, P004M7_A460ContagemResultado_PFBFM, P004M7_n460ContagemResultado_PFBFM, P004M7_A461ContagemResultado_PFLFM, P004M7_n461ContagemResultado_PFLFM, P004M7_A1553ContagemResultado_CntSrvCod, P004M7_n1553ContagemResultado_CntSrvCod, P004M7_A458ContagemResultado_PFBFS, P004M7_n458ContagemResultado_PFBFS,
               P004M7_A459ContagemResultado_PFLFS, P004M7_n459ContagemResultado_PFLFS, P004M7_A483ContagemResultado_StatusCnt, P004M7_A462ContagemResultado_Divergencia, P004M7_A473ContagemResultado_DataCnt, P004M7_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P004M8_A456ContagemResultado_Codigo, P004M8_A484ContagemResultado_StatusDmn, P004M8_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004M12_A601ContagemResultado_Servico, P004M12_n601ContagemResultado_Servico, P004M12_A490ContagemResultado_ContratadaCod, P004M12_n490ContagemResultado_ContratadaCod, P004M12_A457ContagemResultado_Demanda, P004M12_n457ContagemResultado_Demanda, P004M12_A456ContagemResultado_Codigo, P004M12_A484ContagemResultado_StatusDmn, P004M12_n484ContagemResultado_StatusDmn, P004M12_A1596ContagemResultado_CntSrvPrc,
               P004M12_n1596ContagemResultado_CntSrvPrc, P004M12_A798ContagemResultado_PFBFSImp, P004M12_n798ContagemResultado_PFBFSImp, P004M12_A799ContagemResultado_PFLFSImp, P004M12_n799ContagemResultado_PFLFSImp, P004M12_A1553ContagemResultado_CntSrvCod, P004M12_n1553ContagemResultado_CntSrvCod, P004M12_A684ContagemResultado_PFBFSUltima, P004M12_A685ContagemResultado_PFLFSUltima, P004M12_A682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               P004M13_A473ContagemResultado_DataCnt, P004M13_A456ContagemResultado_Codigo, P004M13_A470ContagemResultado_ContadorFMCod, P004M13_A459ContagemResultado_PFLFS, P004M13_n459ContagemResultado_PFLFS, P004M13_A458ContagemResultado_PFBFS, P004M13_n458ContagemResultado_PFBFS, P004M13_A461ContagemResultado_PFLFM, P004M13_n461ContagemResultado_PFLFM, P004M13_A460ContagemResultado_PFBFM,
               P004M13_n460ContagemResultado_PFBFM, P004M13_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               P004M15_A490ContagemResultado_ContratadaCod, P004M15_n490ContagemResultado_ContratadaCod, P004M15_A1553ContagemResultado_CntSrvCod, P004M15_n1553ContagemResultado_CntSrvCod, P004M15_A456ContagemResultado_Codigo, P004M15_A484ContagemResultado_StatusDmn, P004M15_n484ContagemResultado_StatusDmn, P004M15_A890ContagemResultado_Responsavel, P004M15_n890ContagemResultado_Responsavel, P004M15_A602ContagemResultado_OSVinculada,
               P004M15_n602ContagemResultado_OSVinculada, P004M15_A457ContagemResultado_Demanda, P004M15_n457ContagemResultado_Demanda, P004M15_A805ContagemResultado_ContratadaOrigemCod, P004M15_n805ContagemResultado_ContratadaOrigemCod, P004M15_A601ContagemResultado_Servico, P004M15_n601ContagemResultado_Servico, P004M15_A52Contratada_AreaTrabalhoCod, P004M15_n52Contratada_AreaTrabalhoCod, P004M15_A472ContagemResultado_DataEntrega,
               P004M15_n472ContagemResultado_DataEntrega, P004M15_A912ContagemResultado_HoraEntrega, P004M15_n912ContagemResultado_HoraEntrega
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004M19_A70ContratadaUsuario_UsuarioPessoaCod, P004M19_n70ContratadaUsuario_UsuarioPessoaCod, P004M19_A71ContratadaUsuario_UsuarioPessoaNom, P004M19_n71ContratadaUsuario_UsuarioPessoaNom, P004M19_A1394ContratadaUsuario_UsuarioAtivo, P004M19_n1394ContratadaUsuario_UsuarioAtivo, P004M19_A516Contratada_TipoFabrica, P004M19_n516Contratada_TipoFabrica, P004M19_A66ContratadaUsuario_ContratadaCod, P004M19_A1228ContratadaUsuario_AreaTrabalhoCod,
               P004M19_n1228ContratadaUsuario_AreaTrabalhoCod, P004M19_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV16ColDmnn ;
      private short AV60ColDataCntn ;
      private short AV44PraLinha ;
      private short AV9ColContadorFMn ;
      private short AV57ColPFBFSn ;
      private short AV58ColPFLFSn ;
      private short AV17ColPFBFMn ;
      private short AV18ColPFLFMn ;
      private short AV98ColParecern ;
      private short GxWebError ;
      private short AV39Ln ;
      private short AV28ErrCod ;
      private short AV112GXLvl65 ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short AV83Aprovadas ;
      private short AV114GXLvl144 ;
      private short AV70ContagemResultado_StatusCnt ;
      private short AV82Divergencias ;
      private short AV29Erros ;
      private short AV36Lidas ;
      private short AV116GXLvl219 ;
      private short AV34i ;
      private short AV43p ;
      private short AV35LastSpace ;
      private short AV45s ;
      private int AV78ContagemResultado_ContratadaCod ;
      private int AV94Servico ;
      private int AV87ContagemResultado_ContadorFMCod ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV20ContadorFMCod ;
      private int AV24Contratada_Codigo ;
      private int AV19ContadorCodigo ;
      private int AV59ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int GX_INS72 ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int AV106ContratoServicos_Codigo ;
      private int Gx_OldLine ;
      private int A601ContagemResultado_Servico ;
      private int A890ContagemResultado_Responsavel ;
      private int A602ContagemResultado_OSVinculada ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int GX_I ;
      private int AV51WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private decimal AV64PFBFM ;
      private decimal AV65PFLFM ;
      private decimal AV62PFBFS ;
      private decimal AV63PFLFS ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal AV105Servico_Percentual ;
      private decimal AV89ContagemResultado_CstUntPrd ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV76PFFM ;
      private decimal AV75PFFS ;
      private decimal AV69ContagemResultado_PFLFM ;
      private decimal AV68ContagemResultado_PFLFS ;
      private decimal AV71ContagemResultado_Divergencia ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV93PF ;
      private decimal AV77IndiceDivergencia ;
      private decimal AV80ContagemResultado_ValorPF ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV92OQImportar ;
      private String AV8Arquivo ;
      private String AV84Aba ;
      private String AV90FileName ;
      private String AV61ArqTitulo ;
      private String AV101Titulo ;
      private String AV37Linha ;
      private String AV23ContadorNome ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV88ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV91CalculoPFinal ;
      private String Gx_emsg ;
      private String AV99Acao ;
      private String AV100NovoStatus ;
      private String AV49Totais ;
      private String [] AV54Nomes ;
      private String A516Contratada_TipoFabrica ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String AV41NomeAchado ;
      private String AV81CalculoDivergencia ;
      private String Gx_time ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV102Prazo ;
      private DateTime AV95DataCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV96ContagemResultado_DataCnt ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV85Final ;
      private bool returnInSub ;
      private bool AV32FMFlag ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A517ContagemResultado_Ultima ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool AV104TemContagens ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool AV31Flag ;
      private bool AV33FSFlag ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n516Contratada_TipoFabrica ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String AV97ContagemResultado_ParecerTcn ;
      private String A463ContagemResultado_ParecerTcn ;
      private String AV79Demanda ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P004M2_A456ContagemResultado_Codigo ;
      private int[] P004M2_A490ContagemResultado_ContratadaCod ;
      private bool[] P004M2_n490ContagemResultado_ContratadaCod ;
      private String[] P004M2_A457ContagemResultado_Demanda ;
      private bool[] P004M2_n457ContagemResultado_Demanda ;
      private String[] P004M2_A484ContagemResultado_StatusDmn ;
      private bool[] P004M2_n484ContagemResultado_StatusDmn ;
      private decimal[] P004M2_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P004M2_n1596ContagemResultado_CntSrvPrc ;
      private int[] P004M2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004M2_n1553ContagemResultado_CntSrvCod ;
      private int[] P004M3_A456ContagemResultado_Codigo ;
      private bool[] P004M3_A517ContagemResultado_Ultima ;
      private int[] P004M3_A470ContagemResultado_ContadorFMCod ;
      private DateTime[] P004M3_A473ContagemResultado_DataCnt ;
      private String[] P004M3_A511ContagemResultado_HoraCnt ;
      private int[] P004M7_A456ContagemResultado_Codigo ;
      private bool[] P004M7_A517ContagemResultado_Ultima ;
      private decimal[] P004M7_A460ContagemResultado_PFBFM ;
      private bool[] P004M7_n460ContagemResultado_PFBFM ;
      private decimal[] P004M7_A461ContagemResultado_PFLFM ;
      private bool[] P004M7_n461ContagemResultado_PFLFM ;
      private int[] P004M7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004M7_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P004M7_A458ContagemResultado_PFBFS ;
      private bool[] P004M7_n458ContagemResultado_PFBFS ;
      private decimal[] P004M7_A459ContagemResultado_PFLFS ;
      private bool[] P004M7_n459ContagemResultado_PFLFS ;
      private short[] P004M7_A483ContagemResultado_StatusCnt ;
      private decimal[] P004M7_A462ContagemResultado_Divergencia ;
      private DateTime[] P004M7_A473ContagemResultado_DataCnt ;
      private String[] P004M7_A511ContagemResultado_HoraCnt ;
      private int[] P004M8_A456ContagemResultado_Codigo ;
      private String[] P004M8_A484ContagemResultado_StatusDmn ;
      private bool[] P004M8_n484ContagemResultado_StatusDmn ;
      private int[] P004M12_A601ContagemResultado_Servico ;
      private bool[] P004M12_n601ContagemResultado_Servico ;
      private int[] P004M12_A490ContagemResultado_ContratadaCod ;
      private bool[] P004M12_n490ContagemResultado_ContratadaCod ;
      private String[] P004M12_A457ContagemResultado_Demanda ;
      private bool[] P004M12_n457ContagemResultado_Demanda ;
      private int[] P004M12_A456ContagemResultado_Codigo ;
      private String[] P004M12_A484ContagemResultado_StatusDmn ;
      private bool[] P004M12_n484ContagemResultado_StatusDmn ;
      private decimal[] P004M12_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P004M12_n1596ContagemResultado_CntSrvPrc ;
      private decimal[] P004M12_A798ContagemResultado_PFBFSImp ;
      private bool[] P004M12_n798ContagemResultado_PFBFSImp ;
      private decimal[] P004M12_A799ContagemResultado_PFLFSImp ;
      private bool[] P004M12_n799ContagemResultado_PFLFSImp ;
      private int[] P004M12_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004M12_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P004M12_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P004M12_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P004M12_A682ContagemResultado_PFBFMUltima ;
      private DateTime[] P004M13_A473ContagemResultado_DataCnt ;
      private int[] P004M13_A456ContagemResultado_Codigo ;
      private int[] P004M13_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P004M13_A459ContagemResultado_PFLFS ;
      private bool[] P004M13_n459ContagemResultado_PFLFS ;
      private decimal[] P004M13_A458ContagemResultado_PFBFS ;
      private bool[] P004M13_n458ContagemResultado_PFBFS ;
      private decimal[] P004M13_A461ContagemResultado_PFLFM ;
      private bool[] P004M13_n461ContagemResultado_PFLFM ;
      private decimal[] P004M13_A460ContagemResultado_PFBFM ;
      private bool[] P004M13_n460ContagemResultado_PFBFM ;
      private String[] P004M13_A511ContagemResultado_HoraCnt ;
      private int[] P004M15_A490ContagemResultado_ContratadaCod ;
      private bool[] P004M15_n490ContagemResultado_ContratadaCod ;
      private int[] P004M15_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004M15_n1553ContagemResultado_CntSrvCod ;
      private int[] P004M15_A456ContagemResultado_Codigo ;
      private String[] P004M15_A484ContagemResultado_StatusDmn ;
      private bool[] P004M15_n484ContagemResultado_StatusDmn ;
      private int[] P004M15_A890ContagemResultado_Responsavel ;
      private bool[] P004M15_n890ContagemResultado_Responsavel ;
      private int[] P004M15_A602ContagemResultado_OSVinculada ;
      private bool[] P004M15_n602ContagemResultado_OSVinculada ;
      private String[] P004M15_A457ContagemResultado_Demanda ;
      private bool[] P004M15_n457ContagemResultado_Demanda ;
      private int[] P004M15_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P004M15_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P004M15_A601ContagemResultado_Servico ;
      private bool[] P004M15_n601ContagemResultado_Servico ;
      private int[] P004M15_A52Contratada_AreaTrabalhoCod ;
      private bool[] P004M15_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] P004M15_A472ContagemResultado_DataEntrega ;
      private bool[] P004M15_n472ContagemResultado_DataEntrega ;
      private DateTime[] P004M15_A912ContagemResultado_HoraEntrega ;
      private bool[] P004M15_n912ContagemResultado_HoraEntrega ;
      private int[] P004M19_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P004M19_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P004M19_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P004M19_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P004M19_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P004M19_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] P004M19_A516Contratada_TipoFabrica ;
      private bool[] P004M19_n516Contratada_TipoFabrica ;
      private int[] P004M19_A66ContratadaUsuario_ContratadaCod ;
      private int[] P004M19_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P004M19_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P004M19_A69ContratadaUsuario_UsuarioCod ;
      private ExcelDocumentI AV30ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV51WWPContext ;
   }

   public class aprc_importarcontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P004M19( IGxContext context ,
                                              bool AV33FSFlag ,
                                              bool AV32FMFlag ,
                                              int A66ContratadaUsuario_ContratadaCod ,
                                              int AV24Contratada_Codigo ,
                                              String A516Contratada_TipoFabrica ,
                                              bool A1394ContratadaUsuario_UsuarioAtivo ,
                                              String A71ContratadaUsuario_UsuarioPessoaNom ,
                                              String []  aP0_Nomes ,
                                              int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                              int AV51WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String [] AV54Nomes = aP0_Nomes ;
         scmdbuf = "SELECT T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Contratada_TipoFabrica], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Usuario_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Contratada_AreaTrabalhoCod] = @AV51WWPC_1Areatrabalho_codigo)";
         if ( AV33FSFlag )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_ContratadaCod] = @AV24Contratada_Codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV32FMFlag )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_TipoFabrica] = 'M')";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 16 :
                     return conditional_P004M19(context, (bool)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String[])dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004M2 ;
          prmP004M2 = new Object[] {
          new Object[] {"@AV79Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M3 ;
          prmP004M3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M4 ;
          prmP004M4 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP004M5 ;
          prmP004M5 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M6 ;
          prmP004M6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004M7 ;
          prmP004M7 = new Object[] {
          new Object[] {"@AV59ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M8 ;
          prmP004M8 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M9 ;
          prmP004M9 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M10 ;
          prmP004M10 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP004M12 ;
          prmP004M12 = new Object[] {
          new Object[] {"@AV79Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV94Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M13 ;
          prmP004M13 = new Object[] {
          new Object[] {"@AV59ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96ContagemResultado_DataCnt",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP004M14 ;
          prmP004M14 = new Object[] {
          new Object[] {"@AV59ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M15 ;
          prmP004M15 = new Object[] {
          new Object[] {"@AV59ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M16 ;
          prmP004M16 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M17 ;
          prmP004M17 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004M18 ;
          prmP004M18 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004M19 ;
          prmP004M19 = new Object[] {
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004M2", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Demanda] = @AV79Demanda) AND (T1.[ContagemResultado_ContratadaCod] = @AV24Contratada_Codigo) ORDER BY T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M2,1,0,true,false )
             ,new CursorDef("P004M3", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_ContadorFMCod], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M3,1,0,true,false )
             ,new CursorDef("P004M4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M4)
             ,new CursorDef("P004M5", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M5)
             ,new CursorDef("P004M6", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004M6)
             ,new CursorDef("P004M7", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV59ContagemResultado_Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M7,1,0,true,false )
             ,new CursorDef("P004M8", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M8,1,0,true,true )
             ,new CursorDef("P004M9", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M9)
             ,new CursorDef("P004M10", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M10)
             ,new CursorDef("P004M12", "SELECT TOP 1 T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T3.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T2.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T2.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Demanda] = @AV79Demanda) AND (T1.[ContagemResultado_ContratadaCod] = @AV24Contratada_Codigo) AND (T3.[Servico_Codigo] = @AV94Servico) ORDER BY T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M12,1,0,true,true )
             ,new CursorDef("P004M13", "SELECT [ContagemResultado_DataCnt], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV59ContagemResultado_Codigo and [ContagemResultado_DataCnt] = @AV96ContagemResultado_DataCnt ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M13,100,0,false,false )
             ,new CursorDef("P004M14", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=CONVERT(BIT, 0)  WHERE [ContagemResultado_Codigo] = @AV59ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M14)
             ,new CursorDef("P004M15", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaOrigemCod], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega] FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV59ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M15,1,0,true,true )
             ,new CursorDef("P004M16", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M16)
             ,new CursorDef("P004M17", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004M17)
             ,new CursorDef("P004M18", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004M18)
             ,new CursorDef("P004M19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004M19,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(10) ;
                ((String[]) buf[15])[0] = rslt.getString(11, 5) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((decimal[]) buf[19])[0] = rslt.getDecimal(12) ;
                return;
             case 10 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 5) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (short)parms[4]);
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                return;
             case 16 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
