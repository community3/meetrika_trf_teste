/*
               File: PRC_AtributosExcel
        Description: Importar Atributos de Excel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:51.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_atributosexcel : GXProcedure
   {
      public prc_atributosexcel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_atributosexcel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_File_UsuarioCod ,
                           int aP1_Sistema_Codigo ,
                           out short aP2_Ln )
      {
         this.AV13File_UsuarioCod = aP0_File_UsuarioCod;
         this.AV17Sistema_Codigo = aP1_Sistema_Codigo;
         this.AV15Ln = 0 ;
         initialize();
         executePrivate();
         aP2_Ln=this.AV15Ln;
      }

      public short executeUdp( int aP0_File_UsuarioCod ,
                               int aP1_Sistema_Codigo )
      {
         this.AV13File_UsuarioCod = aP0_File_UsuarioCod;
         this.AV17Sistema_Codigo = aP1_Sistema_Codigo;
         this.AV15Ln = 0 ;
         initialize();
         executePrivate();
         aP2_Ln=this.AV15Ln;
         return AV15Ln ;
      }

      public void executeSubmit( int aP0_File_UsuarioCod ,
                                 int aP1_Sistema_Codigo ,
                                 out short aP2_Ln )
      {
         prc_atributosexcel objprc_atributosexcel;
         objprc_atributosexcel = new prc_atributosexcel();
         objprc_atributosexcel.AV13File_UsuarioCod = aP0_File_UsuarioCod;
         objprc_atributosexcel.AV17Sistema_Codigo = aP1_Sistema_Codigo;
         objprc_atributosexcel.AV15Ln = 0 ;
         objprc_atributosexcel.context.SetSubmitInitialConfig(context);
         objprc_atributosexcel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_atributosexcel);
         aP2_Ln=this.AV15Ln;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_atributosexcel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV16Nome = "\\ImportarAtributos.xlsx";
         AV11ExcelDocument.Open(AV16Nome);
         AV15Ln = 2;
         while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ExcelDocument.get_Cells(AV15Ln, 1, 1, 1).Text)) )
         {
            AV14linea = StringUtil.Str( (decimal)(AV15Ln-1), 10, 0) + ") " + AV11ExcelDocument.get_Cells(AV15Ln, 1, 1, 1).Text + ", " + AV11ExcelDocument.get_Cells(AV15Ln, 2, 1, 1).Text + ", " + StringUtil.Str( (decimal)(AV11ExcelDocument.get_Cells(AV15Ln, 3, 1, 1).Number), 10, 2) + ", " + AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Text + ", " + AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Text + ", " + AV11ExcelDocument.get_Cells(AV15Ln, 6, 1, 1).Text;
            AV9Atributos_Nome = StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 1, 1, 1).Text);
            AV19Tabela_Nome = StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 6, 1, 1).Text);
            AV20TipoDados = StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 2, 1, 1).Text);
            if ( StringUtil.StringSearch( AV20TipoDados, "NUM", 1) > 0 )
            {
               AV10Atributos_TipoDados = "N";
            }
            else if ( StringUtil.StringSearch( AV20TipoDados, "VAR", 1) > 0 )
            {
               AV10Atributos_TipoDados = "VC";
            }
            else if ( StringUtil.StringSearch( AV20TipoDados, "CHAR", 1) > 0 )
            {
               AV10Atributos_TipoDados = "C";
            }
            else if ( StringUtil.StringSearch( AV20TipoDados, "TIME", 1) > 0 )
            {
               AV10Atributos_TipoDados = "DT";
            }
            else if ( StringUtil.StringSearch( AV20TipoDados, "DAT", 1) > 0 )
            {
               AV10Atributos_TipoDados = "D";
            }
            else if ( StringUtil.StringSearch( AV20TipoDados, "BLOB", 1) > 0 )
            {
               AV10Atributos_TipoDados = "Blob";
            }
            else if ( ( StringUtil.StringSearch( AV20TipoDados, "BOOL", 1) > 0 ) || ( StringUtil.StringSearch( AV20TipoDados, "LOGIC", 1) > 0 ) )
            {
               AV10Atributos_TipoDados = "Bool";
            }
            else
            {
               AV10Atributos_TipoDados = "Outr";
            }
            AV24GXLvl38 = 0;
            AV25Udparg1 = new prc_padronizastring(context).executeUdp(  AV19Tabela_Nome);
            /* Using cursor P002X2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A172Tabela_Codigo = P002X2_A172Tabela_Codigo[0];
               A173Tabela_Nome = P002X2_A173Tabela_Nome[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A173Tabela_Nome), AV25Udparg1) == 0 )
               {
                  AV24GXLvl38 = 1;
                  AV26GXLvl40 = 0;
                  AV27Udparg2 = new prc_padronizastring(context).executeUdp(  AV9Atributos_Nome);
                  /* Using cursor P002X3 */
                  pr_default.execute(1);
                  while ( (pr_default.getStatus(1) != 101) )
                  {
                     A177Atributos_Nome = P002X3_A177Atributos_Nome[0];
                     A178Atributos_TipoDados = P002X3_A178Atributos_TipoDados[0];
                     n178Atributos_TipoDados = P002X3_n178Atributos_TipoDados[0];
                     A390Atributos_Detalhes = P002X3_A390Atributos_Detalhes[0];
                     n390Atributos_Detalhes = P002X3_n390Atributos_Detalhes[0];
                     A400Atributos_PK = P002X3_A400Atributos_PK[0];
                     n400Atributos_PK = P002X3_n400Atributos_PK[0];
                     A401Atributos_FK = P002X3_A401Atributos_FK[0];
                     n401Atributos_FK = P002X3_n401Atributos_FK[0];
                     A176Atributos_Codigo = P002X3_A176Atributos_Codigo[0];
                     if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A177Atributos_Nome), AV27Udparg2) == 0 )
                     {
                        AV26GXLvl40 = 1;
                        A178Atributos_TipoDados = AV10Atributos_TipoDados;
                        n178Atributos_TipoDados = false;
                        A390Atributos_Detalhes = "(" + StringUtil.Trim( StringUtil.Str( (decimal)(AV11ExcelDocument.get_Cells(AV15Ln, 3, 1, 1).Number), 10, 2)) + ")";
                        n390Atributos_Detalhes = false;
                        A400Atributos_PK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Number==1)));
                        n400Atributos_PK = false;
                        A401Atributos_FK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Number==1)));
                        n401Atributos_FK = false;
                        AV14linea = AV14linea + " ->  Atributo atualizado";
                        /* Using cursor P002X4 */
                        pr_default.execute(2, new Object[] {n178Atributos_TipoDados, A178Atributos_TipoDados, n390Atributos_Detalhes, A390Atributos_Detalhes, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, A176Atributos_Codigo});
                        pr_default.close(2);
                        dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     }
                     pr_default.readNext(1);
                  }
                  pr_default.close(1);
                  if ( AV26GXLvl40 == 0 )
                  {
                     /*
                        INSERT RECORD ON TABLE Atributos

                     */
                     A177Atributos_Nome = AV9Atributos_Nome;
                     A178Atributos_TipoDados = AV10Atributos_TipoDados;
                     n178Atributos_TipoDados = false;
                     A390Atributos_Detalhes = "(" + StringUtil.Trim( StringUtil.Str( (decimal)(AV11ExcelDocument.get_Cells(AV15Ln, 3, 1, 1).Number), 10, 2)) + ")";
                     n390Atributos_Detalhes = false;
                     A400Atributos_PK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Number==1)));
                     n400Atributos_PK = false;
                     A401Atributos_FK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Number==1)));
                     n401Atributos_FK = false;
                     A356Atributos_TabelaCod = A172Tabela_Codigo;
                     AV14linea = AV14linea + " -> Novo atributo";
                     A180Atributos_Ativo = true;
                     /* Using cursor P002X5 */
                     pr_default.execute(3, new Object[] {A177Atributos_Nome, n178Atributos_TipoDados, A178Atributos_TipoDados, A180Atributos_Ativo, A356Atributos_TabelaCod, n390Atributos_Detalhes, A390Atributos_Detalhes, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK});
                     A176Atributos_Codigo = P002X5_A176Atributos_Codigo[0];
                     pr_default.close(3);
                     dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     if ( (pr_default.getStatus(3) == 1) )
                     {
                        context.Gx_err = 1;
                        Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                     }
                     else
                     {
                        context.Gx_err = 0;
                        Gx_emsg = "";
                     }
                     /* End Insert */
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV24GXLvl38 == 0 )
            {
               /*
                  INSERT RECORD ON TABLE Tabela

               */
               A173Tabela_Nome = AV19Tabela_Nome;
               A190Tabela_SistemaCod = AV17Sistema_Codigo;
               A188Tabela_ModuloCod = 0;
               n188Tabela_ModuloCod = false;
               n188Tabela_ModuloCod = true;
               A181Tabela_PaiCod = 0;
               n181Tabela_PaiCod = false;
               n181Tabela_PaiCod = true;
               AV14linea = AV14linea + " ->  Nova tabela";
               A174Tabela_Ativo = true;
               /* Using cursor P002X6 */
               pr_default.execute(4, new Object[] {A173Tabela_Nome, A174Tabela_Ativo, n181Tabela_PaiCod, A181Tabela_PaiCod, n188Tabela_ModuloCod, A188Tabela_ModuloCod, A190Tabela_SistemaCod});
               A172Tabela_Codigo = P002X6_A172Tabela_Codigo[0];
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
               if ( (pr_default.getStatus(4) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV18Tabela_Codigo = A172Tabela_Codigo;
               /*
                  INSERT RECORD ON TABLE Atributos

               */
               A177Atributos_Nome = AV9Atributos_Nome;
               A178Atributos_TipoDados = AV10Atributos_TipoDados;
               n178Atributos_TipoDados = false;
               A390Atributos_Detalhes = "(" + StringUtil.Trim( StringUtil.Str( (decimal)(AV11ExcelDocument.get_Cells(AV15Ln, 3, 1, 1).Number), 10, 2)) + ")";
               n390Atributos_Detalhes = false;
               A400Atributos_PK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 4, 1, 1).Number==1)));
               n400Atributos_PK = false;
               A401Atributos_FK = (bool)(((StringUtil.StrCmp(StringUtil.Upper( AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Text), "TRUE")==0))||((AV11ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Number==1)));
               n401Atributos_FK = false;
               A356Atributos_TabelaCod = AV18Tabela_Codigo;
               AV14linea = AV14linea + ", novo atributo";
               A180Atributos_Ativo = true;
               /* Using cursor P002X7 */
               pr_default.execute(5, new Object[] {A177Atributos_Nome, n178Atributos_TipoDados, A178Atributos_TipoDados, A180Atributos_Ativo, A356Atributos_TabelaCod, n390Atributos_Detalhes, A390Atributos_Detalhes, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK});
               A176Atributos_Codigo = P002X7_A176Atributos_Codigo[0];
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
               if ( (pr_default.getStatus(5) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
            AV15Ln = (short)(AV15Ln+1);
         }
         AV15Ln = (short)(AV15Ln-2);
         AV11ExcelDocument.Close();
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AtributosExcel");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV16Nome = "";
         AV11ExcelDocument = new ExcelDocumentI();
         AV14linea = "";
         AV9Atributos_Nome = "";
         AV19Tabela_Nome = "";
         AV20TipoDados = "";
         AV10Atributos_TipoDados = "";
         AV25Udparg1 = "";
         scmdbuf = "";
         P002X2_A172Tabela_Codigo = new int[1] ;
         P002X2_A173Tabela_Nome = new String[] {""} ;
         A173Tabela_Nome = "";
         AV27Udparg2 = "";
         P002X3_A177Atributos_Nome = new String[] {""} ;
         P002X3_A178Atributos_TipoDados = new String[] {""} ;
         P002X3_n178Atributos_TipoDados = new bool[] {false} ;
         P002X3_A390Atributos_Detalhes = new String[] {""} ;
         P002X3_n390Atributos_Detalhes = new bool[] {false} ;
         P002X3_A400Atributos_PK = new bool[] {false} ;
         P002X3_n400Atributos_PK = new bool[] {false} ;
         P002X3_A401Atributos_FK = new bool[] {false} ;
         P002X3_n401Atributos_FK = new bool[] {false} ;
         P002X3_A176Atributos_Codigo = new int[1] ;
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         A390Atributos_Detalhes = "";
         P002X5_A176Atributos_Codigo = new int[1] ;
         Gx_emsg = "";
         P002X6_A172Tabela_Codigo = new int[1] ;
         P002X7_A176Atributos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_atributosexcel__default(),
            new Object[][] {
                new Object[] {
               P002X2_A172Tabela_Codigo, P002X2_A173Tabela_Nome
               }
               , new Object[] {
               P002X3_A177Atributos_Nome, P002X3_A178Atributos_TipoDados, P002X3_n178Atributos_TipoDados, P002X3_A390Atributos_Detalhes, P002X3_n390Atributos_Detalhes, P002X3_A400Atributos_PK, P002X3_n400Atributos_PK, P002X3_A401Atributos_FK, P002X3_n401Atributos_FK, P002X3_A176Atributos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P002X5_A176Atributos_Codigo
               }
               , new Object[] {
               P002X6_A172Tabela_Codigo
               }
               , new Object[] {
               P002X7_A176Atributos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15Ln ;
      private short AV24GXLvl38 ;
      private short AV26GXLvl40 ;
      private int AV13File_UsuarioCod ;
      private int AV17Sistema_Codigo ;
      private int A172Tabela_Codigo ;
      private int A176Atributos_Codigo ;
      private int GX_INS40 ;
      private int A356Atributos_TabelaCod ;
      private int GX_INS39 ;
      private int A190Tabela_SistemaCod ;
      private int A188Tabela_ModuloCod ;
      private int A181Tabela_PaiCod ;
      private int AV18Tabela_Codigo ;
      private String AV9Atributos_Nome ;
      private String AV19Tabela_Nome ;
      private String AV10Atributos_TipoDados ;
      private String scmdbuf ;
      private String A173Tabela_Nome ;
      private String A177Atributos_Nome ;
      private String A178Atributos_TipoDados ;
      private String A390Atributos_Detalhes ;
      private String Gx_emsg ;
      private bool n178Atributos_TipoDados ;
      private bool n390Atributos_Detalhes ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool A180Atributos_Ativo ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool A174Tabela_Ativo ;
      private String AV16Nome ;
      private String AV14linea ;
      private String AV20TipoDados ;
      private String AV25Udparg1 ;
      private String AV27Udparg2 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P002X2_A172Tabela_Codigo ;
      private String[] P002X2_A173Tabela_Nome ;
      private String[] P002X3_A177Atributos_Nome ;
      private String[] P002X3_A178Atributos_TipoDados ;
      private bool[] P002X3_n178Atributos_TipoDados ;
      private String[] P002X3_A390Atributos_Detalhes ;
      private bool[] P002X3_n390Atributos_Detalhes ;
      private bool[] P002X3_A400Atributos_PK ;
      private bool[] P002X3_n400Atributos_PK ;
      private bool[] P002X3_A401Atributos_FK ;
      private bool[] P002X3_n401Atributos_FK ;
      private int[] P002X3_A176Atributos_Codigo ;
      private int[] P002X5_A176Atributos_Codigo ;
      private int[] P002X6_A172Tabela_Codigo ;
      private int[] P002X7_A176Atributos_Codigo ;
      private short aP2_Ln ;
      private ExcelDocumentI AV11ExcelDocument ;
   }

   public class prc_atributosexcel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002X2 ;
          prmP002X2 = new Object[] {
          } ;
          Object[] prmP002X3 ;
          prmP002X3 = new Object[] {
          } ;
          Object[] prmP002X4 ;
          prmP002X4 = new Object[] {
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002X5 ;
          prmP002X5 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP002X6 ;
          prmP002X6 = new Object[] {
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002X7 ;
          prmP002X7 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002X2", "SELECT [Tabela_Codigo], [Tabela_Nome] FROM [Tabela] WITH (NOLOCK) ORDER BY [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002X2,100,0,true,false )
             ,new CursorDef("P002X3", "SELECT [Atributos_Nome], [Atributos_TipoDados], [Atributos_Detalhes], [Atributos_PK], [Atributos_FK], [Atributos_Codigo] FROM [Atributos] WITH (UPDLOCK) ORDER BY [Atributos_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002X3,1,0,true,false )
             ,new CursorDef("P002X4", "UPDATE [Atributos] SET [Atributos_TipoDados]=@Atributos_TipoDados, [Atributos_Detalhes]=@Atributos_Detalhes, [Atributos_PK]=@Atributos_PK, [Atributos_FK]=@Atributos_FK  WHERE [Atributos_Codigo] = @Atributos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002X4)
             ,new CursorDef("P002X5", "INSERT INTO [Atributos]([Atributos_Nome], [Atributos_TipoDados], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_Detalhes], [Atributos_PK], [Atributos_FK], [Atributos_Descricao], [Atributos_MelhoraCod]) VALUES(@Atributos_Nome, @Atributos_TipoDados, @Atributos_Ativo, @Atributos_TabelaCod, @Atributos_Detalhes, @Atributos_PK, @Atributos_FK, '', convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP002X5)
             ,new CursorDef("P002X6", "INSERT INTO [Tabela]([Tabela_Nome], [Tabela_Ativo], [Tabela_PaiCod], [Tabela_ModuloCod], [Tabela_SistemaCod], [Tabela_Descricao], [Tabela_MelhoraCod]) VALUES(@Tabela_Nome, @Tabela_Ativo, @Tabela_PaiCod, @Tabela_ModuloCod, @Tabela_SistemaCod, '', convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP002X6)
             ,new CursorDef("P002X7", "INSERT INTO [Atributos]([Atributos_Nome], [Atributos_TipoDados], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_Detalhes], [Atributos_PK], [Atributos_FK], [Atributos_Descricao], [Atributos_MelhoraCod]) VALUES(@Atributos_Nome, @Atributos_TipoDados, @Atributos_Ativo, @Atributos_TabelaCod, @Atributos_Detalhes, @Atributos_PK, @Atributos_FK, '', convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP002X7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 4) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                return;
       }
    }

 }

}
