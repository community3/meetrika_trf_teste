/*
               File: GetPromptGeral_UnidadeOrganizacionalFilterData
        Description: Get Prompt Geral_Unidade Organizacional Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:2.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptgeral_unidadeorganizacionalfilterdata : GXProcedure
   {
      public getpromptgeral_unidadeorganizacionalfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptgeral_unidadeorganizacionalfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptgeral_unidadeorganizacionalfilterdata objgetpromptgeral_unidadeorganizacionalfilterdata;
         objgetpromptgeral_unidadeorganizacionalfilterdata = new getpromptgeral_unidadeorganizacionalfilterdata();
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV20DDOName = aP0_DDOName;
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV24OptionsJson = "" ;
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV27OptionsDescJson = "" ;
         objgetpromptgeral_unidadeorganizacionalfilterdata.AV29OptionIndexesJson = "" ;
         objgetpromptgeral_unidadeorganizacionalfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptgeral_unidadeorganizacionalfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptgeral_unidadeorganizacionalfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptgeral_unidadeorganizacionalfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("PromptGeral_UnidadeOrganizacionalGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptGeral_UnidadeOrganizacionalGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("PromptGeral_UnidadeOrganizacionalGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "UNIDADEORGANIZACIONAL_ATIVO") == 0 )
            {
               AV36UnidadeOrganizacional_Ativo = BooleanUtil.Val( AV34GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV10TFUnidadeOrganizacional_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV11TFUnidadeOrganizacional_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTPUO_CODIGO") == 0 )
            {
               AV12TFTpUo_Codigo = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV13TFTpUo_Codigo_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV14TFEstado_UF = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV15TFEstado_UF_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV16TFUnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV17TFUnidadeOrganizacional_Vinculada_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV38UnidadeOrganizacional_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV41UnidadeOrganizacional_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV42DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV43DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV44UnidadeOrganizacional_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' Routine */
         AV10TFUnidadeOrganizacional_Nome = AV18SearchTxt;
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV37DynamicFiltersSelector1 ,
                                              AV38UnidadeOrganizacional_Nome1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41UnidadeOrganizacional_Nome2 ,
                                              AV42DynamicFiltersEnabled3 ,
                                              AV43DynamicFiltersSelector3 ,
                                              AV44UnidadeOrganizacional_Nome3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV12TFTpUo_Codigo ,
                                              AV13TFTpUo_Codigo_To ,
                                              AV15TFEstado_UF_Sel ,
                                              AV14TFEstado_UF ,
                                              AV16TFUnidadeOrganizacional_Vinculada ,
                                              AV17TFUnidadeOrganizacional_Vinculada_To ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A609TpUo_Codigo ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV38UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV38UnidadeOrganizacional_Nome1), 50, "%");
         lV41UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2), 50, "%");
         lV44UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV14TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV14TFEstado_UF), 2, "%");
         /* Using cursor P00NO2 */
         pr_default.execute(0, new Object[] {lV38UnidadeOrganizacional_Nome1, lV41UnidadeOrganizacional_Nome2, lV44UnidadeOrganizacional_Nome3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, AV12TFTpUo_Codigo, AV13TFTpUo_Codigo_To, lV14TFEstado_UF, AV15TFEstado_UF_Sel, AV16TFUnidadeOrganizacional_Vinculada, AV17TFUnidadeOrganizacional_Vinculada_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNO2 = false;
            A629UnidadeOrganizacional_Ativo = P00NO2_A629UnidadeOrganizacional_Ativo[0];
            A612UnidadeOrganizacional_Nome = P00NO2_A612UnidadeOrganizacional_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NO2_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NO2_n613UnidadeOrganizacional_Vinculada[0];
            A23Estado_UF = P00NO2_A23Estado_UF[0];
            A609TpUo_Codigo = P00NO2_A609TpUo_Codigo[0];
            A611UnidadeOrganizacional_Codigo = P00NO2_A611UnidadeOrganizacional_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NO2_A612UnidadeOrganizacional_Nome[0], A612UnidadeOrganizacional_Nome) == 0 ) )
            {
               BRKNO2 = false;
               A611UnidadeOrganizacional_Codigo = P00NO2_A611UnidadeOrganizacional_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKNO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A612UnidadeOrganizacional_Nome)) )
            {
               AV22Option = A612UnidadeOrganizacional_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNO2 )
            {
               BRKNO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV14TFEstado_UF = AV18SearchTxt;
         AV15TFEstado_UF_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV37DynamicFiltersSelector1 ,
                                              AV38UnidadeOrganizacional_Nome1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41UnidadeOrganizacional_Nome2 ,
                                              AV42DynamicFiltersEnabled3 ,
                                              AV43DynamicFiltersSelector3 ,
                                              AV44UnidadeOrganizacional_Nome3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV12TFTpUo_Codigo ,
                                              AV13TFTpUo_Codigo_To ,
                                              AV15TFEstado_UF_Sel ,
                                              AV14TFEstado_UF ,
                                              AV16TFUnidadeOrganizacional_Vinculada ,
                                              AV17TFUnidadeOrganizacional_Vinculada_To ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A609TpUo_Codigo ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV38UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV38UnidadeOrganizacional_Nome1), 50, "%");
         lV41UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2), 50, "%");
         lV44UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV14TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV14TFEstado_UF), 2, "%");
         /* Using cursor P00NO3 */
         pr_default.execute(1, new Object[] {lV38UnidadeOrganizacional_Nome1, lV41UnidadeOrganizacional_Nome2, lV44UnidadeOrganizacional_Nome3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, AV12TFTpUo_Codigo, AV13TFTpUo_Codigo_To, lV14TFEstado_UF, AV15TFEstado_UF_Sel, AV16TFUnidadeOrganizacional_Vinculada, AV17TFUnidadeOrganizacional_Vinculada_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNO4 = false;
            A23Estado_UF = P00NO3_A23Estado_UF[0];
            A613UnidadeOrganizacional_Vinculada = P00NO3_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NO3_n613UnidadeOrganizacional_Vinculada[0];
            A609TpUo_Codigo = P00NO3_A609TpUo_Codigo[0];
            A612UnidadeOrganizacional_Nome = P00NO3_A612UnidadeOrganizacional_Nome[0];
            A629UnidadeOrganizacional_Ativo = P00NO3_A629UnidadeOrganizacional_Ativo[0];
            A611UnidadeOrganizacional_Codigo = P00NO3_A611UnidadeOrganizacional_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00NO3_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKNO4 = false;
               A611UnidadeOrganizacional_Codigo = P00NO3_A611UnidadeOrganizacional_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKNO4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
            {
               AV22Option = A23Estado_UF;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNO4 )
            {
               BRKNO4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV36UnidadeOrganizacional_Ativo = true;
         AV10TFUnidadeOrganizacional_Nome = "";
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         AV14TFEstado_UF = "";
         AV15TFEstado_UF_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV38UnidadeOrganizacional_Nome1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV41UnidadeOrganizacional_Nome2 = "";
         AV43DynamicFiltersSelector3 = "";
         AV44UnidadeOrganizacional_Nome3 = "";
         scmdbuf = "";
         lV38UnidadeOrganizacional_Nome1 = "";
         lV41UnidadeOrganizacional_Nome2 = "";
         lV44UnidadeOrganizacional_Nome3 = "";
         lV10TFUnidadeOrganizacional_Nome = "";
         lV14TFEstado_UF = "";
         A612UnidadeOrganizacional_Nome = "";
         A23Estado_UF = "";
         P00NO2_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NO2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NO2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NO2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NO2_A23Estado_UF = new String[] {""} ;
         P00NO2_A609TpUo_Codigo = new int[1] ;
         P00NO2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         AV22Option = "";
         P00NO3_A23Estado_UF = new String[] {""} ;
         P00NO3_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NO3_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NO3_A609TpUo_Codigo = new int[1] ;
         P00NO3_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NO3_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NO3_A611UnidadeOrganizacional_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptgeral_unidadeorganizacionalfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NO2_A629UnidadeOrganizacional_Ativo, P00NO2_A612UnidadeOrganizacional_Nome, P00NO2_A613UnidadeOrganizacional_Vinculada, P00NO2_n613UnidadeOrganizacional_Vinculada, P00NO2_A23Estado_UF, P00NO2_A609TpUo_Codigo, P00NO2_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NO3_A23Estado_UF, P00NO3_A613UnidadeOrganizacional_Vinculada, P00NO3_n613UnidadeOrganizacional_Vinculada, P00NO3_A609TpUo_Codigo, P00NO3_A612UnidadeOrganizacional_Nome, P00NO3_A629UnidadeOrganizacional_Ativo, P00NO3_A611UnidadeOrganizacional_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV47GXV1 ;
      private int AV12TFTpUo_Codigo ;
      private int AV13TFTpUo_Codigo_To ;
      private int AV16TFUnidadeOrganizacional_Vinculada ;
      private int AV17TFUnidadeOrganizacional_Vinculada_To ;
      private int A609TpUo_Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A611UnidadeOrganizacional_Codigo ;
      private long AV30count ;
      private String AV10TFUnidadeOrganizacional_Nome ;
      private String AV11TFUnidadeOrganizacional_Nome_Sel ;
      private String AV14TFEstado_UF ;
      private String AV15TFEstado_UF_Sel ;
      private String AV38UnidadeOrganizacional_Nome1 ;
      private String AV41UnidadeOrganizacional_Nome2 ;
      private String AV44UnidadeOrganizacional_Nome3 ;
      private String scmdbuf ;
      private String lV38UnidadeOrganizacional_Nome1 ;
      private String lV41UnidadeOrganizacional_Nome2 ;
      private String lV44UnidadeOrganizacional_Nome3 ;
      private String lV10TFUnidadeOrganizacional_Nome ;
      private String lV14TFEstado_UF ;
      private String A612UnidadeOrganizacional_Nome ;
      private String A23Estado_UF ;
      private bool returnInSub ;
      private bool AV36UnidadeOrganizacional_Ativo ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV42DynamicFiltersEnabled3 ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool BRKNO2 ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool BRKNO4 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV43DynamicFiltersSelector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00NO2_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NO2_A612UnidadeOrganizacional_Nome ;
      private int[] P00NO2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NO2_n613UnidadeOrganizacional_Vinculada ;
      private String[] P00NO2_A23Estado_UF ;
      private int[] P00NO2_A609TpUo_Codigo ;
      private int[] P00NO2_A611UnidadeOrganizacional_Codigo ;
      private String[] P00NO3_A23Estado_UF ;
      private int[] P00NO3_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NO3_n613UnidadeOrganizacional_Vinculada ;
      private int[] P00NO3_A609TpUo_Codigo ;
      private String[] P00NO3_A612UnidadeOrganizacional_Nome ;
      private bool[] P00NO3_A629UnidadeOrganizacional_Ativo ;
      private int[] P00NO3_A611UnidadeOrganizacional_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getpromptgeral_unidadeorganizacionalfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NO2( IGxContext context ,
                                             String AV37DynamicFiltersSelector1 ,
                                             String AV38UnidadeOrganizacional_Nome1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41UnidadeOrganizacional_Nome2 ,
                                             bool AV42DynamicFiltersEnabled3 ,
                                             String AV43DynamicFiltersSelector3 ,
                                             String AV44UnidadeOrganizacional_Nome3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             int AV12TFTpUo_Codigo ,
                                             int AV13TFTpUo_Codigo_To ,
                                             String AV15TFEstado_UF_Sel ,
                                             String AV14TFEstado_UF ,
                                             int AV16TFUnidadeOrganizacional_Vinculada ,
                                             int AV17TFUnidadeOrganizacional_Vinculada_To ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Vinculada], [Estado_UF], [TpUo_Codigo], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV38UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV41UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV44UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV12TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV12TFTpUo_Codigo)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV13TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV13TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV14TFEstado_UF)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV15TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV16TFUnidadeOrganizacional_Vinculada) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] >= @AV16TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV17TFUnidadeOrganizacional_Vinculada_To) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] <= @AV17TFUnidadeOrganizacional_Vinculada_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [UnidadeOrganizacional_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NO3( IGxContext context ,
                                             String AV37DynamicFiltersSelector1 ,
                                             String AV38UnidadeOrganizacional_Nome1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41UnidadeOrganizacional_Nome2 ,
                                             bool AV42DynamicFiltersEnabled3 ,
                                             String AV43DynamicFiltersSelector3 ,
                                             String AV44UnidadeOrganizacional_Nome3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             int AV12TFTpUo_Codigo ,
                                             int AV13TFTpUo_Codigo_To ,
                                             String AV15TFEstado_UF_Sel ,
                                             String AV14TFEstado_UF ,
                                             int AV16TFUnidadeOrganizacional_Vinculada ,
                                             int AV17TFUnidadeOrganizacional_Vinculada_To ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Estado_UF], [UnidadeOrganizacional_Vinculada], [TpUo_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV38UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV41UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV44UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV12TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV12TFTpUo_Codigo)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV13TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV13TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV14TFEstado_UF)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV15TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV16TFUnidadeOrganizacional_Vinculada) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] >= @AV16TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV17TFUnidadeOrganizacional_Vinculada_To) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] <= @AV17TFUnidadeOrganizacional_Vinculada_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Estado_UF]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NO2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
               case 1 :
                     return conditional_P00NO3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NO2 ;
          prmP00NO2 = new Object[] {
          new Object[] {"@lV38UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV15TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV16TFUnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFUnidadeOrganizacional_Vinculada_To",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00NO3 ;
          prmP00NO3 = new Object[] {
          new Object[] {"@lV38UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV15TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV16TFUnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFUnidadeOrganizacional_Vinculada_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NO2,100,0,true,false )
             ,new CursorDef("P00NO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NO3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptgeral_unidadeorganizacionalfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptgeral_unidadeorganizacionalfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptgeral_unidadeorganizacionalfilterdata") )
          {
             return  ;
          }
          getpromptgeral_unidadeorganizacionalfilterdata worker = new getpromptgeral_unidadeorganizacionalfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
