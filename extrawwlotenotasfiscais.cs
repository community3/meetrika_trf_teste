/*
               File: ExtraWWLoteNotasFiscais
        Description:  Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:43.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class extrawwlotenotasfiscais : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public extrawwlotenotasfiscais( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public extrawwlotenotasfiscais( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_173 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_173_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_173_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16Lote_NFe1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0)));
               AV17Lote_DataNfe1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_DataNfe1", context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"));
               AV18Lote_DataNfe_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_DataNfe_To1", context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"));
               AV19Lote_NFeDataProtocolo1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_NFeDataProtocolo1", context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"));
               AV20Lote_NFeDataProtocolo_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Lote_NFeDataProtocolo_To1", context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"));
               AV21Lote_PrevPagamento1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_PrevPagamento1", context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"));
               AV22Lote_PrevPagamento_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_PrevPagamento_To1", context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"));
               AV23Lote_LiqData1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_LiqData1", context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"));
               AV24Lote_LiqData_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_LiqData_To1", context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"));
               AV25Lote_Numero1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Lote_Numero1", AV25Lote_Numero1);
               AV27DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
               AV28Lote_NFe2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0)));
               AV29Lote_DataNfe2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Lote_DataNfe2", context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"));
               AV30Lote_DataNfe_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Lote_DataNfe_To2", context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"));
               AV31Lote_NFeDataProtocolo2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Lote_NFeDataProtocolo2", context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"));
               AV32Lote_NFeDataProtocolo_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Lote_NFeDataProtocolo_To2", context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"));
               AV33Lote_PrevPagamento2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Lote_PrevPagamento2", context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"));
               AV34Lote_PrevPagamento_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_PrevPagamento_To2", context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"));
               AV35Lote_LiqData2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Lote_LiqData2", context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"));
               AV36Lote_LiqData_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_LiqData_To2", context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"));
               AV37Lote_Numero2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Lote_Numero2", AV37Lote_Numero2);
               AV39DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
               AV40Lote_NFe3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0)));
               AV41Lote_DataNfe3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Lote_DataNfe3", context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"));
               AV42Lote_DataNfe_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Lote_DataNfe_To3", context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"));
               AV43Lote_NFeDataProtocolo3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Lote_NFeDataProtocolo3", context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"));
               AV44Lote_NFeDataProtocolo_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Lote_NFeDataProtocolo_To3", context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"));
               AV45Lote_PrevPagamento3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Lote_PrevPagamento3", context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"));
               AV46Lote_PrevPagamento_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Lote_PrevPagamento_To3", context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"));
               AV47Lote_LiqData3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Lote_LiqData3", context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"));
               AV48Lote_LiqData_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Lote_LiqData_To3", context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"));
               AV49Lote_Numero3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Lote_Numero3", AV49Lote_Numero3);
               AV26DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
               AV38DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
               AV53Lote_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0)));
               AV150Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV51DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DynamicFiltersIgnoreFirst", AV51DynamicFiltersIgnoreFirst);
               AV50DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
               A596Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         A573Lote_QtdePF = 0;
         /* Using cursor H00H22 */
         pr_default.execute(0, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) && ( H00H22_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  H00H22_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*H00H22_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1058Lote_ValorOSs", StringUtil.LTrim( StringUtil.Str( A1058Lote_ValorOSs, 18, 5)));
            A573Lote_QtdePF = (decimal)(A573Lote_QtdePF+A574ContagemResultado_PFFinal);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      public override short ExecuteStartEvent( )
      {
         PAH22( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTH22( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299394432");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("extrawwlotenotasfiscais.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Lote_NFe1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE1", context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO1", context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO1", context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO_TO1", context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO1", context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO1", context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA1", context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA_TO1", context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO1", StringUtil.RTrim( AV25Lote_Numero1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV27DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Lote_NFe2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE2", context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO2", context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO2", context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO_TO2", context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO2", context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO2", context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA2", context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA_TO2", context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO2", StringUtil.RTrim( AV37Lote_Numero2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV39DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Lote_NFe3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE3", context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO3", context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO3", context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFEDATAPROTOCOLO_TO3", context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO3", context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO3", context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA3", context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_LIQDATA_TO3", context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO3", StringUtil.RTrim( AV49Lote_Numero3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV26DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV38DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_173", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_173), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV150Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV51DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV50DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOROSS", StringUtil.LTrim( StringUtil.NToC( A1058Lote_ValorOSs, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORGLOSAS", StringUtil.LTrim( StringUtil.NToC( A1057Lote_ValorGlosas, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEH22( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTH22( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("extrawwlotenotasfiscais.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ExtraWWLoteNotasFiscais" ;
      }

      public override String GetPgmdesc( )
      {
         return " Lote" ;
      }

      protected void WBH20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_H22( true) ;
         }
         else
         {
            wb_table1_2_H22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 187,'',false,'" + sGXsfl_173_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(187, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,187);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 188,'',false,'" + sGXsfl_173_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV38DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(188, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,188);\"");
         }
         wbLoad = true;
      }

      protected void STARTH22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Lote", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPH20( ) ;
      }

      protected void WSH22( )
      {
         STARTH22( ) ;
         EVTH22( ) ;
      }

      protected void EVTH22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11H22 */
                              E11H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12H22 */
                              E12H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13H22 */
                              E13H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14H22 */
                              E14H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15H22 */
                              E15H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16H22 */
                              E16H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17H22 */
                              E17H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18H22 */
                              E18H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19H22 */
                              E19H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20H22 */
                              E20H22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
                              AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
                              AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
                              AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
                              AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
                              AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
                              AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
                              AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
                              AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
                              AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
                              AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
                              AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
                              AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
                              AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
                              AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
                              AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
                              AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
                              AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
                              AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
                              AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
                              AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
                              AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
                              AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
                              AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
                              AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
                              AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
                              AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
                              AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
                              AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
                              AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
                              AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
                              AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
                              AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
                              AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
                              AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
                              AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_173_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_173_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_173_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1732( ) ;
                              A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLote_Codigo_Internalname), ",", "."));
                              A673Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", "."));
                              n673Lote_NFe = false;
                              A674Lote_DataNfe = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataNfe_Internalname), 0));
                              n674Lote_DataNfe = false;
                              A572Lote_Valor = context.localUtil.CToN( cgiGet( edtLote_Valor_Internalname), ",", ".");
                              A1001Lote_NFeDataProtocolo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_NFeDataProtocolo_Internalname), 0));
                              n1001Lote_NFeDataProtocolo = false;
                              A857Lote_PrevPagamento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_PrevPagamento_Internalname), 0));
                              n857Lote_PrevPagamento = false;
                              A676Lote_LiqData = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_LiqData_Internalname), 0));
                              n676Lote_LiqData = false;
                              A677Lote_LiqValor = context.localUtil.CToN( cgiGet( edtLote_LiqValor_Internalname), ",", ".");
                              n677Lote_LiqValor = false;
                              A562Lote_Numero = cgiGet( edtLote_Numero_Internalname);
                              A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
                              A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname), 0);
                              A573Lote_QtdePF = context.localUtil.CToN( cgiGet( edtLote_QtdePF_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21H22 */
                                    E21H22 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22H22 */
                                    E22H22 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE1"), ",", ".") != Convert.ToDecimal( AV16Lote_NFe1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE1"), 0) != AV17Lote_DataNfe1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO1"), 0) != AV18Lote_DataNfe_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO1"), 0) != AV19Lote_NFeDataProtocolo1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO1"), 0) != AV20Lote_NFeDataProtocolo_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO1"), 0) != AV21Lote_PrevPagamento1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO1"), 0) != AV22Lote_PrevPagamento_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA1"), 0) != AV23Lote_LiqData1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO1"), 0) != AV24Lote_LiqData_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO1"), AV25Lote_Numero1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV27DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE2"), ",", ".") != Convert.ToDecimal( AV28Lote_NFe2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE2"), 0) != AV29Lote_DataNfe2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO2"), 0) != AV30Lote_DataNfe_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO2"), 0) != AV31Lote_NFeDataProtocolo2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO2"), 0) != AV32Lote_NFeDataProtocolo_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO2"), 0) != AV33Lote_PrevPagamento2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO2"), 0) != AV34Lote_PrevPagamento_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA2"), 0) != AV35Lote_LiqData2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO2"), 0) != AV36Lote_LiqData_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO2"), AV37Lote_Numero2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV39DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE3"), ",", ".") != Convert.ToDecimal( AV40Lote_NFe3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE3"), 0) != AV41Lote_DataNfe3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO3"), 0) != AV42Lote_DataNfe_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO3"), 0) != AV43Lote_NFeDataProtocolo3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfedataprotocolo_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO3"), 0) != AV44Lote_NFeDataProtocolo_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO3"), 0) != AV45Lote_PrevPagamento3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO3"), 0) != AV46Lote_PrevPagamento_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA3"), 0) != AV47Lote_LiqData3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_liqdata_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO3"), 0) != AV48Lote_LiqData_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO3"), AV49Lote_Numero3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV26DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV38DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEH22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAH22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LOTE_NFE", "N�mero NFe", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_DATANFE", "Data NFe", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_NFEDATAPROTOCOLO", "Data protocolo", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_PREVPAGAMENTO", "Previs�o pagamento", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_LIQDATA", "Data pagamento", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_NUMERO", "Lote", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LOTE_NFE", "N�mero NFe", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_DATANFE", "Data NFe", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_NFEDATAPROTOCOLO", "Data protocolo", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_PREVPAGAMENTO", "Previs�o pagamento", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_LIQDATA", "Data pagamento", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_NUMERO", "Lote", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV27DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LOTE_NFE", "N�mero NFe", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_DATANFE", "Data NFe", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_NFEDATAPROTOCOLO", "Data protocolo", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_PREVPAGAMENTO", "Previs�o pagamento", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_LIQDATA", "Data pagamento", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_NUMERO", "Lote", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV39DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV39DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1732( ) ;
         while ( nGXsfl_173_idx <= nRC_GXsfl_173 )
         {
            sendrow_1732( ) ;
            nGXsfl_173_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_173_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_173_idx+1));
            sGXsfl_173_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_173_idx), 4, 0)), 4, "0");
            SubsflControlProps_1732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       String AV15DynamicFiltersSelector1 ,
                                       int AV16Lote_NFe1 ,
                                       DateTime AV17Lote_DataNfe1 ,
                                       DateTime AV18Lote_DataNfe_To1 ,
                                       DateTime AV19Lote_NFeDataProtocolo1 ,
                                       DateTime AV20Lote_NFeDataProtocolo_To1 ,
                                       DateTime AV21Lote_PrevPagamento1 ,
                                       DateTime AV22Lote_PrevPagamento_To1 ,
                                       DateTime AV23Lote_LiqData1 ,
                                       DateTime AV24Lote_LiqData_To1 ,
                                       String AV25Lote_Numero1 ,
                                       String AV27DynamicFiltersSelector2 ,
                                       int AV28Lote_NFe2 ,
                                       DateTime AV29Lote_DataNfe2 ,
                                       DateTime AV30Lote_DataNfe_To2 ,
                                       DateTime AV31Lote_NFeDataProtocolo2 ,
                                       DateTime AV32Lote_NFeDataProtocolo_To2 ,
                                       DateTime AV33Lote_PrevPagamento2 ,
                                       DateTime AV34Lote_PrevPagamento_To2 ,
                                       DateTime AV35Lote_LiqData2 ,
                                       DateTime AV36Lote_LiqData_To2 ,
                                       String AV37Lote_Numero2 ,
                                       String AV39DynamicFiltersSelector3 ,
                                       int AV40Lote_NFe3 ,
                                       DateTime AV41Lote_DataNfe3 ,
                                       DateTime AV42Lote_DataNfe_To3 ,
                                       DateTime AV43Lote_NFeDataProtocolo3 ,
                                       DateTime AV44Lote_NFeDataProtocolo_To3 ,
                                       DateTime AV45Lote_PrevPagamento3 ,
                                       DateTime AV46Lote_PrevPagamento_To3 ,
                                       DateTime AV47Lote_LiqData3 ,
                                       DateTime AV48Lote_LiqData_To3 ,
                                       String AV49Lote_Numero3 ,
                                       bool AV26DynamicFiltersEnabled2 ,
                                       bool AV38DynamicFiltersEnabled3 ,
                                       int AV53Lote_ContratadaCod ,
                                       String AV150Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV51DynamicFiltersIgnoreFirst ,
                                       bool AV50DynamicFiltersRemoving ,
                                       int A596Lote_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFH22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTE_NFE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATANFE", GetSecureSignedToken( "", A674Lote_DataNfe));
         GxWebStd.gx_hidden_field( context, "LOTE_DATANFE", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOR", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFEDATAPROTOCOLO", GetSecureSignedToken( "", A1001Lote_NFeDataProtocolo));
         GxWebStd.gx_hidden_field( context, "LOTE_NFEDATAPROTOCOLO", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_PREVPAGAMENTO", GetSecureSignedToken( "", A857Lote_PrevPagamento));
         GxWebStd.gx_hidden_field( context, "LOTE_PREVPAGAMENTO", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_LIQDATA", GetSecureSignedToken( "", A676Lote_LiqData));
         GxWebStd.gx_hidden_field( context, "LOTE_LIQDATA", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_LIQVALOR", GetSecureSignedToken( "", context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "LOTE_LIQVALOR", StringUtil.LTrim( StringUtil.NToC( A677Lote_LiqValor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "LOTE_NUMERO", StringUtil.RTrim( A562Lote_Numero));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "LOTE_NOME", StringUtil.RTrim( A563Lote_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTE_DATA", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_QTDEPF", GetSecureSignedToken( "", context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "LOTE_QTDEPF", StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV27DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV39DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV39DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV150Pgmname = "ExtraWWLoteNotasFiscais";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFH22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 173;
         /* Execute user event: E11H22 */
         E11H22 ();
         nGXsfl_173_idx = 1;
         sGXsfl_173_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_173_idx), 4, 0)), 4, "0");
         SubsflControlProps_1732( ) ;
         nGXsfl_173_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 ,
                                                 AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 ,
                                                 AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 ,
                                                 AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 ,
                                                 AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 ,
                                                 AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 ,
                                                 AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 ,
                                                 AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 ,
                                                 AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 ,
                                                 AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 ,
                                                 AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ,
                                                 AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 ,
                                                 AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 ,
                                                 AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 ,
                                                 AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 ,
                                                 AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 ,
                                                 AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 ,
                                                 AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 ,
                                                 AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 ,
                                                 AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 ,
                                                 AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 ,
                                                 AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 ,
                                                 AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ,
                                                 AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 ,
                                                 AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 ,
                                                 AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 ,
                                                 AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 ,
                                                 AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 ,
                                                 AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 ,
                                                 AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 ,
                                                 AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 ,
                                                 AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 ,
                                                 AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 ,
                                                 AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 ,
                                                 AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ,
                                                 AV16Lote_NFe1 ,
                                                 AV28Lote_NFe2 ,
                                                 AV40Lote_NFe3 ,
                                                 AV17Lote_DataNfe1 ,
                                                 AV29Lote_DataNfe2 ,
                                                 AV41Lote_DataNfe3 ,
                                                 A673Lote_NFe ,
                                                 A674Lote_DataNfe ,
                                                 A1001Lote_NFeDataProtocolo ,
                                                 A857Lote_PrevPagamento ,
                                                 A676Lote_LiqData ,
                                                 A562Lote_Numero ,
                                                 Gx_date ,
                                                 AV13OrderedBy ,
                                                 AV6WWPContext.gxTpr_Userehcontratante ,
                                                 A1231Lote_ContratadaCod ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1), 10, "%");
            lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2), 10, "%");
            lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3), 10, "%");
            /* Using cursor H00H27 */
            pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Userehcontratante, AV6WWPContext.gxTpr_Contratada_codigo, AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1, AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1, AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1, AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1, AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1, AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1, AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1, AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1, AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1, lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1, AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2, AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2, AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2, AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2, AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2, AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2, AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2, AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2, AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2, lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2, AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3, AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3, AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3, AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3, AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3, AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3, AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3, AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3, AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3, lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3, Gx_date, Gx_date, Gx_date, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_173_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A564Lote_Data = H00H27_A564Lote_Data[0];
               A563Lote_Nome = H00H27_A563Lote_Nome[0];
               A562Lote_Numero = H00H27_A562Lote_Numero[0];
               A677Lote_LiqValor = H00H27_A677Lote_LiqValor[0];
               n677Lote_LiqValor = H00H27_n677Lote_LiqValor[0];
               A676Lote_LiqData = H00H27_A676Lote_LiqData[0];
               n676Lote_LiqData = H00H27_n676Lote_LiqData[0];
               A857Lote_PrevPagamento = H00H27_A857Lote_PrevPagamento[0];
               n857Lote_PrevPagamento = H00H27_n857Lote_PrevPagamento[0];
               A1001Lote_NFeDataProtocolo = H00H27_A1001Lote_NFeDataProtocolo[0];
               n1001Lote_NFeDataProtocolo = H00H27_n1001Lote_NFeDataProtocolo[0];
               A674Lote_DataNfe = H00H27_A674Lote_DataNfe[0];
               n674Lote_DataNfe = H00H27_n674Lote_DataNfe[0];
               A673Lote_NFe = H00H27_A673Lote_NFe[0];
               n673Lote_NFe = H00H27_n673Lote_NFe[0];
               A596Lote_Codigo = H00H27_A596Lote_Codigo[0];
               A1231Lote_ContratadaCod = H00H27_A1231Lote_ContratadaCod[0];
               n1231Lote_ContratadaCod = H00H27_n1231Lote_ContratadaCod[0];
               A1057Lote_ValorGlosas = H00H27_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00H27_n1057Lote_ValorGlosas[0];
               A1231Lote_ContratadaCod = H00H27_A1231Lote_ContratadaCod[0];
               n1231Lote_ContratadaCod = H00H27_n1231Lote_ContratadaCod[0];
               A1057Lote_ValorGlosas = H00H27_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00H27_n1057Lote_ValorGlosas[0];
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               /* Execute user event: E22H22 */
               E22H22 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 173;
            WBH20( ) ;
         }
         nGXsfl_173_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 ,
                                              AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 ,
                                              AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 ,
                                              AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 ,
                                              AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 ,
                                              AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 ,
                                              AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 ,
                                              AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 ,
                                              AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 ,
                                              AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 ,
                                              AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ,
                                              AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 ,
                                              AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 ,
                                              AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 ,
                                              AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 ,
                                              AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 ,
                                              AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 ,
                                              AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 ,
                                              AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 ,
                                              AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 ,
                                              AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 ,
                                              AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 ,
                                              AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ,
                                              AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 ,
                                              AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 ,
                                              AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 ,
                                              AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 ,
                                              AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 ,
                                              AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 ,
                                              AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 ,
                                              AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 ,
                                              AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 ,
                                              AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 ,
                                              AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 ,
                                              AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ,
                                              AV16Lote_NFe1 ,
                                              AV28Lote_NFe2 ,
                                              AV40Lote_NFe3 ,
                                              AV17Lote_DataNfe1 ,
                                              AV29Lote_DataNfe2 ,
                                              AV41Lote_DataNfe3 ,
                                              A673Lote_NFe ,
                                              A674Lote_DataNfe ,
                                              A1001Lote_NFeDataProtocolo ,
                                              A857Lote_PrevPagamento ,
                                              A676Lote_LiqData ,
                                              A562Lote_Numero ,
                                              Gx_date ,
                                              AV13OrderedBy ,
                                              AV6WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV6WWPContext.gxTpr_Contratada_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1), 10, "%");
         lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2), 10, "%");
         lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3), 10, "%");
         /* Using cursor H00H212 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Userehcontratante, AV6WWPContext.gxTpr_Contratada_codigo, AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1, AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1, AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1, AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1, AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1, AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1, AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1, AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1, AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1, lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1, AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2, AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2, AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2, AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2, AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2, AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2, AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2, AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2, AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2, lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2, AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3, AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3, AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3, AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3, AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3, AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3, AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3, AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3, AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3, lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3, Gx_date, Gx_date, Gx_date});
         GRID_nRecordCount = H00H212_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPH20( )
      {
         /* Before Start, stand alone formulas. */
         AV150Pgmname = "ExtraWWLoteNotasFiscais";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21H22 */
         E21H22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_CONTRATADACOD");
               GX_FocusControl = edtavLote_contratadacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53Lote_ContratadaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0)));
            }
            else
            {
               AV53Lote_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE1");
               GX_FocusControl = edtavLote_nfe1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16Lote_NFe1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0)));
            }
            else
            {
               AV16Lote_NFe1 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe1"}), 1, "vLOTE_DATANFE1");
               GX_FocusControl = edtavLote_datanfe1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17Lote_DataNfe1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_DataNfe1", context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"));
            }
            else
            {
               AV17Lote_DataNfe1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_DataNfe1", context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To1"}), 1, "vLOTE_DATANFE_TO1");
               GX_FocusControl = edtavLote_datanfe_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18Lote_DataNfe_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_DataNfe_To1", context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"));
            }
            else
            {
               AV18Lote_DataNfe_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_DataNfe_To1", context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo1"}), 1, "vLOTE_NFEDATAPROTOCOLO1");
               GX_FocusControl = edtavLote_nfedataprotocolo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19Lote_NFeDataProtocolo1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_NFeDataProtocolo1", context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"));
            }
            else
            {
               AV19Lote_NFeDataProtocolo1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_NFeDataProtocolo1", context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo_To1"}), 1, "vLOTE_NFEDATAPROTOCOLO_TO1");
               GX_FocusControl = edtavLote_nfedataprotocolo_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Lote_NFeDataProtocolo_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Lote_NFeDataProtocolo_To1", context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"));
            }
            else
            {
               AV20Lote_NFeDataProtocolo_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Lote_NFeDataProtocolo_To1", context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento1"}), 1, "vLOTE_PREVPAGAMENTO1");
               GX_FocusControl = edtavLote_prevpagamento1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21Lote_PrevPagamento1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_PrevPagamento1", context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"));
            }
            else
            {
               AV21Lote_PrevPagamento1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_PrevPagamento1", context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To1"}), 1, "vLOTE_PREVPAGAMENTO_TO1");
               GX_FocusControl = edtavLote_prevpagamento_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22Lote_PrevPagamento_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_PrevPagamento_To1", context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"));
            }
            else
            {
               AV22Lote_PrevPagamento_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_PrevPagamento_To1", context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data1"}), 1, "vLOTE_LIQDATA1");
               GX_FocusControl = edtavLote_liqdata1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23Lote_LiqData1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_LiqData1", context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"));
            }
            else
            {
               AV23Lote_LiqData1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_LiqData1", context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data_To1"}), 1, "vLOTE_LIQDATA_TO1");
               GX_FocusControl = edtavLote_liqdata_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24Lote_LiqData_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_LiqData_To1", context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"));
            }
            else
            {
               AV24Lote_LiqData_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_LiqData_To1", context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"));
            }
            AV25Lote_Numero1 = cgiGet( edtavLote_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Lote_Numero1", AV25Lote_Numero1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV27DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE2");
               GX_FocusControl = edtavLote_nfe2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28Lote_NFe2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0)));
            }
            else
            {
               AV28Lote_NFe2 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe2"}), 1, "vLOTE_DATANFE2");
               GX_FocusControl = edtavLote_datanfe2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29Lote_DataNfe2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Lote_DataNfe2", context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"));
            }
            else
            {
               AV29Lote_DataNfe2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Lote_DataNfe2", context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To2"}), 1, "vLOTE_DATANFE_TO2");
               GX_FocusControl = edtavLote_datanfe_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30Lote_DataNfe_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Lote_DataNfe_To2", context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"));
            }
            else
            {
               AV30Lote_DataNfe_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Lote_DataNfe_To2", context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo2"}), 1, "vLOTE_NFEDATAPROTOCOLO2");
               GX_FocusControl = edtavLote_nfedataprotocolo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31Lote_NFeDataProtocolo2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Lote_NFeDataProtocolo2", context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"));
            }
            else
            {
               AV31Lote_NFeDataProtocolo2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Lote_NFeDataProtocolo2", context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo_To2"}), 1, "vLOTE_NFEDATAPROTOCOLO_TO2");
               GX_FocusControl = edtavLote_nfedataprotocolo_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32Lote_NFeDataProtocolo_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Lote_NFeDataProtocolo_To2", context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"));
            }
            else
            {
               AV32Lote_NFeDataProtocolo_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Lote_NFeDataProtocolo_To2", context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento2"}), 1, "vLOTE_PREVPAGAMENTO2");
               GX_FocusControl = edtavLote_prevpagamento2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Lote_PrevPagamento2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Lote_PrevPagamento2", context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"));
            }
            else
            {
               AV33Lote_PrevPagamento2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Lote_PrevPagamento2", context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To2"}), 1, "vLOTE_PREVPAGAMENTO_TO2");
               GX_FocusControl = edtavLote_prevpagamento_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Lote_PrevPagamento_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_PrevPagamento_To2", context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"));
            }
            else
            {
               AV34Lote_PrevPagamento_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_PrevPagamento_To2", context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data2"}), 1, "vLOTE_LIQDATA2");
               GX_FocusControl = edtavLote_liqdata2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Lote_LiqData2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Lote_LiqData2", context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"));
            }
            else
            {
               AV35Lote_LiqData2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Lote_LiqData2", context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data_To2"}), 1, "vLOTE_LIQDATA_TO2");
               GX_FocusControl = edtavLote_liqdata_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Lote_LiqData_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_LiqData_To2", context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"));
            }
            else
            {
               AV36Lote_LiqData_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_LiqData_To2", context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"));
            }
            AV37Lote_Numero2 = cgiGet( edtavLote_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Lote_Numero2", AV37Lote_Numero2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV39DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE3");
               GX_FocusControl = edtavLote_nfe3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40Lote_NFe3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0)));
            }
            else
            {
               AV40Lote_NFe3 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe3"}), 1, "vLOTE_DATANFE3");
               GX_FocusControl = edtavLote_datanfe3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41Lote_DataNfe3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Lote_DataNfe3", context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"));
            }
            else
            {
               AV41Lote_DataNfe3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Lote_DataNfe3", context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To3"}), 1, "vLOTE_DATANFE_TO3");
               GX_FocusControl = edtavLote_datanfe_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42Lote_DataNfe_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Lote_DataNfe_To3", context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"));
            }
            else
            {
               AV42Lote_DataNfe_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Lote_DataNfe_To3", context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo3"}), 1, "vLOTE_NFEDATAPROTOCOLO3");
               GX_FocusControl = edtavLote_nfedataprotocolo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43Lote_NFeDataProtocolo3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Lote_NFeDataProtocolo3", context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"));
            }
            else
            {
               AV43Lote_NFeDataProtocolo3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Lote_NFeDataProtocolo3", context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_NFe Data Protocolo_To3"}), 1, "vLOTE_NFEDATAPROTOCOLO_TO3");
               GX_FocusControl = edtavLote_nfedataprotocolo_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44Lote_NFeDataProtocolo_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Lote_NFeDataProtocolo_To3", context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"));
            }
            else
            {
               AV44Lote_NFeDataProtocolo_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Lote_NFeDataProtocolo_To3", context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento3"}), 1, "vLOTE_PREVPAGAMENTO3");
               GX_FocusControl = edtavLote_prevpagamento3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45Lote_PrevPagamento3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Lote_PrevPagamento3", context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"));
            }
            else
            {
               AV45Lote_PrevPagamento3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Lote_PrevPagamento3", context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To3"}), 1, "vLOTE_PREVPAGAMENTO_TO3");
               GX_FocusControl = edtavLote_prevpagamento_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46Lote_PrevPagamento_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Lote_PrevPagamento_To3", context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"));
            }
            else
            {
               AV46Lote_PrevPagamento_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Lote_PrevPagamento_To3", context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data3"}), 1, "vLOTE_LIQDATA3");
               GX_FocusControl = edtavLote_liqdata3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47Lote_LiqData3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Lote_LiqData3", context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"));
            }
            else
            {
               AV47Lote_LiqData3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Lote_LiqData3", context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Liq Data_To3"}), 1, "vLOTE_LIQDATA_TO3");
               GX_FocusControl = edtavLote_liqdata_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48Lote_LiqData_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Lote_LiqData_To3", context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"));
            }
            else
            {
               AV48Lote_LiqData_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Lote_LiqData_To3", context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"));
            }
            AV49Lote_Numero3 = cgiGet( edtavLote_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Lote_Numero3", AV49Lote_Numero3);
            AV26DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
            AV38DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_173 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_173"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE1"), ",", ".") != Convert.ToDecimal( AV16Lote_NFe1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE1"), 0) != AV17Lote_DataNfe1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO1"), 0) != AV18Lote_DataNfe_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO1"), 0) != AV19Lote_NFeDataProtocolo1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO1"), 0) != AV20Lote_NFeDataProtocolo_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO1"), 0) != AV21Lote_PrevPagamento1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO1"), 0) != AV22Lote_PrevPagamento_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA1"), 0) != AV23Lote_LiqData1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO1"), 0) != AV24Lote_LiqData_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO1"), AV25Lote_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV27DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE2"), ",", ".") != Convert.ToDecimal( AV28Lote_NFe2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE2"), 0) != AV29Lote_DataNfe2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO2"), 0) != AV30Lote_DataNfe_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO2"), 0) != AV31Lote_NFeDataProtocolo2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO2"), 0) != AV32Lote_NFeDataProtocolo_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO2"), 0) != AV33Lote_PrevPagamento2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO2"), 0) != AV34Lote_PrevPagamento_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA2"), 0) != AV35Lote_LiqData2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO2"), 0) != AV36Lote_LiqData_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO2"), AV37Lote_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV39DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE3"), ",", ".") != Convert.ToDecimal( AV40Lote_NFe3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE3"), 0) != AV41Lote_DataNfe3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO3"), 0) != AV42Lote_DataNfe_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO3"), 0) != AV43Lote_NFeDataProtocolo3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_NFEDATAPROTOCOLO_TO3"), 0) != AV44Lote_NFeDataProtocolo_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO3"), 0) != AV45Lote_PrevPagamento3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO3"), 0) != AV46Lote_PrevPagamento_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA3"), 0) != AV47Lote_LiqData3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_LIQDATA_TO3"), 0) != AV48Lote_LiqData_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO3"), AV49Lote_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV26DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV38DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21H22 */
         E21H22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21H22( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector2 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39DynamicFiltersSelector3 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Notas Fiscais";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Recentes", 0);
         cmbavOrderedby.addItem("2", "Lote", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E11H22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod = AV53Lote_ContratadaCod;
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 = AV16Lote_NFe1;
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = AV17Lote_DataNfe1;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = AV18Lote_DataNfe_To1;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = AV19Lote_NFeDataProtocolo1;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = AV20Lote_NFeDataProtocolo_To1;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = AV21Lote_PrevPagamento1;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = AV22Lote_PrevPagamento_To1;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = AV23Lote_LiqData1;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = AV24Lote_LiqData_To1;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = AV25Lote_Numero1;
         AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 = AV28Lote_NFe2;
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = AV29Lote_DataNfe2;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = AV30Lote_DataNfe_To2;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = AV31Lote_NFeDataProtocolo2;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = AV32Lote_NFeDataProtocolo_To2;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = AV33Lote_PrevPagamento2;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = AV34Lote_PrevPagamento_To2;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = AV35Lote_LiqData2;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = AV36Lote_LiqData_To2;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = AV37Lote_Numero2;
         AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 = AV40Lote_NFe3;
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = AV41Lote_DataNfe3;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = AV42Lote_DataNfe_To3;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = AV43Lote_NFeDataProtocolo3;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = AV44Lote_NFeDataProtocolo_To3;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = AV45Lote_PrevPagamento3;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = AV46Lote_PrevPagamento_To3;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = AV47Lote_LiqData3;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = AV48Lote_LiqData_To3;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = AV49Lote_Numero3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      private void E22H22( )
      {
         /* Grid_Load Routine */
         edtLote_Nome_Link = formatLink("viewlote.aspx") + "?" + UrlEncode("" +A596Lote_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 173;
         }
         sendrow_1732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_173_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(173, GridRow);
         }
      }

      protected void E16H22( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV26DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12H22( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV50DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         AV51DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DynamicFiltersIgnoreFirst", AV51DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV50DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         AV51DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DynamicFiltersIgnoreFirst", AV51DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV39DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E17H22( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18H22( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV38DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13H22( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV50DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV50DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV39DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19H22( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14H22( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV50DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         AV38DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV50DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DynamicFiltersRemoving", AV50DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV15DynamicFiltersSelector1, AV16Lote_NFe1, AV17Lote_DataNfe1, AV18Lote_DataNfe_To1, AV19Lote_NFeDataProtocolo1, AV20Lote_NFeDataProtocolo_To1, AV21Lote_PrevPagamento1, AV22Lote_PrevPagamento_To1, AV23Lote_LiqData1, AV24Lote_LiqData_To1, AV25Lote_Numero1, AV27DynamicFiltersSelector2, AV28Lote_NFe2, AV29Lote_DataNfe2, AV30Lote_DataNfe_To2, AV31Lote_NFeDataProtocolo2, AV32Lote_NFeDataProtocolo_To2, AV33Lote_PrevPagamento2, AV34Lote_PrevPagamento_To2, AV35Lote_LiqData2, AV36Lote_LiqData_To2, AV37Lote_Numero2, AV39DynamicFiltersSelector3, AV40Lote_NFe3, AV41Lote_DataNfe3, AV42Lote_DataNfe_To3, AV43Lote_NFeDataProtocolo3, AV44Lote_NFeDataProtocolo_To3, AV45Lote_PrevPagamento3, AV46Lote_PrevPagamento_To3, AV47Lote_LiqData3, AV48Lote_LiqData_To3, AV49Lote_Numero3, AV26DynamicFiltersEnabled2, AV38DynamicFiltersEnabled3, AV53Lote_ContratadaCod, AV150Pgmname, AV10GridState, AV51DynamicFiltersIgnoreFirst, AV50DynamicFiltersRemoving, A596Lote_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV39DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20H22( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15H22( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV39DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavLote_nfe1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe1_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe1_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento1_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_liqdata1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata1_Visible), 5, 0)));
         edtavLote_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFEDATAPROTOCOLO") == 0 )
         {
            tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_LIQDATA") == 0 )
         {
            tblTablemergeddynamicfilterslote_liqdata1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavLote_nfe2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe2_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe2_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento2_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_liqdata2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata2_Visible), 5, 0)));
         edtavLote_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFEDATAPROTOCOLO") == 0 )
         {
            tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_LIQDATA") == 0 )
         {
            tblTablemergeddynamicfilterslote_liqdata2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavLote_nfe3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe3_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe3_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento3_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_liqdata3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata3_Visible), 5, 0)));
         edtavLote_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFEDATAPROTOCOLO") == 0 )
         {
            tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_LIQDATA") == 0 )
         {
            tblTablemergeddynamicfilterslote_liqdata3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_liqdata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_liqdata3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV26DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         AV27DynamicFiltersSelector2 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         AV28Lote_NFe2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
         AV39DynamicFiltersSelector3 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
         AV40Lote_NFe3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV53Lote_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0)));
         AV15DynamicFiltersSelector1 = "LOTE_NFE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Lote_NFe1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV52Session.Get(AV150Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV150Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV52Session.Get(AV150Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S212( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV151GXV1 = 1;
         while ( AV151GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV151GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "LOTE_CONTRATADACOD") == 0 )
            {
               AV53Lote_ContratadaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0)));
            }
            AV151GXV1 = (int)(AV151GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFE") == 0 )
            {
               AV16Lote_NFe1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
            {
               AV17Lote_DataNfe1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_DataNfe1", context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"));
               AV18Lote_DataNfe_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_DataNfe_To1", context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFEDATAPROTOCOLO") == 0 )
            {
               AV19Lote_NFeDataProtocolo1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_NFeDataProtocolo1", context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"));
               AV20Lote_NFeDataProtocolo_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Lote_NFeDataProtocolo_To1", context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 )
            {
               AV21Lote_PrevPagamento1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_PrevPagamento1", context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"));
               AV22Lote_PrevPagamento_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_PrevPagamento_To1", context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_LIQDATA") == 0 )
            {
               AV23Lote_LiqData1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_LiqData1", context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"));
               AV24Lote_LiqData_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_LiqData_To1", context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
            {
               AV25Lote_Numero1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Lote_Numero1", AV25Lote_Numero1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV26DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV27DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFE") == 0 )
               {
                  AV28Lote_NFe2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
               {
                  AV29Lote_DataNfe2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Lote_DataNfe2", context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"));
                  AV30Lote_DataNfe_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Lote_DataNfe_To2", context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFEDATAPROTOCOLO") == 0 )
               {
                  AV31Lote_NFeDataProtocolo2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Lote_NFeDataProtocolo2", context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"));
                  AV32Lote_NFeDataProtocolo_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Lote_NFeDataProtocolo_To2", context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 )
               {
                  AV33Lote_PrevPagamento2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Lote_PrevPagamento2", context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"));
                  AV34Lote_PrevPagamento_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_PrevPagamento_To2", context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_LIQDATA") == 0 )
               {
                  AV35Lote_LiqData2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Lote_LiqData2", context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"));
                  AV36Lote_LiqData_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_LiqData_To2", context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
               {
                  AV37Lote_Numero2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Lote_Numero2", AV37Lote_Numero2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV38DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersEnabled3", AV38DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersSelector3", AV39DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFE") == 0 )
                  {
                     AV40Lote_NFe3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
                  {
                     AV41Lote_DataNfe3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Lote_DataNfe3", context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"));
                     AV42Lote_DataNfe_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Lote_DataNfe_To3", context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFEDATAPROTOCOLO") == 0 )
                  {
                     AV43Lote_NFeDataProtocolo3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Lote_NFeDataProtocolo3", context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"));
                     AV44Lote_NFeDataProtocolo_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Lote_NFeDataProtocolo_To3", context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 )
                  {
                     AV45Lote_PrevPagamento3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Lote_PrevPagamento3", context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"));
                     AV46Lote_PrevPagamento_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Lote_PrevPagamento_To3", context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_LIQDATA") == 0 )
                  {
                     AV47Lote_LiqData3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Lote_LiqData3", context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"));
                     AV48Lote_LiqData_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Lote_LiqData_To3", context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
                  {
                     AV49Lote_Numero3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Lote_Numero3", AV49Lote_Numero3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV50DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV52Session.Get(AV150Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV53Lote_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV53Lote_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV150Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV51DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFE") == 0 ) && ! (0==AV16Lote_NFe1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV16Lote_NFe1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV17Lote_DataNfe1) && (DateTime.MinValue==AV18Lote_DataNfe_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV17Lote_DataNfe1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV18Lote_DataNfe_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ! ( (DateTime.MinValue==AV19Lote_NFeDataProtocolo1) && (DateTime.MinValue==AV20Lote_NFeDataProtocolo_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV19Lote_NFeDataProtocolo1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV20Lote_NFeDataProtocolo_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV21Lote_PrevPagamento1) && (DateTime.MinValue==AV22Lote_PrevPagamento_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV21Lote_PrevPagamento1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV22Lote_PrevPagamento_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_LIQDATA") == 0 ) && ! ( (DateTime.MinValue==AV23Lote_LiqData1) && (DateTime.MinValue==AV24Lote_LiqData_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV23Lote_LiqData1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV24Lote_LiqData_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Lote_Numero1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Lote_Numero1;
            }
            if ( AV50DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFE") == 0 ) && ! (0==AV28Lote_NFe2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV28Lote_NFe2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV29Lote_DataNfe2) && (DateTime.MinValue==AV30Lote_DataNfe_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV29Lote_DataNfe2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV30Lote_DataNfe_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ! ( (DateTime.MinValue==AV31Lote_NFeDataProtocolo2) && (DateTime.MinValue==AV32Lote_NFeDataProtocolo_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV31Lote_NFeDataProtocolo2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV32Lote_NFeDataProtocolo_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV33Lote_PrevPagamento2) && (DateTime.MinValue==AV34Lote_PrevPagamento_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV33Lote_PrevPagamento2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV34Lote_PrevPagamento_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_LIQDATA") == 0 ) && ! ( (DateTime.MinValue==AV35Lote_LiqData2) && (DateTime.MinValue==AV36Lote_LiqData_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV35Lote_LiqData2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV36Lote_LiqData_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Lote_Numero2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37Lote_Numero2;
            }
            if ( AV50DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV38DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV39DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFE") == 0 ) && ! (0==AV40Lote_NFe3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV40Lote_NFe3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV41Lote_DataNfe3) && (DateTime.MinValue==AV42Lote_DataNfe_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV41Lote_DataNfe3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV42Lote_DataNfe_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ! ( (DateTime.MinValue==AV43Lote_NFeDataProtocolo3) && (DateTime.MinValue==AV44Lote_NFeDataProtocolo_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV43Lote_NFeDataProtocolo3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV44Lote_NFeDataProtocolo_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV45Lote_PrevPagamento3) && (DateTime.MinValue==AV46Lote_PrevPagamento_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV45Lote_PrevPagamento3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV46Lote_PrevPagamento_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_LIQDATA") == 0 ) && ! ( (DateTime.MinValue==AV47Lote_LiqData3) && (DateTime.MinValue==AV48Lote_LiqData_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV47Lote_LiqData3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV48Lote_LiqData_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV49Lote_Numero3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV49Lote_Numero3;
            }
            if ( AV50DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV150Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Lote";
         AV52Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_H22( true) ;
         }
         else
         {
            wb_table2_8_H22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_170_H22( true) ;
         }
         else
         {
            wb_table3_170_H22( false) ;
         }
         return  ;
      }

      protected void wb_table3_170_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H22e( true) ;
         }
         else
         {
            wb_table1_2_H22e( false) ;
         }
      }

      protected void wb_table3_170_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"173\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Lote_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "NFe") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Protocolo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prev. Pgto.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Liquidada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Lote") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A677Lote_LiqValor, 18, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A562Lote_Numero));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A563Lote_Nome));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtLote_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 173 )
         {
            wbEnd = 0;
            nRC_GXsfl_173 = (short)(nGXsfl_173_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_170_H22e( true) ;
         }
         else
         {
            wb_table3_170_H22e( false) ;
         }
      }

      protected void wb_table2_8_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLotetitle_Internalname, "Notas Fiscais", "", "", lblLotetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_13_H22( true) ;
         }
         else
         {
            wb_table4_13_H22( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_173_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_H22( true) ;
         }
         else
         {
            wb_table5_21_H22( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_H22e( true) ;
         }
         else
         {
            wb_table2_8_H22e( false) ;
         }
      }

      protected void wb_table5_21_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_contratadacod_Internalname, "", "", "", lblFiltertextlote_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_contratadacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53Lote_ContratadaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53Lote_ContratadaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_contratadacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_30_H22( true) ;
         }
         else
         {
            wb_table6_30_H22( false) ;
         }
         return  ;
      }

      protected void wb_table6_30_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_search_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(173), 3, 0)+","+"null"+");", "Procurar", bttBtn_search_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_H22e( true) ;
         }
         else
         {
            wb_table5_21_H22e( false) ;
         }
      }

      protected void wb_table6_30_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_173_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Lote_NFe1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16Lote_NFe1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe1_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            wb_table7_40_H22( true) ;
         }
         else
         {
            wb_table7_40_H22( false) ;
         }
         return  ;
      }

      protected void wb_table7_40_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table8_48_H22( true) ;
         }
         else
         {
            wb_table8_48_H22( false) ;
         }
         return  ;
      }

      protected void wb_table8_48_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_56_H22( true) ;
         }
         else
         {
            wb_table9_56_H22( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table10_64_H22( true) ;
         }
         else
         {
            wb_table10_64_H22( false) ;
         }
         return  ;
      }

      protected void wb_table10_64_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero1_Internalname, StringUtil.RTrim( AV25Lote_Numero1), StringUtil.RTrim( context.localUtil.Format( AV25Lote_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_173_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Lote_NFe2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28Lote_NFe2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe2_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            wb_table11_85_H22( true) ;
         }
         else
         {
            wb_table11_85_H22( false) ;
         }
         return  ;
      }

      protected void wb_table11_85_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table12_93_H22( true) ;
         }
         else
         {
            wb_table12_93_H22( false) ;
         }
         return  ;
      }

      protected void wb_table12_93_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table13_101_H22( true) ;
         }
         else
         {
            wb_table13_101_H22( false) ;
         }
         return  ;
      }

      protected void wb_table13_101_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table14_109_H22( true) ;
         }
         else
         {
            wb_table14_109_H22( false) ;
         }
         return  ;
      }

      protected void wb_table14_109_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero2_Internalname, StringUtil.RTrim( AV37Lote_Numero2), StringUtil.RTrim( context.localUtil.Format( AV37Lote_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_173_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV39DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", "", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV39DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Lote_NFe3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40Lote_NFe3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe3_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            wb_table15_130_H22( true) ;
         }
         else
         {
            wb_table15_130_H22( false) ;
         }
         return  ;
      }

      protected void wb_table15_130_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table16_138_H22( true) ;
         }
         else
         {
            wb_table16_138_H22( false) ;
         }
         return  ;
      }

      protected void wb_table16_138_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table17_146_H22( true) ;
         }
         else
         {
            wb_table17_146_H22( false) ;
         }
         return  ;
      }

      protected void wb_table17_146_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table18_154_H22( true) ;
         }
         else
         {
            wb_table18_154_H22( false) ;
         }
         return  ;
      }

      protected void wb_table18_154_H22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_173_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero3_Internalname, StringUtil.RTrim( AV49Lote_Numero3), StringUtil.RTrim( context.localUtil.Format( AV49Lote_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,162);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_H22e( true) ;
         }
         else
         {
            wb_table6_30_H22e( false) ;
         }
      }

      protected void wb_table18_154_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_liqdata3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_liqdata3_Internalname, tblTablemergeddynamicfilterslote_liqdata3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata3_Internalname, context.localUtil.Format(AV47Lote_LiqData3, "99/99/99"), context.localUtil.Format( AV47Lote_LiqData3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,157);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_liqdata_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_liqdata_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata_to3_Internalname, context.localUtil.Format(AV48Lote_LiqData_To3, "99/99/99"), context.localUtil.Format( AV48Lote_LiqData_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,161);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_154_H22e( true) ;
         }
         else
         {
            wb_table18_154_H22e( false) ;
         }
      }

      protected void wb_table17_146_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento3_Internalname, context.localUtil.Format(AV45Lote_PrevPagamento3, "99/99/99"), context.localUtil.Format( AV45Lote_PrevPagamento3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to3_Internalname, context.localUtil.Format(AV46Lote_PrevPagamento_To3, "99/99/99"), context.localUtil.Format( AV46Lote_PrevPagamento_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_146_H22e( true) ;
         }
         else
         {
            wb_table17_146_H22e( false) ;
         }
      }

      protected void wb_table16_138_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname, tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo3_Internalname, context.localUtil.Format(AV43Lote_NFeDataProtocolo3, "99/99/99"), context.localUtil.Format( AV43Lote_NFeDataProtocolo3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo_to3_Internalname, context.localUtil.Format(AV44Lote_NFeDataProtocolo_To3, "99/99/99"), context.localUtil.Format( AV44Lote_NFeDataProtocolo_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_138_H22e( true) ;
         }
         else
         {
            wb_table16_138_H22e( false) ;
         }
      }

      protected void wb_table15_130_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe3_Internalname, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe3_Internalname, context.localUtil.Format(AV41Lote_DataNfe3, "99/99/99"), context.localUtil.Format( AV41Lote_DataNfe3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to3_Internalname, context.localUtil.Format(AV42Lote_DataNfe_To3, "99/99/99"), context.localUtil.Format( AV42Lote_DataNfe_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_130_H22e( true) ;
         }
         else
         {
            wb_table15_130_H22e( false) ;
         }
      }

      protected void wb_table14_109_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_liqdata2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_liqdata2_Internalname, tblTablemergeddynamicfilterslote_liqdata2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata2_Internalname, context.localUtil.Format(AV35Lote_LiqData2, "99/99/99"), context.localUtil.Format( AV35Lote_LiqData2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_liqdata_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_liqdata_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata_to2_Internalname, context.localUtil.Format(AV36Lote_LiqData_To2, "99/99/99"), context.localUtil.Format( AV36Lote_LiqData_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_109_H22e( true) ;
         }
         else
         {
            wb_table14_109_H22e( false) ;
         }
      }

      protected void wb_table13_101_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento2_Internalname, context.localUtil.Format(AV33Lote_PrevPagamento2, "99/99/99"), context.localUtil.Format( AV33Lote_PrevPagamento2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to2_Internalname, context.localUtil.Format(AV34Lote_PrevPagamento_To2, "99/99/99"), context.localUtil.Format( AV34Lote_PrevPagamento_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_101_H22e( true) ;
         }
         else
         {
            wb_table13_101_H22e( false) ;
         }
      }

      protected void wb_table12_93_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname, tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo2_Internalname, context.localUtil.Format(AV31Lote_NFeDataProtocolo2, "99/99/99"), context.localUtil.Format( AV31Lote_NFeDataProtocolo2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo_to2_Internalname, context.localUtil.Format(AV32Lote_NFeDataProtocolo_To2, "99/99/99"), context.localUtil.Format( AV32Lote_NFeDataProtocolo_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_93_H22e( true) ;
         }
         else
         {
            wb_table12_93_H22e( false) ;
         }
      }

      protected void wb_table11_85_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe2_Internalname, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe2_Internalname, context.localUtil.Format(AV29Lote_DataNfe2, "99/99/99"), context.localUtil.Format( AV29Lote_DataNfe2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to2_Internalname, context.localUtil.Format(AV30Lote_DataNfe_To2, "99/99/99"), context.localUtil.Format( AV30Lote_DataNfe_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_85_H22e( true) ;
         }
         else
         {
            wb_table11_85_H22e( false) ;
         }
      }

      protected void wb_table10_64_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_liqdata1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_liqdata1_Internalname, tblTablemergeddynamicfilterslote_liqdata1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata1_Internalname, context.localUtil.Format(AV23Lote_LiqData1, "99/99/99"), context.localUtil.Format( AV23Lote_LiqData1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_liqdata_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_liqdata_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata_to1_Internalname, context.localUtil.Format(AV24Lote_LiqData_To1, "99/99/99"), context.localUtil.Format( AV24Lote_LiqData_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_64_H22e( true) ;
         }
         else
         {
            wb_table10_64_H22e( false) ;
         }
      }

      protected void wb_table9_56_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento1_Internalname, context.localUtil.Format(AV21Lote_PrevPagamento1, "99/99/99"), context.localUtil.Format( AV21Lote_PrevPagamento1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to1_Internalname, context.localUtil.Format(AV22Lote_PrevPagamento_To1, "99/99/99"), context.localUtil.Format( AV22Lote_PrevPagamento_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_H22e( true) ;
         }
         else
         {
            wb_table9_56_H22e( false) ;
         }
      }

      protected void wb_table8_48_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname, tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo1_Internalname, context.localUtil.Format(AV19Lote_NFeDataProtocolo1, "99/99/99"), context.localUtil.Format( AV19Lote_NFeDataProtocolo1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo_to1_Internalname, context.localUtil.Format(AV20Lote_NFeDataProtocolo_To1, "99/99/99"), context.localUtil.Format( AV20Lote_NFeDataProtocolo_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_48_H22e( true) ;
         }
         else
         {
            wb_table8_48_H22e( false) ;
         }
      }

      protected void wb_table7_40_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe1_Internalname, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe1_Internalname, context.localUtil.Format(AV17Lote_DataNfe1, "99/99/99"), context.localUtil.Format( AV17Lote_DataNfe1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_173_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to1_Internalname, context.localUtil.Format(AV18Lote_DataNfe_To1, "99/99/99"), context.localUtil.Format( AV18Lote_DataNfe_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteNotasFiscais.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteNotasFiscais.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_40_H22e( true) ;
         }
         else
         {
            wb_table7_40_H22e( false) ;
         }
      }

      protected void wb_table4_13_H22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_H22e( true) ;
         }
         else
         {
            wb_table4_13_H22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH22( ) ;
         WSH22( ) ;
         WEH22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299394833");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("extrawwlotenotasfiscais.js", "?20205299394834");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1732( )
      {
         edtLote_Codigo_Internalname = "LOTE_CODIGO_"+sGXsfl_173_idx;
         edtLote_NFe_Internalname = "LOTE_NFE_"+sGXsfl_173_idx;
         edtLote_DataNfe_Internalname = "LOTE_DATANFE_"+sGXsfl_173_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_173_idx;
         edtLote_NFeDataProtocolo_Internalname = "LOTE_NFEDATAPROTOCOLO_"+sGXsfl_173_idx;
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO_"+sGXsfl_173_idx;
         edtLote_LiqData_Internalname = "LOTE_LIQDATA_"+sGXsfl_173_idx;
         edtLote_LiqValor_Internalname = "LOTE_LIQVALOR_"+sGXsfl_173_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_173_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_173_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_173_idx;
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF_"+sGXsfl_173_idx;
      }

      protected void SubsflControlProps_fel_1732( )
      {
         edtLote_Codigo_Internalname = "LOTE_CODIGO_"+sGXsfl_173_fel_idx;
         edtLote_NFe_Internalname = "LOTE_NFE_"+sGXsfl_173_fel_idx;
         edtLote_DataNfe_Internalname = "LOTE_DATANFE_"+sGXsfl_173_fel_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_173_fel_idx;
         edtLote_NFeDataProtocolo_Internalname = "LOTE_NFEDATAPROTOCOLO_"+sGXsfl_173_fel_idx;
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO_"+sGXsfl_173_fel_idx;
         edtLote_LiqData_Internalname = "LOTE_LIQDATA_"+sGXsfl_173_fel_idx;
         edtLote_LiqValor_Internalname = "LOTE_LIQVALOR_"+sGXsfl_173_fel_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_173_fel_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_173_fel_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_173_fel_idx;
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF_"+sGXsfl_173_fel_idx;
      }

      protected void sendrow_1732( )
      {
         SubsflControlProps_1732( ) ;
         WBH20( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_173_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_173_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_173_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_NFe_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_NFe_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_DataNfe_Internalname,context.localUtil.Format(A674Lote_DataNfe, "99/99/99"),context.localUtil.Format( A674Lote_DataNfe, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_DataNfe_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ",", "")),context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor3Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_NFeDataProtocolo_Internalname,context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"),context.localUtil.Format( A1001Lote_NFeDataProtocolo, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_NFeDataProtocolo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_PrevPagamento_Internalname,context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"),context.localUtil.Format( A857Lote_PrevPagamento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_PrevPagamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_LiqData_Internalname,context.localUtil.Format(A676Lote_LiqData, "99/99/99"),context.localUtil.Format( A676Lote_LiqData, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_LiqData_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_LiqValor_Internalname,StringUtil.LTrim( StringUtil.NToC( A677Lote_LiqValor, 18, 5, ",", "")),context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_LiqValor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor3Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Numero_Internalname,StringUtil.RTrim( A562Lote_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Nome_Internalname,StringUtil.RTrim( A563Lote_Nome),StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtLote_Nome_Link,(String)"",(String)"",(String)"",(String)edtLote_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Data_Internalname,context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_QtdePF_Internalname,StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ",", "")),context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_QtdePF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)173,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_CODIGO"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFE"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATANFE"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, A674Lote_DataNfe));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFEDATAPROTOCOLO"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, A1001Lote_NFeDataProtocolo));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_PREVPAGAMENTO"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, A857Lote_PrevPagamento));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_LIQDATA"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, A676Lote_LiqData));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_LIQVALOR"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_QTDEPF"+"_"+sGXsfl_173_idx, GetSecureSignedToken( sGXsfl_173_idx, context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_173_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_173_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_173_idx+1));
            sGXsfl_173_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_173_idx), 4, 0)), 4, "0");
            SubsflControlProps_1732( ) ;
         }
         /* End function sendrow_1732 */
      }

      protected void init_default_properties( )
      {
         lblLotetitle_Internalname = "LOTETITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextlote_contratadacod_Internalname = "FILTERTEXTLOTE_CONTRATADACOD";
         edtavLote_contratadacod_Internalname = "vLOTE_CONTRATADACOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavLote_nfe1_Internalname = "vLOTE_NFE1";
         edtavLote_datanfe1_Internalname = "vLOTE_DATANFE1";
         lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT1";
         edtavLote_datanfe_to1_Internalname = "vLOTE_DATANFE_TO1";
         tblTablemergeddynamicfilterslote_datanfe1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1";
         edtavLote_nfedataprotocolo1_Internalname = "vLOTE_NFEDATAPROTOCOLO1";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_NFEDATAPROTOCOLO_RANGEMIDDLETEXT1";
         edtavLote_nfedataprotocolo_to1_Internalname = "vLOTE_NFEDATAPROTOCOLO_TO1";
         tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1";
         edtavLote_prevpagamento1_Internalname = "vLOTE_PREVPAGAMENTO1";
         lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT1";
         edtavLote_prevpagamento_to1_Internalname = "vLOTE_PREVPAGAMENTO_TO1";
         tblTablemergeddynamicfilterslote_prevpagamento1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1";
         edtavLote_liqdata1_Internalname = "vLOTE_LIQDATA1";
         lblDynamicfilterslote_liqdata_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_LIQDATA_RANGEMIDDLETEXT1";
         edtavLote_liqdata_to1_Internalname = "vLOTE_LIQDATA_TO1";
         tblTablemergeddynamicfilterslote_liqdata1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1";
         edtavLote_numero1_Internalname = "vLOTE_NUMERO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavLote_nfe2_Internalname = "vLOTE_NFE2";
         edtavLote_datanfe2_Internalname = "vLOTE_DATANFE2";
         lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT2";
         edtavLote_datanfe_to2_Internalname = "vLOTE_DATANFE_TO2";
         tblTablemergeddynamicfilterslote_datanfe2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2";
         edtavLote_nfedataprotocolo2_Internalname = "vLOTE_NFEDATAPROTOCOLO2";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_NFEDATAPROTOCOLO_RANGEMIDDLETEXT2";
         edtavLote_nfedataprotocolo_to2_Internalname = "vLOTE_NFEDATAPROTOCOLO_TO2";
         tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2";
         edtavLote_prevpagamento2_Internalname = "vLOTE_PREVPAGAMENTO2";
         lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT2";
         edtavLote_prevpagamento_to2_Internalname = "vLOTE_PREVPAGAMENTO_TO2";
         tblTablemergeddynamicfilterslote_prevpagamento2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2";
         edtavLote_liqdata2_Internalname = "vLOTE_LIQDATA2";
         lblDynamicfilterslote_liqdata_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_LIQDATA_RANGEMIDDLETEXT2";
         edtavLote_liqdata_to2_Internalname = "vLOTE_LIQDATA_TO2";
         tblTablemergeddynamicfilterslote_liqdata2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2";
         edtavLote_numero2_Internalname = "vLOTE_NUMERO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavLote_nfe3_Internalname = "vLOTE_NFE3";
         edtavLote_datanfe3_Internalname = "vLOTE_DATANFE3";
         lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT3";
         edtavLote_datanfe_to3_Internalname = "vLOTE_DATANFE_TO3";
         tblTablemergeddynamicfilterslote_datanfe3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3";
         edtavLote_nfedataprotocolo3_Internalname = "vLOTE_NFEDATAPROTOCOLO3";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_NFEDATAPROTOCOLO_RANGEMIDDLETEXT3";
         edtavLote_nfedataprotocolo_to3_Internalname = "vLOTE_NFEDATAPROTOCOLO_TO3";
         tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3";
         edtavLote_prevpagamento3_Internalname = "vLOTE_PREVPAGAMENTO3";
         lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT3";
         edtavLote_prevpagamento_to3_Internalname = "vLOTE_PREVPAGAMENTO_TO3";
         tblTablemergeddynamicfilterslote_prevpagamento3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3";
         edtavLote_liqdata3_Internalname = "vLOTE_LIQDATA3";
         lblDynamicfilterslote_liqdata_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_LIQDATA_RANGEMIDDLETEXT3";
         edtavLote_liqdata_to3_Internalname = "vLOTE_LIQDATA_TO3";
         tblTablemergeddynamicfilterslote_liqdata3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3";
         edtavLote_numero3_Internalname = "vLOTE_NUMERO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtn_search_Internalname = "BTN_SEARCH";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         edtLote_Codigo_Internalname = "LOTE_CODIGO";
         edtLote_NFe_Internalname = "LOTE_NFE";
         edtLote_DataNfe_Internalname = "LOTE_DATANFE";
         edtLote_Valor_Internalname = "LOTE_VALOR";
         edtLote_NFeDataProtocolo_Internalname = "LOTE_NFEDATAPROTOCOLO";
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO";
         edtLote_LiqData_Internalname = "LOTE_LIQDATA";
         edtLote_LiqValor_Internalname = "LOTE_LIQVALOR";
         edtLote_Numero_Internalname = "LOTE_NUMERO";
         edtLote_Nome_Internalname = "LOTE_NOME";
         edtLote_Data_Internalname = "LOTE_DATA";
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLote_QtdePF_Jsonclick = "";
         edtLote_Data_Jsonclick = "";
         edtLote_Nome_Jsonclick = "";
         edtLote_Numero_Jsonclick = "";
         edtLote_LiqValor_Jsonclick = "";
         edtLote_LiqData_Jsonclick = "";
         edtLote_PrevPagamento_Jsonclick = "";
         edtLote_NFeDataProtocolo_Jsonclick = "";
         edtLote_Valor_Jsonclick = "";
         edtLote_DataNfe_Jsonclick = "";
         edtLote_NFe_Jsonclick = "";
         edtLote_Codigo_Jsonclick = "";
         edtavLote_datanfe_to1_Jsonclick = "";
         edtavLote_datanfe1_Jsonclick = "";
         edtavLote_nfedataprotocolo_to1_Jsonclick = "";
         edtavLote_nfedataprotocolo1_Jsonclick = "";
         edtavLote_prevpagamento_to1_Jsonclick = "";
         edtavLote_prevpagamento1_Jsonclick = "";
         edtavLote_liqdata_to1_Jsonclick = "";
         edtavLote_liqdata1_Jsonclick = "";
         edtavLote_datanfe_to2_Jsonclick = "";
         edtavLote_datanfe2_Jsonclick = "";
         edtavLote_nfedataprotocolo_to2_Jsonclick = "";
         edtavLote_nfedataprotocolo2_Jsonclick = "";
         edtavLote_prevpagamento_to2_Jsonclick = "";
         edtavLote_prevpagamento2_Jsonclick = "";
         edtavLote_liqdata_to2_Jsonclick = "";
         edtavLote_liqdata2_Jsonclick = "";
         edtavLote_datanfe_to3_Jsonclick = "";
         edtavLote_datanfe3_Jsonclick = "";
         edtavLote_nfedataprotocolo_to3_Jsonclick = "";
         edtavLote_nfedataprotocolo3_Jsonclick = "";
         edtavLote_prevpagamento_to3_Jsonclick = "";
         edtavLote_prevpagamento3_Jsonclick = "";
         edtavLote_liqdata_to3_Jsonclick = "";
         edtavLote_liqdata3_Jsonclick = "";
         edtavLote_numero3_Jsonclick = "";
         edtavLote_nfe3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavLote_numero2_Jsonclick = "";
         edtavLote_nfe2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavLote_numero1_Jsonclick = "";
         edtavLote_nfe1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavLote_contratadacod_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtLote_Nome_Link = "";
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavLote_numero3_Visible = 1;
         tblTablemergeddynamicfilterslote_liqdata3_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 1;
         tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible = 1;
         tblTablemergeddynamicfilterslote_datanfe3_Visible = 1;
         edtavLote_nfe3_Visible = 1;
         edtavLote_numero2_Visible = 1;
         tblTablemergeddynamicfilterslote_liqdata2_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 1;
         tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible = 1;
         tblTablemergeddynamicfilterslote_datanfe2_Visible = 1;
         edtavLote_nfe2_Visible = 1;
         edtavLote_numero1_Visible = 1;
         tblTablemergeddynamicfilterslote_liqdata1_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 1;
         tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible = 1;
         tblTablemergeddynamicfilterslote_datanfe1_Visible = 1;
         edtavLote_nfe1_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Lote";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E22H22',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtLote_Nome_Link',ctrl:'LOTE_NOME',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E16H22',iparms:[],oparms:[{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12H22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E17H22',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E18H22',iparms:[],oparms:[{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13H22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E19H22',iparms:[{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14H22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E20H22',iparms:[{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15H22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_NFEDATAPROTOCOLO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_liqdata3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_LIQDATA3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV17Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV18Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV19Lote_NFeDataProtocolo1',fld:'vLOTE_NFEDATAPROTOCOLO1',pic:'',nv:''},{av:'AV20Lote_NFeDataProtocolo_To1',fld:'vLOTE_NFEDATAPROTOCOLO_TO1',pic:'',nv:''},{av:'AV21Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV22Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV23Lote_LiqData1',fld:'vLOTE_LIQDATA1',pic:'',nv:''},{av:'AV24Lote_LiqData_To1',fld:'vLOTE_LIQDATA_TO1',pic:'',nv:''},{av:'AV25Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV29Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV30Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV31Lote_NFeDataProtocolo2',fld:'vLOTE_NFEDATAPROTOCOLO2',pic:'',nv:''},{av:'AV32Lote_NFeDataProtocolo_To2',fld:'vLOTE_NFEDATAPROTOCOLO_TO2',pic:'',nv:''},{av:'AV33Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV34Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV35Lote_LiqData2',fld:'vLOTE_LIQDATA2',pic:'',nv:''},{av:'AV36Lote_LiqData_To2',fld:'vLOTE_LIQDATA_TO2',pic:'',nv:''},{av:'AV37Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV38DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV41Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV42Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV43Lote_NFeDataProtocolo3',fld:'vLOTE_NFEDATAPROTOCOLO3',pic:'',nv:''},{av:'AV44Lote_NFeDataProtocolo_To3',fld:'vLOTE_NFEDATAPROTOCOLO_TO3',pic:'',nv:''},{av:'AV45Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV46Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV47Lote_LiqData3',fld:'vLOTE_LIQDATA3',pic:'',nv:''},{av:'AV48Lote_LiqData_To3',fld:'vLOTE_LIQDATA_TO3',pic:'',nv:''},{av:'AV49Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV150Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV51DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Lote_DataNfe1 = DateTime.MinValue;
         AV18Lote_DataNfe_To1 = DateTime.MinValue;
         AV19Lote_NFeDataProtocolo1 = DateTime.MinValue;
         AV20Lote_NFeDataProtocolo_To1 = DateTime.MinValue;
         AV21Lote_PrevPagamento1 = DateTime.MinValue;
         AV22Lote_PrevPagamento_To1 = DateTime.MinValue;
         AV23Lote_LiqData1 = DateTime.MinValue;
         AV24Lote_LiqData_To1 = DateTime.MinValue;
         AV25Lote_Numero1 = "";
         AV27DynamicFiltersSelector2 = "";
         AV29Lote_DataNfe2 = DateTime.MinValue;
         AV30Lote_DataNfe_To2 = DateTime.MinValue;
         AV31Lote_NFeDataProtocolo2 = DateTime.MinValue;
         AV32Lote_NFeDataProtocolo_To2 = DateTime.MinValue;
         AV33Lote_PrevPagamento2 = DateTime.MinValue;
         AV34Lote_PrevPagamento_To2 = DateTime.MinValue;
         AV35Lote_LiqData2 = DateTime.MinValue;
         AV36Lote_LiqData_To2 = DateTime.MinValue;
         AV37Lote_Numero2 = "";
         AV39DynamicFiltersSelector3 = "";
         AV41Lote_DataNfe3 = DateTime.MinValue;
         AV42Lote_DataNfe_To3 = DateTime.MinValue;
         AV43Lote_NFeDataProtocolo3 = DateTime.MinValue;
         AV44Lote_NFeDataProtocolo_To3 = DateTime.MinValue;
         AV45Lote_PrevPagamento3 = DateTime.MinValue;
         AV46Lote_PrevPagamento_To3 = DateTime.MinValue;
         AV47Lote_LiqData3 = DateTime.MinValue;
         AV48Lote_LiqData_To3 = DateTime.MinValue;
         AV49Lote_Numero3 = "";
         AV150Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         scmdbuf = "";
         H00H22_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00H22_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00H22_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00H22_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00H22_A456ContagemResultado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 = "";
         AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 = DateTime.MinValue;
         AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 = DateTime.MinValue;
         AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 = DateTime.MinValue;
         AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 = DateTime.MinValue;
         AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 = DateTime.MinValue;
         AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 = DateTime.MinValue;
         AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 = DateTime.MinValue;
         AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 = DateTime.MinValue;
         AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = "";
         AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 = "";
         AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 = DateTime.MinValue;
         AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 = DateTime.MinValue;
         AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 = DateTime.MinValue;
         AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 = DateTime.MinValue;
         AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 = DateTime.MinValue;
         AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 = DateTime.MinValue;
         AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 = DateTime.MinValue;
         AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 = DateTime.MinValue;
         AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = "";
         AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 = "";
         AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 = DateTime.MinValue;
         AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 = DateTime.MinValue;
         AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 = DateTime.MinValue;
         AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 = DateTime.MinValue;
         AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 = DateTime.MinValue;
         AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 = DateTime.MinValue;
         AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 = DateTime.MinValue;
         AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 = DateTime.MinValue;
         AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = "";
         A674Lote_DataNfe = DateTime.MinValue;
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         A676Lote_LiqData = DateTime.MinValue;
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 = "";
         lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 = "";
         lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 = "";
         H00H27_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         H00H27_A563Lote_Nome = new String[] {""} ;
         H00H27_A562Lote_Numero = new String[] {""} ;
         H00H27_A677Lote_LiqValor = new decimal[1] ;
         H00H27_n677Lote_LiqValor = new bool[] {false} ;
         H00H27_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         H00H27_n676Lote_LiqData = new bool[] {false} ;
         H00H27_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         H00H27_n857Lote_PrevPagamento = new bool[] {false} ;
         H00H27_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         H00H27_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         H00H27_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         H00H27_n674Lote_DataNfe = new bool[] {false} ;
         H00H27_A673Lote_NFe = new int[1] ;
         H00H27_n673Lote_NFe = new bool[] {false} ;
         H00H27_A596Lote_Codigo = new int[1] ;
         H00H27_A1231Lote_ContratadaCod = new int[1] ;
         H00H27_n1231Lote_ContratadaCod = new bool[] {false} ;
         H00H27_A1057Lote_ValorGlosas = new decimal[1] ;
         H00H27_n1057Lote_ValorGlosas = new bool[] {false} ;
         H00H212_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         AV52Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblLotetitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextlote_contratadacod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtn_search_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterslote_liqdata_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_liqdata_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_liqdata_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.extrawwlotenotasfiscais__default(),
            new Object[][] {
                new Object[] {
               H00H22_A597ContagemResultado_LoteAceiteCod, H00H22_n597ContagemResultado_LoteAceiteCod, H00H22_A512ContagemResultado_ValorPF, H00H22_n512ContagemResultado_ValorPF, H00H22_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00H27_A564Lote_Data, H00H27_A563Lote_Nome, H00H27_A562Lote_Numero, H00H27_A677Lote_LiqValor, H00H27_n677Lote_LiqValor, H00H27_A676Lote_LiqData, H00H27_n676Lote_LiqData, H00H27_A857Lote_PrevPagamento, H00H27_n857Lote_PrevPagamento, H00H27_A1001Lote_NFeDataProtocolo,
               H00H27_n1001Lote_NFeDataProtocolo, H00H27_A674Lote_DataNfe, H00H27_n674Lote_DataNfe, H00H27_A673Lote_NFe, H00H27_n673Lote_NFe, H00H27_A596Lote_Codigo, H00H27_A1231Lote_ContratadaCod, H00H27_n1231Lote_ContratadaCod, H00H27_A1057Lote_ValorGlosas, H00H27_n1057Lote_ValorGlosas
               }
               , new Object[] {
               H00H212_AGRID_nRecordCount
               }
            }
         );
         AV150Pgmname = "ExtraWWLoteNotasFiscais";
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         AV150Pgmname = "ExtraWWLoteNotasFiscais";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_173 ;
      private short nGXsfl_173_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_173_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV16Lote_NFe1 ;
      private int AV28Lote_NFe2 ;
      private int AV40Lote_NFe3 ;
      private int AV53Lote_ContratadaCod ;
      private int A596Lote_Codigo ;
      private int AV114ExtraWWLoteNotasFiscaisDS_1_Lote_contratadacod ;
      private int AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 ;
      private int AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 ;
      private int AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 ;
      private int A673Lote_NFe ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int A1231Lote_ContratadaCod ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavLote_nfe1_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe1_Visible ;
      private int tblTablemergeddynamicfilterslote_nfedataprotocolo1_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento1_Visible ;
      private int tblTablemergeddynamicfilterslote_liqdata1_Visible ;
      private int edtavLote_numero1_Visible ;
      private int edtavLote_nfe2_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe2_Visible ;
      private int tblTablemergeddynamicfilterslote_nfedataprotocolo2_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento2_Visible ;
      private int tblTablemergeddynamicfilterslote_liqdata2_Visible ;
      private int edtavLote_numero2_Visible ;
      private int edtavLote_nfe3_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe3_Visible ;
      private int tblTablemergeddynamicfilterslote_nfedataprotocolo3_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento3_Visible ;
      private int tblTablemergeddynamicfilterslote_liqdata3_Visible ;
      private int edtavLote_numero3_Visible ;
      private int AV151GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A573Lote_QtdePF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A572Lote_Valor ;
      private decimal A677Lote_LiqValor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_173_idx="0001" ;
      private String AV25Lote_Numero1 ;
      private String AV37Lote_Numero2 ;
      private String AV49Lote_Numero3 ;
      private String AV150Pgmname ;
      private String GXKey ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ;
      private String AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ;
      private String AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ;
      private String edtLote_Codigo_Internalname ;
      private String edtLote_NFe_Internalname ;
      private String edtLote_DataNfe_Internalname ;
      private String edtLote_Valor_Internalname ;
      private String edtLote_NFeDataProtocolo_Internalname ;
      private String edtLote_PrevPagamento_Internalname ;
      private String edtLote_LiqData_Internalname ;
      private String edtLote_LiqValor_Internalname ;
      private String A562Lote_Numero ;
      private String edtLote_Numero_Internalname ;
      private String A563Lote_Nome ;
      private String edtLote_Nome_Internalname ;
      private String edtLote_Data_Internalname ;
      private String edtLote_QtdePF_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ;
      private String lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ;
      private String lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ;
      private String edtavLote_contratadacod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavLote_nfe1_Internalname ;
      private String edtavLote_datanfe1_Internalname ;
      private String edtavLote_datanfe_to1_Internalname ;
      private String edtavLote_nfedataprotocolo1_Internalname ;
      private String edtavLote_nfedataprotocolo_to1_Internalname ;
      private String edtavLote_prevpagamento1_Internalname ;
      private String edtavLote_prevpagamento_to1_Internalname ;
      private String edtavLote_liqdata1_Internalname ;
      private String edtavLote_liqdata_to1_Internalname ;
      private String edtavLote_numero1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavLote_nfe2_Internalname ;
      private String edtavLote_datanfe2_Internalname ;
      private String edtavLote_datanfe_to2_Internalname ;
      private String edtavLote_nfedataprotocolo2_Internalname ;
      private String edtavLote_nfedataprotocolo_to2_Internalname ;
      private String edtavLote_prevpagamento2_Internalname ;
      private String edtavLote_prevpagamento_to2_Internalname ;
      private String edtavLote_liqdata2_Internalname ;
      private String edtavLote_liqdata_to2_Internalname ;
      private String edtavLote_numero2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavLote_nfe3_Internalname ;
      private String edtavLote_datanfe3_Internalname ;
      private String edtavLote_datanfe_to3_Internalname ;
      private String edtavLote_nfedataprotocolo3_Internalname ;
      private String edtavLote_nfedataprotocolo_to3_Internalname ;
      private String edtavLote_prevpagamento3_Internalname ;
      private String edtavLote_prevpagamento_to3_Internalname ;
      private String edtavLote_liqdata3_Internalname ;
      private String edtavLote_liqdata_to3_Internalname ;
      private String edtavLote_numero3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLote_Nome_Link ;
      private String tblTablemergeddynamicfilterslote_datanfe1_Internalname ;
      private String tblTablemergeddynamicfilterslote_nfedataprotocolo1_Internalname ;
      private String tblTablemergeddynamicfilterslote_prevpagamento1_Internalname ;
      private String tblTablemergeddynamicfilterslote_liqdata1_Internalname ;
      private String tblTablemergeddynamicfilterslote_datanfe2_Internalname ;
      private String tblTablemergeddynamicfilterslote_nfedataprotocolo2_Internalname ;
      private String tblTablemergeddynamicfilterslote_prevpagamento2_Internalname ;
      private String tblTablemergeddynamicfilterslote_liqdata2_Internalname ;
      private String tblTablemergeddynamicfilterslote_datanfe3_Internalname ;
      private String tblTablemergeddynamicfilterslote_nfedataprotocolo3_Internalname ;
      private String tblTablemergeddynamicfilterslote_prevpagamento3_Internalname ;
      private String tblTablemergeddynamicfilterslote_liqdata3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblUnnamedtable1_Internalname ;
      private String lblLotetitle_Internalname ;
      private String lblLotetitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblFiltertextlote_contratadacod_Internalname ;
      private String lblFiltertextlote_contratadacod_Jsonclick ;
      private String edtavLote_contratadacod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtn_search_Internalname ;
      private String bttBtn_search_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavLote_nfe1_Jsonclick ;
      private String edtavLote_numero1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavLote_nfe2_Jsonclick ;
      private String edtavLote_numero2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavLote_nfe3_Jsonclick ;
      private String edtavLote_numero3_Jsonclick ;
      private String edtavLote_liqdata3_Jsonclick ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext3_Jsonclick ;
      private String edtavLote_liqdata_to3_Jsonclick ;
      private String edtavLote_prevpagamento3_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick ;
      private String edtavLote_prevpagamento_to3_Jsonclick ;
      private String edtavLote_nfedataprotocolo3_Jsonclick ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext3_Jsonclick ;
      private String edtavLote_nfedataprotocolo_to3_Jsonclick ;
      private String edtavLote_datanfe3_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick ;
      private String edtavLote_datanfe_to3_Jsonclick ;
      private String edtavLote_liqdata2_Jsonclick ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext2_Jsonclick ;
      private String edtavLote_liqdata_to2_Jsonclick ;
      private String edtavLote_prevpagamento2_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick ;
      private String edtavLote_prevpagamento_to2_Jsonclick ;
      private String edtavLote_nfedataprotocolo2_Jsonclick ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext2_Jsonclick ;
      private String edtavLote_nfedataprotocolo_to2_Jsonclick ;
      private String edtavLote_datanfe2_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick ;
      private String edtavLote_datanfe_to2_Jsonclick ;
      private String edtavLote_liqdata1_Jsonclick ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_liqdata_rangemiddletext1_Jsonclick ;
      private String edtavLote_liqdata_to1_Jsonclick ;
      private String edtavLote_prevpagamento1_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick ;
      private String edtavLote_prevpagamento_to1_Jsonclick ;
      private String edtavLote_nfedataprotocolo1_Jsonclick ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_nfedataprotocolo_rangemiddletext1_Jsonclick ;
      private String edtavLote_nfedataprotocolo_to1_Jsonclick ;
      private String edtavLote_datanfe1_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick ;
      private String edtavLote_datanfe_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_173_fel_idx="0001" ;
      private String ROClassString ;
      private String edtLote_Codigo_Jsonclick ;
      private String edtLote_NFe_Jsonclick ;
      private String edtLote_DataNfe_Jsonclick ;
      private String edtLote_Valor_Jsonclick ;
      private String edtLote_NFeDataProtocolo_Jsonclick ;
      private String edtLote_PrevPagamento_Jsonclick ;
      private String edtLote_LiqData_Jsonclick ;
      private String edtLote_LiqValor_Jsonclick ;
      private String edtLote_Numero_Jsonclick ;
      private String edtLote_Nome_Jsonclick ;
      private String edtLote_Data_Jsonclick ;
      private String edtLote_QtdePF_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A564Lote_Data ;
      private DateTime AV17Lote_DataNfe1 ;
      private DateTime AV18Lote_DataNfe_To1 ;
      private DateTime AV19Lote_NFeDataProtocolo1 ;
      private DateTime AV20Lote_NFeDataProtocolo_To1 ;
      private DateTime AV21Lote_PrevPagamento1 ;
      private DateTime AV22Lote_PrevPagamento_To1 ;
      private DateTime AV23Lote_LiqData1 ;
      private DateTime AV24Lote_LiqData_To1 ;
      private DateTime AV29Lote_DataNfe2 ;
      private DateTime AV30Lote_DataNfe_To2 ;
      private DateTime AV31Lote_NFeDataProtocolo2 ;
      private DateTime AV32Lote_NFeDataProtocolo_To2 ;
      private DateTime AV33Lote_PrevPagamento2 ;
      private DateTime AV34Lote_PrevPagamento_To2 ;
      private DateTime AV35Lote_LiqData2 ;
      private DateTime AV36Lote_LiqData_To2 ;
      private DateTime AV41Lote_DataNfe3 ;
      private DateTime AV42Lote_DataNfe_To3 ;
      private DateTime AV43Lote_NFeDataProtocolo3 ;
      private DateTime AV44Lote_NFeDataProtocolo_To3 ;
      private DateTime AV45Lote_PrevPagamento3 ;
      private DateTime AV46Lote_PrevPagamento_To3 ;
      private DateTime AV47Lote_LiqData3 ;
      private DateTime AV48Lote_LiqData_To3 ;
      private DateTime AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 ;
      private DateTime AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 ;
      private DateTime AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 ;
      private DateTime AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 ;
      private DateTime AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 ;
      private DateTime AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 ;
      private DateTime AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 ;
      private DateTime AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 ;
      private DateTime AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 ;
      private DateTime AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 ;
      private DateTime AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 ;
      private DateTime AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 ;
      private DateTime AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 ;
      private DateTime AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 ;
      private DateTime AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 ;
      private DateTime AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 ;
      private DateTime AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 ;
      private DateTime AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 ;
      private DateTime AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 ;
      private DateTime AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 ;
      private DateTime AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 ;
      private DateTime AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 ;
      private DateTime AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 ;
      private DateTime AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A1001Lote_NFeDataProtocolo ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A676Lote_LiqData ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV26DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool AV51DynamicFiltersIgnoreFirst ;
      private bool AV50DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 ;
      private bool AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 ;
      private bool n673Lote_NFe ;
      private bool n674Lote_DataNfe ;
      private bool n1001Lote_NFeDataProtocolo ;
      private bool n857Lote_PrevPagamento ;
      private bool n676Lote_LiqData ;
      private bool n677Lote_LiqValor ;
      private bool AV6WWPContext_gxTpr_Userehcontratante ;
      private bool n1231Lote_ContratadaCod ;
      private bool n1057Lote_ValorGlosas ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV27DynamicFiltersSelector2 ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 ;
      private String AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 ;
      private String AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 ;
      private IGxSession AV52Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00H22_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00H22_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] H00H22_A512ContagemResultado_ValorPF ;
      private bool[] H00H22_n512ContagemResultado_ValorPF ;
      private int[] H00H22_A456ContagemResultado_Codigo ;
      private DateTime[] H00H27_A564Lote_Data ;
      private String[] H00H27_A563Lote_Nome ;
      private String[] H00H27_A562Lote_Numero ;
      private decimal[] H00H27_A677Lote_LiqValor ;
      private bool[] H00H27_n677Lote_LiqValor ;
      private DateTime[] H00H27_A676Lote_LiqData ;
      private bool[] H00H27_n676Lote_LiqData ;
      private DateTime[] H00H27_A857Lote_PrevPagamento ;
      private bool[] H00H27_n857Lote_PrevPagamento ;
      private DateTime[] H00H27_A1001Lote_NFeDataProtocolo ;
      private bool[] H00H27_n1001Lote_NFeDataProtocolo ;
      private DateTime[] H00H27_A674Lote_DataNfe ;
      private bool[] H00H27_n674Lote_DataNfe ;
      private int[] H00H27_A673Lote_NFe ;
      private bool[] H00H27_n673Lote_NFe ;
      private int[] H00H27_A596Lote_Codigo ;
      private int[] H00H27_A1231Lote_ContratadaCod ;
      private bool[] H00H27_n1231Lote_ContratadaCod ;
      private decimal[] H00H27_A1057Lote_ValorGlosas ;
      private bool[] H00H27_n1057Lote_ValorGlosas ;
      private long[] H00H212_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class extrawwlotenotasfiscais__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00H27( IGxContext context ,
                                             String AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 ,
                                             int AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 ,
                                             DateTime AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 ,
                                             DateTime AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 ,
                                             DateTime AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 ,
                                             DateTime AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 ,
                                             DateTime AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 ,
                                             DateTime AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 ,
                                             DateTime AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 ,
                                             DateTime AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 ,
                                             String AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ,
                                             bool AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 ,
                                             String AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 ,
                                             int AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 ,
                                             DateTime AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 ,
                                             DateTime AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 ,
                                             DateTime AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 ,
                                             DateTime AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 ,
                                             DateTime AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 ,
                                             DateTime AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 ,
                                             DateTime AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 ,
                                             DateTime AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 ,
                                             String AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ,
                                             bool AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 ,
                                             String AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 ,
                                             int AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 ,
                                             DateTime AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 ,
                                             DateTime AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 ,
                                             DateTime AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 ,
                                             DateTime AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 ,
                                             DateTime AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 ,
                                             DateTime AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 ,
                                             DateTime AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 ,
                                             DateTime AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 ,
                                             String AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ,
                                             int AV16Lote_NFe1 ,
                                             int AV28Lote_NFe2 ,
                                             int AV40Lote_NFe3 ,
                                             DateTime AV17Lote_DataNfe1 ,
                                             DateTime AV29Lote_DataNfe2 ,
                                             DateTime AV41Lote_DataNfe3 ,
                                             int A673Lote_NFe ,
                                             DateTime A674Lote_DataNfe ,
                                             DateTime A1001Lote_NFeDataProtocolo ,
                                             DateTime A857Lote_PrevPagamento ,
                                             DateTime A676Lote_LiqData ,
                                             String A562Lote_Numero ,
                                             DateTime Gx_date ,
                                             short AV13OrderedBy ,
                                             bool AV6WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [40] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_LiqValor], T1.[Lote_LiqData], T1.[Lote_PrevPagamento], T1.[Lote_NFeDataProtocolo], T1.[Lote_DataNfe], T1.[Lote_NFe], T1.[Lote_Codigo], COALESCE( T2.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T3.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas";
         sFromString = " FROM (([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T6.[GXC2], 0) + COALESCE( T5.[GXC3], 0) AS Lote_ValorGlosas, T4.[Lote_Codigo] FROM (([Lote] T4 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T7.[ContagemResultadoIndicadores_Valor]) AS GXC3, T8.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T7 WITH (NOLOCK),  [Lote] T8 WITH (NOLOCK) WHERE T7.[ContagemResultadoIndicadores_LoteCod] = T8.[Lote_Codigo] GROUP BY T8.[Lote_Codigo] ) T5 ON T5.[Lote_Codigo] = T4.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T4.[Lote_Codigo]) ) T3 ON T3.[Lote_Codigo] = T1.[Lote_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (@AV6WWPCo_2Userehcontratante = 1 or ( COALESCE( T2.[Lote_ContratadaCod], 0) = @AV6WWPCo_1Contratada_codigo))";
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( (0==AV16Lote_NFe1) && (0==AV28Lote_NFe2) && (0==AV40Lote_NFe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] > 0)";
         }
         if ( (DateTime.MinValue==AV17Lote_DataNfe1) && (DateTime.MinValue==AV29Lote_DataNfe2) && (DateTime.MinValue==AV41Lote_DataNfe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= DATEADD( dd,1, DATEADD( dd,-DAY(@Gx_date), @Gx_date)))";
         }
         else
         {
            GXv_int2[32] = 1;
            GXv_int2[33] = 1;
         }
         if ( (DateTime.MinValue==AV17Lote_DataNfe1) && (DateTime.MinValue==AV29Lote_DataNfe2) && (DateTime.MinValue==AV41Lote_DataNfe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= DATEADD(day, -1, DATEADD(month, 1, DATEADD(day, -DAY(@Gx_date) + 1, @Gx_date))))";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Codigo] DESC";
         }
         else if ( AV13OrderedBy == 2 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Numero]";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00H212( IGxContext context ,
                                              String AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1 ,
                                              int AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1 ,
                                              DateTime AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1 ,
                                              DateTime AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1 ,
                                              DateTime AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1 ,
                                              DateTime AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1 ,
                                              DateTime AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1 ,
                                              DateTime AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1 ,
                                              DateTime AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1 ,
                                              DateTime AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1 ,
                                              String AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 ,
                                              bool AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 ,
                                              String AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2 ,
                                              int AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2 ,
                                              DateTime AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2 ,
                                              DateTime AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2 ,
                                              DateTime AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2 ,
                                              DateTime AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2 ,
                                              DateTime AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2 ,
                                              DateTime AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2 ,
                                              DateTime AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2 ,
                                              DateTime AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2 ,
                                              String AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 ,
                                              bool AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 ,
                                              String AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3 ,
                                              int AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3 ,
                                              DateTime AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3 ,
                                              DateTime AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3 ,
                                              DateTime AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3 ,
                                              DateTime AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3 ,
                                              DateTime AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3 ,
                                              DateTime AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3 ,
                                              DateTime AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3 ,
                                              DateTime AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3 ,
                                              String AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 ,
                                              int AV16Lote_NFe1 ,
                                              int AV28Lote_NFe2 ,
                                              int AV40Lote_NFe3 ,
                                              DateTime AV17Lote_DataNfe1 ,
                                              DateTime AV29Lote_DataNfe2 ,
                                              DateTime AV41Lote_DataNfe3 ,
                                              int A673Lote_NFe ,
                                              DateTime A674Lote_DataNfe ,
                                              DateTime A1001Lote_NFeDataProtocolo ,
                                              DateTime A857Lote_PrevPagamento ,
                                              DateTime A676Lote_LiqData ,
                                              String A562Lote_Numero ,
                                              DateTime Gx_date ,
                                              short AV13OrderedBy ,
                                              bool AV6WWPContext_gxTpr_Userehcontratante ,
                                              int A1231Lote_ContratadaCod ,
                                              int AV6WWPContext_gxTpr_Contratada_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [35] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T6.[GXC2], 0) + COALESCE( T5.[GXC3], 0) AS Lote_ValorGlosas, T4.[Lote_Codigo] FROM (([Lote] T4 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T7.[ContagemResultadoIndicadores_Valor]) AS GXC3, T8.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T7 WITH (NOLOCK),  [Lote] T8 WITH (NOLOCK) WHERE T7.[ContagemResultadoIndicadores_LoteCod] = T8.[Lote_Codigo] GROUP BY T8.[Lote_Codigo] ) T5 ON T5.[Lote_Codigo] = T4.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T4.[Lote_Codigo]) ) T3 ON T3.[Lote_Codigo] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV6WWPCo_2Userehcontratante = 1 or ( COALESCE( T2.[Lote_ContratadaCod], 0) = @AV6WWPCo_1Contratada_codigo))";
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWLoteNotasFiscaisDS_2_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV126ExtraWWLoteNotasFiscaisDS_13_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV127ExtraWWLoteNotasFiscaisDS_14_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] >= @AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NFEDATAPROTOCOLO") == 0 ) && ( ! (DateTime.MinValue==AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFeDataProtocolo] <= @AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] >= @AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_LIQDATA") == 0 ) && ( ! (DateTime.MinValue==AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_LiqData] <= @AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3)";
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( AV138ExtraWWLoteNotasFiscaisDS_25_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139ExtraWWLoteNotasFiscaisDS_26_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( (0==AV16Lote_NFe1) && (0==AV28Lote_NFe2) && (0==AV40Lote_NFe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] > 0)";
         }
         if ( (DateTime.MinValue==AV17Lote_DataNfe1) && (DateTime.MinValue==AV29Lote_DataNfe2) && (DateTime.MinValue==AV41Lote_DataNfe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= DATEADD( dd,1, DATEADD( dd,-DAY(@Gx_date), @Gx_date)))";
         }
         else
         {
            GXv_int4[32] = 1;
            GXv_int4[33] = 1;
         }
         if ( (DateTime.MinValue==AV17Lote_DataNfe1) && (DateTime.MinValue==AV29Lote_DataNfe2) && (DateTime.MinValue==AV41Lote_DataNfe3) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= DATEADD(day, -1, DATEADD(month, 1, DATEADD(day, -DAY(@Gx_date) + 1, @Gx_date))))";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00H27(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (String)dynConstraints[46] , (DateTime)dynConstraints[47] , (short)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
               case 2 :
                     return conditional_H00H212(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (String)dynConstraints[46] , (DateTime)dynConstraints[47] , (short)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H22 ;
          prmH00H22 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H27 ;
          prmH00H27 = new Object[] {
          new Object[] {"@AV6WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00H212 ;
          prmH00H212 = new Object[] {
          new Object[] {"@AV6WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116ExtraWWLoteNotasFiscaisDS_3_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117ExtraWWLoteNotasFiscaisDS_4_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118ExtraWWLoteNotasFiscaisDS_5_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWLoteNotasFiscaisDS_6_Lote_nfedataprotocolo1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWLoteNotasFiscaisDS_7_Lote_nfedataprotocolo_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWLoteNotasFiscaisDS_8_Lote_prevpagamento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV122ExtraWWLoteNotasFiscaisDS_9_Lote_prevpagamento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWLoteNotasFiscaisDS_10_Lote_liqdata1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124ExtraWWLoteNotasFiscaisDS_11_Lote_liqdata_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV125ExtraWWLoteNotasFiscaisDS_12_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@AV128ExtraWWLoteNotasFiscaisDS_15_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV129ExtraWWLoteNotasFiscaisDS_16_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV130ExtraWWLoteNotasFiscaisDS_17_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWLoteNotasFiscaisDS_18_Lote_nfedataprotocolo2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132ExtraWWLoteNotasFiscaisDS_19_Lote_nfedataprotocolo_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133ExtraWWLoteNotasFiscaisDS_20_Lote_prevpagamento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134ExtraWWLoteNotasFiscaisDS_21_Lote_prevpagamento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135ExtraWWLoteNotasFiscaisDS_22_Lote_liqdata2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWLoteNotasFiscaisDS_23_Lote_liqdata_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV137ExtraWWLoteNotasFiscaisDS_24_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@AV140ExtraWWLoteNotasFiscaisDS_27_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWLoteNotasFiscaisDS_28_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV142ExtraWWLoteNotasFiscaisDS_29_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143ExtraWWLoteNotasFiscaisDS_30_Lote_nfedataprotocolo3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144ExtraWWLoteNotasFiscaisDS_31_Lote_nfedataprotocolo_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145ExtraWWLoteNotasFiscaisDS_32_Lote_prevpagamento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV146ExtraWWLoteNotasFiscaisDS_33_Lote_prevpagamento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147ExtraWWLoteNotasFiscaisDS_34_Lote_liqdata3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWLoteNotasFiscaisDS_35_Lote_liqdata_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV149ExtraWWLoteNotasFiscaisDS_36_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H22", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H22,100,0,true,false )
             ,new CursorDef("H00H27", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H27,11,0,true,false )
             ,new CursorDef("H00H212", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H212,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[40]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                return;
       }
    }

 }

}
