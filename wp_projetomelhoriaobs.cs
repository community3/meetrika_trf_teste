/*
               File: WP_ProjetoMelhoriaObs
        Description: Projeto Melhoria Observacoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:49:10.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_projetomelhoriaobs : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_projetomelhoriaobs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_projetomelhoriaobs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo ,
                           int aP1_Sistema_Codigo ,
                           int aP2_AreaTrabalho_Codigo ,
                           String aP3_ParmProjeto_Sigla ,
                           String aP4_Sistema_Nome ,
                           ref decimal aP5_Sistema_FatorAjuste ,
                           ref short aP6_Sistema_Prazo ,
                           ref decimal aP7_Sistema_Custo ,
                           ref short aP8_Sistema_Esforco ,
                           ref String aP9_Sistema_Tipo ,
                           ref String aP10_Sistema_Tecnica )
      {
         this.AV21Projeto_Codigo = aP0_Projeto_Codigo;
         this.AV8Sistema_Codigo = aP1_Sistema_Codigo;
         this.AV9AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV29ParmProjeto_Sigla = aP3_ParmProjeto_Sigla;
         this.AV18Sistema_Nome = aP4_Sistema_Nome;
         this.AV23Sistema_FatorAjuste = aP5_Sistema_FatorAjuste;
         this.AV24Sistema_Prazo = aP6_Sistema_Prazo;
         this.AV25Sistema_Custo = aP7_Sistema_Custo;
         this.AV22Sistema_Esforco = aP8_Sistema_Esforco;
         this.AV26Sistema_Tipo = aP9_Sistema_Tipo;
         this.AV27Sistema_Tecnica = aP10_Sistema_Tecnica;
         executePrivate();
         aP5_Sistema_FatorAjuste=this.AV23Sistema_FatorAjuste;
         aP6_Sistema_Prazo=this.AV24Sistema_Prazo;
         aP7_Sistema_Custo=this.AV25Sistema_Custo;
         aP8_Sistema_Esforco=this.AV22Sistema_Esforco;
         aP9_Sistema_Tipo=this.AV26Sistema_Tipo;
         aP10_Sistema_Tecnica=this.AV27Sistema_Tecnica;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavCtlitemnaomens = new GXCombobox();
         cmbavCtlitemtipoinm = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CTLITEMNAOMENS") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV30WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVCTLITEMNAOMENSDU2( AV30WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Griditnprj") == 0 )
            {
               nRC_GXsfl_33 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_33_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_33_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGriditnprj_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Griditnprj") == 0 )
            {
               subGriditnprj_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV30WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV21Projeto_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Projeto_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Sistema_Codigo), "ZZZZZ9")));
                  AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
                  AV29ParmProjeto_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ParmProjeto_Sigla", AV29ParmProjeto_Sigla);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29ParmProjeto_Sigla, "@!"))));
                  AV18Sistema_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Nome", AV18Sistema_Nome);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!"))));
                  AV23Sistema_FatorAjuste = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV23Sistema_FatorAjuste, 6, 2)));
                  AV24Sistema_Prazo = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Prazo), 4, 0)));
                  AV25Sistema_Custo = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV25Sistema_Custo, 18, 5)));
                  AV22Sistema_Esforco = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Sistema_Esforco), 4, 0)));
                  AV26Sistema_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Sistema_Tipo", AV26Sistema_Tipo);
                  AV27Sistema_Tecnica = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sistema_Tecnica", AV27Sistema_Tecnica);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADU2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDU2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202054849114");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_projetomelhoriaobs.aspx") + "?" + UrlEncode("" +AV21Projeto_Codigo) + "," + UrlEncode("" +AV8Sistema_Codigo) + "," + UrlEncode("" +AV9AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV29ParmProjeto_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV18Sistema_Nome)) + "," + UrlEncode(StringUtil.Str(AV23Sistema_FatorAjuste,6,2)) + "," + UrlEncode("" +AV24Sistema_Prazo) + "," + UrlEncode(StringUtil.Str(AV25Sistema_Custo,18,5)) + "," + UrlEncode("" +AV22Sistema_Esforco) + "," + UrlEncode(StringUtil.RTrim(AV26Sistema_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV27Sistema_Tecnica))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_itnprj", AV14Sdt_ItnPrj);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_itnprj", AV14Sdt_ItnPrj);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_33", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_33), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITNPRJ", AV14Sdt_ItnPrj);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITNPRJ", AV14Sdt_ItnPrj);
         }
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_VALOR", StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPARMPROJETO_SIGLA", StringUtil.RTrim( AV29ParmProjeto_Sigla));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_TIPO", StringUtil.RTrim( AV26Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_TECNICA", StringUtil.RTrim( AV27Sistema_Tecnica));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( AV23Sistema_FatorAjuste, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Sistema_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CUSTO", StringUtil.LTrim( StringUtil.NToC( AV25Sistema_Custo, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_ESFORCO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Sistema_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV30WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV30WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29ParmProjeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29ParmProjeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Width", StringUtil.RTrim( Dvpanel_tblprj_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Cls", StringUtil.RTrim( Dvpanel_tblprj_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Title", StringUtil.RTrim( Dvpanel_tblprj_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Collapsible", StringUtil.BoolToStr( Dvpanel_tblprj_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Collapsed", StringUtil.BoolToStr( Dvpanel_tblprj_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autowidth", StringUtil.BoolToStr( Dvpanel_tblprj_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autoheight", StringUtil.BoolToStr( Dvpanel_tblprj_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tblprj_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Iconposition", StringUtil.RTrim( Dvpanel_tblprj_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autoscroll", StringUtil.BoolToStr( Dvpanel_tblprj_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDU2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDU2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_projetomelhoriaobs.aspx") + "?" + UrlEncode("" +AV21Projeto_Codigo) + "," + UrlEncode("" +AV8Sistema_Codigo) + "," + UrlEncode("" +AV9AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV29ParmProjeto_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV18Sistema_Nome)) + "," + UrlEncode(StringUtil.Str(AV23Sistema_FatorAjuste,6,2)) + "," + UrlEncode("" +AV24Sistema_Prazo) + "," + UrlEncode(StringUtil.Str(AV25Sistema_Custo,18,5)) + "," + UrlEncode("" +AV22Sistema_Esforco) + "," + UrlEncode(StringUtil.RTrim(AV26Sistema_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV27Sistema_Tecnica)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ProjetoMelhoriaObs" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Melhoria Observacoes" ;
      }

      protected void WBDU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DU2( true) ;
         }
         else
         {
            wb_table1_2_DU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DU2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto Melhoria Observacoes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDU0( ) ;
      }

      protected void WSDU2( )
      {
         STARTDU2( ) ;
         EVTDU2( ) ;
      }

      protected void EVTDU2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DU2 */
                              E11DU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'VOLTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DU2 */
                              E12DU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDITNPRJPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDITNPRJPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgriditnprj_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgriditnprj_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgriditnprj_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgriditnprj_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "CTLITEMNAOMENS.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDITNPRJ.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "CTLITEMNAOMENS.CLICK") == 0 ) )
                           {
                              nGXsfl_33_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
                              SubsflControlProps_332( ) ;
                              AV33GXV1 = (short)(nGXsfl_33_idx+GRIDITNPRJ_nFirstRecordOnPage);
                              if ( ( AV14Sdt_ItnPrj.Count >= AV33GXV1 ) && ( AV33GXV1 > 0 ) )
                              {
                                 AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DU2 */
                                    E13DU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CTLITEMNAOMENS.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DU2 */
                                    E14DU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDITNPRJ.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DU2 */
                                    E15DU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: E11DU2 */
                                          E11DU2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CTLITEMNAOMENS_" + sGXsfl_33_idx;
            dynavCtlitemnaomens.Name = GXCCtl;
            dynavCtlitemnaomens.WebTags = "";
            GXCCtl = "CTLITEMTIPOINM_" + sGXsfl_33_idx;
            cmbavCtlitemtipoinm.Name = GXCCtl;
            cmbavCtlitemtipoinm.WebTags = "";
            cmbavCtlitemtipoinm.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
            cmbavCtlitemtipoinm.addItem("1", "PC", 0);
            cmbavCtlitemtipoinm.addItem("2", "PF", 0);
            if ( cmbavCtlitemtipoinm.ItemCount > 0 )
            {
               if ( ( AV33GXV1 > 0 ) && ( AV14Sdt_ItnPrj.Count >= AV33GXV1 ) && (0==((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm) )
               {
                  ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm = (short)(NumberUtil.Val( cmbavCtlitemtipoinm.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm), 2, 0))), "."));
               }
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavProjeto_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVCTLITEMNAOMENSDU2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVCTLITEMNAOMENS_dataDU2( AV30WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVCTLITEMNAOMENS_htmlDU2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         String gxdynajaxvalue ;
         GXDLVCTLITEMNAOMENS_dataDU2( AV30WWPContext) ;
         gxdynajaxindex = 1;
         dynavCtlitemnaomens.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavCtlitemnaomens.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVCTLITEMNAOMENS_dataDU2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("-----");
         /* * Property Areatrabalho_cod not supported in */
         /* Using cursor H00DU2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* * Property Areatrabalho_cod not supported in */
            /* * Property Areatrabalho_cod not supported in */
            /* * Property Areatrabalho_cod not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00DU2_A715ItemNaoMensuravel_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00DU2_A715ItemNaoMensuravel_Codigo[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGriditnprj_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_332( ) ;
         while ( nGXsfl_33_idx <= nRC_GXsfl_33 )
         {
            sendrow_332( ) ;
            nGXsfl_33_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_33_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         context.GX_webresponse.AddString(GriditnprjContainer.ToJavascriptSource());
         /* End function gxnrGriditnprj_newrow */
      }

      protected void gxgrGriditnprj_refresh( int subGriditnprj_Rows ,
                                             wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         GRIDITNPRJ_nCurrentRecord = 0;
         RFDU2( ) ;
         /* End function gxgrGriditnprj_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla_Enabled), 5, 0)));
         edtavSistema_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome_Enabled), 5, 0)));
         edtavPrjitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0)));
         edtavPrjitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipo_Enabled), 5, 0)));
         edtavPrjitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemnome_Enabled), 5, 0)));
         edtavCtlitemvalor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemvalor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemvalor_Enabled), 5, 0)));
         cmbavCtlitemtipoinm.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlitemtipoinm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlitemtipoinm.Enabled), 5, 0)));
         edtavCtlitempfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitempfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitempfb_Enabled), 5, 0)));
         edtavCtlitempfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitempfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitempfl_Enabled), 5, 0)));
      }

      protected void RFDU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GriditnprjContainer.ClearRows();
         }
         wbStart = 33;
         nGXsfl_33_idx = 1;
         sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
         SubsflControlProps_332( ) ;
         nGXsfl_33_Refreshing = 1;
         GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
         GriditnprjContainer.AddObjectProperty("CmpContext", "");
         GriditnprjContainer.AddObjectProperty("InMasterPage", "false");
         GriditnprjContainer.AddObjectProperty("Class", "WorkWith");
         GriditnprjContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GriditnprjContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GriditnprjContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Backcolorstyle), 1, 0, ".", "")));
         GriditnprjContainer.PageSize = subGriditnprj_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_332( ) ;
            /* Execute user event: E15DU2 */
            E15DU2 ();
            if ( ( GRIDITNPRJ_nCurrentRecord > 0 ) && ( GRIDITNPRJ_nGridOutOfScope == 0 ) && ( nGXsfl_33_idx == 1 ) )
            {
               GRIDITNPRJ_nCurrentRecord = 0;
               GRIDITNPRJ_nGridOutOfScope = 1;
               subgriditnprj_firstpage( ) ;
               /* Execute user event: E15DU2 */
               E15DU2 ();
            }
            wbEnd = 33;
            WBDU0( ) ;
         }
         nGXsfl_33_Refreshing = 0;
      }

      protected int subGriditnprj_Pagecount( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( ((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nRecordCount/ (decimal)(subGriditnprj_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nRecordCount/ (decimal)(subGriditnprj_Recordsperpage( ))))+1) ;
      }

      protected int subGriditnprj_Recordcount( )
      {
         return AV14Sdt_ItnPrj.Count ;
      }

      protected int subGriditnprj_Recordsperpage( )
      {
         if ( subGriditnprj_Rows > 0 )
         {
            return subGriditnprj_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGriditnprj_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nFirstRecordOnPage/ (decimal)(subGriditnprj_Recordsperpage( ))))+1) ;
      }

      protected short subgriditnprj_firstpage( )
      {
         GRIDITNPRJ_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         }
         return 0 ;
      }

      protected short subgriditnprj_nextpage( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( ( GRIDITNPRJ_nRecordCount >= subGriditnprj_Recordsperpage( ) ) && ( GRIDITNPRJ_nEOF == 0 ) )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nFirstRecordOnPage+subGriditnprj_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         }
         return (short)(((GRIDITNPRJ_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgriditnprj_previouspage( )
      {
         if ( GRIDITNPRJ_nFirstRecordOnPage >= subGriditnprj_Recordsperpage( ) )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nFirstRecordOnPage-subGriditnprj_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         }
         return 0 ;
      }

      protected short subgriditnprj_lastpage( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( GRIDITNPRJ_nRecordCount > subGriditnprj_Recordsperpage( ) )
         {
            if ( ((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))) == 0 )
            {
               GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nRecordCount-subGriditnprj_Recordsperpage( ));
            }
            else
            {
               GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nRecordCount-((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDITNPRJ_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         }
         return 0 ;
      }

      protected int subgriditnprj_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(subGriditnprj_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDITNPRJ_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDU0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla_Enabled), 5, 0)));
         edtavSistema_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome_Enabled), 5, 0)));
         edtavPrjitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0)));
         edtavPrjitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipo_Enabled), 5, 0)));
         edtavPrjitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemnome_Enabled), 5, 0)));
         edtavCtlitemvalor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemvalor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemvalor_Enabled), 5, 0)));
         cmbavCtlitemtipoinm.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlitemtipoinm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlitemtipoinm.Enabled), 5, 0)));
         edtavCtlitempfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitempfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitempfb_Enabled), 5, 0)));
         edtavCtlitempfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitempfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitempfl_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13DU2 */
         E13DU2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVCTLITEMNAOMENS_htmlDU2( AV30WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_itnprj"), AV14Sdt_ItnPrj);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_ITNPRJ"), AV14Sdt_ItnPrj);
            /* Read variables values. */
            AV7Projeto_Sigla = StringUtil.Upper( cgiGet( edtavProjeto_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Projeto_Sigla", AV7Projeto_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Projeto_Sigla, "@!"))));
            /* Read saved values. */
            nRC_GXsfl_33 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_33"), ",", "."));
            GRIDITNPRJ_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_nFirstRecordOnPage"), ",", "."));
            GRIDITNPRJ_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_nEOF"), ",", "."));
            subGriditnprj_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
            Dvpanel_tblprj_Width = cgiGet( "DVPANEL_TBLPRJ_Width");
            Dvpanel_tblprj_Cls = cgiGet( "DVPANEL_TBLPRJ_Cls");
            Dvpanel_tblprj_Title = cgiGet( "DVPANEL_TBLPRJ_Title");
            Dvpanel_tblprj_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Collapsible"));
            Dvpanel_tblprj_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Collapsed"));
            Dvpanel_tblprj_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autowidth"));
            Dvpanel_tblprj_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autoheight"));
            Dvpanel_tblprj_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Showcollapseicon"));
            Dvpanel_tblprj_Iconposition = cgiGet( "DVPANEL_TBLPRJ_Iconposition");
            Dvpanel_tblprj_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autoscroll"));
            nRC_GXsfl_33 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_33"), ",", "."));
            nGXsfl_33_fel_idx = 0;
            while ( nGXsfl_33_fel_idx < nRC_GXsfl_33 )
            {
               nGXsfl_33_fel_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_33_fel_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_33_fel_idx+1));
               sGXsfl_33_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_332( ) ;
               AV33GXV1 = (short)(nGXsfl_33_fel_idx+GRIDITNPRJ_nFirstRecordOnPage);
               if ( ( AV14Sdt_ItnPrj.Count >= AV33GXV1 ) && ( AV33GXV1 > 0 ) )
               {
                  AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1));
               }
            }
            if ( nGXsfl_33_fel_idx == 0 )
            {
               nGXsfl_33_idx = 1;
               sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
               SubsflControlProps_332( ) ;
            }
            nGXsfl_33_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13DU2 */
         E13DU2 ();
         if (returnInSub) return;
      }

      protected void E13DU2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV30WWPContext) ;
         subGriditnprj_Rows = 15;
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         AV14Sdt_ItnPrj.FromXml(AV19WebSession.Get("ItensProjeto"), "");
         gx_BV33 = true;
         AV7Projeto_Sigla = AV29ParmProjeto_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Projeto_Sigla", AV7Projeto_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Projeto_Sigla, "@!"))));
      }

      protected void E14DU2( )
      {
         AV33GXV1 = (short)(nGXsfl_33_idx+GRIDITNPRJ_nFirstRecordOnPage);
         if ( AV14Sdt_ItnPrj.Count >= AV33GXV1 )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1));
         }
         /* Ctlitemnaomens_Click Routine */
         /* Using cursor H00DU3 */
         pr_default.execute(1, new Object[] {AV30WWPContext.gxTpr_Areatrabalho_codigo, ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemnaomens});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A715ItemNaoMensuravel_Codigo = H00DU3_A715ItemNaoMensuravel_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = H00DU3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A719ItemNaoMensuravel_Valor = H00DU3_A719ItemNaoMensuravel_Valor[0];
            A717ItemNaoMensuravel_Tipo = H00DU3_A717ItemNaoMensuravel_Tipo[0];
            ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemvalor = A719ItemNaoMensuravel_Valor;
            ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemtipoinm = A717ItemNaoMensuravel_Tipo;
            if ( A717ItemNaoMensuravel_Tipo == 2 )
            {
               ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itempfl = A719ItemNaoMensuravel_Valor;
            }
            else
            {
               ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itempfl = (decimal)(((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itempfb*A719ItemNaoMensuravel_Valor);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Sdt_ItnPrj", AV14Sdt_ItnPrj);
         nGXsfl_33_bak_idx = nGXsfl_33_idx;
         gxgrGriditnprj_refresh( subGriditnprj_Rows, AV30WWPContext) ;
         nGXsfl_33_idx = nGXsfl_33_bak_idx;
         sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
         SubsflControlProps_332( ) ;
      }

      protected void E11DU2( )
      {
         AV33GXV1 = (short)(nGXsfl_33_idx+GRIDITNPRJ_nFirstRecordOnPage);
         if ( AV14Sdt_ItnPrj.Count >= AV33GXV1 )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1));
         }
         /* 'Enter' Routine */
         AV19WebSession.Set("ItensProjeto", AV14Sdt_ItnPrj.ToXml(false, true, "SDT_ItensConagem", "GxEv3Up14_MeetrikaVs3"));
         AV20Sistema = new SdtSistema(context);
         AV20Sistema.gxTpr_Sistema_areatrabalhocod = AV9AreaTrabalho_Codigo;
         AV20Sistema.gxTpr_Sistema_nome = AV29ParmProjeto_Sigla;
         AV20Sistema.gxTpr_Sistema_sigla = AV29ParmProjeto_Sigla;
         AV20Sistema.gxTpr_Sistema_tipo = AV26Sistema_Tipo;
         AV20Sistema.gxTpr_Sistema_tecnica = AV27Sistema_Tecnica;
         AV20Sistema.gxTpr_Sistema_fatorajuste = AV23Sistema_FatorAjuste;
         AV20Sistema.gxTpr_Sistema_prazo = AV24Sistema_Prazo;
         AV20Sistema.gxTpr_Sistema_custo = AV25Sistema_Custo;
         AV20Sistema.gxTpr_Sistema_esforco = AV22Sistema_Esforco;
         AV20Sistema.gxTv_SdtSistema_Metodologia_codigo_SetNull();
         AV20Sistema.gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull();
         AV20Sistema.Save();
         context.CommitDataStores( "WP_ProjetoMelhoriaObs");
         if ( AV20Sistema.Success() )
         {
            GXt_int1 = AV20Sistema.gxTpr_Sistema_codigo;
            new prc_projetomelhoriains(context ).execute( ref  AV21Projeto_Codigo, ref  GXt_int1,  AV8Sistema_Codigo) ;
            AV20Sistema.gxTpr_Sistema_codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Projeto_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Sistema_Codigo), "ZZZZZ9")));
            context.wjLoc = formatLink("wwsistema.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         else
         {
            context.RollbackDataStores( "WP_ProjetoMelhoriaObs");
         }
      }

      protected void E12DU2( )
      {
         /* 'Voltar' Routine */
         context.setWebReturnParms(new Object[] {(decimal)AV23Sistema_FatorAjuste,(short)AV24Sistema_Prazo,(decimal)AV25Sistema_Custo,(short)AV22Sistema_Esforco,(String)AV26Sistema_Tipo,(String)AV27Sistema_Tecnica});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      private void E15DU2( )
      {
         /* Griditnprj_Load Routine */
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV14Sdt_ItnPrj.Count )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 33;
            }
            if ( ( subGriditnprj_Islastpage == 1 ) || ( subGriditnprj_Rows == 0 ) || ( ( GRIDITNPRJ_nCurrentRecord >= GRIDITNPRJ_nFirstRecordOnPage ) && ( GRIDITNPRJ_nCurrentRecord < GRIDITNPRJ_nFirstRecordOnPage + subGriditnprj_Recordsperpage( ) ) ) )
            {
               sendrow_332( ) ;
               GRIDITNPRJ_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ".", "")));
               if ( GRIDITNPRJ_nCurrentRecord + 1 >= subGriditnprj_Recordcount( ) )
               {
                  GRIDITNPRJ_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDITNPRJ_nCurrentRecord = (long)(GRIDITNPRJ_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_33_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(33, GriditnprjRow);
            }
            AV33GXV1 = (short)(AV33GXV1+1);
         }
      }

      protected void wb_table1_2_DU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain2_Internalname, tblTablemain2_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table2_5_DU2( true) ;
         }
         else
         {
            wb_table2_5_DU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_DU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_16_DU2( true) ;
         }
         else
         {
            wb_table3_16_DU2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_DU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttVoltar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(33), 2, 0)+","+"null"+");", "Voltar", bttVoltar_Jsonclick, 5, "Voltar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'VOLTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirmar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(33), 2, 0)+","+"null"+");", "Confirmar", bttConfirmar_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DU2e( true) ;
         }
         else
         {
            wb_table1_2_DU2e( false) ;
         }
      }

      protected void wb_table3_16_DU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table4_19_DU2( true) ;
         }
         else
         {
            wb_table4_19_DU2( false) ;
         }
         return  ;
      }

      protected void wb_table4_19_DU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TBLPRJContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TBLPRJContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_30_DU2( true) ;
         }
         else
         {
            wb_table5_30_DU2( false) ;
         }
         return  ;
      }

      protected void wb_table5_30_DU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_DU2e( true) ;
         }
         else
         {
            wb_table3_16_DU2e( false) ;
         }
      }

      protected void wb_table5_30_DU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblprj_Internalname, tblTblprj_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GriditnprjContainer.SetWrapped(nGXWrapped);
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GriditnprjContainer"+"DivS\" data-gxgridid=\"33\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGriditnprj_Internalname, subGriditnprj_Internalname, "", "WorkWith", 0, "", "", 2, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGriditnprj_Backcolorstyle == 0 )
               {
                  subGriditnprj_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Title";
                  }
               }
               else
               {
                  subGriditnprj_Titlebackstyle = 1;
                  if ( subGriditnprj_Backcolorstyle == 1 )
                  {
                     subGriditnprj_Titlebackcolor = subGriditnprj_Allbackcolor;
                     if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                     {
                        subGriditnprj_Linesclass = subGriditnprj_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                     {
                        subGriditnprj_Linesclass = subGriditnprj_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Dados/Trn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Melhoria") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "INM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Valor") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Regra") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF.B") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF.L") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
            }
            else
            {
               GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
               GriditnprjContainer.AddObjectProperty("Class", "WorkWith");
               GriditnprjContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Backcolorstyle), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("CmpContext", "");
               GriditnprjContainer.AddObjectProperty("InMasterPage", "false");
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemtipo_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemnome_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitemvalor_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlitemtipoinm.Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitempfb_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitempfl_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowselection), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Selectioncolor), 9, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowhovering), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Hoveringcolor), 9, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowcollapsing), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 33 )
         {
            wbEnd = 0;
            nRC_GXsfl_33 = (short)(nGXsfl_33_idx-1);
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GriditnprjContainer.AddObjectProperty("GRIDITNPRJ_nEOF", GRIDITNPRJ_nEOF);
               GriditnprjContainer.AddObjectProperty("GRIDITNPRJ_nFirstRecordOnPage", GRIDITNPRJ_nFirstRecordOnPage);
               AV33GXV1 = nGXsfl_33_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GriditnprjContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Griditnprj", GriditnprjContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnprjContainerData", GriditnprjContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnprjContainerData"+"V", GriditnprjContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GriditnprjContainerData"+"V"+"\" value='"+GriditnprjContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_30_DU2e( true) ;
         }
         else
         {
            wb_table5_30_DU2e( false) ;
         }
      }

      protected void wb_table4_19_DU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo2_Internalname, "Sistema", "", "", lblTextblocksistema_codigo2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavSistema_nome_Internalname, AV18Sistema_Nome, StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSistema_nome_Enabled, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_19_DU2e( true) ;
         }
         else
         {
            wb_table4_19_DU2e( false) ;
         }
      }

      protected void wb_table2_5_DU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtextblocktitle2_Internalname, tblTablemergedtextblocktitle2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle2_Internalname, "Projeto de Melhoria :: ", "", "", lblTextblocktitle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla_Internalname, StringUtil.RTrim( AV7Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( AV7Projeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavProjeto_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ProjetoMelhoriaObs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_DU2e( true) ;
         }
         else
         {
            wb_table2_5_DU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV21Projeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Projeto_Codigo), 6, 0)));
         AV8Sistema_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Sistema_Codigo), "ZZZZZ9")));
         AV9AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         AV29ParmProjeto_Sigla = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ParmProjeto_Sigla", AV29ParmProjeto_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29ParmProjeto_Sigla, "@!"))));
         AV18Sistema_Nome = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Nome", AV18Sistema_Nome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18Sistema_Nome, "@!"))));
         AV23Sistema_FatorAjuste = (decimal)(Convert.ToDecimal((decimal)getParm(obj,5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV23Sistema_FatorAjuste, 6, 2)));
         AV24Sistema_Prazo = Convert.ToInt16(getParm(obj,6));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Prazo), 4, 0)));
         AV25Sistema_Custo = (decimal)(Convert.ToDecimal((decimal)getParm(obj,7)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV25Sistema_Custo, 18, 5)));
         AV22Sistema_Esforco = Convert.ToInt16(getParm(obj,8));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Sistema_Esforco), 4, 0)));
         AV26Sistema_Tipo = (String)getParm(obj,9);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Sistema_Tipo", AV26Sistema_Tipo);
         AV27Sistema_Tecnica = (String)getParm(obj,10);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sistema_Tecnica", AV27Sistema_Tecnica);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADU2( ) ;
         WSDU2( ) ;
         WEDU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548491198");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_projetomelhoriaobs.js", "?2020548491199");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_332( )
      {
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO_"+sGXsfl_33_idx;
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO_"+sGXsfl_33_idx;
         edtavPrjitemnome_Internalname = "PRJITEMNOME_"+sGXsfl_33_idx;
         edtavCtlitemobservacao_Internalname = "CTLITEMOBSERVACAO_"+sGXsfl_33_idx;
         dynavCtlitemnaomens_Internalname = "CTLITEMNAOMENS_"+sGXsfl_33_idx;
         edtavCtlitemvalor_Internalname = "CTLITEMVALOR_"+sGXsfl_33_idx;
         cmbavCtlitemtipoinm_Internalname = "CTLITEMTIPOINM_"+sGXsfl_33_idx;
         edtavCtlitempfb_Internalname = "CTLITEMPFB_"+sGXsfl_33_idx;
         edtavCtlitempfl_Internalname = "CTLITEMPFL_"+sGXsfl_33_idx;
      }

      protected void SubsflControlProps_fel_332( )
      {
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO_"+sGXsfl_33_fel_idx;
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO_"+sGXsfl_33_fel_idx;
         edtavPrjitemnome_Internalname = "PRJITEMNOME_"+sGXsfl_33_fel_idx;
         edtavCtlitemobservacao_Internalname = "CTLITEMOBSERVACAO_"+sGXsfl_33_fel_idx;
         dynavCtlitemnaomens_Internalname = "CTLITEMNAOMENS_"+sGXsfl_33_fel_idx;
         edtavCtlitemvalor_Internalname = "CTLITEMVALOR_"+sGXsfl_33_fel_idx;
         cmbavCtlitemtipoinm_Internalname = "CTLITEMTIPOINM_"+sGXsfl_33_fel_idx;
         edtavCtlitempfb_Internalname = "CTLITEMPFB_"+sGXsfl_33_fel_idx;
         edtavCtlitempfl_Internalname = "CTLITEMPFL_"+sGXsfl_33_fel_idx;
      }

      protected void sendrow_332( )
      {
         SubsflControlProps_332( ) ;
         WBDU0( ) ;
         if ( ( subGriditnprj_Rows * 1 == 0 ) || ( nGXsfl_33_idx <= subGriditnprj_Recordsperpage( ) * 1 ) )
         {
            GriditnprjRow = GXWebRow.GetNew(context,GriditnprjContainer);
            if ( subGriditnprj_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGriditnprj_Backstyle = 0;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
               }
            }
            else if ( subGriditnprj_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGriditnprj_Backstyle = 0;
               subGriditnprj_Backcolor = subGriditnprj_Allbackcolor;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Uniform";
               }
            }
            else if ( subGriditnprj_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGriditnprj_Backstyle = 1;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
               }
               subGriditnprj_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGriditnprj_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGriditnprj_Backstyle = 1;
               if ( ((int)((nGXsfl_33_idx) % (2))) == 0 )
               {
                  subGriditnprj_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Even";
                  }
               }
               else
               {
                  subGriditnprj_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
                  }
               }
            }
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGriditnprj_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_33_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemcodigo), 6, 0, ",", "")),((edtavPrjitemcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemcodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemcodigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPrjitemcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemtipo_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemtipo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPrjitemtipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemnome_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemnome),StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemnome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemnome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPrjitemnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Multiple line edit */
            TempTags = " " + ((edtavCtlitemobservacao_Enabled!=0)&&(edtavCtlitemobservacao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 37,'',false,'"+sGXsfl_33_idx+"',33)\"" : " ");
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GriditnprjRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemobservacao_Internalname,((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemobservacao,(String)"",TempTags+((edtavCtlitemobservacao_Enabled!=0)&&(edtavCtlitemobservacao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlitemobservacao_Enabled!=0)&&(edtavCtlitemobservacao_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,37);\"" : " "),(short)0,(short)-1,(short)1,(short)0,(short)570,(String)"px",(short)48,(String)"px",(String)StyleString,(String)ClassString,(String)"",(String)"500",(short)1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
            GXVCTLITEMNAOMENS_htmlDU2( AV30WWPContext) ;
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((dynavCtlitemnaomens.Enabled!=0)&&(dynavCtlitemnaomens.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 38,'',false,'"+sGXsfl_33_idx+"',33)\"" : " ");
            if ( ( nGXsfl_33_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CTLITEMNAOMENS_" + sGXsfl_33_idx;
               dynavCtlitemnaomens.Name = GXCCtl;
               dynavCtlitemnaomens.WebTags = "";
            }
            /* ComboBox */
            GriditnprjRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynavCtlitemnaomens,(String)dynavCtlitemnaomens_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemnaomens),(short)1,(String)dynavCtlitemnaomens_Jsonclick,(short)5,"'"+""+"'"+",false,"+"'"+"ECTLITEMNAOMENS.CLICK."+sGXsfl_33_idx+"'",(String)"char",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((dynavCtlitemnaomens.Enabled!=0)&&(dynavCtlitemnaomens.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((dynavCtlitemnaomens.Enabled!=0)&&(dynavCtlitemnaomens.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,38);\"" : " "),(String)"",(bool)true});
            dynavCtlitemnaomens.CurrentValue = StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemnaomens);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCtlitemnaomens_Internalname, "Values", (String)(dynavCtlitemnaomens.ToJavascriptSource()));
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemvalor_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemvalor, 18, 5, ",", "")),((edtavCtlitemvalor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemvalor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemvalor, "ZZZ,ZZZ,ZZZ,ZZ9.99")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitemvalor_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlitemvalor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_33_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CTLITEMTIPOINM_" + sGXsfl_33_idx;
               cmbavCtlitemtipoinm.Name = GXCCtl;
               cmbavCtlitemtipoinm.WebTags = "";
               cmbavCtlitemtipoinm.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
               cmbavCtlitemtipoinm.addItem("1", "PC", 0);
               cmbavCtlitemtipoinm.addItem("2", "PF", 0);
               if ( cmbavCtlitemtipoinm.ItemCount > 0 )
               {
                  if ( ( AV33GXV1 > 0 ) && ( AV14Sdt_ItnPrj.Count >= AV33GXV1 ) && (0==((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm) )
                  {
                     ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm = (short)(NumberUtil.Val( cmbavCtlitemtipoinm.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm), 2, 0))), "."));
                  }
               }
            }
            /* ComboBox */
            GriditnprjRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlitemtipoinm,(String)cmbavCtlitemtipoinm_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm), 2, 0)),(short)1,(String)cmbavCtlitemtipoinm_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavCtlitemtipoinm.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavCtlitemtipoinm.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itemtipoinm), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlitemtipoinm_Internalname, "Values", (String)(cmbavCtlitemtipoinm.ToJavascriptSource()));
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitempfb_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfb, 14, 5, ",", "")),((edtavCtlitempfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfb, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfb, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitempfb_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlitempfb_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitempfl_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfl, 14, 5, ",", "")),((edtavCtlitempfl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfl, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV33GXV1)).gxTpr_Itempfl, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitempfl_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlitempfl_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GriditnprjContainer.AddRow(GriditnprjRow);
            nGXsfl_33_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_33_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         /* End function sendrow_332 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle2_Internalname = "TEXTBLOCKTITLE2";
         edtavProjeto_sigla_Internalname = "vPROJETO_SIGLA";
         tblTablemergedtextblocktitle2_Internalname = "TABLEMERGEDTEXTBLOCKTITLE2";
         lblTextblocksistema_codigo2_Internalname = "TEXTBLOCKSISTEMA_CODIGO2";
         edtavSistema_nome_Internalname = "vSISTEMA_NOME";
         tblTable2_Internalname = "TABLE2";
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO";
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO";
         edtavPrjitemnome_Internalname = "PRJITEMNOME";
         edtavCtlitemobservacao_Internalname = "CTLITEMOBSERVACAO";
         dynavCtlitemnaomens_Internalname = "CTLITEMNAOMENS";
         edtavCtlitemvalor_Internalname = "CTLITEMVALOR";
         cmbavCtlitemtipoinm_Internalname = "CTLITEMTIPOINM";
         edtavCtlitempfb_Internalname = "CTLITEMPFB";
         edtavCtlitempfl_Internalname = "CTLITEMPFL";
         tblTblprj_Internalname = "TBLPRJ";
         Dvpanel_tblprj_Internalname = "DVPANEL_TBLPRJ";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttVoltar_Internalname = "VOLTAR";
         bttConfirmar_Internalname = "CONFIRMAR";
         tblTablemain2_Internalname = "TABLEMAIN2";
         Form.Internalname = "FORM";
         subGriditnprj_Internalname = "GRIDITNPRJ";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlitempfl_Jsonclick = "";
         edtavCtlitempfb_Jsonclick = "";
         cmbavCtlitemtipoinm_Jsonclick = "";
         edtavCtlitemvalor_Jsonclick = "";
         dynavCtlitemnaomens_Jsonclick = "";
         dynavCtlitemnaomens.Visible = -1;
         dynavCtlitemnaomens.Enabled = 1;
         edtavCtlitemobservacao_Visible = -1;
         edtavCtlitemobservacao_Enabled = 1;
         edtavPrjitemnome_Jsonclick = "";
         edtavPrjitemtipo_Jsonclick = "";
         edtavPrjitemcodigo_Jsonclick = "";
         edtavProjeto_sigla_Jsonclick = "";
         edtavProjeto_sigla_Enabled = 1;
         edtavSistema_nome_Jsonclick = "";
         edtavSistema_nome_Enabled = 0;
         subGriditnprj_Allowcollapsing = 0;
         subGriditnprj_Allowselection = 0;
         edtavCtlitempfl_Enabled = 0;
         edtavCtlitempfb_Enabled = 0;
         cmbavCtlitemtipoinm.Enabled = 0;
         edtavCtlitemvalor_Enabled = 0;
         edtavPrjitemnome_Enabled = 0;
         edtavPrjitemtipo_Enabled = 0;
         edtavPrjitemcodigo_Enabled = 0;
         subGriditnprj_Class = "WorkWith";
         subGriditnprj_Backcolorstyle = 3;
         edtavCtlitempfl_Enabled = -1;
         edtavCtlitempfb_Enabled = -1;
         cmbavCtlitemtipoinm.Enabled = -1;
         edtavCtlitemvalor_Enabled = -1;
         edtavPrjitemnome_Enabled = -1;
         edtavPrjitemtipo_Enabled = -1;
         edtavPrjitemcodigo_Enabled = -1;
         Dvpanel_tblprj_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Iconposition = "left";
         Dvpanel_tblprj_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tblprj_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tblprj_Title = "Itens do Projeto";
         Dvpanel_tblprj_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tblprj_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto Melhoria Observacoes";
         subGriditnprj_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("CTLITEMNAOMENS.CLICK","{handler:'E14DU2',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'A719ItemNaoMensuravel_Valor',fld:'ITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A717ItemNaoMensuravel_Tipo',fld:'ITEMNAOMENSURAVEL_TIPO',pic:'Z9',nv:0}],oparms:[{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null}]}");
         setEventMetadata("'ENTER'","{handler:'E11DU2',iparms:[{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null},{av:'AV9AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV29ParmProjeto_Sigla',fld:'vPARMPROJETO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'AV26Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV27Sistema_Tecnica',fld:'vSISTEMA_TECNICA',pic:'',nv:''},{av:'AV23Sistema_FatorAjuste',fld:'vSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV24Sistema_Prazo',fld:'vSISTEMA_PRAZO',pic:'ZZZ9',nv:0},{av:'AV25Sistema_Custo',fld:'vSISTEMA_CUSTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV22Sistema_Esforco',fld:'vSISTEMA_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV21Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV21Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'VOLTAR'","{handler:'E12DU2',iparms:[{av:'AV27Sistema_Tecnica',fld:'vSISTEMA_TECNICA',pic:'',nv:''},{av:'AV26Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV22Sistema_Esforco',fld:'vSISTEMA_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV25Sistema_Custo',fld:'vSISTEMA_CUSTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV24Sistema_Prazo',fld:'vSISTEMA_PRAZO',pic:'ZZZ9',nv:0},{av:'AV23Sistema_FatorAjuste',fld:'vSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_FIRSTPAGE","{handler:'subgriditnprj_firstpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_PREVPAGE","{handler:'subgriditnprj_previouspage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_NEXTPAGE","{handler:'subgriditnprj_nextpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_LASTPAGE","{handler:'subgriditnprj_lastpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:33,pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV29ParmProjeto_Sigla = "";
         wcpOAV18Sistema_Nome = "";
         wcpOAV26Sistema_Tipo = "";
         wcpOAV27Sistema_Tecnica = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV30WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV14Sdt_ItnPrj = new GxObjectCollection( context, "SDT_ItensConagem.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ItensConagem_Item", "GeneXus.Programs");
         A715ItemNaoMensuravel_Codigo = "";
         AV7Projeto_Sigla = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00DU2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00DU2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         GriditnprjContainer = new GXWebGrid( context);
         AV19WebSession = context.GetSession();
         H00DU3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00DU3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00DU3_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         H00DU3_A717ItemNaoMensuravel_Tipo = new short[1] ;
         AV20Sistema = new SdtSistema(context);
         GriditnprjRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttVoltar_Jsonclick = "";
         bttConfirmar_Jsonclick = "";
         subGriditnprj_Linesclass = "";
         GriditnprjColumn = new GXWebColumn();
         lblTextblocksistema_codigo2_Jsonclick = "";
         lblTextblocktitle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_projetomelhoriaobs__default(),
            new Object[][] {
                new Object[] {
               H00DU2_A715ItemNaoMensuravel_Codigo, H00DU2_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               H00DU3_A715ItemNaoMensuravel_Codigo, H00DU3_A718ItemNaoMensuravel_AreaTrabalhoCod, H00DU3_A719ItemNaoMensuravel_Valor, H00DU3_A717ItemNaoMensuravel_Tipo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         edtavSistema_nome_Enabled = 0;
         edtavPrjitemcodigo_Enabled = 0;
         edtavPrjitemtipo_Enabled = 0;
         edtavPrjitemnome_Enabled = 0;
         edtavCtlitemvalor_Enabled = 0;
         cmbavCtlitemtipoinm.Enabled = 0;
         edtavCtlitempfb_Enabled = 0;
         edtavCtlitempfl_Enabled = 0;
      }

      private short AV24Sistema_Prazo ;
      private short AV22Sistema_Esforco ;
      private short wcpOAV24Sistema_Prazo ;
      private short wcpOAV22Sistema_Esforco ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_33 ;
      private short nGXsfl_33_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDITNPRJ_nEOF ;
      private short A717ItemNaoMensuravel_Tipo ;
      private short wbEnd ;
      private short wbStart ;
      private short AV33GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_33_Refreshing=0 ;
      private short subGriditnprj_Backcolorstyle ;
      private short nGXsfl_33_fel_idx=1 ;
      private short nGXsfl_33_bak_idx=1 ;
      private short subGriditnprj_Titlebackstyle ;
      private short subGriditnprj_Allowselection ;
      private short subGriditnprj_Allowhovering ;
      private short subGriditnprj_Allowcollapsing ;
      private short subGriditnprj_Collapsed ;
      private short nGXWrapped ;
      private short subGriditnprj_Backstyle ;
      private int AV21Projeto_Codigo ;
      private int AV8Sistema_Codigo ;
      private int AV9AreaTrabalho_Codigo ;
      private int wcpOAV21Projeto_Codigo ;
      private int wcpOAV8Sistema_Codigo ;
      private int wcpOAV9AreaTrabalho_Codigo ;
      private int subGriditnprj_Rows ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int gxdynajaxindex ;
      private int subGriditnprj_Islastpage ;
      private int edtavProjeto_sigla_Enabled ;
      private int edtavSistema_nome_Enabled ;
      private int edtavPrjitemcodigo_Enabled ;
      private int edtavPrjitemtipo_Enabled ;
      private int edtavPrjitemnome_Enabled ;
      private int edtavCtlitemvalor_Enabled ;
      private int edtavCtlitempfb_Enabled ;
      private int edtavCtlitempfl_Enabled ;
      private int GRIDITNPRJ_nGridOutOfScope ;
      private int GXt_int1 ;
      private int subGriditnprj_Titlebackcolor ;
      private int subGriditnprj_Allbackcolor ;
      private int subGriditnprj_Selectioncolor ;
      private int subGriditnprj_Hoveringcolor ;
      private int idxLst ;
      private int subGriditnprj_Backcolor ;
      private int edtavCtlitemobservacao_Enabled ;
      private int edtavCtlitemobservacao_Visible ;
      private long GRIDITNPRJ_nFirstRecordOnPage ;
      private long GRIDITNPRJ_nCurrentRecord ;
      private long GRIDITNPRJ_nRecordCount ;
      private decimal AV23Sistema_FatorAjuste ;
      private decimal AV25Sistema_Custo ;
      private decimal wcpOAV23Sistema_FatorAjuste ;
      private decimal wcpOAV25Sistema_Custo ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String AV29ParmProjeto_Sigla ;
      private String AV26Sistema_Tipo ;
      private String AV27Sistema_Tecnica ;
      private String wcpOAV29ParmProjeto_Sigla ;
      private String wcpOAV26Sistema_Tipo ;
      private String wcpOAV27Sistema_Tecnica ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_33_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String AV7Projeto_Sigla ;
      private String Dvpanel_tblprj_Width ;
      private String Dvpanel_tblprj_Cls ;
      private String Dvpanel_tblprj_Title ;
      private String Dvpanel_tblprj_Iconposition ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String edtavProjeto_sigla_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavSistema_nome_Internalname ;
      private String edtavPrjitemcodigo_Internalname ;
      private String edtavPrjitemtipo_Internalname ;
      private String edtavPrjitemnome_Internalname ;
      private String edtavCtlitemvalor_Internalname ;
      private String cmbavCtlitemtipoinm_Internalname ;
      private String edtavCtlitempfb_Internalname ;
      private String edtavCtlitempfl_Internalname ;
      private String sGXsfl_33_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTablemain2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttVoltar_Internalname ;
      private String bttVoltar_Jsonclick ;
      private String bttConfirmar_Internalname ;
      private String bttConfirmar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTblprj_Internalname ;
      private String subGriditnprj_Internalname ;
      private String subGriditnprj_Class ;
      private String subGriditnprj_Linesclass ;
      private String tblTable2_Internalname ;
      private String lblTextblocksistema_codigo2_Internalname ;
      private String lblTextblocksistema_codigo2_Jsonclick ;
      private String edtavSistema_nome_Jsonclick ;
      private String tblTablemergedtextblocktitle2_Internalname ;
      private String lblTextblocktitle2_Internalname ;
      private String lblTextblocktitle2_Jsonclick ;
      private String edtavProjeto_sigla_Jsonclick ;
      private String edtavCtlitemobservacao_Internalname ;
      private String dynavCtlitemnaomens_Internalname ;
      private String ROClassString ;
      private String edtavPrjitemcodigo_Jsonclick ;
      private String edtavPrjitemtipo_Jsonclick ;
      private String edtavPrjitemnome_Jsonclick ;
      private String dynavCtlitemnaomens_Jsonclick ;
      private String edtavCtlitemvalor_Jsonclick ;
      private String cmbavCtlitemtipoinm_Jsonclick ;
      private String edtavCtlitempfb_Jsonclick ;
      private String edtavCtlitempfl_Jsonclick ;
      private String Dvpanel_tblprj_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Dvpanel_tblprj_Collapsible ;
      private bool Dvpanel_tblprj_Collapsed ;
      private bool Dvpanel_tblprj_Autowidth ;
      private bool Dvpanel_tblprj_Autoheight ;
      private bool Dvpanel_tblprj_Showcollapseicon ;
      private bool Dvpanel_tblprj_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_BV33 ;
      private String AV18Sistema_Nome ;
      private String wcpOAV18Sistema_Nome ;
      private IGxSession AV19WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GriditnprjContainer ;
      private GXWebRow GriditnprjRow ;
      private GXWebColumn GriditnprjColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private decimal aP5_Sistema_FatorAjuste ;
      private short aP6_Sistema_Prazo ;
      private decimal aP7_Sistema_Custo ;
      private short aP8_Sistema_Esforco ;
      private String aP9_Sistema_Tipo ;
      private String aP10_Sistema_Tecnica ;
      private GXCombobox dynavCtlitemnaomens ;
      private GXCombobox cmbavCtlitemtipoinm ;
      private IDataStoreProvider pr_default ;
      private String[] H00DU2_A715ItemNaoMensuravel_Codigo ;
      private int[] H00DU2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] H00DU3_A715ItemNaoMensuravel_Codigo ;
      private int[] H00DU3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private decimal[] H00DU3_A719ItemNaoMensuravel_Valor ;
      private short[] H00DU3_A717ItemNaoMensuravel_Tipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ItensConagem_Item ))]
      private IGxCollection AV14Sdt_ItnPrj ;
      private GXWebForm Form ;
      private SdtSistema AV20Sistema ;
      private wwpbaseobjects.SdtWWPContext AV30WWPContext ;
   }

   public class wp_projetomelhoriaobs__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DU2 ;
          prmH00DU2 = new Object[] {
          } ;
          Object[] prmH00DU3 ;
          prmH00DU3 = new Object[] {
          new Object[] {"@AV30WWPC_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SdtSDTCurrentItem_1Itemna",SqlDbType.Char,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DU2", "SELECT [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod] FROM [ItemNaoMensuravel] WITH (NOLOCK) ORDER BY [ItemNaoMensuravel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DU2,0,0,true,false )
             ,new CursorDef("H00DU3", "SELECT TOP 1 [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Tipo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @AV30WWPC_2Areatrabalho_codigo and [ItemNaoMensuravel_Codigo] = @SdtSDTCurrentItem_1Itemna ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DU3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
