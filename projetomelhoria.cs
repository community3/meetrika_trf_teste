/*
               File: ProjetoMelhoria
        Description: Projeto Melhoria
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:27.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetomelhoria : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETOMELHORIA_FNDADOSCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETOMELHORIA_FNDADOSCOD2B93( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A694ProjetoMelhoria_ProjetoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A695ProjetoMelhoria_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A695ProjetoMelhoria_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n697ProjetoMelhoria_FnDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A697ProjetoMelhoria_FnDadosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A698ProjetoMelhoria_FnAPFCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n698ProjetoMelhoria_FnAPFCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A698ProjetoMelhoria_FnAPFCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynProjetoMelhoria_ProjetoCod.Name = "PROJETOMELHORIA_PROJETOCOD";
         dynProjetoMelhoria_ProjetoCod.WebTags = "";
         dynProjetoMelhoria_ProjetoCod.removeAllItems();
         /* Using cursor T002B8 */
         pr_default.execute(6);
         while ( (pr_default.getStatus(6) != 101) )
         {
            dynProjetoMelhoria_ProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T002B8_A648Projeto_Codigo[0]), 6, 0)), T002B8_A650Projeto_Sigla[0], 0);
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( dynProjetoMelhoria_ProjetoCod.ItemCount > 0 )
         {
            A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( dynProjetoMelhoria_ProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
         }
         dynProjetoMelhoria_FnDadosCod.Name = "PROJETOMELHORIA_FNDADOSCOD";
         dynProjetoMelhoria_FnDadosCod.WebTags = "";
         cmbProjetomelhoria_Tipo.Name = "PROJETOMELHORIA_TIPO";
         cmbProjetomelhoria_Tipo.WebTags = "";
         cmbProjetomelhoria_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
         cmbProjetomelhoria_Tipo.addItem("1", "PC", 0);
         cmbProjetomelhoria_Tipo.addItem("2", "PF", 0);
         if ( cmbProjetomelhoria_Tipo.ItemCount > 0 )
         {
            A732Projetomelhoria_Tipo = (short)(NumberUtil.Val( cmbProjetomelhoria_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0))), "."));
            n732Projetomelhoria_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto Melhoria", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public projetomelhoria( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projetomelhoria( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynProjetoMelhoria_ProjetoCod = new GXCombobox();
         dynProjetoMelhoria_FnDadosCod = new GXCombobox();
         cmbProjetomelhoria_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynProjetoMelhoria_ProjetoCod.ItemCount > 0 )
         {
            A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( dynProjetoMelhoria_ProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
         }
         if ( dynProjetoMelhoria_FnDadosCod.ItemCount > 0 )
         {
            A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( dynProjetoMelhoria_FnDadosCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0))), "."));
            n697ProjetoMelhoria_FnDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
         }
         if ( cmbProjetomelhoria_Tipo.ItemCount > 0 )
         {
            A732Projetomelhoria_Tipo = (short)(NumberUtil.Val( cmbProjetomelhoria_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0))), "."));
            n732Projetomelhoria_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2B93( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2B93e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2B93( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2B93( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2B93e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Projeto Melhoria", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ProjetoMelhoria.htm");
            wb_table3_28_2B93( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2B93e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2B93e( true) ;
         }
         else
         {
            wb_table1_2_2B93e( false) ;
         }
      }

      protected void wb_table3_28_2B93( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2B93( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2B93e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoMelhoria.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoMelhoria.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2B93e( true) ;
         }
         else
         {
            wb_table3_28_2B93e( false) ;
         }
      }

      protected void wb_table4_34_2B93( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_codigo_Internalname, "Melhoria_Codigo", "", "", lblTextblockprojetomelhoria_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0, ",", "")), ((edtProjetoMelhoria_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A736ProjetoMelhoria_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A736ProjetoMelhoria_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_projetocod_Internalname, "Sigla", "", "", lblTextblockprojetomelhoria_projetocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjetoMelhoria_ProjetoCod, dynProjetoMelhoria_ProjetoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)), 1, dynProjetoMelhoria_ProjetoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjetoMelhoria_ProjetoCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_ProjetoMelhoria.htm");
            dynProjetoMelhoria_ProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjetoMelhoria_ProjetoCod_Internalname, "Values", (String)(dynProjetoMelhoria_ProjetoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_sistemacod_Internalname, "Sistema", "", "", lblTextblockprojetomelhoria_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0, ",", "")), ((edtProjetoMelhoria_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A695ProjetoMelhoria_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A695ProjetoMelhoria_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_fndadoscod_Internalname, "Dados", "", "", lblTextblockprojetomelhoria_fndadoscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjetoMelhoria_FnDadosCod, dynProjetoMelhoria_FnDadosCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)), 1, dynProjetoMelhoria_FnDadosCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjetoMelhoria_FnDadosCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_ProjetoMelhoria.htm");
            dynProjetoMelhoria_FnDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjetoMelhoria_FnDadosCod_Internalname, "Values", (String)(dynProjetoMelhoria_FnDadosCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_nome_Internalname, "Fun��o Dados", "", "", lblTextblockfuncaodados_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Nome_Internalname, A369FuncaoDados_Nome, "", "", 0, 1, edtFuncaoDados_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_fnapfcod_Internalname, "de Transa��o", "", "", lblTextblockprojetomelhoria_fnapfcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_FnAPFCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0, ",", "")), ((edtProjetoMelhoria_FnAPFCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A698ProjetoMelhoria_FnAPFCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A698ProjetoMelhoria_FnAPFCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_FnAPFCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_FnAPFCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Fun��o de Transa��o", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, edtFuncaoAPF_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_fnnome_Internalname, "Item", "", "", lblTextblockprojetomelhoria_fnnome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_FnNome_Internalname, StringUtil.RTrim( A701ProjetoMelhoria_FnNome), StringUtil.RTrim( context.localUtil.Format( A701ProjetoMelhoria_FnNome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_FnNome_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_FnNome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_observacao_Internalname, "Observa��o", "", "", lblTextblockprojetomelhoria_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjetoMelhoria_Observacao_Internalname, A704ProjetoMelhoria_Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", 0, 1, edtProjetoMelhoria_Observacao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_inm_Internalname, "INM", "", "", lblTextblockprojetomelhoria_inm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_INM_Internalname, StringUtil.RTrim( A730ProjetoMelhoria_INM), StringUtil.RTrim( context.localUtil.Format( A730ProjetoMelhoria_INM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_INM_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_INM_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "CodigoINM", "left", true, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_valor_Internalname, "Valor", "", "", lblTextblockprojetomelhoria_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A731ProjetoMelhoria_Valor, 18, 5, ",", "")), ((edtProjetoMelhoria_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A731ProjetoMelhoria_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A731ProjetoMelhoria_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_Valor_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_tipo_Internalname, "Tipo", "", "", lblTextblockprojetomelhoria_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjetomelhoria_Tipo, cmbProjetomelhoria_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)), 1, cmbProjetomelhoria_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbProjetomelhoria_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "", true, "HLP_ProjetoMelhoria.htm");
            cmbProjetomelhoria_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjetomelhoria_Tipo_Internalname, "Values", (String)(cmbProjetomelhoria_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_pfb_Internalname, "PFB", "", "", lblTextblockprojetomelhoria_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A733ProjetoMelhoria_PFB, 14, 5, ",", "")), ((edtProjetoMelhoria_PFB_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A733ProjetoMelhoria_PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A733ProjetoMelhoria_PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_PFB_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_PFB_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetomelhoria_pfl_Internalname, "PFL", "", "", lblTextblockprojetomelhoria_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoMelhoria_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A734ProjetoMelhoria_PFL, 14, 5, ",", "")), ((edtProjetoMelhoria_PFL_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A734ProjetoMelhoria_PFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A734ProjetoMelhoria_PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoMelhoria_PFL_Jsonclick, 0, "Attribute", "", "", "", 1, edtProjetoMelhoria_PFL_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ProjetoMelhoria.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2B93e( true) ;
         }
         else
         {
            wb_table4_34_2B93e( false) ;
         }
      }

      protected void wb_table2_5_2B93( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoMelhoria.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2B93e( true) ;
         }
         else
         {
            wb_table2_5_2B93e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A736ProjetoMelhoria_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
               }
               else
               {
                  A736ProjetoMelhoria_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
               }
               dynProjetoMelhoria_ProjetoCod.CurrentValue = cgiGet( dynProjetoMelhoria_ProjetoCod_Internalname);
               A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( cgiGet( dynProjetoMelhoria_ProjetoCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A695ProjetoMelhoria_SistemaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
               }
               else
               {
                  A695ProjetoMelhoria_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoMelhoria_SistemaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
               }
               dynProjetoMelhoria_FnDadosCod.CurrentValue = cgiGet( dynProjetoMelhoria_FnDadosCod_Internalname);
               A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( cgiGet( dynProjetoMelhoria_FnDadosCod_Internalname), "."));
               n697ProjetoMelhoria_FnDadosCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
               n697ProjetoMelhoria_FnDadosCod = ((0==A697ProjetoMelhoria_FnDadosCod) ? true : false);
               A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
               n369FuncaoDados_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_FnAPFCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_FnAPFCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_FNAPFCOD");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_FnAPFCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A698ProjetoMelhoria_FnAPFCod = 0;
                  n698ProjetoMelhoria_FnAPFCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
               }
               else
               {
                  A698ProjetoMelhoria_FnAPFCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoMelhoria_FnAPFCod_Internalname), ",", "."));
                  n698ProjetoMelhoria_FnAPFCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
               }
               n698ProjetoMelhoria_FnAPFCod = ((0==A698ProjetoMelhoria_FnAPFCod) ? true : false);
               A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
               n166FuncaoAPF_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A701ProjetoMelhoria_FnNome = StringUtil.Upper( cgiGet( edtProjetoMelhoria_FnNome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
               A704ProjetoMelhoria_Observacao = cgiGet( edtProjetoMelhoria_Observacao_Internalname);
               n704ProjetoMelhoria_Observacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A704ProjetoMelhoria_Observacao", A704ProjetoMelhoria_Observacao);
               n704ProjetoMelhoria_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A704ProjetoMelhoria_Observacao)) ? true : false);
               A730ProjetoMelhoria_INM = StringUtil.Upper( cgiGet( edtProjetoMelhoria_INM_Internalname));
               n730ProjetoMelhoria_INM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A730ProjetoMelhoria_INM", A730ProjetoMelhoria_INM);
               n730ProjetoMelhoria_INM = (String.IsNullOrEmpty(StringUtil.RTrim( A730ProjetoMelhoria_INM)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A731ProjetoMelhoria_Valor = 0;
                  n731ProjetoMelhoria_Valor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.Str( A731ProjetoMelhoria_Valor, 18, 5)));
               }
               else
               {
                  A731ProjetoMelhoria_Valor = context.localUtil.CToN( cgiGet( edtProjetoMelhoria_Valor_Internalname), ",", ".");
                  n731ProjetoMelhoria_Valor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.Str( A731ProjetoMelhoria_Valor, 18, 5)));
               }
               n731ProjetoMelhoria_Valor = ((Convert.ToDecimal(0)==A731ProjetoMelhoria_Valor) ? true : false);
               cmbProjetomelhoria_Tipo.CurrentValue = cgiGet( cmbProjetomelhoria_Tipo_Internalname);
               A732Projetomelhoria_Tipo = (short)(NumberUtil.Val( cgiGet( cmbProjetomelhoria_Tipo_Internalname), "."));
               n732Projetomelhoria_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
               n732Projetomelhoria_Tipo = ((0==A732Projetomelhoria_Tipo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFB_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFB_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_PFB");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A733ProjetoMelhoria_PFB = 0;
                  n733ProjetoMelhoria_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.Str( A733ProjetoMelhoria_PFB, 14, 5)));
               }
               else
               {
                  A733ProjetoMelhoria_PFB = context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFB_Internalname), ",", ".");
                  n733ProjetoMelhoria_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.Str( A733ProjetoMelhoria_PFB, 14, 5)));
               }
               n733ProjetoMelhoria_PFB = ((Convert.ToDecimal(0)==A733ProjetoMelhoria_PFB) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFL_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFL_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETOMELHORIA_PFL");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_PFL_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A734ProjetoMelhoria_PFL = 0;
                  n734ProjetoMelhoria_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.Str( A734ProjetoMelhoria_PFL, 14, 5)));
               }
               else
               {
                  A734ProjetoMelhoria_PFL = context.localUtil.CToN( cgiGet( edtProjetoMelhoria_PFL_Internalname), ",", ".");
                  n734ProjetoMelhoria_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.Str( A734ProjetoMelhoria_PFL, 14, 5)));
               }
               n734ProjetoMelhoria_PFL = ((Convert.ToDecimal(0)==A734ProjetoMelhoria_PFL) ? true : false);
               /* Read saved values. */
               Z736ProjetoMelhoria_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z736ProjetoMelhoria_Codigo"), ",", "."));
               Z730ProjetoMelhoria_INM = cgiGet( "Z730ProjetoMelhoria_INM");
               n730ProjetoMelhoria_INM = (String.IsNullOrEmpty(StringUtil.RTrim( A730ProjetoMelhoria_INM)) ? true : false);
               Z731ProjetoMelhoria_Valor = context.localUtil.CToN( cgiGet( "Z731ProjetoMelhoria_Valor"), ",", ".");
               n731ProjetoMelhoria_Valor = ((Convert.ToDecimal(0)==A731ProjetoMelhoria_Valor) ? true : false);
               Z732Projetomelhoria_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z732Projetomelhoria_Tipo"), ",", "."));
               n732Projetomelhoria_Tipo = ((0==A732Projetomelhoria_Tipo) ? true : false);
               Z733ProjetoMelhoria_PFB = context.localUtil.CToN( cgiGet( "Z733ProjetoMelhoria_PFB"), ",", ".");
               n733ProjetoMelhoria_PFB = ((Convert.ToDecimal(0)==A733ProjetoMelhoria_PFB) ? true : false);
               Z734ProjetoMelhoria_PFL = context.localUtil.CToN( cgiGet( "Z734ProjetoMelhoria_PFL"), ",", ".");
               n734ProjetoMelhoria_PFL = ((Convert.ToDecimal(0)==A734ProjetoMelhoria_PFL) ? true : false);
               Z694ProjetoMelhoria_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z694ProjetoMelhoria_ProjetoCod"), ",", "."));
               Z695ProjetoMelhoria_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z695ProjetoMelhoria_SistemaCod"), ",", "."));
               Z697ProjetoMelhoria_FnDadosCod = (int)(context.localUtil.CToN( cgiGet( "Z697ProjetoMelhoria_FnDadosCod"), ",", "."));
               n697ProjetoMelhoria_FnDadosCod = ((0==A697ProjetoMelhoria_FnDadosCod) ? true : false);
               Z698ProjetoMelhoria_FnAPFCod = (int)(context.localUtil.CToN( cgiGet( "Z698ProjetoMelhoria_FnAPFCod"), ",", "."));
               n698ProjetoMelhoria_FnAPFCod = ((0==A698ProjetoMelhoria_FnAPFCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A736ProjetoMelhoria_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2B93( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2B93( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2B0( )
      {
      }

      protected void ZM2B93( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z730ProjetoMelhoria_INM = T002B3_A730ProjetoMelhoria_INM[0];
               Z731ProjetoMelhoria_Valor = T002B3_A731ProjetoMelhoria_Valor[0];
               Z732Projetomelhoria_Tipo = T002B3_A732Projetomelhoria_Tipo[0];
               Z733ProjetoMelhoria_PFB = T002B3_A733ProjetoMelhoria_PFB[0];
               Z734ProjetoMelhoria_PFL = T002B3_A734ProjetoMelhoria_PFL[0];
               Z694ProjetoMelhoria_ProjetoCod = T002B3_A694ProjetoMelhoria_ProjetoCod[0];
               Z695ProjetoMelhoria_SistemaCod = T002B3_A695ProjetoMelhoria_SistemaCod[0];
               Z697ProjetoMelhoria_FnDadosCod = T002B3_A697ProjetoMelhoria_FnDadosCod[0];
               Z698ProjetoMelhoria_FnAPFCod = T002B3_A698ProjetoMelhoria_FnAPFCod[0];
            }
            else
            {
               Z730ProjetoMelhoria_INM = A730ProjetoMelhoria_INM;
               Z731ProjetoMelhoria_Valor = A731ProjetoMelhoria_Valor;
               Z732Projetomelhoria_Tipo = A732Projetomelhoria_Tipo;
               Z733ProjetoMelhoria_PFB = A733ProjetoMelhoria_PFB;
               Z734ProjetoMelhoria_PFL = A734ProjetoMelhoria_PFL;
               Z694ProjetoMelhoria_ProjetoCod = A694ProjetoMelhoria_ProjetoCod;
               Z695ProjetoMelhoria_SistemaCod = A695ProjetoMelhoria_SistemaCod;
               Z697ProjetoMelhoria_FnDadosCod = A697ProjetoMelhoria_FnDadosCod;
               Z698ProjetoMelhoria_FnAPFCod = A698ProjetoMelhoria_FnAPFCod;
            }
         }
         if ( GX_JID == -4 )
         {
            Z736ProjetoMelhoria_Codigo = A736ProjetoMelhoria_Codigo;
            Z704ProjetoMelhoria_Observacao = A704ProjetoMelhoria_Observacao;
            Z730ProjetoMelhoria_INM = A730ProjetoMelhoria_INM;
            Z731ProjetoMelhoria_Valor = A731ProjetoMelhoria_Valor;
            Z732Projetomelhoria_Tipo = A732Projetomelhoria_Tipo;
            Z733ProjetoMelhoria_PFB = A733ProjetoMelhoria_PFB;
            Z734ProjetoMelhoria_PFL = A734ProjetoMelhoria_PFL;
            Z694ProjetoMelhoria_ProjetoCod = A694ProjetoMelhoria_ProjetoCod;
            Z695ProjetoMelhoria_SistemaCod = A695ProjetoMelhoria_SistemaCod;
            Z697ProjetoMelhoria_FnDadosCod = A697ProjetoMelhoria_FnDadosCod;
            Z698ProjetoMelhoria_FnAPFCod = A698ProjetoMelhoria_FnAPFCod;
            Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAPROJETOMELHORIA_FNDADOSCOD_html2B93( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002B6 */
            pr_default.execute(4, new Object[] {n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod});
            A369FuncaoDados_Nome = T002B6_A369FuncaoDados_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            n369FuncaoDados_Nome = T002B6_n369FuncaoDados_Nome[0];
            pr_default.close(4);
         }
      }

      protected void Load2B93( )
      {
         /* Using cursor T002B9 */
         pr_default.execute(7, new Object[] {A736ProjetoMelhoria_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound93 = 1;
            A369FuncaoDados_Nome = T002B9_A369FuncaoDados_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            n369FuncaoDados_Nome = T002B9_n369FuncaoDados_Nome[0];
            A166FuncaoAPF_Nome = T002B9_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            n166FuncaoAPF_Nome = T002B9_n166FuncaoAPF_Nome[0];
            A704ProjetoMelhoria_Observacao = T002B9_A704ProjetoMelhoria_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A704ProjetoMelhoria_Observacao", A704ProjetoMelhoria_Observacao);
            n704ProjetoMelhoria_Observacao = T002B9_n704ProjetoMelhoria_Observacao[0];
            A730ProjetoMelhoria_INM = T002B9_A730ProjetoMelhoria_INM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A730ProjetoMelhoria_INM", A730ProjetoMelhoria_INM);
            n730ProjetoMelhoria_INM = T002B9_n730ProjetoMelhoria_INM[0];
            A731ProjetoMelhoria_Valor = T002B9_A731ProjetoMelhoria_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.Str( A731ProjetoMelhoria_Valor, 18, 5)));
            n731ProjetoMelhoria_Valor = T002B9_n731ProjetoMelhoria_Valor[0];
            A732Projetomelhoria_Tipo = T002B9_A732Projetomelhoria_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
            n732Projetomelhoria_Tipo = T002B9_n732Projetomelhoria_Tipo[0];
            A733ProjetoMelhoria_PFB = T002B9_A733ProjetoMelhoria_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.Str( A733ProjetoMelhoria_PFB, 14, 5)));
            n733ProjetoMelhoria_PFB = T002B9_n733ProjetoMelhoria_PFB[0];
            A734ProjetoMelhoria_PFL = T002B9_A734ProjetoMelhoria_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.Str( A734ProjetoMelhoria_PFL, 14, 5)));
            n734ProjetoMelhoria_PFL = T002B9_n734ProjetoMelhoria_PFL[0];
            A694ProjetoMelhoria_ProjetoCod = T002B9_A694ProjetoMelhoria_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
            A695ProjetoMelhoria_SistemaCod = T002B9_A695ProjetoMelhoria_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
            A697ProjetoMelhoria_FnDadosCod = T002B9_A697ProjetoMelhoria_FnDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
            n697ProjetoMelhoria_FnDadosCod = T002B9_n697ProjetoMelhoria_FnDadosCod[0];
            A698ProjetoMelhoria_FnAPFCod = T002B9_A698ProjetoMelhoria_FnAPFCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
            n698ProjetoMelhoria_FnAPFCod = T002B9_n698ProjetoMelhoria_FnAPFCod[0];
            ZM2B93( -4) ;
         }
         pr_default.close(7);
         OnLoadActions2B93( ) ;
      }

      protected void OnLoadActions2B93( )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
         {
            A701ProjetoMelhoria_FnNome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
         }
         else
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
            {
               A701ProjetoMelhoria_FnNome = A166FuncaoAPF_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
            }
            else
            {
               A701ProjetoMelhoria_FnNome = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
            }
         }
      }

      protected void CheckExtendedTable2B93( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002B4 */
         pr_default.execute(2, new Object[] {A694ProjetoMelhoria_ProjetoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Projeto Melhoria_Projeto'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_PROJETOCOD");
            AnyError = 1;
            GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T002B5 */
         pr_default.execute(3, new Object[] {A695ProjetoMelhoria_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoMelhoria_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T002B6 */
         pr_default.execute(4, new Object[] {n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A697ProjetoMelhoria_FnDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNDADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynProjetoMelhoria_FnDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A369FuncaoDados_Nome = T002B6_A369FuncaoDados_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         n369FuncaoDados_Nome = T002B6_n369FuncaoDados_Nome[0];
         pr_default.close(4);
         /* Using cursor T002B7 */
         pr_default.execute(5, new Object[] {n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A698ProjetoMelhoria_FnAPFCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Fn APF'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNAPFCOD");
               AnyError = 1;
               GX_FocusControl = edtProjetoMelhoria_FnAPFCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A166FuncaoAPF_Nome = T002B7_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         n166FuncaoAPF_Nome = T002B7_n166FuncaoAPF_Nome[0];
         pr_default.close(5);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
         {
            A701ProjetoMelhoria_FnNome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
         }
         else
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
            {
               A701ProjetoMelhoria_FnNome = A166FuncaoAPF_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
            }
            else
            {
               A701ProjetoMelhoria_FnNome = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
            }
         }
         if ( ! ( ( A732Projetomelhoria_Tipo == 1 ) || ( A732Projetomelhoria_Tipo == 2 ) || (0==A732Projetomelhoria_Tipo) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "PROJETOMELHORIA_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbProjetomelhoria_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2B93( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A694ProjetoMelhoria_ProjetoCod )
      {
         /* Using cursor T002B10 */
         pr_default.execute(8, new Object[] {A694ProjetoMelhoria_ProjetoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Projeto Melhoria_Projeto'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_PROJETOCOD");
            AnyError = 1;
            GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_6( int A695ProjetoMelhoria_SistemaCod )
      {
         /* Using cursor T002B11 */
         pr_default.execute(9, new Object[] {A695ProjetoMelhoria_SistemaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoMelhoria_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_7( int A697ProjetoMelhoria_FnDadosCod )
      {
         /* Using cursor T002B12 */
         pr_default.execute(10, new Object[] {n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A697ProjetoMelhoria_FnDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNDADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynProjetoMelhoria_FnDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A369FuncaoDados_Nome = T002B12_A369FuncaoDados_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         n369FuncaoDados_Nome = T002B12_n369FuncaoDados_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A369FuncaoDados_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_8( int A698ProjetoMelhoria_FnAPFCod )
      {
         /* Using cursor T002B13 */
         pr_default.execute(11, new Object[] {n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A698ProjetoMelhoria_FnAPFCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Fn APF'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNAPFCOD");
               AnyError = 1;
               GX_FocusControl = edtProjetoMelhoria_FnAPFCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A166FuncaoAPF_Nome = T002B13_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         n166FuncaoAPF_Nome = T002B13_n166FuncaoAPF_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A166FuncaoAPF_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void GetKey2B93( )
      {
         /* Using cursor T002B14 */
         pr_default.execute(12, new Object[] {A736ProjetoMelhoria_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound93 = 1;
         }
         else
         {
            RcdFound93 = 0;
         }
         pr_default.close(12);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002B3 */
         pr_default.execute(1, new Object[] {A736ProjetoMelhoria_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2B93( 4) ;
            RcdFound93 = 1;
            A736ProjetoMelhoria_Codigo = T002B3_A736ProjetoMelhoria_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
            A704ProjetoMelhoria_Observacao = T002B3_A704ProjetoMelhoria_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A704ProjetoMelhoria_Observacao", A704ProjetoMelhoria_Observacao);
            n704ProjetoMelhoria_Observacao = T002B3_n704ProjetoMelhoria_Observacao[0];
            A730ProjetoMelhoria_INM = T002B3_A730ProjetoMelhoria_INM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A730ProjetoMelhoria_INM", A730ProjetoMelhoria_INM);
            n730ProjetoMelhoria_INM = T002B3_n730ProjetoMelhoria_INM[0];
            A731ProjetoMelhoria_Valor = T002B3_A731ProjetoMelhoria_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.Str( A731ProjetoMelhoria_Valor, 18, 5)));
            n731ProjetoMelhoria_Valor = T002B3_n731ProjetoMelhoria_Valor[0];
            A732Projetomelhoria_Tipo = T002B3_A732Projetomelhoria_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
            n732Projetomelhoria_Tipo = T002B3_n732Projetomelhoria_Tipo[0];
            A733ProjetoMelhoria_PFB = T002B3_A733ProjetoMelhoria_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.Str( A733ProjetoMelhoria_PFB, 14, 5)));
            n733ProjetoMelhoria_PFB = T002B3_n733ProjetoMelhoria_PFB[0];
            A734ProjetoMelhoria_PFL = T002B3_A734ProjetoMelhoria_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.Str( A734ProjetoMelhoria_PFL, 14, 5)));
            n734ProjetoMelhoria_PFL = T002B3_n734ProjetoMelhoria_PFL[0];
            A694ProjetoMelhoria_ProjetoCod = T002B3_A694ProjetoMelhoria_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
            A695ProjetoMelhoria_SistemaCod = T002B3_A695ProjetoMelhoria_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
            A697ProjetoMelhoria_FnDadosCod = T002B3_A697ProjetoMelhoria_FnDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
            n697ProjetoMelhoria_FnDadosCod = T002B3_n697ProjetoMelhoria_FnDadosCod[0];
            A698ProjetoMelhoria_FnAPFCod = T002B3_A698ProjetoMelhoria_FnAPFCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
            n698ProjetoMelhoria_FnAPFCod = T002B3_n698ProjetoMelhoria_FnAPFCod[0];
            Z736ProjetoMelhoria_Codigo = A736ProjetoMelhoria_Codigo;
            sMode93 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2B93( ) ;
            if ( AnyError == 1 )
            {
               RcdFound93 = 0;
               InitializeNonKey2B93( ) ;
            }
            Gx_mode = sMode93;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound93 = 0;
            InitializeNonKey2B93( ) ;
            sMode93 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode93;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2B93( ) ;
         if ( RcdFound93 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound93 = 0;
         /* Using cursor T002B15 */
         pr_default.execute(13, new Object[] {A736ProjetoMelhoria_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T002B15_A736ProjetoMelhoria_Codigo[0] < A736ProjetoMelhoria_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T002B15_A736ProjetoMelhoria_Codigo[0] > A736ProjetoMelhoria_Codigo ) ) )
            {
               A736ProjetoMelhoria_Codigo = T002B15_A736ProjetoMelhoria_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
               RcdFound93 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void move_previous( )
      {
         RcdFound93 = 0;
         /* Using cursor T002B16 */
         pr_default.execute(14, new Object[] {A736ProjetoMelhoria_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T002B16_A736ProjetoMelhoria_Codigo[0] > A736ProjetoMelhoria_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T002B16_A736ProjetoMelhoria_Codigo[0] < A736ProjetoMelhoria_Codigo ) ) )
            {
               A736ProjetoMelhoria_Codigo = T002B16_A736ProjetoMelhoria_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
               RcdFound93 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2B93( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2B93( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound93 == 1 )
            {
               if ( A736ProjetoMelhoria_Codigo != Z736ProjetoMelhoria_Codigo )
               {
                  A736ProjetoMelhoria_Codigo = Z736ProjetoMelhoria_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROJETOMELHORIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2B93( ) ;
                  GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A736ProjetoMelhoria_Codigo != Z736ProjetoMelhoria_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2B93( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROJETOMELHORIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2B93( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A736ProjetoMelhoria_Codigo != Z736ProjetoMelhoria_Codigo )
         {
            A736ProjetoMelhoria_Codigo = Z736ProjetoMelhoria_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROJETOMELHORIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound93 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "PROJETOMELHORIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtProjetoMelhoria_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2B93( ) ;
         if ( RcdFound93 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2B93( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound93 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound93 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2B93( ) ;
         if ( RcdFound93 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound93 != 0 )
            {
               ScanNext2B93( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2B93( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2B93( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002B2 */
            pr_default.execute(0, new Object[] {A736ProjetoMelhoria_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoMelhoria"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z730ProjetoMelhoria_INM, T002B2_A730ProjetoMelhoria_INM[0]) != 0 ) || ( Z731ProjetoMelhoria_Valor != T002B2_A731ProjetoMelhoria_Valor[0] ) || ( Z732Projetomelhoria_Tipo != T002B2_A732Projetomelhoria_Tipo[0] ) || ( Z733ProjetoMelhoria_PFB != T002B2_A733ProjetoMelhoria_PFB[0] ) || ( Z734ProjetoMelhoria_PFL != T002B2_A734ProjetoMelhoria_PFL[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z694ProjetoMelhoria_ProjetoCod != T002B2_A694ProjetoMelhoria_ProjetoCod[0] ) || ( Z695ProjetoMelhoria_SistemaCod != T002B2_A695ProjetoMelhoria_SistemaCod[0] ) || ( Z697ProjetoMelhoria_FnDadosCod != T002B2_A697ProjetoMelhoria_FnDadosCod[0] ) || ( Z698ProjetoMelhoria_FnAPFCod != T002B2_A698ProjetoMelhoria_FnAPFCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z730ProjetoMelhoria_INM, T002B2_A730ProjetoMelhoria_INM[0]) != 0 )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_INM");
                  GXUtil.WriteLogRaw("Old: ",Z730ProjetoMelhoria_INM);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A730ProjetoMelhoria_INM[0]);
               }
               if ( Z731ProjetoMelhoria_Valor != T002B2_A731ProjetoMelhoria_Valor[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z731ProjetoMelhoria_Valor);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A731ProjetoMelhoria_Valor[0]);
               }
               if ( Z732Projetomelhoria_Tipo != T002B2_A732Projetomelhoria_Tipo[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"Projetomelhoria_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z732Projetomelhoria_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A732Projetomelhoria_Tipo[0]);
               }
               if ( Z733ProjetoMelhoria_PFB != T002B2_A733ProjetoMelhoria_PFB[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_PFB");
                  GXUtil.WriteLogRaw("Old: ",Z733ProjetoMelhoria_PFB);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A733ProjetoMelhoria_PFB[0]);
               }
               if ( Z734ProjetoMelhoria_PFL != T002B2_A734ProjetoMelhoria_PFL[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_PFL");
                  GXUtil.WriteLogRaw("Old: ",Z734ProjetoMelhoria_PFL);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A734ProjetoMelhoria_PFL[0]);
               }
               if ( Z694ProjetoMelhoria_ProjetoCod != T002B2_A694ProjetoMelhoria_ProjetoCod[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_ProjetoCod");
                  GXUtil.WriteLogRaw("Old: ",Z694ProjetoMelhoria_ProjetoCod);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A694ProjetoMelhoria_ProjetoCod[0]);
               }
               if ( Z695ProjetoMelhoria_SistemaCod != T002B2_A695ProjetoMelhoria_SistemaCod[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z695ProjetoMelhoria_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A695ProjetoMelhoria_SistemaCod[0]);
               }
               if ( Z697ProjetoMelhoria_FnDadosCod != T002B2_A697ProjetoMelhoria_FnDadosCod[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_FnDadosCod");
                  GXUtil.WriteLogRaw("Old: ",Z697ProjetoMelhoria_FnDadosCod);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A697ProjetoMelhoria_FnDadosCod[0]);
               }
               if ( Z698ProjetoMelhoria_FnAPFCod != T002B2_A698ProjetoMelhoria_FnAPFCod[0] )
               {
                  GXUtil.WriteLog("projetomelhoria:[seudo value changed for attri]"+"ProjetoMelhoria_FnAPFCod");
                  GXUtil.WriteLogRaw("Old: ",Z698ProjetoMelhoria_FnAPFCod);
                  GXUtil.WriteLogRaw("Current: ",T002B2_A698ProjetoMelhoria_FnAPFCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ProjetoMelhoria"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2B93( )
      {
         BeforeValidate2B93( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2B93( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2B93( 0) ;
            CheckOptimisticConcurrency2B93( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2B93( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2B93( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002B17 */
                     pr_default.execute(15, new Object[] {n704ProjetoMelhoria_Observacao, A704ProjetoMelhoria_Observacao, n730ProjetoMelhoria_INM, A730ProjetoMelhoria_INM, n731ProjetoMelhoria_Valor, A731ProjetoMelhoria_Valor, n732Projetomelhoria_Tipo, A732Projetomelhoria_Tipo, n733ProjetoMelhoria_PFB, A733ProjetoMelhoria_PFB, n734ProjetoMelhoria_PFL, A734ProjetoMelhoria_PFL, A694ProjetoMelhoria_ProjetoCod, A695ProjetoMelhoria_SistemaCod, n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod, n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod});
                     A736ProjetoMelhoria_Codigo = T002B17_A736ProjetoMelhoria_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2B0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2B93( ) ;
            }
            EndLevel2B93( ) ;
         }
         CloseExtendedTableCursors2B93( ) ;
      }

      protected void Update2B93( )
      {
         BeforeValidate2B93( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2B93( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2B93( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2B93( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2B93( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002B18 */
                     pr_default.execute(16, new Object[] {n704ProjetoMelhoria_Observacao, A704ProjetoMelhoria_Observacao, n730ProjetoMelhoria_INM, A730ProjetoMelhoria_INM, n731ProjetoMelhoria_Valor, A731ProjetoMelhoria_Valor, n732Projetomelhoria_Tipo, A732Projetomelhoria_Tipo, n733ProjetoMelhoria_PFB, A733ProjetoMelhoria_PFB, n734ProjetoMelhoria_PFL, A734ProjetoMelhoria_PFL, A694ProjetoMelhoria_ProjetoCod, A695ProjetoMelhoria_SistemaCod, n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod, n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod, A736ProjetoMelhoria_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
                     if ( (pr_default.getStatus(16) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoMelhoria"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2B93( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2B0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2B93( ) ;
         }
         CloseExtendedTableCursors2B93( ) ;
      }

      protected void DeferredUpdate2B93( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2B93( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2B93( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2B93( ) ;
            AfterConfirm2B93( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2B93( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002B19 */
                  pr_default.execute(17, new Object[] {A736ProjetoMelhoria_Codigo});
                  pr_default.close(17);
                  dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound93 == 0 )
                        {
                           InitAll2B93( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2B0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode93 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2B93( ) ;
         Gx_mode = sMode93;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2B93( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002B20 */
            pr_default.execute(18, new Object[] {n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod});
            A369FuncaoDados_Nome = T002B20_A369FuncaoDados_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            n369FuncaoDados_Nome = T002B20_n369FuncaoDados_Nome[0];
            pr_default.close(18);
            /* Using cursor T002B21 */
            pr_default.execute(19, new Object[] {n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod});
            A166FuncaoAPF_Nome = T002B21_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            n166FuncaoAPF_Nome = T002B21_n166FuncaoAPF_Nome[0];
            pr_default.close(19);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
            {
               A701ProjetoMelhoria_FnNome = A369FuncaoDados_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
            }
            else
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
               {
                  A701ProjetoMelhoria_FnNome = A166FuncaoAPF_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
               }
               else
               {
                  A701ProjetoMelhoria_FnNome = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
               }
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002B22 */
            pr_default.execute(20, new Object[] {A736ProjetoMelhoria_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Baseline"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
         }
      }

      protected void EndLevel2B93( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2B93( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(18);
            pr_default.close(19);
            context.CommitDataStores( "ProjetoMelhoria");
            if ( AnyError == 0 )
            {
               ConfirmValues2B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(18);
            pr_default.close(19);
            context.RollbackDataStores( "ProjetoMelhoria");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2B93( )
      {
         /* Using cursor T002B23 */
         pr_default.execute(21);
         RcdFound93 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound93 = 1;
            A736ProjetoMelhoria_Codigo = T002B23_A736ProjetoMelhoria_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2B93( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound93 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound93 = 1;
            A736ProjetoMelhoria_Codigo = T002B23_A736ProjetoMelhoria_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2B93( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm2B93( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2B93( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2B93( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2B93( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2B93( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2B93( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2B93( )
      {
         edtProjetoMelhoria_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_Codigo_Enabled), 5, 0)));
         dynProjetoMelhoria_ProjetoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjetoMelhoria_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjetoMelhoria_ProjetoCod.Enabled), 5, 0)));
         edtProjetoMelhoria_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_SistemaCod_Enabled), 5, 0)));
         dynProjetoMelhoria_FnDadosCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjetoMelhoria_FnDadosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjetoMelhoria_FnDadosCod.Enabled), 5, 0)));
         edtFuncaoDados_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Nome_Enabled), 5, 0)));
         edtProjetoMelhoria_FnAPFCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_FnAPFCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_FnAPFCod_Enabled), 5, 0)));
         edtFuncaoAPF_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Enabled), 5, 0)));
         edtProjetoMelhoria_FnNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_FnNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_FnNome_Enabled), 5, 0)));
         edtProjetoMelhoria_Observacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_Observacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_Observacao_Enabled), 5, 0)));
         edtProjetoMelhoria_INM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_INM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_INM_Enabled), 5, 0)));
         edtProjetoMelhoria_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_Valor_Enabled), 5, 0)));
         cmbProjetomelhoria_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjetomelhoria_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjetomelhoria_Tipo.Enabled), 5, 0)));
         edtProjetoMelhoria_PFB_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_PFB_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_PFB_Enabled), 5, 0)));
         edtProjetoMelhoria_PFL_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoMelhoria_PFL_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoMelhoria_PFL_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623552940");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetomelhoria.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z736ProjetoMelhoria_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z730ProjetoMelhoria_INM", StringUtil.RTrim( Z730ProjetoMelhoria_INM));
         GxWebStd.gx_hidden_field( context, "Z731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.NToC( Z731ProjetoMelhoria_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z732Projetomelhoria_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.NToC( Z733ProjetoMelhoria_PFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.NToC( Z734ProjetoMelhoria_PFL, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z694ProjetoMelhoria_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z695ProjetoMelhoria_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z697ProjetoMelhoria_FnDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z698ProjetoMelhoria_FnAPFCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("projetomelhoria.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ProjetoMelhoria" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Melhoria" ;
      }

      protected void InitializeNonKey2B93( )
      {
         A701ProjetoMelhoria_FnNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A701ProjetoMelhoria_FnNome", A701ProjetoMelhoria_FnNome);
         A694ProjetoMelhoria_ProjetoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A694ProjetoMelhoria_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0)));
         A695ProjetoMelhoria_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A695ProjetoMelhoria_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0)));
         A697ProjetoMelhoria_FnDadosCod = 0;
         n697ProjetoMelhoria_FnDadosCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A697ProjetoMelhoria_FnDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0)));
         n697ProjetoMelhoria_FnDadosCod = ((0==A697ProjetoMelhoria_FnDadosCod) ? true : false);
         A369FuncaoDados_Nome = "";
         n369FuncaoDados_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         A698ProjetoMelhoria_FnAPFCod = 0;
         n698ProjetoMelhoria_FnAPFCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A698ProjetoMelhoria_FnAPFCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0)));
         n698ProjetoMelhoria_FnAPFCod = ((0==A698ProjetoMelhoria_FnAPFCod) ? true : false);
         A166FuncaoAPF_Nome = "";
         n166FuncaoAPF_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A704ProjetoMelhoria_Observacao = "";
         n704ProjetoMelhoria_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A704ProjetoMelhoria_Observacao", A704ProjetoMelhoria_Observacao);
         n704ProjetoMelhoria_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A704ProjetoMelhoria_Observacao)) ? true : false);
         A730ProjetoMelhoria_INM = "";
         n730ProjetoMelhoria_INM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A730ProjetoMelhoria_INM", A730ProjetoMelhoria_INM);
         n730ProjetoMelhoria_INM = (String.IsNullOrEmpty(StringUtil.RTrim( A730ProjetoMelhoria_INM)) ? true : false);
         A731ProjetoMelhoria_Valor = 0;
         n731ProjetoMelhoria_Valor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A731ProjetoMelhoria_Valor", StringUtil.LTrim( StringUtil.Str( A731ProjetoMelhoria_Valor, 18, 5)));
         n731ProjetoMelhoria_Valor = ((Convert.ToDecimal(0)==A731ProjetoMelhoria_Valor) ? true : false);
         A732Projetomelhoria_Tipo = 0;
         n732Projetomelhoria_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A732Projetomelhoria_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0)));
         n732Projetomelhoria_Tipo = ((0==A732Projetomelhoria_Tipo) ? true : false);
         A733ProjetoMelhoria_PFB = 0;
         n733ProjetoMelhoria_PFB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A733ProjetoMelhoria_PFB", StringUtil.LTrim( StringUtil.Str( A733ProjetoMelhoria_PFB, 14, 5)));
         n733ProjetoMelhoria_PFB = ((Convert.ToDecimal(0)==A733ProjetoMelhoria_PFB) ? true : false);
         A734ProjetoMelhoria_PFL = 0;
         n734ProjetoMelhoria_PFL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A734ProjetoMelhoria_PFL", StringUtil.LTrim( StringUtil.Str( A734ProjetoMelhoria_PFL, 14, 5)));
         n734ProjetoMelhoria_PFL = ((Convert.ToDecimal(0)==A734ProjetoMelhoria_PFL) ? true : false);
         Z730ProjetoMelhoria_INM = "";
         Z731ProjetoMelhoria_Valor = 0;
         Z732Projetomelhoria_Tipo = 0;
         Z733ProjetoMelhoria_PFB = 0;
         Z734ProjetoMelhoria_PFL = 0;
         Z694ProjetoMelhoria_ProjetoCod = 0;
         Z695ProjetoMelhoria_SistemaCod = 0;
         Z697ProjetoMelhoria_FnDadosCod = 0;
         Z698ProjetoMelhoria_FnAPFCod = 0;
      }

      protected void InitAll2B93( )
      {
         A736ProjetoMelhoria_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A736ProjetoMelhoria_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A736ProjetoMelhoria_Codigo), 6, 0)));
         InitializeNonKey2B93( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623552950");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("projetomelhoria.js", "?20205623552950");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockprojetomelhoria_codigo_Internalname = "TEXTBLOCKPROJETOMELHORIA_CODIGO";
         edtProjetoMelhoria_Codigo_Internalname = "PROJETOMELHORIA_CODIGO";
         lblTextblockprojetomelhoria_projetocod_Internalname = "TEXTBLOCKPROJETOMELHORIA_PROJETOCOD";
         dynProjetoMelhoria_ProjetoCod_Internalname = "PROJETOMELHORIA_PROJETOCOD";
         lblTextblockprojetomelhoria_sistemacod_Internalname = "TEXTBLOCKPROJETOMELHORIA_SISTEMACOD";
         edtProjetoMelhoria_SistemaCod_Internalname = "PROJETOMELHORIA_SISTEMACOD";
         lblTextblockprojetomelhoria_fndadoscod_Internalname = "TEXTBLOCKPROJETOMELHORIA_FNDADOSCOD";
         dynProjetoMelhoria_FnDadosCod_Internalname = "PROJETOMELHORIA_FNDADOSCOD";
         lblTextblockfuncaodados_nome_Internalname = "TEXTBLOCKFUNCAODADOS_NOME";
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME";
         lblTextblockprojetomelhoria_fnapfcod_Internalname = "TEXTBLOCKPROJETOMELHORIA_FNAPFCOD";
         edtProjetoMelhoria_FnAPFCod_Internalname = "PROJETOMELHORIA_FNAPFCOD";
         lblTextblockfuncaoapf_nome_Internalname = "TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         lblTextblockprojetomelhoria_fnnome_Internalname = "TEXTBLOCKPROJETOMELHORIA_FNNOME";
         edtProjetoMelhoria_FnNome_Internalname = "PROJETOMELHORIA_FNNOME";
         lblTextblockprojetomelhoria_observacao_Internalname = "TEXTBLOCKPROJETOMELHORIA_OBSERVACAO";
         edtProjetoMelhoria_Observacao_Internalname = "PROJETOMELHORIA_OBSERVACAO";
         lblTextblockprojetomelhoria_inm_Internalname = "TEXTBLOCKPROJETOMELHORIA_INM";
         edtProjetoMelhoria_INM_Internalname = "PROJETOMELHORIA_INM";
         lblTextblockprojetomelhoria_valor_Internalname = "TEXTBLOCKPROJETOMELHORIA_VALOR";
         edtProjetoMelhoria_Valor_Internalname = "PROJETOMELHORIA_VALOR";
         lblTextblockprojetomelhoria_tipo_Internalname = "TEXTBLOCKPROJETOMELHORIA_TIPO";
         cmbProjetomelhoria_Tipo_Internalname = "PROJETOMELHORIA_TIPO";
         lblTextblockprojetomelhoria_pfb_Internalname = "TEXTBLOCKPROJETOMELHORIA_PFB";
         edtProjetoMelhoria_PFB_Internalname = "PROJETOMELHORIA_PFB";
         lblTextblockprojetomelhoria_pfl_Internalname = "TEXTBLOCKPROJETOMELHORIA_PFL";
         edtProjetoMelhoria_PFL_Internalname = "PROJETOMELHORIA_PFL";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto Melhoria";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtProjetoMelhoria_PFL_Jsonclick = "";
         edtProjetoMelhoria_PFL_Enabled = 1;
         edtProjetoMelhoria_PFB_Jsonclick = "";
         edtProjetoMelhoria_PFB_Enabled = 1;
         cmbProjetomelhoria_Tipo_Jsonclick = "";
         cmbProjetomelhoria_Tipo.Enabled = 1;
         edtProjetoMelhoria_Valor_Jsonclick = "";
         edtProjetoMelhoria_Valor_Enabled = 1;
         edtProjetoMelhoria_INM_Jsonclick = "";
         edtProjetoMelhoria_INM_Enabled = 1;
         edtProjetoMelhoria_Observacao_Enabled = 1;
         edtProjetoMelhoria_FnNome_Jsonclick = "";
         edtProjetoMelhoria_FnNome_Enabled = 0;
         edtFuncaoAPF_Nome_Enabled = 0;
         edtProjetoMelhoria_FnAPFCod_Jsonclick = "";
         edtProjetoMelhoria_FnAPFCod_Enabled = 1;
         edtFuncaoDados_Nome_Enabled = 0;
         dynProjetoMelhoria_FnDadosCod_Jsonclick = "";
         dynProjetoMelhoria_FnDadosCod.Enabled = 1;
         edtProjetoMelhoria_SistemaCod_Jsonclick = "";
         edtProjetoMelhoria_SistemaCod_Enabled = 1;
         dynProjetoMelhoria_ProjetoCod_Jsonclick = "";
         dynProjetoMelhoria_ProjetoCod.Enabled = 1;
         edtProjetoMelhoria_Codigo_Jsonclick = "";
         edtProjetoMelhoria_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPROJETOMELHORIA_PROJETOCOD2B1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETOMELHORIA_PROJETOCOD_data2B1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETOMELHORIA_PROJETOCOD_html2B1( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETOMELHORIA_PROJETOCOD_data2B1( ) ;
         gxdynajaxindex = 1;
         dynProjetoMelhoria_ProjetoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjetoMelhoria_ProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETOMELHORIA_PROJETOCOD_data2B1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002B24 */
         pr_default.execute(22);
         while ( (pr_default.getStatus(22) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002B24_A648Projeto_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002B24_A650Projeto_Sigla[0]));
            pr_default.readNext(22);
         }
         pr_default.close(22);
      }

      protected void GXDLAPROJETOMELHORIA_FNDADOSCOD2B93( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETOMELHORIA_FNDADOSCOD_data2B93( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETOMELHORIA_FNDADOSCOD_html2B93( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETOMELHORIA_FNDADOSCOD_data2B93( ) ;
         gxdynajaxindex = 1;
         dynProjetoMelhoria_FnDadosCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjetoMelhoria_FnDadosCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETOMELHORIA_FNDADOSCOD_data2B93( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T002B25 */
         pr_default.execute(23);
         while ( (pr_default.getStatus(23) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002B25_A697ProjetoMelhoria_FnDadosCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002B25_A369FuncaoDados_Nome[0]);
            pr_default.readNext(23);
         }
         pr_default.close(23);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Projetomelhoria_codigo( int GX_Parm1 ,
                                                String GX_Parm2 ,
                                                String GX_Parm3 ,
                                                decimal GX_Parm4 ,
                                                GXCombobox cmbGX_Parm5 ,
                                                decimal GX_Parm6 ,
                                                decimal GX_Parm7 ,
                                                GXCombobox dynGX_Parm8 ,
                                                int GX_Parm9 ,
                                                GXCombobox dynGX_Parm10 ,
                                                int GX_Parm11 )
      {
         A736ProjetoMelhoria_Codigo = GX_Parm1;
         A704ProjetoMelhoria_Observacao = GX_Parm2;
         n704ProjetoMelhoria_Observacao = false;
         A730ProjetoMelhoria_INM = GX_Parm3;
         n730ProjetoMelhoria_INM = false;
         A731ProjetoMelhoria_Valor = GX_Parm4;
         n731ProjetoMelhoria_Valor = false;
         cmbProjetomelhoria_Tipo = cmbGX_Parm5;
         A732Projetomelhoria_Tipo = (short)(NumberUtil.Val( cmbProjetomelhoria_Tipo.CurrentValue, "."));
         n732Projetomelhoria_Tipo = false;
         cmbProjetomelhoria_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0));
         A733ProjetoMelhoria_PFB = GX_Parm6;
         n733ProjetoMelhoria_PFB = false;
         A734ProjetoMelhoria_PFL = GX_Parm7;
         n734ProjetoMelhoria_PFL = false;
         dynProjetoMelhoria_ProjetoCod = dynGX_Parm8;
         A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( dynProjetoMelhoria_ProjetoCod.CurrentValue, "."));
         A695ProjetoMelhoria_SistemaCod = GX_Parm9;
         dynProjetoMelhoria_FnDadosCod = dynGX_Parm10;
         A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( dynProjetoMelhoria_FnDadosCod.CurrentValue, "."));
         n697ProjetoMelhoria_FnDadosCod = false;
         A698ProjetoMelhoria_FnAPFCod = GX_Parm11;
         n698ProjetoMelhoria_FnAPFCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A369FuncaoDados_Nome = "";
            n369FuncaoDados_Nome = false;
            A166FuncaoAPF_Nome = "";
            n166FuncaoAPF_Nome = false;
         }
         dynProjetoMelhoria_ProjetoCod.removeAllItems();
         /* Using cursor T002B26 */
         pr_default.execute(24);
         while ( (pr_default.getStatus(24) != 101) )
         {
            dynProjetoMelhoria_ProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T002B26_A648Projeto_Codigo[0]), 6, 0)), T002B26_A650Projeto_Sigla[0], 0);
            pr_default.readNext(24);
         }
         pr_default.close(24);
         dynProjetoMelhoria_ProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0));
         GXAPROJETOMELHORIA_FNDADOSCOD_html2B93( ) ;
         dynProjetoMelhoria_FnDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0));
         if ( dynProjetoMelhoria_ProjetoCod.ItemCount > 0 )
         {
            A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( dynProjetoMelhoria_ProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0))), "."));
         }
         dynProjetoMelhoria_ProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A694ProjetoMelhoria_ProjetoCod), 6, 0));
         isValidOutput.Add(dynProjetoMelhoria_ProjetoCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A695ProjetoMelhoria_SistemaCod), 6, 0, ".", "")));
         if ( dynProjetoMelhoria_FnDadosCod.ItemCount > 0 )
         {
            A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( dynProjetoMelhoria_FnDadosCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0))), "."));
            n697ProjetoMelhoria_FnDadosCod = false;
         }
         dynProjetoMelhoria_FnDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A697ProjetoMelhoria_FnDadosCod), 6, 0));
         isValidOutput.Add(dynProjetoMelhoria_FnDadosCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A698ProjetoMelhoria_FnAPFCod), 6, 0, ".", "")));
         isValidOutput.Add(A704ProjetoMelhoria_Observacao);
         isValidOutput.Add(StringUtil.RTrim( A730ProjetoMelhoria_INM));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A731ProjetoMelhoria_Valor, 18, 5, ".", "")));
         cmbProjetomelhoria_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A732Projetomelhoria_Tipo), 2, 0));
         isValidOutput.Add(cmbProjetomelhoria_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A733ProjetoMelhoria_PFB, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A734ProjetoMelhoria_PFL, 14, 5, ".", "")));
         isValidOutput.Add(A369FuncaoDados_Nome);
         isValidOutput.Add(A166FuncaoAPF_Nome);
         isValidOutput.Add(StringUtil.RTrim( A701ProjetoMelhoria_FnNome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z736ProjetoMelhoria_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z694ProjetoMelhoria_ProjetoCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z695ProjetoMelhoria_SistemaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z697ProjetoMelhoria_FnDadosCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z698ProjetoMelhoria_FnAPFCod), 6, 0, ",", "")));
         isValidOutput.Add(Z704ProjetoMelhoria_Observacao);
         isValidOutput.Add(StringUtil.RTrim( Z730ProjetoMelhoria_INM));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z731ProjetoMelhoria_Valor, 18, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z732Projetomelhoria_Tipo), 2, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z733ProjetoMelhoria_PFB, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z734ProjetoMelhoria_PFL, 14, 5, ",", "")));
         isValidOutput.Add(Z369FuncaoDados_Nome);
         isValidOutput.Add(Z166FuncaoAPF_Nome);
         isValidOutput.Add(StringUtil.RTrim( Z701ProjetoMelhoria_FnNome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projetomelhoria_projetocod( GXCombobox dynGX_Parm1 )
      {
         dynProjetoMelhoria_ProjetoCod = dynGX_Parm1;
         A694ProjetoMelhoria_ProjetoCod = (int)(NumberUtil.Val( dynProjetoMelhoria_ProjetoCod.CurrentValue, "."));
         /* Using cursor T002B27 */
         pr_default.execute(25, new Object[] {A694ProjetoMelhoria_ProjetoCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Projeto Melhoria_Projeto'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_PROJETOCOD");
            AnyError = 1;
            GX_FocusControl = dynProjetoMelhoria_ProjetoCod_Internalname;
         }
         pr_default.close(25);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projetomelhoria_sistemacod( int GX_Parm1 )
      {
         A695ProjetoMelhoria_SistemaCod = GX_Parm1;
         /* Using cursor T002B28 */
         pr_default.execute(26, new Object[] {A695ProjetoMelhoria_SistemaCod});
         if ( (pr_default.getStatus(26) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoMelhoria_SistemaCod_Internalname;
         }
         pr_default.close(26);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projetomelhoria_fndadoscod( GXCombobox dynGX_Parm1 ,
                                                    String GX_Parm2 )
      {
         dynProjetoMelhoria_FnDadosCod = dynGX_Parm1;
         A697ProjetoMelhoria_FnDadosCod = (int)(NumberUtil.Val( dynProjetoMelhoria_FnDadosCod.CurrentValue, "."));
         n697ProjetoMelhoria_FnDadosCod = false;
         A369FuncaoDados_Nome = GX_Parm2;
         n369FuncaoDados_Nome = false;
         /* Using cursor T002B20 */
         pr_default.execute(18, new Object[] {n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A697ProjetoMelhoria_FnDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Sistema'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNDADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynProjetoMelhoria_FnDadosCod_Internalname;
            }
         }
         A369FuncaoDados_Nome = T002B20_A369FuncaoDados_Nome[0];
         n369FuncaoDados_Nome = T002B20_n369FuncaoDados_Nome[0];
         pr_default.close(18);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A369FuncaoDados_Nome = "";
            n369FuncaoDados_Nome = false;
         }
         isValidOutput.Add(A369FuncaoDados_Nome);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projetomelhoria_fnapfcod( int GX_Parm1 ,
                                                  String GX_Parm2 ,
                                                  String GX_Parm3 ,
                                                  String GX_Parm4 )
      {
         A698ProjetoMelhoria_FnAPFCod = GX_Parm1;
         n698ProjetoMelhoria_FnAPFCod = false;
         A369FuncaoDados_Nome = GX_Parm2;
         n369FuncaoDados_Nome = false;
         A166FuncaoAPF_Nome = GX_Parm3;
         n166FuncaoAPF_Nome = false;
         A701ProjetoMelhoria_FnNome = GX_Parm4;
         /* Using cursor T002B21 */
         pr_default.execute(19, new Object[] {n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            if ( ! ( (0==A698ProjetoMelhoria_FnAPFCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto Melhoria_Fn APF'.", "ForeignKeyNotFound", 1, "PROJETOMELHORIA_FNAPFCOD");
               AnyError = 1;
               GX_FocusControl = edtProjetoMelhoria_FnAPFCod_Internalname;
            }
         }
         A166FuncaoAPF_Nome = T002B21_A166FuncaoAPF_Nome[0];
         n166FuncaoAPF_Nome = T002B21_n166FuncaoAPF_Nome[0];
         pr_default.close(19);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
         {
            A701ProjetoMelhoria_FnNome = A369FuncaoDados_Nome;
         }
         else
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
            {
               A701ProjetoMelhoria_FnNome = A166FuncaoAPF_Nome;
            }
            else
            {
               A701ProjetoMelhoria_FnNome = "";
            }
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A166FuncaoAPF_Nome = "";
            n166FuncaoAPF_Nome = false;
         }
         isValidOutput.Add(A166FuncaoAPF_Nome);
         isValidOutput.Add(StringUtil.RTrim( A701ProjetoMelhoria_FnNome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(25);
         pr_default.close(26);
         pr_default.close(18);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z730ProjetoMelhoria_INM = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T002B8_A648Projeto_Codigo = new int[1] ;
         T002B8_A650Projeto_Sigla = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockprojetomelhoria_codigo_Jsonclick = "";
         lblTextblockprojetomelhoria_projetocod_Jsonclick = "";
         lblTextblockprojetomelhoria_sistemacod_Jsonclick = "";
         lblTextblockprojetomelhoria_fndadoscod_Jsonclick = "";
         lblTextblockfuncaodados_nome_Jsonclick = "";
         A369FuncaoDados_Nome = "";
         lblTextblockprojetomelhoria_fnapfcod_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         A166FuncaoAPF_Nome = "";
         lblTextblockprojetomelhoria_fnnome_Jsonclick = "";
         A701ProjetoMelhoria_FnNome = "";
         lblTextblockprojetomelhoria_observacao_Jsonclick = "";
         A704ProjetoMelhoria_Observacao = "";
         lblTextblockprojetomelhoria_inm_Jsonclick = "";
         A730ProjetoMelhoria_INM = "";
         lblTextblockprojetomelhoria_valor_Jsonclick = "";
         lblTextblockprojetomelhoria_tipo_Jsonclick = "";
         lblTextblockprojetomelhoria_pfb_Jsonclick = "";
         lblTextblockprojetomelhoria_pfl_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z704ProjetoMelhoria_Observacao = "";
         Z369FuncaoDados_Nome = "";
         Z166FuncaoAPF_Nome = "";
         T002B6_A369FuncaoDados_Nome = new String[] {""} ;
         T002B6_n369FuncaoDados_Nome = new bool[] {false} ;
         T002B9_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B9_A369FuncaoDados_Nome = new String[] {""} ;
         T002B9_n369FuncaoDados_Nome = new bool[] {false} ;
         T002B9_A166FuncaoAPF_Nome = new String[] {""} ;
         T002B9_n166FuncaoAPF_Nome = new bool[] {false} ;
         T002B9_A704ProjetoMelhoria_Observacao = new String[] {""} ;
         T002B9_n704ProjetoMelhoria_Observacao = new bool[] {false} ;
         T002B9_A730ProjetoMelhoria_INM = new String[] {""} ;
         T002B9_n730ProjetoMelhoria_INM = new bool[] {false} ;
         T002B9_A731ProjetoMelhoria_Valor = new decimal[1] ;
         T002B9_n731ProjetoMelhoria_Valor = new bool[] {false} ;
         T002B9_A732Projetomelhoria_Tipo = new short[1] ;
         T002B9_n732Projetomelhoria_Tipo = new bool[] {false} ;
         T002B9_A733ProjetoMelhoria_PFB = new decimal[1] ;
         T002B9_n733ProjetoMelhoria_PFB = new bool[] {false} ;
         T002B9_A734ProjetoMelhoria_PFL = new decimal[1] ;
         T002B9_n734ProjetoMelhoria_PFL = new bool[] {false} ;
         T002B9_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B9_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         T002B9_A697ProjetoMelhoria_FnDadosCod = new int[1] ;
         T002B9_n697ProjetoMelhoria_FnDadosCod = new bool[] {false} ;
         T002B9_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         T002B9_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         T002B4_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B5_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         T002B7_A166FuncaoAPF_Nome = new String[] {""} ;
         T002B7_n166FuncaoAPF_Nome = new bool[] {false} ;
         T002B10_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B11_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         T002B12_A369FuncaoDados_Nome = new String[] {""} ;
         T002B12_n369FuncaoDados_Nome = new bool[] {false} ;
         T002B13_A166FuncaoAPF_Nome = new String[] {""} ;
         T002B13_n166FuncaoAPF_Nome = new bool[] {false} ;
         T002B14_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B3_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B3_A704ProjetoMelhoria_Observacao = new String[] {""} ;
         T002B3_n704ProjetoMelhoria_Observacao = new bool[] {false} ;
         T002B3_A730ProjetoMelhoria_INM = new String[] {""} ;
         T002B3_n730ProjetoMelhoria_INM = new bool[] {false} ;
         T002B3_A731ProjetoMelhoria_Valor = new decimal[1] ;
         T002B3_n731ProjetoMelhoria_Valor = new bool[] {false} ;
         T002B3_A732Projetomelhoria_Tipo = new short[1] ;
         T002B3_n732Projetomelhoria_Tipo = new bool[] {false} ;
         T002B3_A733ProjetoMelhoria_PFB = new decimal[1] ;
         T002B3_n733ProjetoMelhoria_PFB = new bool[] {false} ;
         T002B3_A734ProjetoMelhoria_PFL = new decimal[1] ;
         T002B3_n734ProjetoMelhoria_PFL = new bool[] {false} ;
         T002B3_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B3_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         T002B3_A697ProjetoMelhoria_FnDadosCod = new int[1] ;
         T002B3_n697ProjetoMelhoria_FnDadosCod = new bool[] {false} ;
         T002B3_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         T002B3_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         sMode93 = "";
         T002B15_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B16_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B2_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B2_A704ProjetoMelhoria_Observacao = new String[] {""} ;
         T002B2_n704ProjetoMelhoria_Observacao = new bool[] {false} ;
         T002B2_A730ProjetoMelhoria_INM = new String[] {""} ;
         T002B2_n730ProjetoMelhoria_INM = new bool[] {false} ;
         T002B2_A731ProjetoMelhoria_Valor = new decimal[1] ;
         T002B2_n731ProjetoMelhoria_Valor = new bool[] {false} ;
         T002B2_A732Projetomelhoria_Tipo = new short[1] ;
         T002B2_n732Projetomelhoria_Tipo = new bool[] {false} ;
         T002B2_A733ProjetoMelhoria_PFB = new decimal[1] ;
         T002B2_n733ProjetoMelhoria_PFB = new bool[] {false} ;
         T002B2_A734ProjetoMelhoria_PFL = new decimal[1] ;
         T002B2_n734ProjetoMelhoria_PFL = new bool[] {false} ;
         T002B2_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B2_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         T002B2_A697ProjetoMelhoria_FnDadosCod = new int[1] ;
         T002B2_n697ProjetoMelhoria_FnDadosCod = new bool[] {false} ;
         T002B2_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         T002B2_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         T002B17_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002B20_A369FuncaoDados_Nome = new String[] {""} ;
         T002B20_n369FuncaoDados_Nome = new bool[] {false} ;
         T002B21_A166FuncaoAPF_Nome = new String[] {""} ;
         T002B21_n166FuncaoAPF_Nome = new bool[] {false} ;
         T002B22_A722Baseline_Codigo = new int[1] ;
         T002B23_A736ProjetoMelhoria_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002B24_A648Projeto_Codigo = new int[1] ;
         T002B24_A650Projeto_Sigla = new String[] {""} ;
         T002B25_A697ProjetoMelhoria_FnDadosCod = new int[1] ;
         T002B25_n697ProjetoMelhoria_FnDadosCod = new bool[] {false} ;
         T002B25_A369FuncaoDados_Nome = new String[] {""} ;
         T002B25_n369FuncaoDados_Nome = new bool[] {false} ;
         Z701ProjetoMelhoria_FnNome = "";
         T002B26_A648Projeto_Codigo = new int[1] ;
         T002B26_A650Projeto_Sigla = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002B27_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         T002B28_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetomelhoria__default(),
            new Object[][] {
                new Object[] {
               T002B2_A736ProjetoMelhoria_Codigo, T002B2_A704ProjetoMelhoria_Observacao, T002B2_n704ProjetoMelhoria_Observacao, T002B2_A730ProjetoMelhoria_INM, T002B2_n730ProjetoMelhoria_INM, T002B2_A731ProjetoMelhoria_Valor, T002B2_n731ProjetoMelhoria_Valor, T002B2_A732Projetomelhoria_Tipo, T002B2_n732Projetomelhoria_Tipo, T002B2_A733ProjetoMelhoria_PFB,
               T002B2_n733ProjetoMelhoria_PFB, T002B2_A734ProjetoMelhoria_PFL, T002B2_n734ProjetoMelhoria_PFL, T002B2_A694ProjetoMelhoria_ProjetoCod, T002B2_A695ProjetoMelhoria_SistemaCod, T002B2_A697ProjetoMelhoria_FnDadosCod, T002B2_n697ProjetoMelhoria_FnDadosCod, T002B2_A698ProjetoMelhoria_FnAPFCod, T002B2_n698ProjetoMelhoria_FnAPFCod
               }
               , new Object[] {
               T002B3_A736ProjetoMelhoria_Codigo, T002B3_A704ProjetoMelhoria_Observacao, T002B3_n704ProjetoMelhoria_Observacao, T002B3_A730ProjetoMelhoria_INM, T002B3_n730ProjetoMelhoria_INM, T002B3_A731ProjetoMelhoria_Valor, T002B3_n731ProjetoMelhoria_Valor, T002B3_A732Projetomelhoria_Tipo, T002B3_n732Projetomelhoria_Tipo, T002B3_A733ProjetoMelhoria_PFB,
               T002B3_n733ProjetoMelhoria_PFB, T002B3_A734ProjetoMelhoria_PFL, T002B3_n734ProjetoMelhoria_PFL, T002B3_A694ProjetoMelhoria_ProjetoCod, T002B3_A695ProjetoMelhoria_SistemaCod, T002B3_A697ProjetoMelhoria_FnDadosCod, T002B3_n697ProjetoMelhoria_FnDadosCod, T002B3_A698ProjetoMelhoria_FnAPFCod, T002B3_n698ProjetoMelhoria_FnAPFCod
               }
               , new Object[] {
               T002B4_A694ProjetoMelhoria_ProjetoCod
               }
               , new Object[] {
               T002B5_A695ProjetoMelhoria_SistemaCod
               }
               , new Object[] {
               T002B6_A369FuncaoDados_Nome, T002B6_n369FuncaoDados_Nome
               }
               , new Object[] {
               T002B7_A166FuncaoAPF_Nome, T002B7_n166FuncaoAPF_Nome
               }
               , new Object[] {
               T002B8_A648Projeto_Codigo, T002B8_A650Projeto_Sigla
               }
               , new Object[] {
               T002B9_A736ProjetoMelhoria_Codigo, T002B9_A369FuncaoDados_Nome, T002B9_n369FuncaoDados_Nome, T002B9_A166FuncaoAPF_Nome, T002B9_n166FuncaoAPF_Nome, T002B9_A704ProjetoMelhoria_Observacao, T002B9_n704ProjetoMelhoria_Observacao, T002B9_A730ProjetoMelhoria_INM, T002B9_n730ProjetoMelhoria_INM, T002B9_A731ProjetoMelhoria_Valor,
               T002B9_n731ProjetoMelhoria_Valor, T002B9_A732Projetomelhoria_Tipo, T002B9_n732Projetomelhoria_Tipo, T002B9_A733ProjetoMelhoria_PFB, T002B9_n733ProjetoMelhoria_PFB, T002B9_A734ProjetoMelhoria_PFL, T002B9_n734ProjetoMelhoria_PFL, T002B9_A694ProjetoMelhoria_ProjetoCod, T002B9_A695ProjetoMelhoria_SistemaCod, T002B9_A697ProjetoMelhoria_FnDadosCod,
               T002B9_n697ProjetoMelhoria_FnDadosCod, T002B9_A698ProjetoMelhoria_FnAPFCod, T002B9_n698ProjetoMelhoria_FnAPFCod
               }
               , new Object[] {
               T002B10_A694ProjetoMelhoria_ProjetoCod
               }
               , new Object[] {
               T002B11_A695ProjetoMelhoria_SistemaCod
               }
               , new Object[] {
               T002B12_A369FuncaoDados_Nome, T002B12_n369FuncaoDados_Nome
               }
               , new Object[] {
               T002B13_A166FuncaoAPF_Nome, T002B13_n166FuncaoAPF_Nome
               }
               , new Object[] {
               T002B14_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002B15_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002B16_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002B17_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002B20_A369FuncaoDados_Nome, T002B20_n369FuncaoDados_Nome
               }
               , new Object[] {
               T002B21_A166FuncaoAPF_Nome, T002B21_n166FuncaoAPF_Nome
               }
               , new Object[] {
               T002B22_A722Baseline_Codigo
               }
               , new Object[] {
               T002B23_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002B24_A648Projeto_Codigo, T002B24_A650Projeto_Sigla
               }
               , new Object[] {
               T002B25_A697ProjetoMelhoria_FnDadosCod, T002B25_A369FuncaoDados_Nome, T002B25_n369FuncaoDados_Nome
               }
               , new Object[] {
               T002B26_A648Projeto_Codigo, T002B26_A650Projeto_Sigla
               }
               , new Object[] {
               T002B27_A694ProjetoMelhoria_ProjetoCod
               }
               , new Object[] {
               T002B28_A695ProjetoMelhoria_SistemaCod
               }
            }
         );
      }

      private short Z732Projetomelhoria_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A732Projetomelhoria_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound93 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z736ProjetoMelhoria_Codigo ;
      private int Z694ProjetoMelhoria_ProjetoCod ;
      private int Z695ProjetoMelhoria_SistemaCod ;
      private int Z697ProjetoMelhoria_FnDadosCod ;
      private int Z698ProjetoMelhoria_FnAPFCod ;
      private int A694ProjetoMelhoria_ProjetoCod ;
      private int A695ProjetoMelhoria_SistemaCod ;
      private int A697ProjetoMelhoria_FnDadosCod ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A736ProjetoMelhoria_Codigo ;
      private int edtProjetoMelhoria_Codigo_Enabled ;
      private int edtProjetoMelhoria_SistemaCod_Enabled ;
      private int edtFuncaoDados_Nome_Enabled ;
      private int edtProjetoMelhoria_FnAPFCod_Enabled ;
      private int edtFuncaoAPF_Nome_Enabled ;
      private int edtProjetoMelhoria_FnNome_Enabled ;
      private int edtProjetoMelhoria_Observacao_Enabled ;
      private int edtProjetoMelhoria_INM_Enabled ;
      private int edtProjetoMelhoria_Valor_Enabled ;
      private int edtProjetoMelhoria_PFB_Enabled ;
      private int edtProjetoMelhoria_PFL_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z731ProjetoMelhoria_Valor ;
      private decimal Z733ProjetoMelhoria_PFB ;
      private decimal Z734ProjetoMelhoria_PFL ;
      private decimal A731ProjetoMelhoria_Valor ;
      private decimal A733ProjetoMelhoria_PFB ;
      private decimal A734ProjetoMelhoria_PFL ;
      private String sPrefix ;
      private String Z730ProjetoMelhoria_INM ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProjetoMelhoria_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockprojetomelhoria_codigo_Internalname ;
      private String lblTextblockprojetomelhoria_codigo_Jsonclick ;
      private String edtProjetoMelhoria_Codigo_Jsonclick ;
      private String lblTextblockprojetomelhoria_projetocod_Internalname ;
      private String lblTextblockprojetomelhoria_projetocod_Jsonclick ;
      private String dynProjetoMelhoria_ProjetoCod_Internalname ;
      private String dynProjetoMelhoria_ProjetoCod_Jsonclick ;
      private String lblTextblockprojetomelhoria_sistemacod_Internalname ;
      private String lblTextblockprojetomelhoria_sistemacod_Jsonclick ;
      private String edtProjetoMelhoria_SistemaCod_Internalname ;
      private String edtProjetoMelhoria_SistemaCod_Jsonclick ;
      private String lblTextblockprojetomelhoria_fndadoscod_Internalname ;
      private String lblTextblockprojetomelhoria_fndadoscod_Jsonclick ;
      private String dynProjetoMelhoria_FnDadosCod_Internalname ;
      private String dynProjetoMelhoria_FnDadosCod_Jsonclick ;
      private String lblTextblockfuncaodados_nome_Internalname ;
      private String lblTextblockfuncaodados_nome_Jsonclick ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String lblTextblockprojetomelhoria_fnapfcod_Internalname ;
      private String lblTextblockprojetomelhoria_fnapfcod_Jsonclick ;
      private String edtProjetoMelhoria_FnAPFCod_Internalname ;
      private String edtProjetoMelhoria_FnAPFCod_Jsonclick ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String lblTextblockprojetomelhoria_fnnome_Internalname ;
      private String lblTextblockprojetomelhoria_fnnome_Jsonclick ;
      private String edtProjetoMelhoria_FnNome_Internalname ;
      private String A701ProjetoMelhoria_FnNome ;
      private String edtProjetoMelhoria_FnNome_Jsonclick ;
      private String lblTextblockprojetomelhoria_observacao_Internalname ;
      private String lblTextblockprojetomelhoria_observacao_Jsonclick ;
      private String edtProjetoMelhoria_Observacao_Internalname ;
      private String lblTextblockprojetomelhoria_inm_Internalname ;
      private String lblTextblockprojetomelhoria_inm_Jsonclick ;
      private String edtProjetoMelhoria_INM_Internalname ;
      private String A730ProjetoMelhoria_INM ;
      private String edtProjetoMelhoria_INM_Jsonclick ;
      private String lblTextblockprojetomelhoria_valor_Internalname ;
      private String lblTextblockprojetomelhoria_valor_Jsonclick ;
      private String edtProjetoMelhoria_Valor_Internalname ;
      private String edtProjetoMelhoria_Valor_Jsonclick ;
      private String lblTextblockprojetomelhoria_tipo_Internalname ;
      private String lblTextblockprojetomelhoria_tipo_Jsonclick ;
      private String cmbProjetomelhoria_Tipo_Internalname ;
      private String cmbProjetomelhoria_Tipo_Jsonclick ;
      private String lblTextblockprojetomelhoria_pfb_Internalname ;
      private String lblTextblockprojetomelhoria_pfb_Jsonclick ;
      private String edtProjetoMelhoria_PFB_Internalname ;
      private String edtProjetoMelhoria_PFB_Jsonclick ;
      private String lblTextblockprojetomelhoria_pfl_Internalname ;
      private String lblTextblockprojetomelhoria_pfl_Jsonclick ;
      private String edtProjetoMelhoria_PFL_Internalname ;
      private String edtProjetoMelhoria_PFL_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode93 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private String Z701ProjetoMelhoria_FnNome ;
      private bool entryPointCalled ;
      private bool n697ProjetoMelhoria_FnDadosCod ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private bool toggleJsOutput ;
      private bool n732Projetomelhoria_Tipo ;
      private bool wbErr ;
      private bool n369FuncaoDados_Nome ;
      private bool n166FuncaoAPF_Nome ;
      private bool n704ProjetoMelhoria_Observacao ;
      private bool n730ProjetoMelhoria_INM ;
      private bool n731ProjetoMelhoria_Valor ;
      private bool n733ProjetoMelhoria_PFB ;
      private bool n734ProjetoMelhoria_PFL ;
      private bool Gx_longc ;
      private String A704ProjetoMelhoria_Observacao ;
      private String Z704ProjetoMelhoria_Observacao ;
      private String A369FuncaoDados_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String Z369FuncaoDados_Nome ;
      private String Z166FuncaoAPF_Nome ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T002B8_A648Projeto_Codigo ;
      private String[] T002B8_A650Projeto_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynProjetoMelhoria_ProjetoCod ;
      private GXCombobox dynProjetoMelhoria_FnDadosCod ;
      private GXCombobox cmbProjetomelhoria_Tipo ;
      private String[] T002B6_A369FuncaoDados_Nome ;
      private bool[] T002B6_n369FuncaoDados_Nome ;
      private int[] T002B9_A736ProjetoMelhoria_Codigo ;
      private String[] T002B9_A369FuncaoDados_Nome ;
      private bool[] T002B9_n369FuncaoDados_Nome ;
      private String[] T002B9_A166FuncaoAPF_Nome ;
      private bool[] T002B9_n166FuncaoAPF_Nome ;
      private String[] T002B9_A704ProjetoMelhoria_Observacao ;
      private bool[] T002B9_n704ProjetoMelhoria_Observacao ;
      private String[] T002B9_A730ProjetoMelhoria_INM ;
      private bool[] T002B9_n730ProjetoMelhoria_INM ;
      private decimal[] T002B9_A731ProjetoMelhoria_Valor ;
      private bool[] T002B9_n731ProjetoMelhoria_Valor ;
      private short[] T002B9_A732Projetomelhoria_Tipo ;
      private bool[] T002B9_n732Projetomelhoria_Tipo ;
      private decimal[] T002B9_A733ProjetoMelhoria_PFB ;
      private bool[] T002B9_n733ProjetoMelhoria_PFB ;
      private decimal[] T002B9_A734ProjetoMelhoria_PFL ;
      private bool[] T002B9_n734ProjetoMelhoria_PFL ;
      private int[] T002B9_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B9_A695ProjetoMelhoria_SistemaCod ;
      private int[] T002B9_A697ProjetoMelhoria_FnDadosCod ;
      private bool[] T002B9_n697ProjetoMelhoria_FnDadosCod ;
      private int[] T002B9_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] T002B9_n698ProjetoMelhoria_FnAPFCod ;
      private int[] T002B4_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B5_A695ProjetoMelhoria_SistemaCod ;
      private String[] T002B7_A166FuncaoAPF_Nome ;
      private bool[] T002B7_n166FuncaoAPF_Nome ;
      private int[] T002B10_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B11_A695ProjetoMelhoria_SistemaCod ;
      private String[] T002B12_A369FuncaoDados_Nome ;
      private bool[] T002B12_n369FuncaoDados_Nome ;
      private String[] T002B13_A166FuncaoAPF_Nome ;
      private bool[] T002B13_n166FuncaoAPF_Nome ;
      private int[] T002B14_A736ProjetoMelhoria_Codigo ;
      private int[] T002B3_A736ProjetoMelhoria_Codigo ;
      private String[] T002B3_A704ProjetoMelhoria_Observacao ;
      private bool[] T002B3_n704ProjetoMelhoria_Observacao ;
      private String[] T002B3_A730ProjetoMelhoria_INM ;
      private bool[] T002B3_n730ProjetoMelhoria_INM ;
      private decimal[] T002B3_A731ProjetoMelhoria_Valor ;
      private bool[] T002B3_n731ProjetoMelhoria_Valor ;
      private short[] T002B3_A732Projetomelhoria_Tipo ;
      private bool[] T002B3_n732Projetomelhoria_Tipo ;
      private decimal[] T002B3_A733ProjetoMelhoria_PFB ;
      private bool[] T002B3_n733ProjetoMelhoria_PFB ;
      private decimal[] T002B3_A734ProjetoMelhoria_PFL ;
      private bool[] T002B3_n734ProjetoMelhoria_PFL ;
      private int[] T002B3_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B3_A695ProjetoMelhoria_SistemaCod ;
      private int[] T002B3_A697ProjetoMelhoria_FnDadosCod ;
      private bool[] T002B3_n697ProjetoMelhoria_FnDadosCod ;
      private int[] T002B3_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] T002B3_n698ProjetoMelhoria_FnAPFCod ;
      private int[] T002B15_A736ProjetoMelhoria_Codigo ;
      private int[] T002B16_A736ProjetoMelhoria_Codigo ;
      private int[] T002B2_A736ProjetoMelhoria_Codigo ;
      private String[] T002B2_A704ProjetoMelhoria_Observacao ;
      private bool[] T002B2_n704ProjetoMelhoria_Observacao ;
      private String[] T002B2_A730ProjetoMelhoria_INM ;
      private bool[] T002B2_n730ProjetoMelhoria_INM ;
      private decimal[] T002B2_A731ProjetoMelhoria_Valor ;
      private bool[] T002B2_n731ProjetoMelhoria_Valor ;
      private short[] T002B2_A732Projetomelhoria_Tipo ;
      private bool[] T002B2_n732Projetomelhoria_Tipo ;
      private decimal[] T002B2_A733ProjetoMelhoria_PFB ;
      private bool[] T002B2_n733ProjetoMelhoria_PFB ;
      private decimal[] T002B2_A734ProjetoMelhoria_PFL ;
      private bool[] T002B2_n734ProjetoMelhoria_PFL ;
      private int[] T002B2_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B2_A695ProjetoMelhoria_SistemaCod ;
      private int[] T002B2_A697ProjetoMelhoria_FnDadosCod ;
      private bool[] T002B2_n697ProjetoMelhoria_FnDadosCod ;
      private int[] T002B2_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] T002B2_n698ProjetoMelhoria_FnAPFCod ;
      private int[] T002B17_A736ProjetoMelhoria_Codigo ;
      private String[] T002B20_A369FuncaoDados_Nome ;
      private bool[] T002B20_n369FuncaoDados_Nome ;
      private String[] T002B21_A166FuncaoAPF_Nome ;
      private bool[] T002B21_n166FuncaoAPF_Nome ;
      private int[] T002B22_A722Baseline_Codigo ;
      private int[] T002B23_A736ProjetoMelhoria_Codigo ;
      private int[] T002B24_A648Projeto_Codigo ;
      private String[] T002B24_A650Projeto_Sigla ;
      private int[] T002B25_A697ProjetoMelhoria_FnDadosCod ;
      private bool[] T002B25_n697ProjetoMelhoria_FnDadosCod ;
      private String[] T002B25_A369FuncaoDados_Nome ;
      private bool[] T002B25_n369FuncaoDados_Nome ;
      private int[] T002B26_A648Projeto_Codigo ;
      private String[] T002B26_A650Projeto_Sigla ;
      private int[] T002B27_A694ProjetoMelhoria_ProjetoCod ;
      private int[] T002B28_A695ProjetoMelhoria_SistemaCod ;
      private GXWebForm Form ;
   }

   public class projetomelhoria__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002B8 ;
          prmT002B8 = new Object[] {
          } ;
          Object[] prmT002B9 ;
          prmT002B9 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B4 ;
          prmT002B4 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B5 ;
          prmT002B5 = new Object[] {
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B6 ;
          prmT002B6 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B7 ;
          prmT002B7 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B10 ;
          prmT002B10 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B11 ;
          prmT002B11 = new Object[] {
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B12 ;
          prmT002B12 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B13 ;
          prmT002B13 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B14 ;
          prmT002B14 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B3 ;
          prmT002B3 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B15 ;
          prmT002B15 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B16 ;
          prmT002B16 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B2 ;
          prmT002B2 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B17 ;
          prmT002B17 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoMelhoria_INM",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoMelhoria_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Projetomelhoria_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ProjetoMelhoria_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B18 ;
          prmT002B18 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoMelhoria_INM",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoMelhoria_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Projetomelhoria_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ProjetoMelhoria_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B19 ;
          prmT002B19 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B22 ;
          prmT002B22 = new Object[] {
          new Object[] {"@ProjetoMelhoria_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B23 ;
          prmT002B23 = new Object[] {
          } ;
          Object[] prmT002B24 ;
          prmT002B24 = new Object[] {
          } ;
          Object[] prmT002B25 ;
          prmT002B25 = new Object[] {
          } ;
          Object[] prmT002B26 ;
          prmT002B26 = new Object[] {
          } ;
          Object[] prmT002B27 ;
          prmT002B27 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B28 ;
          prmT002B28 = new Object[] {
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B20 ;
          prmT002B20 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002B21 ;
          prmT002B21 = new Object[] {
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002B2", "SELECT [ProjetoMelhoria_Codigo], [ProjetoMelhoria_Observacao], [ProjetoMelhoria_INM], [ProjetoMelhoria_Valor], [Projetomelhoria_Tipo], [ProjetoMelhoria_PFB], [ProjetoMelhoria_PFL], [ProjetoMelhoria_ProjetoCod] AS ProjetoMelhoria_ProjetoCod, [ProjetoMelhoria_SistemaCod] AS ProjetoMelhoria_SistemaCod, [ProjetoMelhoria_FnDadosCod] AS ProjetoMelhoria_FnDadosCod, [ProjetoMelhoria_FnAPFCod] AS ProjetoMelhoria_FnAPFCod FROM [ProjetoMelhoria] WITH (UPDLOCK) WHERE [ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B2,1,0,true,false )
             ,new CursorDef("T002B3", "SELECT [ProjetoMelhoria_Codigo], [ProjetoMelhoria_Observacao], [ProjetoMelhoria_INM], [ProjetoMelhoria_Valor], [Projetomelhoria_Tipo], [ProjetoMelhoria_PFB], [ProjetoMelhoria_PFL], [ProjetoMelhoria_ProjetoCod] AS ProjetoMelhoria_ProjetoCod, [ProjetoMelhoria_SistemaCod] AS ProjetoMelhoria_SistemaCod, [ProjetoMelhoria_FnDadosCod] AS ProjetoMelhoria_FnDadosCod, [ProjetoMelhoria_FnAPFCod] AS ProjetoMelhoria_FnAPFCod FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B3,1,0,true,false )
             ,new CursorDef("T002B4", "SELECT [Projeto_Codigo] AS ProjetoMelhoria_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @ProjetoMelhoria_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B4,1,0,true,false )
             ,new CursorDef("T002B5", "SELECT [Sistema_Codigo] AS ProjetoMelhoria_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoMelhoria_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B5,1,0,true,false )
             ,new CursorDef("T002B6", "SELECT [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @ProjetoMelhoria_FnDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B6,1,0,true,false )
             ,new CursorDef("T002B7", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @ProjetoMelhoria_FnAPFCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B7,1,0,true,false )
             ,new CursorDef("T002B8", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B8,0,0,true,false )
             ,new CursorDef("T002B9", "SELECT TM1.[ProjetoMelhoria_Codigo], T2.[FuncaoDados_Nome], T3.[FuncaoAPF_Nome], TM1.[ProjetoMelhoria_Observacao], TM1.[ProjetoMelhoria_INM], TM1.[ProjetoMelhoria_Valor], TM1.[Projetomelhoria_Tipo], TM1.[ProjetoMelhoria_PFB], TM1.[ProjetoMelhoria_PFL], TM1.[ProjetoMelhoria_ProjetoCod] AS ProjetoMelhoria_ProjetoCod, TM1.[ProjetoMelhoria_SistemaCod] AS ProjetoMelhoria_SistemaCod, TM1.[ProjetoMelhoria_FnDadosCod] AS ProjetoMelhoria_FnDadosCod, TM1.[ProjetoMelhoria_FnAPFCod] AS ProjetoMelhoria_FnAPFCod FROM (([ProjetoMelhoria] TM1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = TM1.[ProjetoMelhoria_FnDadosCod]) LEFT JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = TM1.[ProjetoMelhoria_FnAPFCod]) WHERE TM1.[ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo ORDER BY TM1.[ProjetoMelhoria_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002B9,100,0,true,false )
             ,new CursorDef("T002B10", "SELECT [Projeto_Codigo] AS ProjetoMelhoria_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @ProjetoMelhoria_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B10,1,0,true,false )
             ,new CursorDef("T002B11", "SELECT [Sistema_Codigo] AS ProjetoMelhoria_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoMelhoria_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B11,1,0,true,false )
             ,new CursorDef("T002B12", "SELECT [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @ProjetoMelhoria_FnDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B12,1,0,true,false )
             ,new CursorDef("T002B13", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @ProjetoMelhoria_FnAPFCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B13,1,0,true,false )
             ,new CursorDef("T002B14", "SELECT [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002B14,1,0,true,false )
             ,new CursorDef("T002B15", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE ( [ProjetoMelhoria_Codigo] > @ProjetoMelhoria_Codigo) ORDER BY [ProjetoMelhoria_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002B15,1,0,true,true )
             ,new CursorDef("T002B16", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE ( [ProjetoMelhoria_Codigo] < @ProjetoMelhoria_Codigo) ORDER BY [ProjetoMelhoria_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002B16,1,0,true,true )
             ,new CursorDef("T002B17", "INSERT INTO [ProjetoMelhoria]([ProjetoMelhoria_Observacao], [ProjetoMelhoria_INM], [ProjetoMelhoria_Valor], [Projetomelhoria_Tipo], [ProjetoMelhoria_PFB], [ProjetoMelhoria_PFL], [ProjetoMelhoria_ProjetoCod], [ProjetoMelhoria_SistemaCod], [ProjetoMelhoria_FnDadosCod], [ProjetoMelhoria_FnAPFCod]) VALUES(@ProjetoMelhoria_Observacao, @ProjetoMelhoria_INM, @ProjetoMelhoria_Valor, @Projetomelhoria_Tipo, @ProjetoMelhoria_PFB, @ProjetoMelhoria_PFL, @ProjetoMelhoria_ProjetoCod, @ProjetoMelhoria_SistemaCod, @ProjetoMelhoria_FnDadosCod, @ProjetoMelhoria_FnAPFCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002B17)
             ,new CursorDef("T002B18", "UPDATE [ProjetoMelhoria] SET [ProjetoMelhoria_Observacao]=@ProjetoMelhoria_Observacao, [ProjetoMelhoria_INM]=@ProjetoMelhoria_INM, [ProjetoMelhoria_Valor]=@ProjetoMelhoria_Valor, [Projetomelhoria_Tipo]=@Projetomelhoria_Tipo, [ProjetoMelhoria_PFB]=@ProjetoMelhoria_PFB, [ProjetoMelhoria_PFL]=@ProjetoMelhoria_PFL, [ProjetoMelhoria_ProjetoCod]=@ProjetoMelhoria_ProjetoCod, [ProjetoMelhoria_SistemaCod]=@ProjetoMelhoria_SistemaCod, [ProjetoMelhoria_FnDadosCod]=@ProjetoMelhoria_FnDadosCod, [ProjetoMelhoria_FnAPFCod]=@ProjetoMelhoria_FnAPFCod  WHERE [ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo", GxErrorMask.GX_NOMASK,prmT002B18)
             ,new CursorDef("T002B19", "DELETE FROM [ProjetoMelhoria]  WHERE [ProjetoMelhoria_Codigo] = @ProjetoMelhoria_Codigo", GxErrorMask.GX_NOMASK,prmT002B19)
             ,new CursorDef("T002B20", "SELECT [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @ProjetoMelhoria_FnDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B20,1,0,true,false )
             ,new CursorDef("T002B21", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @ProjetoMelhoria_FnAPFCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B21,1,0,true,false )
             ,new CursorDef("T002B22", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE [Baseline_ProjetoMelCod] = @ProjetoMelhoria_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B22,1,0,true,true )
             ,new CursorDef("T002B23", "SELECT [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) ORDER BY [ProjetoMelhoria_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002B23,100,0,true,false )
             ,new CursorDef("T002B24", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B24,0,0,true,false )
             ,new CursorDef("T002B25", "SELECT [FuncaoDados_Codigo] AS ProjetoMelhoria_FnDadosCod, [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B25,0,0,true,false )
             ,new CursorDef("T002B26", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B26,0,0,true,false )
             ,new CursorDef("T002B27", "SELECT [Projeto_Codigo] AS ProjetoMelhoria_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @ProjetoMelhoria_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B27,1,0,true,false )
             ,new CursorDef("T002B28", "SELECT [Sistema_Codigo] AS ProjetoMelhoria_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoMelhoria_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002B28,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                stmt.SetParameter(7, (int)parms[12]);
                stmt.SetParameter(8, (int)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[17]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                stmt.SetParameter(7, (int)parms[12]);
                stmt.SetParameter(8, (int)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[17]);
                }
                stmt.SetParameter(11, (int)parms[18]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
