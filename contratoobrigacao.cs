/*
               File: ContratoObrigacao
        Description: Obriga��es
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 2:23:6.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoobrigacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoObrigacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoObrigacao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOOBRIGACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoObrigacao_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Obriga��es", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoObrigacao_Item_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoobrigacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoobrigacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoObrigacao_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoObrigacao_Codigo = aP1_ContratoObrigacao_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_68_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table3_68_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1G55e( true) ;
         }
         else
         {
            wb_table1_2_1G55e( false) ;
         }
      }

      protected void wb_table3_68_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_68_1G55e( true) ;
         }
         else
         {
            wb_table3_68_1G55e( false) ;
         }
      }

      protected void wb_table2_5_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1G55e( true) ;
         }
         else
         {
            wb_table2_5_1G55e( false) ;
         }
      }

      protected void wb_table4_13_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "Dados do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoObrigacao.htm");
            wb_table5_17_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoobrigacao_item_Internalname, "Item", "", "", lblTextblockcontratoobrigacao_item_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoObrigacao_Item_Internalname, A322ContratoObrigacao_Item, StringUtil.RTrim( context.localUtil.Format( A322ContratoObrigacao_Item, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoObrigacao_Item_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoObrigacao_Item_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoobrigacao_subitem_Internalname, "Subitem", "", "", lblTextblockcontratoobrigacao_subitem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoObrigacao_SubItem_Internalname, A1425ContratoObrigacao_SubItem, StringUtil.RTrim( context.localUtil.Format( A1425ContratoObrigacao_SubItem, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoObrigacao_SubItem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoObrigacao_SubItem_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoobrigacao_datamaxima_Internalname, "Data M�x. de Cumprimento", "", "", lblTextblockcontratoobrigacao_datamaxima_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoObrigacao_DataMaxima_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoObrigacao_DataMaxima_Internalname, context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"), context.localUtil.Format( A1424ContratoObrigacao_DataMaxima, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoObrigacao_DataMaxima_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoObrigacao_DataMaxima_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoObrigacao.htm");
            GxWebStd.gx_bitmap( context, edtContratoObrigacao_DataMaxima_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoObrigacao_DataMaxima_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoobrigacao_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoobrigacao_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoObrigacao_Descricao_Internalname, A323ContratoObrigacao_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, 1, edtContratoObrigacao_Descricao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1000", -1, "", "", -1, true, "Observacao", "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1G55e( true) ;
         }
         else
         {
            wb_table4_13_1G55e( false) ;
         }
      }

      protected void wb_table5_17_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_22_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table6_22_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_38_1G55( true) ;
         }
         return  ;
      }

      protected void wb_table7_38_1G55e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_1G55e( true) ;
         }
         else
         {
            wb_table5_17_1G55e( false) ;
         }
      }

      protected void wb_table7_38_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_38_1G55e( true) ;
         }
         else
         {
            wb_table7_38_1G55e( false) ;
         }
      }

      protected void wb_table6_22_1G55( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoObrigacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_1G55e( true) ;
         }
         else
         {
            wb_table6_22_1G55e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111G2 */
         E111G2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               A322ContratoObrigacao_Item = cgiGet( edtContratoObrigacao_Item_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A322ContratoObrigacao_Item", A322ContratoObrigacao_Item);
               A1425ContratoObrigacao_SubItem = cgiGet( edtContratoObrigacao_SubItem_Internalname);
               n1425ContratoObrigacao_SubItem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1425ContratoObrigacao_SubItem", A1425ContratoObrigacao_SubItem);
               n1425ContratoObrigacao_SubItem = (String.IsNullOrEmpty(StringUtil.RTrim( A1425ContratoObrigacao_SubItem)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoObrigacao_DataMaxima_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data M�xima de Cumprimento"}), 1, "CONTRATOOBRIGACAO_DATAMAXIMA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoObrigacao_DataMaxima_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
                  n1424ContratoObrigacao_DataMaxima = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1424ContratoObrigacao_DataMaxima", context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"));
               }
               else
               {
                  A1424ContratoObrigacao_DataMaxima = context.localUtil.CToD( cgiGet( edtContratoObrigacao_DataMaxima_Internalname), 2);
                  n1424ContratoObrigacao_DataMaxima = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1424ContratoObrigacao_DataMaxima", context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"));
               }
               n1424ContratoObrigacao_DataMaxima = ((DateTime.MinValue==A1424ContratoObrigacao_DataMaxima) ? true : false);
               A323ContratoObrigacao_Descricao = cgiGet( edtContratoObrigacao_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A323ContratoObrigacao_Descricao", A323ContratoObrigacao_Descricao);
               /* Read saved values. */
               Z321ContratoObrigacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z321ContratoObrigacao_Codigo"), ",", "."));
               Z322ContratoObrigacao_Item = cgiGet( "Z322ContratoObrigacao_Item");
               Z1425ContratoObrigacao_SubItem = cgiGet( "Z1425ContratoObrigacao_SubItem");
               n1425ContratoObrigacao_SubItem = (String.IsNullOrEmpty(StringUtil.RTrim( A1425ContratoObrigacao_SubItem)) ? true : false);
               Z1424ContratoObrigacao_DataMaxima = context.localUtil.CToD( cgiGet( "Z1424ContratoObrigacao_DataMaxima"), 0);
               n1424ContratoObrigacao_DataMaxima = ((DateTime.MinValue==A1424ContratoObrigacao_DataMaxima) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               AV7ContratoObrigacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOOBRIGACAO_CODIGO"), ",", "."));
               A321ContratoObrigacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOOBRIGACAO_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoObrigacao";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A321ContratoObrigacao_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoobrigacao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoobrigacao:[SecurityCheckFailed value for]"+"ContratoObrigacao_Codigo:"+context.localUtil.Format( (decimal)(A321ContratoObrigacao_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A321ContratoObrigacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A321ContratoObrigacao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode55 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode55;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound55 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1G0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111G2 */
                           E111G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121G2 */
                           E121G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121G2 */
            E121G2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1G55( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1G55( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1G0( )
      {
         BeforeValidate1G55( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1G55( ) ;
            }
            else
            {
               CheckExtendedTable1G55( ) ;
               CloseExtendedTableCursors1G55( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1G0( )
      {
      }

      protected void E111G2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E121G2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoobrigacao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1G55( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z322ContratoObrigacao_Item = T001G3_A322ContratoObrigacao_Item[0];
               Z1425ContratoObrigacao_SubItem = T001G3_A1425ContratoObrigacao_SubItem[0];
               Z1424ContratoObrigacao_DataMaxima = T001G3_A1424ContratoObrigacao_DataMaxima[0];
            }
            else
            {
               Z322ContratoObrigacao_Item = A322ContratoObrigacao_Item;
               Z1425ContratoObrigacao_SubItem = A1425ContratoObrigacao_SubItem;
               Z1424ContratoObrigacao_DataMaxima = A1424ContratoObrigacao_DataMaxima;
            }
         }
         if ( GX_JID == -9 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z321ContratoObrigacao_Codigo = A321ContratoObrigacao_Codigo;
            Z322ContratoObrigacao_Item = A322ContratoObrigacao_Item;
            Z323ContratoObrigacao_Descricao = A323ContratoObrigacao_Descricao;
            Z1425ContratoObrigacao_SubItem = A1425ContratoObrigacao_SubItem;
            Z1424ContratoObrigacao_DataMaxima = A1424ContratoObrigacao_DataMaxima;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         AV13Pgmname = "ContratoObrigacao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoObrigacao_Codigo) )
         {
            A321ContratoObrigacao_Codigo = AV7ContratoObrigacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A321ContratoObrigacao_Codigo), 6, 0)));
         }
         /* Using cursor T001G4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T001G4_A39Contratada_Codigo[0];
         A77Contrato_Numero = T001G4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T001G4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T001G4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T001G4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         pr_default.close(2);
         /* Using cursor T001G5 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T001G5_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T001G6 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T001G6_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T001G6_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T001G6_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T001G6_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
            {
               A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1G55( )
      {
         /* Using cursor T001G7 */
         pr_default.execute(5, new Object[] {A74Contrato_Codigo, A321ContratoObrigacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound55 = 1;
            A39Contratada_Codigo = T001G7_A39Contratada_Codigo[0];
            A77Contrato_Numero = T001G7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001G7_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001G7_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001G7_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T001G7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001G7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001G7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001G7_n42Contratada_PessoaCNPJ[0];
            A322ContratoObrigacao_Item = T001G7_A322ContratoObrigacao_Item[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A322ContratoObrigacao_Item", A322ContratoObrigacao_Item);
            A323ContratoObrigacao_Descricao = T001G7_A323ContratoObrigacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A323ContratoObrigacao_Descricao", A323ContratoObrigacao_Descricao);
            A1425ContratoObrigacao_SubItem = T001G7_A1425ContratoObrigacao_SubItem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1425ContratoObrigacao_SubItem", A1425ContratoObrigacao_SubItem);
            n1425ContratoObrigacao_SubItem = T001G7_n1425ContratoObrigacao_SubItem[0];
            A1424ContratoObrigacao_DataMaxima = T001G7_A1424ContratoObrigacao_DataMaxima[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1424ContratoObrigacao_DataMaxima", context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"));
            n1424ContratoObrigacao_DataMaxima = T001G7_n1424ContratoObrigacao_DataMaxima[0];
            A40Contratada_PessoaCod = T001G7_A40Contratada_PessoaCod[0];
            ZM1G55( -9) ;
         }
         pr_default.close(5);
         OnLoadActions1G55( ) ;
      }

      protected void OnLoadActions1G55( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable1G55( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A322ContratoObrigacao_Item)) )
         {
            GX_msglist.addItem("Item da Minuta � obrigat�rio.", 1, "CONTRATOOBRIGACAO_ITEM");
            AnyError = 1;
            GX_FocusControl = edtContratoObrigacao_Item_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A323ContratoObrigacao_Descricao)) )
         {
            GX_msglist.addItem("Descri��o � obrigat�rio.", 1, "CONTRATOOBRIGACAO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtContratoObrigacao_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1424ContratoObrigacao_DataMaxima) || ( A1424ContratoObrigacao_DataMaxima >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data M�xima de Cumprimento fora do intervalo", "OutOfRange", 1, "CONTRATOOBRIGACAO_DATAMAXIMA");
            AnyError = 1;
            GX_FocusControl = edtContratoObrigacao_DataMaxima_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors1G55( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1G55( )
      {
         /* Using cursor T001G8 */
         pr_default.execute(6, new Object[] {A321ContratoObrigacao_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound55 = 1;
         }
         else
         {
            RcdFound55 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001G3 */
         pr_default.execute(1, new Object[] {A321ContratoObrigacao_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T001G3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
         {
            ZM1G55( 9) ;
            RcdFound55 = 1;
            A321ContratoObrigacao_Codigo = T001G3_A321ContratoObrigacao_Codigo[0];
            A322ContratoObrigacao_Item = T001G3_A322ContratoObrigacao_Item[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A322ContratoObrigacao_Item", A322ContratoObrigacao_Item);
            A323ContratoObrigacao_Descricao = T001G3_A323ContratoObrigacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A323ContratoObrigacao_Descricao", A323ContratoObrigacao_Descricao);
            A1425ContratoObrigacao_SubItem = T001G3_A1425ContratoObrigacao_SubItem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1425ContratoObrigacao_SubItem", A1425ContratoObrigacao_SubItem);
            n1425ContratoObrigacao_SubItem = T001G3_n1425ContratoObrigacao_SubItem[0];
            A1424ContratoObrigacao_DataMaxima = T001G3_A1424ContratoObrigacao_DataMaxima[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1424ContratoObrigacao_DataMaxima", context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"));
            n1424ContratoObrigacao_DataMaxima = T001G3_n1424ContratoObrigacao_DataMaxima[0];
            Z321ContratoObrigacao_Codigo = A321ContratoObrigacao_Codigo;
            sMode55 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1G55( ) ;
            if ( AnyError == 1 )
            {
               RcdFound55 = 0;
               InitializeNonKey1G55( ) ;
            }
            Gx_mode = sMode55;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound55 = 0;
            InitializeNonKey1G55( ) ;
            sMode55 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode55;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1G55( ) ;
         if ( RcdFound55 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound55 = 0;
         /* Using cursor T001G9 */
         pr_default.execute(7, new Object[] {A321ContratoObrigacao_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001G9_A321ContratoObrigacao_Codigo[0] < A321ContratoObrigacao_Codigo ) ) && ( T001G9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001G9_A321ContratoObrigacao_Codigo[0] > A321ContratoObrigacao_Codigo ) ) && ( T001G9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A321ContratoObrigacao_Codigo = T001G9_A321ContratoObrigacao_Codigo[0];
               RcdFound55 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound55 = 0;
         /* Using cursor T001G10 */
         pr_default.execute(8, new Object[] {A321ContratoObrigacao_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001G10_A321ContratoObrigacao_Codigo[0] > A321ContratoObrigacao_Codigo ) ) && ( T001G10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001G10_A321ContratoObrigacao_Codigo[0] < A321ContratoObrigacao_Codigo ) ) && ( T001G10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A321ContratoObrigacao_Codigo = T001G10_A321ContratoObrigacao_Codigo[0];
               RcdFound55 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1G55( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoObrigacao_Item_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1G55( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound55 == 1 )
            {
               if ( A321ContratoObrigacao_Codigo != Z321ContratoObrigacao_Codigo )
               {
                  A321ContratoObrigacao_Codigo = Z321ContratoObrigacao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A321ContratoObrigacao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoObrigacao_Item_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1G55( ) ;
                  GX_FocusControl = edtContratoObrigacao_Item_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A321ContratoObrigacao_Codigo != Z321ContratoObrigacao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoObrigacao_Item_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1G55( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoObrigacao_Item_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1G55( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A321ContratoObrigacao_Codigo != Z321ContratoObrigacao_Codigo )
         {
            A321ContratoObrigacao_Codigo = Z321ContratoObrigacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A321ContratoObrigacao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoObrigacao_Item_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1G55( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001G2 */
            pr_default.execute(0, new Object[] {A321ContratoObrigacao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoObrigacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z322ContratoObrigacao_Item, T001G2_A322ContratoObrigacao_Item[0]) != 0 ) || ( StringUtil.StrCmp(Z1425ContratoObrigacao_SubItem, T001G2_A1425ContratoObrigacao_SubItem[0]) != 0 ) || ( Z1424ContratoObrigacao_DataMaxima != T001G2_A1424ContratoObrigacao_DataMaxima[0] ) )
            {
               if ( StringUtil.StrCmp(Z322ContratoObrigacao_Item, T001G2_A322ContratoObrigacao_Item[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoobrigacao:[seudo value changed for attri]"+"ContratoObrigacao_Item");
                  GXUtil.WriteLogRaw("Old: ",Z322ContratoObrigacao_Item);
                  GXUtil.WriteLogRaw("Current: ",T001G2_A322ContratoObrigacao_Item[0]);
               }
               if ( StringUtil.StrCmp(Z1425ContratoObrigacao_SubItem, T001G2_A1425ContratoObrigacao_SubItem[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoobrigacao:[seudo value changed for attri]"+"ContratoObrigacao_SubItem");
                  GXUtil.WriteLogRaw("Old: ",Z1425ContratoObrigacao_SubItem);
                  GXUtil.WriteLogRaw("Current: ",T001G2_A1425ContratoObrigacao_SubItem[0]);
               }
               if ( Z1424ContratoObrigacao_DataMaxima != T001G2_A1424ContratoObrigacao_DataMaxima[0] )
               {
                  GXUtil.WriteLog("contratoobrigacao:[seudo value changed for attri]"+"ContratoObrigacao_DataMaxima");
                  GXUtil.WriteLogRaw("Old: ",Z1424ContratoObrigacao_DataMaxima);
                  GXUtil.WriteLogRaw("Current: ",T001G2_A1424ContratoObrigacao_DataMaxima[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoObrigacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1G55( )
      {
         BeforeValidate1G55( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1G55( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1G55( 0) ;
            CheckOptimisticConcurrency1G55( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1G55( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1G55( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001G11 */
                     pr_default.execute(9, new Object[] {A74Contrato_Codigo, A322ContratoObrigacao_Item, A323ContratoObrigacao_Descricao, n1425ContratoObrigacao_SubItem, A1425ContratoObrigacao_SubItem, n1424ContratoObrigacao_DataMaxima, A1424ContratoObrigacao_DataMaxima});
                     A321ContratoObrigacao_Codigo = T001G11_A321ContratoObrigacao_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoObrigacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1G0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1G55( ) ;
            }
            EndLevel1G55( ) ;
         }
         CloseExtendedTableCursors1G55( ) ;
      }

      protected void Update1G55( )
      {
         BeforeValidate1G55( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1G55( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1G55( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1G55( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1G55( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001G12 */
                     pr_default.execute(10, new Object[] {A74Contrato_Codigo, A322ContratoObrigacao_Item, A323ContratoObrigacao_Descricao, n1425ContratoObrigacao_SubItem, A1425ContratoObrigacao_SubItem, n1424ContratoObrigacao_DataMaxima, A1424ContratoObrigacao_DataMaxima, A321ContratoObrigacao_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoObrigacao") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoObrigacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1G55( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1G55( ) ;
         }
         CloseExtendedTableCursors1G55( ) ;
      }

      protected void DeferredUpdate1G55( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1G55( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1G55( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1G55( ) ;
            AfterConfirm1G55( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1G55( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001G13 */
                  pr_default.execute(11, new Object[] {A321ContratoObrigacao_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoObrigacao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode55 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1G55( ) ;
         Gx_mode = sMode55;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1G55( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1G55( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1G55( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoObrigacao");
            if ( AnyError == 0 )
            {
               ConfirmValues1G0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoObrigacao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1G55( )
      {
         /* Scan By routine */
         /* Using cursor T001G14 */
         pr_default.execute(12, new Object[] {A74Contrato_Codigo});
         RcdFound55 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound55 = 1;
            A321ContratoObrigacao_Codigo = T001G14_A321ContratoObrigacao_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1G55( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound55 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound55 = 1;
            A321ContratoObrigacao_Codigo = T001G14_A321ContratoObrigacao_Codigo[0];
         }
      }

      protected void ScanEnd1G55( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm1G55( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1G55( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1G55( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1G55( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1G55( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1G55( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1G55( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoObrigacao_Item_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoObrigacao_Item_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoObrigacao_Item_Enabled), 5, 0)));
         edtContratoObrigacao_SubItem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoObrigacao_SubItem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoObrigacao_SubItem_Enabled), 5, 0)));
         edtContratoObrigacao_DataMaxima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoObrigacao_DataMaxima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoObrigacao_DataMaxima_Enabled), 5, 0)));
         edtContratoObrigacao_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoObrigacao_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoObrigacao_Descricao_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1G0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020429223829");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoobrigacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoObrigacao_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z321ContratoObrigacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z322ContratoObrigacao_Item", Z322ContratoObrigacao_Item);
         GxWebStd.gx_hidden_field( context, "Z1425ContratoObrigacao_SubItem", Z1425ContratoObrigacao_SubItem);
         GxWebStd.gx_hidden_field( context, "Z1424ContratoObrigacao_DataMaxima", context.localUtil.DToC( Z1424ContratoObrigacao_DataMaxima, 0, "/"));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOOBRIGACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoObrigacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOBRIGACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A321ContratoObrigacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOOBRIGACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoObrigacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoObrigacao";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A321ContratoObrigacao_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoobrigacao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoobrigacao:[SendSecurityCheck value for]"+"ContratoObrigacao_Codigo:"+context.localUtil.Format( (decimal)(A321ContratoObrigacao_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoobrigacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoObrigacao_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoObrigacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Obriga��es" ;
      }

      protected void InitializeNonKey1G55( )
      {
         A322ContratoObrigacao_Item = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A322ContratoObrigacao_Item", A322ContratoObrigacao_Item);
         A323ContratoObrigacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A323ContratoObrigacao_Descricao", A323ContratoObrigacao_Descricao);
         A1425ContratoObrigacao_SubItem = "";
         n1425ContratoObrigacao_SubItem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1425ContratoObrigacao_SubItem", A1425ContratoObrigacao_SubItem);
         n1425ContratoObrigacao_SubItem = (String.IsNullOrEmpty(StringUtil.RTrim( A1425ContratoObrigacao_SubItem)) ? true : false);
         A1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
         n1424ContratoObrigacao_DataMaxima = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1424ContratoObrigacao_DataMaxima", context.localUtil.Format(A1424ContratoObrigacao_DataMaxima, "99/99/99"));
         n1424ContratoObrigacao_DataMaxima = ((DateTime.MinValue==A1424ContratoObrigacao_DataMaxima) ? true : false);
         Z322ContratoObrigacao_Item = "";
         Z1425ContratoObrigacao_SubItem = "";
         Z1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
      }

      protected void InitAll1G55( )
      {
         A321ContratoObrigacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A321ContratoObrigacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A321ContratoObrigacao_Codigo), 6, 0)));
         InitializeNonKey1G55( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020429223862");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoobrigacao.js", "?2020429223862");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratoobrigacao_item_Internalname = "TEXTBLOCKCONTRATOOBRIGACAO_ITEM";
         edtContratoObrigacao_Item_Internalname = "CONTRATOOBRIGACAO_ITEM";
         lblTextblockcontratoobrigacao_subitem_Internalname = "TEXTBLOCKCONTRATOOBRIGACAO_SUBITEM";
         edtContratoObrigacao_SubItem_Internalname = "CONTRATOOBRIGACAO_SUBITEM";
         lblTextblockcontratoobrigacao_datamaxima_Internalname = "TEXTBLOCKCONTRATOOBRIGACAO_DATAMAXIMA";
         edtContratoObrigacao_DataMaxima_Internalname = "CONTRATOOBRIGACAO_DATAMAXIMA";
         lblTextblockcontratoobrigacao_descricao_Internalname = "TEXTBLOCKCONTRATOOBRIGACAO_DESCRICAO";
         edtContratoObrigacao_Descricao_Internalname = "CONTRATOOBRIGACAO_DESCRICAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Obriga��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Obriga��es";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContratoObrigacao_Descricao_Enabled = 1;
         edtContratoObrigacao_DataMaxima_Jsonclick = "";
         edtContratoObrigacao_DataMaxima_Enabled = 1;
         edtContratoObrigacao_SubItem_Jsonclick = "";
         edtContratoObrigacao_SubItem_Enabled = 1;
         edtContratoObrigacao_Item_Jsonclick = "";
         edtContratoObrigacao_Item_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoObrigacao_Codigo',fld:'vCONTRATOOBRIGACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121G2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z322ContratoObrigacao_Item = "";
         Z1425ContratoObrigacao_SubItem = "";
         Z1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratoobrigacao_item_Jsonclick = "";
         A322ContratoObrigacao_Item = "";
         lblTextblockcontratoobrigacao_subitem_Jsonclick = "";
         A1425ContratoObrigacao_SubItem = "";
         lblTextblockcontratoobrigacao_datamaxima_Jsonclick = "";
         A1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
         lblTextblockcontratoobrigacao_descricao_Jsonclick = "";
         A323ContratoObrigacao_Descricao = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode55 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z323ContratoObrigacao_Descricao = "";
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T001G4_A39Contratada_Codigo = new int[1] ;
         T001G4_A77Contrato_Numero = new String[] {""} ;
         T001G4_A78Contrato_NumeroAta = new String[] {""} ;
         T001G4_n78Contrato_NumeroAta = new bool[] {false} ;
         T001G4_A79Contrato_Ano = new short[1] ;
         T001G5_A40Contratada_PessoaCod = new int[1] ;
         T001G6_A41Contratada_PessoaNom = new String[] {""} ;
         T001G6_n41Contratada_PessoaNom = new bool[] {false} ;
         T001G6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001G6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001G7_A39Contratada_Codigo = new int[1] ;
         T001G7_A74Contrato_Codigo = new int[1] ;
         T001G7_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G7_A77Contrato_Numero = new String[] {""} ;
         T001G7_A78Contrato_NumeroAta = new String[] {""} ;
         T001G7_n78Contrato_NumeroAta = new bool[] {false} ;
         T001G7_A79Contrato_Ano = new short[1] ;
         T001G7_A41Contratada_PessoaNom = new String[] {""} ;
         T001G7_n41Contratada_PessoaNom = new bool[] {false} ;
         T001G7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001G7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001G7_A322ContratoObrigacao_Item = new String[] {""} ;
         T001G7_A323ContratoObrigacao_Descricao = new String[] {""} ;
         T001G7_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         T001G7_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         T001G7_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         T001G7_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         T001G7_A40Contratada_PessoaCod = new int[1] ;
         T001G8_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G3_A74Contrato_Codigo = new int[1] ;
         T001G3_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G3_A322ContratoObrigacao_Item = new String[] {""} ;
         T001G3_A323ContratoObrigacao_Descricao = new String[] {""} ;
         T001G3_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         T001G3_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         T001G3_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         T001G3_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         T001G9_A74Contrato_Codigo = new int[1] ;
         T001G9_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G10_A74Contrato_Codigo = new int[1] ;
         T001G10_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G2_A74Contrato_Codigo = new int[1] ;
         T001G2_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G2_A322ContratoObrigacao_Item = new String[] {""} ;
         T001G2_A323ContratoObrigacao_Descricao = new String[] {""} ;
         T001G2_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         T001G2_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         T001G2_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         T001G2_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         T001G11_A321ContratoObrigacao_Codigo = new int[1] ;
         T001G14_A321ContratoObrigacao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoobrigacao__default(),
            new Object[][] {
                new Object[] {
               T001G2_A74Contrato_Codigo, T001G2_A321ContratoObrigacao_Codigo, T001G2_A322ContratoObrigacao_Item, T001G2_A323ContratoObrigacao_Descricao, T001G2_A1425ContratoObrigacao_SubItem, T001G2_n1425ContratoObrigacao_SubItem, T001G2_A1424ContratoObrigacao_DataMaxima, T001G2_n1424ContratoObrigacao_DataMaxima
               }
               , new Object[] {
               T001G3_A74Contrato_Codigo, T001G3_A321ContratoObrigacao_Codigo, T001G3_A322ContratoObrigacao_Item, T001G3_A323ContratoObrigacao_Descricao, T001G3_A1425ContratoObrigacao_SubItem, T001G3_n1425ContratoObrigacao_SubItem, T001G3_A1424ContratoObrigacao_DataMaxima, T001G3_n1424ContratoObrigacao_DataMaxima
               }
               , new Object[] {
               T001G4_A39Contratada_Codigo, T001G4_A77Contrato_Numero, T001G4_A78Contrato_NumeroAta, T001G4_n78Contrato_NumeroAta, T001G4_A79Contrato_Ano
               }
               , new Object[] {
               T001G5_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001G6_A41Contratada_PessoaNom, T001G6_n41Contratada_PessoaNom, T001G6_A42Contratada_PessoaCNPJ, T001G6_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001G7_A39Contratada_Codigo, T001G7_A74Contrato_Codigo, T001G7_A321ContratoObrigacao_Codigo, T001G7_A77Contrato_Numero, T001G7_A78Contrato_NumeroAta, T001G7_n78Contrato_NumeroAta, T001G7_A79Contrato_Ano, T001G7_A41Contratada_PessoaNom, T001G7_n41Contratada_PessoaNom, T001G7_A42Contratada_PessoaCNPJ,
               T001G7_n42Contratada_PessoaCNPJ, T001G7_A322ContratoObrigacao_Item, T001G7_A323ContratoObrigacao_Descricao, T001G7_A1425ContratoObrigacao_SubItem, T001G7_n1425ContratoObrigacao_SubItem, T001G7_A1424ContratoObrigacao_DataMaxima, T001G7_n1424ContratoObrigacao_DataMaxima, T001G7_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001G8_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               T001G9_A74Contrato_Codigo, T001G9_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               T001G10_A74Contrato_Codigo, T001G10_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               T001G11_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001G14_A321ContratoObrigacao_Codigo
               }
            }
         );
         N74Contrato_Codigo = 0;
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV13Pgmname = "ContratoObrigacao";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short RcdFound55 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7ContratoObrigacao_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z321ContratoObrigacao_Codigo ;
      private int N74Contrato_Codigo ;
      private int AV7ContratoObrigacao_Codigo ;
      private int A74Contrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoObrigacao_Item_Enabled ;
      private int edtContratoObrigacao_SubItem_Enabled ;
      private int edtContratoObrigacao_DataMaxima_Enabled ;
      private int edtContratoObrigacao_Descricao_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A321ContratoObrigacao_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV14GXV1 ;
      private int Z74Contrato_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoObrigacao_Item_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratoobrigacao_item_Internalname ;
      private String lblTextblockcontratoobrigacao_item_Jsonclick ;
      private String edtContratoObrigacao_Item_Jsonclick ;
      private String lblTextblockcontratoobrigacao_subitem_Internalname ;
      private String lblTextblockcontratoobrigacao_subitem_Jsonclick ;
      private String edtContratoObrigacao_SubItem_Internalname ;
      private String edtContratoObrigacao_SubItem_Jsonclick ;
      private String lblTextblockcontratoobrigacao_datamaxima_Internalname ;
      private String lblTextblockcontratoobrigacao_datamaxima_Jsonclick ;
      private String edtContratoObrigacao_DataMaxima_Internalname ;
      private String edtContratoObrigacao_DataMaxima_Jsonclick ;
      private String lblTextblockcontratoobrigacao_descricao_Internalname ;
      private String lblTextblockcontratoobrigacao_descricao_Jsonclick ;
      private String edtContratoObrigacao_Descricao_Internalname ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode55 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1424ContratoObrigacao_DataMaxima ;
      private DateTime A1424ContratoObrigacao_DataMaxima ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n1425ContratoObrigacao_SubItem ;
      private bool n1424ContratoObrigacao_DataMaxima ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A323ContratoObrigacao_Descricao ;
      private String Z323ContratoObrigacao_Descricao ;
      private String Z322ContratoObrigacao_Item ;
      private String Z1425ContratoObrigacao_SubItem ;
      private String A322ContratoObrigacao_Item ;
      private String A1425ContratoObrigacao_SubItem ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] T001G4_A39Contratada_Codigo ;
      private String[] T001G4_A77Contrato_Numero ;
      private String[] T001G4_A78Contrato_NumeroAta ;
      private bool[] T001G4_n78Contrato_NumeroAta ;
      private short[] T001G4_A79Contrato_Ano ;
      private int[] T001G5_A40Contratada_PessoaCod ;
      private String[] T001G6_A41Contratada_PessoaNom ;
      private bool[] T001G6_n41Contratada_PessoaNom ;
      private String[] T001G6_A42Contratada_PessoaCNPJ ;
      private bool[] T001G6_n42Contratada_PessoaCNPJ ;
      private int[] T001G7_A39Contratada_Codigo ;
      private int[] T001G7_A74Contrato_Codigo ;
      private int[] T001G7_A321ContratoObrigacao_Codigo ;
      private String[] T001G7_A77Contrato_Numero ;
      private String[] T001G7_A78Contrato_NumeroAta ;
      private bool[] T001G7_n78Contrato_NumeroAta ;
      private short[] T001G7_A79Contrato_Ano ;
      private String[] T001G7_A41Contratada_PessoaNom ;
      private bool[] T001G7_n41Contratada_PessoaNom ;
      private String[] T001G7_A42Contratada_PessoaCNPJ ;
      private bool[] T001G7_n42Contratada_PessoaCNPJ ;
      private String[] T001G7_A322ContratoObrigacao_Item ;
      private String[] T001G7_A323ContratoObrigacao_Descricao ;
      private String[] T001G7_A1425ContratoObrigacao_SubItem ;
      private bool[] T001G7_n1425ContratoObrigacao_SubItem ;
      private DateTime[] T001G7_A1424ContratoObrigacao_DataMaxima ;
      private bool[] T001G7_n1424ContratoObrigacao_DataMaxima ;
      private int[] T001G7_A40Contratada_PessoaCod ;
      private int[] T001G8_A321ContratoObrigacao_Codigo ;
      private int[] T001G3_A74Contrato_Codigo ;
      private int[] T001G3_A321ContratoObrigacao_Codigo ;
      private String[] T001G3_A322ContratoObrigacao_Item ;
      private String[] T001G3_A323ContratoObrigacao_Descricao ;
      private String[] T001G3_A1425ContratoObrigacao_SubItem ;
      private bool[] T001G3_n1425ContratoObrigacao_SubItem ;
      private DateTime[] T001G3_A1424ContratoObrigacao_DataMaxima ;
      private bool[] T001G3_n1424ContratoObrigacao_DataMaxima ;
      private int[] T001G9_A74Contrato_Codigo ;
      private int[] T001G9_A321ContratoObrigacao_Codigo ;
      private int[] T001G10_A74Contrato_Codigo ;
      private int[] T001G10_A321ContratoObrigacao_Codigo ;
      private int[] T001G2_A74Contrato_Codigo ;
      private int[] T001G2_A321ContratoObrigacao_Codigo ;
      private String[] T001G2_A322ContratoObrigacao_Item ;
      private String[] T001G2_A323ContratoObrigacao_Descricao ;
      private String[] T001G2_A1425ContratoObrigacao_SubItem ;
      private bool[] T001G2_n1425ContratoObrigacao_SubItem ;
      private DateTime[] T001G2_A1424ContratoObrigacao_DataMaxima ;
      private bool[] T001G2_n1424ContratoObrigacao_DataMaxima ;
      private int[] T001G11_A321ContratoObrigacao_Codigo ;
      private int[] T001G14_A321ContratoObrigacao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratoobrigacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001G4 ;
          prmT001G4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G5 ;
          prmT001G5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G6 ;
          prmT001G6 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G7 ;
          prmT001G7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G8 ;
          prmT001G8 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G3 ;
          prmT001G3 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G9 ;
          prmT001G9 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G10 ;
          prmT001G10 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G2 ;
          prmT001G2 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G11 ;
          prmT001G11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_Descricao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT001G12 ;
          prmT001G12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_Descricao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G13 ;
          prmT001G13 = new Object[] {
          new Object[] {"@ContratoObrigacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001G14 ;
          prmT001G14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001G2", "SELECT [Contrato_Codigo], [ContratoObrigacao_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_Descricao], [ContratoObrigacao_SubItem], [ContratoObrigacao_DataMaxima] FROM [ContratoObrigacao] WITH (UPDLOCK) WHERE [ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001G2,1,0,true,false )
             ,new CursorDef("T001G3", "SELECT [Contrato_Codigo], [ContratoObrigacao_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_Descricao], [ContratoObrigacao_SubItem], [ContratoObrigacao_DataMaxima] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001G3,1,0,true,false )
             ,new CursorDef("T001G4", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001G4,1,0,true,false )
             ,new CursorDef("T001G5", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001G5,1,0,true,false )
             ,new CursorDef("T001G6", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001G6,1,0,true,false )
             ,new CursorDef("T001G7", "SELECT T2.[Contratada_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoObrigacao_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoObrigacao_Item], TM1.[ContratoObrigacao_Descricao], TM1.[ContratoObrigacao_SubItem], TM1.[ContratoObrigacao_DataMaxima], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoObrigacao] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo and TM1.[ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo ORDER BY TM1.[ContratoObrigacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001G7,100,0,true,false )
             ,new CursorDef("T001G8", "SELECT [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001G8,1,0,true,false )
             ,new CursorDef("T001G9", "SELECT TOP 1 [Contrato_Codigo], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE ( [ContratoObrigacao_Codigo] > @ContratoObrigacao_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoObrigacao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001G9,1,0,true,true )
             ,new CursorDef("T001G10", "SELECT TOP 1 [Contrato_Codigo], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE ( [ContratoObrigacao_Codigo] < @ContratoObrigacao_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoObrigacao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001G10,1,0,true,true )
             ,new CursorDef("T001G11", "INSERT INTO [ContratoObrigacao]([Contrato_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_Descricao], [ContratoObrigacao_SubItem], [ContratoObrigacao_DataMaxima]) VALUES(@Contrato_Codigo, @ContratoObrigacao_Item, @ContratoObrigacao_Descricao, @ContratoObrigacao_SubItem, @ContratoObrigacao_DataMaxima); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001G11)
             ,new CursorDef("T001G12", "UPDATE [ContratoObrigacao] SET [Contrato_Codigo]=@Contrato_Codigo, [ContratoObrigacao_Item]=@ContratoObrigacao_Item, [ContratoObrigacao_Descricao]=@ContratoObrigacao_Descricao, [ContratoObrigacao_SubItem]=@ContratoObrigacao_SubItem, [ContratoObrigacao_DataMaxima]=@ContratoObrigacao_DataMaxima  WHERE [ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo", GxErrorMask.GX_NOMASK,prmT001G12)
             ,new CursorDef("T001G13", "DELETE FROM [ContratoObrigacao]  WHERE [ContratoObrigacao_Codigo] = @ContratoObrigacao_Codigo", GxErrorMask.GX_NOMASK,prmT001G13)
             ,new CursorDef("T001G14", "SELECT [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoObrigacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001G14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((String[]) buf[12])[0] = rslt.getLongVarchar(10) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((int[]) buf[17])[0] = rslt.getInt(13) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[6]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[6]);
                }
                stmt.SetParameter(6, (int)parms[7]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
