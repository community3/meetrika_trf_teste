/*
               File: LoadTargets
        Description: Load Targets
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 23:4:37.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadtargets : GXProcedure
   {
      public loadtargets( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadtargets( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_Codigo ,
                           bool aP1_OS ,
                           bool aP2_TA ,
                           int aP3_ContagemResultado_SS ,
                           out IGxCollection aP4_Gxm2rootcol )
      {
         this.AV9Lote_Codigo = aP0_Lote_Codigo;
         this.AV6OS = aP1_OS;
         this.AV7TA = aP2_TA;
         this.AV10ContagemResultado_SS = aP3_ContagemResultado_SS;
         this.Gxm2rootcol = new GxObjectCollection( context, "Targets.Target", "GxEv3Up14_MeetrikaVs3", "SdtTargets_Target", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP4_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Lote_Codigo ,
                                       bool aP1_OS ,
                                       bool aP2_TA ,
                                       int aP3_ContagemResultado_SS )
      {
         this.AV9Lote_Codigo = aP0_Lote_Codigo;
         this.AV6OS = aP1_OS;
         this.AV7TA = aP2_TA;
         this.AV10ContagemResultado_SS = aP3_ContagemResultado_SS;
         this.Gxm2rootcol = new GxObjectCollection( context, "Targets.Target", "GxEv3Up14_MeetrikaVs3", "SdtTargets_Target", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP4_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Lote_Codigo ,
                                 bool aP1_OS ,
                                 bool aP2_TA ,
                                 int aP3_ContagemResultado_SS ,
                                 out IGxCollection aP4_Gxm2rootcol )
      {
         loadtargets objloadtargets;
         objloadtargets = new loadtargets();
         objloadtargets.AV9Lote_Codigo = aP0_Lote_Codigo;
         objloadtargets.AV6OS = aP1_OS;
         objloadtargets.AV7TA = aP2_TA;
         objloadtargets.AV10ContagemResultado_SS = aP3_ContagemResultado_SS;
         objloadtargets.Gxm2rootcol = new GxObjectCollection( context, "Targets.Target", "GxEv3Up14_MeetrikaVs3", "SdtTargets_Target", "GeneXus.Programs") ;
         objloadtargets.context.SetSubmitInitialConfig(context);
         objloadtargets.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadtargets);
         aP4_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadtargets)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ! AV7TA && ( AV6OS ) )
         {
            Gxm1targets = new SdtTargets_Target(context);
            Gxm2rootcol.Add(Gxm1targets, 0);
            Gxm1targets.gxTpr_Target = "arel_contagemcapa.aspx"+ "?" + GXUtil.UrlEncode("" +AV9Lote_Codigo) + "," + GXUtil.UrlEncode("" +AV10ContagemResultado_SS);
         }
         if ( ! AV7TA && ( AV6OS ) )
         {
            Gxm1targets = new SdtTargets_Target(context);
            Gxm2rootcol.Add(Gxm1targets, 0);
            Gxm1targets.gxTpr_Target = "arel_contagemgerencial2.aspx"+ "?" + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode(StringUtil.RTrim("")) + "," + GXUtil.UrlEncode("" +AV9Lote_Codigo);
         }
         if ( ! AV7TA && ( AV6OS ) )
         {
            Gxm1targets = new SdtTargets_Target(context);
            Gxm2rootcol.Add(Gxm1targets, 0);
            Gxm1targets.gxTpr_Target = "arel_contagemos.aspx"+ "?" + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode(StringUtil.RTrim("")) + "," + GXUtil.UrlEncode("" +AV9Lote_Codigo);
         }
         if ( ( AV7TA ) && ! AV6OS )
         {
            Gxm1targets = new SdtTargets_Target(context);
            Gxm2rootcol.Add(Gxm1targets, 0);
            Gxm1targets.gxTpr_Target = "arel_contagemta.aspx"+ "?" + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode(StringUtil.RTrim("")) + "," + GXUtil.UrlEncode("" +AV9Lote_Codigo) + "," + GXUtil.UrlEncode("" +AV10ContagemResultado_SS);
         }
         if ( ( AV7TA ) && ( AV6OS ) )
         {
            Gxm1targets = new SdtTargets_Target(context);
            Gxm2rootcol.Add(Gxm1targets, 0);
            Gxm1targets.gxTpr_Target = "arel_contagem.aspx"+ "?" + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode(StringUtil.RTrim("")) + "," + GXUtil.UrlEncode("" +AV9Lote_Codigo);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1targets = new SdtTargets_Target(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Lote_Codigo ;
      private int AV10ContagemResultado_SS ;
      private bool AV6OS ;
      private bool AV7TA ;
      private IGxCollection aP4_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtTargets_Target ))]
      private IGxCollection Gxm2rootcol ;
      private SdtTargets_Target Gxm1targets ;
   }

}
