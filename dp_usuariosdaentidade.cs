/*
               File: DP_UsuariosDaEntidade
        Description: Usuarios Da Entidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:47:47.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_usuariosdaentidade : GXProcedure
   {
      public dp_usuariosdaentidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_usuariosdaentidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Contratante_Codigo ,
                           out IGxCollection aP2_Gxm2rootcol )
      {
         this.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV6Contratante_Codigo = aP1_Contratante_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Contratada_Codigo ,
                                       int aP1_Contratante_Codigo )
      {
         this.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV6Contratante_Codigo = aP1_Contratante_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Contratante_Codigo ,
                                 out IGxCollection aP2_Gxm2rootcol )
      {
         dp_usuariosdaentidade objdp_usuariosdaentidade;
         objdp_usuariosdaentidade = new dp_usuariosdaentidade();
         objdp_usuariosdaentidade.AV5Contratada_Codigo = aP0_Contratada_Codigo;
         objdp_usuariosdaentidade.AV6Contratante_Codigo = aP1_Contratante_Codigo;
         objdp_usuariosdaentidade.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         objdp_usuariosdaentidade.context.SetSubmitInitialConfig(context);
         objdp_usuariosdaentidade.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_usuariosdaentidade);
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_usuariosdaentidade)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000J2 */
         pr_default.execute(0, new Object[] {AV5Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A67ContratadaUsuario_ContratadaPessoaCod = P000J2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P000J2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A66ContratadaUsuario_ContratadaCod = P000J2_A66ContratadaUsuario_ContratadaCod[0];
            A43Contratada_Ativo = P000J2_A43Contratada_Ativo[0];
            n43Contratada_Ativo = P000J2_n43Contratada_Ativo[0];
            A1394ContratadaUsuario_UsuarioAtivo = P000J2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P000J2_n1394ContratadaUsuario_UsuarioAtivo[0];
            A69ContratadaUsuario_UsuarioCod = P000J2_A69ContratadaUsuario_UsuarioCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P000J2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P000J2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P000J2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P000J2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A43Contratada_Ativo = P000J2_A43Contratada_Ativo[0];
            n43Contratada_Ativo = P000J2_n43Contratada_Ativo[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P000J2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P000J2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A1394ContratadaUsuario_UsuarioAtivo = P000J2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P000J2_n1394ContratadaUsuario_UsuarioAtivo[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A69ContratadaUsuario_UsuarioCod;
            Gxm1sdt_codigos.gxTpr_Descricao = A68ContratadaUsuario_ContratadaPessoaNom;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P000J3 */
         pr_default.execute(1, new Object[] {AV6Contratante_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A61ContratanteUsuario_UsuarioPessoaCod = P000J3_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P000J3_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A63ContratanteUsuario_ContratanteCod = P000J3_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = P000J3_A60ContratanteUsuario_UsuarioCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P000J3_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P000J3_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P000J3_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P000J3_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P000J3_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P000J3_n62ContratanteUsuario_UsuarioPessoaNom[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A60ContratanteUsuario_UsuarioCod;
            Gxm1sdt_codigos.gxTpr_Descricao = A62ContratanteUsuario_UsuarioPessoaNom;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000J2_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P000J2_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P000J2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P000J2_A43Contratada_Ativo = new bool[] {false} ;
         P000J2_n43Contratada_Ativo = new bool[] {false} ;
         P000J2_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P000J2_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P000J2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P000J2_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P000J2_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         Gxm1sdt_codigos = new SdtSDT_Codigos(context);
         P000J3_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P000J3_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P000J3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P000J3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P000J3_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P000J3_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_usuariosdaentidade__default(),
            new Object[][] {
                new Object[] {
               P000J2_A67ContratadaUsuario_ContratadaPessoaCod, P000J2_n67ContratadaUsuario_ContratadaPessoaCod, P000J2_A66ContratadaUsuario_ContratadaCod, P000J2_A43Contratada_Ativo, P000J2_n43Contratada_Ativo, P000J2_A1394ContratadaUsuario_UsuarioAtivo, P000J2_n1394ContratadaUsuario_UsuarioAtivo, P000J2_A69ContratadaUsuario_UsuarioCod, P000J2_A68ContratadaUsuario_ContratadaPessoaNom, P000J2_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               P000J3_A61ContratanteUsuario_UsuarioPessoaCod, P000J3_n61ContratanteUsuario_UsuarioPessoaCod, P000J3_A63ContratanteUsuario_ContratanteCod, P000J3_A60ContratanteUsuario_UsuarioCod, P000J3_A62ContratanteUsuario_UsuarioPessoaNom, P000J3_n62ContratanteUsuario_UsuarioPessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5Contratada_Codigo ;
      private int AV6Contratante_Codigo ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private String scmdbuf ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool A43Contratada_Ativo ;
      private bool n43Contratada_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000J2_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P000J2_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P000J2_A66ContratadaUsuario_ContratadaCod ;
      private bool[] P000J2_A43Contratada_Ativo ;
      private bool[] P000J2_n43Contratada_Ativo ;
      private bool[] P000J2_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P000J2_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] P000J2_A69ContratadaUsuario_UsuarioCod ;
      private String[] P000J2_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P000J2_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] P000J3_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P000J3_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P000J3_A63ContratanteUsuario_ContratanteCod ;
      private int[] P000J3_A60ContratanteUsuario_UsuarioCod ;
      private String[] P000J3_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P000J3_n62ContratanteUsuario_UsuarioPessoaNom ;
      private IGxCollection aP2_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Codigos Gxm1sdt_codigos ;
   }

   public class dp_usuariosdaentidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000J2 ;
          prmP000J2 = new Object[] {
          new Object[] {"@AV5Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP000J3 ;
          prmP000J3 = new Object[] {
          new Object[] {"@AV6Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000J2", "SELECT T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Ativo], T4.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T4.[Usuario_Ativo] = 1) AND (T2.[Contratada_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV5Contratada_Codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000J2,100,0,false,false )
             ,new CursorDef("P000J3", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV6Contratante_Codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000J3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
