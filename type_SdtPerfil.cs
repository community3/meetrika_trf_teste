/*
               File: type_SdtPerfil
        Description: Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/3/2020 0:45:33.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Perfil" )]
   [XmlType(TypeName =  "Perfil" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtPerfil : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPerfil( )
      {
         /* Constructor for serialization */
         gxTv_SdtPerfil_Perfil_nome = "";
         gxTv_SdtPerfil_Perfil_areatrabalhodes = "";
         gxTv_SdtPerfil_Perfil_gamguid = "";
         gxTv_SdtPerfil_Mode = "";
         gxTv_SdtPerfil_Perfil_nome_Z = "";
         gxTv_SdtPerfil_Perfil_areatrabalhodes_Z = "";
         gxTv_SdtPerfil_Perfil_gamguid_Z = "";
      }

      public SdtPerfil( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV3Perfil_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV3Perfil_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Perfil_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Perfil");
         metadata.Set("BT", "Perfil");
         metadata.Set("PK", "[ \"Perfil_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Perfil_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"Perfil_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_gamguid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_gamid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_areatrabalhodes_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtPerfil deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtPerfil)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtPerfil obj ;
         obj = this;
         obj.gxTpr_Perfil_codigo = deserialized.gxTpr_Perfil_codigo;
         obj.gxTpr_Perfil_nome = deserialized.gxTpr_Perfil_nome;
         obj.gxTpr_Perfil_areatrabalhocod = deserialized.gxTpr_Perfil_areatrabalhocod;
         obj.gxTpr_Perfil_areatrabalhodes = deserialized.gxTpr_Perfil_areatrabalhodes;
         obj.gxTpr_Perfil_tipo = deserialized.gxTpr_Perfil_tipo;
         obj.gxTpr_Perfil_gamguid = deserialized.gxTpr_Perfil_gamguid;
         obj.gxTpr_Perfil_gamid = deserialized.gxTpr_Perfil_gamid;
         obj.gxTpr_Perfil_ativo = deserialized.gxTpr_Perfil_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Perfil_codigo_Z = deserialized.gxTpr_Perfil_codigo_Z;
         obj.gxTpr_Perfil_nome_Z = deserialized.gxTpr_Perfil_nome_Z;
         obj.gxTpr_Perfil_areatrabalhocod_Z = deserialized.gxTpr_Perfil_areatrabalhocod_Z;
         obj.gxTpr_Perfil_areatrabalhodes_Z = deserialized.gxTpr_Perfil_areatrabalhodes_Z;
         obj.gxTpr_Perfil_tipo_Z = deserialized.gxTpr_Perfil_tipo_Z;
         obj.gxTpr_Perfil_gamguid_Z = deserialized.gxTpr_Perfil_gamguid_Z;
         obj.gxTpr_Perfil_gamid_Z = deserialized.gxTpr_Perfil_gamid_Z;
         obj.gxTpr_Perfil_ativo_Z = deserialized.gxTpr_Perfil_ativo_Z;
         obj.gxTpr_Perfil_areatrabalhodes_N = deserialized.gxTpr_Perfil_areatrabalhodes_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo") )
               {
                  gxTv_SdtPerfil_Perfil_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome") )
               {
                  gxTv_SdtPerfil_Perfil_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoCod") )
               {
                  gxTv_SdtPerfil_Perfil_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoDes") )
               {
                  gxTv_SdtPerfil_Perfil_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Tipo") )
               {
                  gxTv_SdtPerfil_Perfil_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamGUID") )
               {
                  gxTv_SdtPerfil_Perfil_gamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamId") )
               {
                  gxTv_SdtPerfil_Perfil_gamid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo") )
               {
                  gxTv_SdtPerfil_Perfil_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtPerfil_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtPerfil_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo_Z") )
               {
                  gxTv_SdtPerfil_Perfil_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome_Z") )
               {
                  gxTv_SdtPerfil_Perfil_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtPerfil_Perfil_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtPerfil_Perfil_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Tipo_Z") )
               {
                  gxTv_SdtPerfil_Perfil_tipo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamGUID_Z") )
               {
                  gxTv_SdtPerfil_Perfil_gamguid_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamId_Z") )
               {
                  gxTv_SdtPerfil_Perfil_gamid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo_Z") )
               {
                  gxTv_SdtPerfil_Perfil_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtPerfil_Perfil_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Perfil";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Perfil_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Nome", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_tipo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_GamGUID", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_gamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_GamId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_gamid), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtPerfil_Perfil_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtPerfil_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Nome_Z", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Tipo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_tipo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_GamGUID_Z", StringUtil.RTrim( gxTv_SdtPerfil_Perfil_gamguid_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_GamId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_gamid_Z), 12, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtPerfil_Perfil_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPerfil_Perfil_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Perfil_Codigo", gxTv_SdtPerfil_Perfil_codigo, false);
         AddObjectProperty("Perfil_Nome", gxTv_SdtPerfil_Perfil_nome, false);
         AddObjectProperty("Perfil_AreaTrabalhoCod", gxTv_SdtPerfil_Perfil_areatrabalhocod, false);
         AddObjectProperty("Perfil_AreaTrabalhoDes", gxTv_SdtPerfil_Perfil_areatrabalhodes, false);
         AddObjectProperty("Perfil_Tipo", gxTv_SdtPerfil_Perfil_tipo, false);
         AddObjectProperty("Perfil_GamGUID", gxTv_SdtPerfil_Perfil_gamguid, false);
         AddObjectProperty("Perfil_GamId", gxTv_SdtPerfil_Perfil_gamid, false);
         AddObjectProperty("Perfil_Ativo", gxTv_SdtPerfil_Perfil_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtPerfil_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtPerfil_Initialized, false);
            AddObjectProperty("Perfil_Codigo_Z", gxTv_SdtPerfil_Perfil_codigo_Z, false);
            AddObjectProperty("Perfil_Nome_Z", gxTv_SdtPerfil_Perfil_nome_Z, false);
            AddObjectProperty("Perfil_AreaTrabalhoCod_Z", gxTv_SdtPerfil_Perfil_areatrabalhocod_Z, false);
            AddObjectProperty("Perfil_AreaTrabalhoDes_Z", gxTv_SdtPerfil_Perfil_areatrabalhodes_Z, false);
            AddObjectProperty("Perfil_Tipo_Z", gxTv_SdtPerfil_Perfil_tipo_Z, false);
            AddObjectProperty("Perfil_GamGUID_Z", gxTv_SdtPerfil_Perfil_gamguid_Z, false);
            AddObjectProperty("Perfil_GamId_Z", gxTv_SdtPerfil_Perfil_gamid_Z, false);
            AddObjectProperty("Perfil_Ativo_Z", gxTv_SdtPerfil_Perfil_ativo_Z, false);
            AddObjectProperty("Perfil_AreaTrabalhoDes_N", gxTv_SdtPerfil_Perfil_areatrabalhodes_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Perfil_Codigo" )]
      [  XmlElement( ElementName = "Perfil_Codigo"   )]
      public int gxTpr_Perfil_codigo
      {
         get {
            return gxTv_SdtPerfil_Perfil_codigo ;
         }

         set {
            if ( gxTv_SdtPerfil_Perfil_codigo != value )
            {
               gxTv_SdtPerfil_Mode = "INS";
               this.gxTv_SdtPerfil_Perfil_codigo_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_nome_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_tipo_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_gamguid_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_gamid_Z_SetNull( );
               this.gxTv_SdtPerfil_Perfil_ativo_Z_SetNull( );
            }
            gxTv_SdtPerfil_Perfil_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Nome" )]
      [  XmlElement( ElementName = "Perfil_Nome"   )]
      public String gxTpr_Perfil_nome
      {
         get {
            return gxTv_SdtPerfil_Perfil_nome ;
         }

         set {
            gxTv_SdtPerfil_Perfil_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoCod"   )]
      public int gxTpr_Perfil_areatrabalhocod
      {
         get {
            return gxTv_SdtPerfil_Perfil_areatrabalhocod ;
         }

         set {
            gxTv_SdtPerfil_Perfil_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoDes"   )]
      public String gxTpr_Perfil_areatrabalhodes
      {
         get {
            return gxTv_SdtPerfil_Perfil_areatrabalhodes ;
         }

         set {
            gxTv_SdtPerfil_Perfil_areatrabalhodes_N = 0;
            gxTv_SdtPerfil_Perfil_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_areatrabalhodes_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_areatrabalhodes_N = 1;
         gxTv_SdtPerfil_Perfil_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Tipo" )]
      [  XmlElement( ElementName = "Perfil_Tipo"   )]
      public short gxTpr_Perfil_tipo
      {
         get {
            return gxTv_SdtPerfil_Perfil_tipo ;
         }

         set {
            gxTv_SdtPerfil_Perfil_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_GamGUID" )]
      [  XmlElement( ElementName = "Perfil_GamGUID"   )]
      public String gxTpr_Perfil_gamguid
      {
         get {
            return gxTv_SdtPerfil_Perfil_gamguid ;
         }

         set {
            gxTv_SdtPerfil_Perfil_gamguid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_GamId" )]
      [  XmlElement( ElementName = "Perfil_GamId"   )]
      public long gxTpr_Perfil_gamid
      {
         get {
            return gxTv_SdtPerfil_Perfil_gamid ;
         }

         set {
            gxTv_SdtPerfil_Perfil_gamid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Ativo" )]
      [  XmlElement( ElementName = "Perfil_Ativo"   )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return gxTv_SdtPerfil_Perfil_ativo ;
         }

         set {
            gxTv_SdtPerfil_Perfil_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtPerfil_Mode ;
         }

         set {
            gxTv_SdtPerfil_Mode = (String)(value);
         }

      }

      public void gxTv_SdtPerfil_Mode_SetNull( )
      {
         gxTv_SdtPerfil_Mode = "";
         return  ;
      }

      public bool gxTv_SdtPerfil_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtPerfil_Initialized ;
         }

         set {
            gxTv_SdtPerfil_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtPerfil_Initialized_SetNull( )
      {
         gxTv_SdtPerfil_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Codigo_Z" )]
      [  XmlElement( ElementName = "Perfil_Codigo_Z"   )]
      public int gxTpr_Perfil_codigo_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_codigo_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_codigo_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Nome_Z" )]
      [  XmlElement( ElementName = "Perfil_Nome_Z"   )]
      public String gxTpr_Perfil_nome_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_nome_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_nome_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Perfil_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Perfil_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Tipo_Z" )]
      [  XmlElement( ElementName = "Perfil_Tipo_Z"   )]
      public short gxTpr_Perfil_tipo_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_tipo_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_tipo_Z = (short)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_tipo_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_tipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_GamGUID_Z" )]
      [  XmlElement( ElementName = "Perfil_GamGUID_Z"   )]
      public String gxTpr_Perfil_gamguid_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_gamguid_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_gamguid_Z = (String)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_gamguid_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_gamguid_Z = "";
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_gamguid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_GamId_Z" )]
      [  XmlElement( ElementName = "Perfil_GamId_Z"   )]
      public long gxTpr_Perfil_gamid_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_gamid_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_gamid_Z = (long)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_gamid_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_gamid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_gamid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Ativo_Z" )]
      [  XmlElement( ElementName = "Perfil_Ativo_Z"   )]
      public bool gxTpr_Perfil_ativo_Z
      {
         get {
            return gxTv_SdtPerfil_Perfil_ativo_Z ;
         }

         set {
            gxTv_SdtPerfil_Perfil_ativo_Z = value;
         }

      }

      public void gxTv_SdtPerfil_Perfil_ativo_Z_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoDes_N"   )]
      public short gxTpr_Perfil_areatrabalhodes_N
      {
         get {
            return gxTv_SdtPerfil_Perfil_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtPerfil_Perfil_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtPerfil_Perfil_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtPerfil_Perfil_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtPerfil_Perfil_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtPerfil_Perfil_nome = "";
         gxTv_SdtPerfil_Perfil_areatrabalhodes = "";
         gxTv_SdtPerfil_Perfil_gamguid = "";
         gxTv_SdtPerfil_Mode = "";
         gxTv_SdtPerfil_Perfil_nome_Z = "";
         gxTv_SdtPerfil_Perfil_areatrabalhodes_Z = "";
         gxTv_SdtPerfil_Perfil_gamguid_Z = "";
         gxTv_SdtPerfil_Perfil_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "perfil", "GeneXus.Programs.perfil_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtPerfil_Perfil_tipo ;
      private short gxTv_SdtPerfil_Initialized ;
      private short gxTv_SdtPerfil_Perfil_tipo_Z ;
      private short gxTv_SdtPerfil_Perfil_areatrabalhodes_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtPerfil_Perfil_codigo ;
      private int gxTv_SdtPerfil_Perfil_areatrabalhocod ;
      private int gxTv_SdtPerfil_Perfil_codigo_Z ;
      private int gxTv_SdtPerfil_Perfil_areatrabalhocod_Z ;
      private long gxTv_SdtPerfil_Perfil_gamid ;
      private long gxTv_SdtPerfil_Perfil_gamid_Z ;
      private String gxTv_SdtPerfil_Perfil_nome ;
      private String gxTv_SdtPerfil_Perfil_gamguid ;
      private String gxTv_SdtPerfil_Mode ;
      private String gxTv_SdtPerfil_Perfil_nome_Z ;
      private String gxTv_SdtPerfil_Perfil_gamguid_Z ;
      private String sTagName ;
      private bool gxTv_SdtPerfil_Perfil_ativo ;
      private bool gxTv_SdtPerfil_Perfil_ativo_Z ;
      private String gxTv_SdtPerfil_Perfil_areatrabalhodes ;
      private String gxTv_SdtPerfil_Perfil_areatrabalhodes_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Perfil", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtPerfil_RESTInterface : GxGenericCollectionItem<SdtPerfil>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPerfil_RESTInterface( ) : base()
      {
      }

      public SdtPerfil_RESTInterface( SdtPerfil psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Perfil_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Perfil_codigo
      {
         get {
            return sdt.gxTpr_Perfil_codigo ;
         }

         set {
            sdt.gxTpr_Perfil_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Perfil_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_nome) ;
         }

         set {
            sdt.gxTpr_Perfil_nome = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_AreaTrabalhoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Perfil_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Perfil_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Perfil_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_AreaTrabalhoDes" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Perfil_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Perfil_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Perfil_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_Tipo" , Order = 4 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Perfil_tipo
      {
         get {
            return sdt.gxTpr_Perfil_tipo ;
         }

         set {
            sdt.gxTpr_Perfil_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_GamGUID" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Perfil_gamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_gamguid) ;
         }

         set {
            sdt.gxTpr_Perfil_gamguid = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_GamId" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Perfil_gamid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Perfil_gamid), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Perfil_gamid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Perfil_Ativo" , Order = 7 )]
      [GxSeudo()]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return sdt.gxTpr_Perfil_ativo ;
         }

         set {
            sdt.gxTpr_Perfil_ativo = value;
         }

      }

      public SdtPerfil sdt
      {
         get {
            return (SdtPerfil)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtPerfil() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 19 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
