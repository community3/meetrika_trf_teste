/*
               File: ContratoServicosArtefatosWC
        Description: Contrato Servicos Artefatos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:59.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosartefatoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosartefatoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosartefatoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.AV6ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynArtefatos_Codigo = new GXCombobox();
         chkContratoServicosArtefato_Obrigatorio = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV6ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV6ContratoServicos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ARTEFATOS_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAARTEFATOS_CODIGON62( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV22TFArtefatos_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
                  AV23TFArtefatos_Codigo_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFArtefatos_Codigo_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0)));
                  AV26TFContratoServicosArtefato_Obrigatorio_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosArtefato_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0));
                  AV6ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
                  AV24ddo_Artefatos_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_Artefatos_CodigoTitleControlIdToReplace", AV24ddo_Artefatos_CodigoTitleControlIdToReplace);
                  AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace", AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace);
                  AV38Pgmname = GetNextPar( );
                  AV29TFArtefatos_Codigo_SelDsc = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFArtefatos_Codigo_SelDsc", AV29TFArtefatos_Codigo_SelDsc);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosArtefatosWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contratoservicosartefatoswc:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAN62( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV38Pgmname = "ContratoServicosArtefatosWC";
               context.Gx_err = 0;
               GXAARTEFATOS_CODIGO_htmlN62( ) ;
               /* Using cursor H00N62 */
               pr_default.execute(0, new Object[] {A1749Artefatos_Codigo});
               A1751Artefatos_Descricao = H00N62_A1751Artefatos_Descricao[0];
               pr_default.close(0);
               WSN62( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Artefatos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823907");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosartefatoswc.aspx") + "?" + UrlEncode("" +AV6ContratoServicos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFARTEFATOS_CODIGO", AV22TFArtefatos_Codigo);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFARTEFATOS_CODIGO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vARTEFATOS_CODIGOTITLEFILTERDATA", AV21Artefatos_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vARTEFATOS_CODIGOTITLEFILTERDATA", AV21Artefatos_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLEFILTERDATA", AV25ContratoServicosArtefato_ObrigatorioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLEFILTERDATA", AV25ContratoServicosArtefato_ObrigatorioTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV6ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV38Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOARTEFATO_ARTEFATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Width", StringUtil.RTrim( Confirmpanel2_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Icon", StringUtil.RTrim( Confirmpanel2_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Confirmtext", StringUtil.RTrim( Confirmpanel2_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Buttonyestext", StringUtil.RTrim( Confirmpanel2_Buttonyestext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Buttonnotext", StringUtil.RTrim( Confirmpanel2_Buttonnotext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Confirmtype", StringUtil.RTrim( Confirmpanel2_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Caption", StringUtil.RTrim( Ddo_artefatos_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Tooltip", StringUtil.RTrim( Ddo_artefatos_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Cls", StringUtil.RTrim( Ddo_artefatos_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_artefatos_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedvalue_set", StringUtil.RTrim( Ddo_artefatos_codigo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedtext_set", StringUtil.RTrim( Ddo_artefatos_codigo_Selectedtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_artefatos_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_artefatos_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_artefatos_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_artefatos_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_artefatos_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_artefatos_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Filtertype", StringUtil.RTrim( Ddo_artefatos_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_artefatos_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_artefatos_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Datalisttype", StringUtil.RTrim( Ddo_artefatos_codigo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_artefatos_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistproc", StringUtil.RTrim( Ddo_artefatos_codigo_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_artefatos_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Sortasc", StringUtil.RTrim( Ddo_artefatos_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_artefatos_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_artefatos_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_artefatos_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_artefatos_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_artefatos_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_artefatos_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_artefatos_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Caption", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Cls", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosartefato_obrigatorio_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosartefato_obrigatorio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosartefato_obrigatorio_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosartefato_obrigatorio_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosartefato_obrigatorio_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosartefato_obrigatorio_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_artefatos_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_artefatos_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedvalue_get", StringUtil.RTrim( Ddo_artefatos_codigo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedtext_get", StringUtil.RTrim( Ddo_artefatos_codigo_Selectedtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosArtefatosWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosartefatoswc:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormN62( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosartefatoswc.js", "?2020428239099");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosArtefatosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Artefatos WC" ;
      }

      protected void WBN60( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosartefatoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_N62( true) ;
         }
         else
         {
            wb_table1_2_N62( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicos_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(AV6ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratoservicos_codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Servico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Servico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavServico_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFaltam_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Faltam), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Faltam), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFaltam_Jsonclick, 0, "Attribute", "", "", "", edtavFaltam_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfartefatos_codigo_Internalname, AV22TFArtefatos_Codigo, StringUtil.RTrim( context.localUtil.Format( AV22TFArtefatos_Codigo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfartefatos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfartefatos_codigo_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfartefatos_codigo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23TFArtefatos_Codigo_Sel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfartefatos_codigo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfartefatos_codigo_sel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfartefatos_codigo_seldsc_Internalname, AV29TFArtefatos_Codigo_SelDsc, StringUtil.RTrim( context.localUtil.Format( AV29TFArtefatos_Codigo_SelDsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfartefatos_codigo_seldsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfartefatos_codigo_seldsc_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosartefato_obrigatorio_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosartefato_obrigatorio_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosArtefatosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ARTEFATOS_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_artefatos_codigotitlecontrolidtoreplace_Internalname, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_artefatos_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosArtefatosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Internalname, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosArtefatosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTN62( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Artefatos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPN60( ) ;
            }
         }
      }

      protected void WSN62( )
      {
         STARTN62( ) ;
         EVTN62( ) ;
      }

      protected void EVTN62( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11N62 */
                                    E11N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12N62 */
                                    E12N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ARTEFATOS_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13N62 */
                                    E13N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14N62 */
                                    E14N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15N62 */
                                    E15N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLONAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16N62 */
                                    E16N62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavServico_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN60( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV19Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV36Update_GXI : context.convertURL( context.PathToRelativeUrl( AV19Update))));
                              AV7Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV37Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV7Delete))));
                              dynArtefatos_Codigo.Name = dynArtefatos_Codigo_Internalname;
                              dynArtefatos_Codigo.CurrentValue = cgiGet( dynArtefatos_Codigo_Internalname);
                              A1749Artefatos_Codigo = (int)(NumberUtil.Val( cgiGet( dynArtefatos_Codigo_Internalname), "."));
                              A1768ContratoServicosArtefato_Obrigatorio = StringUtil.StrToBool( cgiGet( chkContratoServicosArtefato_Obrigatorio_Internalname));
                              n1768ContratoServicosArtefato_Obrigatorio = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17N62 */
                                          E17N62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18N62 */
                                          E18N62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19N62 */
                                          E19N62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20N62 */
                                          E20N62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21N62 */
                                          E21N62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfartefatos_codigo Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFARTEFATOS_CODIGO"), AV22TFArtefatos_Codigo) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfartefatos_codigo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFARTEFATOS_CODIGO_SEL"), ",", ".") != Convert.ToDecimal( AV23TFArtefatos_Codigo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosartefato_obrigatorio_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL"), ",", ".") != Convert.ToDecimal( AV26TFContratoServicosArtefato_Obrigatorio_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPN60( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEN62( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormN62( ) ;
            }
         }
      }

      protected void PAN62( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "ARTEFATOS_CODIGO_" + sGXsfl_11_idx;
            dynArtefatos_Codigo.Name = GXCCtl;
            dynArtefatos_Codigo.WebTags = "";
            GXCCtl = "CONTRATOSERVICOSARTEFATO_OBRIGATORIO_" + sGXsfl_11_idx;
            chkContratoServicosArtefato_Obrigatorio.Name = GXCCtl;
            chkContratoServicosArtefato_Obrigatorio.WebTags = "";
            chkContratoServicosArtefato_Obrigatorio.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosArtefato_Obrigatorio_Internalname, "TitleCaption", chkContratoServicosArtefato_Obrigatorio.Caption);
            chkContratoServicosArtefato_Obrigatorio.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavServico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAARTEFATOS_CODIGON62( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAARTEFATOS_CODIGO_dataN62( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAARTEFATOS_CODIGO_htmlN62( )
      {
         int gxdynajaxvalue ;
         GXDLAARTEFATOS_CODIGO_dataN62( ) ;
         gxdynajaxindex = 1;
         dynArtefatos_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynArtefatos_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAARTEFATOS_CODIGO_dataN62( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00N63 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00N63_A1749Artefatos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00N63_A1751Artefatos_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV22TFArtefatos_Codigo ,
                                       int AV23TFArtefatos_Codigo_Sel ,
                                       short AV26TFContratoServicosArtefato_Obrigatorio_Sel ,
                                       int AV6ContratoServicos_Codigo ,
                                       String AV24ddo_Artefatos_CodigoTitleControlIdToReplace ,
                                       String AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace ,
                                       String AV38Pgmname ,
                                       String AV29TFArtefatos_Codigo_SelDsc ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFN62( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ARTEFATOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSARTEFATO_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1768ContratoServicosArtefato_Obrigatorio));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSARTEFATO_OBRIGATORIO", StringUtil.BoolToStr( A1768ContratoServicosArtefato_Obrigatorio));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN62( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV38Pgmname = "ContratoServicosArtefatosWC";
         context.Gx_err = 0;
      }

      protected void RFN62( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E18N62 */
         E18N62 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV23TFArtefatos_Codigo_Sel ,
                                                 AV22TFArtefatos_Codigo ,
                                                 AV26TFContratoServicosArtefato_Obrigatorio_Sel ,
                                                 A1751Artefatos_Descricao ,
                                                 A1749Artefatos_Codigo ,
                                                 A1768ContratoServicosArtefato_Obrigatorio ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A160ContratoServicos_Codigo ,
                                                 AV6ContratoServicos_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV22TFArtefatos_Codigo = StringUtil.Concat( StringUtil.RTrim( AV22TFArtefatos_Codigo), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
            /* Using cursor H00N64 */
            pr_default.execute(2, new Object[] {AV6ContratoServicos_Codigo, lV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_11_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1751Artefatos_Descricao = H00N64_A1751Artefatos_Descricao[0];
               A160ContratoServicos_Codigo = H00N64_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1768ContratoServicosArtefato_Obrigatorio = H00N64_A1768ContratoServicosArtefato_Obrigatorio[0];
               n1768ContratoServicosArtefato_Obrigatorio = H00N64_n1768ContratoServicosArtefato_Obrigatorio[0];
               A1749Artefatos_Codigo = H00N64_A1749Artefatos_Codigo[0];
               A1751Artefatos_Descricao = H00N64_A1751Artefatos_Descricao[0];
               /* Execute user event: E19N62 */
               E19N62 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 11;
            WBN60( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV23TFArtefatos_Codigo_Sel ,
                                              AV22TFArtefatos_Codigo ,
                                              AV26TFContratoServicosArtefato_Obrigatorio_Sel ,
                                              A1751Artefatos_Descricao ,
                                              A1749Artefatos_Codigo ,
                                              A1768ContratoServicosArtefato_Obrigatorio ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A160ContratoServicos_Codigo ,
                                              AV6ContratoServicos_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV22TFArtefatos_Codigo = StringUtil.Concat( StringUtil.RTrim( AV22TFArtefatos_Codigo), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
         /* Using cursor H00N65 */
         pr_default.execute(3, new Object[] {AV6ContratoServicos_Codigo, lV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel});
         GRID_nRecordCount = H00N65_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFArtefatos_Codigo, AV23TFArtefatos_Codigo_Sel, AV26TFContratoServicosArtefato_Obrigatorio_Sel, AV6ContratoServicos_Codigo, AV24ddo_Artefatos_CodigoTitleControlIdToReplace, AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, AV38Pgmname, AV29TFArtefatos_Codigo_SelDsc, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPN60( )
      {
         /* Before Start, stand alone formulas. */
         AV38Pgmname = "ContratoServicosArtefatosWC";
         context.Gx_err = 0;
         GXAARTEFATOS_CODIGO_htmlN62( ) ;
         /* Using cursor H00N66 */
         pr_default.execute(4, new Object[] {A1749Artefatos_Codigo});
         A1751Artefatos_Descricao = H00N66_A1751Artefatos_Descricao[0];
         pr_default.close(4);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17N62 */
         E17N62 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV28DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vARTEFATOS_CODIGOTITLEFILTERDATA"), AV21Artefatos_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLEFILTERDATA"), AV25ContratoServicosArtefato_ObrigatorioTitleFilterData);
            /* Read variables values. */
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_CODIGO");
               GX_FocusControl = edtavServico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15Servico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Servico_Codigo), 6, 0)));
            }
            else
            {
               AV15Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Servico_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFaltam_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFaltam_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFALTAM");
               GX_FocusControl = edtavFaltam_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Faltam = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Faltam", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Faltam), 3, 0)));
            }
            else
            {
               AV8Faltam = (short)(context.localUtil.CToN( cgiGet( edtavFaltam_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Faltam", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Faltam), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV22TFArtefatos_Codigo = StringUtil.Upper( cgiGet( edtavTfartefatos_codigo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfartefatos_codigo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfartefatos_codigo_sel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFARTEFATOS_CODIGO_SEL");
               GX_FocusControl = edtavTfartefatos_codigo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFArtefatos_Codigo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFArtefatos_Codigo_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0)));
            }
            else
            {
               AV23TFArtefatos_Codigo_Sel = (int)(context.localUtil.CToN( cgiGet( edtavTfartefatos_codigo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFArtefatos_Codigo_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0)));
            }
            AV29TFArtefatos_Codigo_SelDsc = cgiGet( edtavTfartefatos_codigo_seldsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFArtefatos_Codigo_SelDsc", AV29TFArtefatos_Codigo_SelDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL");
               GX_FocusControl = edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFContratoServicosArtefato_Obrigatorio_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosArtefato_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0));
            }
            else
            {
               AV26TFContratoServicosArtefato_Obrigatorio_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosArtefato_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0));
            }
            AV24ddo_Artefatos_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_artefatos_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_Artefatos_CodigoTitleControlIdToReplace", AV24ddo_Artefatos_CodigoTitleControlIdToReplace);
            AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace", AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            AV31GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV32GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV6ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContratoServicos_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Confirmpanel_Width = cgiGet( sPrefix+"CONFIRMPANEL_Width");
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( sPrefix+"CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( sPrefix+"CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( sPrefix+"CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtype");
            Confirmpanel2_Width = cgiGet( sPrefix+"CONFIRMPANEL2_Width");
            Confirmpanel2_Title = cgiGet( sPrefix+"CONFIRMPANEL2_Title");
            Confirmpanel2_Icon = cgiGet( sPrefix+"CONFIRMPANEL2_Icon");
            Confirmpanel2_Confirmtext = cgiGet( sPrefix+"CONFIRMPANEL2_Confirmtext");
            Confirmpanel2_Buttonyestext = cgiGet( sPrefix+"CONFIRMPANEL2_Buttonyestext");
            Confirmpanel2_Buttonnotext = cgiGet( sPrefix+"CONFIRMPANEL2_Buttonnotext");
            Confirmpanel2_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL2_Confirmtype");
            Ddo_artefatos_codigo_Caption = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Caption");
            Ddo_artefatos_codigo_Tooltip = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Tooltip");
            Ddo_artefatos_codigo_Cls = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Cls");
            Ddo_artefatos_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Filteredtext_set");
            Ddo_artefatos_codigo_Selectedvalue_set = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedvalue_set");
            Ddo_artefatos_codigo_Selectedtext_set = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedtext_set");
            Ddo_artefatos_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Dropdownoptionstype");
            Ddo_artefatos_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Titlecontrolidtoreplace");
            Ddo_artefatos_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Includesortasc"));
            Ddo_artefatos_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Includesortdsc"));
            Ddo_artefatos_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Sortedstatus");
            Ddo_artefatos_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Includefilter"));
            Ddo_artefatos_codigo_Filtertype = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Filtertype");
            Ddo_artefatos_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Filterisrange"));
            Ddo_artefatos_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Includedatalist"));
            Ddo_artefatos_codigo_Datalisttype = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Datalisttype");
            Ddo_artefatos_codigo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistfixedvalues");
            Ddo_artefatos_codigo_Datalistproc = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistproc");
            Ddo_artefatos_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_artefatos_codigo_Sortasc = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Sortasc");
            Ddo_artefatos_codigo_Sortdsc = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Sortdsc");
            Ddo_artefatos_codigo_Loadingdata = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Loadingdata");
            Ddo_artefatos_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Cleanfilter");
            Ddo_artefatos_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Rangefilterfrom");
            Ddo_artefatos_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Rangefilterto");
            Ddo_artefatos_codigo_Noresultsfound = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Noresultsfound");
            Ddo_artefatos_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Searchbuttontext");
            Ddo_contratoservicosartefato_obrigatorio_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Caption");
            Ddo_contratoservicosartefato_obrigatorio_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Tooltip");
            Ddo_contratoservicosartefato_obrigatorio_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Cls");
            Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Selectedvalue_set");
            Ddo_contratoservicosartefato_obrigatorio_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Dropdownoptionstype");
            Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Titlecontrolidtoreplace");
            Ddo_contratoservicosartefato_obrigatorio_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includesortasc"));
            Ddo_contratoservicosartefato_obrigatorio_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includesortdsc"));
            Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortedstatus");
            Ddo_contratoservicosartefato_obrigatorio_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includefilter"));
            Ddo_contratoservicosartefato_obrigatorio_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Filterisrange"));
            Ddo_contratoservicosartefato_obrigatorio_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Includedatalist"));
            Ddo_contratoservicosartefato_obrigatorio_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalisttype");
            Ddo_contratoservicosartefato_obrigatorio_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalistfixedvalues");
            Ddo_contratoservicosartefato_obrigatorio_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosartefato_obrigatorio_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortasc");
            Ddo_contratoservicosartefato_obrigatorio_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Sortdsc");
            Ddo_contratoservicosartefato_obrigatorio_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Loadingdata");
            Ddo_contratoservicosartefato_obrigatorio_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Cleanfilter");
            Ddo_contratoservicosartefato_obrigatorio_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Rangefilterfrom");
            Ddo_contratoservicosartefato_obrigatorio_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Rangefilterto");
            Ddo_contratoservicosartefato_obrigatorio_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Noresultsfound");
            Ddo_contratoservicosartefato_obrigatorio_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_artefatos_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Activeeventkey");
            Ddo_artefatos_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Filteredtext_get");
            Ddo_artefatos_codigo_Selectedvalue_get = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedvalue_get");
            Ddo_artefatos_codigo_Selectedtext_get = cgiGet( sPrefix+"DDO_ARTEFATOS_CODIGO_Selectedtext_get");
            Ddo_contratoservicosartefato_obrigatorio_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Activeeventkey");
            Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO_Selectedvalue_get");
            Confirmpanel_Result = cgiGet( sPrefix+"CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosArtefatosWC";
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoservicosartefatoswc:[SecurityCheckFailed value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFARTEFATOS_CODIGO"), AV22TFArtefatos_Codigo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFARTEFATOS_CODIGO_SEL"), ",", ".") != Convert.ToDecimal( AV23TFArtefatos_Codigo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL"), ",", ".") != Convert.ToDecimal( AV26TFContratoServicosArtefato_Obrigatorio_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17N62 */
         E17N62 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17N62( )
      {
         /* Start Routine */
         edtavContratoservicos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo_Visible), 5, 0)));
         edtavServico_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_codigo_Visible), 5, 0)));
         edtavFaltam_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFaltam_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFaltam_Visible), 5, 0)));
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfartefatos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfartefatos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfartefatos_codigo_Visible), 5, 0)));
         edtavTfartefatos_codigo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfartefatos_codigo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfartefatos_codigo_sel_Visible), 5, 0)));
         edtavTfartefatos_codigo_seldsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfartefatos_codigo_seldsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfartefatos_codigo_seldsc_Visible), 5, 0)));
         edtavTfcontratoservicosartefato_obrigatorio_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosartefato_obrigatorio_sel_Visible), 5, 0)));
         Ddo_artefatos_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Artefatos_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "TitleControlIdToReplace", Ddo_artefatos_codigo_Titlecontrolidtoreplace);
         AV24ddo_Artefatos_CodigoTitleControlIdToReplace = Ddo_artefatos_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_Artefatos_CodigoTitleControlIdToReplace", AV24ddo_Artefatos_CodigoTitleControlIdToReplace);
         edtavDdo_artefatos_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_artefatos_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_artefatos_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosArtefato_Obrigatorio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace);
         AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace = Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace", AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace);
         edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV28DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV28DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         /* Using cursor H00N67 */
         pr_default.execute(5, new Object[] {AV6ContratoServicos_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A160ContratoServicos_Codigo = H00N67_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A155Servico_Codigo = H00N67_A155Servico_Codigo[0];
            AV15Servico_Codigo = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Servico_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
      }

      protected void E18N62( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV21Artefatos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25ContratoServicosArtefato_ObrigatorioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         dynArtefatos_Codigo_Titleformat = 2;
         dynArtefatos_Codigo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV24ddo_Artefatos_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynArtefatos_Codigo_Internalname, "Title", dynArtefatos_Codigo.Title.Text);
         chkContratoServicosArtefato_Obrigatorio_Titleformat = 2;
         chkContratoServicosArtefato_Obrigatorio.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Obrigat�rio", AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosArtefato_Obrigatorio_Internalname, "Title", chkContratoServicosArtefato_Obrigatorio.Title.Text);
         AV31GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GridCurrentPage), 10, 0)));
         AV32GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV20WWPContext.gxTpr_Update&&AV20WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV20WWPContext.gxTpr_Delete&&AV20WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         imgInsert_Visible = (AV20WWPContext.gxTpr_Insert&&AV20WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         imgClonar_Visible = (AV20WWPContext.gxTpr_Insert&&AV20WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgClonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgClonar_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV21Artefatos_CodigoTitleFilterData", AV21Artefatos_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25ContratoServicosArtefato_ObrigatorioTitleFilterData", AV25ContratoServicosArtefato_ObrigatorioTitleFilterData);
      }

      protected void E11N62( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV30PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV30PageToGo) ;
         }
      }

      protected void E13N62( )
      {
         /* Ddo_artefatos_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_artefatos_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_artefatos_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SortedStatus", Ddo_artefatos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_artefatos_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_artefatos_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SortedStatus", Ddo_artefatos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_artefatos_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFArtefatos_Codigo = Ddo_artefatos_codigo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
            AV23TFArtefatos_Codigo_Sel = (int)(NumberUtil.Val( Ddo_artefatos_codigo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFArtefatos_Codigo_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0)));
            AV29TFArtefatos_Codigo_SelDsc = Ddo_artefatos_codigo_Selectedtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFArtefatos_Codigo_SelDsc", AV29TFArtefatos_Codigo_SelDsc);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14N62( )
      {
         /* Ddo_contratoservicosartefato_obrigatorio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosartefato_obrigatorio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "SortedStatus", Ddo_contratoservicosartefato_obrigatorio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosartefato_obrigatorio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "SortedStatus", Ddo_contratoservicosartefato_obrigatorio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosartefato_obrigatorio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV26TFContratoServicosArtefato_Obrigatorio_Sel = (short)(NumberUtil.Val( Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosArtefato_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E19N62( )
      {
         /* Grid_Load Routine */
         AV19Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV19Update);
         AV36Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         AV7Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV7Delete);
         AV37Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 11;
         }
         sendrow_112( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(11, GridRow);
         }
      }

      protected void E15N62( )
      {
         /* 'DoInsert' Routine */
         context.PopUp(formatLink("contratoservicosartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +AV6ContratoServicos_Codigo) + "," + UrlEncode("" +0), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E16N62( )
      {
         /* 'DoClonar' Routine */
         /* Execute user subroutine: 'FALTAM' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( (0==AV8Faltam) )
         {
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
         }
         else
         {
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void E20N62( )
      {
         /* 'DoUpdate' Routine */
         context.PopUp(formatLink("contratoservicosartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +AV6ContratoServicos_Codigo) + "," + UrlEncode("" +A1749Artefatos_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E21N62( )
      {
         /* 'DoDelete' Routine */
         context.PopUp(formatLink("contratoservicosartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +AV6ContratoServicos_Codigo) + "," + UrlEncode("" +A1749Artefatos_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_artefatos_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SortedStatus", Ddo_artefatos_codigo_Sortedstatus);
         Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "SortedStatus", Ddo_contratoservicosartefato_obrigatorio_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_artefatos_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SortedStatus", Ddo_artefatos_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "SortedStatus", Ddo_contratoservicosartefato_obrigatorio_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV38Pgmname+"GridState"), "") == 0 )
         {
            AV9GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV38Pgmname+"GridState"), "");
         }
         else
         {
            AV9GridState.FromXml(AV16Session.Get(AV38Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV9GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV9GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV9GridState.gxTpr_Filtervalues.Count )
         {
            AV10GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV9GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV10GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_CODIGO") == 0 )
            {
               AV22TFArtefatos_Codigo = AV10GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFArtefatos_Codigo", AV22TFArtefatos_Codigo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFArtefatos_Codigo)) )
               {
                  Ddo_artefatos_codigo_Filteredtext_set = AV22TFArtefatos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "FilteredText_set", Ddo_artefatos_codigo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV10GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_CODIGO_SEL") == 0 )
            {
               AV23TFArtefatos_Codigo_Sel = (int)(NumberUtil.Val( AV10GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFArtefatos_Codigo_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0)));
               if ( ! (0==AV23TFArtefatos_Codigo_Sel) )
               {
                  Ddo_artefatos_codigo_Selectedvalue_set = StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SelectedValue_set", Ddo_artefatos_codigo_Selectedvalue_set);
                  AV29TFArtefatos_Codigo_SelDsc = AV10GridStateFilterValue.gxTpr_Valueto;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFArtefatos_Codigo_SelDsc", AV29TFArtefatos_Codigo_SelDsc);
                  Ddo_artefatos_codigo_Selectedtext_set = AV29TFArtefatos_Codigo_SelDsc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_artefatos_codigo_Internalname, "SelectedText_set", Ddo_artefatos_codigo_Selectedtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV10GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL") == 0 )
            {
               AV26TFContratoServicosArtefato_Obrigatorio_Sel = (short)(NumberUtil.Val( AV10GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosArtefato_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0));
               if ( ! (0==AV26TFContratoServicosArtefato_Obrigatorio_Sel) )
               {
                  Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set = StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosartefato_obrigatorio_Internalname, "SelectedValue_set", Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set);
               }
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV9GridState.FromXml(AV16Session.Get(AV38Pgmname+"GridState"), "");
         AV9GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV9GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV9GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFArtefatos_Codigo)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFARTEFATOS_CODIGO";
            AV10GridStateFilterValue.gxTpr_Value = AV22TFArtefatos_Codigo;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! (0==AV23TFArtefatos_Codigo_Sel) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFARTEFATOS_CODIGO_SEL";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV23TFArtefatos_Codigo_Sel), 6, 0);
            AV10GridStateFilterValue.gxTpr_Valueto = AV29TFArtefatos_Codigo_SelDsc;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! (0==AV26TFContratoServicosArtefato_Obrigatorio_Sel) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV26TFContratoServicosArtefato_Obrigatorio_Sel), 1, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! (0==AV6ContratoServicos_Codigo) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOSERVICOS_CODIGO";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV38Pgmname+"GridState",  AV9GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV17TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV17TrnContext.gxTpr_Callerobject = AV38Pgmname;
         AV17TrnContext.gxTpr_Callerondelete = true;
         AV17TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV17TrnContext.gxTpr_Transactionname = "ContratoServicos";
         AV18TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV18TrnContextAtt.gxTpr_Attributename = "ContratoServicos_Codigo";
         AV18TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0);
         AV17TrnContext.gxTpr_Attributes.Add(AV18TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV17TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E12N62( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            new prc_insartefatospadrao(context ).execute( ref  AV6ContratoServicos_Codigo, ref  AV15Servico_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Servico_Codigo), 6, 0)));
            context.DoAjaxRefreshCmp(sPrefix);
         }
      }

      protected void S162( )
      {
         /* 'FALTAM' Routine */
         AV8Faltam = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Faltam", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Faltam), 3, 0)));
         /* Using cursor H00N68 */
         pr_default.execute(6, new Object[] {AV15Servico_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1766ServicoArtefato_ArtefatoCod = H00N68_A1766ServicoArtefato_ArtefatoCod[0];
            A155Servico_Codigo = H00N68_A155Servico_Codigo[0];
            AV41GXLvl356 = 0;
            /* Using cursor H00N69 */
            pr_default.execute(7, new Object[] {AV6ContratoServicos_Codigo, A1766ServicoArtefato_ArtefatoCod});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A1749Artefatos_Codigo = H00N69_A1749Artefatos_Codigo[0];
               A160ContratoServicos_Codigo = H00N69_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               AV41GXLvl356 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
            if ( AV41GXLvl356 == 0 )
            {
               AV8Faltam = (short)(AV8Faltam+1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Faltam", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Faltam), 3, 0)));
            }
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void wb_table1_2_N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_N62( true) ;
         }
         else
         {
            wb_table2_5_N62( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_N62( true) ;
         }
         else
         {
            wb_table3_29_N62( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N62e( true) ;
         }
         else
         {
            wb_table1_2_N62e( false) ;
         }
      }

      protected void wb_table3_29_N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANEL2Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_N62e( true) ;
         }
         else
         {
            wb_table3_29_N62e( false) ;
         }
      }

      protected void wb_table2_5_N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_8_N62( true) ;
         }
         else
         {
            wb_table4_8_N62( false) ;
         }
         return  ;
      }

      protected void wb_table4_8_N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_20_N62( true) ;
         }
         else
         {
            wb_table5_20_N62( false) ;
         }
         return  ;
      }

      protected void wb_table5_20_N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_N62e( true) ;
         }
         else
         {
            wb_table2_5_N62e( false) ;
         }
      }

      protected void wb_table5_20_N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir artefatos", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosArtefatosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgClonar_Internalname, context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgClonar_Visible, 1, "", "Copiar artefatos do cat�logo de servi�os", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgClonar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLONAR\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosArtefatosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_N62e( true) ;
         }
         else
         {
            wb_table5_20_N62e( false) ;
         }
      }

      protected void wb_table4_8_N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynArtefatos_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( dynArtefatos_Codigo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynArtefatos_Codigo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratoServicosArtefato_Obrigatorio_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratoServicosArtefato_Obrigatorio.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratoServicosArtefato_Obrigatorio.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Update));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV7Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynArtefatos_Codigo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynArtefatos_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1768ContratoServicosArtefato_Obrigatorio));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratoServicosArtefato_Obrigatorio.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratoServicosArtefato_Obrigatorio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_8_N62e( true) ;
         }
         else
         {
            wb_table4_8_N62e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN62( ) ;
         WSN62( ) ;
         WEN62( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV6ContratoServicos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAN62( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosartefatoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAN62( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV6ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
         }
         wcpOAV6ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContratoServicos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV6ContratoServicos_Codigo != wcpOAV6ContratoServicos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV6ContratoServicos_Codigo = AV6ContratoServicos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV6ContratoServicos_Codigo = cgiGet( sPrefix+"AV6ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV6ContratoServicos_Codigo) > 0 )
         {
            AV6ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV6ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            AV6ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV6ContratoServicos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAN62( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSN62( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSN62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV6ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlAV6ContratoServicos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEN62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428239388");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosartefatoswc.js", "?2020428239388");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_11_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_11_idx;
         dynArtefatos_Codigo_Internalname = sPrefix+"ARTEFATOS_CODIGO_"+sGXsfl_11_idx;
         chkContratoServicosArtefato_Obrigatorio_Internalname = sPrefix+"CONTRATOSERVICOSARTEFATO_OBRIGATORIO_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_11_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_11_fel_idx;
         dynArtefatos_Codigo_Internalname = sPrefix+"ARTEFATOS_CODIGO_"+sGXsfl_11_fel_idx;
         chkContratoServicosArtefato_Obrigatorio_Internalname = sPrefix+"CONTRATOSERVICOSARTEFATO_OBRIGATORIO_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBN60( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_11_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavUpdate_Enabled!=0)&&(edtavUpdate_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'"+sPrefix+"',false,'',11)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV19Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV36Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV36Update_GXI : context.PathToRelativeUrl( AV19Update)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavUpdate_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+sGXsfl_11_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV19Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'"+sPrefix+"',false,'',11)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV7Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV37Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV37Delete_GXI : context.PathToRelativeUrl( AV7Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+sGXsfl_11_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV7Delete_IsBlob,(bool)false});
            GXAARTEFATOS_CODIGO_htmlN62( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ARTEFATOS_CODIGO_" + sGXsfl_11_idx;
               dynArtefatos_Codigo.Name = GXCCtl;
               dynArtefatos_Codigo.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynArtefatos_Codigo,(String)dynArtefatos_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)),(short)1,(String)dynArtefatos_Codigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynArtefatos_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynArtefatos_Codigo_Internalname, "Values", (String)(dynArtefatos_Codigo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratoServicosArtefato_Obrigatorio_Internalname,StringUtil.BoolToStr( A1768ContratoServicosArtefato_Obrigatorio),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ARTEFATOS_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSARTEFATO_OBRIGATORIO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, A1768ContratoServicosArtefato_Obrigatorio));
            GridContainer.AddRow(GridRow);
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         dynArtefatos_Codigo_Internalname = sPrefix+"ARTEFATOS_CODIGO";
         chkContratoServicosArtefato_Obrigatorio_Internalname = sPrefix+"CONTRATOSERVICOSARTEFATO_OBRIGATORIO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgClonar_Internalname = sPrefix+"CLONAR";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         Confirmpanel2_Internalname = sPrefix+"CONFIRMPANEL2";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavContratoservicos_codigo_Internalname = sPrefix+"vCONTRATOSERVICOS_CODIGO";
         edtavServico_codigo_Internalname = sPrefix+"vSERVICO_CODIGO";
         edtavFaltam_Internalname = sPrefix+"vFALTAM";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfartefatos_codigo_Internalname = sPrefix+"vTFARTEFATOS_CODIGO";
         edtavTfartefatos_codigo_sel_Internalname = sPrefix+"vTFARTEFATOS_CODIGO_SEL";
         edtavTfartefatos_codigo_seldsc_Internalname = sPrefix+"vTFARTEFATOS_CODIGO_SELDSC";
         edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL";
         Ddo_artefatos_codigo_Internalname = sPrefix+"DDO_ARTEFATOS_CODIGO";
         edtavDdo_artefatos_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosartefato_obrigatorio_Internalname = sPrefix+"DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO";
         edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         dynArtefatos_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Jsonclick = "";
         edtavUpdate_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavUpdate_Tooltiptext = "Modifica";
         chkContratoServicosArtefato_Obrigatorio_Titleformat = 0;
         dynArtefatos_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgClonar_Visible = 1;
         imgInsert_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkContratoServicosArtefato_Obrigatorio.Title.Text = "Obrigat�rio";
         dynArtefatos_Codigo.Title.Text = "Descri��o";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratoServicosArtefato_Obrigatorio.Caption = "";
         edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_artefatos_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosartefato_obrigatorio_sel_Jsonclick = "";
         edtavTfcontratoservicosartefato_obrigatorio_sel_Visible = 1;
         edtavTfartefatos_codigo_seldsc_Jsonclick = "";
         edtavTfartefatos_codigo_seldsc_Visible = 1;
         edtavTfartefatos_codigo_sel_Jsonclick = "";
         edtavTfartefatos_codigo_sel_Visible = 1;
         edtavTfartefatos_codigo_Jsonclick = "";
         edtavTfartefatos_codigo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtavFaltam_Jsonclick = "";
         edtavFaltam_Visible = 1;
         edtavServico_codigo_Jsonclick = "";
         edtavServico_codigo_Visible = 1;
         edtavContratoservicos_codigo_Jsonclick = "";
         edtavContratoservicos_codigo_Visible = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Visible = 1;
         Ddo_contratoservicosartefato_obrigatorio_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosartefato_obrigatorio_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosartefato_obrigatorio_Rangefilterto = "At�";
         Ddo_contratoservicosartefato_obrigatorio_Rangefilterfrom = "Desde";
         Ddo_contratoservicosartefato_obrigatorio_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosartefato_obrigatorio_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosartefato_obrigatorio_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosartefato_obrigatorio_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosartefato_obrigatorio_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosartefato_obrigatorio_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratoservicosartefato_obrigatorio_Datalisttype = "FixedValues";
         Ddo_contratoservicosartefato_obrigatorio_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosartefato_obrigatorio_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosartefato_obrigatorio_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosartefato_obrigatorio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosartefato_obrigatorio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosartefato_obrigatorio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosartefato_obrigatorio_Cls = "ColumnSettings";
         Ddo_contratoservicosartefato_obrigatorio_Tooltip = "Op��es";
         Ddo_contratoservicosartefato_obrigatorio_Caption = "";
         Ddo_artefatos_codigo_Searchbuttontext = "Pesquisar";
         Ddo_artefatos_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_artefatos_codigo_Rangefilterto = "At�";
         Ddo_artefatos_codigo_Rangefilterfrom = "Desde";
         Ddo_artefatos_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_artefatos_codigo_Loadingdata = "Carregando dados...";
         Ddo_artefatos_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_artefatos_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_artefatos_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_artefatos_codigo_Datalistproc = "GetContratoServicosArtefatosWCFilterData";
         Ddo_artefatos_codigo_Datalistfixedvalues = "";
         Ddo_artefatos_codigo_Datalisttype = "Dynamic";
         Ddo_artefatos_codigo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_artefatos_codigo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_artefatos_codigo_Filtertype = "Character";
         Ddo_artefatos_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_artefatos_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_artefatos_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_artefatos_codigo_Titlecontrolidtoreplace = "";
         Ddo_artefatos_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_artefatos_codigo_Cls = "ColumnSettings";
         Ddo_artefatos_codigo_Tooltip = "Op��es";
         Ddo_artefatos_codigo_Caption = "";
         Confirmpanel2_Confirmtype = "0";
         Confirmpanel2_Buttonnotext = "N�o";
         Confirmpanel2_Buttonyestext = "Fechar";
         Confirmpanel2_Confirmtext = "N�o existem mais artefatos padr�o no cat�logo deste servi�o!";
         Confirmpanel2_Icon = "2";
         Confirmpanel2_Title = "Aten��o";
         Confirmpanel_Confirmtype = "1";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Sim";
         Confirmpanel_Confirmtext = "Confirma copiar os artefatos padr�es do cat�logo?";
         Confirmpanel_Icon = "3";
         Confirmpanel_Title = "Confirma��o";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21Artefatos_CodigoTitleFilterData',fld:'vARTEFATOS_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV25ContratoServicosArtefato_ObrigatorioTitleFilterData',fld:'vCONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLEFILTERDATA',pic:'',nv:null},{av:'dynArtefatos_Codigo'},{av:'chkContratoServicosArtefato_Obrigatorio_Titleformat',ctrl:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'Titleformat'},{av:'chkContratoServicosArtefato_Obrigatorio.Title.Text',ctrl:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'Title'},{av:'AV31GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV32GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'imgClonar_Visible',ctrl:'CLONAR',prop:'Visible'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_ARTEFATOS_CODIGO.ONOPTIONCLICKED","{handler:'E13N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_artefatos_codigo_Activeeventkey',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_artefatos_codigo_Filteredtext_get',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'FilteredText_get'},{av:'Ddo_artefatos_codigo_Selectedvalue_get',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'SelectedValue_get'},{av:'Ddo_artefatos_codigo_Selectedtext_get',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'SelectedText_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_artefatos_codigo_Sortedstatus',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'SortedStatus'},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'Ddo_contratoservicosartefato_obrigatorio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO.ONOPTIONCLICKED","{handler:'E14N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosartefato_obrigatorio_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosartefato_obrigatorio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIO',prop:'SortedStatus'},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'Ddo_artefatos_codigo_Sortedstatus',ctrl:'DDO_ARTEFATOS_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19N62',iparms:[],oparms:[{av:'AV19Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'AV7Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E15N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLONAR'","{handler:'E16N62',iparms:[{av:'AV8Faltam',fld:'vFALTAM',pic:'ZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1766ServicoArtefato_ArtefatoCod',fld:'SERVICOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8Faltam',fld:'vFALTAM',pic:'ZZ9',nv:0}]}");
         setEventMetadata("'DOUPDATE'","{handler:'E20N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E21N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E12N62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFArtefatos_Codigo',fld:'vTFARTEFATOS_CODIGO',pic:'@!',nv:''},{av:'AV23TFArtefatos_Codigo_Sel',fld:'vTFARTEFATOS_CODIGO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV26TFContratoServicosArtefato_Obrigatorio_Sel',fld:'vTFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_Artefatos_CodigoTitleControlIdToReplace',fld:'vDDO_ARTEFATOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSARTEFATO_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29TFArtefatos_Codigo_SelDsc',fld:'vTFARTEFATOS_CODIGO_SELDSC',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'AV15Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_artefatos_codigo_Activeeventkey = "";
         Ddo_artefatos_codigo_Filteredtext_get = "";
         Ddo_artefatos_codigo_Selectedvalue_get = "";
         Ddo_artefatos_codigo_Selectedtext_get = "";
         Ddo_contratoservicosartefato_obrigatorio_Activeeventkey = "";
         Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get = "";
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22TFArtefatos_Codigo = "";
         AV24ddo_Artefatos_CodigoTitleControlIdToReplace = "";
         AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace = "";
         AV38Pgmname = "";
         AV29TFArtefatos_Codigo_SelDsc = "";
         GXKey = "";
         forbiddenHiddens = "";
         scmdbuf = "";
         H00N62_A1751Artefatos_Descricao = new String[] {""} ;
         A1751Artefatos_Descricao = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV28DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV21Artefatos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25ContratoServicosArtefato_ObrigatorioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Confirmpanel_Width = "";
         Confirmpanel2_Width = "";
         Ddo_artefatos_codigo_Filteredtext_set = "";
         Ddo_artefatos_codigo_Selectedvalue_set = "";
         Ddo_artefatos_codigo_Selectedtext_set = "";
         Ddo_artefatos_codigo_Sortedstatus = "";
         Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set = "";
         Ddo_contratoservicosartefato_obrigatorio_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Update = "";
         AV36Update_GXI = "";
         AV7Delete = "";
         AV37Delete_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00N63_A1749Artefatos_Codigo = new int[1] ;
         H00N63_A1751Artefatos_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV22TFArtefatos_Codigo = "";
         H00N64_A1751Artefatos_Descricao = new String[] {""} ;
         H00N64_A160ContratoServicos_Codigo = new int[1] ;
         H00N64_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00N64_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00N64_A1749Artefatos_Codigo = new int[1] ;
         H00N65_AGRID_nRecordCount = new long[1] ;
         H00N66_A1751Artefatos_Descricao = new String[] {""} ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H00N67_A160ContratoServicos_Codigo = new int[1] ;
         H00N67_A155Servico_Codigo = new int[1] ;
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV9GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV17TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV18TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00N68_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         H00N68_A155Servico_Codigo = new int[1] ;
         H00N69_A1749Artefatos_Codigo = new int[1] ;
         H00N69_A160ContratoServicos_Codigo = new int[1] ;
         sStyleString = "";
         imgInsert_Jsonclick = "";
         imgClonar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV6ContratoServicos_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosartefatoswc__default(),
            new Object[][] {
                new Object[] {
               H00N62_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00N63_A1749Artefatos_Codigo, H00N63_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00N64_A1751Artefatos_Descricao, H00N64_A160ContratoServicos_Codigo, H00N64_A1768ContratoServicosArtefato_Obrigatorio, H00N64_n1768ContratoServicosArtefato_Obrigatorio, H00N64_A1749Artefatos_Codigo
               }
               , new Object[] {
               H00N65_AGRID_nRecordCount
               }
               , new Object[] {
               H00N66_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00N67_A160ContratoServicos_Codigo, H00N67_A155Servico_Codigo
               }
               , new Object[] {
               H00N68_A1766ServicoArtefato_ArtefatoCod, H00N68_A155Servico_Codigo
               }
               , new Object[] {
               H00N69_A1749Artefatos_Codigo, H00N69_A160ContratoServicos_Codigo
               }
            }
         );
         AV38Pgmname = "ContratoServicosArtefatosWC";
         /* GeneXus formulas. */
         AV38Pgmname = "ContratoServicosArtefatosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short AV13OrderedBy ;
      private short AV26TFContratoServicosArtefato_Obrigatorio_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV8Faltam ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short dynArtefatos_Codigo_Titleformat ;
      private short chkContratoServicosArtefato_Obrigatorio_Titleformat ;
      private short AV41GXLvl356 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV6ContratoServicos_Codigo ;
      private int wcpOAV6ContratoServicos_Codigo ;
      private int subGrid_Rows ;
      private int AV23TFArtefatos_Codigo_Sel ;
      private int A160ContratoServicos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int A155Servico_Codigo ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_artefatos_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosartefato_obrigatorio_Datalistupdateminimumcharacters ;
      private int edtContratoServicos_Codigo_Visible ;
      private int edtavContratoservicos_codigo_Visible ;
      private int AV15Servico_Codigo ;
      private int edtavServico_codigo_Visible ;
      private int edtavFaltam_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfartefatos_codigo_Visible ;
      private int edtavTfartefatos_codigo_sel_Visible ;
      private int edtavTfartefatos_codigo_seldsc_Visible ;
      private int edtavTfcontratoservicosartefato_obrigatorio_sel_Visible ;
      private int edtavDdo_artefatos_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgInsert_Visible ;
      private int imgClonar_Visible ;
      private int AV30PageToGo ;
      private int AV39GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV31GridCurrentPage ;
      private long AV32GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_artefatos_codigo_Activeeventkey ;
      private String Ddo_artefatos_codigo_Filteredtext_get ;
      private String Ddo_artefatos_codigo_Selectedvalue_get ;
      private String Ddo_artefatos_codigo_Selectedtext_get ;
      private String Ddo_contratoservicosartefato_obrigatorio_Activeeventkey ;
      private String Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_get ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String AV38Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Confirmtype ;
      private String Confirmpanel2_Width ;
      private String Confirmpanel2_Title ;
      private String Confirmpanel2_Icon ;
      private String Confirmpanel2_Confirmtext ;
      private String Confirmpanel2_Buttonyestext ;
      private String Confirmpanel2_Buttonnotext ;
      private String Confirmpanel2_Confirmtype ;
      private String Ddo_artefatos_codigo_Caption ;
      private String Ddo_artefatos_codigo_Tooltip ;
      private String Ddo_artefatos_codigo_Cls ;
      private String Ddo_artefatos_codigo_Filteredtext_set ;
      private String Ddo_artefatos_codigo_Selectedvalue_set ;
      private String Ddo_artefatos_codigo_Selectedtext_set ;
      private String Ddo_artefatos_codigo_Dropdownoptionstype ;
      private String Ddo_artefatos_codigo_Titlecontrolidtoreplace ;
      private String Ddo_artefatos_codigo_Sortedstatus ;
      private String Ddo_artefatos_codigo_Filtertype ;
      private String Ddo_artefatos_codigo_Datalisttype ;
      private String Ddo_artefatos_codigo_Datalistfixedvalues ;
      private String Ddo_artefatos_codigo_Datalistproc ;
      private String Ddo_artefatos_codigo_Sortasc ;
      private String Ddo_artefatos_codigo_Sortdsc ;
      private String Ddo_artefatos_codigo_Loadingdata ;
      private String Ddo_artefatos_codigo_Cleanfilter ;
      private String Ddo_artefatos_codigo_Rangefilterfrom ;
      private String Ddo_artefatos_codigo_Rangefilterto ;
      private String Ddo_artefatos_codigo_Noresultsfound ;
      private String Ddo_artefatos_codigo_Searchbuttontext ;
      private String Ddo_contratoservicosartefato_obrigatorio_Caption ;
      private String Ddo_contratoservicosartefato_obrigatorio_Tooltip ;
      private String Ddo_contratoservicosartefato_obrigatorio_Cls ;
      private String Ddo_contratoservicosartefato_obrigatorio_Selectedvalue_set ;
      private String Ddo_contratoservicosartefato_obrigatorio_Dropdownoptionstype ;
      private String Ddo_contratoservicosartefato_obrigatorio_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosartefato_obrigatorio_Sortedstatus ;
      private String Ddo_contratoservicosartefato_obrigatorio_Datalisttype ;
      private String Ddo_contratoservicosartefato_obrigatorio_Datalistfixedvalues ;
      private String Ddo_contratoservicosartefato_obrigatorio_Sortasc ;
      private String Ddo_contratoservicosartefato_obrigatorio_Sortdsc ;
      private String Ddo_contratoservicosartefato_obrigatorio_Loadingdata ;
      private String Ddo_contratoservicosartefato_obrigatorio_Cleanfilter ;
      private String Ddo_contratoservicosartefato_obrigatorio_Rangefilterfrom ;
      private String Ddo_contratoservicosartefato_obrigatorio_Rangefilterto ;
      private String Ddo_contratoservicosartefato_obrigatorio_Noresultsfound ;
      private String Ddo_contratoservicosartefato_obrigatorio_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String edtavContratoservicos_codigo_Internalname ;
      private String edtavContratoservicos_codigo_Jsonclick ;
      private String TempTags ;
      private String edtavServico_codigo_Internalname ;
      private String edtavServico_codigo_Jsonclick ;
      private String edtavFaltam_Internalname ;
      private String edtavFaltam_Jsonclick ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfartefatos_codigo_Internalname ;
      private String edtavTfartefatos_codigo_Jsonclick ;
      private String edtavTfartefatos_codigo_sel_Internalname ;
      private String edtavTfartefatos_codigo_sel_Jsonclick ;
      private String edtavTfartefatos_codigo_seldsc_Internalname ;
      private String edtavTfartefatos_codigo_seldsc_Jsonclick ;
      private String edtavTfcontratoservicosartefato_obrigatorio_sel_Internalname ;
      private String edtavTfcontratoservicosartefato_obrigatorio_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_artefatos_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosartefato_obrigatoriotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String dynArtefatos_Codigo_Internalname ;
      private String chkContratoServicosArtefato_Obrigatorio_Internalname ;
      private String GXCCtl ;
      private String gxwrpcisep ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_artefatos_codigo_Internalname ;
      private String Ddo_contratoservicosartefato_obrigatorio_Internalname ;
      private String imgInsert_Internalname ;
      private String imgClonar_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgClonar_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV6ContratoServicos_Codigo ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String edtavUpdate_Jsonclick ;
      private String edtavDelete_Jsonclick ;
      private String dynArtefatos_Codigo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Confirmpanel_Internalname ;
      private String Confirmpanel2_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_artefatos_codigo_Includesortasc ;
      private bool Ddo_artefatos_codigo_Includesortdsc ;
      private bool Ddo_artefatos_codigo_Includefilter ;
      private bool Ddo_artefatos_codigo_Filterisrange ;
      private bool Ddo_artefatos_codigo_Includedatalist ;
      private bool Ddo_contratoservicosartefato_obrigatorio_Includesortasc ;
      private bool Ddo_contratoservicosartefato_obrigatorio_Includesortdsc ;
      private bool Ddo_contratoservicosartefato_obrigatorio_Includefilter ;
      private bool Ddo_contratoservicosartefato_obrigatorio_Filterisrange ;
      private bool Ddo_contratoservicosartefato_obrigatorio_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV19Update_IsBlob ;
      private bool AV7Delete_IsBlob ;
      private String AV22TFArtefatos_Codigo ;
      private String AV24ddo_Artefatos_CodigoTitleControlIdToReplace ;
      private String AV27ddo_ContratoServicosArtefato_ObrigatorioTitleControlIdToReplace ;
      private String AV29TFArtefatos_Codigo_SelDsc ;
      private String A1751Artefatos_Descricao ;
      private String AV36Update_GXI ;
      private String AV37Delete_GXI ;
      private String lV22TFArtefatos_Codigo ;
      private String AV19Update ;
      private String AV7Delete ;
      private IGxSession AV16Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynArtefatos_Codigo ;
      private GXCheckbox chkContratoServicosArtefato_Obrigatorio ;
      private IDataStoreProvider pr_default ;
      private String[] H00N62_A1751Artefatos_Descricao ;
      private int[] H00N63_A1749Artefatos_Codigo ;
      private String[] H00N63_A1751Artefatos_Descricao ;
      private String[] H00N64_A1751Artefatos_Descricao ;
      private int[] H00N64_A160ContratoServicos_Codigo ;
      private bool[] H00N64_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] H00N64_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] H00N64_A1749Artefatos_Codigo ;
      private long[] H00N65_AGRID_nRecordCount ;
      private String[] H00N66_A1751Artefatos_Descricao ;
      private int[] H00N67_A160ContratoServicos_Codigo ;
      private int[] H00N67_A155Servico_Codigo ;
      private int[] H00N68_A1766ServicoArtefato_ArtefatoCod ;
      private int[] H00N68_A155Servico_Codigo ;
      private int[] H00N69_A1749Artefatos_Codigo ;
      private int[] H00N69_A160ContratoServicos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21Artefatos_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV25ContratoServicosArtefato_ObrigatorioTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV9GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV10GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV17TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV18TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV28DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicosartefatoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00N64( IGxContext context ,
                                             int AV23TFArtefatos_Codigo_Sel ,
                                             String AV22TFArtefatos_Codigo ,
                                             short AV26TFContratoServicosArtefato_Obrigatorio_Sel ,
                                             String A1751Artefatos_Descricao ,
                                             int A1749Artefatos_Codigo ,
                                             bool A1768ContratoServicosArtefato_Obrigatorio ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV6ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Artefatos_Descricao], T1.[ContratoServicos_Codigo], T1.[ContratoServicosArtefato_Obrigatorio], T1.[Artefatos_Codigo]";
         sFromString = " FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicos_Codigo] = @AV6ContratoServicos_Codigo)";
         if ( (0==AV23TFArtefatos_Codigo_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFArtefatos_Codigo)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV22TFArtefatos_Codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV23TFArtefatos_Codigo_Sel) )
         {
            sWhereString = sWhereString + " and (T1.[Artefatos_Codigo] = @AV23TFArtefatos_Codigo_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV26TFContratoServicosArtefato_Obrigatorio_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 1)";
         }
         if ( AV26TFContratoServicosArtefato_Obrigatorio_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 0)";
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Artefatos_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC, T1.[Artefatos_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T1.[ContratoServicosArtefato_Obrigatorio]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC, T1.[ContratoServicosArtefato_Obrigatorio] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00N65( IGxContext context ,
                                             int AV23TFArtefatos_Codigo_Sel ,
                                             String AV22TFArtefatos_Codigo ,
                                             short AV26TFContratoServicosArtefato_Obrigatorio_Sel ,
                                             String A1751Artefatos_Descricao ,
                                             int A1749Artefatos_Codigo ,
                                             bool A1768ContratoServicosArtefato_Obrigatorio ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV6ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicos_Codigo] = @AV6ContratoServicos_Codigo)";
         if ( (0==AV23TFArtefatos_Codigo_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFArtefatos_Codigo)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV22TFArtefatos_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV23TFArtefatos_Codigo_Sel) )
         {
            sWhereString = sWhereString + " and (T1.[Artefatos_Codigo] = @AV23TFArtefatos_Codigo_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV26TFContratoServicosArtefato_Obrigatorio_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 1)";
         }
         if ( AV26TFContratoServicosArtefato_Obrigatorio_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00N64(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
               case 3 :
                     return conditional_H00N65(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N62 ;
          prmH00N62 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N63 ;
          prmH00N63 = new Object[] {
          } ;
          Object[] prmH00N66 ;
          prmH00N66 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N67 ;
          prmH00N67 = new Object[] {
          new Object[] {"@AV6ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N68 ;
          prmH00N68 = new Object[] {
          new Object[] {"@AV15Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N69 ;
          prmH00N69 = new Object[] {
          new Object[] {"@AV6ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N64 ;
          prmH00N64 = new Object[] {
          new Object[] {"@AV6ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFArtefatos_Codigo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFArtefatos_Codigo_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00N65 ;
          prmH00N65 = new Object[] {
          new Object[] {"@AV6ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFArtefatos_Codigo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFArtefatos_Codigo_Sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N62", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N62,1,0,true,true )
             ,new CursorDef("H00N63", "SELECT [Artefatos_Codigo], [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N63,0,0,true,false )
             ,new CursorDef("H00N64", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N64,11,0,true,false )
             ,new CursorDef("H00N65", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N65,1,0,true,false )
             ,new CursorDef("H00N66", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N66,1,0,true,false )
             ,new CursorDef("H00N67", "SELECT TOP 1 [ContratoServicos_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV6ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N67,1,0,false,true )
             ,new CursorDef("H00N68", "SELECT [ServicoArtefato_ArtefatoCod], [Servico_Codigo] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV15Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N68,100,0,true,false )
             ,new CursorDef("H00N69", "SELECT TOP 1 [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV6ContratoServicos_Codigo and [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N69,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
