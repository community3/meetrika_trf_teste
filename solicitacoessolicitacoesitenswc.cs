/*
               File: SolicitacoesSolicitacoesItensWC
        Description: Solicitacoes Solicitacoes Itens WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:1:41.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoessolicitacoesitenswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public solicitacoessolicitacoesitenswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public solicitacoessolicitacoesitenswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Solicitacoes_Codigo )
      {
         this.AV7Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Solicitacoes_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_83_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18SolicitacoesItens_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18SolicitacoesItens_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0)));
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22SolicitacoesItens_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22SolicitacoesItens_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0)));
                  AV24DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                  AV26SolicitacoesItens_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SolicitacoesItens_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0)));
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV35TFSolicitacoesItens_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
                  AV36TFSolicitacoesItens_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
                  AV39TFSolicitacoesItens_descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
                  AV40TFSolicitacoesItens_descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSolicitacoesItens_descricao_Sel", AV40TFSolicitacoesItens_descricao_Sel);
                  AV43TFFuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
                  AV44TFFuncaoUsuario_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
                  AV7Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0)));
                  AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace", AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace);
                  AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace", AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace);
                  AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace", AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace);
                  AV54Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
                  AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
                  A447SolicitacoesItens_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV54Pgmname = "SolicitacoesSolicitacoesItensWC";
               context.Gx_err = 0;
               WSAY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Solicitacoes Solicitacoes Itens WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282314184");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacoessolicitacoesitenswc.aspx") + "?" + UrlEncode("" +AV7Solicitacoes_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSOLICITACOESITENS_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSOLICITACOESITENS_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSOLICITACOESITENS_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO", AV39TFSolicitacoesItens_descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO_SEL", AV40TFSolicitacoesItens_descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSOLICITACOESITENS_CODIGOTITLEFILTERDATA", AV34SolicitacoesItens_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSOLICITACOESITENS_CODIGOTITLEFILTERDATA", AV34SolicitacoesItens_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSOLICITACOESITENS_DESCRICAOTITLEFILTERDATA", AV38SolicitacoesItens_descricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSOLICITACOESITENS_DESCRICAOTITLEFILTERDATA", AV38SolicitacoesItens_descricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOUSUARIO_CODIGOTITLEFILTERDATA", AV42FuncaoUsuario_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOUSUARIO_CODIGOTITLEFILTERDATA", AV42FuncaoUsuario_CodigoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSOLICITACOES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV54Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Caption", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Tooltip", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Cls", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoesitens_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoesitens_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoesitens_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filtertype", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoesitens_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoesitens_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoesitens_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortasc", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoesitens_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoesitens_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoesitens_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoesitens_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoesitens_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoesitens_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Caption", StringUtil.RTrim( Ddo_funcaousuario_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_funcaousuario_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Cls", StringUtil.RTrim( Ddo_funcaousuario_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_funcaousuario_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaousuario_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaousuario_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaousuario_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaousuario_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaousuario_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_funcaousuario_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_funcaousuario_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_funcaousuario_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_funcaousuario_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaousuario_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaousuario_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaousuario_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_funcaousuario_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_funcaousuario_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_funcaousuario_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_funcaousuario_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaousuario_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_funcaousuario_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_funcaousuario_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaousuario_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoesitens_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_solicitacoesitens_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_funcaousuario_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_funcaousuario_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaousuario_codigo_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("solicitacoessolicitacoesitenswc.js", "?20204282314332");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SolicitacoesSolicitacoesItensWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacoes Solicitacoes Itens WC" ;
      }

      protected void WBAY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "solicitacoessolicitacoesitenswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_AY2( true) ;
         }
         else
         {
            wb_table1_2_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSolicitacoes_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoesitens_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFSolicitacoesItens_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoesitens_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoesitens_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoesitens_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFSolicitacoesItens_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoesitens_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoesitens_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsolicitacoesitens_descricao_Internalname, AV39TFSolicitacoesItens_descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavTfsolicitacoesitens_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsolicitacoesitens_descricao_sel_Internalname, AV40TFSolicitacoesItens_descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavTfsolicitacoesitens_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFFuncaoUsuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFFuncaoUsuario_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SOLICITACOESITENS_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Internalname, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SOLICITACOESITENS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Internalname, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOUSUARIO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Internalname, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SolicitacoesSolicitacoesItensWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTAY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Solicitacoes Solicitacoes Itens WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAY0( ) ;
            }
         }
      }

      protected void WSAY2( )
      {
         STARTAY2( ) ;
         EVTAY2( ) ;
      }

      protected void EVTAY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AY2 */
                                    E11AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOESITENS_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AY2 */
                                    E12AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOESITENS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AY2 */
                                    E13AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOUSUARIO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AY2 */
                                    E14AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15AY2 */
                                    E15AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16AY2 */
                                    E16AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17AY2 */
                                    E17AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18AY2 */
                                    E18AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19AY2 */
                                    E19AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20AY2 */
                                    E20AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21AY2 */
                                    E21AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22AY2 */
                                    E22AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23AY2 */
                                    E23AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24AY2 */
                                    E24AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E25AY2 */
                                    E25AY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAY0( ) ;
                              }
                              nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                              SubsflControlProps_832( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV52Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV30Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV53Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV30Delete))));
                              A447SolicitacoesItens_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoesItens_Codigo_Internalname), ",", "."));
                              A448SolicitacoesItens_descricao = cgiGet( edtSolicitacoesItens_descricao_Internalname);
                              n448SolicitacoesItens_descricao = false;
                              A449SolicitacoesItens_Arquivo = cgiGet( edtSolicitacoesItens_Arquivo_Internalname);
                              n449SolicitacoesItens_Arquivo = false;
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
                              A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26AY2 */
                                          E26AY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E27AY2 */
                                          E27AY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E28AY2 */
                                          E28AY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Solicitacoesitens_codigo1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO1"), ",", ".") != Convert.ToDecimal( AV18SolicitacoesItens_Codigo1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Solicitacoesitens_codigo2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO2"), ",", ".") != Convert.ToDecimal( AV22SolicitacoesItens_Codigo2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Solicitacoesitens_codigo3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO3"), ",", ".") != Convert.ToDecimal( AV26SolicitacoesItens_Codigo3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacoesitens_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFSolicitacoesItens_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacoesitens_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFSolicitacoesItens_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacoesitens_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO"), AV39TFSolicitacoesItens_descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacoesitens_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO_SEL"), AV40TFSolicitacoesItens_descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaousuario_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoUsuario_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaousuario_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFFuncaoUsuario_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPAY0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAY2( ) ;
            }
         }
      }

      protected void PAAY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SOLICITACOESITENS_CODIGO", "Itens_Codigo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SOLICITACOESITENS_CODIGO", "Itens_Codigo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SOLICITACOESITENS_CODIGO", "Itens_Codigo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       int AV18SolicitacoesItens_Codigo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       int AV22SolicitacoesItens_Codigo2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       int AV26SolicitacoesItens_Codigo3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV35TFSolicitacoesItens_Codigo ,
                                       int AV36TFSolicitacoesItens_Codigo_To ,
                                       String AV39TFSolicitacoesItens_descricao ,
                                       String AV40TFSolicitacoesItens_descricao_Sel ,
                                       int AV43TFFuncaoUsuario_Codigo ,
                                       int AV44TFFuncaoUsuario_Codigo_To ,
                                       int AV7Solicitacoes_Codigo ,
                                       String AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace ,
                                       String AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace ,
                                       String AV54Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       int A447SolicitacoesItens_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAY2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOESITENS_DESCRICAO", GetSecureSignedToken( sPrefix, A448SolicitacoesItens_descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"SOLICITACOESITENS_DESCRICAO", A448SolicitacoesItens_descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOUSUARIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV54Pgmname = "SolicitacoesSolicitacoesItensWC";
         context.Gx_err = 0;
      }

      protected void RFAY2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E27AY2 */
         E27AY2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18SolicitacoesItens_Codigo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22SolicitacoesItens_Codigo2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV26SolicitacoesItens_Codigo3 ,
                                                 AV35TFSolicitacoesItens_Codigo ,
                                                 AV36TFSolicitacoesItens_Codigo_To ,
                                                 AV40TFSolicitacoesItens_descricao_Sel ,
                                                 AV39TFSolicitacoesItens_descricao ,
                                                 AV43TFFuncaoUsuario_Codigo ,
                                                 AV44TFFuncaoUsuario_Codigo_To ,
                                                 A447SolicitacoesItens_Codigo ,
                                                 A448SolicitacoesItens_descricao ,
                                                 A161FuncaoUsuario_Codigo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A439Solicitacoes_Codigo ,
                                                 AV7Solicitacoes_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV39TFSolicitacoesItens_descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFSolicitacoesItens_descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
            /* Using cursor H00AY2 */
            pr_default.execute(0, new Object[] {AV7Solicitacoes_Codigo, AV18SolicitacoesItens_Codigo1, AV18SolicitacoesItens_Codigo1, AV18SolicitacoesItens_Codigo1, AV22SolicitacoesItens_Codigo2, AV22SolicitacoesItens_Codigo2, AV22SolicitacoesItens_Codigo2, AV26SolicitacoesItens_Codigo3, AV26SolicitacoesItens_Codigo3, AV26SolicitacoesItens_Codigo3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, lV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A439Solicitacoes_Codigo = H00AY2_A439Solicitacoes_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               A161FuncaoUsuario_Codigo = H00AY2_A161FuncaoUsuario_Codigo[0];
               A448SolicitacoesItens_descricao = H00AY2_A448SolicitacoesItens_descricao[0];
               n448SolicitacoesItens_descricao = H00AY2_n448SolicitacoesItens_descricao[0];
               A447SolicitacoesItens_Codigo = H00AY2_A447SolicitacoesItens_Codigo[0];
               A449SolicitacoesItens_Arquivo = H00AY2_A449SolicitacoesItens_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
               n449SolicitacoesItens_Arquivo = H00AY2_n449SolicitacoesItens_Arquivo[0];
               /* Execute user event: E28AY2 */
               E28AY2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WBAY0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18SolicitacoesItens_Codigo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22SolicitacoesItens_Codigo2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV26SolicitacoesItens_Codigo3 ,
                                              AV35TFSolicitacoesItens_Codigo ,
                                              AV36TFSolicitacoesItens_Codigo_To ,
                                              AV40TFSolicitacoesItens_descricao_Sel ,
                                              AV39TFSolicitacoesItens_descricao ,
                                              AV43TFFuncaoUsuario_Codigo ,
                                              AV44TFFuncaoUsuario_Codigo_To ,
                                              A447SolicitacoesItens_Codigo ,
                                              A448SolicitacoesItens_descricao ,
                                              A161FuncaoUsuario_Codigo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A439Solicitacoes_Codigo ,
                                              AV7Solicitacoes_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV39TFSolicitacoesItens_descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFSolicitacoesItens_descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
         /* Using cursor H00AY3 */
         pr_default.execute(1, new Object[] {AV7Solicitacoes_Codigo, AV18SolicitacoesItens_Codigo1, AV18SolicitacoesItens_Codigo1, AV18SolicitacoesItens_Codigo1, AV22SolicitacoesItens_Codigo2, AV22SolicitacoesItens_Codigo2, AV22SolicitacoesItens_Codigo2, AV26SolicitacoesItens_Codigo3, AV26SolicitacoesItens_Codigo3, AV26SolicitacoesItens_Codigo3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, lV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To});
         GRID_nRecordCount = H00AY3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAY0( )
      {
         /* Before Start, stand alone formulas. */
         AV54Pgmname = "SolicitacoesSolicitacoesItensWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26AY2 */
         E26AY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV46DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSOLICITACOESITENS_CODIGOTITLEFILTERDATA"), AV34SolicitacoesItens_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSOLICITACOESITENS_DESCRICAOTITLEFILTERDATA"), AV38SolicitacoesItens_descricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOUSUARIO_CODIGOTITLEFILTERDATA"), AV42FuncaoUsuario_CodigoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSOLICITACOESITENS_CODIGO1");
               GX_FocusControl = edtavSolicitacoesitens_codigo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18SolicitacoesItens_Codigo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18SolicitacoesItens_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0)));
            }
            else
            {
               AV18SolicitacoesItens_Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18SolicitacoesItens_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSOLICITACOESITENS_CODIGO2");
               GX_FocusControl = edtavSolicitacoesitens_codigo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22SolicitacoesItens_Codigo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22SolicitacoesItens_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0)));
            }
            else
            {
               AV22SolicitacoesItens_Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22SolicitacoesItens_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSOLICITACOESITENS_CODIGO3");
               GX_FocusControl = edtavSolicitacoesitens_codigo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26SolicitacoesItens_Codigo3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SolicitacoesItens_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0)));
            }
            else
            {
               AV26SolicitacoesItens_Codigo3 = (int)(context.localUtil.CToN( cgiGet( edtavSolicitacoesitens_codigo3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SolicitacoesItens_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0)));
            }
            A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOESITENS_CODIGO");
               GX_FocusControl = edtavTfsolicitacoesitens_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFSolicitacoesItens_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
            }
            else
            {
               AV35TFSolicitacoesItens_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOESITENS_CODIGO_TO");
               GX_FocusControl = edtavTfsolicitacoesitens_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFSolicitacoesItens_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFSolicitacoesItens_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoesitens_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
            }
            AV39TFSolicitacoesItens_descricao = cgiGet( edtavTfsolicitacoesitens_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
            AV40TFSolicitacoesItens_descricao_Sel = cgiGet( edtavTfsolicitacoesitens_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSolicitacoesItens_descricao_Sel", AV40TFSolicitacoesItens_descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOUSUARIO_CODIGO");
               GX_FocusControl = edtavTffuncaousuario_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFFuncaoUsuario_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
            }
            else
            {
               AV43TFFuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOUSUARIO_CODIGO_TO");
               GX_FocusControl = edtavTffuncaousuario_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFFuncaoUsuario_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
            }
            else
            {
               AV44TFFuncaoUsuario_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaousuario_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
            }
            AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace", AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace);
            AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace", AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace);
            AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace", AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_83"), ",", "."));
            AV48GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV49GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Solicitacoes_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_solicitacoesitens_codigo_Caption = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Caption");
            Ddo_solicitacoesitens_codigo_Tooltip = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Tooltip");
            Ddo_solicitacoesitens_codigo_Cls = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Cls");
            Ddo_solicitacoesitens_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtext_set");
            Ddo_solicitacoesitens_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtextto_set");
            Ddo_solicitacoesitens_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Dropdownoptionstype");
            Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Titlecontrolidtoreplace");
            Ddo_solicitacoesitens_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includesortasc"));
            Ddo_solicitacoesitens_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includesortdsc"));
            Ddo_solicitacoesitens_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortedstatus");
            Ddo_solicitacoesitens_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includefilter"));
            Ddo_solicitacoesitens_codigo_Filtertype = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filtertype");
            Ddo_solicitacoesitens_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filterisrange"));
            Ddo_solicitacoesitens_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Includedatalist"));
            Ddo_solicitacoesitens_codigo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Datalistfixedvalues");
            Ddo_solicitacoesitens_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoesitens_codigo_Sortasc = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortasc");
            Ddo_solicitacoesitens_codigo_Sortdsc = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Sortdsc");
            Ddo_solicitacoesitens_codigo_Loadingdata = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Loadingdata");
            Ddo_solicitacoesitens_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Cleanfilter");
            Ddo_solicitacoesitens_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Rangefilterfrom");
            Ddo_solicitacoesitens_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Rangefilterto");
            Ddo_solicitacoesitens_codigo_Noresultsfound = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Noresultsfound");
            Ddo_solicitacoesitens_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Searchbuttontext");
            Ddo_solicitacoesitens_descricao_Caption = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Caption");
            Ddo_solicitacoesitens_descricao_Tooltip = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Tooltip");
            Ddo_solicitacoesitens_descricao_Cls = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Cls");
            Ddo_solicitacoesitens_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filteredtext_set");
            Ddo_solicitacoesitens_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Selectedvalue_set");
            Ddo_solicitacoesitens_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Dropdownoptionstype");
            Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_solicitacoesitens_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includesortasc"));
            Ddo_solicitacoesitens_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includesortdsc"));
            Ddo_solicitacoesitens_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortedstatus");
            Ddo_solicitacoesitens_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includefilter"));
            Ddo_solicitacoesitens_descricao_Filtertype = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filtertype");
            Ddo_solicitacoesitens_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filterisrange"));
            Ddo_solicitacoesitens_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Includedatalist"));
            Ddo_solicitacoesitens_descricao_Datalisttype = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalisttype");
            Ddo_solicitacoesitens_descricao_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistfixedvalues");
            Ddo_solicitacoesitens_descricao_Datalistproc = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistproc");
            Ddo_solicitacoesitens_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoesitens_descricao_Sortasc = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortasc");
            Ddo_solicitacoesitens_descricao_Sortdsc = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Sortdsc");
            Ddo_solicitacoesitens_descricao_Loadingdata = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Loadingdata");
            Ddo_solicitacoesitens_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Cleanfilter");
            Ddo_solicitacoesitens_descricao_Rangefilterfrom = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Rangefilterfrom");
            Ddo_solicitacoesitens_descricao_Rangefilterto = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Rangefilterto");
            Ddo_solicitacoesitens_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Noresultsfound");
            Ddo_solicitacoesitens_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Searchbuttontext");
            Ddo_funcaousuario_codigo_Caption = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Caption");
            Ddo_funcaousuario_codigo_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Tooltip");
            Ddo_funcaousuario_codigo_Cls = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Cls");
            Ddo_funcaousuario_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtext_set");
            Ddo_funcaousuario_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtextto_set");
            Ddo_funcaousuario_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Dropdownoptionstype");
            Ddo_funcaousuario_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Titlecontrolidtoreplace");
            Ddo_funcaousuario_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includesortasc"));
            Ddo_funcaousuario_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includesortdsc"));
            Ddo_funcaousuario_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortedstatus");
            Ddo_funcaousuario_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includefilter"));
            Ddo_funcaousuario_codigo_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filtertype");
            Ddo_funcaousuario_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filterisrange"));
            Ddo_funcaousuario_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Includedatalist"));
            Ddo_funcaousuario_codigo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Datalistfixedvalues");
            Ddo_funcaousuario_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaousuario_codigo_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortasc");
            Ddo_funcaousuario_codigo_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Sortdsc");
            Ddo_funcaousuario_codigo_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Loadingdata");
            Ddo_funcaousuario_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Cleanfilter");
            Ddo_funcaousuario_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Rangefilterfrom");
            Ddo_funcaousuario_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Rangefilterto");
            Ddo_funcaousuario_codigo_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Noresultsfound");
            Ddo_funcaousuario_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_solicitacoesitens_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Activeeventkey");
            Ddo_solicitacoesitens_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtext_get");
            Ddo_solicitacoesitens_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_CODIGO_Filteredtextto_get");
            Ddo_solicitacoesitens_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Activeeventkey");
            Ddo_solicitacoesitens_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Filteredtext_get");
            Ddo_solicitacoesitens_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO_Selectedvalue_get");
            Ddo_funcaousuario_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Activeeventkey");
            Ddo_funcaousuario_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtext_get");
            Ddo_funcaousuario_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_CODIGO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO1"), ",", ".") != Convert.ToDecimal( AV18SolicitacoesItens_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO2"), ",", ".") != Convert.ToDecimal( AV22SolicitacoesItens_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vSOLICITACOESITENS_CODIGO3"), ",", ".") != Convert.ToDecimal( AV26SolicitacoesItens_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFSolicitacoesItens_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFSolicitacoesItens_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO"), AV39TFSolicitacoesItens_descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSOLICITACOESITENS_DESCRICAO_SEL"), AV40TFSolicitacoesItens_descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoUsuario_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFFuncaoUsuario_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26AY2 */
         E26AY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26AY2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfsolicitacoesitens_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacoesitens_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoesitens_codigo_Visible), 5, 0)));
         edtavTfsolicitacoesitens_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacoesitens_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoesitens_codigo_to_Visible), 5, 0)));
         edtavTfsolicitacoesitens_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacoesitens_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoesitens_descricao_Visible), 5, 0)));
         edtavTfsolicitacoesitens_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacoesitens_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoesitens_descricao_sel_Visible), 5, 0)));
         edtavTffuncaousuario_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaousuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_codigo_Visible), 5, 0)));
         edtavTffuncaousuario_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaousuario_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_codigo_to_Visible), 5, 0)));
         Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacoesItens_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "TitleControlIdToReplace", Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace);
         AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace = Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace", AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace);
         edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacoesItens_descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "TitleControlIdToReplace", Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace);
         AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace = Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace", AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace);
         edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaousuario_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoUsuario_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "TitleControlIdToReplace", Ddo_funcaousuario_codigo_Titlecontrolidtoreplace);
         AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace = Ddo_funcaousuario_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace", AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace);
         edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         edtSolicitacoes_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoes_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Itens_Codigo", 0);
         cmbavOrderedby.addItem("2", "Itens_descricao", 0);
         cmbavOrderedby.addItem("3", "Fun��o de Usu�rio", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV46DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV46DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27AY2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34SolicitacoesItens_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38SolicitacoesItens_descricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoUsuario_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSolicitacoesItens_Codigo_Titleformat = 2;
         edtSolicitacoesItens_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Itens_Codigo", AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Codigo_Internalname, "Title", edtSolicitacoesItens_Codigo_Title);
         edtSolicitacoesItens_descricao_Titleformat = 2;
         edtSolicitacoesItens_descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Itens_descricao", AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_descricao_Internalname, "Title", edtSolicitacoesItens_descricao_Title);
         edtFuncaoUsuario_Codigo_Titleformat = 2;
         edtFuncaoUsuario_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fun��o de Usu�rio", AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoUsuario_Codigo_Internalname, "Title", edtFuncaoUsuario_Codigo_Title);
         AV48GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48GridCurrentPage), 10, 0)));
         AV49GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34SolicitacoesItens_CodigoTitleFilterData", AV34SolicitacoesItens_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38SolicitacoesItens_descricaoTitleFilterData", AV38SolicitacoesItens_descricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42FuncaoUsuario_CodigoTitleFilterData", AV42FuncaoUsuario_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11AY2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV47PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV47PageToGo) ;
         }
      }

      protected void E12AY2( )
      {
         /* Ddo_solicitacoesitens_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoesitens_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_solicitacoesitens_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "SortedStatus", Ddo_solicitacoesitens_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoesitens_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_solicitacoesitens_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "SortedStatus", Ddo_solicitacoesitens_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoesitens_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFSolicitacoesItens_Codigo = (int)(NumberUtil.Val( Ddo_solicitacoesitens_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
            AV36TFSolicitacoesItens_Codigo_To = (int)(NumberUtil.Val( Ddo_solicitacoesitens_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AY2( )
      {
         /* Ddo_solicitacoesitens_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoesitens_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_solicitacoesitens_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SortedStatus", Ddo_solicitacoesitens_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoesitens_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_solicitacoesitens_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SortedStatus", Ddo_solicitacoesitens_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoesitens_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFSolicitacoesItens_descricao = Ddo_solicitacoesitens_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
            AV40TFSolicitacoesItens_descricao_Sel = Ddo_solicitacoesitens_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSolicitacoesItens_descricao_Sel", AV40TFSolicitacoesItens_descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14AY2( )
      {
         /* Ddo_funcaousuario_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaousuario_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaousuario_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "SortedStatus", Ddo_funcaousuario_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaousuario_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "SortedStatus", Ddo_funcaousuario_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFuncaoUsuario_Codigo = (int)(NumberUtil.Val( Ddo_funcaousuario_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
            AV44TFFuncaoUsuario_Codigo_To = (int)(NumberUtil.Val( Ddo_funcaousuario_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28AY2( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV29Update);
         AV52Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("solicitacoesitens.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A447SolicitacoesItens_Codigo);
         AV30Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV30Delete);
         AV53Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("solicitacoesitens.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A447SolicitacoesItens_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      protected void E15AY2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E21AY2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16AY2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22AY2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23AY2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17AY2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24AY2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18AY2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18SolicitacoesItens_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22SolicitacoesItens_Codigo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26SolicitacoesItens_Codigo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV35TFSolicitacoesItens_Codigo, AV36TFSolicitacoesItens_Codigo_To, AV39TFSolicitacoesItens_descricao, AV40TFSolicitacoesItens_descricao_Sel, AV43TFFuncaoUsuario_Codigo, AV44TFFuncaoUsuario_Codigo_To, AV7Solicitacoes_Codigo, AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace, AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace, AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace, AV54Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A447SolicitacoesItens_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25AY2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19AY2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20AY2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("solicitacoesitens.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_solicitacoesitens_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "SortedStatus", Ddo_solicitacoesitens_codigo_Sortedstatus);
         Ddo_solicitacoesitens_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SortedStatus", Ddo_solicitacoesitens_descricao_Sortedstatus);
         Ddo_funcaousuario_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "SortedStatus", Ddo_funcaousuario_codigo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_solicitacoesitens_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "SortedStatus", Ddo_solicitacoesitens_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_solicitacoesitens_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SortedStatus", Ddo_solicitacoesitens_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_funcaousuario_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "SortedStatus", Ddo_funcaousuario_codigo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSolicitacoesitens_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 )
         {
            edtavSolicitacoesitens_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavSolicitacoesitens_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 )
         {
            edtavSolicitacoesitens_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavSolicitacoesitens_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 )
         {
            edtavSolicitacoesitens_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSolicitacoesitens_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoesitens_codigo3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22SolicitacoesItens_Codigo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22SolicitacoesItens_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26SolicitacoesItens_Codigo3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SolicitacoesItens_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFSolicitacoesItens_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
         Ddo_solicitacoesitens_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "FilteredText_set", Ddo_solicitacoesitens_codigo_Filteredtext_set);
         AV36TFSolicitacoesItens_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
         Ddo_solicitacoesitens_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "FilteredTextTo_set", Ddo_solicitacoesitens_codigo_Filteredtextto_set);
         AV39TFSolicitacoesItens_descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
         Ddo_solicitacoesitens_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "FilteredText_set", Ddo_solicitacoesitens_descricao_Filteredtext_set);
         AV40TFSolicitacoesItens_descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSolicitacoesItens_descricao_Sel", AV40TFSolicitacoesItens_descricao_Sel);
         Ddo_solicitacoesitens_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SelectedValue_set", Ddo_solicitacoesitens_descricao_Selectedvalue_set);
         AV43TFFuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
         Ddo_funcaousuario_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "FilteredText_set", Ddo_funcaousuario_codigo_Filteredtext_set);
         AV44TFFuncaoUsuario_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
         Ddo_funcaousuario_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "FilteredTextTo_set", Ddo_funcaousuario_codigo_Filteredtextto_set);
         AV16DynamicFiltersSelector1 = "SOLICITACOESITENS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18SolicitacoesItens_Codigo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18SolicitacoesItens_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV54Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV54Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV54Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSOLICITACOESITENS_CODIGO") == 0 )
            {
               AV35TFSolicitacoesItens_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0)));
               AV36TFSolicitacoesItens_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSolicitacoesItens_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0)));
               if ( ! (0==AV35TFSolicitacoesItens_Codigo) )
               {
                  Ddo_solicitacoesitens_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "FilteredText_set", Ddo_solicitacoesitens_codigo_Filteredtext_set);
               }
               if ( ! (0==AV36TFSolicitacoesItens_Codigo_To) )
               {
                  Ddo_solicitacoesitens_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_codigo_Internalname, "FilteredTextTo_set", Ddo_solicitacoesitens_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSOLICITACOESITENS_DESCRICAO") == 0 )
            {
               AV39TFSolicitacoesItens_descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSolicitacoesItens_descricao", AV39TFSolicitacoesItens_descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSolicitacoesItens_descricao)) )
               {
                  Ddo_solicitacoesitens_descricao_Filteredtext_set = AV39TFSolicitacoesItens_descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "FilteredText_set", Ddo_solicitacoesitens_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSOLICITACOESITENS_DESCRICAO_SEL") == 0 )
            {
               AV40TFSolicitacoesItens_descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSolicitacoesItens_descricao_Sel", AV40TFSolicitacoesItens_descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) )
               {
                  Ddo_solicitacoesitens_descricao_Selectedvalue_set = AV40TFSolicitacoesItens_descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacoesitens_descricao_Internalname, "SelectedValue_set", Ddo_solicitacoesitens_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_CODIGO") == 0 )
            {
               AV43TFFuncaoUsuario_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0)));
               AV44TFFuncaoUsuario_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoUsuario_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0)));
               if ( ! (0==AV43TFFuncaoUsuario_Codigo) )
               {
                  Ddo_funcaousuario_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "FilteredText_set", Ddo_funcaousuario_codigo_Filteredtext_set);
               }
               if ( ! (0==AV44TFFuncaoUsuario_Codigo_To) )
               {
                  Ddo_funcaousuario_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_codigo_Internalname, "FilteredTextTo_set", Ddo_funcaousuario_codigo_Filteredtextto_set);
               }
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18SolicitacoesItens_Codigo1 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18SolicitacoesItens_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22SolicitacoesItens_Codigo2 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22SolicitacoesItens_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26SolicitacoesItens_Codigo3 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SolicitacoesItens_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV54Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV35TFSolicitacoesItens_Codigo) && (0==AV36TFSolicitacoesItens_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSOLICITACOESITENS_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFSolicitacoesItens_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFSolicitacoesItens_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSolicitacoesItens_descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSOLICITACOESITENS_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFSolicitacoesItens_descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSOLICITACOESITENS_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFSolicitacoesItens_descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFFuncaoUsuario_Codigo) && (0==AV44TFFuncaoUsuario_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFFuncaoUsuario_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFFuncaoUsuario_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Solicitacoes_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SOLICITACOES_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV54Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ! (0==AV18SolicitacoesItens_Codigo1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ! (0==AV22SolicitacoesItens_Codigo2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ! (0==AV26SolicitacoesItens_Codigo3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV54Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "SolicitacoesItens";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Solicitacoes_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_AY2( true) ;
         }
         else
         {
            wb_table2_8_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_AY2( true) ;
         }
         else
         {
            wb_table3_80_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AY2e( true) ;
         }
         else
         {
            wb_table1_2_AY2e( false) ;
         }
      }

      protected void wb_table3_80_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoesItens_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoesItens_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoesItens_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoesItens_descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoesItens_descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoesItens_descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Itens_Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoUsuario_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoUsuario_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoUsuario_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A447SolicitacoesItens_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoesItens_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoesItens_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A448SolicitacoesItens_descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoesItens_descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoesItens_descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A449SolicitacoesItens_Arquivo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoUsuario_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoUsuario_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_AY2e( true) ;
         }
         else
         {
            wb_table3_80_AY2e( false) ;
         }
      }

      protected void wb_table2_8_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_AY2( true) ;
         }
         else
         {
            wb_table4_11_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_AY2( true) ;
         }
         else
         {
            wb_table5_21_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AY2e( true) ;
         }
         else
         {
            wb_table2_8_AY2e( false) ;
         }
      }

      protected void wb_table5_21_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_AY2( true) ;
         }
         else
         {
            wb_table6_26_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_AY2e( true) ;
         }
         else
         {
            wb_table5_21_AY2e( false) ;
         }
      }

      protected void wb_table6_26_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_35_AY2( true) ;
         }
         else
         {
            wb_table7_35_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_52_AY2( true) ;
         }
         else
         {
            wb_table8_52_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table8_52_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_69_AY2( true) ;
         }
         else
         {
            wb_table9_69_AY2( false) ;
         }
         return  ;
      }

      protected void wb_table9_69_AY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_AY2e( true) ;
         }
         else
         {
            wb_table6_26_AY2e( false) ;
         }
      }

      protected void wb_table9_69_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSolicitacoesitens_codigo3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26SolicitacoesItens_Codigo3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26SolicitacoesItens_Codigo3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacoesitens_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSolicitacoesitens_codigo3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_69_AY2e( true) ;
         }
         else
         {
            wb_table9_69_AY2e( false) ;
         }
      }

      protected void wb_table8_52_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSolicitacoesitens_codigo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22SolicitacoesItens_Codigo2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22SolicitacoesItens_Codigo2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacoesitens_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSolicitacoesitens_codigo2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_52_AY2e( true) ;
         }
         else
         {
            wb_table8_52_AY2e( false) ;
         }
      }

      protected void wb_table7_35_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSolicitacoesitens_codigo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18SolicitacoesItens_Codigo1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18SolicitacoesItens_Codigo1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacoesitens_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSolicitacoesitens_codigo1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_AY2e( true) ;
         }
         else
         {
            wb_table7_35_AY2e( false) ;
         }
      }

      protected void wb_table4_11_AY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesSolicitacoesItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_AY2e( true) ;
         }
         else
         {
            wb_table4_11_AY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Solicitacoes_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAY2( ) ;
         WSAY2( ) ;
         WEAY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Solicitacoes_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "solicitacoessolicitacoesitenswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Solicitacoes_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0)));
         }
         wcpOAV7Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Solicitacoes_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Solicitacoes_Codigo != wcpOAV7Solicitacoes_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Solicitacoes_Codigo = AV7Solicitacoes_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Solicitacoes_Codigo = cgiGet( sPrefix+"AV7Solicitacoes_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Solicitacoes_Codigo) > 0 )
         {
            AV7Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Solicitacoes_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Solicitacoes_Codigo), 6, 0)));
         }
         else
         {
            AV7Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Solicitacoes_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Solicitacoes_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Solicitacoes_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Solicitacoes_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Solicitacoes_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Solicitacoes_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282314956");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("solicitacoessolicitacoesitenswc.js", "?20204282314957");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_83_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_83_idx;
         edtSolicitacoesItens_Codigo_Internalname = sPrefix+"SOLICITACOESITENS_CODIGO_"+sGXsfl_83_idx;
         edtSolicitacoesItens_descricao_Internalname = sPrefix+"SOLICITACOESITENS_DESCRICAO_"+sGXsfl_83_idx;
         edtSolicitacoesItens_Arquivo_Internalname = sPrefix+"SOLICITACOESITENS_ARQUIVO_"+sGXsfl_83_idx;
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_83_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_83_fel_idx;
         edtSolicitacoesItens_Codigo_Internalname = sPrefix+"SOLICITACOESITENS_CODIGO_"+sGXsfl_83_fel_idx;
         edtSolicitacoesItens_descricao_Internalname = sPrefix+"SOLICITACOESITENS_DESCRICAO_"+sGXsfl_83_fel_idx;
         edtSolicitacoesItens_Arquivo_Internalname = sPrefix+"SOLICITACOESITENS_ARQUIVO_"+sGXsfl_83_fel_idx;
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WBAY0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV52Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV52Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV53Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV53Delete_GXI : context.PathToRelativeUrl( AV30Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoesItens_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A447SolicitacoesItens_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A447SolicitacoesItens_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoesItens_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoesItens_descricao_Internalname,(String)A448SolicitacoesItens_descricao,(String)A448SolicitacoesItens_descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoesItens_descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)83,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtSolicitacoesItens_Arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "Filetype", edtSolicitacoesItens_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) )
            {
               gxblobfileaux.Source = A449SolicitacoesItens_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtSolicitacoesItens_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtSolicitacoesItens_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A449SolicitacoesItens_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n449SolicitacoesItens_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
                  edtSolicitacoesItens_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "Filetype", edtSolicitacoesItens_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoesItens_Arquivo_Internalname,StringUtil.RTrim( A449SolicitacoesItens_Arquivo),context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Filetype)) ? A449SolicitacoesItens_Arquivo : edtSolicitacoesItens_Arquivo_Filetype)) : edtSolicitacoesItens_Arquivo_Contenttype),(bool)false,(String)"",(String)edtSolicitacoesItens_Arquivo_Parameters,(short)0,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtSolicitacoesItens_Arquivo_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACOESITENS_DESCRICAO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, A448SolicitacoesItens_descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOUSUARIO_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavSolicitacoesitens_codigo1_Internalname = sPrefix+"vSOLICITACOESITENS_CODIGO1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavSolicitacoesitens_codigo2_Internalname = sPrefix+"vSOLICITACOESITENS_CODIGO2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavSolicitacoesitens_codigo3_Internalname = sPrefix+"vSOLICITACOESITENS_CODIGO3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtSolicitacoesItens_Codigo_Internalname = sPrefix+"SOLICITACOESITENS_CODIGO";
         edtSolicitacoesItens_descricao_Internalname = sPrefix+"SOLICITACOESITENS_DESCRICAO";
         edtSolicitacoesItens_Arquivo_Internalname = sPrefix+"SOLICITACOESITENS_ARQUIVO";
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtSolicitacoes_Codigo_Internalname = sPrefix+"SOLICITACOES_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavTfsolicitacoesitens_codigo_Internalname = sPrefix+"vTFSOLICITACOESITENS_CODIGO";
         edtavTfsolicitacoesitens_codigo_to_Internalname = sPrefix+"vTFSOLICITACOESITENS_CODIGO_TO";
         edtavTfsolicitacoesitens_descricao_Internalname = sPrefix+"vTFSOLICITACOESITENS_DESCRICAO";
         edtavTfsolicitacoesitens_descricao_sel_Internalname = sPrefix+"vTFSOLICITACOESITENS_DESCRICAO_SEL";
         edtavTffuncaousuario_codigo_Internalname = sPrefix+"vTFFUNCAOUSUARIO_CODIGO";
         edtavTffuncaousuario_codigo_to_Internalname = sPrefix+"vTFFUNCAOUSUARIO_CODIGO_TO";
         Ddo_solicitacoesitens_codigo_Internalname = sPrefix+"DDO_SOLICITACOESITENS_CODIGO";
         edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoesitens_descricao_Internalname = sPrefix+"DDO_SOLICITACOESITENS_DESCRICAO";
         edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_funcaousuario_codigo_Internalname = sPrefix+"DDO_FUNCAOUSUARIO_CODIGO";
         edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtSolicitacoesItens_Arquivo_Jsonclick = "";
         edtSolicitacoesItens_Arquivo_Parameters = "";
         edtSolicitacoesItens_Arquivo_Contenttype = "";
         edtSolicitacoesItens_Arquivo_Filetype = "";
         edtSolicitacoesItens_descricao_Jsonclick = "";
         edtSolicitacoesItens_Codigo_Jsonclick = "";
         edtavSolicitacoesitens_codigo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavSolicitacoesitens_codigo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavSolicitacoesitens_codigo3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtFuncaoUsuario_Codigo_Titleformat = 0;
         edtSolicitacoesItens_descricao_Titleformat = 0;
         edtSolicitacoesItens_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavSolicitacoesitens_codigo3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavSolicitacoesitens_codigo2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavSolicitacoesitens_codigo1_Visible = 1;
         edtFuncaoUsuario_Codigo_Title = "Fun��o de Usu�rio";
         edtSolicitacoesItens_descricao_Title = "Itens_descricao";
         edtSolicitacoesItens_Codigo_Title = "Itens_Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaousuario_codigo_to_Jsonclick = "";
         edtavTffuncaousuario_codigo_to_Visible = 1;
         edtavTffuncaousuario_codigo_Jsonclick = "";
         edtavTffuncaousuario_codigo_Visible = 1;
         edtavTfsolicitacoesitens_descricao_sel_Visible = 1;
         edtavTfsolicitacoesitens_descricao_Visible = 1;
         edtavTfsolicitacoesitens_codigo_to_Jsonclick = "";
         edtavTfsolicitacoesitens_codigo_to_Visible = 1;
         edtavTfsolicitacoesitens_codigo_Jsonclick = "";
         edtavTfsolicitacoesitens_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtSolicitacoes_Codigo_Jsonclick = "";
         edtSolicitacoes_Codigo_Visible = 1;
         Ddo_funcaousuario_codigo_Searchbuttontext = "Pesquisar";
         Ddo_funcaousuario_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaousuario_codigo_Rangefilterto = "At�";
         Ddo_funcaousuario_codigo_Rangefilterfrom = "Desde";
         Ddo_funcaousuario_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaousuario_codigo_Loadingdata = "Carregando dados...";
         Ddo_funcaousuario_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaousuario_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaousuario_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_funcaousuario_codigo_Datalistfixedvalues = "";
         Ddo_funcaousuario_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaousuario_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaousuario_codigo_Filtertype = "Numeric";
         Ddo_funcaousuario_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaousuario_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_codigo_Titlecontrolidtoreplace = "";
         Ddo_funcaousuario_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaousuario_codigo_Cls = "ColumnSettings";
         Ddo_funcaousuario_codigo_Tooltip = "Op��es";
         Ddo_funcaousuario_codigo_Caption = "";
         Ddo_solicitacoesitens_descricao_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoesitens_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoesitens_descricao_Rangefilterto = "At�";
         Ddo_solicitacoesitens_descricao_Rangefilterfrom = "Desde";
         Ddo_solicitacoesitens_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoesitens_descricao_Loadingdata = "Carregando dados...";
         Ddo_solicitacoesitens_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoesitens_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoesitens_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoesitens_descricao_Datalistproc = "GetSolicitacoesSolicitacoesItensWCFilterData";
         Ddo_solicitacoesitens_descricao_Datalistfixedvalues = "";
         Ddo_solicitacoesitens_descricao_Datalisttype = "Dynamic";
         Ddo_solicitacoesitens_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_solicitacoesitens_descricao_Filtertype = "Character";
         Ddo_solicitacoesitens_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace = "";
         Ddo_solicitacoesitens_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoesitens_descricao_Cls = "ColumnSettings";
         Ddo_solicitacoesitens_descricao_Tooltip = "Op��es";
         Ddo_solicitacoesitens_descricao_Caption = "";
         Ddo_solicitacoesitens_codigo_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoesitens_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoesitens_codigo_Rangefilterto = "At�";
         Ddo_solicitacoesitens_codigo_Rangefilterfrom = "Desde";
         Ddo_solicitacoesitens_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoesitens_codigo_Loadingdata = "Carregando dados...";
         Ddo_solicitacoesitens_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoesitens_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoesitens_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoesitens_codigo_Datalistfixedvalues = "";
         Ddo_solicitacoesitens_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoesitens_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_codigo_Filtertype = "Numeric";
         Ddo_solicitacoesitens_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace = "";
         Ddo_solicitacoesitens_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoesitens_codigo_Cls = "ColumnSettings";
         Ddo_solicitacoesitens_codigo_Tooltip = "Op��es";
         Ddo_solicitacoesitens_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV34SolicitacoesItens_CodigoTitleFilterData',fld:'vSOLICITACOESITENS_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38SolicitacoesItens_descricaoTitleFilterData',fld:'vSOLICITACOESITENS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42FuncaoUsuario_CodigoTitleFilterData',fld:'vFUNCAOUSUARIO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'edtSolicitacoesItens_Codigo_Titleformat',ctrl:'SOLICITACOESITENS_CODIGO',prop:'Titleformat'},{av:'edtSolicitacoesItens_Codigo_Title',ctrl:'SOLICITACOESITENS_CODIGO',prop:'Title'},{av:'edtSolicitacoesItens_descricao_Titleformat',ctrl:'SOLICITACOESITENS_DESCRICAO',prop:'Titleformat'},{av:'edtSolicitacoesItens_descricao_Title',ctrl:'SOLICITACOESITENS_DESCRICAO',prop:'Title'},{av:'edtFuncaoUsuario_Codigo_Titleformat',ctrl:'FUNCAOUSUARIO_CODIGO',prop:'Titleformat'},{av:'edtFuncaoUsuario_Codigo_Title',ctrl:'FUNCAOUSUARIO_CODIGO',prop:'Title'},{av:'AV48GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV49GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SOLICITACOESITENS_CODIGO.ONOPTIONCLICKED","{handler:'E12AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_solicitacoesitens_codigo_Activeeventkey',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoesitens_codigo_Filteredtext_get',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'FilteredText_get'},{av:'Ddo_solicitacoesitens_codigo_Filteredtextto_get',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoesitens_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'SortedStatus'},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoesitens_descricao_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaousuario_codigo_Sortedstatus',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOESITENS_DESCRICAO.ONOPTIONCLICKED","{handler:'E13AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_solicitacoesitens_descricao_Activeeventkey',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoesitens_descricao_Filteredtext_get',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_solicitacoesitens_descricao_Selectedvalue_get',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoesitens_descricao_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'SortedStatus'},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoesitens_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaousuario_codigo_Sortedstatus',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOUSUARIO_CODIGO.ONOPTIONCLICKED","{handler:'E14AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaousuario_codigo_Activeeventkey',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_funcaousuario_codigo_Filteredtext_get',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_funcaousuario_codigo_Filteredtextto_get',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaousuario_codigo_Sortedstatus',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'SortedStatus'},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoesitens_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoesitens_descricao_Sortedstatus',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28AY2',iparms:[{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV30Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21AY2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSolicitacoesitens_codigo2_Visible',ctrl:'vSOLICITACOESITENS_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSolicitacoesitens_codigo3_Visible',ctrl:'vSOLICITACOESITENS_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSolicitacoesitens_codigo1_Visible',ctrl:'vSOLICITACOESITENS_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22AY2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSolicitacoesitens_codigo1_Visible',ctrl:'vSOLICITACOESITENS_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23AY2',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSolicitacoesitens_codigo2_Visible',ctrl:'vSOLICITACOESITENS_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSolicitacoesitens_codigo3_Visible',ctrl:'vSOLICITACOESITENS_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSolicitacoesitens_codigo1_Visible',ctrl:'vSOLICITACOESITENS_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24AY2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavSolicitacoesitens_codigo2_Visible',ctrl:'vSOLICITACOESITENS_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSolicitacoesitens_codigo2_Visible',ctrl:'vSOLICITACOESITENS_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSolicitacoesitens_codigo3_Visible',ctrl:'vSOLICITACOESITENS_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSolicitacoesitens_codigo1_Visible',ctrl:'vSOLICITACOESITENS_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25AY2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavSolicitacoesitens_codigo3_Visible',ctrl:'vSOLICITACOESITENS_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19AY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV7Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace',fld:'vDDO_SOLICITACOESITENS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV35TFSolicitacoesItens_Codigo',fld:'vTFSOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoesitens_codigo_Filteredtext_set',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'FilteredText_set'},{av:'AV36TFSolicitacoesItens_Codigo_To',fld:'vTFSOLICITACOESITENS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoesitens_codigo_Filteredtextto_set',ctrl:'DDO_SOLICITACOESITENS_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFSolicitacoesItens_descricao',fld:'vTFSOLICITACOESITENS_DESCRICAO',pic:'',nv:''},{av:'Ddo_solicitacoesitens_descricao_Filteredtext_set',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'FilteredText_set'},{av:'AV40TFSolicitacoesItens_descricao_Sel',fld:'vTFSOLICITACOESITENS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoesitens_descricao_Selectedvalue_set',ctrl:'DDO_SOLICITACOESITENS_DESCRICAO',prop:'SelectedValue_set'},{av:'AV43TFFuncaoUsuario_Codigo',fld:'vTFFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaousuario_codigo_Filteredtext_set',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'FilteredText_set'},{av:'AV44TFFuncaoUsuario_Codigo_To',fld:'vTFFUNCAOUSUARIO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaousuario_codigo_Filteredtextto_set',ctrl:'DDO_FUNCAOUSUARIO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18SolicitacoesItens_Codigo1',fld:'vSOLICITACOESITENS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavSolicitacoesitens_codigo1_Visible',ctrl:'vSOLICITACOESITENS_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22SolicitacoesItens_Codigo2',fld:'vSOLICITACOESITENS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26SolicitacoesItens_Codigo3',fld:'vSOLICITACOESITENS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSolicitacoesitens_codigo2_Visible',ctrl:'vSOLICITACOESITENS_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSolicitacoesitens_codigo3_Visible',ctrl:'vSOLICITACOESITENS_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20AY2',iparms:[{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A447SolicitacoesItens_Codigo',fld:'SOLICITACOESITENS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_solicitacoesitens_codigo_Activeeventkey = "";
         Ddo_solicitacoesitens_codigo_Filteredtext_get = "";
         Ddo_solicitacoesitens_codigo_Filteredtextto_get = "";
         Ddo_solicitacoesitens_descricao_Activeeventkey = "";
         Ddo_solicitacoesitens_descricao_Filteredtext_get = "";
         Ddo_solicitacoesitens_descricao_Selectedvalue_get = "";
         Ddo_funcaousuario_codigo_Activeeventkey = "";
         Ddo_funcaousuario_codigo_Filteredtext_get = "";
         Ddo_funcaousuario_codigo_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV39TFSolicitacoesItens_descricao = "";
         AV40TFSolicitacoesItens_descricao_Sel = "";
         AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace = "";
         AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace = "";
         AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace = "";
         AV54Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV46DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34SolicitacoesItens_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38SolicitacoesItens_descricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoUsuario_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_solicitacoesitens_codigo_Filteredtext_set = "";
         Ddo_solicitacoesitens_codigo_Filteredtextto_set = "";
         Ddo_solicitacoesitens_codigo_Sortedstatus = "";
         Ddo_solicitacoesitens_descricao_Filteredtext_set = "";
         Ddo_solicitacoesitens_descricao_Selectedvalue_set = "";
         Ddo_solicitacoesitens_descricao_Sortedstatus = "";
         Ddo_funcaousuario_codigo_Filteredtext_set = "";
         Ddo_funcaousuario_codigo_Filteredtextto_set = "";
         Ddo_funcaousuario_codigo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV52Update_GXI = "";
         AV30Delete = "";
         AV53Delete_GXI = "";
         A448SolicitacoesItens_descricao = "";
         A449SolicitacoesItens_Arquivo = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV39TFSolicitacoesItens_descricao = "";
         H00AY2_A439Solicitacoes_Codigo = new int[1] ;
         H00AY2_A161FuncaoUsuario_Codigo = new int[1] ;
         H00AY2_A448SolicitacoesItens_descricao = new String[] {""} ;
         H00AY2_n448SolicitacoesItens_descricao = new bool[] {false} ;
         H00AY2_A447SolicitacoesItens_Codigo = new int[1] ;
         H00AY2_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         H00AY2_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         H00AY3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Solicitacoes_Codigo = "";
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoessolicitacoesitenswc__default(),
            new Object[][] {
                new Object[] {
               H00AY2_A439Solicitacoes_Codigo, H00AY2_A161FuncaoUsuario_Codigo, H00AY2_A448SolicitacoesItens_descricao, H00AY2_n448SolicitacoesItens_descricao, H00AY2_A447SolicitacoesItens_Codigo, H00AY2_A449SolicitacoesItens_Arquivo, H00AY2_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               H00AY3_AGRID_nRecordCount
               }
            }
         );
         AV54Pgmname = "SolicitacoesSolicitacoesItensWC";
         /* GeneXus formulas. */
         AV54Pgmname = "SolicitacoesSolicitacoesItensWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSolicitacoesItens_Codigo_Titleformat ;
      private short edtSolicitacoesItens_descricao_Titleformat ;
      private short edtFuncaoUsuario_Codigo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Solicitacoes_Codigo ;
      private int wcpOAV7Solicitacoes_Codigo ;
      private int subGrid_Rows ;
      private int AV18SolicitacoesItens_Codigo1 ;
      private int AV22SolicitacoesItens_Codigo2 ;
      private int AV26SolicitacoesItens_Codigo3 ;
      private int AV35TFSolicitacoesItens_Codigo ;
      private int AV36TFSolicitacoesItens_Codigo_To ;
      private int AV43TFFuncaoUsuario_Codigo ;
      private int AV44TFFuncaoUsuario_Codigo_To ;
      private int A447SolicitacoesItens_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_solicitacoesitens_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoesitens_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_funcaousuario_codigo_Datalistupdateminimumcharacters ;
      private int A439Solicitacoes_Codigo ;
      private int edtSolicitacoes_Codigo_Visible ;
      private int edtavTfsolicitacoesitens_codigo_Visible ;
      private int edtavTfsolicitacoesitens_codigo_to_Visible ;
      private int edtavTfsolicitacoesitens_descricao_Visible ;
      private int edtavTfsolicitacoesitens_descricao_sel_Visible ;
      private int edtavTffuncaousuario_codigo_Visible ;
      private int edtavTffuncaousuario_codigo_to_Visible ;
      private int edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Visible ;
      private int A161FuncaoUsuario_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV47PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavSolicitacoesitens_codigo1_Visible ;
      private int edtavSolicitacoesitens_codigo2_Visible ;
      private int edtavSolicitacoesitens_codigo3_Visible ;
      private int AV55GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV48GridCurrentPage ;
      private long AV49GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_solicitacoesitens_codigo_Activeeventkey ;
      private String Ddo_solicitacoesitens_codigo_Filteredtext_get ;
      private String Ddo_solicitacoesitens_codigo_Filteredtextto_get ;
      private String Ddo_solicitacoesitens_descricao_Activeeventkey ;
      private String Ddo_solicitacoesitens_descricao_Filteredtext_get ;
      private String Ddo_solicitacoesitens_descricao_Selectedvalue_get ;
      private String Ddo_funcaousuario_codigo_Activeeventkey ;
      private String Ddo_funcaousuario_codigo_Filteredtext_get ;
      private String Ddo_funcaousuario_codigo_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_83_idx="0001" ;
      private String AV54Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_solicitacoesitens_codigo_Caption ;
      private String Ddo_solicitacoesitens_codigo_Tooltip ;
      private String Ddo_solicitacoesitens_codigo_Cls ;
      private String Ddo_solicitacoesitens_codigo_Filteredtext_set ;
      private String Ddo_solicitacoesitens_codigo_Filteredtextto_set ;
      private String Ddo_solicitacoesitens_codigo_Dropdownoptionstype ;
      private String Ddo_solicitacoesitens_codigo_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoesitens_codigo_Sortedstatus ;
      private String Ddo_solicitacoesitens_codigo_Filtertype ;
      private String Ddo_solicitacoesitens_codigo_Datalistfixedvalues ;
      private String Ddo_solicitacoesitens_codigo_Sortasc ;
      private String Ddo_solicitacoesitens_codigo_Sortdsc ;
      private String Ddo_solicitacoesitens_codigo_Loadingdata ;
      private String Ddo_solicitacoesitens_codigo_Cleanfilter ;
      private String Ddo_solicitacoesitens_codigo_Rangefilterfrom ;
      private String Ddo_solicitacoesitens_codigo_Rangefilterto ;
      private String Ddo_solicitacoesitens_codigo_Noresultsfound ;
      private String Ddo_solicitacoesitens_codigo_Searchbuttontext ;
      private String Ddo_solicitacoesitens_descricao_Caption ;
      private String Ddo_solicitacoesitens_descricao_Tooltip ;
      private String Ddo_solicitacoesitens_descricao_Cls ;
      private String Ddo_solicitacoesitens_descricao_Filteredtext_set ;
      private String Ddo_solicitacoesitens_descricao_Selectedvalue_set ;
      private String Ddo_solicitacoesitens_descricao_Dropdownoptionstype ;
      private String Ddo_solicitacoesitens_descricao_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoesitens_descricao_Sortedstatus ;
      private String Ddo_solicitacoesitens_descricao_Filtertype ;
      private String Ddo_solicitacoesitens_descricao_Datalisttype ;
      private String Ddo_solicitacoesitens_descricao_Datalistfixedvalues ;
      private String Ddo_solicitacoesitens_descricao_Datalistproc ;
      private String Ddo_solicitacoesitens_descricao_Sortasc ;
      private String Ddo_solicitacoesitens_descricao_Sortdsc ;
      private String Ddo_solicitacoesitens_descricao_Loadingdata ;
      private String Ddo_solicitacoesitens_descricao_Cleanfilter ;
      private String Ddo_solicitacoesitens_descricao_Rangefilterfrom ;
      private String Ddo_solicitacoesitens_descricao_Rangefilterto ;
      private String Ddo_solicitacoesitens_descricao_Noresultsfound ;
      private String Ddo_solicitacoesitens_descricao_Searchbuttontext ;
      private String Ddo_funcaousuario_codigo_Caption ;
      private String Ddo_funcaousuario_codigo_Tooltip ;
      private String Ddo_funcaousuario_codigo_Cls ;
      private String Ddo_funcaousuario_codigo_Filteredtext_set ;
      private String Ddo_funcaousuario_codigo_Filteredtextto_set ;
      private String Ddo_funcaousuario_codigo_Dropdownoptionstype ;
      private String Ddo_funcaousuario_codigo_Titlecontrolidtoreplace ;
      private String Ddo_funcaousuario_codigo_Sortedstatus ;
      private String Ddo_funcaousuario_codigo_Filtertype ;
      private String Ddo_funcaousuario_codigo_Datalistfixedvalues ;
      private String Ddo_funcaousuario_codigo_Sortasc ;
      private String Ddo_funcaousuario_codigo_Sortdsc ;
      private String Ddo_funcaousuario_codigo_Loadingdata ;
      private String Ddo_funcaousuario_codigo_Cleanfilter ;
      private String Ddo_funcaousuario_codigo_Rangefilterfrom ;
      private String Ddo_funcaousuario_codigo_Rangefilterto ;
      private String Ddo_funcaousuario_codigo_Noresultsfound ;
      private String Ddo_funcaousuario_codigo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtSolicitacoes_Codigo_Internalname ;
      private String edtSolicitacoes_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfsolicitacoesitens_codigo_Internalname ;
      private String edtavTfsolicitacoesitens_codigo_Jsonclick ;
      private String edtavTfsolicitacoesitens_codigo_to_Internalname ;
      private String edtavTfsolicitacoesitens_codigo_to_Jsonclick ;
      private String edtavTfsolicitacoesitens_descricao_Internalname ;
      private String edtavTfsolicitacoesitens_descricao_sel_Internalname ;
      private String edtavTffuncaousuario_codigo_Internalname ;
      private String edtavTffuncaousuario_codigo_Jsonclick ;
      private String edtavTffuncaousuario_codigo_to_Internalname ;
      private String edtavTffuncaousuario_codigo_to_Jsonclick ;
      private String edtavDdo_solicitacoesitens_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoesitens_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaousuario_codigotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtSolicitacoesItens_Codigo_Internalname ;
      private String edtSolicitacoesItens_descricao_Internalname ;
      private String edtSolicitacoesItens_Arquivo_Internalname ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavSolicitacoesitens_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavSolicitacoesitens_codigo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavSolicitacoesitens_codigo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_solicitacoesitens_codigo_Internalname ;
      private String Ddo_solicitacoesitens_descricao_Internalname ;
      private String Ddo_funcaousuario_codigo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSolicitacoesItens_Codigo_Title ;
      private String edtSolicitacoesItens_descricao_Title ;
      private String edtFuncaoUsuario_Codigo_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavSolicitacoesitens_codigo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavSolicitacoesitens_codigo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavSolicitacoesitens_codigo1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Solicitacoes_Codigo ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSolicitacoesItens_Codigo_Jsonclick ;
      private String edtSolicitacoesItens_descricao_Jsonclick ;
      private String edtSolicitacoesItens_Arquivo_Filetype ;
      private String edtSolicitacoesItens_Arquivo_Contenttype ;
      private String edtSolicitacoesItens_Arquivo_Parameters ;
      private String edtSolicitacoesItens_Arquivo_Jsonclick ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_solicitacoesitens_codigo_Includesortasc ;
      private bool Ddo_solicitacoesitens_codigo_Includesortdsc ;
      private bool Ddo_solicitacoesitens_codigo_Includefilter ;
      private bool Ddo_solicitacoesitens_codigo_Filterisrange ;
      private bool Ddo_solicitacoesitens_codigo_Includedatalist ;
      private bool Ddo_solicitacoesitens_descricao_Includesortasc ;
      private bool Ddo_solicitacoesitens_descricao_Includesortdsc ;
      private bool Ddo_solicitacoesitens_descricao_Includefilter ;
      private bool Ddo_solicitacoesitens_descricao_Filterisrange ;
      private bool Ddo_solicitacoesitens_descricao_Includedatalist ;
      private bool Ddo_funcaousuario_codigo_Includesortasc ;
      private bool Ddo_funcaousuario_codigo_Includesortdsc ;
      private bool Ddo_funcaousuario_codigo_Includefilter ;
      private bool Ddo_funcaousuario_codigo_Filterisrange ;
      private bool Ddo_funcaousuario_codigo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n448SolicitacoesItens_descricao ;
      private bool n449SolicitacoesItens_Arquivo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private bool AV30Delete_IsBlob ;
      private String A448SolicitacoesItens_descricao ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV39TFSolicitacoesItens_descricao ;
      private String AV40TFSolicitacoesItens_descricao_Sel ;
      private String AV37ddo_SolicitacoesItens_CodigoTitleControlIdToReplace ;
      private String AV41ddo_SolicitacoesItens_descricaoTitleControlIdToReplace ;
      private String AV45ddo_FuncaoUsuario_CodigoTitleControlIdToReplace ;
      private String AV52Update_GXI ;
      private String AV53Delete_GXI ;
      private String lV39TFSolicitacoesItens_descricao ;
      private String AV29Update ;
      private String AV30Delete ;
      private String A449SolicitacoesItens_Arquivo ;
      private IGxSession AV31Session ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00AY2_A439Solicitacoes_Codigo ;
      private int[] H00AY2_A161FuncaoUsuario_Codigo ;
      private String[] H00AY2_A448SolicitacoesItens_descricao ;
      private bool[] H00AY2_n448SolicitacoesItens_descricao ;
      private int[] H00AY2_A447SolicitacoesItens_Codigo ;
      private String[] H00AY2_A449SolicitacoesItens_Arquivo ;
      private bool[] H00AY2_n449SolicitacoesItens_Arquivo ;
      private long[] H00AY3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34SolicitacoesItens_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38SolicitacoesItens_descricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42FuncaoUsuario_CodigoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV46DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class solicitacoessolicitacoesitenswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AY2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV18SolicitacoesItens_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             int AV22SolicitacoesItens_Codigo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             int AV26SolicitacoesItens_Codigo3 ,
                                             int AV35TFSolicitacoesItens_Codigo ,
                                             int AV36TFSolicitacoesItens_Codigo_To ,
                                             String AV40TFSolicitacoesItens_descricao_Sel ,
                                             String AV39TFSolicitacoesItens_descricao ,
                                             int AV43TFFuncaoUsuario_Codigo ,
                                             int AV44TFFuncaoUsuario_Codigo_To ,
                                             int A447SolicitacoesItens_Codigo ,
                                             String A448SolicitacoesItens_descricao ,
                                             int A161FuncaoUsuario_Codigo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A439Solicitacoes_Codigo ,
                                             int AV7Solicitacoes_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_descricao], [SolicitacoesItens_Codigo], [SolicitacoesItens_Arquivo]";
         sFromString = " FROM [SolicitacoesItens] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Solicitacoes_Codigo] = @AV7Solicitacoes_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV35TFSolicitacoesItens_Codigo) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] >= @AV35TFSolicitacoesItens_Codigo)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV36TFSolicitacoesItens_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] <= @AV36TFSolicitacoesItens_Codigo_To)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSolicitacoesItens_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_descricao] like @lV39TFSolicitacoesItens_descricao)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_descricao] = @AV40TFSolicitacoesItens_descricao_Sel)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV43TFFuncaoUsuario_Codigo) )
         {
            sWhereString = sWhereString + " and ([FuncaoUsuario_Codigo] >= @AV43TFFuncaoUsuario_Codigo)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV44TFFuncaoUsuario_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([FuncaoUsuario_Codigo] <= @AV44TFFuncaoUsuario_Codigo_To)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo], [SolicitacoesItens_Codigo]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo] DESC, [SolicitacoesItens_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo], [SolicitacoesItens_descricao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo] DESC, [SolicitacoesItens_descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo], [FuncaoUsuario_Codigo]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo] DESC, [FuncaoUsuario_Codigo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacoesItens_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AY3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV18SolicitacoesItens_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             int AV22SolicitacoesItens_Codigo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             int AV26SolicitacoesItens_Codigo3 ,
                                             int AV35TFSolicitacoesItens_Codigo ,
                                             int AV36TFSolicitacoesItens_Codigo_To ,
                                             String AV40TFSolicitacoesItens_descricao_Sel ,
                                             String AV39TFSolicitacoesItens_descricao ,
                                             int AV43TFFuncaoUsuario_Codigo ,
                                             int AV44TFFuncaoUsuario_Codigo_To ,
                                             int A447SolicitacoesItens_Codigo ,
                                             String A448SolicitacoesItens_descricao ,
                                             int A161FuncaoUsuario_Codigo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A439Solicitacoes_Codigo ,
                                             int AV7Solicitacoes_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [SolicitacoesItens] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Solicitacoes_Codigo] = @AV7Solicitacoes_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (0==AV18SolicitacoesItens_Codigo1) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV18SolicitacoesItens_Codigo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV22SolicitacoesItens_Codigo2) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV22SolicitacoesItens_Codigo2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] < @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] = @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SOLICITACOESITENS_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! (0==AV26SolicitacoesItens_Codigo3) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] > @AV26SolicitacoesItens_Codigo3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV35TFSolicitacoesItens_Codigo) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] >= @AV35TFSolicitacoesItens_Codigo)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV36TFSolicitacoesItens_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_Codigo] <= @AV36TFSolicitacoesItens_Codigo_To)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSolicitacoesItens_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_descricao] like @lV39TFSolicitacoesItens_descricao)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSolicitacoesItens_descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([SolicitacoesItens_descricao] = @AV40TFSolicitacoesItens_descricao_Sel)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV43TFFuncaoUsuario_Codigo) )
         {
            sWhereString = sWhereString + " and ([FuncaoUsuario_Codigo] >= @AV43TFFuncaoUsuario_Codigo)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV44TFFuncaoUsuario_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([FuncaoUsuario_Codigo] <= @AV44TFFuncaoUsuario_Codigo_To)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AY2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_H00AY3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AY2 ;
          prmH00AY2 = new Object[] {
          new Object[] {"@AV7Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSolicitacoesItens_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFSolicitacoesItens_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFSolicitacoesItens_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV40TFSolicitacoesItens_descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43TFFuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFFuncaoUsuario_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AY3 ;
          prmH00AY3 = new Object[] {
          new Object[] {"@AV7Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18SolicitacoesItens_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22SolicitacoesItens_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26SolicitacoesItens_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSolicitacoesItens_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFSolicitacoesItens_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFSolicitacoesItens_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV40TFSolicitacoesItens_descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43TFFuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFFuncaoUsuario_Codigo_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AY2,11,0,true,false )
             ,new CursorDef("H00AY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AY3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
       }
    }

 }

}
