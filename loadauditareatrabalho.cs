/*
               File: LoadAuditAreaTrabalho
        Description: Load Audit Area Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:27.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditareatrabalho : GXProcedure
   {
      public loadauditareatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditareatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_AreaTrabalho_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_AreaTrabalho_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditareatrabalho objloadauditareatrabalho;
         objloadauditareatrabalho = new loadauditareatrabalho();
         objloadauditareatrabalho.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditareatrabalho.AV10AuditingObject = aP1_AuditingObject;
         objloadauditareatrabalho.AV16AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         objloadauditareatrabalho.AV14ActualMode = aP3_ActualMode;
         objloadauditareatrabalho.context.SetSubmitInitialConfig(context);
         objloadauditareatrabalho.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditareatrabalho);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditareatrabalho)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A63 */
         pr_default.execute(0, new Object[] {AV16AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A6AreaTrabalho_Descricao = P00A63_A6AreaTrabalho_Descricao[0];
            A1216AreaTrabalho_OrganizacaoCod = P00A63_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00A63_n1216AreaTrabalho_OrganizacaoCod[0];
            A1214Organizacao_Nome = P00A63_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00A63_n1214Organizacao_Nome[0];
            A29Contratante_Codigo = P00A63_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00A63_n29Contratante_Codigo[0];
            A335Contratante_PessoaCod = P00A63_A335Contratante_PessoaCod[0];
            A23Estado_UF = P00A63_A23Estado_UF[0];
            A24Estado_Nome = P00A63_A24Estado_Nome[0];
            A25Municipio_Codigo = P00A63_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A63_n25Municipio_Codigo[0];
            A26Municipio_Nome = P00A63_A26Municipio_Nome[0];
            A33Contratante_Fax = P00A63_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A63_n33Contratante_Fax[0];
            A32Contratante_Ramal = P00A63_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A63_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00A63_A31Contratante_Telefone[0];
            A14Contratante_Email = P00A63_A14Contratante_Email[0];
            n14Contratante_Email = P00A63_n14Contratante_Email[0];
            A13Contratante_WebSite = P00A63_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A63_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = P00A63_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A63_n12Contratante_CNPJ[0];
            A11Contratante_IE = P00A63_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = P00A63_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00A63_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A63_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = P00A63_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A63_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A63_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A63_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A63_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A63_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A63_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A63_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A63_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A63_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A63_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A63_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A63_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A63_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = P00A63_A642AreaTrabalho_CalculoPFinal[0];
            A830AreaTrabalho_ServicoPadrao = P00A63_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00A63_n830AreaTrabalho_ServicoPadrao[0];
            A834AreaTrabalho_ValidaOSFM = P00A63_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00A63_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = P00A63_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A63_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = P00A63_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = P00A63_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = P00A63_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = P00A63_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = P00A63_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = P00A63_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = P00A63_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = P00A63_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00A63_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = P00A63_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = P00A63_n2080AreaTrabalho_SelUsrPrestadora[0];
            A5AreaTrabalho_Codigo = P00A63_A5AreaTrabalho_Codigo[0];
            A272AreaTrabalho_ContagensQtdGeral = P00A63_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = P00A63_n272AreaTrabalho_ContagensQtdGeral[0];
            A1214Organizacao_Nome = P00A63_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00A63_n1214Organizacao_Nome[0];
            A335Contratante_PessoaCod = P00A63_A335Contratante_PessoaCod[0];
            A25Municipio_Codigo = P00A63_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A63_n25Municipio_Codigo[0];
            A33Contratante_Fax = P00A63_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A63_n33Contratante_Fax[0];
            A32Contratante_Ramal = P00A63_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A63_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00A63_A31Contratante_Telefone[0];
            A14Contratante_Email = P00A63_A14Contratante_Email[0];
            n14Contratante_Email = P00A63_n14Contratante_Email[0];
            A13Contratante_WebSite = P00A63_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A63_n13Contratante_WebSite[0];
            A11Contratante_IE = P00A63_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = P00A63_A10Contratante_NomeFantasia[0];
            A547Contratante_EmailSdaHost = P00A63_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A63_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A63_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A63_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A63_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A63_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A63_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A63_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A63_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A63_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A63_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A63_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A63_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A63_n1048Contratante_EmailSdaSec[0];
            A12Contratante_CNPJ = P00A63_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A63_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A63_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A63_n9Contratante_RazaoSocial[0];
            A23Estado_UF = P00A63_A23Estado_UF[0];
            A26Municipio_Nome = P00A63_A26Municipio_Nome[0];
            A24Estado_Nome = P00A63_A24Estado_Nome[0];
            A272AreaTrabalho_ContagensQtdGeral = P00A63_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = P00A63_n272AreaTrabalho_ContagensQtdGeral[0];
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "AreaTrabalho";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A6AreaTrabalho_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_OrganizacaoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Organiza��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Organizacao_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Organiza��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1214Organizacao_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa Jur�dica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_UF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "UF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A23Estado_UF;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Estado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A24Estado_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Munic�pio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A26Municipio_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A33Contratante_Fax;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ramal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ramal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A32Contratante_Ramal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A31Contratante_Telefone;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Email";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Email";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A14Contratante_Email;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_WebSite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Site";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A13Contratante_WebSite;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A12Contratante_CNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_IE";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Insc. Estadual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A11Contratante_IE;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_NomeFantasia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Fantasia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A10Contratante_NomeFantasia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RazaoSocial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Raz�o Social";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A9Contratante_RazaoSocial;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaHost";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Host SMTP";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A547Contratante_EmailSdaHost;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaUser";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A548Contratante_EmailSdaUser;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPass";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Senha";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A549Contratante_EmailSdaPass;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaKey";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Key";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A550Contratante_EmailSdaKey;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaAut";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Autentica��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPort";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Porta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaSec";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seguran�a";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ContagensQtdGeral";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd. de Contagens Geral";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_CalculoPFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Calculo P. Final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A642AreaTrabalho_CalculoPFinal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ServicoPadrao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o padr�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ValidaOSFM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valida OS FM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_DiasParaPagar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para pagar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ContratadaUpdBslCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quem atualiza o Baseline";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_TipoPlanilha";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Planilha";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A72AreaTrabalho_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_SS_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Area Trabalho_SS_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_VerTA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o do TA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_SelUsrPrestadora";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seleciona usu�rio da Prestadora";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A65 */
         pr_default.execute(1, new Object[] {AV16AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A6AreaTrabalho_Descricao = P00A65_A6AreaTrabalho_Descricao[0];
            A1216AreaTrabalho_OrganizacaoCod = P00A65_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00A65_n1216AreaTrabalho_OrganizacaoCod[0];
            A1214Organizacao_Nome = P00A65_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00A65_n1214Organizacao_Nome[0];
            A29Contratante_Codigo = P00A65_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00A65_n29Contratante_Codigo[0];
            A335Contratante_PessoaCod = P00A65_A335Contratante_PessoaCod[0];
            A23Estado_UF = P00A65_A23Estado_UF[0];
            A24Estado_Nome = P00A65_A24Estado_Nome[0];
            A25Municipio_Codigo = P00A65_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A65_n25Municipio_Codigo[0];
            A26Municipio_Nome = P00A65_A26Municipio_Nome[0];
            A33Contratante_Fax = P00A65_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A65_n33Contratante_Fax[0];
            A32Contratante_Ramal = P00A65_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A65_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00A65_A31Contratante_Telefone[0];
            A14Contratante_Email = P00A65_A14Contratante_Email[0];
            n14Contratante_Email = P00A65_n14Contratante_Email[0];
            A13Contratante_WebSite = P00A65_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A65_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = P00A65_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A65_n12Contratante_CNPJ[0];
            A11Contratante_IE = P00A65_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = P00A65_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00A65_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A65_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = P00A65_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A65_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A65_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A65_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A65_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A65_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A65_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A65_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A65_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A65_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A65_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A65_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A65_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A65_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = P00A65_A642AreaTrabalho_CalculoPFinal[0];
            A830AreaTrabalho_ServicoPadrao = P00A65_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00A65_n830AreaTrabalho_ServicoPadrao[0];
            A834AreaTrabalho_ValidaOSFM = P00A65_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P00A65_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = P00A65_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A65_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = P00A65_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = P00A65_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = P00A65_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = P00A65_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = P00A65_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = P00A65_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = P00A65_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = P00A65_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00A65_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = P00A65_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = P00A65_n2080AreaTrabalho_SelUsrPrestadora[0];
            A5AreaTrabalho_Codigo = P00A65_A5AreaTrabalho_Codigo[0];
            A272AreaTrabalho_ContagensQtdGeral = P00A65_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = P00A65_n272AreaTrabalho_ContagensQtdGeral[0];
            A1214Organizacao_Nome = P00A65_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = P00A65_n1214Organizacao_Nome[0];
            A335Contratante_PessoaCod = P00A65_A335Contratante_PessoaCod[0];
            A25Municipio_Codigo = P00A65_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A65_n25Municipio_Codigo[0];
            A33Contratante_Fax = P00A65_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A65_n33Contratante_Fax[0];
            A32Contratante_Ramal = P00A65_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A65_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00A65_A31Contratante_Telefone[0];
            A14Contratante_Email = P00A65_A14Contratante_Email[0];
            n14Contratante_Email = P00A65_n14Contratante_Email[0];
            A13Contratante_WebSite = P00A65_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A65_n13Contratante_WebSite[0];
            A11Contratante_IE = P00A65_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = P00A65_A10Contratante_NomeFantasia[0];
            A547Contratante_EmailSdaHost = P00A65_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A65_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A65_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A65_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A65_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A65_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A65_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A65_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A65_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A65_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A65_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A65_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A65_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A65_n1048Contratante_EmailSdaSec[0];
            A12Contratante_CNPJ = P00A65_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A65_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A65_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A65_n9Contratante_RazaoSocial[0];
            A23Estado_UF = P00A65_A23Estado_UF[0];
            A26Municipio_Nome = P00A65_A26Municipio_Nome[0];
            A24Estado_Nome = P00A65_A24Estado_Nome[0];
            A272AreaTrabalho_ContagensQtdGeral = P00A65_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = P00A65_n272AreaTrabalho_ContagensQtdGeral[0];
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "AreaTrabalho";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A6AreaTrabalho_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_OrganizacaoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Organiza��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Organizacao_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Organiza��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1214Organizacao_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa Jur�dica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_UF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "UF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A23Estado_UF;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Estado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A24Estado_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Munic�pio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A26Municipio_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A33Contratante_Fax;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ramal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ramal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A32Contratante_Ramal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A31Contratante_Telefone;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Email";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Email";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A14Contratante_Email;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_WebSite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Site";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A13Contratante_WebSite;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A12Contratante_CNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_IE";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Insc. Estadual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A11Contratante_IE;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_NomeFantasia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Fantasia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A10Contratante_NomeFantasia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RazaoSocial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Raz�o Social";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A9Contratante_RazaoSocial;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaHost";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Host SMTP";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A547Contratante_EmailSdaHost;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaUser";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A548Contratante_EmailSdaUser;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPass";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Senha";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A549Contratante_EmailSdaPass;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaKey";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Key";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A550Contratante_EmailSdaKey;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaAut";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Autentica��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPort";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Porta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaSec";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seguran�a";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ContagensQtdGeral";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd. de Contagens Geral";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_CalculoPFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Calculo P. Final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A642AreaTrabalho_CalculoPFinal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ServicoPadrao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o padr�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ValidaOSFM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valida OS FM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_DiasParaPagar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para pagar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_ContratadaUpdBslCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quem atualiza o Baseline";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_TipoPlanilha";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Planilha";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A72AreaTrabalho_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_SS_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Area Trabalho_SS_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_VerTA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o do TA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_SelUsrPrestadora";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seleciona usu�rio da Prestadora";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A65_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) )
                  {
                     A6AreaTrabalho_Descricao = P00A65_A6AreaTrabalho_Descricao[0];
                     A1216AreaTrabalho_OrganizacaoCod = P00A65_A1216AreaTrabalho_OrganizacaoCod[0];
                     n1216AreaTrabalho_OrganizacaoCod = P00A65_n1216AreaTrabalho_OrganizacaoCod[0];
                     A1214Organizacao_Nome = P00A65_A1214Organizacao_Nome[0];
                     n1214Organizacao_Nome = P00A65_n1214Organizacao_Nome[0];
                     A29Contratante_Codigo = P00A65_A29Contratante_Codigo[0];
                     n29Contratante_Codigo = P00A65_n29Contratante_Codigo[0];
                     A335Contratante_PessoaCod = P00A65_A335Contratante_PessoaCod[0];
                     A23Estado_UF = P00A65_A23Estado_UF[0];
                     A24Estado_Nome = P00A65_A24Estado_Nome[0];
                     A25Municipio_Codigo = P00A65_A25Municipio_Codigo[0];
                     n25Municipio_Codigo = P00A65_n25Municipio_Codigo[0];
                     A26Municipio_Nome = P00A65_A26Municipio_Nome[0];
                     A33Contratante_Fax = P00A65_A33Contratante_Fax[0];
                     n33Contratante_Fax = P00A65_n33Contratante_Fax[0];
                     A32Contratante_Ramal = P00A65_A32Contratante_Ramal[0];
                     n32Contratante_Ramal = P00A65_n32Contratante_Ramal[0];
                     A31Contratante_Telefone = P00A65_A31Contratante_Telefone[0];
                     A14Contratante_Email = P00A65_A14Contratante_Email[0];
                     n14Contratante_Email = P00A65_n14Contratante_Email[0];
                     A13Contratante_WebSite = P00A65_A13Contratante_WebSite[0];
                     n13Contratante_WebSite = P00A65_n13Contratante_WebSite[0];
                     A12Contratante_CNPJ = P00A65_A12Contratante_CNPJ[0];
                     n12Contratante_CNPJ = P00A65_n12Contratante_CNPJ[0];
                     A11Contratante_IE = P00A65_A11Contratante_IE[0];
                     A10Contratante_NomeFantasia = P00A65_A10Contratante_NomeFantasia[0];
                     A9Contratante_RazaoSocial = P00A65_A9Contratante_RazaoSocial[0];
                     n9Contratante_RazaoSocial = P00A65_n9Contratante_RazaoSocial[0];
                     A547Contratante_EmailSdaHost = P00A65_A547Contratante_EmailSdaHost[0];
                     n547Contratante_EmailSdaHost = P00A65_n547Contratante_EmailSdaHost[0];
                     A548Contratante_EmailSdaUser = P00A65_A548Contratante_EmailSdaUser[0];
                     n548Contratante_EmailSdaUser = P00A65_n548Contratante_EmailSdaUser[0];
                     A549Contratante_EmailSdaPass = P00A65_A549Contratante_EmailSdaPass[0];
                     n549Contratante_EmailSdaPass = P00A65_n549Contratante_EmailSdaPass[0];
                     A550Contratante_EmailSdaKey = P00A65_A550Contratante_EmailSdaKey[0];
                     n550Contratante_EmailSdaKey = P00A65_n550Contratante_EmailSdaKey[0];
                     A551Contratante_EmailSdaAut = P00A65_A551Contratante_EmailSdaAut[0];
                     n551Contratante_EmailSdaAut = P00A65_n551Contratante_EmailSdaAut[0];
                     A552Contratante_EmailSdaPort = P00A65_A552Contratante_EmailSdaPort[0];
                     n552Contratante_EmailSdaPort = P00A65_n552Contratante_EmailSdaPort[0];
                     A1048Contratante_EmailSdaSec = P00A65_A1048Contratante_EmailSdaSec[0];
                     n1048Contratante_EmailSdaSec = P00A65_n1048Contratante_EmailSdaSec[0];
                     A642AreaTrabalho_CalculoPFinal = P00A65_A642AreaTrabalho_CalculoPFinal[0];
                     A830AreaTrabalho_ServicoPadrao = P00A65_A830AreaTrabalho_ServicoPadrao[0];
                     n830AreaTrabalho_ServicoPadrao = P00A65_n830AreaTrabalho_ServicoPadrao[0];
                     A834AreaTrabalho_ValidaOSFM = P00A65_A834AreaTrabalho_ValidaOSFM[0];
                     n834AreaTrabalho_ValidaOSFM = P00A65_n834AreaTrabalho_ValidaOSFM[0];
                     A855AreaTrabalho_DiasParaPagar = P00A65_A855AreaTrabalho_DiasParaPagar[0];
                     n855AreaTrabalho_DiasParaPagar = P00A65_n855AreaTrabalho_DiasParaPagar[0];
                     A987AreaTrabalho_ContratadaUpdBslCod = P00A65_A987AreaTrabalho_ContratadaUpdBslCod[0];
                     n987AreaTrabalho_ContratadaUpdBslCod = P00A65_n987AreaTrabalho_ContratadaUpdBslCod[0];
                     A1154AreaTrabalho_TipoPlanilha = P00A65_A1154AreaTrabalho_TipoPlanilha[0];
                     n1154AreaTrabalho_TipoPlanilha = P00A65_n1154AreaTrabalho_TipoPlanilha[0];
                     A72AreaTrabalho_Ativo = P00A65_A72AreaTrabalho_Ativo[0];
                     A1588AreaTrabalho_SS_Codigo = P00A65_A1588AreaTrabalho_SS_Codigo[0];
                     n1588AreaTrabalho_SS_Codigo = P00A65_n1588AreaTrabalho_SS_Codigo[0];
                     A2081AreaTrabalho_VerTA = P00A65_A2081AreaTrabalho_VerTA[0];
                     n2081AreaTrabalho_VerTA = P00A65_n2081AreaTrabalho_VerTA[0];
                     A2080AreaTrabalho_SelUsrPrestadora = P00A65_A2080AreaTrabalho_SelUsrPrestadora[0];
                     n2080AreaTrabalho_SelUsrPrestadora = P00A65_n2080AreaTrabalho_SelUsrPrestadora[0];
                     A272AreaTrabalho_ContagensQtdGeral = P00A65_A272AreaTrabalho_ContagensQtdGeral[0];
                     n272AreaTrabalho_ContagensQtdGeral = P00A65_n272AreaTrabalho_ContagensQtdGeral[0];
                     A1214Organizacao_Nome = P00A65_A1214Organizacao_Nome[0];
                     n1214Organizacao_Nome = P00A65_n1214Organizacao_Nome[0];
                     A335Contratante_PessoaCod = P00A65_A335Contratante_PessoaCod[0];
                     A25Municipio_Codigo = P00A65_A25Municipio_Codigo[0];
                     n25Municipio_Codigo = P00A65_n25Municipio_Codigo[0];
                     A33Contratante_Fax = P00A65_A33Contratante_Fax[0];
                     n33Contratante_Fax = P00A65_n33Contratante_Fax[0];
                     A32Contratante_Ramal = P00A65_A32Contratante_Ramal[0];
                     n32Contratante_Ramal = P00A65_n32Contratante_Ramal[0];
                     A31Contratante_Telefone = P00A65_A31Contratante_Telefone[0];
                     A14Contratante_Email = P00A65_A14Contratante_Email[0];
                     n14Contratante_Email = P00A65_n14Contratante_Email[0];
                     A13Contratante_WebSite = P00A65_A13Contratante_WebSite[0];
                     n13Contratante_WebSite = P00A65_n13Contratante_WebSite[0];
                     A11Contratante_IE = P00A65_A11Contratante_IE[0];
                     A10Contratante_NomeFantasia = P00A65_A10Contratante_NomeFantasia[0];
                     A547Contratante_EmailSdaHost = P00A65_A547Contratante_EmailSdaHost[0];
                     n547Contratante_EmailSdaHost = P00A65_n547Contratante_EmailSdaHost[0];
                     A548Contratante_EmailSdaUser = P00A65_A548Contratante_EmailSdaUser[0];
                     n548Contratante_EmailSdaUser = P00A65_n548Contratante_EmailSdaUser[0];
                     A549Contratante_EmailSdaPass = P00A65_A549Contratante_EmailSdaPass[0];
                     n549Contratante_EmailSdaPass = P00A65_n549Contratante_EmailSdaPass[0];
                     A550Contratante_EmailSdaKey = P00A65_A550Contratante_EmailSdaKey[0];
                     n550Contratante_EmailSdaKey = P00A65_n550Contratante_EmailSdaKey[0];
                     A551Contratante_EmailSdaAut = P00A65_A551Contratante_EmailSdaAut[0];
                     n551Contratante_EmailSdaAut = P00A65_n551Contratante_EmailSdaAut[0];
                     A552Contratante_EmailSdaPort = P00A65_A552Contratante_EmailSdaPort[0];
                     n552Contratante_EmailSdaPort = P00A65_n552Contratante_EmailSdaPort[0];
                     A1048Contratante_EmailSdaSec = P00A65_A1048Contratante_EmailSdaSec[0];
                     n1048Contratante_EmailSdaSec = P00A65_n1048Contratante_EmailSdaSec[0];
                     A12Contratante_CNPJ = P00A65_A12Contratante_CNPJ[0];
                     n12Contratante_CNPJ = P00A65_n12Contratante_CNPJ[0];
                     A9Contratante_RazaoSocial = P00A65_A9Contratante_RazaoSocial[0];
                     n9Contratante_RazaoSocial = P00A65_n9Contratante_RazaoSocial[0];
                     A23Estado_UF = P00A65_A23Estado_UF[0];
                     A26Municipio_Nome = P00A65_A26Municipio_Nome[0];
                     A24Estado_Nome = P00A65_A24Estado_Nome[0];
                     A272AreaTrabalho_ContagensQtdGeral = P00A65_A272AreaTrabalho_ContagensQtdGeral[0];
                     n272AreaTrabalho_ContagensQtdGeral = P00A65_n272AreaTrabalho_ContagensQtdGeral[0];
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_Descricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A6AreaTrabalho_Descricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_OrganizacaoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Organizacao_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1214Organizacao_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Estado_UF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A23Estado_UF;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Estado_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A24Estado_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Municipio_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Municipio_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A26Municipio_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Fax") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A33Contratante_Fax;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Ramal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A32Contratante_Ramal;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Telefone") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A31Contratante_Telefone;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Email") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A14Contratante_Email;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_WebSite") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A13Contratante_WebSite;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_CNPJ") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A12Contratante_CNPJ;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_IE") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A11Contratante_IE;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_NomeFantasia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A10Contratante_NomeFantasia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_RazaoSocial") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A9Contratante_RazaoSocial;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaHost") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A547Contratante_EmailSdaHost;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaUser") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A548Contratante_EmailSdaUser;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaPass") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A549Contratante_EmailSdaPass;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaKey") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A550Contratante_EmailSdaKey;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaAut") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaPort") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaSec") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_ContagensQtdGeral") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_CalculoPFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A642AreaTrabalho_CalculoPFinal;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_ServicoPadrao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_ValidaOSFM") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_DiasParaPagar") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_ContratadaUpdBslCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_TipoPlanilha") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A72AreaTrabalho_Ativo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_SS_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_VerTA") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_SelUsrPrestadora") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora);
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A63_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00A63_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00A63_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00A63_A1214Organizacao_Nome = new String[] {""} ;
         P00A63_n1214Organizacao_Nome = new bool[] {false} ;
         P00A63_A29Contratante_Codigo = new int[1] ;
         P00A63_n29Contratante_Codigo = new bool[] {false} ;
         P00A63_A335Contratante_PessoaCod = new int[1] ;
         P00A63_A23Estado_UF = new String[] {""} ;
         P00A63_A24Estado_Nome = new String[] {""} ;
         P00A63_A25Municipio_Codigo = new int[1] ;
         P00A63_n25Municipio_Codigo = new bool[] {false} ;
         P00A63_A26Municipio_Nome = new String[] {""} ;
         P00A63_A33Contratante_Fax = new String[] {""} ;
         P00A63_n33Contratante_Fax = new bool[] {false} ;
         P00A63_A32Contratante_Ramal = new String[] {""} ;
         P00A63_n32Contratante_Ramal = new bool[] {false} ;
         P00A63_A31Contratante_Telefone = new String[] {""} ;
         P00A63_A14Contratante_Email = new String[] {""} ;
         P00A63_n14Contratante_Email = new bool[] {false} ;
         P00A63_A13Contratante_WebSite = new String[] {""} ;
         P00A63_n13Contratante_WebSite = new bool[] {false} ;
         P00A63_A12Contratante_CNPJ = new String[] {""} ;
         P00A63_n12Contratante_CNPJ = new bool[] {false} ;
         P00A63_A11Contratante_IE = new String[] {""} ;
         P00A63_A10Contratante_NomeFantasia = new String[] {""} ;
         P00A63_A9Contratante_RazaoSocial = new String[] {""} ;
         P00A63_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00A63_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00A63_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00A63_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00A63_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00A63_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00A63_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00A63_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00A63_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00A63_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A63_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A63_A552Contratante_EmailSdaPort = new short[1] ;
         P00A63_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00A63_A1048Contratante_EmailSdaSec = new short[1] ;
         P00A63_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         P00A63_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00A63_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00A63_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00A63_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00A63_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00A63_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00A63_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00A63_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         P00A63_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         P00A63_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         P00A63_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         P00A63_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00A63_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         P00A63_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         P00A63_A2081AreaTrabalho_VerTA = new short[1] ;
         P00A63_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00A63_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         P00A63_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         P00A63_A5AreaTrabalho_Codigo = new int[1] ;
         P00A63_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         P00A63_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         A6AreaTrabalho_Descricao = "";
         A1214Organizacao_Nome = "";
         A23Estado_UF = "";
         A24Estado_Nome = "";
         A26Municipio_Nome = "";
         A33Contratante_Fax = "";
         A32Contratante_Ramal = "";
         A31Contratante_Telefone = "";
         A14Contratante_Email = "";
         A13Contratante_WebSite = "";
         A12Contratante_CNPJ = "";
         A11Contratante_IE = "";
         A10Contratante_NomeFantasia = "";
         A9Contratante_RazaoSocial = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         A642AreaTrabalho_CalculoPFinal = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A65_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00A65_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00A65_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00A65_A1214Organizacao_Nome = new String[] {""} ;
         P00A65_n1214Organizacao_Nome = new bool[] {false} ;
         P00A65_A29Contratante_Codigo = new int[1] ;
         P00A65_n29Contratante_Codigo = new bool[] {false} ;
         P00A65_A335Contratante_PessoaCod = new int[1] ;
         P00A65_A23Estado_UF = new String[] {""} ;
         P00A65_A24Estado_Nome = new String[] {""} ;
         P00A65_A25Municipio_Codigo = new int[1] ;
         P00A65_n25Municipio_Codigo = new bool[] {false} ;
         P00A65_A26Municipio_Nome = new String[] {""} ;
         P00A65_A33Contratante_Fax = new String[] {""} ;
         P00A65_n33Contratante_Fax = new bool[] {false} ;
         P00A65_A32Contratante_Ramal = new String[] {""} ;
         P00A65_n32Contratante_Ramal = new bool[] {false} ;
         P00A65_A31Contratante_Telefone = new String[] {""} ;
         P00A65_A14Contratante_Email = new String[] {""} ;
         P00A65_n14Contratante_Email = new bool[] {false} ;
         P00A65_A13Contratante_WebSite = new String[] {""} ;
         P00A65_n13Contratante_WebSite = new bool[] {false} ;
         P00A65_A12Contratante_CNPJ = new String[] {""} ;
         P00A65_n12Contratante_CNPJ = new bool[] {false} ;
         P00A65_A11Contratante_IE = new String[] {""} ;
         P00A65_A10Contratante_NomeFantasia = new String[] {""} ;
         P00A65_A9Contratante_RazaoSocial = new String[] {""} ;
         P00A65_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00A65_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00A65_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00A65_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00A65_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00A65_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00A65_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00A65_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00A65_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00A65_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A65_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A65_A552Contratante_EmailSdaPort = new short[1] ;
         P00A65_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00A65_A1048Contratante_EmailSdaSec = new short[1] ;
         P00A65_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         P00A65_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00A65_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00A65_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00A65_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00A65_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P00A65_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00A65_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00A65_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         P00A65_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         P00A65_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         P00A65_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         P00A65_A72AreaTrabalho_Ativo = new bool[] {false} ;
         P00A65_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         P00A65_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         P00A65_A2081AreaTrabalho_VerTA = new short[1] ;
         P00A65_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         P00A65_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         P00A65_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         P00A65_A5AreaTrabalho_Codigo = new int[1] ;
         P00A65_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         P00A65_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditareatrabalho__default(),
            new Object[][] {
                new Object[] {
               P00A63_A6AreaTrabalho_Descricao, P00A63_A1216AreaTrabalho_OrganizacaoCod, P00A63_n1216AreaTrabalho_OrganizacaoCod, P00A63_A1214Organizacao_Nome, P00A63_n1214Organizacao_Nome, P00A63_A29Contratante_Codigo, P00A63_n29Contratante_Codigo, P00A63_A335Contratante_PessoaCod, P00A63_A23Estado_UF, P00A63_A24Estado_Nome,
               P00A63_A25Municipio_Codigo, P00A63_n25Municipio_Codigo, P00A63_A26Municipio_Nome, P00A63_A33Contratante_Fax, P00A63_n33Contratante_Fax, P00A63_A32Contratante_Ramal, P00A63_n32Contratante_Ramal, P00A63_A31Contratante_Telefone, P00A63_A14Contratante_Email, P00A63_n14Contratante_Email,
               P00A63_A13Contratante_WebSite, P00A63_n13Contratante_WebSite, P00A63_A12Contratante_CNPJ, P00A63_n12Contratante_CNPJ, P00A63_A11Contratante_IE, P00A63_A10Contratante_NomeFantasia, P00A63_A9Contratante_RazaoSocial, P00A63_n9Contratante_RazaoSocial, P00A63_A547Contratante_EmailSdaHost, P00A63_n547Contratante_EmailSdaHost,
               P00A63_A548Contratante_EmailSdaUser, P00A63_n548Contratante_EmailSdaUser, P00A63_A549Contratante_EmailSdaPass, P00A63_n549Contratante_EmailSdaPass, P00A63_A550Contratante_EmailSdaKey, P00A63_n550Contratante_EmailSdaKey, P00A63_A551Contratante_EmailSdaAut, P00A63_n551Contratante_EmailSdaAut, P00A63_A552Contratante_EmailSdaPort, P00A63_n552Contratante_EmailSdaPort,
               P00A63_A1048Contratante_EmailSdaSec, P00A63_n1048Contratante_EmailSdaSec, P00A63_A642AreaTrabalho_CalculoPFinal, P00A63_A830AreaTrabalho_ServicoPadrao, P00A63_n830AreaTrabalho_ServicoPadrao, P00A63_A834AreaTrabalho_ValidaOSFM, P00A63_n834AreaTrabalho_ValidaOSFM, P00A63_A855AreaTrabalho_DiasParaPagar, P00A63_n855AreaTrabalho_DiasParaPagar, P00A63_A987AreaTrabalho_ContratadaUpdBslCod,
               P00A63_n987AreaTrabalho_ContratadaUpdBslCod, P00A63_A1154AreaTrabalho_TipoPlanilha, P00A63_n1154AreaTrabalho_TipoPlanilha, P00A63_A72AreaTrabalho_Ativo, P00A63_A1588AreaTrabalho_SS_Codigo, P00A63_n1588AreaTrabalho_SS_Codigo, P00A63_A2081AreaTrabalho_VerTA, P00A63_n2081AreaTrabalho_VerTA, P00A63_A2080AreaTrabalho_SelUsrPrestadora, P00A63_n2080AreaTrabalho_SelUsrPrestadora,
               P00A63_A5AreaTrabalho_Codigo, P00A63_A272AreaTrabalho_ContagensQtdGeral, P00A63_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               P00A65_A6AreaTrabalho_Descricao, P00A65_A1216AreaTrabalho_OrganizacaoCod, P00A65_n1216AreaTrabalho_OrganizacaoCod, P00A65_A1214Organizacao_Nome, P00A65_n1214Organizacao_Nome, P00A65_A29Contratante_Codigo, P00A65_n29Contratante_Codigo, P00A65_A335Contratante_PessoaCod, P00A65_A23Estado_UF, P00A65_A24Estado_Nome,
               P00A65_A25Municipio_Codigo, P00A65_n25Municipio_Codigo, P00A65_A26Municipio_Nome, P00A65_A33Contratante_Fax, P00A65_n33Contratante_Fax, P00A65_A32Contratante_Ramal, P00A65_n32Contratante_Ramal, P00A65_A31Contratante_Telefone, P00A65_A14Contratante_Email, P00A65_n14Contratante_Email,
               P00A65_A13Contratante_WebSite, P00A65_n13Contratante_WebSite, P00A65_A12Contratante_CNPJ, P00A65_n12Contratante_CNPJ, P00A65_A11Contratante_IE, P00A65_A10Contratante_NomeFantasia, P00A65_A9Contratante_RazaoSocial, P00A65_n9Contratante_RazaoSocial, P00A65_A547Contratante_EmailSdaHost, P00A65_n547Contratante_EmailSdaHost,
               P00A65_A548Contratante_EmailSdaUser, P00A65_n548Contratante_EmailSdaUser, P00A65_A549Contratante_EmailSdaPass, P00A65_n549Contratante_EmailSdaPass, P00A65_A550Contratante_EmailSdaKey, P00A65_n550Contratante_EmailSdaKey, P00A65_A551Contratante_EmailSdaAut, P00A65_n551Contratante_EmailSdaAut, P00A65_A552Contratante_EmailSdaPort, P00A65_n552Contratante_EmailSdaPort,
               P00A65_A1048Contratante_EmailSdaSec, P00A65_n1048Contratante_EmailSdaSec, P00A65_A642AreaTrabalho_CalculoPFinal, P00A65_A830AreaTrabalho_ServicoPadrao, P00A65_n830AreaTrabalho_ServicoPadrao, P00A65_A834AreaTrabalho_ValidaOSFM, P00A65_n834AreaTrabalho_ValidaOSFM, P00A65_A855AreaTrabalho_DiasParaPagar, P00A65_n855AreaTrabalho_DiasParaPagar, P00A65_A987AreaTrabalho_ContratadaUpdBslCod,
               P00A65_n987AreaTrabalho_ContratadaUpdBslCod, P00A65_A1154AreaTrabalho_TipoPlanilha, P00A65_n1154AreaTrabalho_TipoPlanilha, P00A65_A72AreaTrabalho_Ativo, P00A65_A1588AreaTrabalho_SS_Codigo, P00A65_n1588AreaTrabalho_SS_Codigo, P00A65_A2081AreaTrabalho_VerTA, P00A65_n2081AreaTrabalho_VerTA, P00A65_A2080AreaTrabalho_SelUsrPrestadora, P00A65_n2080AreaTrabalho_SelUsrPrestadora,
               P00A65_A5AreaTrabalho_Codigo, P00A65_A272AreaTrabalho_ContagensQtdGeral, P00A65_n272AreaTrabalho_ContagensQtdGeral
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short A1154AreaTrabalho_TipoPlanilha ;
      private short A2081AreaTrabalho_VerTA ;
      private short A272AreaTrabalho_ContagensQtdGeral ;
      private int AV16AreaTrabalho_Codigo ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A25Municipio_Codigo ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A987AreaTrabalho_ContratadaUpdBslCod ;
      private int A1588AreaTrabalho_SS_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1214Organizacao_Nome ;
      private String A23Estado_UF ;
      private String A24Estado_Nome ;
      private String A26Municipio_Nome ;
      private String A33Contratante_Fax ;
      private String A32Contratante_Ramal ;
      private String A31Contratante_Telefone ;
      private String A11Contratante_IE ;
      private String A10Contratante_NomeFantasia ;
      private String A9Contratante_RazaoSocial ;
      private String A550Contratante_EmailSdaKey ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private bool returnInSub ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n1214Organizacao_Nome ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n33Contratante_Fax ;
      private bool n32Contratante_Ramal ;
      private bool n14Contratante_Email ;
      private bool n13Contratante_WebSite ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n987AreaTrabalho_ContratadaUpdBslCod ;
      private bool n1154AreaTrabalho_TipoPlanilha ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n1588AreaTrabalho_SS_Codigo ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool A2080AreaTrabalho_SelUsrPrestadora ;
      private bool n2080AreaTrabalho_SelUsrPrestadora ;
      private bool n272AreaTrabalho_ContagensQtdGeral ;
      private String A6AreaTrabalho_Descricao ;
      private String A14Contratante_Email ;
      private String A13Contratante_WebSite ;
      private String A12Contratante_CNPJ ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private String[] P00A63_A6AreaTrabalho_Descricao ;
      private int[] P00A63_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00A63_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00A63_A1214Organizacao_Nome ;
      private bool[] P00A63_n1214Organizacao_Nome ;
      private int[] P00A63_A29Contratante_Codigo ;
      private bool[] P00A63_n29Contratante_Codigo ;
      private int[] P00A63_A335Contratante_PessoaCod ;
      private String[] P00A63_A23Estado_UF ;
      private String[] P00A63_A24Estado_Nome ;
      private int[] P00A63_A25Municipio_Codigo ;
      private bool[] P00A63_n25Municipio_Codigo ;
      private String[] P00A63_A26Municipio_Nome ;
      private String[] P00A63_A33Contratante_Fax ;
      private bool[] P00A63_n33Contratante_Fax ;
      private String[] P00A63_A32Contratante_Ramal ;
      private bool[] P00A63_n32Contratante_Ramal ;
      private String[] P00A63_A31Contratante_Telefone ;
      private String[] P00A63_A14Contratante_Email ;
      private bool[] P00A63_n14Contratante_Email ;
      private String[] P00A63_A13Contratante_WebSite ;
      private bool[] P00A63_n13Contratante_WebSite ;
      private String[] P00A63_A12Contratante_CNPJ ;
      private bool[] P00A63_n12Contratante_CNPJ ;
      private String[] P00A63_A11Contratante_IE ;
      private String[] P00A63_A10Contratante_NomeFantasia ;
      private String[] P00A63_A9Contratante_RazaoSocial ;
      private bool[] P00A63_n9Contratante_RazaoSocial ;
      private String[] P00A63_A547Contratante_EmailSdaHost ;
      private bool[] P00A63_n547Contratante_EmailSdaHost ;
      private String[] P00A63_A548Contratante_EmailSdaUser ;
      private bool[] P00A63_n548Contratante_EmailSdaUser ;
      private String[] P00A63_A549Contratante_EmailSdaPass ;
      private bool[] P00A63_n549Contratante_EmailSdaPass ;
      private String[] P00A63_A550Contratante_EmailSdaKey ;
      private bool[] P00A63_n550Contratante_EmailSdaKey ;
      private bool[] P00A63_A551Contratante_EmailSdaAut ;
      private bool[] P00A63_n551Contratante_EmailSdaAut ;
      private short[] P00A63_A552Contratante_EmailSdaPort ;
      private bool[] P00A63_n552Contratante_EmailSdaPort ;
      private short[] P00A63_A1048Contratante_EmailSdaSec ;
      private bool[] P00A63_n1048Contratante_EmailSdaSec ;
      private String[] P00A63_A642AreaTrabalho_CalculoPFinal ;
      private int[] P00A63_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00A63_n830AreaTrabalho_ServicoPadrao ;
      private bool[] P00A63_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00A63_n834AreaTrabalho_ValidaOSFM ;
      private short[] P00A63_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00A63_n855AreaTrabalho_DiasParaPagar ;
      private int[] P00A63_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] P00A63_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] P00A63_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] P00A63_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] P00A63_A72AreaTrabalho_Ativo ;
      private int[] P00A63_A1588AreaTrabalho_SS_Codigo ;
      private bool[] P00A63_n1588AreaTrabalho_SS_Codigo ;
      private short[] P00A63_A2081AreaTrabalho_VerTA ;
      private bool[] P00A63_n2081AreaTrabalho_VerTA ;
      private bool[] P00A63_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] P00A63_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] P00A63_A5AreaTrabalho_Codigo ;
      private short[] P00A63_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] P00A63_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] P00A65_A6AreaTrabalho_Descricao ;
      private int[] P00A65_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00A65_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00A65_A1214Organizacao_Nome ;
      private bool[] P00A65_n1214Organizacao_Nome ;
      private int[] P00A65_A29Contratante_Codigo ;
      private bool[] P00A65_n29Contratante_Codigo ;
      private int[] P00A65_A335Contratante_PessoaCod ;
      private String[] P00A65_A23Estado_UF ;
      private String[] P00A65_A24Estado_Nome ;
      private int[] P00A65_A25Municipio_Codigo ;
      private bool[] P00A65_n25Municipio_Codigo ;
      private String[] P00A65_A26Municipio_Nome ;
      private String[] P00A65_A33Contratante_Fax ;
      private bool[] P00A65_n33Contratante_Fax ;
      private String[] P00A65_A32Contratante_Ramal ;
      private bool[] P00A65_n32Contratante_Ramal ;
      private String[] P00A65_A31Contratante_Telefone ;
      private String[] P00A65_A14Contratante_Email ;
      private bool[] P00A65_n14Contratante_Email ;
      private String[] P00A65_A13Contratante_WebSite ;
      private bool[] P00A65_n13Contratante_WebSite ;
      private String[] P00A65_A12Contratante_CNPJ ;
      private bool[] P00A65_n12Contratante_CNPJ ;
      private String[] P00A65_A11Contratante_IE ;
      private String[] P00A65_A10Contratante_NomeFantasia ;
      private String[] P00A65_A9Contratante_RazaoSocial ;
      private bool[] P00A65_n9Contratante_RazaoSocial ;
      private String[] P00A65_A547Contratante_EmailSdaHost ;
      private bool[] P00A65_n547Contratante_EmailSdaHost ;
      private String[] P00A65_A548Contratante_EmailSdaUser ;
      private bool[] P00A65_n548Contratante_EmailSdaUser ;
      private String[] P00A65_A549Contratante_EmailSdaPass ;
      private bool[] P00A65_n549Contratante_EmailSdaPass ;
      private String[] P00A65_A550Contratante_EmailSdaKey ;
      private bool[] P00A65_n550Contratante_EmailSdaKey ;
      private bool[] P00A65_A551Contratante_EmailSdaAut ;
      private bool[] P00A65_n551Contratante_EmailSdaAut ;
      private short[] P00A65_A552Contratante_EmailSdaPort ;
      private bool[] P00A65_n552Contratante_EmailSdaPort ;
      private short[] P00A65_A1048Contratante_EmailSdaSec ;
      private bool[] P00A65_n1048Contratante_EmailSdaSec ;
      private String[] P00A65_A642AreaTrabalho_CalculoPFinal ;
      private int[] P00A65_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00A65_n830AreaTrabalho_ServicoPadrao ;
      private bool[] P00A65_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P00A65_n834AreaTrabalho_ValidaOSFM ;
      private short[] P00A65_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00A65_n855AreaTrabalho_DiasParaPagar ;
      private int[] P00A65_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] P00A65_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] P00A65_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] P00A65_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] P00A65_A72AreaTrabalho_Ativo ;
      private int[] P00A65_A1588AreaTrabalho_SS_Codigo ;
      private bool[] P00A65_n1588AreaTrabalho_SS_Codigo ;
      private short[] P00A65_A2081AreaTrabalho_VerTA ;
      private bool[] P00A65_n2081AreaTrabalho_VerTA ;
      private bool[] P00A65_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] P00A65_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] P00A65_A5AreaTrabalho_Codigo ;
      private short[] P00A65_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] P00A65_n272AreaTrabalho_ContagensQtdGeral ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditareatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A63 ;
          prmP00A63 = new Object[] {
          new Object[] {"@AV16AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A63 ;
          cmdBufferP00A63=" SELECT T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T2.[Organizacao_Nome], T1.[Contratante_Codigo], T3.[Contratante_PessoaCod] AS Contratante_PessoaCod, T5.[Estado_UF], T6.[Estado_Nome], T3.[Municipio_Codigo], T5.[Municipio_Nome], T3.[Contratante_Fax], T3.[Contratante_Ramal], T3.[Contratante_Telefone], T3.[Contratante_Email], T3.[Contratante_WebSite], T4.[Pessoa_Docto] AS Contratante_CNPJ, T3.[Contratante_IE], T3.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Contratante_EmailSdaHost], T3.[Contratante_EmailSdaUser], T3.[Contratante_EmailSdaPass], T3.[Contratante_EmailSdaKey], T3.[Contratante_EmailSdaAut], T3.[Contratante_EmailSdaPort], T3.[Contratante_EmailSdaSec], T1.[AreaTrabalho_CalculoPFinal], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_ValidaOSFM], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ContratadaUpdBslCod], T1.[AreaTrabalho_TipoPlanilha], T1.[AreaTrabalho_Ativo], T1.[AreaTrabalho_SS_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_SelUsrPrestadora], T1.[AreaTrabalho_Codigo], COALESCE( T7.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (((((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Organizacao] T2 WITH (NOLOCK) ON T2.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T3.[Municipio_Codigo]) LEFT JOIN [Estado] T6 WITH (NOLOCK) ON T6.[Estado_UF] = T5.[Estado_UF]) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] "
          + " ) T7 ON T7.[Contagem_AreaTrabalhoCod] = T1.[AreaTrabalho_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV16AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo]" ;
          Object[] prmP00A65 ;
          prmP00A65 = new Object[] {
          new Object[] {"@AV16AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A65 ;
          cmdBufferP00A65=" SELECT T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T2.[Organizacao_Nome], T1.[Contratante_Codigo], T3.[Contratante_PessoaCod] AS Contratante_PessoaCod, T5.[Estado_UF], T6.[Estado_Nome], T3.[Municipio_Codigo], T5.[Municipio_Nome], T3.[Contratante_Fax], T3.[Contratante_Ramal], T3.[Contratante_Telefone], T3.[Contratante_Email], T3.[Contratante_WebSite], T4.[Pessoa_Docto] AS Contratante_CNPJ, T3.[Contratante_IE], T3.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Contratante_EmailSdaHost], T3.[Contratante_EmailSdaUser], T3.[Contratante_EmailSdaPass], T3.[Contratante_EmailSdaKey], T3.[Contratante_EmailSdaAut], T3.[Contratante_EmailSdaPort], T3.[Contratante_EmailSdaSec], T1.[AreaTrabalho_CalculoPFinal], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_ValidaOSFM], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ContratadaUpdBslCod], T1.[AreaTrabalho_TipoPlanilha], T1.[AreaTrabalho_Ativo], T1.[AreaTrabalho_SS_Codigo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_SelUsrPrestadora], T1.[AreaTrabalho_Codigo], COALESCE( T7.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (((((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Organizacao] T2 WITH (NOLOCK) ON T2.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T3.[Municipio_Codigo]) LEFT JOIN [Estado] T6 WITH (NOLOCK) ON T6.[Estado_UF] = T5.[Estado_UF]) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] "
          + " ) T7 ON T7.[Contagem_AreaTrabalhoCod] = T1.[AreaTrabalho_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV16AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo]" ;
          def= new CursorDef[] {
              new CursorDef("P00A63", cmdBufferP00A63,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A63,1,0,false,true )
             ,new CursorDef("P00A65", cmdBufferP00A65,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A65,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 20) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getString(16, 15) ;
                ((String[]) buf[25])[0] = rslt.getString(17, 100) ;
                ((String[]) buf[26])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((String[]) buf[28])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((String[]) buf[30])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(20);
                ((String[]) buf[32])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getString(22, 32) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((bool[]) buf[36])[0] = rslt.getBool(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((short[]) buf[38])[0] = rslt.getShort(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((short[]) buf[40])[0] = rslt.getShort(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((String[]) buf[42])[0] = rslt.getString(26, 2) ;
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((bool[]) buf[45])[0] = rslt.getBool(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((short[]) buf[47])[0] = rslt.getShort(29) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((int[]) buf[49])[0] = rslt.getInt(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((short[]) buf[51])[0] = rslt.getShort(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((bool[]) buf[53])[0] = rslt.getBool(32) ;
                ((int[]) buf[54])[0] = rslt.getInt(33) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(33);
                ((short[]) buf[56])[0] = rslt.getShort(34) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(34);
                ((bool[]) buf[58])[0] = rslt.getBool(35) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(35);
                ((int[]) buf[60])[0] = rslt.getInt(36) ;
                ((short[]) buf[61])[0] = rslt.getShort(37) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(37);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 20) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getString(16, 15) ;
                ((String[]) buf[25])[0] = rslt.getString(17, 100) ;
                ((String[]) buf[26])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((String[]) buf[28])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((String[]) buf[30])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(20);
                ((String[]) buf[32])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getString(22, 32) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((bool[]) buf[36])[0] = rslt.getBool(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((short[]) buf[38])[0] = rslt.getShort(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((short[]) buf[40])[0] = rslt.getShort(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((String[]) buf[42])[0] = rslt.getString(26, 2) ;
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((bool[]) buf[45])[0] = rslt.getBool(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((short[]) buf[47])[0] = rslt.getShort(29) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((int[]) buf[49])[0] = rslt.getInt(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((short[]) buf[51])[0] = rslt.getShort(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((bool[]) buf[53])[0] = rslt.getBool(32) ;
                ((int[]) buf[54])[0] = rslt.getInt(33) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(33);
                ((short[]) buf[56])[0] = rslt.getShort(34) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(34);
                ((bool[]) buf[58])[0] = rslt.getBool(35) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(35);
                ((int[]) buf[60])[0] = rslt.getInt(36) ;
                ((short[]) buf[61])[0] = rslt.getShort(37) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(37);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
