/*
               File: PRC_ConcluirSsOsOrigem
        Description: Concluir SS OS Origem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:24.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_concluirssosorigem : GXProcedure
   {
      public prc_concluirssosorigem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_concluirssosorigem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_UserId )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV10UserId = aP1_UserId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_UserId )
      {
         prc_concluirssosorigem objprc_concluirssosorigem;
         objprc_concluirssosorigem = new prc_concluirssosorigem();
         objprc_concluirssosorigem.AV8Codigo = aP0_Codigo;
         objprc_concluirssosorigem.AV10UserId = aP1_UserId;
         objprc_concluirssosorigem.context.SetSubmitInitialConfig(context);
         objprc_concluirssosorigem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_concluirssosorigem);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_concluirssosorigem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_int1 = AV9ContagemResultado_Codigo;
         new prc_ssorigem(context ).execute( ref  AV8Codigo, out  GXt_int1) ;
         AV9ContagemResultado_Codigo = GXt_int1;
         if ( AV9ContagemResultado_Codigo > 0 )
         {
            /* Execute user subroutine: 'SSTEMPENDENCIAS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            if ( AV9ContagemResultado_Codigo > 0 )
            {
               /* Execute user subroutine: 'CONCLUIRDEMANDA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONCLUIRDEMANDA' Routine */
         /* Using cursor P00DJ2 */
         pr_default.execute(0, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00DJ2_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00DJ2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00DJ2_n484ContagemResultado_StatusDmn[0];
            A1349ContagemResultado_DataExecucao = P00DJ2_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00DJ2_n1349ContagemResultado_DataExecucao[0];
            AV11StatusAnterior = A484ContagemResultado_StatusDmn;
            A484ContagemResultado_StatusDmn = "R";
            n484ContagemResultado_StatusDmn = false;
            A1349ContagemResultado_DataExecucao = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1349ContagemResultado_DataExecucao = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00DJ3 */
            pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00DJ4 */
            pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         GXt_dtime2 = DateTimeUtil.ResetTime( DateTime.MinValue ) ;
         new prc_inslogresponsavel(context ).execute( ref  AV9ContagemResultado_Codigo,  0,  "V",  "D",  AV10UserId,  0,  AV11StatusAnterior,  "R",  "",  GXt_dtime2,  true) ;
         new prc_disparoservicovinculado(context ).execute(  AV9ContagemResultado_Codigo,  AV10UserId) ;
      }

      protected void S121( )
      {
         /* 'SSTEMPENDENCIAS' Routine */
         /* Using cursor P00DJ5 */
         pr_default.execute(3, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A456ContagemResultado_Codigo = P00DJ5_A456ContagemResultado_Codigo[0];
            A517ContagemResultado_Ultima = P00DJ5_A517ContagemResultado_Ultima[0];
            A483ContagemResultado_StatusCnt = P00DJ5_A483ContagemResultado_StatusCnt[0];
            A602ContagemResultado_OSVinculada = P00DJ5_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00DJ5_n602ContagemResultado_OSVinculada[0];
            A473ContagemResultado_DataCnt = P00DJ5_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00DJ5_A511ContagemResultado_HoraCnt[0];
            A602ContagemResultado_OSVinculada = P00DJ5_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00DJ5_n602ContagemResultado_OSVinculada[0];
            AV9ContagemResultado_Codigo = 0;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DJ2_A456ContagemResultado_Codigo = new int[1] ;
         P00DJ2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00DJ2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00DJ2_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00DJ2_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         AV11StatusAnterior = "";
         Gx_date = DateTime.MinValue;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         P00DJ5_A456ContagemResultado_Codigo = new int[1] ;
         P00DJ5_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00DJ5_A483ContagemResultado_StatusCnt = new short[1] ;
         P00DJ5_A602ContagemResultado_OSVinculada = new int[1] ;
         P00DJ5_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00DJ5_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00DJ5_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_concluirssosorigem__default(),
            new Object[][] {
                new Object[] {
               P00DJ2_A456ContagemResultado_Codigo, P00DJ2_A484ContagemResultado_StatusDmn, P00DJ2_n484ContagemResultado_StatusDmn, P00DJ2_A1349ContagemResultado_DataExecucao, P00DJ2_n1349ContagemResultado_DataExecucao
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00DJ5_A456ContagemResultado_Codigo, P00DJ5_A517ContagemResultado_Ultima, P00DJ5_A483ContagemResultado_StatusCnt, P00DJ5_A602ContagemResultado_OSVinculada, P00DJ5_n602ContagemResultado_OSVinculada, P00DJ5_A473ContagemResultado_DataCnt, P00DJ5_A511ContagemResultado_HoraCnt
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short A483ContagemResultado_StatusCnt ;
      private int AV8Codigo ;
      private int AV10UserId ;
      private int AV9ContagemResultado_Codigo ;
      private int GXt_int1 ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV11StatusAnterior ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime GXt_dtime2 ;
      private DateTime Gx_date ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool A517ContagemResultado_Ultima ;
      private bool n602ContagemResultado_OSVinculada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00DJ2_A456ContagemResultado_Codigo ;
      private String[] P00DJ2_A484ContagemResultado_StatusDmn ;
      private bool[] P00DJ2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00DJ2_A1349ContagemResultado_DataExecucao ;
      private bool[] P00DJ2_n1349ContagemResultado_DataExecucao ;
      private int[] P00DJ5_A456ContagemResultado_Codigo ;
      private bool[] P00DJ5_A517ContagemResultado_Ultima ;
      private short[] P00DJ5_A483ContagemResultado_StatusCnt ;
      private int[] P00DJ5_A602ContagemResultado_OSVinculada ;
      private bool[] P00DJ5_n602ContagemResultado_OSVinculada ;
      private DateTime[] P00DJ5_A473ContagemResultado_DataCnt ;
      private String[] P00DJ5_A511ContagemResultado_HoraCnt ;
   }

   public class prc_concluirssosorigem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DJ2 ;
          prmP00DJ2 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DJ3 ;
          prmP00DJ3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DJ4 ;
          prmP00DJ4 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DJ5 ;
          prmP00DJ5 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DJ2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_DataExecucao] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DJ2,1,0,true,true )
             ,new CursorDef("P00DJ3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00DJ3)
             ,new CursorDef("P00DJ4", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00DJ4)
             ,new CursorDef("P00DJ5", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_OSVinculada], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_StatusCnt] <> 5) AND (T1.[ContagemResultado_Ultima] = 1) AND (T2.[ContagemResultado_OSVinculada] = @AV9ContagemResultado_Codigo) ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DJ5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
