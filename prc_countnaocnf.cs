/*
               File: PRC_CountNaoCnf
        Description: Count Nao Conformidades
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:14.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_countnaocnf : GXProcedure
   {
      public prc_countnaocnf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_countnaocnf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Indicador ,
                           int aP1_Codigo ,
                           IGxCollection aP2_Codigos ,
                           DateTime aP3_Date ,
                           out short aP4_Count )
      {
         this.AV12Indicador = aP0_Indicador;
         this.AV13Codigo = aP1_Codigo;
         this.AV10Codigos = aP2_Codigos;
         this.AV15Date = aP3_Date;
         this.AV11Count = 0 ;
         initialize();
         executePrivate();
         aP4_Count=this.AV11Count;
      }

      public short executeUdp( int aP0_Indicador ,
                               int aP1_Codigo ,
                               IGxCollection aP2_Codigos ,
                               DateTime aP3_Date )
      {
         this.AV12Indicador = aP0_Indicador;
         this.AV13Codigo = aP1_Codigo;
         this.AV10Codigos = aP2_Codigos;
         this.AV15Date = aP3_Date;
         this.AV11Count = 0 ;
         initialize();
         executePrivate();
         aP4_Count=this.AV11Count;
         return AV11Count ;
      }

      public void executeSubmit( int aP0_Indicador ,
                                 int aP1_Codigo ,
                                 IGxCollection aP2_Codigos ,
                                 DateTime aP3_Date ,
                                 out short aP4_Count )
      {
         prc_countnaocnf objprc_countnaocnf;
         objprc_countnaocnf = new prc_countnaocnf();
         objprc_countnaocnf.AV12Indicador = aP0_Indicador;
         objprc_countnaocnf.AV13Codigo = aP1_Codigo;
         objprc_countnaocnf.AV10Codigos = aP2_Codigos;
         objprc_countnaocnf.AV15Date = aP3_Date;
         objprc_countnaocnf.AV11Count = 0 ;
         objprc_countnaocnf.context.SetSubmitInitialConfig(context);
         objprc_countnaocnf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_countnaocnf);
         aP4_Count=this.AV11Count;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_countnaocnf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A2020ContagemResultadoNaoCnf_OSCod ,
                                              AV10Codigos ,
                                              AV13Codigo ,
                                              AV10Codigos.Count ,
                                              AV12Indicador ,
                                              A2060ContagemResultadoNaoCnf_IndCod ,
                                              AV15Date ,
                                              A566ContagemResultado_DataUltCnt },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00WX2 */
         pr_default.execute(0, new Object[] {AV13Codigo, AV12Indicador});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2060ContagemResultadoNaoCnf_IndCod = P00WX2_A2060ContagemResultadoNaoCnf_IndCod[0];
            n2060ContagemResultadoNaoCnf_IndCod = P00WX2_n2060ContagemResultadoNaoCnf_IndCod[0];
            A2020ContagemResultadoNaoCnf_OSCod = P00WX2_A2020ContagemResultadoNaoCnf_OSCod[0];
            A2024ContagemResultadoNaoCnf_Codigo = P00WX2_A2024ContagemResultadoNaoCnf_Codigo[0];
            AV11Count = (short)(AV11Count+1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00WX2_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         P00WX2_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         P00WX2_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         P00WX2_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_countnaocnf__default(),
            new Object[][] {
                new Object[] {
               P00WX2_A2060ContagemResultadoNaoCnf_IndCod, P00WX2_n2060ContagemResultadoNaoCnf_IndCod, P00WX2_A2020ContagemResultadoNaoCnf_OSCod, P00WX2_A2024ContagemResultadoNaoCnf_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11Count ;
      private int AV12Indicador ;
      private int AV13Codigo ;
      private int AV10Codigos_Count ;
      private int A2020ContagemResultadoNaoCnf_OSCod ;
      private int A2060ContagemResultadoNaoCnf_IndCod ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private String scmdbuf ;
      private DateTime AV15Date ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool n2060ContagemResultadoNaoCnf_IndCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00WX2_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] P00WX2_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] P00WX2_A2020ContagemResultadoNaoCnf_OSCod ;
      private int[] P00WX2_A2024ContagemResultadoNaoCnf_Codigo ;
      private short aP4_Count ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV10Codigos ;
   }

   public class prc_countnaocnf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00WX2( IGxContext context ,
                                             int A2020ContagemResultadoNaoCnf_OSCod ,
                                             IGxCollection AV10Codigos ,
                                             int AV13Codigo ,
                                             int AV10Codigos_Count ,
                                             int AV12Indicador ,
                                             int A2060ContagemResultadoNaoCnf_IndCod ,
                                             DateTime AV15Date ,
                                             DateTime A566ContagemResultado_DataUltCnt )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoNaoCnf_IndCod], [ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK)";
         if ( AV13Codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoNaoCnf_OSCod] = @AV13Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoNaoCnf_OSCod] = @AV13Codigo)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV10Codigos_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV10Codigos, "[ContagemResultadoNaoCnf_OSCod] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV10Codigos, "[ContagemResultadoNaoCnf_OSCod] IN (", ")") + ")";
            }
         }
         if ( AV12Indicador > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoNaoCnf_IndCod] = @AV12Indicador)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoNaoCnf_IndCod] = @AV12Indicador)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoNaoCnf_OSCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00WX2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WX2 ;
          prmP00WX2 = new Object[] {
          new Object[] {"@AV13Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Indicador",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WX2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
