/*
               File: GetWWFuncoesAPFAtributosFilterData
        Description: Get WWFuncoes APFAtributos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:52.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwfuncoesapfatributosfilterdata : GXProcedure
   {
      public getwwfuncoesapfatributosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwfuncoesapfatributosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
         return AV42OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwfuncoesapfatributosfilterdata objgetwwfuncoesapfatributosfilterdata;
         objgetwwfuncoesapfatributosfilterdata = new getwwfuncoesapfatributosfilterdata();
         objgetwwfuncoesapfatributosfilterdata.AV33DDOName = aP0_DDOName;
         objgetwwfuncoesapfatributosfilterdata.AV31SearchTxt = aP1_SearchTxt;
         objgetwwfuncoesapfatributosfilterdata.AV32SearchTxtTo = aP2_SearchTxtTo;
         objgetwwfuncoesapfatributosfilterdata.AV37OptionsJson = "" ;
         objgetwwfuncoesapfatributosfilterdata.AV40OptionsDescJson = "" ;
         objgetwwfuncoesapfatributosfilterdata.AV42OptionIndexesJson = "" ;
         objgetwwfuncoesapfatributosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwfuncoesapfatributosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwfuncoesapfatributosfilterdata);
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwfuncoesapfatributosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV36Options = (IGxCollection)(new GxSimpleCollection());
         AV39OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV41OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCAOAPF_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFATRIBUTOS_ATRTABELANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCOESAPFATRIBUTOS_CODE") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCOESAPFATRIBUTOS_CODEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCOESAPFATRIBUTOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCOESAPFATRIBUTOS_NOMEOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCOESAPFATRIBUTOS_DESCRICAOOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV37OptionsJson = AV36Options.ToJSonString(false);
         AV40OptionsDescJson = AV39OptionsDesc.ToJSonString(false);
         AV42OptionIndexesJson = AV41OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV44Session.Get("WWFuncoesAPFAtributosGridState"), "") == 0 )
         {
            AV46GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFuncoesAPFAtributosGridState"), "");
         }
         else
         {
            AV46GridState.FromXml(AV44Session.Get("WWFuncoesAPFAtributosGridState"), "");
         }
         AV65GXV1 = 1;
         while ( AV65GXV1 <= AV46GridState.gxTpr_Filtervalues.Count )
         {
            AV47GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV46GridState.gxTpr_Filtervalues.Item(AV65GXV1));
            if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_CODIGO") == 0 )
            {
               AV10TFFuncaoAPF_Codigo = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV11TFFuncaoAPF_Codigo_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV12TFFuncaoAPF_Nome = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV13TFFuncaoAPF_Nome_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD") == 0 )
            {
               AV14TFFuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV15TFFuncaoAPFAtributos_AtributosCod_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV16TFFuncaoAPFAtributos_AtributosNom = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL") == 0 )
            {
               AV17TFFuncaoAPFAtributos_AtributosNom_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD") == 0 )
            {
               AV18TFFuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELACOD") == 0 )
            {
               AV20TFFuncaoAPFAtributos_AtrTabelaCod = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV21TFFuncaoAPFAtributos_AtrTabelaCod_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
            {
               AV22TFFuncaoAPFAtributos_AtrTabelaNom = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL") == 0 )
            {
               AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_REGRA_SEL") == 0 )
            {
               AV24TFFuncoesAPFAtributos_Regra_Sel = (short)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_CODE") == 0 )
            {
               AV25TFFuncoesAPFAtributos_Code = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_CODE_SEL") == 0 )
            {
               AV26TFFuncoesAPFAtributos_Code_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_NOME") == 0 )
            {
               AV27TFFuncoesAPFAtributos_Nome = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_NOME_SEL") == 0 )
            {
               AV28TFFuncoesAPFAtributos_Nome_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_DESCRICAO") == 0 )
            {
               AV29TFFuncoesAPFAtributos_Descricao = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL") == 0 )
            {
               AV30TFFuncoesAPFAtributos_Descricao_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            AV65GXV1 = (int)(AV65GXV1+1);
         }
         if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(1));
            AV49DynamicFiltersSelector1 = AV48GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV50DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV51FuncaoAPF_Nome1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV50DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV52FuncaoAPFAtributos_AtributosNom1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV53DynamicFiltersEnabled2 = true;
               AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(2));
               AV54DynamicFiltersSelector2 = AV48GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV54DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV55DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV56FuncaoAPF_Nome2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
               {
                  AV55DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV57FuncaoAPFAtributos_AtributosNom2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV58DynamicFiltersEnabled3 = true;
                  AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(3));
                  AV59DynamicFiltersSelector3 = AV48GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV60DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV61FuncaoAPF_Nome3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
                  {
                     AV60DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV62FuncaoAPFAtributos_AtributosNom3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPF_NOMEOPTIONS' Routine */
         AV12TFFuncaoAPF_Nome = AV31SearchTxt;
         AV13TFFuncaoAPF_Nome_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN2 */
         pr_default.execute(0, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLN2 = false;
            A165FuncaoAPF_Codigo = P00LN2_A165FuncaoAPF_Codigo[0];
            A385FuncoesAPFAtributos_Descricao = P00LN2_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN2_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P00LN2_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN2_n384FuncoesAPFAtributos_Nome[0];
            A383FuncoesAPFAtributos_Code = P00LN2_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN2_n383FuncoesAPFAtributos_Code[0];
            A389FuncoesAPFAtributos_Regra = P00LN2_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN2_n389FuncoesAPFAtributos_Regra[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN2_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN2_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN2_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN2_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN2_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN2_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00LN2_A364FuncaoAPFAtributos_AtributosCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN2_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN2_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN2_A166FuncaoAPF_Nome[0];
            A166FuncaoAPF_Nome = P00LN2_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN2_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN2_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN2_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN2_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN2_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN2_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            AV43count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00LN2_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
            {
               BRKLN2 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00LN2_A364FuncaoAPFAtributos_AtributosCod[0];
               AV43count = (long)(AV43count+1);
               BRKLN2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
            {
               AV35Option = A166FuncaoAPF_Nome;
               AV34InsertIndex = 1;
               while ( ( AV34InsertIndex <= AV36Options.Count ) && ( StringUtil.StrCmp(((String)AV36Options.Item(AV34InsertIndex)), AV35Option) < 0 ) )
               {
                  AV34InsertIndex = (int)(AV34InsertIndex+1);
               }
               AV36Options.Add(AV35Option, AV34InsertIndex);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), AV34InsertIndex);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN2 )
            {
               BRKLN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMOPTIONS' Routine */
         AV16TFFuncaoAPFAtributos_AtributosNom = AV31SearchTxt;
         AV17TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN3 */
         pr_default.execute(1, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLN4 = false;
            A364FuncaoAPFAtributos_AtributosCod = P00LN3_A364FuncaoAPFAtributos_AtributosCod[0];
            A385FuncoesAPFAtributos_Descricao = P00LN3_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN3_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P00LN3_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN3_n384FuncoesAPFAtributos_Nome[0];
            A383FuncoesAPFAtributos_Code = P00LN3_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN3_n383FuncoesAPFAtributos_Code[0];
            A389FuncoesAPFAtributos_Regra = P00LN3_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN3_n389FuncoesAPFAtributos_Regra[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN3_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN3_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN3_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN3_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN3_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN3_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A165FuncaoAPF_Codigo = P00LN3_A165FuncaoAPF_Codigo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN3_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN3_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN3_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN3_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN3_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN3_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN3_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN3_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN3_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A166FuncaoAPF_Nome = P00LN3_A166FuncaoAPF_Nome[0];
            AV43count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00LN3_A364FuncaoAPFAtributos_AtributosCod[0] == A364FuncaoAPFAtributos_AtributosCod ) )
            {
               BRKLN4 = false;
               A165FuncaoAPF_Codigo = P00LN3_A165FuncaoAPF_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKLN4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom)) )
            {
               AV35Option = A365FuncaoAPFAtributos_AtributosNom;
               AV34InsertIndex = 1;
               while ( ( AV34InsertIndex <= AV36Options.Count ) && ( StringUtil.StrCmp(((String)AV36Options.Item(AV34InsertIndex)), AV35Option) < 0 ) )
               {
                  AV34InsertIndex = (int)(AV34InsertIndex+1);
               }
               AV36Options.Add(AV35Option, AV34InsertIndex);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), AV34InsertIndex);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN4 )
            {
               BRKLN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADFUNCAOAPFATRIBUTOS_ATRTABELANOMOPTIONS' Routine */
         AV22TFFuncaoAPFAtributos_AtrTabelaNom = AV31SearchTxt;
         AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN4 */
         pr_default.execute(2, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKLN6 = false;
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN4_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A385FuncoesAPFAtributos_Descricao = P00LN4_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN4_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P00LN4_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN4_n384FuncoesAPFAtributos_Nome[0];
            A383FuncoesAPFAtributos_Code = P00LN4_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN4_n383FuncoesAPFAtributos_Code[0];
            A389FuncoesAPFAtributos_Regra = P00LN4_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN4_n389FuncoesAPFAtributos_Regra[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN4_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN4_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00LN4_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = P00LN4_A165FuncaoAPF_Codigo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN4_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN4_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN4_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN4_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN4_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN4_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A166FuncaoAPF_Nome = P00LN4_A166FuncaoAPF_Nome[0];
            AV43count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom[0], A367FuncaoAPFAtributos_AtrTabelaNom) == 0 ) )
            {
               BRKLN6 = false;
               A366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A364FuncaoAPFAtributos_AtributosCod = P00LN4_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = P00LN4_A165FuncaoAPF_Codigo[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               AV43count = (long)(AV43count+1);
               BRKLN6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom)) )
            {
               AV35Option = A367FuncaoAPFAtributos_AtrTabelaNom;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN6 )
            {
               BRKLN6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADFUNCOESAPFATRIBUTOS_CODEOPTIONS' Routine */
         AV25TFFuncoesAPFAtributos_Code = AV31SearchTxt;
         AV26TFFuncoesAPFAtributos_Code_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN5 */
         pr_default.execute(3, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKLN8 = false;
            A383FuncoesAPFAtributos_Code = P00LN5_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN5_n383FuncoesAPFAtributos_Code[0];
            A385FuncoesAPFAtributos_Descricao = P00LN5_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN5_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P00LN5_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN5_n384FuncoesAPFAtributos_Nome[0];
            A389FuncoesAPFAtributos_Regra = P00LN5_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN5_n389FuncoesAPFAtributos_Regra[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN5_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN5_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN5_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN5_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN5_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN5_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00LN5_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = P00LN5_A165FuncaoAPF_Codigo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN5_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN5_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN5_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN5_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN5_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN5_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN5_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN5_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN5_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A166FuncaoAPF_Nome = P00LN5_A166FuncaoAPF_Nome[0];
            AV43count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00LN5_A383FuncoesAPFAtributos_Code[0], A383FuncoesAPFAtributos_Code) == 0 ) )
            {
               BRKLN8 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00LN5_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = P00LN5_A165FuncaoAPF_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKLN8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A383FuncoesAPFAtributos_Code)) )
            {
               AV35Option = A383FuncoesAPFAtributos_Code;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN8 )
            {
               BRKLN8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADFUNCOESAPFATRIBUTOS_NOMEOPTIONS' Routine */
         AV27TFFuncoesAPFAtributos_Nome = AV31SearchTxt;
         AV28TFFuncoesAPFAtributos_Nome_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN6 */
         pr_default.execute(4, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKLN10 = false;
            A384FuncoesAPFAtributos_Nome = P00LN6_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN6_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = P00LN6_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN6_n385FuncoesAPFAtributos_Descricao[0];
            A383FuncoesAPFAtributos_Code = P00LN6_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN6_n383FuncoesAPFAtributos_Code[0];
            A389FuncoesAPFAtributos_Regra = P00LN6_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN6_n389FuncoesAPFAtributos_Regra[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN6_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN6_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN6_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN6_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN6_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN6_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00LN6_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = P00LN6_A165FuncaoAPF_Codigo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN6_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN6_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN6_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN6_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN6_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN6_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN6_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN6_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN6_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A166FuncaoAPF_Nome = P00LN6_A166FuncaoAPF_Nome[0];
            AV43count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00LN6_A384FuncoesAPFAtributos_Nome[0], A384FuncoesAPFAtributos_Nome) == 0 ) )
            {
               BRKLN10 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00LN6_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = P00LN6_A165FuncaoAPF_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKLN10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A384FuncoesAPFAtributos_Nome)) )
            {
               AV35Option = A384FuncoesAPFAtributos_Nome;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN10 )
            {
               BRKLN10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADFUNCOESAPFATRIBUTOS_DESCRICAOOPTIONS' Routine */
         AV29TFFuncoesAPFAtributos_Descricao = AV31SearchTxt;
         AV30TFFuncoesAPFAtributos_Descricao_Sel = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = AV51FuncaoAPF_Nome1;
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = AV52FuncaoAPFAtributos_AtributosNom1;
         AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = AV56FuncaoAPF_Nome2;
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = AV57FuncaoAPFAtributos_AtributosNom2;
         AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 = AV60DynamicFiltersOperator3;
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = AV61FuncaoAPF_Nome3;
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = AV62FuncaoAPFAtributos_AtributosNom3;
         AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo = AV10TFFuncaoAPF_Codigo;
         AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to = AV11TFFuncaoAPF_Codigo_To;
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = AV12TFFuncaoAPF_Nome;
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = AV13TFFuncaoAPF_Nome_Sel;
         AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod = AV14TFFuncaoAPFAtributos_AtributosCod;
         AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to = AV15TFFuncaoAPFAtributos_AtributosCod_To;
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = AV16TFFuncaoAPFAtributos_AtributosNom;
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = AV17TFFuncaoAPFAtributos_AtributosNom_Sel;
         AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod = AV18TFFuncaoAPFAtributos_FuncaoDadosCod;
         AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to = AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To;
         AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod = AV20TFFuncaoAPFAtributos_AtrTabelaCod;
         AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to = AV21TFFuncaoAPFAtributos_AtrTabelaCod_To;
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = AV22TFFuncaoAPFAtributos_AtrTabelaNom;
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
         AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel = AV24TFFuncoesAPFAtributos_Regra_Sel;
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = AV25TFFuncoesAPFAtributos_Code;
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = AV26TFFuncoesAPFAtributos_Code_Sel;
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = AV27TFFuncoesAPFAtributos_Nome;
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = AV28TFFuncoesAPFAtributos_Nome_Sel;
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = AV29TFFuncoesAPFAtributos_Descricao;
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = AV30TFFuncoesAPFAtributos_Descricao_Sel;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                              AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                              AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                              AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                              AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                              AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                              AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                              AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                              AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                              AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                              AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                              AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                              AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                              AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                              AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                              AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                              AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                              AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                              AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                              AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                              AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                              AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                              AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                              AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                              AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                              AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                              AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                              AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                              AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                              AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                              AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                              AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                              AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                              AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1), "%", "");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1), 50, "%");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2), "%", "");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = StringUtil.PadR( StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2), 50, "%");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3), "%", "");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = StringUtil.PadR( StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3), 50, "%");
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome), "%", "");
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = StringUtil.PadR( StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom), 50, "%");
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = StringUtil.PadR( StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom), 50, "%");
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = StringUtil.Concat( StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code), "%", "");
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome), 50, "%");
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao), "%", "");
         /* Using cursor P00LN7 */
         pr_default.execute(5, new Object[] {lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3, AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo, AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to, lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome, AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel, AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod, AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to, lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom, AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel, AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod, AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to, AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod, AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to, lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom, AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel, lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code, AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel, lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome, AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel, lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao, AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKLN12 = false;
            A385FuncoesAPFAtributos_Descricao = P00LN7_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P00LN7_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P00LN7_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P00LN7_n384FuncoesAPFAtributos_Nome[0];
            A383FuncoesAPFAtributos_Code = P00LN7_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P00LN7_n383FuncoesAPFAtributos_Code[0];
            A389FuncoesAPFAtributos_Regra = P00LN7_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P00LN7_n389FuncoesAPFAtributos_Regra[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN7_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN7_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN7_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN7_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00LN7_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00LN7_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00LN7_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = P00LN7_A165FuncaoAPF_Codigo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN7_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN7_n365FuncaoAPFAtributos_AtributosNom[0];
            A166FuncaoAPF_Nome = P00LN7_A166FuncaoAPF_Nome[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00LN7_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00LN7_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A365FuncaoAPFAtributos_AtributosNom = P00LN7_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00LN7_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00LN7_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00LN7_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A166FuncaoAPF_Nome = P00LN7_A166FuncaoAPF_Nome[0];
            AV43count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( StringUtil.StrCmp(P00LN7_A385FuncoesAPFAtributos_Descricao[0], A385FuncoesAPFAtributos_Descricao) == 0 ) )
            {
               BRKLN12 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00LN7_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = P00LN7_A165FuncaoAPF_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKLN12 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A385FuncoesAPFAtributos_Descricao)) )
            {
               AV35Option = A385FuncoesAPFAtributos_Descricao;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLN12 )
            {
               BRKLN12 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV36Options = new GxSimpleCollection();
         AV39OptionsDesc = new GxSimpleCollection();
         AV41OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV44Session = context.GetSession();
         AV46GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV47GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFFuncaoAPF_Nome = "";
         AV13TFFuncaoAPF_Nome_Sel = "";
         AV16TFFuncaoAPFAtributos_AtributosNom = "";
         AV17TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         AV22TFFuncaoAPFAtributos_AtrTabelaNom = "";
         AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         AV25TFFuncoesAPFAtributos_Code = "";
         AV26TFFuncoesAPFAtributos_Code_Sel = "";
         AV27TFFuncoesAPFAtributos_Nome = "";
         AV28TFFuncoesAPFAtributos_Nome_Sel = "";
         AV29TFFuncoesAPFAtributos_Descricao = "";
         AV30TFFuncoesAPFAtributos_Descricao_Sel = "";
         AV48GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV49DynamicFiltersSelector1 = "";
         AV51FuncaoAPF_Nome1 = "";
         AV52FuncaoAPFAtributos_AtributosNom1 = "";
         AV54DynamicFiltersSelector2 = "";
         AV56FuncaoAPF_Nome2 = "";
         AV57FuncaoAPFAtributos_AtributosNom2 = "";
         AV59DynamicFiltersSelector3 = "";
         AV61FuncaoAPF_Nome3 = "";
         AV62FuncaoAPFAtributos_AtributosNom3 = "";
         AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 = "";
         AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = "";
         AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = "";
         AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 = "";
         AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = "";
         AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = "";
         AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 = "";
         AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = "";
         AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = "";
         AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = "";
         AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel = "";
         AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = "";
         AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel = "";
         AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = "";
         AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel = "";
         AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = "";
         AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel = "";
         AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = "";
         AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel = "";
         AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = "";
         AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel = "";
         scmdbuf = "";
         lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 = "";
         lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 = "";
         lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 = "";
         lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 = "";
         lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 = "";
         lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 = "";
         lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome = "";
         lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom = "";
         lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom = "";
         lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code = "";
         lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome = "";
         lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao = "";
         A166FuncaoAPF_Nome = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         A385FuncoesAPFAtributos_Descricao = "";
         P00LN2_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN2_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN2_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN2_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN2_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN2_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN2_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN2_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN2_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN2_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN2_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN2_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN2_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN2_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN2_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN2_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN2_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN2_A166FuncaoAPF_Nome = new String[] {""} ;
         AV35Option = "";
         P00LN3_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN3_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN3_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN3_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN3_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN3_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN3_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN3_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN3_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN3_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN3_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN3_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN3_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN3_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN3_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN3_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN3_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN3_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN3_A166FuncaoAPF_Nome = new String[] {""} ;
         P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN4_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN4_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN4_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN4_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN4_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN4_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN4_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN4_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN4_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN4_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN4_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN4_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN4_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN4_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN4_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN4_A166FuncaoAPF_Nome = new String[] {""} ;
         P00LN5_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN5_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN5_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN5_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN5_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN5_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN5_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN5_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN5_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN5_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN5_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN5_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN5_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN5_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN5_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN5_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN5_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN5_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN5_A166FuncaoAPF_Nome = new String[] {""} ;
         P00LN6_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN6_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN6_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN6_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN6_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN6_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN6_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN6_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN6_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN6_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN6_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN6_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN6_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN6_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN6_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN6_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN6_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN6_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN6_A166FuncaoAPF_Nome = new String[] {""} ;
         P00LN7_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P00LN7_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P00LN7_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P00LN7_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P00LN7_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P00LN7_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P00LN7_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN7_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P00LN7_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00LN7_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00LN7_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00LN7_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00LN7_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00LN7_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00LN7_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00LN7_A165FuncaoAPF_Codigo = new int[1] ;
         P00LN7_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00LN7_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00LN7_A166FuncaoAPF_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwfuncoesapfatributosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LN2_A165FuncaoAPF_Codigo, P00LN2_A385FuncoesAPFAtributos_Descricao, P00LN2_n385FuncoesAPFAtributos_Descricao, P00LN2_A384FuncoesAPFAtributos_Nome, P00LN2_n384FuncoesAPFAtributos_Nome, P00LN2_A383FuncoesAPFAtributos_Code, P00LN2_n383FuncoesAPFAtributos_Code, P00LN2_A389FuncoesAPFAtributos_Regra, P00LN2_n389FuncoesAPFAtributos_Regra, P00LN2_A367FuncaoAPFAtributos_AtrTabelaNom,
               P00LN2_n367FuncaoAPFAtributos_AtrTabelaNom, P00LN2_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN2_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN2_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN2_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN2_A364FuncaoAPFAtributos_AtributosCod, P00LN2_A365FuncaoAPFAtributos_AtributosNom, P00LN2_n365FuncaoAPFAtributos_AtributosNom, P00LN2_A166FuncaoAPF_Nome
               }
               , new Object[] {
               P00LN3_A364FuncaoAPFAtributos_AtributosCod, P00LN3_A385FuncoesAPFAtributos_Descricao, P00LN3_n385FuncoesAPFAtributos_Descricao, P00LN3_A384FuncoesAPFAtributos_Nome, P00LN3_n384FuncoesAPFAtributos_Nome, P00LN3_A383FuncoesAPFAtributos_Code, P00LN3_n383FuncoesAPFAtributos_Code, P00LN3_A389FuncoesAPFAtributos_Regra, P00LN3_n389FuncoesAPFAtributos_Regra, P00LN3_A367FuncaoAPFAtributos_AtrTabelaNom,
               P00LN3_n367FuncaoAPFAtributos_AtrTabelaNom, P00LN3_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN3_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN3_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN3_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN3_A165FuncaoAPF_Codigo, P00LN3_A365FuncaoAPFAtributos_AtributosNom, P00LN3_n365FuncaoAPFAtributos_AtributosNom, P00LN3_A166FuncaoAPF_Nome
               }
               , new Object[] {
               P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom, P00LN4_n367FuncaoAPFAtributos_AtrTabelaNom, P00LN4_A385FuncoesAPFAtributos_Descricao, P00LN4_n385FuncoesAPFAtributos_Descricao, P00LN4_A384FuncoesAPFAtributos_Nome, P00LN4_n384FuncoesAPFAtributos_Nome, P00LN4_A383FuncoesAPFAtributos_Code, P00LN4_n383FuncoesAPFAtributos_Code, P00LN4_A389FuncoesAPFAtributos_Regra, P00LN4_n389FuncoesAPFAtributos_Regra,
               P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN4_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN4_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN4_A364FuncaoAPFAtributos_AtributosCod, P00LN4_A165FuncaoAPF_Codigo, P00LN4_A365FuncaoAPFAtributos_AtributosNom, P00LN4_n365FuncaoAPFAtributos_AtributosNom, P00LN4_A166FuncaoAPF_Nome
               }
               , new Object[] {
               P00LN5_A383FuncoesAPFAtributos_Code, P00LN5_n383FuncoesAPFAtributos_Code, P00LN5_A385FuncoesAPFAtributos_Descricao, P00LN5_n385FuncoesAPFAtributos_Descricao, P00LN5_A384FuncoesAPFAtributos_Nome, P00LN5_n384FuncoesAPFAtributos_Nome, P00LN5_A389FuncoesAPFAtributos_Regra, P00LN5_n389FuncoesAPFAtributos_Regra, P00LN5_A367FuncaoAPFAtributos_AtrTabelaNom, P00LN5_n367FuncaoAPFAtributos_AtrTabelaNom,
               P00LN5_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN5_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN5_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN5_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN5_A364FuncaoAPFAtributos_AtributosCod, P00LN5_A165FuncaoAPF_Codigo, P00LN5_A365FuncaoAPFAtributos_AtributosNom, P00LN5_n365FuncaoAPFAtributos_AtributosNom, P00LN5_A166FuncaoAPF_Nome
               }
               , new Object[] {
               P00LN6_A384FuncoesAPFAtributos_Nome, P00LN6_n384FuncoesAPFAtributos_Nome, P00LN6_A385FuncoesAPFAtributos_Descricao, P00LN6_n385FuncoesAPFAtributos_Descricao, P00LN6_A383FuncoesAPFAtributos_Code, P00LN6_n383FuncoesAPFAtributos_Code, P00LN6_A389FuncoesAPFAtributos_Regra, P00LN6_n389FuncoesAPFAtributos_Regra, P00LN6_A367FuncaoAPFAtributos_AtrTabelaNom, P00LN6_n367FuncaoAPFAtributos_AtrTabelaNom,
               P00LN6_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN6_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN6_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN6_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN6_A364FuncaoAPFAtributos_AtributosCod, P00LN6_A165FuncaoAPF_Codigo, P00LN6_A365FuncaoAPFAtributos_AtributosNom, P00LN6_n365FuncaoAPFAtributos_AtributosNom, P00LN6_A166FuncaoAPF_Nome
               }
               , new Object[] {
               P00LN7_A385FuncoesAPFAtributos_Descricao, P00LN7_n385FuncoesAPFAtributos_Descricao, P00LN7_A384FuncoesAPFAtributos_Nome, P00LN7_n384FuncoesAPFAtributos_Nome, P00LN7_A383FuncoesAPFAtributos_Code, P00LN7_n383FuncoesAPFAtributos_Code, P00LN7_A389FuncoesAPFAtributos_Regra, P00LN7_n389FuncoesAPFAtributos_Regra, P00LN7_A367FuncaoAPFAtributos_AtrTabelaNom, P00LN7_n367FuncaoAPFAtributos_AtrTabelaNom,
               P00LN7_A366FuncaoAPFAtributos_AtrTabelaCod, P00LN7_n366FuncaoAPFAtributos_AtrTabelaCod, P00LN7_A378FuncaoAPFAtributos_FuncaoDadosCod, P00LN7_n378FuncaoAPFAtributos_FuncaoDadosCod, P00LN7_A364FuncaoAPFAtributos_AtributosCod, P00LN7_A165FuncaoAPF_Codigo, P00LN7_A365FuncaoAPFAtributos_AtributosNom, P00LN7_n365FuncaoAPFAtributos_AtributosNom, P00LN7_A166FuncaoAPF_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV24TFFuncoesAPFAtributos_Regra_Sel ;
      private short AV50DynamicFiltersOperator1 ;
      private short AV55DynamicFiltersOperator2 ;
      private short AV60DynamicFiltersOperator3 ;
      private short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ;
      private short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ;
      private short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ;
      private short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ;
      private int AV65GXV1 ;
      private int AV10TFFuncaoAPF_Codigo ;
      private int AV11TFFuncaoAPF_Codigo_To ;
      private int AV14TFFuncaoAPFAtributos_AtributosCod ;
      private int AV15TFFuncaoAPFAtributos_AtributosCod_To ;
      private int AV18TFFuncaoAPFAtributos_FuncaoDadosCod ;
      private int AV19TFFuncaoAPFAtributos_FuncaoDadosCod_To ;
      private int AV20TFFuncaoAPFAtributos_AtrTabelaCod ;
      private int AV21TFFuncaoAPFAtributos_AtrTabelaCod_To ;
      private int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ;
      private int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ;
      private int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ;
      private int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ;
      private int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ;
      private int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ;
      private int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ;
      private int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int AV34InsertIndex ;
      private long AV43count ;
      private String AV16TFFuncaoAPFAtributos_AtributosNom ;
      private String AV17TFFuncaoAPFAtributos_AtributosNom_Sel ;
      private String AV22TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String AV23TFFuncaoAPFAtributos_AtrTabelaNom_Sel ;
      private String AV27TFFuncoesAPFAtributos_Nome ;
      private String AV28TFFuncoesAPFAtributos_Nome_Sel ;
      private String AV52FuncaoAPFAtributos_AtributosNom1 ;
      private String AV57FuncaoAPFAtributos_AtributosNom2 ;
      private String AV62FuncaoAPFAtributos_AtributosNom3 ;
      private String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ;
      private String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ;
      private String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ;
      private String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ;
      private String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ;
      private String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ;
      private String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ;
      private String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ;
      private String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ;
      private String scmdbuf ;
      private String lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ;
      private String lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ;
      private String lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ;
      private String lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ;
      private String lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ;
      private String lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String A384FuncoesAPFAtributos_Nome ;
      private bool returnInSub ;
      private bool AV53DynamicFiltersEnabled2 ;
      private bool AV58DynamicFiltersEnabled3 ;
      private bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ;
      private bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool BRKLN2 ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool BRKLN4 ;
      private bool BRKLN6 ;
      private bool BRKLN8 ;
      private bool BRKLN10 ;
      private bool BRKLN12 ;
      private String AV42OptionIndexesJson ;
      private String AV37OptionsJson ;
      private String AV40OptionsDescJson ;
      private String AV33DDOName ;
      private String AV31SearchTxt ;
      private String AV32SearchTxtTo ;
      private String AV12TFFuncaoAPF_Nome ;
      private String AV13TFFuncaoAPF_Nome_Sel ;
      private String AV25TFFuncoesAPFAtributos_Code ;
      private String AV26TFFuncoesAPFAtributos_Code_Sel ;
      private String AV29TFFuncoesAPFAtributos_Descricao ;
      private String AV30TFFuncoesAPFAtributos_Descricao_Sel ;
      private String AV49DynamicFiltersSelector1 ;
      private String AV51FuncaoAPF_Nome1 ;
      private String AV54DynamicFiltersSelector2 ;
      private String AV56FuncaoAPF_Nome2 ;
      private String AV59DynamicFiltersSelector3 ;
      private String AV61FuncaoAPF_Nome3 ;
      private String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ;
      private String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ;
      private String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ;
      private String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ;
      private String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ;
      private String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ;
      private String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ;
      private String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ;
      private String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ;
      private String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ;
      private String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ;
      private String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ;
      private String lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ;
      private String lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ;
      private String lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ;
      private String lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ;
      private String lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ;
      private String lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ;
      private String A166FuncaoAPF_Nome ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String AV35Option ;
      private IGxSession AV44Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LN2_A165FuncaoAPF_Codigo ;
      private String[] P00LN2_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN2_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN2_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN2_n384FuncoesAPFAtributos_Nome ;
      private String[] P00LN2_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN2_n383FuncoesAPFAtributos_Code ;
      private bool[] P00LN2_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN2_n389FuncoesAPFAtributos_Regra ;
      private String[] P00LN2_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN2_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] P00LN2_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN2_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN2_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN2_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN2_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] P00LN2_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN2_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN2_A166FuncaoAPF_Nome ;
      private int[] P00LN3_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] P00LN3_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN3_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN3_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN3_n384FuncoesAPFAtributos_Nome ;
      private String[] P00LN3_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN3_n383FuncoesAPFAtributos_Code ;
      private bool[] P00LN3_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN3_n389FuncoesAPFAtributos_Regra ;
      private String[] P00LN3_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN3_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] P00LN3_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN3_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN3_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN3_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN3_A165FuncaoAPF_Codigo ;
      private String[] P00LN3_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN3_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN3_A166FuncaoAPF_Nome ;
      private String[] P00LN4_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN4_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] P00LN4_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN4_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN4_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN4_n384FuncoesAPFAtributos_Nome ;
      private String[] P00LN4_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN4_n383FuncoesAPFAtributos_Code ;
      private bool[] P00LN4_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN4_n389FuncoesAPFAtributos_Regra ;
      private int[] P00LN4_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN4_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN4_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN4_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN4_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00LN4_A165FuncaoAPF_Codigo ;
      private String[] P00LN4_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN4_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN4_A166FuncaoAPF_Nome ;
      private String[] P00LN5_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN5_n383FuncoesAPFAtributos_Code ;
      private String[] P00LN5_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN5_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN5_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN5_n384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN5_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN5_n389FuncoesAPFAtributos_Regra ;
      private String[] P00LN5_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN5_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] P00LN5_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN5_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN5_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN5_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN5_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00LN5_A165FuncaoAPF_Codigo ;
      private String[] P00LN5_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN5_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN5_A166FuncaoAPF_Nome ;
      private String[] P00LN6_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN6_n384FuncoesAPFAtributos_Nome ;
      private String[] P00LN6_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN6_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN6_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN6_n383FuncoesAPFAtributos_Code ;
      private bool[] P00LN6_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN6_n389FuncoesAPFAtributos_Regra ;
      private String[] P00LN6_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN6_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] P00LN6_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN6_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN6_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN6_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN6_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00LN6_A165FuncaoAPF_Codigo ;
      private String[] P00LN6_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN6_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN6_A166FuncaoAPF_Nome ;
      private String[] P00LN7_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P00LN7_n385FuncoesAPFAtributos_Descricao ;
      private String[] P00LN7_A384FuncoesAPFAtributos_Nome ;
      private bool[] P00LN7_n384FuncoesAPFAtributos_Nome ;
      private String[] P00LN7_A383FuncoesAPFAtributos_Code ;
      private bool[] P00LN7_n383FuncoesAPFAtributos_Code ;
      private bool[] P00LN7_A389FuncoesAPFAtributos_Regra ;
      private bool[] P00LN7_n389FuncoesAPFAtributos_Regra ;
      private String[] P00LN7_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00LN7_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] P00LN7_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00LN7_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00LN7_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00LN7_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00LN7_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00LN7_A165FuncaoAPF_Codigo ;
      private String[] P00LN7_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00LN7_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] P00LN7_A166FuncaoAPF_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV46GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV47GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV48GridStateDynamicFilter ;
   }

   public class getwwfuncoesapfatributosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LN2( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [32] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_Codigo], T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T2.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LN3( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [32] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPF_Codigo], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T4.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00LN4( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [32] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T4.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Tabela_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00LN5( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [32] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Regra], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T4.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncoesAPFAtributos_Code]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00LN6( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [32] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T4.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int9[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int9[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int9[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncoesAPFAtributos_Nome]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00LN7( IGxContext context ,
                                             String AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1 ,
                                             String AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1 ,
                                             bool AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 ,
                                             String AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2 ,
                                             short AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 ,
                                             String AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2 ,
                                             String AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2 ,
                                             bool AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 ,
                                             String AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3 ,
                                             short AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 ,
                                             String AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3 ,
                                             String AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3 ,
                                             int AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo ,
                                             int AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to ,
                                             String AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel ,
                                             String AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome ,
                                             int AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod ,
                                             int AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to ,
                                             String AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel ,
                                             String AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom ,
                                             int AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod ,
                                             int AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to ,
                                             int AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod ,
                                             int AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to ,
                                             String AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel ,
                                             String AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom ,
                                             short AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel ,
                                             String AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel ,
                                             String AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code ,
                                             String AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel ,
                                             String AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome ,
                                             String AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel ,
                                             String AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [32] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T4.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1)";
            }
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWFuncoesAPFAtributosDS_1_Dynamicfiltersselector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV68WWFuncoesAPFAtributosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1)";
            }
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2)";
            }
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( AV71WWFuncoesAPFAtributosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWFuncoesAPFAtributosDS_6_Dynamicfiltersselector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV73WWFuncoesAPFAtributosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2)";
            }
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3)";
            }
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( AV76WWFuncoesAPFAtributosDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWFuncoesAPFAtributosDS_11_Dynamicfiltersselector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV78WWFuncoesAPFAtributosDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3)";
            }
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( ! (0==AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo)";
            }
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( ! (0==AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to)";
            }
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( ! (0==AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod)";
            }
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( ! (0==AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to)";
            }
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom)";
            }
         }
         else
         {
            GXv_int11[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel)";
            }
         }
         else
         {
            GXv_int11[19] = 1;
         }
         if ( ! (0==AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod)";
            }
         }
         else
         {
            GXv_int11[20] = 1;
         }
         if ( ! (0==AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to)";
            }
         }
         else
         {
            GXv_int11[21] = 1;
         }
         if ( ! (0==AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod)";
            }
         }
         else
         {
            GXv_int11[22] = 1;
         }
         if ( ! (0==AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to)";
            }
         }
         else
         {
            GXv_int11[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom)";
            }
         }
         else
         {
            GXv_int11[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel)";
            }
         }
         else
         {
            GXv_int11[25] = 1;
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV95WWFuncoesAPFAtributosDS_29_Tffuncoesapfatributos_regra_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code)";
            }
         }
         else
         {
            GXv_int11[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel)";
            }
         }
         else
         {
            GXv_int11[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome)";
            }
         }
         else
         {
            GXv_int11[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int11[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao)";
            }
         }
         else
         {
            GXv_int11[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel)";
            }
         }
         else
         {
            GXv_int11[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncoesAPFAtributos_Descricao]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LN2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
               case 1 :
                     return conditional_P00LN3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
               case 2 :
                     return conditional_P00LN4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
               case 3 :
                     return conditional_P00LN5(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
               case 4 :
                     return conditional_P00LN6(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
               case 5 :
                     return conditional_P00LN7(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LN2 ;
          prmP00LN2 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LN3 ;
          prmP00LN3 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LN4 ;
          prmP00LN4 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LN5 ;
          prmP00LN5 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LN6 ;
          prmP00LN6 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LN7 ;
          prmP00LN7 = new Object[] {
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncoesAPFAtributosDS_3_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWFuncoesAPFAtributosDS_4_Funcaoapfatributos_atributosnom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncoesAPFAtributosDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWFuncoesAPFAtributosDS_9_Funcaoapfatributos_atributosnom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV79WWFuncoesAPFAtributosDS_13_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWFuncoesAPFAtributosDS_14_Funcaoapfatributos_atributosnom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWFuncoesAPFAtributosDS_15_Tffuncaoapf_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWFuncoesAPFAtributosDS_16_Tffuncaoapf_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV83WWFuncoesAPFAtributosDS_17_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV84WWFuncoesAPFAtributosDS_18_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV85WWFuncoesAPFAtributosDS_19_Tffuncaoapfatributos_atributoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWFuncoesAPFAtributosDS_20_Tffuncaoapfatributos_atributoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWFuncoesAPFAtributosDS_21_Tffuncaoapfatributos_atributosnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWFuncoesAPFAtributosDS_22_Tffuncaoapfatributos_atributosnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWFuncoesAPFAtributosDS_23_Tffuncaoapfatributos_funcaodadoscod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWFuncoesAPFAtributosDS_24_Tffuncaoapfatributos_funcaodadoscod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91WWFuncoesAPFAtributosDS_25_Tffuncaoapfatributos_atrtabelacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWFuncoesAPFAtributosDS_26_Tffuncaoapfatributos_atrtabelacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWFuncoesAPFAtributosDS_27_Tffuncaoapfatributos_atrtabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWFuncoesAPFAtributosDS_28_Tffuncaoapfatributos_atrtabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWFuncoesAPFAtributosDS_30_Tffuncoesapfatributos_code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV97WWFuncoesAPFAtributosDS_31_Tffuncoesapfatributos_code_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV98WWFuncoesAPFAtributosDS_32_Tffuncoesapfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWFuncoesAPFAtributosDS_33_Tffuncoesapfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV100WWFuncoesAPFAtributosDS_34_Tffuncoesapfatributos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWFuncoesAPFAtributosDS_35_Tffuncoesapfatributos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN2,100,0,true,false )
             ,new CursorDef("P00LN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN3,100,0,true,false )
             ,new CursorDef("P00LN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN4,100,0,true,false )
             ,new CursorDef("P00LN5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN5,100,0,true,false )
             ,new CursorDef("P00LN6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN6,100,0,true,false )
             ,new CursorDef("P00LN7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LN7,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwfuncoesapfatributosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwfuncoesapfatributosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwfuncoesapfatributosfilterdata") )
          {
             return  ;
          }
          getwwfuncoesapfatributosfilterdata worker = new getwwfuncoesapfatributosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
