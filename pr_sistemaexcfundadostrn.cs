/*
               File: PR_SistemaExcFunDadosTrn
        Description: Exclus�o e Massa de Fun��es Dados e Fun��es Transa��es
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:21.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pr_sistemaexcfundadostrn : GXProcedure
   {
      public pr_sistemaexcfundadostrn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pr_sistemaexcfundadostrn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref int aP1_FuncaoAPF_SistemaCod ,
                           ref String aP2_Tipo ,
                           ref String aP3_Mesagem )
      {
         this.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV9FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         this.AV10Tipo = aP2_Tipo;
         this.AV11Mesagem = aP3_Mesagem;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_SistemaCod=this.AV9FuncaoAPF_SistemaCod;
         aP2_Tipo=this.AV10Tipo;
         aP3_Mesagem=this.AV11Mesagem;
      }

      public String executeUdp( ref int aP0_FuncaoAPF_Codigo ,
                                ref int aP1_FuncaoAPF_SistemaCod ,
                                ref String aP2_Tipo )
      {
         this.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV9FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         this.AV10Tipo = aP2_Tipo;
         this.AV11Mesagem = aP3_Mesagem;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_SistemaCod=this.AV9FuncaoAPF_SistemaCod;
         aP2_Tipo=this.AV10Tipo;
         aP3_Mesagem=this.AV11Mesagem;
         return AV11Mesagem ;
      }

      public void executeSubmit( ref int aP0_FuncaoAPF_Codigo ,
                                 ref int aP1_FuncaoAPF_SistemaCod ,
                                 ref String aP2_Tipo ,
                                 ref String aP3_Mesagem )
      {
         pr_sistemaexcfundadostrn objpr_sistemaexcfundadostrn;
         objpr_sistemaexcfundadostrn = new pr_sistemaexcfundadostrn();
         objpr_sistemaexcfundadostrn.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objpr_sistemaexcfundadostrn.AV9FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         objpr_sistemaexcfundadostrn.AV10Tipo = aP2_Tipo;
         objpr_sistemaexcfundadostrn.AV11Mesagem = aP3_Mesagem;
         objpr_sistemaexcfundadostrn.context.SetSubmitInitialConfig(context);
         objpr_sistemaexcfundadostrn.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpr_sistemaexcfundadostrn);
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_SistemaCod=this.AV9FuncaoAPF_SistemaCod;
         aP2_Tipo=this.AV10Tipo;
         aP3_Mesagem=this.AV11Mesagem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pr_sistemaexcfundadostrn)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV10Tipo, "DADOS") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(AV10Tipo, "TRANSACOES") == 0 )
         {
            AV12TotalRegistros = 0;
            /* Using cursor P00YI2 */
            pr_default.execute(0, new Object[] {AV8FuncaoAPF_Codigo, AV9FuncaoAPF_SistemaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A360FuncaoAPF_SistemaCod = P00YI2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = P00YI2_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = P00YI2_A165FuncaoAPF_Codigo[0];
               A411FuncaoAPFEvidencia_Data = P00YI2_A411FuncaoAPFEvidencia_Data[0];
               A406FuncaoAPFEvidencia_Codigo = P00YI2_A406FuncaoAPFEvidencia_Codigo[0];
               A360FuncaoAPF_SistemaCod = P00YI2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = P00YI2_n360FuncaoAPF_SistemaCod[0];
               /* Using cursor P00YI3 */
               pr_default.execute(1, new Object[] {A406FuncaoAPFEvidencia_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
               AV12TotalRegistros = (int)(AV12TotalRegistros+1);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Using cursor P00YI4 */
            pr_default.execute(2, new Object[] {AV8FuncaoAPF_Codigo, AV9FuncaoAPF_SistemaCod});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A366FuncaoAPFAtributos_AtrTabelaCod = P00YI4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00YI4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A393FuncaoAPFAtributos_SistemaCod = P00YI4_A393FuncaoAPFAtributos_SistemaCod[0];
               n393FuncaoAPFAtributos_SistemaCod = P00YI4_n393FuncaoAPFAtributos_SistemaCod[0];
               A165FuncaoAPF_Codigo = P00YI4_A165FuncaoAPF_Codigo[0];
               A364FuncaoAPFAtributos_AtributosCod = P00YI4_A364FuncaoAPFAtributos_AtributosCod[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = P00YI4_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00YI4_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A393FuncaoAPFAtributos_SistemaCod = P00YI4_A393FuncaoAPFAtributos_SistemaCod[0];
               n393FuncaoAPFAtributos_SistemaCod = P00YI4_n393FuncaoAPFAtributos_SistemaCod[0];
               /* Using cursor P00YI5 */
               pr_default.execute(3, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
               AV12TotalRegistros = (int)(AV12TotalRegistros+1);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Using cursor P00YI6 */
            pr_default.execute(4, new Object[] {AV8FuncaoAPF_Codigo, AV9FuncaoAPF_SistemaCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A360FuncaoAPF_SistemaCod = P00YI6_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = P00YI6_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = P00YI6_A165FuncaoAPF_Codigo[0];
               A413FuncaoAPF_Acao = P00YI6_A413FuncaoAPF_Acao[0];
               n413FuncaoAPF_Acao = P00YI6_n413FuncaoAPF_Acao[0];
               /* Using cursor P00YI7 */
               pr_default.execute(5, new Object[] {A165FuncaoAPF_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
               AV12TotalRegistros = (int)(AV12TotalRegistros+1);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
         }
         if ( AV12TotalRegistros > 0 )
         {
            AV11Mesagem = "";
            AV11Mesagem = AV11Mesagem + "<strong>Aten��o</strong></br></br>";
            AV11Mesagem = AV11Mesagem + "Total Registros Eliminados: " + StringUtil.Str( (decimal)(AV12TotalRegistros), 10, 0);
            AV11Mesagem = AV11Mesagem + "</br>";
         }
         else
         {
            AV11Mesagem = "";
            AV11Mesagem = AV11Mesagem + "<strong>Aten��o</strong></br></br>";
            AV11Mesagem = AV11Mesagem + "Nenhum Registro foi Eliminado";
            AV11Mesagem = AV11Mesagem + "</br>";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PR_SistemaExcFunDadosTrn");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00YI2_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00YI2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00YI2_A165FuncaoAPF_Codigo = new int[1] ;
         P00YI2_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00YI2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         P00YI4_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00YI4_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00YI4_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         P00YI4_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         P00YI4_A165FuncaoAPF_Codigo = new int[1] ;
         P00YI4_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00YI6_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00YI6_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00YI6_A165FuncaoAPF_Codigo = new int[1] ;
         P00YI6_A413FuncaoAPF_Acao = new bool[] {false} ;
         P00YI6_n413FuncaoAPF_Acao = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pr_sistemaexcfundadostrn__default(),
            new Object[][] {
                new Object[] {
               P00YI2_A360FuncaoAPF_SistemaCod, P00YI2_n360FuncaoAPF_SistemaCod, P00YI2_A165FuncaoAPF_Codigo, P00YI2_A411FuncaoAPFEvidencia_Data, P00YI2_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00YI4_A366FuncaoAPFAtributos_AtrTabelaCod, P00YI4_n366FuncaoAPFAtributos_AtrTabelaCod, P00YI4_A393FuncaoAPFAtributos_SistemaCod, P00YI4_n393FuncaoAPFAtributos_SistemaCod, P00YI4_A165FuncaoAPF_Codigo, P00YI4_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00YI6_A360FuncaoAPF_SistemaCod, P00YI6_n360FuncaoAPF_SistemaCod, P00YI6_A165FuncaoAPF_Codigo, P00YI6_A413FuncaoAPF_Acao, P00YI6_n413FuncaoAPF_Acao
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8FuncaoAPF_Codigo ;
      private int AV9FuncaoAPF_SistemaCod ;
      private int AV12TotalRegistros ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int A393FuncaoAPFAtributos_SistemaCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private String scmdbuf ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n393FuncaoAPFAtributos_SistemaCod ;
      private bool A413FuncaoAPF_Acao ;
      private bool n413FuncaoAPF_Acao ;
      private String AV10Tipo ;
      private String AV11Mesagem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private int aP1_FuncaoAPF_SistemaCod ;
      private String aP2_Tipo ;
      private String aP3_Mesagem ;
      private IDataStoreProvider pr_default ;
      private int[] P00YI2_A360FuncaoAPF_SistemaCod ;
      private bool[] P00YI2_n360FuncaoAPF_SistemaCod ;
      private int[] P00YI2_A165FuncaoAPF_Codigo ;
      private DateTime[] P00YI2_A411FuncaoAPFEvidencia_Data ;
      private int[] P00YI2_A406FuncaoAPFEvidencia_Codigo ;
      private int[] P00YI4_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00YI4_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00YI4_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] P00YI4_n393FuncaoAPFAtributos_SistemaCod ;
      private int[] P00YI4_A165FuncaoAPF_Codigo ;
      private int[] P00YI4_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00YI6_A360FuncaoAPF_SistemaCod ;
      private bool[] P00YI6_n360FuncaoAPF_SistemaCod ;
      private int[] P00YI6_A165FuncaoAPF_Codigo ;
      private bool[] P00YI6_A413FuncaoAPF_Acao ;
      private bool[] P00YI6_n413FuncaoAPF_Acao ;
   }

   public class pr_sistemaexcfundadostrn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YI2 ;
          prmP00YI2 = new Object[] {
          new Object[] {"@AV8FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YI3 ;
          prmP00YI3 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YI4 ;
          prmP00YI4 = new Object[] {
          new Object[] {"@AV8FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YI5 ;
          prmP00YI5 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YI6 ;
          prmP00YI6 = new Object[] {
          new Object[] {"@AV8FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YI7 ;
          prmP00YI7 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YI2", "SELECT T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_Data], T1.[FuncaoAPFEvidencia_Codigo] FROM ([FuncaoAPFEvidencia] T1 WITH (UPDLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) WHERE (T1.[FuncaoAPF_Codigo] = @AV8FuncaoAPF_Codigo) AND (T2.[FuncaoAPF_SistemaCod] = @AV9FuncaoAPF_SistemaCod) ORDER BY T1.[FuncaoAPF_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YI2,1,0,true,false )
             ,new CursorDef("P00YI3", "DELETE FROM [FuncaoAPFEvidencia]  WHERE [FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YI3)
             ,new CursorDef("P00YI4", "SELECT T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T3.[Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod, T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM (([FuncoesAPFAtributos] T1 WITH (UPDLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE (T1.[FuncaoAPF_Codigo] = @AV8FuncaoAPF_Codigo) AND (T3.[Tabela_SistemaCod] = @AV9FuncaoAPF_SistemaCod) ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YI4,100,0,true,false )
             ,new CursorDef("P00YI5", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YI5)
             ,new CursorDef("P00YI6", "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo], [FuncaoAPF_Acao] FROM [FuncoesAPF] WITH (UPDLOCK) WHERE ([FuncaoAPF_Codigo] = @AV8FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @AV9FuncaoAPF_SistemaCod) ORDER BY [FuncaoAPF_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YI6,1,0,true,true )
             ,new CursorDef("P00YI7", "DELETE FROM [FuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YI7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
