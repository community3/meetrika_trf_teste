/*
               File: GetWWContratoClausulasFilterData
        Description: Get WWContrato Clausulas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:24.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoclausulasfilterdata : GXProcedure
   {
      public getwwcontratoclausulasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoclausulasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoclausulasfilterdata objgetwwcontratoclausulasfilterdata;
         objgetwwcontratoclausulasfilterdata = new getwwcontratoclausulasfilterdata();
         objgetwwcontratoclausulasfilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontratoclausulasfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontratoclausulasfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoclausulasfilterdata.AV22OptionsJson = "" ;
         objgetwwcontratoclausulasfilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontratoclausulasfilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontratoclausulasfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoclausulasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoclausulasfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoclausulasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOCLAUSULAS_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOCLAUSULAS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContratoClausulasGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoClausulasGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContratoClausulasGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM") == 0 )
            {
               AV12TFContratoClausulas_Item = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM_SEL") == 0 )
            {
               AV13TFContratoClausulas_Item_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO") == 0 )
            {
               AV14TFContratoClausulas_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoClausulas_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV16SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV48WWContratoClausulasDS_1_Tfcontrato_numero = AV10TFContrato_Numero;
         AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV12TFContratoClausulas_Item;
         AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV13TFContratoClausulas_Item_Sel;
         AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV14TFContratoClausulas_Descricao;
         AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV15TFContratoClausulas_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                              AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                              AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                              AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                              AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                              AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                              A77Contrato_Numero ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV48WWContratoClausulasDS_1_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero), 20, "%");
         lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item), 10, "%");
         lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao), "%", "");
         /* Using cursor P00JB2 */
         pr_default.execute(0, new Object[] {lV48WWContratoClausulasDS_1_Tfcontrato_numero, AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel, lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item, AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel, lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao, AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJB2 = false;
            A74Contrato_Codigo = P00JB2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00JB2_A77Contrato_Numero[0];
            A154ContratoClausulas_Descricao = P00JB2_A154ContratoClausulas_Descricao[0];
            A153ContratoClausulas_Item = P00JB2_A153ContratoClausulas_Item[0];
            A152ContratoClausulas_Codigo = P00JB2_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00JB2_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JB2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKJB2 = false;
               A74Contrato_Codigo = P00JB2_A74Contrato_Codigo[0];
               A152ContratoClausulas_Codigo = P00JB2_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV20Option = A77Contrato_Numero;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJB2 )
            {
               BRKJB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' Routine */
         AV12TFContratoClausulas_Item = AV16SearchTxt;
         AV13TFContratoClausulas_Item_Sel = "";
         AV48WWContratoClausulasDS_1_Tfcontrato_numero = AV10TFContrato_Numero;
         AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV12TFContratoClausulas_Item;
         AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV13TFContratoClausulas_Item_Sel;
         AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV14TFContratoClausulas_Descricao;
         AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV15TFContratoClausulas_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                              AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                              AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                              AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                              AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                              AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                              A77Contrato_Numero ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV48WWContratoClausulasDS_1_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero), 20, "%");
         lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item), 10, "%");
         lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao), "%", "");
         /* Using cursor P00JB3 */
         pr_default.execute(1, new Object[] {lV48WWContratoClausulasDS_1_Tfcontrato_numero, AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel, lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item, AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel, lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao, AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJB4 = false;
            A74Contrato_Codigo = P00JB3_A74Contrato_Codigo[0];
            A153ContratoClausulas_Item = P00JB3_A153ContratoClausulas_Item[0];
            A154ContratoClausulas_Descricao = P00JB3_A154ContratoClausulas_Descricao[0];
            A77Contrato_Numero = P00JB3_A77Contrato_Numero[0];
            A152ContratoClausulas_Codigo = P00JB3_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00JB3_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00JB3_A153ContratoClausulas_Item[0], A153ContratoClausulas_Item) == 0 ) )
            {
               BRKJB4 = false;
               A152ContratoClausulas_Codigo = P00JB3_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A153ContratoClausulas_Item)) )
            {
               AV20Option = A153ContratoClausulas_Item;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJB4 )
            {
               BRKJB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' Routine */
         AV14TFContratoClausulas_Descricao = AV16SearchTxt;
         AV15TFContratoClausulas_Descricao_Sel = "";
         AV48WWContratoClausulasDS_1_Tfcontrato_numero = AV10TFContrato_Numero;
         AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV12TFContratoClausulas_Item;
         AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV13TFContratoClausulas_Item_Sel;
         AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV14TFContratoClausulas_Descricao;
         AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV15TFContratoClausulas_Descricao_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                              AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                              AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                              AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                              AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                              AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                              A77Contrato_Numero ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV48WWContratoClausulasDS_1_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero), 20, "%");
         lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item), 10, "%");
         lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao), "%", "");
         /* Using cursor P00JB4 */
         pr_default.execute(2, new Object[] {lV48WWContratoClausulasDS_1_Tfcontrato_numero, AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel, lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item, AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel, lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao, AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJB6 = false;
            A74Contrato_Codigo = P00JB4_A74Contrato_Codigo[0];
            A154ContratoClausulas_Descricao = P00JB4_A154ContratoClausulas_Descricao[0];
            A153ContratoClausulas_Item = P00JB4_A153ContratoClausulas_Item[0];
            A77Contrato_Numero = P00JB4_A77Contrato_Numero[0];
            A152ContratoClausulas_Codigo = P00JB4_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00JB4_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00JB4_A154ContratoClausulas_Descricao[0], A154ContratoClausulas_Descricao) == 0 ) )
            {
               BRKJB6 = false;
               A152ContratoClausulas_Codigo = P00JB4_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJB6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A154ContratoClausulas_Descricao)) )
            {
               AV20Option = A154ContratoClausulas_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJB6 )
            {
               BRKJB6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoClausulas_Item = "";
         AV13TFContratoClausulas_Item_Sel = "";
         AV14TFContratoClausulas_Descricao = "";
         AV15TFContratoClausulas_Descricao_Sel = "";
         AV48WWContratoClausulasDS_1_Tfcontrato_numero = "";
         AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel = "";
         AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = "";
         AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = "";
         AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = "";
         AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = "";
         scmdbuf = "";
         lV48WWContratoClausulasDS_1_Tfcontrato_numero = "";
         lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item = "";
         lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = "";
         A77Contrato_Numero = "";
         A153ContratoClausulas_Item = "";
         A154ContratoClausulas_Descricao = "";
         P00JB2_A74Contrato_Codigo = new int[1] ;
         P00JB2_A77Contrato_Numero = new String[] {""} ;
         P00JB2_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00JB2_A153ContratoClausulas_Item = new String[] {""} ;
         P00JB2_A152ContratoClausulas_Codigo = new int[1] ;
         AV20Option = "";
         P00JB3_A74Contrato_Codigo = new int[1] ;
         P00JB3_A153ContratoClausulas_Item = new String[] {""} ;
         P00JB3_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00JB3_A77Contrato_Numero = new String[] {""} ;
         P00JB3_A152ContratoClausulas_Codigo = new int[1] ;
         P00JB4_A74Contrato_Codigo = new int[1] ;
         P00JB4_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00JB4_A153ContratoClausulas_Item = new String[] {""} ;
         P00JB4_A77Contrato_Numero = new String[] {""} ;
         P00JB4_A152ContratoClausulas_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoclausulasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JB2_A74Contrato_Codigo, P00JB2_A77Contrato_Numero, P00JB2_A154ContratoClausulas_Descricao, P00JB2_A153ContratoClausulas_Item, P00JB2_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00JB3_A74Contrato_Codigo, P00JB3_A153ContratoClausulas_Item, P00JB3_A154ContratoClausulas_Descricao, P00JB3_A77Contrato_Numero, P00JB3_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00JB4_A74Contrato_Codigo, P00JB4_A154ContratoClausulas_Descricao, P00JB4_A153ContratoClausulas_Item, P00JB4_A77Contrato_Numero, P00JB4_A152ContratoClausulas_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV46GXV1 ;
      private int A74Contrato_Codigo ;
      private int A152ContratoClausulas_Codigo ;
      private long AV28count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContratoClausulas_Item ;
      private String AV13TFContratoClausulas_Item_Sel ;
      private String AV48WWContratoClausulasDS_1_Tfcontrato_numero ;
      private String AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ;
      private String AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ;
      private String AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ;
      private String scmdbuf ;
      private String lV48WWContratoClausulasDS_1_Tfcontrato_numero ;
      private String lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ;
      private String A77Contrato_Numero ;
      private String A153ContratoClausulas_Item ;
      private bool returnInSub ;
      private bool BRKJB2 ;
      private bool BRKJB4 ;
      private bool BRKJB6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A154ContratoClausulas_Descricao ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFContratoClausulas_Descricao ;
      private String AV15TFContratoClausulas_Descricao_Sel ;
      private String AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ;
      private String AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ;
      private String lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JB2_A74Contrato_Codigo ;
      private String[] P00JB2_A77Contrato_Numero ;
      private String[] P00JB2_A154ContratoClausulas_Descricao ;
      private String[] P00JB2_A153ContratoClausulas_Item ;
      private int[] P00JB2_A152ContratoClausulas_Codigo ;
      private int[] P00JB3_A74Contrato_Codigo ;
      private String[] P00JB3_A153ContratoClausulas_Item ;
      private String[] P00JB3_A154ContratoClausulas_Descricao ;
      private String[] P00JB3_A77Contrato_Numero ;
      private int[] P00JB3_A152ContratoClausulas_Codigo ;
      private int[] P00JB4_A74Contrato_Codigo ;
      private String[] P00JB4_A154ContratoClausulas_Descricao ;
      private String[] P00JB4_A153ContratoClausulas_Item ;
      private String[] P00JB4_A77Contrato_Numero ;
      private int[] P00JB4_A152ContratoClausulas_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getwwcontratoclausulasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JB2( IGxContext context ,
                                             String AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                             String AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                             String AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                             String AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                             String AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                             String AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                             String A77Contrato_Numero ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoClausulas_Descricao], T1.[ContratoClausulas_Item], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JB3( IGxContext context ,
                                             String AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                             String AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                             String AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                             String AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                             String AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                             String AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                             String A77Contrato_Numero ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoClausulas_Item], T1.[ContratoClausulas_Descricao], T2.[Contrato_Numero], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoClausulas_Item]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00JB4( IGxContext context ,
                                             String AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                             String AV48WWContratoClausulasDS_1_Tfcontrato_numero ,
                                             String AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                             String AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                             String AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                             String AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                             String A77Contrato_Numero ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [6] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoClausulas_Descricao], T1.[ContratoClausulas_Item], T2.[Contrato_Numero], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoClausulasDS_1_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV48WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoClausulas_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 1 :
                     return conditional_P00JB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 2 :
                     return conditional_P00JB4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JB2 ;
          prmP00JB2 = new Object[] {
          new Object[] {"@lV48WWContratoClausulasDS_1_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JB3 ;
          prmP00JB3 = new Object[] {
          new Object[] {"@lV48WWContratoClausulasDS_1_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JB4 ;
          prmP00JB4 = new Object[] {
          new Object[] {"@lV48WWContratoClausulasDS_1_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49WWContratoClausulasDS_2_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV50WWContratoClausulasDS_3_Tfcontratoclausulas_item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV51WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52WWContratoClausulasDS_5_Tfcontratoclausulas_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JB2,100,0,true,false )
             ,new CursorDef("P00JB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JB3,100,0,true,false )
             ,new CursorDef("P00JB4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JB4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoclausulasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoclausulasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoclausulasfilterdata") )
          {
             return  ;
          }
          getwwcontratoclausulasfilterdata worker = new getwwcontratoclausulasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
