/*
               File: Email_Instancia
        Description: Email_Instancia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:52.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class email_instancia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1665Email_Guid = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1665Email_Guid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Email_Instancia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public email_instancia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public email_instancia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_45184( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_45184e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_45184( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_45184( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_45184e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Email_Instancia", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Email_Instancia.htm");
            wb_table3_28_45184( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_45184e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_45184e( true) ;
         }
         else
         {
            wb_table1_2_45184e( false) ;
         }
      }

      protected void wb_table3_28_45184( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_45184( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_45184e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email_Instancia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email_Instancia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_45184e( true) ;
         }
         else
         {
            wb_table3_28_45184e( false) ;
         }
      }

      protected void wb_table4_34_45184( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_instancia_guid_Internalname, "Email_Instancia_Guid", "", "", lblTextblockemail_instancia_guid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Instancia_Guid_Internalname, A1666Email_Instancia_Guid.ToString(), A1666Email_Instancia_Guid.ToString(), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Instancia_Guid_Jsonclick, 0, "Attribute", "", "", "", 1, edtEmail_Instancia_Guid_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_guid_Internalname, "Email_Guid", "", "", lblTextblockemail_guid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Guid_Internalname, A1665Email_Guid.ToString(), A1665Email_Guid.ToString(), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Guid_Jsonclick, 0, "Attribute", "", "", "", 1, edtEmail_Guid_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_titulo_Internalname, "Assunto", "", "", lblTextblockemail_titulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEmail_Titulo_Internalname, A1669Email_Titulo, StringUtil.RTrim( context.localUtil.Format( A1669Email_Titulo, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Titulo_Jsonclick, 0, "Attribute", "", "", "", 1, edtEmail_Titulo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_corpo_Internalname, "Corpo", "", "", lblTextblockemail_corpo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmail_Corpo_Internalname, A1670Email_Corpo, "", "", 0, 1, edtEmail_Corpo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_instancia_titulo_Internalname, "Email_Instancia_Titulo", "", "", lblTextblockemail_instancia_titulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Instancia_Titulo_Internalname, A1673Email_Instancia_Titulo, StringUtil.RTrim( context.localUtil.Format( A1673Email_Instancia_Titulo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Instancia_Titulo_Jsonclick, 0, "Attribute", "", "", "", 1, edtEmail_Instancia_Titulo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_instancia_corpo_Internalname, "Email_Instancia_Corpo", "", "", lblTextblockemail_instancia_corpo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmail_Instancia_Corpo_Internalname, A1674Email_Instancia_Corpo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, 1, edtEmail_Instancia_Corpo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_instancia_areatrabalho_Internalname, "Trabalho", "", "", lblTextblockemail_instancia_areatrabalho_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Instancia_AreaTrabalho_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0, ",", "")), ((edtEmail_Instancia_AreaTrabalho_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1677Email_Instancia_AreaTrabalho), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1677Email_Instancia_AreaTrabalho), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Instancia_AreaTrabalho_Jsonclick, 0, "Attribute", "", "", "", 1, edtEmail_Instancia_AreaTrabalho_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_instancia_parms_Internalname, "Email_Instancia_Parms", "", "", lblTextblockemail_instancia_parms_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmail_Instancia_Parms_Internalname, A1678Email_Instancia_Parms, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, 1, edtEmail_Instancia_Parms_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "json", "HLP_Email_Instancia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_45184e( true) ;
         }
         else
         {
            wb_table4_34_45184e( false) ;
         }
      }

      protected void wb_table2_5_45184( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Email_Instancia.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_45184e( true) ;
         }
         else
         {
            wb_table2_5_45184e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtEmail_Instancia_Guid_Internalname), "") == 0 )
               {
                  A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
               }
               else
               {
                  try
                  {
                     A1666Email_Instancia_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmail_Instancia_Guid_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMAIL_INSTANCIA_GUID");
                     AnyError = 1;
                     GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               if ( StringUtil.StrCmp(cgiGet( edtEmail_Guid_Internalname), "") == 0 )
               {
                  A1665Email_Guid = (Guid)(System.Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
               }
               else
               {
                  try
                  {
                     A1665Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmail_Guid_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMAIL_GUID");
                     AnyError = 1;
                     GX_FocusControl = edtEmail_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               A1669Email_Titulo = cgiGet( edtEmail_Titulo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
               A1670Email_Corpo = cgiGet( edtEmail_Corpo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
               A1673Email_Instancia_Titulo = cgiGet( edtEmail_Instancia_Titulo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1673Email_Instancia_Titulo", A1673Email_Instancia_Titulo);
               A1674Email_Instancia_Corpo = cgiGet( edtEmail_Instancia_Corpo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1674Email_Instancia_Corpo", A1674Email_Instancia_Corpo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtEmail_Instancia_AreaTrabalho_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtEmail_Instancia_AreaTrabalho_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "EMAIL_INSTANCIA_AREATRABALHO");
                  AnyError = 1;
                  GX_FocusControl = edtEmail_Instancia_AreaTrabalho_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1677Email_Instancia_AreaTrabalho = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0)));
               }
               else
               {
                  A1677Email_Instancia_AreaTrabalho = (int)(context.localUtil.CToN( cgiGet( edtEmail_Instancia_AreaTrabalho_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0)));
               }
               A1678Email_Instancia_Parms = cgiGet( edtEmail_Instancia_Parms_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1678Email_Instancia_Parms", A1678Email_Instancia_Parms);
               /* Read saved values. */
               Z1666Email_Instancia_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( "Z1666Email_Instancia_Guid")));
               Z1673Email_Instancia_Titulo = cgiGet( "Z1673Email_Instancia_Titulo");
               Z1677Email_Instancia_AreaTrabalho = (int)(context.localUtil.CToN( cgiGet( "Z1677Email_Instancia_AreaTrabalho"), ",", "."));
               Z1665Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( "Z1665Email_Guid")));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1666Email_Instancia_Guid = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll45184( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes45184( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption450( )
      {
      }

      protected void ZM45184( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1673Email_Instancia_Titulo = T00453_A1673Email_Instancia_Titulo[0];
               Z1677Email_Instancia_AreaTrabalho = T00453_A1677Email_Instancia_AreaTrabalho[0];
               Z1665Email_Guid = (Guid)(T00453_A1665Email_Guid[0]);
            }
            else
            {
               Z1673Email_Instancia_Titulo = A1673Email_Instancia_Titulo;
               Z1677Email_Instancia_AreaTrabalho = A1677Email_Instancia_AreaTrabalho;
               Z1665Email_Guid = (Guid)(A1665Email_Guid);
            }
         }
         if ( GX_JID == -4 )
         {
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            Z1673Email_Instancia_Titulo = A1673Email_Instancia_Titulo;
            Z1674Email_Instancia_Corpo = A1674Email_Instancia_Corpo;
            Z1677Email_Instancia_AreaTrabalho = A1677Email_Instancia_AreaTrabalho;
            Z1678Email_Instancia_Parms = A1678Email_Instancia_Parms;
            Z1665Email_Guid = (Guid)(A1665Email_Guid);
            Z1669Email_Titulo = A1669Email_Titulo;
            Z1670Email_Corpo = A1670Email_Corpo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load45184( )
      {
         /* Using cursor T00455 */
         pr_default.execute(3, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound184 = 1;
            A1669Email_Titulo = T00455_A1669Email_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
            A1670Email_Corpo = T00455_A1670Email_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
            A1673Email_Instancia_Titulo = T00455_A1673Email_Instancia_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1673Email_Instancia_Titulo", A1673Email_Instancia_Titulo);
            A1674Email_Instancia_Corpo = T00455_A1674Email_Instancia_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1674Email_Instancia_Corpo", A1674Email_Instancia_Corpo);
            A1677Email_Instancia_AreaTrabalho = T00455_A1677Email_Instancia_AreaTrabalho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0)));
            A1678Email_Instancia_Parms = T00455_A1678Email_Instancia_Parms[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1678Email_Instancia_Parms", A1678Email_Instancia_Parms);
            A1665Email_Guid = (Guid)((Guid)(T00455_A1665Email_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            ZM45184( -4) ;
         }
         pr_default.close(3);
         OnLoadActions45184( ) ;
      }

      protected void OnLoadActions45184( )
      {
         if ( (System.Guid.Empty==A1666Email_Instancia_Guid) )
         {
            A1666Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
         }
      }

      protected void CheckExtendedTable45184( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( (System.Guid.Empty==A1666Email_Instancia_Guid) )
         {
            A1666Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
         }
         /* Using cursor T00454 */
         pr_default.execute(2, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Email'.", "ForeignKeyNotFound", 1, "EMAIL_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1669Email_Titulo = T00454_A1669Email_Titulo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
         A1670Email_Corpo = T00454_A1670Email_Corpo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors45184( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( Guid A1665Email_Guid )
      {
         /* Using cursor T00456 */
         pr_default.execute(4, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Email'.", "ForeignKeyNotFound", 1, "EMAIL_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1669Email_Titulo = T00456_A1669Email_Titulo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
         A1670Email_Corpo = T00456_A1670Email_Corpo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1669Email_Titulo)+"\""+","+"\""+GXUtil.EncodeJSConstant( A1670Email_Corpo)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey45184( )
      {
         /* Using cursor T00457 */
         pr_default.execute(5, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound184 = 1;
         }
         else
         {
            RcdFound184 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00453 */
         pr_default.execute(1, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM45184( 4) ;
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(T00453_A1666Email_Instancia_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
            A1673Email_Instancia_Titulo = T00453_A1673Email_Instancia_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1673Email_Instancia_Titulo", A1673Email_Instancia_Titulo);
            A1674Email_Instancia_Corpo = T00453_A1674Email_Instancia_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1674Email_Instancia_Corpo", A1674Email_Instancia_Corpo);
            A1677Email_Instancia_AreaTrabalho = T00453_A1677Email_Instancia_AreaTrabalho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0)));
            A1678Email_Instancia_Parms = T00453_A1678Email_Instancia_Parms[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1678Email_Instancia_Parms", A1678Email_Instancia_Parms);
            A1665Email_Guid = (Guid)((Guid)(T00453_A1665Email_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            sMode184 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load45184( ) ;
            if ( AnyError == 1 )
            {
               RcdFound184 = 0;
               InitializeNonKey45184( ) ;
            }
            Gx_mode = sMode184;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound184 = 0;
            InitializeNonKey45184( ) ;
            sMode184 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode184;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey45184( ) ;
         if ( RcdFound184 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound184 = 0;
         /* Using cursor T00458 */
         pr_default.execute(6, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( GuidUtil.Compare(T00458_A1666Email_Instancia_Guid[0], A1666Email_Instancia_Guid, 1) < 0 ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( GuidUtil.Compare(T00458_A1666Email_Instancia_Guid[0], A1666Email_Instancia_Guid, 1) > 0 ) ) )
            {
               A1666Email_Instancia_Guid = (Guid)((Guid)(T00458_A1666Email_Instancia_Guid[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
               RcdFound184 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound184 = 0;
         /* Using cursor T00459 */
         pr_default.execute(7, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( GuidUtil.Compare(T00459_A1666Email_Instancia_Guid[0], A1666Email_Instancia_Guid, 1) > 0 ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( GuidUtil.Compare(T00459_A1666Email_Instancia_Guid[0], A1666Email_Instancia_Guid, 1) < 0 ) ) )
            {
               A1666Email_Instancia_Guid = (Guid)((Guid)(T00459_A1666Email_Instancia_Guid[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
               RcdFound184 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey45184( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert45184( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound184 == 1 )
            {
               if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
               {
                  A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "EMAIL_INSTANCIA_GUID");
                  AnyError = 1;
                  GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update45184( ) ;
                  GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert45184( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "EMAIL_INSTANCIA_GUID");
                     AnyError = 1;
                     GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert45184( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
         {
            A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "EMAIL_INSTANCIA_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "EMAIL_INSTANCIA_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Instancia_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart45184( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd45184( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart45184( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound184 != 0 )
            {
               ScanNext45184( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd45184( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency45184( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00452 */
            pr_default.execute(0, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1673Email_Instancia_Titulo, T00452_A1673Email_Instancia_Titulo[0]) != 0 ) || ( Z1677Email_Instancia_AreaTrabalho != T00452_A1677Email_Instancia_AreaTrabalho[0] ) || ( Z1665Email_Guid != T00452_A1665Email_Guid[0] ) )
            {
               if ( StringUtil.StrCmp(Z1673Email_Instancia_Titulo, T00452_A1673Email_Instancia_Titulo[0]) != 0 )
               {
                  GXUtil.WriteLog("email_instancia:[seudo value changed for attri]"+"Email_Instancia_Titulo");
                  GXUtil.WriteLogRaw("Old: ",Z1673Email_Instancia_Titulo);
                  GXUtil.WriteLogRaw("Current: ",T00452_A1673Email_Instancia_Titulo[0]);
               }
               if ( Z1677Email_Instancia_AreaTrabalho != T00452_A1677Email_Instancia_AreaTrabalho[0] )
               {
                  GXUtil.WriteLog("email_instancia:[seudo value changed for attri]"+"Email_Instancia_AreaTrabalho");
                  GXUtil.WriteLogRaw("Old: ",Z1677Email_Instancia_AreaTrabalho);
                  GXUtil.WriteLogRaw("Current: ",T00452_A1677Email_Instancia_AreaTrabalho[0]);
               }
               if ( Z1665Email_Guid != T00452_A1665Email_Guid[0] )
               {
                  GXUtil.WriteLog("email_instancia:[seudo value changed for attri]"+"Email_Guid");
                  GXUtil.WriteLogRaw("Old: ",Z1665Email_Guid);
                  GXUtil.WriteLogRaw("Current: ",T00452_A1665Email_Guid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Email_Instancia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert45184( )
      {
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable45184( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM45184( 0) ;
            CheckOptimisticConcurrency45184( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm45184( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert45184( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004510 */
                     pr_default.execute(8, new Object[] {A1666Email_Instancia_Guid, A1673Email_Instancia_Titulo, A1674Email_Instancia_Corpo, A1677Email_Instancia_AreaTrabalho, A1678Email_Instancia_Parms, A1665Email_Guid});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption450( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load45184( ) ;
            }
            EndLevel45184( ) ;
         }
         CloseExtendedTableCursors45184( ) ;
      }

      protected void Update45184( )
      {
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable45184( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency45184( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm45184( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate45184( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004511 */
                     pr_default.execute(9, new Object[] {A1673Email_Instancia_Titulo, A1674Email_Instancia_Corpo, A1677Email_Instancia_AreaTrabalho, A1678Email_Instancia_Parms, A1665Email_Guid, A1666Email_Instancia_Guid});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate45184( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption450( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel45184( ) ;
         }
         CloseExtendedTableCursors45184( ) ;
      }

      protected void DeferredUpdate45184( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency45184( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls45184( ) ;
            AfterConfirm45184( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete45184( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004512 */
                  pr_default.execute(10, new Object[] {A1666Email_Instancia_Guid});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound184 == 0 )
                        {
                           InitAll45184( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption450( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode184 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel45184( ) ;
         Gx_mode = sMode184;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls45184( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004513 */
            pr_default.execute(11, new Object[] {A1665Email_Guid});
            A1669Email_Titulo = T004513_A1669Email_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
            A1670Email_Corpo = T004513_A1670Email_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T004514 */
            pr_default.execute(12, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Email_Instancia_Destinatarios"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel45184( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete45184( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Email_Instancia");
            if ( AnyError == 0 )
            {
               ConfirmValues450( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Email_Instancia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart45184( )
      {
         /* Using cursor T004515 */
         pr_default.execute(13);
         RcdFound184 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(T004515_A1666Email_Instancia_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext45184( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound184 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(T004515_A1666Email_Instancia_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
         }
      }

      protected void ScanEnd45184( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm45184( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert45184( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate45184( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete45184( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete45184( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate45184( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes45184( )
      {
         edtEmail_Instancia_Guid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Instancia_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Instancia_Guid_Enabled), 5, 0)));
         edtEmail_Guid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Enabled), 5, 0)));
         edtEmail_Titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Titulo_Enabled), 5, 0)));
         edtEmail_Corpo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Corpo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Corpo_Enabled), 5, 0)));
         edtEmail_Instancia_Titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Instancia_Titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Instancia_Titulo_Enabled), 5, 0)));
         edtEmail_Instancia_Corpo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Instancia_Corpo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Instancia_Corpo_Enabled), 5, 0)));
         edtEmail_Instancia_AreaTrabalho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Instancia_AreaTrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Instancia_AreaTrabalho_Enabled), 5, 0)));
         edtEmail_Instancia_Parms_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Instancia_Parms_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Instancia_Parms_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues450( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282385314");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("email_instancia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1666Email_Instancia_Guid", Z1666Email_Instancia_Guid.ToString());
         GxWebStd.gx_hidden_field( context, "Z1673Email_Instancia_Titulo", Z1673Email_Instancia_Titulo);
         GxWebStd.gx_hidden_field( context, "Z1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1677Email_Instancia_AreaTrabalho), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1665Email_Guid", Z1665Email_Guid.ToString());
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("email_instancia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Email_Instancia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Email_Instancia" ;
      }

      protected void InitializeNonKey45184( )
      {
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
         A1669Email_Titulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
         A1670Email_Corpo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
         A1673Email_Instancia_Titulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1673Email_Instancia_Titulo", A1673Email_Instancia_Titulo);
         A1674Email_Instancia_Corpo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1674Email_Instancia_Corpo", A1674Email_Instancia_Corpo);
         A1677Email_Instancia_AreaTrabalho = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1677Email_Instancia_AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0)));
         A1678Email_Instancia_Parms = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1678Email_Instancia_Parms", A1678Email_Instancia_Parms);
         Z1673Email_Instancia_Titulo = "";
         Z1677Email_Instancia_AreaTrabalho = 0;
         Z1665Email_Guid = (Guid)(System.Guid.Empty);
      }

      protected void InitAll45184( )
      {
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1666Email_Instancia_Guid", A1666Email_Instancia_Guid.ToString());
         InitializeNonKey45184( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282385318");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("email_instancia.js", "?20204282385318");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockemail_instancia_guid_Internalname = "TEXTBLOCKEMAIL_INSTANCIA_GUID";
         edtEmail_Instancia_Guid_Internalname = "EMAIL_INSTANCIA_GUID";
         lblTextblockemail_guid_Internalname = "TEXTBLOCKEMAIL_GUID";
         edtEmail_Guid_Internalname = "EMAIL_GUID";
         lblTextblockemail_titulo_Internalname = "TEXTBLOCKEMAIL_TITULO";
         edtEmail_Titulo_Internalname = "EMAIL_TITULO";
         lblTextblockemail_corpo_Internalname = "TEXTBLOCKEMAIL_CORPO";
         edtEmail_Corpo_Internalname = "EMAIL_CORPO";
         lblTextblockemail_instancia_titulo_Internalname = "TEXTBLOCKEMAIL_INSTANCIA_TITULO";
         edtEmail_Instancia_Titulo_Internalname = "EMAIL_INSTANCIA_TITULO";
         lblTextblockemail_instancia_corpo_Internalname = "TEXTBLOCKEMAIL_INSTANCIA_CORPO";
         edtEmail_Instancia_Corpo_Internalname = "EMAIL_INSTANCIA_CORPO";
         lblTextblockemail_instancia_areatrabalho_Internalname = "TEXTBLOCKEMAIL_INSTANCIA_AREATRABALHO";
         edtEmail_Instancia_AreaTrabalho_Internalname = "EMAIL_INSTANCIA_AREATRABALHO";
         lblTextblockemail_instancia_parms_Internalname = "TEXTBLOCKEMAIL_INSTANCIA_PARMS";
         edtEmail_Instancia_Parms_Internalname = "EMAIL_INSTANCIA_PARMS";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Email_Instancia";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtEmail_Instancia_Parms_Enabled = 1;
         edtEmail_Instancia_AreaTrabalho_Jsonclick = "";
         edtEmail_Instancia_AreaTrabalho_Enabled = 1;
         edtEmail_Instancia_Corpo_Enabled = 1;
         edtEmail_Instancia_Titulo_Jsonclick = "";
         edtEmail_Instancia_Titulo_Enabled = 1;
         edtEmail_Corpo_Enabled = 0;
         edtEmail_Titulo_Jsonclick = "";
         edtEmail_Titulo_Enabled = 0;
         edtEmail_Guid_Jsonclick = "";
         edtEmail_Guid_Enabled = 1;
         edtEmail_Instancia_Guid_Jsonclick = "";
         edtEmail_Instancia_Guid_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtEmail_Guid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Email_instancia_guid( Guid GX_Parm1 ,
                                              String GX_Parm2 ,
                                              String GX_Parm3 ,
                                              int GX_Parm4 ,
                                              String GX_Parm5 ,
                                              Guid GX_Parm6 )
      {
         A1666Email_Instancia_Guid = (Guid)(GX_Parm1);
         A1673Email_Instancia_Titulo = GX_Parm2;
         A1674Email_Instancia_Corpo = GX_Parm3;
         A1677Email_Instancia_AreaTrabalho = GX_Parm4;
         A1678Email_Instancia_Parms = GX_Parm5;
         A1665Email_Guid = (Guid)(GX_Parm6);
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         if ( (System.Guid.Empty==A1666Email_Instancia_Guid) )
         {
            A1666Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1669Email_Titulo = "";
            A1670Email_Corpo = "";
         }
         isValidOutput.Add(A1665Email_Guid.ToString());
         isValidOutput.Add(A1673Email_Instancia_Titulo);
         isValidOutput.Add(A1674Email_Instancia_Corpo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1677Email_Instancia_AreaTrabalho), 6, 0, ".", "")));
         isValidOutput.Add(A1678Email_Instancia_Parms);
         isValidOutput.Add(A1666Email_Instancia_Guid.ToString());
         isValidOutput.Add(A1669Email_Titulo);
         isValidOutput.Add(A1670Email_Corpo);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(Z1665Email_Guid.ToString());
         isValidOutput.Add(Z1673Email_Instancia_Titulo);
         isValidOutput.Add(Z1674Email_Instancia_Corpo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1677Email_Instancia_AreaTrabalho), 6, 0, ",", "")));
         isValidOutput.Add(Z1678Email_Instancia_Parms);
         isValidOutput.Add(Z1666Email_Instancia_Guid.ToString());
         isValidOutput.Add(Z1669Email_Titulo);
         isValidOutput.Add(Z1670Email_Corpo);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Email_guid( Guid GX_Parm1 ,
                                    String GX_Parm2 ,
                                    String GX_Parm3 )
      {
         A1665Email_Guid = (Guid)(GX_Parm1);
         A1669Email_Titulo = GX_Parm2;
         A1670Email_Corpo = GX_Parm3;
         /* Using cursor T004513 */
         pr_default.execute(11, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Email'.", "ForeignKeyNotFound", 1, "EMAIL_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Guid_Internalname;
         }
         A1669Email_Titulo = T004513_A1669Email_Titulo[0];
         A1670Email_Corpo = T004513_A1670Email_Corpo[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1669Email_Titulo = "";
            A1670Email_Corpo = "";
         }
         isValidOutput.Add(A1669Email_Titulo);
         isValidOutput.Add(A1670Email_Corpo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         Z1673Email_Instancia_Titulo = "";
         Z1665Email_Guid = (Guid)(System.Guid.Empty);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockemail_instancia_guid_Jsonclick = "";
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         lblTextblockemail_guid_Jsonclick = "";
         lblTextblockemail_titulo_Jsonclick = "";
         A1669Email_Titulo = "";
         lblTextblockemail_corpo_Jsonclick = "";
         A1670Email_Corpo = "";
         lblTextblockemail_instancia_titulo_Jsonclick = "";
         A1673Email_Instancia_Titulo = "";
         lblTextblockemail_instancia_corpo_Jsonclick = "";
         A1674Email_Instancia_Corpo = "";
         lblTextblockemail_instancia_areatrabalho_Jsonclick = "";
         lblTextblockemail_instancia_parms_Jsonclick = "";
         A1678Email_Instancia_Parms = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1674Email_Instancia_Corpo = "";
         Z1678Email_Instancia_Parms = "";
         Z1669Email_Titulo = "";
         Z1670Email_Corpo = "";
         T00455_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00455_A1669Email_Titulo = new String[] {""} ;
         T00455_A1670Email_Corpo = new String[] {""} ;
         T00455_A1673Email_Instancia_Titulo = new String[] {""} ;
         T00455_A1674Email_Instancia_Corpo = new String[] {""} ;
         T00455_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         T00455_A1678Email_Instancia_Parms = new String[] {""} ;
         T00455_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00454_A1669Email_Titulo = new String[] {""} ;
         T00454_A1670Email_Corpo = new String[] {""} ;
         T00456_A1669Email_Titulo = new String[] {""} ;
         T00456_A1670Email_Corpo = new String[] {""} ;
         T00457_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00453_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00453_A1673Email_Instancia_Titulo = new String[] {""} ;
         T00453_A1674Email_Instancia_Corpo = new String[] {""} ;
         T00453_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         T00453_A1678Email_Instancia_Parms = new String[] {""} ;
         T00453_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         sMode184 = "";
         T00458_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00459_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00452_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T00452_A1673Email_Instancia_Titulo = new String[] {""} ;
         T00452_A1674Email_Instancia_Corpo = new String[] {""} ;
         T00452_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         T00452_A1678Email_Instancia_Parms = new String[] {""} ;
         T00452_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T004513_A1669Email_Titulo = new String[] {""} ;
         T004513_A1670Email_Corpo = new String[] {""} ;
         T004514_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T004514_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         T004515_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.email_instancia__default(),
            new Object[][] {
                new Object[] {
               T00452_A1666Email_Instancia_Guid, T00452_A1673Email_Instancia_Titulo, T00452_A1674Email_Instancia_Corpo, T00452_A1677Email_Instancia_AreaTrabalho, T00452_A1678Email_Instancia_Parms, T00452_A1665Email_Guid
               }
               , new Object[] {
               T00453_A1666Email_Instancia_Guid, T00453_A1673Email_Instancia_Titulo, T00453_A1674Email_Instancia_Corpo, T00453_A1677Email_Instancia_AreaTrabalho, T00453_A1678Email_Instancia_Parms, T00453_A1665Email_Guid
               }
               , new Object[] {
               T00454_A1669Email_Titulo, T00454_A1670Email_Corpo
               }
               , new Object[] {
               T00455_A1666Email_Instancia_Guid, T00455_A1669Email_Titulo, T00455_A1670Email_Corpo, T00455_A1673Email_Instancia_Titulo, T00455_A1674Email_Instancia_Corpo, T00455_A1677Email_Instancia_AreaTrabalho, T00455_A1678Email_Instancia_Parms, T00455_A1665Email_Guid
               }
               , new Object[] {
               T00456_A1669Email_Titulo, T00456_A1670Email_Corpo
               }
               , new Object[] {
               T00457_A1666Email_Instancia_Guid
               }
               , new Object[] {
               T00458_A1666Email_Instancia_Guid
               }
               , new Object[] {
               T00459_A1666Email_Instancia_Guid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004513_A1669Email_Titulo, T004513_A1670Email_Corpo
               }
               , new Object[] {
               T004514_A1666Email_Instancia_Guid, T004514_A1667Email_Instancia_Destinatarios_Guid
               }
               , new Object[] {
               T004515_A1666Email_Instancia_Guid
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound184 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1677Email_Instancia_AreaTrabalho ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtEmail_Instancia_Guid_Enabled ;
      private int edtEmail_Guid_Enabled ;
      private int edtEmail_Titulo_Enabled ;
      private int edtEmail_Corpo_Enabled ;
      private int edtEmail_Instancia_Titulo_Enabled ;
      private int edtEmail_Instancia_Corpo_Enabled ;
      private int A1677Email_Instancia_AreaTrabalho ;
      private int edtEmail_Instancia_AreaTrabalho_Enabled ;
      private int edtEmail_Instancia_Parms_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtEmail_Instancia_Guid_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockemail_instancia_guid_Internalname ;
      private String lblTextblockemail_instancia_guid_Jsonclick ;
      private String edtEmail_Instancia_Guid_Jsonclick ;
      private String lblTextblockemail_guid_Internalname ;
      private String lblTextblockemail_guid_Jsonclick ;
      private String edtEmail_Guid_Internalname ;
      private String edtEmail_Guid_Jsonclick ;
      private String lblTextblockemail_titulo_Internalname ;
      private String lblTextblockemail_titulo_Jsonclick ;
      private String edtEmail_Titulo_Internalname ;
      private String edtEmail_Titulo_Jsonclick ;
      private String lblTextblockemail_corpo_Internalname ;
      private String lblTextblockemail_corpo_Jsonclick ;
      private String edtEmail_Corpo_Internalname ;
      private String lblTextblockemail_instancia_titulo_Internalname ;
      private String lblTextblockemail_instancia_titulo_Jsonclick ;
      private String edtEmail_Instancia_Titulo_Internalname ;
      private String edtEmail_Instancia_Titulo_Jsonclick ;
      private String lblTextblockemail_instancia_corpo_Internalname ;
      private String lblTextblockemail_instancia_corpo_Jsonclick ;
      private String edtEmail_Instancia_Corpo_Internalname ;
      private String lblTextblockemail_instancia_areatrabalho_Internalname ;
      private String lblTextblockemail_instancia_areatrabalho_Jsonclick ;
      private String edtEmail_Instancia_AreaTrabalho_Internalname ;
      private String edtEmail_Instancia_AreaTrabalho_Jsonclick ;
      private String lblTextblockemail_instancia_parms_Internalname ;
      private String lblTextblockemail_instancia_parms_Jsonclick ;
      private String edtEmail_Instancia_Parms_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode184 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private String A1670Email_Corpo ;
      private String A1674Email_Instancia_Corpo ;
      private String A1678Email_Instancia_Parms ;
      private String Z1674Email_Instancia_Corpo ;
      private String Z1678Email_Instancia_Parms ;
      private String Z1670Email_Corpo ;
      private String Z1673Email_Instancia_Titulo ;
      private String A1669Email_Titulo ;
      private String A1673Email_Instancia_Titulo ;
      private String Z1669Email_Titulo ;
      private Guid Z1666Email_Instancia_Guid ;
      private Guid Z1665Email_Guid ;
      private Guid A1665Email_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00455_A1666Email_Instancia_Guid ;
      private String[] T00455_A1669Email_Titulo ;
      private String[] T00455_A1670Email_Corpo ;
      private String[] T00455_A1673Email_Instancia_Titulo ;
      private String[] T00455_A1674Email_Instancia_Corpo ;
      private int[] T00455_A1677Email_Instancia_AreaTrabalho ;
      private String[] T00455_A1678Email_Instancia_Parms ;
      private Guid[] T00455_A1665Email_Guid ;
      private String[] T00454_A1669Email_Titulo ;
      private String[] T00454_A1670Email_Corpo ;
      private String[] T00456_A1669Email_Titulo ;
      private String[] T00456_A1670Email_Corpo ;
      private Guid[] T00457_A1666Email_Instancia_Guid ;
      private Guid[] T00453_A1666Email_Instancia_Guid ;
      private String[] T00453_A1673Email_Instancia_Titulo ;
      private String[] T00453_A1674Email_Instancia_Corpo ;
      private int[] T00453_A1677Email_Instancia_AreaTrabalho ;
      private String[] T00453_A1678Email_Instancia_Parms ;
      private Guid[] T00453_A1665Email_Guid ;
      private Guid[] T00458_A1666Email_Instancia_Guid ;
      private Guid[] T00459_A1666Email_Instancia_Guid ;
      private Guid[] T00452_A1666Email_Instancia_Guid ;
      private String[] T00452_A1673Email_Instancia_Titulo ;
      private String[] T00452_A1674Email_Instancia_Corpo ;
      private int[] T00452_A1677Email_Instancia_AreaTrabalho ;
      private String[] T00452_A1678Email_Instancia_Parms ;
      private Guid[] T00452_A1665Email_Guid ;
      private String[] T004513_A1669Email_Titulo ;
      private String[] T004513_A1670Email_Corpo ;
      private Guid[] T004514_A1666Email_Instancia_Guid ;
      private Guid[] T004514_A1667Email_Instancia_Destinatarios_Guid ;
      private Guid[] T004515_A1666Email_Instancia_Guid ;
      private GXWebForm Form ;
   }

   public class email_instancia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00455 ;
          prmT00455 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00454 ;
          prmT00454 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00456 ;
          prmT00456 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00457 ;
          prmT00457 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00453 ;
          prmT00453 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00458 ;
          prmT00458 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00459 ;
          prmT00459 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00452 ;
          prmT00452 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004510 ;
          prmT004510 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Instancia_AreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Email_Instancia_Parms",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004511 ;
          prmT004511 = new Object[] {
          new Object[] {"@Email_Instancia_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Instancia_AreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Email_Instancia_Parms",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004512 ;
          prmT004512 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004514 ;
          prmT004514 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004515 ;
          prmT004515 = new Object[] {
          } ;
          Object[] prmT004513 ;
          prmT004513 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00452", "SELECT [Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid] FROM [Email_Instancia] WITH (UPDLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00452,1,0,true,false )
             ,new CursorDef("T00453", "SELECT [Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00453,1,0,true,false )
             ,new CursorDef("T00454", "SELECT [Email_Titulo], [Email_Corpo] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00454,1,0,true,false )
             ,new CursorDef("T00455", "SELECT TM1.[Email_Instancia_Guid], T2.[Email_Titulo], T2.[Email_Corpo], TM1.[Email_Instancia_Titulo], TM1.[Email_Instancia_Corpo], TM1.[Email_Instancia_AreaTrabalho], TM1.[Email_Instancia_Parms], TM1.[Email_Guid] FROM ([Email_Instancia] TM1 WITH (NOLOCK) INNER JOIN [Email] T2 WITH (NOLOCK) ON T2.[Email_Guid] = TM1.[Email_Guid]) WHERE TM1.[Email_Instancia_Guid] = @Email_Instancia_Guid ORDER BY TM1.[Email_Instancia_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00455,100,0,true,false )
             ,new CursorDef("T00456", "SELECT [Email_Titulo], [Email_Corpo] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00456,1,0,true,false )
             ,new CursorDef("T00457", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00457,1,0,true,false )
             ,new CursorDef("T00458", "SELECT TOP 1 [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE ( [Email_Instancia_Guid] > @Email_Instancia_Guid) ORDER BY [Email_Instancia_Guid]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00458,1,0,true,true )
             ,new CursorDef("T00459", "SELECT TOP 1 [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE ( [Email_Instancia_Guid] < @Email_Instancia_Guid) ORDER BY [Email_Instancia_Guid] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00459,1,0,true,true )
             ,new CursorDef("T004510", "INSERT INTO [Email_Instancia]([Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid]) VALUES(@Email_Instancia_Guid, @Email_Instancia_Titulo, @Email_Instancia_Corpo, @Email_Instancia_AreaTrabalho, @Email_Instancia_Parms, @Email_Guid)", GxErrorMask.GX_NOMASK,prmT004510)
             ,new CursorDef("T004511", "UPDATE [Email_Instancia] SET [Email_Instancia_Titulo]=@Email_Instancia_Titulo, [Email_Instancia_Corpo]=@Email_Instancia_Corpo, [Email_Instancia_AreaTrabalho]=@Email_Instancia_AreaTrabalho, [Email_Instancia_Parms]=@Email_Instancia_Parms, [Email_Guid]=@Email_Guid  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid", GxErrorMask.GX_NOMASK,prmT004511)
             ,new CursorDef("T004512", "DELETE FROM [Email_Instancia]  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid", GxErrorMask.GX_NOMASK,prmT004512)
             ,new CursorDef("T004513", "SELECT [Email_Titulo], [Email_Corpo] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT004513,1,0,true,false )
             ,new CursorDef("T004514", "SELECT TOP 1 [Email_Instancia_Guid], [Email_Instancia_Destinatarios_Guid] FROM [Email_Instancia_Destinatarios] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT004514,1,0,true,true )
             ,new CursorDef("T004515", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) ORDER BY [Email_Instancia_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004515,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((Guid[]) buf[5])[0] = rslt.getGuid(6) ;
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((Guid[]) buf[5])[0] = rslt.getGuid(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                return;
             case 3 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((Guid[]) buf[7])[0] = rslt.getGuid(8) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                return;
             case 5 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 6 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 7 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                return;
             case 12 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
                return;
             case 13 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (Guid)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (Guid)parms[4]);
                stmt.SetParameter(6, (Guid)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
