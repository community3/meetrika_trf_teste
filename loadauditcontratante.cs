/*
               File: LoadAuditContratante
        Description: Load Audit Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:42.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontratante : GXProcedure
   {
      public loadauditcontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Contratante_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Contratante_Codigo = aP2_Contratante_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Contratante_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontratante objloadauditcontratante;
         objloadauditcontratante = new loadauditcontratante();
         objloadauditcontratante.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontratante.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontratante.AV16Contratante_Codigo = aP2_Contratante_Codigo;
         objloadauditcontratante.AV14ActualMode = aP3_ActualMode;
         objloadauditcontratante.context.SetSubmitInitialConfig(context);
         objloadauditcontratante.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontratante);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontratante)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A22 */
         pr_default.execute(0, new Object[] {AV16Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00A22_A29Contratante_Codigo[0];
            A1125Contratante_LogoNomeArq = P00A22_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = P00A22_n1125Contratante_LogoNomeArq[0];
            A1126Contratante_LogoTipoArq = P00A22_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = P00A22_n1126Contratante_LogoTipoArq[0];
            A335Contratante_PessoaCod = P00A22_A335Contratante_PessoaCod[0];
            A12Contratante_CNPJ = P00A22_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A22_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A22_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A22_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = P00A22_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = P00A22_A11Contratante_IE[0];
            A13Contratante_WebSite = P00A22_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A22_n13Contratante_WebSite[0];
            A14Contratante_Email = P00A22_A14Contratante_Email[0];
            n14Contratante_Email = P00A22_n14Contratante_Email[0];
            A31Contratante_Telefone = P00A22_A31Contratante_Telefone[0];
            A32Contratante_Ramal = P00A22_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A22_n32Contratante_Ramal[0];
            A33Contratante_Fax = P00A22_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A22_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = P00A22_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = P00A22_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = P00A22_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = P00A22_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = P00A22_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = P00A22_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = P00A22_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = P00A22_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = P00A22_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = P00A22_n16Contratante_ContaCorrente[0];
            A25Municipio_Codigo = P00A22_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A22_n25Municipio_Codigo[0];
            A26Municipio_Nome = P00A22_A26Municipio_Nome[0];
            A23Estado_UF = P00A22_A23Estado_UF[0];
            A30Contratante_Ativo = P00A22_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = P00A22_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A22_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A22_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A22_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A22_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A22_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A22_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A22_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A22_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A22_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A22_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A22_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A22_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A22_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = P00A22_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = P00A22_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = P00A22_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = P00A22_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = P00A22_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00A22_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = P00A22_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00A22_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = P00A22_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = P00A22_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = P00A22_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = P00A22_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = P00A22_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = P00A22_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = P00A22_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = P00A22_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = P00A22_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = P00A22_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = P00A22_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = P00A22_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = P00A22_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = P00A22_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = P00A22_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = P00A22_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = P00A22_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = P00A22_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = P00A22_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = P00A22_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = P00A22_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = P00A22_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = P00A22_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = P00A22_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = P00A22_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = P00A22_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = P00A22_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = P00A22_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = P00A22_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = P00A22_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = P00A22_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = P00A22_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = P00A22_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = P00A22_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = P00A22_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = P00A22_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = P00A22_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = P00A22_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = P00A22_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = P00A22_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = P00A22_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P00A22_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = P00A22_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = P00A22_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = P00A22_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = P00A22_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = P00A22_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = P00A22_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = P00A22_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = P00A22_A2090Contratante_ExibePF[0];
            A2208Contratante_Sigla = P00A22_A2208Contratante_Sigla[0];
            n2208Contratante_Sigla = P00A22_n2208Contratante_Sigla[0];
            A1124Contratante_LogoArquivo = P00A22_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = P00A22_n1124Contratante_LogoArquivo[0];
            A12Contratante_CNPJ = P00A22_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A22_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A22_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A22_n9Contratante_RazaoSocial[0];
            A26Municipio_Nome = P00A22_A26Municipio_Nome[0];
            A23Estado_UF = P00A22_A23Estado_UF[0];
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contratante";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa Jur�dica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A12Contratante_CNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RazaoSocial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Raz�o Social";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A9Contratante_RazaoSocial;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_NomeFantasia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Fantasia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A10Contratante_NomeFantasia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_IE";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Insc. Estadual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A11Contratante_IE;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_WebSite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Site";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A13Contratante_WebSite;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Email";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Email";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A14Contratante_Email;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A31Contratante_Telefone;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ramal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ramal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A32Contratante_Ramal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A33Contratante_Fax;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AgenciaNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ag�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A336Contratante_AgenciaNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AgenciaNro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A337Contratante_AgenciaNro;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_BancoNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A338Contratante_BancoNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_BancoNro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A339Contratante_BancoNro;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ContaCorrente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Conta Corrente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A16Contratante_ContaCorrente;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Municipio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Munic�pio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A26Municipio_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_UF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "UF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A23Estado_UF;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A30Contratante_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaHost";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Host SMTP";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A547Contratante_EmailSdaHost;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaUser";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A548Contratante_EmailSdaUser;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPass";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Senha";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A549Contratante_EmailSdaPass;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaKey";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Key";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A550Contratante_EmailSdaKey;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaAut";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Autentica��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPort";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Porta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaSec";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seguran�a";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSAutomatica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numera��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSAutomatica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numera��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1652Contratante_SSAutomatica);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ExisteConferencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Existe confer�ncia?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A594Contratante_ExisteConferencia);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoArquivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Logomarca";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1124Contratante_LogoArquivo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoNomeArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome de arquivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1125Contratante_LogoNomeArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoTipoArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1126Contratante_LogoTipoArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_InicioDoExpediente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Expediente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_FimDoExpediente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�s";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ServicoSSPadrao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico SS padr�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RetornaSS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Retorna SS?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1729Contratante_RetornaSS);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSStatusPlanejamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status em planejamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1732Contratante_SSStatusPlanejamento;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSPrestadoraUnica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora �nica?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrSrv";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer servi�o?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrPrz";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer prazo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrEsf";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer esfor�o?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer valores?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaNvlCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nivel dos valores?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraOS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gera OS?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1757Contratante_OSGeraOS);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTRP";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recebimento Provis�rio?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1758Contratante_OSGeraTRP);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTH";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Homologa��o?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1759Contratante_OSGeraTH);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTRD";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recebimento Definitivo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1760Contratante_OSGeraTRD);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTR";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recusa?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1761Contratante_OSGeraTR);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSHmlgComPnd";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Homologa com pend�ncias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AtivoCirculante";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo Circulante";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PassivoCirculante";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Passivo Circulante";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PatrimonioLiquido";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Patrimonio Liquido";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ReceitaBruta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Receita Bruta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_UsaOSistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usa o Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1822Contratante_UsaOSistema);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_TtlRltGerencial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Titulo relat�rio gerencial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2034Contratante_TtlRltGerencial;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PrzAoRtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Retorno da OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2084Contratante_PrzAoRtr), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PrzActRtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aceite do Retorno";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2083Contratante_PrzActRtr), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RequerOrigem";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cadastrar Origem";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2085Contratante_RequerOrigem);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SelecionaResponsavelOS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seleciona o Profissional respons�vel pela OS?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ExibePF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exibir PF Bruto e L�quido?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2090Contratante_ExibePF);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2208Contratante_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A23 */
         pr_default.execute(1, new Object[] {AV16Contratante_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1125Contratante_LogoNomeArq = P00A23_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = P00A23_n1125Contratante_LogoNomeArq[0];
            A1126Contratante_LogoTipoArq = P00A23_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = P00A23_n1126Contratante_LogoTipoArq[0];
            A29Contratante_Codigo = P00A23_A29Contratante_Codigo[0];
            A335Contratante_PessoaCod = P00A23_A335Contratante_PessoaCod[0];
            A12Contratante_CNPJ = P00A23_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A23_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A23_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A23_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = P00A23_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = P00A23_A11Contratante_IE[0];
            A13Contratante_WebSite = P00A23_A13Contratante_WebSite[0];
            n13Contratante_WebSite = P00A23_n13Contratante_WebSite[0];
            A14Contratante_Email = P00A23_A14Contratante_Email[0];
            n14Contratante_Email = P00A23_n14Contratante_Email[0];
            A31Contratante_Telefone = P00A23_A31Contratante_Telefone[0];
            A32Contratante_Ramal = P00A23_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00A23_n32Contratante_Ramal[0];
            A33Contratante_Fax = P00A23_A33Contratante_Fax[0];
            n33Contratante_Fax = P00A23_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = P00A23_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = P00A23_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = P00A23_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = P00A23_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = P00A23_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = P00A23_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = P00A23_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = P00A23_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = P00A23_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = P00A23_n16Contratante_ContaCorrente[0];
            A25Municipio_Codigo = P00A23_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00A23_n25Municipio_Codigo[0];
            A26Municipio_Nome = P00A23_A26Municipio_Nome[0];
            A23Estado_UF = P00A23_A23Estado_UF[0];
            A30Contratante_Ativo = P00A23_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = P00A23_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00A23_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P00A23_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00A23_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = P00A23_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00A23_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = P00A23_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00A23_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = P00A23_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00A23_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = P00A23_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00A23_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = P00A23_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00A23_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = P00A23_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = P00A23_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = P00A23_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = P00A23_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = P00A23_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00A23_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = P00A23_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00A23_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = P00A23_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = P00A23_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = P00A23_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = P00A23_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = P00A23_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = P00A23_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = P00A23_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = P00A23_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = P00A23_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = P00A23_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = P00A23_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = P00A23_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = P00A23_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = P00A23_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = P00A23_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = P00A23_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = P00A23_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = P00A23_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = P00A23_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = P00A23_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = P00A23_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = P00A23_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = P00A23_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = P00A23_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = P00A23_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = P00A23_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = P00A23_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = P00A23_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = P00A23_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = P00A23_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = P00A23_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = P00A23_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = P00A23_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = P00A23_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = P00A23_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = P00A23_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = P00A23_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = P00A23_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = P00A23_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = P00A23_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = P00A23_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P00A23_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = P00A23_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = P00A23_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = P00A23_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = P00A23_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = P00A23_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = P00A23_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = P00A23_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = P00A23_A2090Contratante_ExibePF[0];
            A2208Contratante_Sigla = P00A23_A2208Contratante_Sigla[0];
            n2208Contratante_Sigla = P00A23_n2208Contratante_Sigla[0];
            A1124Contratante_LogoArquivo = P00A23_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = P00A23_n1124Contratante_LogoArquivo[0];
            A12Contratante_CNPJ = P00A23_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00A23_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00A23_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00A23_n9Contratante_RazaoSocial[0];
            A26Municipio_Nome = P00A23_A26Municipio_Nome[0];
            A23Estado_UF = P00A23_A23Estado_UF[0];
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contratante";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa Jur�dica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A12Contratante_CNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RazaoSocial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Raz�o Social";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A9Contratante_RazaoSocial;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_NomeFantasia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Fantasia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A10Contratante_NomeFantasia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_IE";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Insc. Estadual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A11Contratante_IE;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_WebSite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Site";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A13Contratante_WebSite;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Email";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Email";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A14Contratante_Email;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A31Contratante_Telefone;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ramal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ramal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A32Contratante_Ramal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A33Contratante_Fax;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AgenciaNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ag�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A336Contratante_AgenciaNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AgenciaNro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A337Contratante_AgenciaNro;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_BancoNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A338Contratante_BancoNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_BancoNro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A339Contratante_BancoNro;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ContaCorrente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Conta Corrente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A16Contratante_ContaCorrente;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Municipio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Municipio_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Munic�pio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A26Municipio_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Estado_UF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "UF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A23Estado_UF;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A30Contratante_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaHost";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Host SMTP";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A547Contratante_EmailSdaHost;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaUser";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A548Contratante_EmailSdaUser;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPass";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Senha";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A549Contratante_EmailSdaPass;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaKey";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Key";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A550Contratante_EmailSdaKey;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaAut";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Autentica��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaPort";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Porta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_EmailSdaSec";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seguran�a";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSAutomatica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numera��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSAutomatica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numera��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1652Contratante_SSAutomatica);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ExisteConferencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Existe confer�ncia?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A594Contratante_ExisteConferencia);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoArquivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Logomarca";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1124Contratante_LogoArquivo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoNomeArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome de arquivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1125Contratante_LogoNomeArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_LogoTipoArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1126Contratante_LogoTipoArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_InicioDoExpediente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Expediente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_FimDoExpediente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�s";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ServicoSSPadrao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico SS padr�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RetornaSS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Retorna SS?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1729Contratante_RetornaSS);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSStatusPlanejamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status em planejamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1732Contratante_SSStatusPlanejamento;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SSPrestadoraUnica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora �nica?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrSrv";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer servi�o?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrPrz";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer prazo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrEsf";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer esfor�o?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaRqrCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Requer valores?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PropostaNvlCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nivel dos valores?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraOS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gera OS?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1757Contratante_OSGeraOS);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTRP";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recebimento Provis�rio?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1758Contratante_OSGeraTRP);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTH";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Homologa��o?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1759Contratante_OSGeraTH);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTRD";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recebimento Definitivo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1760Contratante_OSGeraTRD);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSGeraTR";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo de Recusa?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1761Contratante_OSGeraTR);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_OSHmlgComPnd";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Homologa com pend�ncias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_AtivoCirculante";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo Circulante";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PassivoCirculante";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Passivo Circulante";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PatrimonioLiquido";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Patrimonio Liquido";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ReceitaBruta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Receita Bruta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_UsaOSistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usa o Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1822Contratante_UsaOSistema);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_TtlRltGerencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Titulo relat�rio gerencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2034Contratante_TtlRltGerencial;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PrzAoRtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Retorno da OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2084Contratante_PrzAoRtr), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_PrzActRtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aceite do Retorno";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2083Contratante_PrzActRtr), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_RequerOrigem";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cadastrar Origem";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2085Contratante_RequerOrigem);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_SelecionaResponsavelOS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Seleciona o Profissional respons�vel pela OS?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_ExibePF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exibir PF Bruto e L�quido?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2090Contratante_ExibePF);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratante_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2208Contratante_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A23_A29Contratante_Codigo[0] == A29Contratante_Codigo ) )
                  {
                     A1125Contratante_LogoNomeArq = P00A23_A1125Contratante_LogoNomeArq[0];
                     n1125Contratante_LogoNomeArq = P00A23_n1125Contratante_LogoNomeArq[0];
                     A1126Contratante_LogoTipoArq = P00A23_A1126Contratante_LogoTipoArq[0];
                     n1126Contratante_LogoTipoArq = P00A23_n1126Contratante_LogoTipoArq[0];
                     A335Contratante_PessoaCod = P00A23_A335Contratante_PessoaCod[0];
                     A12Contratante_CNPJ = P00A23_A12Contratante_CNPJ[0];
                     n12Contratante_CNPJ = P00A23_n12Contratante_CNPJ[0];
                     A9Contratante_RazaoSocial = P00A23_A9Contratante_RazaoSocial[0];
                     n9Contratante_RazaoSocial = P00A23_n9Contratante_RazaoSocial[0];
                     A10Contratante_NomeFantasia = P00A23_A10Contratante_NomeFantasia[0];
                     A11Contratante_IE = P00A23_A11Contratante_IE[0];
                     A13Contratante_WebSite = P00A23_A13Contratante_WebSite[0];
                     n13Contratante_WebSite = P00A23_n13Contratante_WebSite[0];
                     A14Contratante_Email = P00A23_A14Contratante_Email[0];
                     n14Contratante_Email = P00A23_n14Contratante_Email[0];
                     A31Contratante_Telefone = P00A23_A31Contratante_Telefone[0];
                     A32Contratante_Ramal = P00A23_A32Contratante_Ramal[0];
                     n32Contratante_Ramal = P00A23_n32Contratante_Ramal[0];
                     A33Contratante_Fax = P00A23_A33Contratante_Fax[0];
                     n33Contratante_Fax = P00A23_n33Contratante_Fax[0];
                     A336Contratante_AgenciaNome = P00A23_A336Contratante_AgenciaNome[0];
                     n336Contratante_AgenciaNome = P00A23_n336Contratante_AgenciaNome[0];
                     A337Contratante_AgenciaNro = P00A23_A337Contratante_AgenciaNro[0];
                     n337Contratante_AgenciaNro = P00A23_n337Contratante_AgenciaNro[0];
                     A338Contratante_BancoNome = P00A23_A338Contratante_BancoNome[0];
                     n338Contratante_BancoNome = P00A23_n338Contratante_BancoNome[0];
                     A339Contratante_BancoNro = P00A23_A339Contratante_BancoNro[0];
                     n339Contratante_BancoNro = P00A23_n339Contratante_BancoNro[0];
                     A16Contratante_ContaCorrente = P00A23_A16Contratante_ContaCorrente[0];
                     n16Contratante_ContaCorrente = P00A23_n16Contratante_ContaCorrente[0];
                     A25Municipio_Codigo = P00A23_A25Municipio_Codigo[0];
                     n25Municipio_Codigo = P00A23_n25Municipio_Codigo[0];
                     A26Municipio_Nome = P00A23_A26Municipio_Nome[0];
                     A23Estado_UF = P00A23_A23Estado_UF[0];
                     A30Contratante_Ativo = P00A23_A30Contratante_Ativo[0];
                     A547Contratante_EmailSdaHost = P00A23_A547Contratante_EmailSdaHost[0];
                     n547Contratante_EmailSdaHost = P00A23_n547Contratante_EmailSdaHost[0];
                     A548Contratante_EmailSdaUser = P00A23_A548Contratante_EmailSdaUser[0];
                     n548Contratante_EmailSdaUser = P00A23_n548Contratante_EmailSdaUser[0];
                     A549Contratante_EmailSdaPass = P00A23_A549Contratante_EmailSdaPass[0];
                     n549Contratante_EmailSdaPass = P00A23_n549Contratante_EmailSdaPass[0];
                     A550Contratante_EmailSdaKey = P00A23_A550Contratante_EmailSdaKey[0];
                     n550Contratante_EmailSdaKey = P00A23_n550Contratante_EmailSdaKey[0];
                     A551Contratante_EmailSdaAut = P00A23_A551Contratante_EmailSdaAut[0];
                     n551Contratante_EmailSdaAut = P00A23_n551Contratante_EmailSdaAut[0];
                     A552Contratante_EmailSdaPort = P00A23_A552Contratante_EmailSdaPort[0];
                     n552Contratante_EmailSdaPort = P00A23_n552Contratante_EmailSdaPort[0];
                     A1048Contratante_EmailSdaSec = P00A23_A1048Contratante_EmailSdaSec[0];
                     n1048Contratante_EmailSdaSec = P00A23_n1048Contratante_EmailSdaSec[0];
                     A593Contratante_OSAutomatica = P00A23_A593Contratante_OSAutomatica[0];
                     A1652Contratante_SSAutomatica = P00A23_A1652Contratante_SSAutomatica[0];
                     n1652Contratante_SSAutomatica = P00A23_n1652Contratante_SSAutomatica[0];
                     A594Contratante_ExisteConferencia = P00A23_A594Contratante_ExisteConferencia[0];
                     A1448Contratante_InicioDoExpediente = P00A23_A1448Contratante_InicioDoExpediente[0];
                     n1448Contratante_InicioDoExpediente = P00A23_n1448Contratante_InicioDoExpediente[0];
                     A1192Contratante_FimDoExpediente = P00A23_A1192Contratante_FimDoExpediente[0];
                     n1192Contratante_FimDoExpediente = P00A23_n1192Contratante_FimDoExpediente[0];
                     A1727Contratante_ServicoSSPadrao = P00A23_A1727Contratante_ServicoSSPadrao[0];
                     n1727Contratante_ServicoSSPadrao = P00A23_n1727Contratante_ServicoSSPadrao[0];
                     A1729Contratante_RetornaSS = P00A23_A1729Contratante_RetornaSS[0];
                     n1729Contratante_RetornaSS = P00A23_n1729Contratante_RetornaSS[0];
                     A1732Contratante_SSStatusPlanejamento = P00A23_A1732Contratante_SSStatusPlanejamento[0];
                     n1732Contratante_SSStatusPlanejamento = P00A23_n1732Contratante_SSStatusPlanejamento[0];
                     A1733Contratante_SSPrestadoraUnica = P00A23_A1733Contratante_SSPrestadoraUnica[0];
                     n1733Contratante_SSPrestadoraUnica = P00A23_n1733Contratante_SSPrestadoraUnica[0];
                     A1738Contratante_PropostaRqrSrv = P00A23_A1738Contratante_PropostaRqrSrv[0];
                     n1738Contratante_PropostaRqrSrv = P00A23_n1738Contratante_PropostaRqrSrv[0];
                     A1739Contratante_PropostaRqrPrz = P00A23_A1739Contratante_PropostaRqrPrz[0];
                     n1739Contratante_PropostaRqrPrz = P00A23_n1739Contratante_PropostaRqrPrz[0];
                     A1740Contratante_PropostaRqrEsf = P00A23_A1740Contratante_PropostaRqrEsf[0];
                     n1740Contratante_PropostaRqrEsf = P00A23_n1740Contratante_PropostaRqrEsf[0];
                     A1741Contratante_PropostaRqrCnt = P00A23_A1741Contratante_PropostaRqrCnt[0];
                     n1741Contratante_PropostaRqrCnt = P00A23_n1741Contratante_PropostaRqrCnt[0];
                     A1742Contratante_PropostaNvlCnt = P00A23_A1742Contratante_PropostaNvlCnt[0];
                     n1742Contratante_PropostaNvlCnt = P00A23_n1742Contratante_PropostaNvlCnt[0];
                     A1757Contratante_OSGeraOS = P00A23_A1757Contratante_OSGeraOS[0];
                     n1757Contratante_OSGeraOS = P00A23_n1757Contratante_OSGeraOS[0];
                     A1758Contratante_OSGeraTRP = P00A23_A1758Contratante_OSGeraTRP[0];
                     n1758Contratante_OSGeraTRP = P00A23_n1758Contratante_OSGeraTRP[0];
                     A1759Contratante_OSGeraTH = P00A23_A1759Contratante_OSGeraTH[0];
                     n1759Contratante_OSGeraTH = P00A23_n1759Contratante_OSGeraTH[0];
                     A1760Contratante_OSGeraTRD = P00A23_A1760Contratante_OSGeraTRD[0];
                     n1760Contratante_OSGeraTRD = P00A23_n1760Contratante_OSGeraTRD[0];
                     A1761Contratante_OSGeraTR = P00A23_A1761Contratante_OSGeraTR[0];
                     n1761Contratante_OSGeraTR = P00A23_n1761Contratante_OSGeraTR[0];
                     A1803Contratante_OSHmlgComPnd = P00A23_A1803Contratante_OSHmlgComPnd[0];
                     n1803Contratante_OSHmlgComPnd = P00A23_n1803Contratante_OSHmlgComPnd[0];
                     A1805Contratante_AtivoCirculante = P00A23_A1805Contratante_AtivoCirculante[0];
                     n1805Contratante_AtivoCirculante = P00A23_n1805Contratante_AtivoCirculante[0];
                     A1806Contratante_PassivoCirculante = P00A23_A1806Contratante_PassivoCirculante[0];
                     n1806Contratante_PassivoCirculante = P00A23_n1806Contratante_PassivoCirculante[0];
                     A1807Contratante_PatrimonioLiquido = P00A23_A1807Contratante_PatrimonioLiquido[0];
                     n1807Contratante_PatrimonioLiquido = P00A23_n1807Contratante_PatrimonioLiquido[0];
                     A1808Contratante_ReceitaBruta = P00A23_A1808Contratante_ReceitaBruta[0];
                     n1808Contratante_ReceitaBruta = P00A23_n1808Contratante_ReceitaBruta[0];
                     A1822Contratante_UsaOSistema = P00A23_A1822Contratante_UsaOSistema[0];
                     n1822Contratante_UsaOSistema = P00A23_n1822Contratante_UsaOSistema[0];
                     A2034Contratante_TtlRltGerencial = P00A23_A2034Contratante_TtlRltGerencial[0];
                     n2034Contratante_TtlRltGerencial = P00A23_n2034Contratante_TtlRltGerencial[0];
                     A2084Contratante_PrzAoRtr = P00A23_A2084Contratante_PrzAoRtr[0];
                     n2084Contratante_PrzAoRtr = P00A23_n2084Contratante_PrzAoRtr[0];
                     A2083Contratante_PrzActRtr = P00A23_A2083Contratante_PrzActRtr[0];
                     n2083Contratante_PrzActRtr = P00A23_n2083Contratante_PrzActRtr[0];
                     A2085Contratante_RequerOrigem = P00A23_A2085Contratante_RequerOrigem[0];
                     n2085Contratante_RequerOrigem = P00A23_n2085Contratante_RequerOrigem[0];
                     A2089Contratante_SelecionaResponsavelOS = P00A23_A2089Contratante_SelecionaResponsavelOS[0];
                     A2090Contratante_ExibePF = P00A23_A2090Contratante_ExibePF[0];
                     A2208Contratante_Sigla = P00A23_A2208Contratante_Sigla[0];
                     n2208Contratante_Sigla = P00A23_n2208Contratante_Sigla[0];
                     A1124Contratante_LogoArquivo = P00A23_A1124Contratante_LogoArquivo[0];
                     n1124Contratante_LogoArquivo = P00A23_n1124Contratante_LogoArquivo[0];
                     A12Contratante_CNPJ = P00A23_A12Contratante_CNPJ[0];
                     n12Contratante_CNPJ = P00A23_n12Contratante_CNPJ[0];
                     A9Contratante_RazaoSocial = P00A23_A9Contratante_RazaoSocial[0];
                     n9Contratante_RazaoSocial = P00A23_n9Contratante_RazaoSocial[0];
                     A26Municipio_Nome = P00A23_A26Municipio_Nome[0];
                     A23Estado_UF = P00A23_A23Estado_UF[0];
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_CNPJ") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A12Contratante_CNPJ;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_RazaoSocial") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A9Contratante_RazaoSocial;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_NomeFantasia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A10Contratante_NomeFantasia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_IE") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A11Contratante_IE;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_WebSite") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A13Contratante_WebSite;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Email") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A14Contratante_Email;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Telefone") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A31Contratante_Telefone;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Ramal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A32Contratante_Ramal;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Fax") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A33Contratante_Fax;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_AgenciaNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A336Contratante_AgenciaNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_AgenciaNro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A337Contratante_AgenciaNro;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_BancoNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A338Contratante_BancoNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_BancoNro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A339Contratante_BancoNro;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_ContaCorrente") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A16Contratante_ContaCorrente;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Municipio_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Municipio_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A26Municipio_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Estado_UF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A23Estado_UF;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A30Contratante_Ativo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaHost") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A547Contratante_EmailSdaHost;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaUser") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A548Contratante_EmailSdaUser;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaPass") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A549Contratante_EmailSdaPass;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaKey") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A550Contratante_EmailSdaKey;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaAut") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaPort") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_EmailSdaSec") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSAutomatica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_SSAutomatica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1652Contratante_SSAutomatica);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_ExisteConferencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A594Contratante_ExisteConferencia);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_LogoArquivo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1124Contratante_LogoArquivo;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_LogoNomeArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1125Contratante_LogoNomeArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_LogoTipoArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1126Contratante_LogoTipoArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_InicioDoExpediente") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_FimDoExpediente") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_ServicoSSPadrao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_RetornaSS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1729Contratante_RetornaSS);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_SSStatusPlanejamento") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1732Contratante_SSStatusPlanejamento;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_SSPrestadoraUnica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PropostaRqrSrv") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PropostaRqrPrz") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PropostaRqrEsf") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PropostaRqrCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PropostaNvlCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSGeraOS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1757Contratante_OSGeraOS);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSGeraTRP") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1758Contratante_OSGeraTRP);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSGeraTH") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1759Contratante_OSGeraTH);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSGeraTRD") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1760Contratante_OSGeraTRD);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSGeraTR") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1761Contratante_OSGeraTR);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_OSHmlgComPnd") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_AtivoCirculante") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PassivoCirculante") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PatrimonioLiquido") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_ReceitaBruta") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_UsaOSistema") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1822Contratante_UsaOSistema);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_TtlRltGerencial") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2034Contratante_TtlRltGerencial;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PrzAoRtr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2084Contratante_PrzAoRtr), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_PrzActRtr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2083Contratante_PrzActRtr), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_RequerOrigem") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2085Contratante_RequerOrigem);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_SelecionaResponsavelOS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_ExibePF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2090Contratante_ExibePF);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratante_Sigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2208Contratante_Sigla;
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A22_A29Contratante_Codigo = new int[1] ;
         P00A22_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00A22_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00A22_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00A22_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00A22_A335Contratante_PessoaCod = new int[1] ;
         P00A22_A12Contratante_CNPJ = new String[] {""} ;
         P00A22_n12Contratante_CNPJ = new bool[] {false} ;
         P00A22_A9Contratante_RazaoSocial = new String[] {""} ;
         P00A22_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00A22_A10Contratante_NomeFantasia = new String[] {""} ;
         P00A22_A11Contratante_IE = new String[] {""} ;
         P00A22_A13Contratante_WebSite = new String[] {""} ;
         P00A22_n13Contratante_WebSite = new bool[] {false} ;
         P00A22_A14Contratante_Email = new String[] {""} ;
         P00A22_n14Contratante_Email = new bool[] {false} ;
         P00A22_A31Contratante_Telefone = new String[] {""} ;
         P00A22_A32Contratante_Ramal = new String[] {""} ;
         P00A22_n32Contratante_Ramal = new bool[] {false} ;
         P00A22_A33Contratante_Fax = new String[] {""} ;
         P00A22_n33Contratante_Fax = new bool[] {false} ;
         P00A22_A336Contratante_AgenciaNome = new String[] {""} ;
         P00A22_n336Contratante_AgenciaNome = new bool[] {false} ;
         P00A22_A337Contratante_AgenciaNro = new String[] {""} ;
         P00A22_n337Contratante_AgenciaNro = new bool[] {false} ;
         P00A22_A338Contratante_BancoNome = new String[] {""} ;
         P00A22_n338Contratante_BancoNome = new bool[] {false} ;
         P00A22_A339Contratante_BancoNro = new String[] {""} ;
         P00A22_n339Contratante_BancoNro = new bool[] {false} ;
         P00A22_A16Contratante_ContaCorrente = new String[] {""} ;
         P00A22_n16Contratante_ContaCorrente = new bool[] {false} ;
         P00A22_A25Municipio_Codigo = new int[1] ;
         P00A22_n25Municipio_Codigo = new bool[] {false} ;
         P00A22_A26Municipio_Nome = new String[] {""} ;
         P00A22_A23Estado_UF = new String[] {""} ;
         P00A22_A30Contratante_Ativo = new bool[] {false} ;
         P00A22_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00A22_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00A22_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00A22_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00A22_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00A22_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00A22_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00A22_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00A22_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A22_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A22_A552Contratante_EmailSdaPort = new short[1] ;
         P00A22_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00A22_A1048Contratante_EmailSdaSec = new short[1] ;
         P00A22_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         P00A22_A593Contratante_OSAutomatica = new bool[] {false} ;
         P00A22_A1652Contratante_SSAutomatica = new bool[] {false} ;
         P00A22_n1652Contratante_SSAutomatica = new bool[] {false} ;
         P00A22_A594Contratante_ExisteConferencia = new bool[] {false} ;
         P00A22_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00A22_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00A22_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00A22_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00A22_A1727Contratante_ServicoSSPadrao = new int[1] ;
         P00A22_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         P00A22_A1729Contratante_RetornaSS = new bool[] {false} ;
         P00A22_n1729Contratante_RetornaSS = new bool[] {false} ;
         P00A22_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         P00A22_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         P00A22_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         P00A22_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         P00A22_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00A22_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00A22_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00A22_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00A22_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00A22_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00A22_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00A22_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00A22_A1742Contratante_PropostaNvlCnt = new short[1] ;
         P00A22_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         P00A22_A1757Contratante_OSGeraOS = new bool[] {false} ;
         P00A22_n1757Contratante_OSGeraOS = new bool[] {false} ;
         P00A22_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         P00A22_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         P00A22_A1759Contratante_OSGeraTH = new bool[] {false} ;
         P00A22_n1759Contratante_OSGeraTH = new bool[] {false} ;
         P00A22_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         P00A22_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         P00A22_A1761Contratante_OSGeraTR = new bool[] {false} ;
         P00A22_n1761Contratante_OSGeraTR = new bool[] {false} ;
         P00A22_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         P00A22_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         P00A22_A1805Contratante_AtivoCirculante = new decimal[1] ;
         P00A22_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         P00A22_A1806Contratante_PassivoCirculante = new decimal[1] ;
         P00A22_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         P00A22_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         P00A22_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         P00A22_A1808Contratante_ReceitaBruta = new decimal[1] ;
         P00A22_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         P00A22_A1822Contratante_UsaOSistema = new bool[] {false} ;
         P00A22_n1822Contratante_UsaOSistema = new bool[] {false} ;
         P00A22_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         P00A22_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         P00A22_A2084Contratante_PrzAoRtr = new short[1] ;
         P00A22_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         P00A22_A2083Contratante_PrzActRtr = new short[1] ;
         P00A22_n2083Contratante_PrzActRtr = new bool[] {false} ;
         P00A22_A2085Contratante_RequerOrigem = new bool[] {false} ;
         P00A22_n2085Contratante_RequerOrigem = new bool[] {false} ;
         P00A22_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         P00A22_A2090Contratante_ExibePF = new bool[] {false} ;
         P00A22_A2208Contratante_Sigla = new String[] {""} ;
         P00A22_n2208Contratante_Sigla = new bool[] {false} ;
         P00A22_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00A22_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1125Contratante_LogoNomeArq = "";
         A1126Contratante_LogoTipoArq = "";
         A12Contratante_CNPJ = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A11Contratante_IE = "";
         A13Contratante_WebSite = "";
         A14Contratante_Email = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A33Contratante_Fax = "";
         A336Contratante_AgenciaNome = "";
         A337Contratante_AgenciaNro = "";
         A338Contratante_BancoNome = "";
         A339Contratante_BancoNro = "";
         A16Contratante_ContaCorrente = "";
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         A1732Contratante_SSStatusPlanejamento = "";
         A2034Contratante_TtlRltGerencial = "";
         A2208Contratante_Sigla = "";
         A1124Contratante_LogoArquivo = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A23_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00A23_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00A23_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00A23_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00A23_A29Contratante_Codigo = new int[1] ;
         P00A23_A335Contratante_PessoaCod = new int[1] ;
         P00A23_A12Contratante_CNPJ = new String[] {""} ;
         P00A23_n12Contratante_CNPJ = new bool[] {false} ;
         P00A23_A9Contratante_RazaoSocial = new String[] {""} ;
         P00A23_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00A23_A10Contratante_NomeFantasia = new String[] {""} ;
         P00A23_A11Contratante_IE = new String[] {""} ;
         P00A23_A13Contratante_WebSite = new String[] {""} ;
         P00A23_n13Contratante_WebSite = new bool[] {false} ;
         P00A23_A14Contratante_Email = new String[] {""} ;
         P00A23_n14Contratante_Email = new bool[] {false} ;
         P00A23_A31Contratante_Telefone = new String[] {""} ;
         P00A23_A32Contratante_Ramal = new String[] {""} ;
         P00A23_n32Contratante_Ramal = new bool[] {false} ;
         P00A23_A33Contratante_Fax = new String[] {""} ;
         P00A23_n33Contratante_Fax = new bool[] {false} ;
         P00A23_A336Contratante_AgenciaNome = new String[] {""} ;
         P00A23_n336Contratante_AgenciaNome = new bool[] {false} ;
         P00A23_A337Contratante_AgenciaNro = new String[] {""} ;
         P00A23_n337Contratante_AgenciaNro = new bool[] {false} ;
         P00A23_A338Contratante_BancoNome = new String[] {""} ;
         P00A23_n338Contratante_BancoNome = new bool[] {false} ;
         P00A23_A339Contratante_BancoNro = new String[] {""} ;
         P00A23_n339Contratante_BancoNro = new bool[] {false} ;
         P00A23_A16Contratante_ContaCorrente = new String[] {""} ;
         P00A23_n16Contratante_ContaCorrente = new bool[] {false} ;
         P00A23_A25Municipio_Codigo = new int[1] ;
         P00A23_n25Municipio_Codigo = new bool[] {false} ;
         P00A23_A26Municipio_Nome = new String[] {""} ;
         P00A23_A23Estado_UF = new String[] {""} ;
         P00A23_A30Contratante_Ativo = new bool[] {false} ;
         P00A23_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00A23_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00A23_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00A23_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00A23_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00A23_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00A23_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00A23_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00A23_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A23_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00A23_A552Contratante_EmailSdaPort = new short[1] ;
         P00A23_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00A23_A1048Contratante_EmailSdaSec = new short[1] ;
         P00A23_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         P00A23_A593Contratante_OSAutomatica = new bool[] {false} ;
         P00A23_A1652Contratante_SSAutomatica = new bool[] {false} ;
         P00A23_n1652Contratante_SSAutomatica = new bool[] {false} ;
         P00A23_A594Contratante_ExisteConferencia = new bool[] {false} ;
         P00A23_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00A23_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00A23_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00A23_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00A23_A1727Contratante_ServicoSSPadrao = new int[1] ;
         P00A23_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         P00A23_A1729Contratante_RetornaSS = new bool[] {false} ;
         P00A23_n1729Contratante_RetornaSS = new bool[] {false} ;
         P00A23_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         P00A23_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         P00A23_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         P00A23_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         P00A23_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00A23_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         P00A23_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00A23_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         P00A23_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00A23_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         P00A23_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00A23_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         P00A23_A1742Contratante_PropostaNvlCnt = new short[1] ;
         P00A23_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         P00A23_A1757Contratante_OSGeraOS = new bool[] {false} ;
         P00A23_n1757Contratante_OSGeraOS = new bool[] {false} ;
         P00A23_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         P00A23_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         P00A23_A1759Contratante_OSGeraTH = new bool[] {false} ;
         P00A23_n1759Contratante_OSGeraTH = new bool[] {false} ;
         P00A23_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         P00A23_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         P00A23_A1761Contratante_OSGeraTR = new bool[] {false} ;
         P00A23_n1761Contratante_OSGeraTR = new bool[] {false} ;
         P00A23_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         P00A23_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         P00A23_A1805Contratante_AtivoCirculante = new decimal[1] ;
         P00A23_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         P00A23_A1806Contratante_PassivoCirculante = new decimal[1] ;
         P00A23_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         P00A23_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         P00A23_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         P00A23_A1808Contratante_ReceitaBruta = new decimal[1] ;
         P00A23_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         P00A23_A1822Contratante_UsaOSistema = new bool[] {false} ;
         P00A23_n1822Contratante_UsaOSistema = new bool[] {false} ;
         P00A23_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         P00A23_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         P00A23_A2084Contratante_PrzAoRtr = new short[1] ;
         P00A23_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         P00A23_A2083Contratante_PrzActRtr = new short[1] ;
         P00A23_n2083Contratante_PrzActRtr = new bool[] {false} ;
         P00A23_A2085Contratante_RequerOrigem = new bool[] {false} ;
         P00A23_n2085Contratante_RequerOrigem = new bool[] {false} ;
         P00A23_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         P00A23_A2090Contratante_ExibePF = new bool[] {false} ;
         P00A23_A2208Contratante_Sigla = new String[] {""} ;
         P00A23_n2208Contratante_Sigla = new bool[] {false} ;
         P00A23_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00A23_n1124Contratante_LogoArquivo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontratante__default(),
            new Object[][] {
                new Object[] {
               P00A22_A29Contratante_Codigo, P00A22_A1125Contratante_LogoNomeArq, P00A22_n1125Contratante_LogoNomeArq, P00A22_A1126Contratante_LogoTipoArq, P00A22_n1126Contratante_LogoTipoArq, P00A22_A335Contratante_PessoaCod, P00A22_A12Contratante_CNPJ, P00A22_n12Contratante_CNPJ, P00A22_A9Contratante_RazaoSocial, P00A22_n9Contratante_RazaoSocial,
               P00A22_A10Contratante_NomeFantasia, P00A22_A11Contratante_IE, P00A22_A13Contratante_WebSite, P00A22_n13Contratante_WebSite, P00A22_A14Contratante_Email, P00A22_n14Contratante_Email, P00A22_A31Contratante_Telefone, P00A22_A32Contratante_Ramal, P00A22_n32Contratante_Ramal, P00A22_A33Contratante_Fax,
               P00A22_n33Contratante_Fax, P00A22_A336Contratante_AgenciaNome, P00A22_n336Contratante_AgenciaNome, P00A22_A337Contratante_AgenciaNro, P00A22_n337Contratante_AgenciaNro, P00A22_A338Contratante_BancoNome, P00A22_n338Contratante_BancoNome, P00A22_A339Contratante_BancoNro, P00A22_n339Contratante_BancoNro, P00A22_A16Contratante_ContaCorrente,
               P00A22_n16Contratante_ContaCorrente, P00A22_A25Municipio_Codigo, P00A22_n25Municipio_Codigo, P00A22_A26Municipio_Nome, P00A22_A23Estado_UF, P00A22_A30Contratante_Ativo, P00A22_A547Contratante_EmailSdaHost, P00A22_n547Contratante_EmailSdaHost, P00A22_A548Contratante_EmailSdaUser, P00A22_n548Contratante_EmailSdaUser,
               P00A22_A549Contratante_EmailSdaPass, P00A22_n549Contratante_EmailSdaPass, P00A22_A550Contratante_EmailSdaKey, P00A22_n550Contratante_EmailSdaKey, P00A22_A551Contratante_EmailSdaAut, P00A22_n551Contratante_EmailSdaAut, P00A22_A552Contratante_EmailSdaPort, P00A22_n552Contratante_EmailSdaPort, P00A22_A1048Contratante_EmailSdaSec, P00A22_n1048Contratante_EmailSdaSec,
               P00A22_A593Contratante_OSAutomatica, P00A22_A1652Contratante_SSAutomatica, P00A22_n1652Contratante_SSAutomatica, P00A22_A594Contratante_ExisteConferencia, P00A22_A1448Contratante_InicioDoExpediente, P00A22_n1448Contratante_InicioDoExpediente, P00A22_A1192Contratante_FimDoExpediente, P00A22_n1192Contratante_FimDoExpediente, P00A22_A1727Contratante_ServicoSSPadrao, P00A22_n1727Contratante_ServicoSSPadrao,
               P00A22_A1729Contratante_RetornaSS, P00A22_n1729Contratante_RetornaSS, P00A22_A1732Contratante_SSStatusPlanejamento, P00A22_n1732Contratante_SSStatusPlanejamento, P00A22_A1733Contratante_SSPrestadoraUnica, P00A22_n1733Contratante_SSPrestadoraUnica, P00A22_A1738Contratante_PropostaRqrSrv, P00A22_n1738Contratante_PropostaRqrSrv, P00A22_A1739Contratante_PropostaRqrPrz, P00A22_n1739Contratante_PropostaRqrPrz,
               P00A22_A1740Contratante_PropostaRqrEsf, P00A22_n1740Contratante_PropostaRqrEsf, P00A22_A1741Contratante_PropostaRqrCnt, P00A22_n1741Contratante_PropostaRqrCnt, P00A22_A1742Contratante_PropostaNvlCnt, P00A22_n1742Contratante_PropostaNvlCnt, P00A22_A1757Contratante_OSGeraOS, P00A22_n1757Contratante_OSGeraOS, P00A22_A1758Contratante_OSGeraTRP, P00A22_n1758Contratante_OSGeraTRP,
               P00A22_A1759Contratante_OSGeraTH, P00A22_n1759Contratante_OSGeraTH, P00A22_A1760Contratante_OSGeraTRD, P00A22_n1760Contratante_OSGeraTRD, P00A22_A1761Contratante_OSGeraTR, P00A22_n1761Contratante_OSGeraTR, P00A22_A1803Contratante_OSHmlgComPnd, P00A22_n1803Contratante_OSHmlgComPnd, P00A22_A1805Contratante_AtivoCirculante, P00A22_n1805Contratante_AtivoCirculante,
               P00A22_A1806Contratante_PassivoCirculante, P00A22_n1806Contratante_PassivoCirculante, P00A22_A1807Contratante_PatrimonioLiquido, P00A22_n1807Contratante_PatrimonioLiquido, P00A22_A1808Contratante_ReceitaBruta, P00A22_n1808Contratante_ReceitaBruta, P00A22_A1822Contratante_UsaOSistema, P00A22_n1822Contratante_UsaOSistema, P00A22_A2034Contratante_TtlRltGerencial, P00A22_n2034Contratante_TtlRltGerencial,
               P00A22_A2084Contratante_PrzAoRtr, P00A22_n2084Contratante_PrzAoRtr, P00A22_A2083Contratante_PrzActRtr, P00A22_n2083Contratante_PrzActRtr, P00A22_A2085Contratante_RequerOrigem, P00A22_n2085Contratante_RequerOrigem, P00A22_A2089Contratante_SelecionaResponsavelOS, P00A22_A2090Contratante_ExibePF, P00A22_A2208Contratante_Sigla, P00A22_n2208Contratante_Sigla,
               P00A22_A1124Contratante_LogoArquivo, P00A22_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               P00A23_A1125Contratante_LogoNomeArq, P00A23_n1125Contratante_LogoNomeArq, P00A23_A1126Contratante_LogoTipoArq, P00A23_n1126Contratante_LogoTipoArq, P00A23_A29Contratante_Codigo, P00A23_A335Contratante_PessoaCod, P00A23_A12Contratante_CNPJ, P00A23_n12Contratante_CNPJ, P00A23_A9Contratante_RazaoSocial, P00A23_n9Contratante_RazaoSocial,
               P00A23_A10Contratante_NomeFantasia, P00A23_A11Contratante_IE, P00A23_A13Contratante_WebSite, P00A23_n13Contratante_WebSite, P00A23_A14Contratante_Email, P00A23_n14Contratante_Email, P00A23_A31Contratante_Telefone, P00A23_A32Contratante_Ramal, P00A23_n32Contratante_Ramal, P00A23_A33Contratante_Fax,
               P00A23_n33Contratante_Fax, P00A23_A336Contratante_AgenciaNome, P00A23_n336Contratante_AgenciaNome, P00A23_A337Contratante_AgenciaNro, P00A23_n337Contratante_AgenciaNro, P00A23_A338Contratante_BancoNome, P00A23_n338Contratante_BancoNome, P00A23_A339Contratante_BancoNro, P00A23_n339Contratante_BancoNro, P00A23_A16Contratante_ContaCorrente,
               P00A23_n16Contratante_ContaCorrente, P00A23_A25Municipio_Codigo, P00A23_n25Municipio_Codigo, P00A23_A26Municipio_Nome, P00A23_A23Estado_UF, P00A23_A30Contratante_Ativo, P00A23_A547Contratante_EmailSdaHost, P00A23_n547Contratante_EmailSdaHost, P00A23_A548Contratante_EmailSdaUser, P00A23_n548Contratante_EmailSdaUser,
               P00A23_A549Contratante_EmailSdaPass, P00A23_n549Contratante_EmailSdaPass, P00A23_A550Contratante_EmailSdaKey, P00A23_n550Contratante_EmailSdaKey, P00A23_A551Contratante_EmailSdaAut, P00A23_n551Contratante_EmailSdaAut, P00A23_A552Contratante_EmailSdaPort, P00A23_n552Contratante_EmailSdaPort, P00A23_A1048Contratante_EmailSdaSec, P00A23_n1048Contratante_EmailSdaSec,
               P00A23_A593Contratante_OSAutomatica, P00A23_A1652Contratante_SSAutomatica, P00A23_n1652Contratante_SSAutomatica, P00A23_A594Contratante_ExisteConferencia, P00A23_A1448Contratante_InicioDoExpediente, P00A23_n1448Contratante_InicioDoExpediente, P00A23_A1192Contratante_FimDoExpediente, P00A23_n1192Contratante_FimDoExpediente, P00A23_A1727Contratante_ServicoSSPadrao, P00A23_n1727Contratante_ServicoSSPadrao,
               P00A23_A1729Contratante_RetornaSS, P00A23_n1729Contratante_RetornaSS, P00A23_A1732Contratante_SSStatusPlanejamento, P00A23_n1732Contratante_SSStatusPlanejamento, P00A23_A1733Contratante_SSPrestadoraUnica, P00A23_n1733Contratante_SSPrestadoraUnica, P00A23_A1738Contratante_PropostaRqrSrv, P00A23_n1738Contratante_PropostaRqrSrv, P00A23_A1739Contratante_PropostaRqrPrz, P00A23_n1739Contratante_PropostaRqrPrz,
               P00A23_A1740Contratante_PropostaRqrEsf, P00A23_n1740Contratante_PropostaRqrEsf, P00A23_A1741Contratante_PropostaRqrCnt, P00A23_n1741Contratante_PropostaRqrCnt, P00A23_A1742Contratante_PropostaNvlCnt, P00A23_n1742Contratante_PropostaNvlCnt, P00A23_A1757Contratante_OSGeraOS, P00A23_n1757Contratante_OSGeraOS, P00A23_A1758Contratante_OSGeraTRP, P00A23_n1758Contratante_OSGeraTRP,
               P00A23_A1759Contratante_OSGeraTH, P00A23_n1759Contratante_OSGeraTH, P00A23_A1760Contratante_OSGeraTRD, P00A23_n1760Contratante_OSGeraTRD, P00A23_A1761Contratante_OSGeraTR, P00A23_n1761Contratante_OSGeraTR, P00A23_A1803Contratante_OSHmlgComPnd, P00A23_n1803Contratante_OSHmlgComPnd, P00A23_A1805Contratante_AtivoCirculante, P00A23_n1805Contratante_AtivoCirculante,
               P00A23_A1806Contratante_PassivoCirculante, P00A23_n1806Contratante_PassivoCirculante, P00A23_A1807Contratante_PatrimonioLiquido, P00A23_n1807Contratante_PatrimonioLiquido, P00A23_A1808Contratante_ReceitaBruta, P00A23_n1808Contratante_ReceitaBruta, P00A23_A1822Contratante_UsaOSistema, P00A23_n1822Contratante_UsaOSistema, P00A23_A2034Contratante_TtlRltGerencial, P00A23_n2034Contratante_TtlRltGerencial,
               P00A23_A2084Contratante_PrzAoRtr, P00A23_n2084Contratante_PrzAoRtr, P00A23_A2083Contratante_PrzActRtr, P00A23_n2083Contratante_PrzActRtr, P00A23_A2085Contratante_RequerOrigem, P00A23_n2085Contratante_RequerOrigem, P00A23_A2089Contratante_SelecionaResponsavelOS, P00A23_A2090Contratante_ExibePF, P00A23_A2208Contratante_Sigla, P00A23_n2208Contratante_Sigla,
               P00A23_A1124Contratante_LogoArquivo, P00A23_n1124Contratante_LogoArquivo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private short A1742Contratante_PropostaNvlCnt ;
      private short A2084Contratante_PrzAoRtr ;
      private short A2083Contratante_PrzActRtr ;
      private int AV16Contratante_Codigo ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A25Municipio_Codigo ;
      private int A1727Contratante_ServicoSSPadrao ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private decimal A1805Contratante_AtivoCirculante ;
      private decimal A1806Contratante_PassivoCirculante ;
      private decimal A1807Contratante_PatrimonioLiquido ;
      private decimal A1808Contratante_ReceitaBruta ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1126Contratante_LogoTipoArq ;
      private String A9Contratante_RazaoSocial ;
      private String A10Contratante_NomeFantasia ;
      private String A11Contratante_IE ;
      private String A31Contratante_Telefone ;
      private String A32Contratante_Ramal ;
      private String A33Contratante_Fax ;
      private String A336Contratante_AgenciaNome ;
      private String A337Contratante_AgenciaNro ;
      private String A338Contratante_BancoNome ;
      private String A339Contratante_BancoNro ;
      private String A16Contratante_ContaCorrente ;
      private String A26Municipio_Nome ;
      private String A23Estado_UF ;
      private String A550Contratante_EmailSdaKey ;
      private String A1732Contratante_SSStatusPlanejamento ;
      private String A2208Contratante_Sigla ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool returnInSub ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n13Contratante_WebSite ;
      private bool n14Contratante_Email ;
      private bool n32Contratante_Ramal ;
      private bool n33Contratante_Fax ;
      private bool n336Contratante_AgenciaNome ;
      private bool n337Contratante_AgenciaNro ;
      private bool n338Contratante_BancoNome ;
      private bool n339Contratante_BancoNro ;
      private bool n16Contratante_ContaCorrente ;
      private bool n25Municipio_Codigo ;
      private bool A30Contratante_Ativo ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool A593Contratante_OSAutomatica ;
      private bool A1652Contratante_SSAutomatica ;
      private bool n1652Contratante_SSAutomatica ;
      private bool A594Contratante_ExisteConferencia ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n1727Contratante_ServicoSSPadrao ;
      private bool A1729Contratante_RetornaSS ;
      private bool n1729Contratante_RetornaSS ;
      private bool n1732Contratante_SSStatusPlanejamento ;
      private bool A1733Contratante_SSPrestadoraUnica ;
      private bool n1733Contratante_SSPrestadoraUnica ;
      private bool A1738Contratante_PropostaRqrSrv ;
      private bool n1738Contratante_PropostaRqrSrv ;
      private bool A1739Contratante_PropostaRqrPrz ;
      private bool n1739Contratante_PropostaRqrPrz ;
      private bool A1740Contratante_PropostaRqrEsf ;
      private bool n1740Contratante_PropostaRqrEsf ;
      private bool A1741Contratante_PropostaRqrCnt ;
      private bool n1741Contratante_PropostaRqrCnt ;
      private bool n1742Contratante_PropostaNvlCnt ;
      private bool A1757Contratante_OSGeraOS ;
      private bool n1757Contratante_OSGeraOS ;
      private bool A1758Contratante_OSGeraTRP ;
      private bool n1758Contratante_OSGeraTRP ;
      private bool A1759Contratante_OSGeraTH ;
      private bool n1759Contratante_OSGeraTH ;
      private bool A1760Contratante_OSGeraTRD ;
      private bool n1760Contratante_OSGeraTRD ;
      private bool A1761Contratante_OSGeraTR ;
      private bool n1761Contratante_OSGeraTR ;
      private bool A1803Contratante_OSHmlgComPnd ;
      private bool n1803Contratante_OSHmlgComPnd ;
      private bool n1805Contratante_AtivoCirculante ;
      private bool n1806Contratante_PassivoCirculante ;
      private bool n1807Contratante_PatrimonioLiquido ;
      private bool n1808Contratante_ReceitaBruta ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n2084Contratante_PrzAoRtr ;
      private bool n2083Contratante_PrzActRtr ;
      private bool A2085Contratante_RequerOrigem ;
      private bool n2085Contratante_RequerOrigem ;
      private bool A2089Contratante_SelecionaResponsavelOS ;
      private bool A2090Contratante_ExibePF ;
      private bool n2208Contratante_Sigla ;
      private bool n1124Contratante_LogoArquivo ;
      private String A12Contratante_CNPJ ;
      private String A13Contratante_WebSite ;
      private String A14Contratante_Email ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private String A2034Contratante_TtlRltGerencial ;
      private String A1124Contratante_LogoArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00A22_A29Contratante_Codigo ;
      private String[] P00A22_A1125Contratante_LogoNomeArq ;
      private bool[] P00A22_n1125Contratante_LogoNomeArq ;
      private String[] P00A22_A1126Contratante_LogoTipoArq ;
      private bool[] P00A22_n1126Contratante_LogoTipoArq ;
      private int[] P00A22_A335Contratante_PessoaCod ;
      private String[] P00A22_A12Contratante_CNPJ ;
      private bool[] P00A22_n12Contratante_CNPJ ;
      private String[] P00A22_A9Contratante_RazaoSocial ;
      private bool[] P00A22_n9Contratante_RazaoSocial ;
      private String[] P00A22_A10Contratante_NomeFantasia ;
      private String[] P00A22_A11Contratante_IE ;
      private String[] P00A22_A13Contratante_WebSite ;
      private bool[] P00A22_n13Contratante_WebSite ;
      private String[] P00A22_A14Contratante_Email ;
      private bool[] P00A22_n14Contratante_Email ;
      private String[] P00A22_A31Contratante_Telefone ;
      private String[] P00A22_A32Contratante_Ramal ;
      private bool[] P00A22_n32Contratante_Ramal ;
      private String[] P00A22_A33Contratante_Fax ;
      private bool[] P00A22_n33Contratante_Fax ;
      private String[] P00A22_A336Contratante_AgenciaNome ;
      private bool[] P00A22_n336Contratante_AgenciaNome ;
      private String[] P00A22_A337Contratante_AgenciaNro ;
      private bool[] P00A22_n337Contratante_AgenciaNro ;
      private String[] P00A22_A338Contratante_BancoNome ;
      private bool[] P00A22_n338Contratante_BancoNome ;
      private String[] P00A22_A339Contratante_BancoNro ;
      private bool[] P00A22_n339Contratante_BancoNro ;
      private String[] P00A22_A16Contratante_ContaCorrente ;
      private bool[] P00A22_n16Contratante_ContaCorrente ;
      private int[] P00A22_A25Municipio_Codigo ;
      private bool[] P00A22_n25Municipio_Codigo ;
      private String[] P00A22_A26Municipio_Nome ;
      private String[] P00A22_A23Estado_UF ;
      private bool[] P00A22_A30Contratante_Ativo ;
      private String[] P00A22_A547Contratante_EmailSdaHost ;
      private bool[] P00A22_n547Contratante_EmailSdaHost ;
      private String[] P00A22_A548Contratante_EmailSdaUser ;
      private bool[] P00A22_n548Contratante_EmailSdaUser ;
      private String[] P00A22_A549Contratante_EmailSdaPass ;
      private bool[] P00A22_n549Contratante_EmailSdaPass ;
      private String[] P00A22_A550Contratante_EmailSdaKey ;
      private bool[] P00A22_n550Contratante_EmailSdaKey ;
      private bool[] P00A22_A551Contratante_EmailSdaAut ;
      private bool[] P00A22_n551Contratante_EmailSdaAut ;
      private short[] P00A22_A552Contratante_EmailSdaPort ;
      private bool[] P00A22_n552Contratante_EmailSdaPort ;
      private short[] P00A22_A1048Contratante_EmailSdaSec ;
      private bool[] P00A22_n1048Contratante_EmailSdaSec ;
      private bool[] P00A22_A593Contratante_OSAutomatica ;
      private bool[] P00A22_A1652Contratante_SSAutomatica ;
      private bool[] P00A22_n1652Contratante_SSAutomatica ;
      private bool[] P00A22_A594Contratante_ExisteConferencia ;
      private DateTime[] P00A22_A1448Contratante_InicioDoExpediente ;
      private bool[] P00A22_n1448Contratante_InicioDoExpediente ;
      private DateTime[] P00A22_A1192Contratante_FimDoExpediente ;
      private bool[] P00A22_n1192Contratante_FimDoExpediente ;
      private int[] P00A22_A1727Contratante_ServicoSSPadrao ;
      private bool[] P00A22_n1727Contratante_ServicoSSPadrao ;
      private bool[] P00A22_A1729Contratante_RetornaSS ;
      private bool[] P00A22_n1729Contratante_RetornaSS ;
      private String[] P00A22_A1732Contratante_SSStatusPlanejamento ;
      private bool[] P00A22_n1732Contratante_SSStatusPlanejamento ;
      private bool[] P00A22_A1733Contratante_SSPrestadoraUnica ;
      private bool[] P00A22_n1733Contratante_SSPrestadoraUnica ;
      private bool[] P00A22_A1738Contratante_PropostaRqrSrv ;
      private bool[] P00A22_n1738Contratante_PropostaRqrSrv ;
      private bool[] P00A22_A1739Contratante_PropostaRqrPrz ;
      private bool[] P00A22_n1739Contratante_PropostaRqrPrz ;
      private bool[] P00A22_A1740Contratante_PropostaRqrEsf ;
      private bool[] P00A22_n1740Contratante_PropostaRqrEsf ;
      private bool[] P00A22_A1741Contratante_PropostaRqrCnt ;
      private bool[] P00A22_n1741Contratante_PropostaRqrCnt ;
      private short[] P00A22_A1742Contratante_PropostaNvlCnt ;
      private bool[] P00A22_n1742Contratante_PropostaNvlCnt ;
      private bool[] P00A22_A1757Contratante_OSGeraOS ;
      private bool[] P00A22_n1757Contratante_OSGeraOS ;
      private bool[] P00A22_A1758Contratante_OSGeraTRP ;
      private bool[] P00A22_n1758Contratante_OSGeraTRP ;
      private bool[] P00A22_A1759Contratante_OSGeraTH ;
      private bool[] P00A22_n1759Contratante_OSGeraTH ;
      private bool[] P00A22_A1760Contratante_OSGeraTRD ;
      private bool[] P00A22_n1760Contratante_OSGeraTRD ;
      private bool[] P00A22_A1761Contratante_OSGeraTR ;
      private bool[] P00A22_n1761Contratante_OSGeraTR ;
      private bool[] P00A22_A1803Contratante_OSHmlgComPnd ;
      private bool[] P00A22_n1803Contratante_OSHmlgComPnd ;
      private decimal[] P00A22_A1805Contratante_AtivoCirculante ;
      private bool[] P00A22_n1805Contratante_AtivoCirculante ;
      private decimal[] P00A22_A1806Contratante_PassivoCirculante ;
      private bool[] P00A22_n1806Contratante_PassivoCirculante ;
      private decimal[] P00A22_A1807Contratante_PatrimonioLiquido ;
      private bool[] P00A22_n1807Contratante_PatrimonioLiquido ;
      private decimal[] P00A22_A1808Contratante_ReceitaBruta ;
      private bool[] P00A22_n1808Contratante_ReceitaBruta ;
      private bool[] P00A22_A1822Contratante_UsaOSistema ;
      private bool[] P00A22_n1822Contratante_UsaOSistema ;
      private String[] P00A22_A2034Contratante_TtlRltGerencial ;
      private bool[] P00A22_n2034Contratante_TtlRltGerencial ;
      private short[] P00A22_A2084Contratante_PrzAoRtr ;
      private bool[] P00A22_n2084Contratante_PrzAoRtr ;
      private short[] P00A22_A2083Contratante_PrzActRtr ;
      private bool[] P00A22_n2083Contratante_PrzActRtr ;
      private bool[] P00A22_A2085Contratante_RequerOrigem ;
      private bool[] P00A22_n2085Contratante_RequerOrigem ;
      private bool[] P00A22_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] P00A22_A2090Contratante_ExibePF ;
      private String[] P00A22_A2208Contratante_Sigla ;
      private bool[] P00A22_n2208Contratante_Sigla ;
      private String[] P00A22_A1124Contratante_LogoArquivo ;
      private bool[] P00A22_n1124Contratante_LogoArquivo ;
      private String[] P00A23_A1125Contratante_LogoNomeArq ;
      private bool[] P00A23_n1125Contratante_LogoNomeArq ;
      private String[] P00A23_A1126Contratante_LogoTipoArq ;
      private bool[] P00A23_n1126Contratante_LogoTipoArq ;
      private int[] P00A23_A29Contratante_Codigo ;
      private int[] P00A23_A335Contratante_PessoaCod ;
      private String[] P00A23_A12Contratante_CNPJ ;
      private bool[] P00A23_n12Contratante_CNPJ ;
      private String[] P00A23_A9Contratante_RazaoSocial ;
      private bool[] P00A23_n9Contratante_RazaoSocial ;
      private String[] P00A23_A10Contratante_NomeFantasia ;
      private String[] P00A23_A11Contratante_IE ;
      private String[] P00A23_A13Contratante_WebSite ;
      private bool[] P00A23_n13Contratante_WebSite ;
      private String[] P00A23_A14Contratante_Email ;
      private bool[] P00A23_n14Contratante_Email ;
      private String[] P00A23_A31Contratante_Telefone ;
      private String[] P00A23_A32Contratante_Ramal ;
      private bool[] P00A23_n32Contratante_Ramal ;
      private String[] P00A23_A33Contratante_Fax ;
      private bool[] P00A23_n33Contratante_Fax ;
      private String[] P00A23_A336Contratante_AgenciaNome ;
      private bool[] P00A23_n336Contratante_AgenciaNome ;
      private String[] P00A23_A337Contratante_AgenciaNro ;
      private bool[] P00A23_n337Contratante_AgenciaNro ;
      private String[] P00A23_A338Contratante_BancoNome ;
      private bool[] P00A23_n338Contratante_BancoNome ;
      private String[] P00A23_A339Contratante_BancoNro ;
      private bool[] P00A23_n339Contratante_BancoNro ;
      private String[] P00A23_A16Contratante_ContaCorrente ;
      private bool[] P00A23_n16Contratante_ContaCorrente ;
      private int[] P00A23_A25Municipio_Codigo ;
      private bool[] P00A23_n25Municipio_Codigo ;
      private String[] P00A23_A26Municipio_Nome ;
      private String[] P00A23_A23Estado_UF ;
      private bool[] P00A23_A30Contratante_Ativo ;
      private String[] P00A23_A547Contratante_EmailSdaHost ;
      private bool[] P00A23_n547Contratante_EmailSdaHost ;
      private String[] P00A23_A548Contratante_EmailSdaUser ;
      private bool[] P00A23_n548Contratante_EmailSdaUser ;
      private String[] P00A23_A549Contratante_EmailSdaPass ;
      private bool[] P00A23_n549Contratante_EmailSdaPass ;
      private String[] P00A23_A550Contratante_EmailSdaKey ;
      private bool[] P00A23_n550Contratante_EmailSdaKey ;
      private bool[] P00A23_A551Contratante_EmailSdaAut ;
      private bool[] P00A23_n551Contratante_EmailSdaAut ;
      private short[] P00A23_A552Contratante_EmailSdaPort ;
      private bool[] P00A23_n552Contratante_EmailSdaPort ;
      private short[] P00A23_A1048Contratante_EmailSdaSec ;
      private bool[] P00A23_n1048Contratante_EmailSdaSec ;
      private bool[] P00A23_A593Contratante_OSAutomatica ;
      private bool[] P00A23_A1652Contratante_SSAutomatica ;
      private bool[] P00A23_n1652Contratante_SSAutomatica ;
      private bool[] P00A23_A594Contratante_ExisteConferencia ;
      private DateTime[] P00A23_A1448Contratante_InicioDoExpediente ;
      private bool[] P00A23_n1448Contratante_InicioDoExpediente ;
      private DateTime[] P00A23_A1192Contratante_FimDoExpediente ;
      private bool[] P00A23_n1192Contratante_FimDoExpediente ;
      private int[] P00A23_A1727Contratante_ServicoSSPadrao ;
      private bool[] P00A23_n1727Contratante_ServicoSSPadrao ;
      private bool[] P00A23_A1729Contratante_RetornaSS ;
      private bool[] P00A23_n1729Contratante_RetornaSS ;
      private String[] P00A23_A1732Contratante_SSStatusPlanejamento ;
      private bool[] P00A23_n1732Contratante_SSStatusPlanejamento ;
      private bool[] P00A23_A1733Contratante_SSPrestadoraUnica ;
      private bool[] P00A23_n1733Contratante_SSPrestadoraUnica ;
      private bool[] P00A23_A1738Contratante_PropostaRqrSrv ;
      private bool[] P00A23_n1738Contratante_PropostaRqrSrv ;
      private bool[] P00A23_A1739Contratante_PropostaRqrPrz ;
      private bool[] P00A23_n1739Contratante_PropostaRqrPrz ;
      private bool[] P00A23_A1740Contratante_PropostaRqrEsf ;
      private bool[] P00A23_n1740Contratante_PropostaRqrEsf ;
      private bool[] P00A23_A1741Contratante_PropostaRqrCnt ;
      private bool[] P00A23_n1741Contratante_PropostaRqrCnt ;
      private short[] P00A23_A1742Contratante_PropostaNvlCnt ;
      private bool[] P00A23_n1742Contratante_PropostaNvlCnt ;
      private bool[] P00A23_A1757Contratante_OSGeraOS ;
      private bool[] P00A23_n1757Contratante_OSGeraOS ;
      private bool[] P00A23_A1758Contratante_OSGeraTRP ;
      private bool[] P00A23_n1758Contratante_OSGeraTRP ;
      private bool[] P00A23_A1759Contratante_OSGeraTH ;
      private bool[] P00A23_n1759Contratante_OSGeraTH ;
      private bool[] P00A23_A1760Contratante_OSGeraTRD ;
      private bool[] P00A23_n1760Contratante_OSGeraTRD ;
      private bool[] P00A23_A1761Contratante_OSGeraTR ;
      private bool[] P00A23_n1761Contratante_OSGeraTR ;
      private bool[] P00A23_A1803Contratante_OSHmlgComPnd ;
      private bool[] P00A23_n1803Contratante_OSHmlgComPnd ;
      private decimal[] P00A23_A1805Contratante_AtivoCirculante ;
      private bool[] P00A23_n1805Contratante_AtivoCirculante ;
      private decimal[] P00A23_A1806Contratante_PassivoCirculante ;
      private bool[] P00A23_n1806Contratante_PassivoCirculante ;
      private decimal[] P00A23_A1807Contratante_PatrimonioLiquido ;
      private bool[] P00A23_n1807Contratante_PatrimonioLiquido ;
      private decimal[] P00A23_A1808Contratante_ReceitaBruta ;
      private bool[] P00A23_n1808Contratante_ReceitaBruta ;
      private bool[] P00A23_A1822Contratante_UsaOSistema ;
      private bool[] P00A23_n1822Contratante_UsaOSistema ;
      private String[] P00A23_A2034Contratante_TtlRltGerencial ;
      private bool[] P00A23_n2034Contratante_TtlRltGerencial ;
      private short[] P00A23_A2084Contratante_PrzAoRtr ;
      private bool[] P00A23_n2084Contratante_PrzAoRtr ;
      private short[] P00A23_A2083Contratante_PrzActRtr ;
      private bool[] P00A23_n2083Contratante_PrzActRtr ;
      private bool[] P00A23_A2085Contratante_RequerOrigem ;
      private bool[] P00A23_n2085Contratante_RequerOrigem ;
      private bool[] P00A23_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] P00A23_A2090Contratante_ExibePF ;
      private String[] P00A23_A2208Contratante_Sigla ;
      private bool[] P00A23_n2208Contratante_Sigla ;
      private String[] P00A23_A1124Contratante_LogoArquivo ;
      private bool[] P00A23_n1124Contratante_LogoArquivo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditcontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A22 ;
          prmP00A22 = new Object[] {
          new Object[] {"@AV16Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A22 ;
          cmdBufferP00A22=" SELECT T1.[Contratante_Codigo], T1.[Contratante_LogoNomeArq], T1.[Contratante_LogoTipoArq], T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T2.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[Contratante_NomeFantasia], T1.[Contratante_IE], T1.[Contratante_WebSite], T1.[Contratante_Email], T1.[Contratante_Telefone], T1.[Contratante_Ramal], T1.[Contratante_Fax], T1.[Contratante_AgenciaNome], T1.[Contratante_AgenciaNro], T1.[Contratante_BancoNome], T1.[Contratante_BancoNro], T1.[Contratante_ContaCorrente], T1.[Municipio_Codigo], T3.[Municipio_Nome], T3.[Estado_UF], T1.[Contratante_Ativo], T1.[Contratante_EmailSdaHost], T1.[Contratante_EmailSdaUser], T1.[Contratante_EmailSdaPass], T1.[Contratante_EmailSdaKey], T1.[Contratante_EmailSdaAut], T1.[Contratante_EmailSdaPort], T1.[Contratante_EmailSdaSec], T1.[Contratante_OSAutomatica], T1.[Contratante_SSAutomatica], T1.[Contratante_ExisteConferencia], T1.[Contratante_InicioDoExpediente], T1.[Contratante_FimDoExpediente], T1.[Contratante_ServicoSSPadrao], T1.[Contratante_RetornaSS], T1.[Contratante_SSStatusPlanejamento], T1.[Contratante_SSPrestadoraUnica], T1.[Contratante_PropostaRqrSrv], T1.[Contratante_PropostaRqrPrz], T1.[Contratante_PropostaRqrEsf], T1.[Contratante_PropostaRqrCnt], T1.[Contratante_PropostaNvlCnt], T1.[Contratante_OSGeraOS], T1.[Contratante_OSGeraTRP], T1.[Contratante_OSGeraTH], T1.[Contratante_OSGeraTRD], T1.[Contratante_OSGeraTR], T1.[Contratante_OSHmlgComPnd], T1.[Contratante_AtivoCirculante], T1.[Contratante_PassivoCirculante], T1.[Contratante_PatrimonioLiquido], T1.[Contratante_ReceitaBruta], T1.[Contratante_UsaOSistema], T1.[Contratante_TtlRltGerencial], T1.[Contratante_PrzAoRtr], T1.[Contratante_PrzActRtr], T1.[Contratante_RequerOrigem], T1.[Contratante_SelecionaResponsavelOS], "
          + " T1.[Contratante_ExibePF], T1.[Contratante_Sigla], T1.[Contratante_LogoArquivo] FROM (([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) WHERE T1.[Contratante_Codigo] = @AV16Contratante_Codigo ORDER BY T1.[Contratante_Codigo]" ;
          Object[] prmP00A23 ;
          prmP00A23 = new Object[] {
          new Object[] {"@AV16Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A23 ;
          cmdBufferP00A23=" SELECT T1.[Contratante_LogoNomeArq], T1.[Contratante_LogoTipoArq], T1.[Contratante_Codigo], T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T2.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[Contratante_NomeFantasia], T1.[Contratante_IE], T1.[Contratante_WebSite], T1.[Contratante_Email], T1.[Contratante_Telefone], T1.[Contratante_Ramal], T1.[Contratante_Fax], T1.[Contratante_AgenciaNome], T1.[Contratante_AgenciaNro], T1.[Contratante_BancoNome], T1.[Contratante_BancoNro], T1.[Contratante_ContaCorrente], T1.[Municipio_Codigo], T3.[Municipio_Nome], T3.[Estado_UF], T1.[Contratante_Ativo], T1.[Contratante_EmailSdaHost], T1.[Contratante_EmailSdaUser], T1.[Contratante_EmailSdaPass], T1.[Contratante_EmailSdaKey], T1.[Contratante_EmailSdaAut], T1.[Contratante_EmailSdaPort], T1.[Contratante_EmailSdaSec], T1.[Contratante_OSAutomatica], T1.[Contratante_SSAutomatica], T1.[Contratante_ExisteConferencia], T1.[Contratante_InicioDoExpediente], T1.[Contratante_FimDoExpediente], T1.[Contratante_ServicoSSPadrao], T1.[Contratante_RetornaSS], T1.[Contratante_SSStatusPlanejamento], T1.[Contratante_SSPrestadoraUnica], T1.[Contratante_PropostaRqrSrv], T1.[Contratante_PropostaRqrPrz], T1.[Contratante_PropostaRqrEsf], T1.[Contratante_PropostaRqrCnt], T1.[Contratante_PropostaNvlCnt], T1.[Contratante_OSGeraOS], T1.[Contratante_OSGeraTRP], T1.[Contratante_OSGeraTH], T1.[Contratante_OSGeraTRD], T1.[Contratante_OSGeraTR], T1.[Contratante_OSHmlgComPnd], T1.[Contratante_AtivoCirculante], T1.[Contratante_PassivoCirculante], T1.[Contratante_PatrimonioLiquido], T1.[Contratante_ReceitaBruta], T1.[Contratante_UsaOSistema], T1.[Contratante_TtlRltGerencial], T1.[Contratante_PrzAoRtr], T1.[Contratante_PrzActRtr], T1.[Contratante_RequerOrigem], T1.[Contratante_SelecionaResponsavelOS], "
          + " T1.[Contratante_ExibePF], T1.[Contratante_Sigla], T1.[Contratante_LogoArquivo] FROM (([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) WHERE T1.[Contratante_Codigo] = @AV16Contratante_Codigo ORDER BY T1.[Contratante_Codigo]" ;
          def= new CursorDef[] {
              new CursorDef("P00A22", cmdBufferP00A22,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A22,1,0,false,true )
             ,new CursorDef("P00A23", cmdBufferP00A23,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A23,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 20) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 20) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((String[]) buf[21])[0] = rslt.getString(14, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getString(17, 6) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 10) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getString(20, 50) ;
                ((String[]) buf[34])[0] = rslt.getString(21, 2) ;
                ((bool[]) buf[35])[0] = rslt.getBool(22) ;
                ((String[]) buf[36])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((String[]) buf[38])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((String[]) buf[40])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((String[]) buf[42])[0] = rslt.getString(26, 32) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((bool[]) buf[44])[0] = rslt.getBool(27) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(27);
                ((short[]) buf[46])[0] = rslt.getShort(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((short[]) buf[48])[0] = rslt.getShort(29) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(29);
                ((bool[]) buf[50])[0] = rslt.getBool(30) ;
                ((bool[]) buf[51])[0] = rslt.getBool(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((bool[]) buf[53])[0] = rslt.getBool(32) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDateTime(33) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[56])[0] = rslt.getGXDateTime(34) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(34);
                ((int[]) buf[58])[0] = rslt.getInt(35) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(35);
                ((bool[]) buf[60])[0] = rslt.getBool(36) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(36);
                ((String[]) buf[62])[0] = rslt.getString(37, 1) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(37);
                ((bool[]) buf[64])[0] = rslt.getBool(38) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(38);
                ((bool[]) buf[66])[0] = rslt.getBool(39) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(39);
                ((bool[]) buf[68])[0] = rslt.getBool(40) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(40);
                ((bool[]) buf[70])[0] = rslt.getBool(41) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(41);
                ((bool[]) buf[72])[0] = rslt.getBool(42) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(42);
                ((short[]) buf[74])[0] = rslt.getShort(43) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(43);
                ((bool[]) buf[76])[0] = rslt.getBool(44) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(44);
                ((bool[]) buf[78])[0] = rslt.getBool(45) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(45);
                ((bool[]) buf[80])[0] = rslt.getBool(46) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(46);
                ((bool[]) buf[82])[0] = rslt.getBool(47) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(47);
                ((bool[]) buf[84])[0] = rslt.getBool(48) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(48);
                ((bool[]) buf[86])[0] = rslt.getBool(49) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(49);
                ((decimal[]) buf[88])[0] = rslt.getDecimal(50) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(50);
                ((decimal[]) buf[90])[0] = rslt.getDecimal(51) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(51);
                ((decimal[]) buf[92])[0] = rslt.getDecimal(52) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(52);
                ((decimal[]) buf[94])[0] = rslt.getDecimal(53) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(53);
                ((bool[]) buf[96])[0] = rslt.getBool(54) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(54);
                ((String[]) buf[98])[0] = rslt.getVarchar(55) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(55);
                ((short[]) buf[100])[0] = rslt.getShort(56) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(56);
                ((short[]) buf[102])[0] = rslt.getShort(57) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                ((bool[]) buf[104])[0] = rslt.getBool(58) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(58);
                ((bool[]) buf[106])[0] = rslt.getBool(59) ;
                ((bool[]) buf[107])[0] = rslt.getBool(60) ;
                ((String[]) buf[108])[0] = rslt.getString(61, 15) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                ((String[]) buf[110])[0] = rslt.getBLOBFile(62, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(62);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 20) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 20) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((String[]) buf[21])[0] = rslt.getString(14, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getString(17, 6) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 10) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getString(20, 50) ;
                ((String[]) buf[34])[0] = rslt.getString(21, 2) ;
                ((bool[]) buf[35])[0] = rslt.getBool(22) ;
                ((String[]) buf[36])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((String[]) buf[38])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((String[]) buf[40])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((String[]) buf[42])[0] = rslt.getString(26, 32) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((bool[]) buf[44])[0] = rslt.getBool(27) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(27);
                ((short[]) buf[46])[0] = rslt.getShort(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((short[]) buf[48])[0] = rslt.getShort(29) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(29);
                ((bool[]) buf[50])[0] = rslt.getBool(30) ;
                ((bool[]) buf[51])[0] = rslt.getBool(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((bool[]) buf[53])[0] = rslt.getBool(32) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDateTime(33) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[56])[0] = rslt.getGXDateTime(34) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(34);
                ((int[]) buf[58])[0] = rslt.getInt(35) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(35);
                ((bool[]) buf[60])[0] = rslt.getBool(36) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(36);
                ((String[]) buf[62])[0] = rslt.getString(37, 1) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(37);
                ((bool[]) buf[64])[0] = rslt.getBool(38) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(38);
                ((bool[]) buf[66])[0] = rslt.getBool(39) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(39);
                ((bool[]) buf[68])[0] = rslt.getBool(40) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(40);
                ((bool[]) buf[70])[0] = rslt.getBool(41) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(41);
                ((bool[]) buf[72])[0] = rslt.getBool(42) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(42);
                ((short[]) buf[74])[0] = rslt.getShort(43) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(43);
                ((bool[]) buf[76])[0] = rslt.getBool(44) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(44);
                ((bool[]) buf[78])[0] = rslt.getBool(45) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(45);
                ((bool[]) buf[80])[0] = rslt.getBool(46) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(46);
                ((bool[]) buf[82])[0] = rslt.getBool(47) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(47);
                ((bool[]) buf[84])[0] = rslt.getBool(48) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(48);
                ((bool[]) buf[86])[0] = rslt.getBool(49) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(49);
                ((decimal[]) buf[88])[0] = rslt.getDecimal(50) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(50);
                ((decimal[]) buf[90])[0] = rslt.getDecimal(51) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(51);
                ((decimal[]) buf[92])[0] = rslt.getDecimal(52) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(52);
                ((decimal[]) buf[94])[0] = rslt.getDecimal(53) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(53);
                ((bool[]) buf[96])[0] = rslt.getBool(54) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(54);
                ((String[]) buf[98])[0] = rslt.getVarchar(55) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(55);
                ((short[]) buf[100])[0] = rslt.getShort(56) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(56);
                ((short[]) buf[102])[0] = rslt.getShort(57) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                ((bool[]) buf[104])[0] = rslt.getBool(58) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(58);
                ((bool[]) buf[106])[0] = rslt.getBool(59) ;
                ((bool[]) buf[107])[0] = rslt.getBool(60) ;
                ((String[]) buf[108])[0] = rslt.getString(61, 15) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                ((String[]) buf[110])[0] = rslt.getBLOBFile(62, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(62);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
