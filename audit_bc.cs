/*
               File: Audit_BC
        Description: Audit
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:35.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class audit_bc : GXHttpHandler, IGxSilentTrn
   {
      public audit_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public audit_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3P170( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3P170( ) ;
         standaloneModal( ) ;
         AddRow3P170( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E113P2 */
            E113P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1430AuditId = A1430AuditId;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3P0( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3P170( ) ;
            }
            else
            {
               CheckExtendedTable3P170( ) ;
               if ( AnyError == 0 )
               {
                  ZM3P170( 5) ;
                  ZM3P170( 6) ;
               }
               CloseExtendedTableCursors3P170( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E123P2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_Codigo") == 0 )
               {
                  AV11Insert_Usuario_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
            }
         }
      }

      protected void E113P2( )
      {
         /* After Trn Routine */
      }

      protected void ZM3P170( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1431AuditDate = A1431AuditDate;
            Z1432AuditTableName = A1432AuditTableName;
            Z1433AuditShortDescription = A1433AuditShortDescription;
            Z1435AuditAction = A1435AuditAction;
            Z1Usuario_Codigo = A1Usuario_Codigo;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z2Usuario_Nome = A2Usuario_Nome;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
         }
         if ( GX_JID == -4 )
         {
            Z1430AuditId = A1430AuditId;
            Z1431AuditDate = A1431AuditDate;
            Z1432AuditTableName = A1432AuditTableName;
            Z1434AuditDescription = A1434AuditDescription;
            Z1433AuditShortDescription = A1433AuditShortDescription;
            Z1435AuditAction = A1435AuditAction;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         AV13Pgmname = "Audit_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3P170( )
      {
         /* Using cursor BC003P6 */
         pr_default.execute(4, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound170 = 1;
            A57Usuario_PessoaCod = BC003P6_A57Usuario_PessoaCod[0];
            A1431AuditDate = BC003P6_A1431AuditDate[0];
            n1431AuditDate = BC003P6_n1431AuditDate[0];
            A1432AuditTableName = BC003P6_A1432AuditTableName[0];
            n1432AuditTableName = BC003P6_n1432AuditTableName[0];
            A1434AuditDescription = BC003P6_A1434AuditDescription[0];
            n1434AuditDescription = BC003P6_n1434AuditDescription[0];
            A1433AuditShortDescription = BC003P6_A1433AuditShortDescription[0];
            n1433AuditShortDescription = BC003P6_n1433AuditShortDescription[0];
            A58Usuario_PessoaNom = BC003P6_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC003P6_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC003P6_A2Usuario_Nome[0];
            n2Usuario_Nome = BC003P6_n2Usuario_Nome[0];
            A1435AuditAction = BC003P6_A1435AuditAction[0];
            n1435AuditAction = BC003P6_n1435AuditAction[0];
            A1Usuario_Codigo = BC003P6_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC003P6_n1Usuario_Codigo[0];
            ZM3P170( -4) ;
         }
         pr_default.close(4);
         OnLoadActions3P170( ) ;
      }

      protected void OnLoadActions3P170( )
      {
      }

      protected void CheckExtendedTable3P170( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1431AuditDate) || ( A1431AuditDate >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data / Hora fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC003P4 */
         pr_default.execute(2, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1Usuario_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
            }
         }
         A57Usuario_PessoaCod = BC003P4_A57Usuario_PessoaCod[0];
         A2Usuario_Nome = BC003P4_A2Usuario_Nome[0];
         n2Usuario_Nome = BC003P4_n2Usuario_Nome[0];
         pr_default.close(2);
         /* Using cursor BC003P5 */
         pr_default.execute(3, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A57Usuario_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A58Usuario_PessoaNom = BC003P5_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = BC003P5_n58Usuario_PessoaNom[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3P170( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3P170( )
      {
         /* Using cursor BC003P7 */
         pr_default.execute(5, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound170 = 1;
         }
         else
         {
            RcdFound170 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003P3 */
         pr_default.execute(1, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3P170( 4) ;
            RcdFound170 = 1;
            A1430AuditId = BC003P3_A1430AuditId[0];
            A1431AuditDate = BC003P3_A1431AuditDate[0];
            n1431AuditDate = BC003P3_n1431AuditDate[0];
            A1432AuditTableName = BC003P3_A1432AuditTableName[0];
            n1432AuditTableName = BC003P3_n1432AuditTableName[0];
            A1434AuditDescription = BC003P3_A1434AuditDescription[0];
            n1434AuditDescription = BC003P3_n1434AuditDescription[0];
            A1433AuditShortDescription = BC003P3_A1433AuditShortDescription[0];
            n1433AuditShortDescription = BC003P3_n1433AuditShortDescription[0];
            A1435AuditAction = BC003P3_A1435AuditAction[0];
            n1435AuditAction = BC003P3_n1435AuditAction[0];
            A1Usuario_Codigo = BC003P3_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC003P3_n1Usuario_Codigo[0];
            Z1430AuditId = A1430AuditId;
            sMode170 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3P170( ) ;
            if ( AnyError == 1 )
            {
               RcdFound170 = 0;
               InitializeNonKey3P170( ) ;
            }
            Gx_mode = sMode170;
         }
         else
         {
            RcdFound170 = 0;
            InitializeNonKey3P170( ) ;
            sMode170 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode170;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3P170( ) ;
         if ( RcdFound170 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3P0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3P170( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003P2 */
            pr_default.execute(0, new Object[] {A1430AuditId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Audit"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1431AuditDate != BC003P2_A1431AuditDate[0] ) || ( StringUtil.StrCmp(Z1432AuditTableName, BC003P2_A1432AuditTableName[0]) != 0 ) || ( StringUtil.StrCmp(Z1433AuditShortDescription, BC003P2_A1433AuditShortDescription[0]) != 0 ) || ( StringUtil.StrCmp(Z1435AuditAction, BC003P2_A1435AuditAction[0]) != 0 ) || ( Z1Usuario_Codigo != BC003P2_A1Usuario_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Audit"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3P170( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3P170( 0) ;
            CheckOptimisticConcurrency3P170( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3P170( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3P170( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003P8 */
                     pr_default.execute(6, new Object[] {n1431AuditDate, A1431AuditDate, n1432AuditTableName, A1432AuditTableName, n1434AuditDescription, A1434AuditDescription, n1433AuditShortDescription, A1433AuditShortDescription, n1435AuditAction, A1435AuditAction, n1Usuario_Codigo, A1Usuario_Codigo});
                     A1430AuditId = BC003P8_A1430AuditId[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3P170( ) ;
            }
            EndLevel3P170( ) ;
         }
         CloseExtendedTableCursors3P170( ) ;
      }

      protected void Update3P170( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3P170( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3P170( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3P170( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003P9 */
                     pr_default.execute(7, new Object[] {n1431AuditDate, A1431AuditDate, n1432AuditTableName, A1432AuditTableName, n1434AuditDescription, A1434AuditDescription, n1433AuditShortDescription, A1433AuditShortDescription, n1435AuditAction, A1435AuditAction, n1Usuario_Codigo, A1Usuario_Codigo, A1430AuditId});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Audit"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3P170( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3P170( ) ;
         }
         CloseExtendedTableCursors3P170( ) ;
      }

      protected void DeferredUpdate3P170( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3P170( ) ;
            AfterConfirm3P170( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3P170( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003P10 */
                  pr_default.execute(8, new Object[] {A1430AuditId});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode170 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3P170( ) ;
         Gx_mode = sMode170;
      }

      protected void OnDeleteControls3P170( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003P11 */
            pr_default.execute(9, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            A57Usuario_PessoaCod = BC003P11_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = BC003P11_A2Usuario_Nome[0];
            n2Usuario_Nome = BC003P11_n2Usuario_Nome[0];
            pr_default.close(9);
            /* Using cursor BC003P12 */
            pr_default.execute(10, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = BC003P12_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC003P12_n58Usuario_PessoaNom[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel3P170( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3P170( )
      {
         /* Scan By routine */
         /* Using cursor BC003P13 */
         pr_default.execute(11, new Object[] {A1430AuditId});
         RcdFound170 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound170 = 1;
            A57Usuario_PessoaCod = BC003P13_A57Usuario_PessoaCod[0];
            A1430AuditId = BC003P13_A1430AuditId[0];
            A1431AuditDate = BC003P13_A1431AuditDate[0];
            n1431AuditDate = BC003P13_n1431AuditDate[0];
            A1432AuditTableName = BC003P13_A1432AuditTableName[0];
            n1432AuditTableName = BC003P13_n1432AuditTableName[0];
            A1434AuditDescription = BC003P13_A1434AuditDescription[0];
            n1434AuditDescription = BC003P13_n1434AuditDescription[0];
            A1433AuditShortDescription = BC003P13_A1433AuditShortDescription[0];
            n1433AuditShortDescription = BC003P13_n1433AuditShortDescription[0];
            A58Usuario_PessoaNom = BC003P13_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC003P13_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC003P13_A2Usuario_Nome[0];
            n2Usuario_Nome = BC003P13_n2Usuario_Nome[0];
            A1435AuditAction = BC003P13_A1435AuditAction[0];
            n1435AuditAction = BC003P13_n1435AuditAction[0];
            A1Usuario_Codigo = BC003P13_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC003P13_n1Usuario_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3P170( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound170 = 0;
         ScanKeyLoad3P170( ) ;
      }

      protected void ScanKeyLoad3P170( )
      {
         sMode170 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound170 = 1;
            A57Usuario_PessoaCod = BC003P13_A57Usuario_PessoaCod[0];
            A1430AuditId = BC003P13_A1430AuditId[0];
            A1431AuditDate = BC003P13_A1431AuditDate[0];
            n1431AuditDate = BC003P13_n1431AuditDate[0];
            A1432AuditTableName = BC003P13_A1432AuditTableName[0];
            n1432AuditTableName = BC003P13_n1432AuditTableName[0];
            A1434AuditDescription = BC003P13_A1434AuditDescription[0];
            n1434AuditDescription = BC003P13_n1434AuditDescription[0];
            A1433AuditShortDescription = BC003P13_A1433AuditShortDescription[0];
            n1433AuditShortDescription = BC003P13_n1433AuditShortDescription[0];
            A58Usuario_PessoaNom = BC003P13_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC003P13_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC003P13_A2Usuario_Nome[0];
            n2Usuario_Nome = BC003P13_n2Usuario_Nome[0];
            A1435AuditAction = BC003P13_A1435AuditAction[0];
            n1435AuditAction = BC003P13_n1435AuditAction[0];
            A1Usuario_Codigo = BC003P13_A1Usuario_Codigo[0];
            n1Usuario_Codigo = BC003P13_n1Usuario_Codigo[0];
         }
         Gx_mode = sMode170;
      }

      protected void ScanKeyEnd3P170( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3P170( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3P170( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3P170( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3P170( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3P170( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3P170( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3P170( )
      {
      }

      protected void AddRow3P170( )
      {
         VarsToRow170( bcAudit) ;
      }

      protected void ReadRow3P170( )
      {
         RowToVars170( bcAudit, 1) ;
      }

      protected void InitializeNonKey3P170( )
      {
         A57Usuario_PessoaCod = 0;
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         n1431AuditDate = false;
         A1432AuditTableName = "";
         n1432AuditTableName = false;
         A1434AuditDescription = "";
         n1434AuditDescription = false;
         A1433AuditShortDescription = "";
         n1433AuditShortDescription = false;
         A1Usuario_Codigo = 0;
         n1Usuario_Codigo = false;
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         A1435AuditAction = "";
         n1435AuditAction = false;
         Z1431AuditDate = (DateTime)(DateTime.MinValue);
         Z1432AuditTableName = "";
         Z1433AuditShortDescription = "";
         Z1435AuditAction = "";
         Z1Usuario_Codigo = 0;
      }

      protected void InitAll3P170( )
      {
         A1430AuditId = 0;
         InitializeNonKey3P170( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow170( SdtAudit obj170 )
      {
         obj170.gxTpr_Mode = Gx_mode;
         obj170.gxTpr_Auditdate = A1431AuditDate;
         obj170.gxTpr_Audittablename = A1432AuditTableName;
         obj170.gxTpr_Auditdescription = A1434AuditDescription;
         obj170.gxTpr_Auditshortdescription = A1433AuditShortDescription;
         obj170.gxTpr_Usuario_codigo = A1Usuario_Codigo;
         obj170.gxTpr_Usuario_pessoanom = A58Usuario_PessoaNom;
         obj170.gxTpr_Usuario_nome = A2Usuario_Nome;
         obj170.gxTpr_Auditaction = A1435AuditAction;
         obj170.gxTpr_Auditid = A1430AuditId;
         obj170.gxTpr_Auditid_Z = Z1430AuditId;
         obj170.gxTpr_Auditdate_Z = Z1431AuditDate;
         obj170.gxTpr_Audittablename_Z = Z1432AuditTableName;
         obj170.gxTpr_Auditshortdescription_Z = Z1433AuditShortDescription;
         obj170.gxTpr_Usuario_codigo_Z = Z1Usuario_Codigo;
         obj170.gxTpr_Usuario_pessoanom_Z = Z58Usuario_PessoaNom;
         obj170.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj170.gxTpr_Auditaction_Z = Z1435AuditAction;
         obj170.gxTpr_Auditdate_N = (short)(Convert.ToInt16(n1431AuditDate));
         obj170.gxTpr_Audittablename_N = (short)(Convert.ToInt16(n1432AuditTableName));
         obj170.gxTpr_Auditdescription_N = (short)(Convert.ToInt16(n1434AuditDescription));
         obj170.gxTpr_Auditshortdescription_N = (short)(Convert.ToInt16(n1433AuditShortDescription));
         obj170.gxTpr_Usuario_codigo_N = (short)(Convert.ToInt16(n1Usuario_Codigo));
         obj170.gxTpr_Usuario_pessoanom_N = (short)(Convert.ToInt16(n58Usuario_PessoaNom));
         obj170.gxTpr_Usuario_nome_N = (short)(Convert.ToInt16(n2Usuario_Nome));
         obj170.gxTpr_Auditaction_N = (short)(Convert.ToInt16(n1435AuditAction));
         obj170.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow170( SdtAudit obj170 )
      {
         obj170.gxTpr_Auditid = A1430AuditId;
         return  ;
      }

      public void RowToVars170( SdtAudit obj170 ,
                                int forceLoad )
      {
         Gx_mode = obj170.gxTpr_Mode;
         A1431AuditDate = obj170.gxTpr_Auditdate;
         n1431AuditDate = false;
         A1432AuditTableName = obj170.gxTpr_Audittablename;
         n1432AuditTableName = false;
         A1434AuditDescription = obj170.gxTpr_Auditdescription;
         n1434AuditDescription = false;
         A1433AuditShortDescription = obj170.gxTpr_Auditshortdescription;
         n1433AuditShortDescription = false;
         A1Usuario_Codigo = obj170.gxTpr_Usuario_codigo;
         n1Usuario_Codigo = false;
         A58Usuario_PessoaNom = obj170.gxTpr_Usuario_pessoanom;
         n58Usuario_PessoaNom = false;
         A2Usuario_Nome = obj170.gxTpr_Usuario_nome;
         n2Usuario_Nome = false;
         A1435AuditAction = obj170.gxTpr_Auditaction;
         n1435AuditAction = false;
         A1430AuditId = obj170.gxTpr_Auditid;
         Z1430AuditId = obj170.gxTpr_Auditid_Z;
         Z1431AuditDate = obj170.gxTpr_Auditdate_Z;
         Z1432AuditTableName = obj170.gxTpr_Audittablename_Z;
         Z1433AuditShortDescription = obj170.gxTpr_Auditshortdescription_Z;
         Z1Usuario_Codigo = obj170.gxTpr_Usuario_codigo_Z;
         Z58Usuario_PessoaNom = obj170.gxTpr_Usuario_pessoanom_Z;
         Z2Usuario_Nome = obj170.gxTpr_Usuario_nome_Z;
         Z1435AuditAction = obj170.gxTpr_Auditaction_Z;
         n1431AuditDate = (bool)(Convert.ToBoolean(obj170.gxTpr_Auditdate_N));
         n1432AuditTableName = (bool)(Convert.ToBoolean(obj170.gxTpr_Audittablename_N));
         n1434AuditDescription = (bool)(Convert.ToBoolean(obj170.gxTpr_Auditdescription_N));
         n1433AuditShortDescription = (bool)(Convert.ToBoolean(obj170.gxTpr_Auditshortdescription_N));
         n1Usuario_Codigo = (bool)(Convert.ToBoolean(obj170.gxTpr_Usuario_codigo_N));
         n58Usuario_PessoaNom = (bool)(Convert.ToBoolean(obj170.gxTpr_Usuario_pessoanom_N));
         n2Usuario_Nome = (bool)(Convert.ToBoolean(obj170.gxTpr_Usuario_nome_N));
         n1435AuditAction = (bool)(Convert.ToBoolean(obj170.gxTpr_Auditaction_N));
         Gx_mode = obj170.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1430AuditId = (long)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3P170( ) ;
         ScanKeyStart3P170( ) ;
         if ( RcdFound170 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1430AuditId = A1430AuditId;
         }
         ZM3P170( -4) ;
         OnLoadActions3P170( ) ;
         AddRow3P170( ) ;
         ScanKeyEnd3P170( ) ;
         if ( RcdFound170 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars170( bcAudit, 0) ;
         ScanKeyStart3P170( ) ;
         if ( RcdFound170 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1430AuditId = A1430AuditId;
         }
         ZM3P170( -4) ;
         OnLoadActions3P170( ) ;
         AddRow3P170( ) ;
         ScanKeyEnd3P170( ) ;
         if ( RcdFound170 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars170( bcAudit, 0) ;
         nKeyPressed = 1;
         GetKey3P170( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3P170( ) ;
         }
         else
         {
            if ( RcdFound170 == 1 )
            {
               if ( A1430AuditId != Z1430AuditId )
               {
                  A1430AuditId = Z1430AuditId;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3P170( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1430AuditId != Z1430AuditId )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3P170( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3P170( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow170( bcAudit) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars170( bcAudit, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3P170( ) ;
         if ( RcdFound170 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1430AuditId != Z1430AuditId )
            {
               A1430AuditId = Z1430AuditId;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1430AuditId != Z1430AuditId )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "Audit_BC");
         VarsToRow170( bcAudit) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAudit.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAudit.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAudit )
         {
            bcAudit = (SdtAudit)(sdt);
            if ( StringUtil.StrCmp(bcAudit.gxTpr_Mode, "") == 0 )
            {
               bcAudit.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow170( bcAudit) ;
            }
            else
            {
               RowToVars170( bcAudit, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAudit.gxTpr_Mode, "") == 0 )
            {
               bcAudit.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars170( bcAudit, 1) ;
         return  ;
      }

      public SdtAudit Audit_BC
      {
         get {
            return bcAudit ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1431AuditDate = (DateTime)(DateTime.MinValue);
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         Z1432AuditTableName = "";
         A1432AuditTableName = "";
         Z1433AuditShortDescription = "";
         A1433AuditShortDescription = "";
         Z1435AuditAction = "";
         A1435AuditAction = "";
         Z2Usuario_Nome = "";
         A2Usuario_Nome = "";
         Z58Usuario_PessoaNom = "";
         A58Usuario_PessoaNom = "";
         Z1434AuditDescription = "";
         A1434AuditDescription = "";
         BC003P6_A57Usuario_PessoaCod = new int[1] ;
         BC003P6_A1430AuditId = new long[1] ;
         BC003P6_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         BC003P6_n1431AuditDate = new bool[] {false} ;
         BC003P6_A1432AuditTableName = new String[] {""} ;
         BC003P6_n1432AuditTableName = new bool[] {false} ;
         BC003P6_A1434AuditDescription = new String[] {""} ;
         BC003P6_n1434AuditDescription = new bool[] {false} ;
         BC003P6_A1433AuditShortDescription = new String[] {""} ;
         BC003P6_n1433AuditShortDescription = new bool[] {false} ;
         BC003P6_A58Usuario_PessoaNom = new String[] {""} ;
         BC003P6_n58Usuario_PessoaNom = new bool[] {false} ;
         BC003P6_A2Usuario_Nome = new String[] {""} ;
         BC003P6_n2Usuario_Nome = new bool[] {false} ;
         BC003P6_A1435AuditAction = new String[] {""} ;
         BC003P6_n1435AuditAction = new bool[] {false} ;
         BC003P6_A1Usuario_Codigo = new int[1] ;
         BC003P6_n1Usuario_Codigo = new bool[] {false} ;
         BC003P4_A57Usuario_PessoaCod = new int[1] ;
         BC003P4_A2Usuario_Nome = new String[] {""} ;
         BC003P4_n2Usuario_Nome = new bool[] {false} ;
         BC003P5_A58Usuario_PessoaNom = new String[] {""} ;
         BC003P5_n58Usuario_PessoaNom = new bool[] {false} ;
         BC003P7_A1430AuditId = new long[1] ;
         BC003P3_A1430AuditId = new long[1] ;
         BC003P3_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         BC003P3_n1431AuditDate = new bool[] {false} ;
         BC003P3_A1432AuditTableName = new String[] {""} ;
         BC003P3_n1432AuditTableName = new bool[] {false} ;
         BC003P3_A1434AuditDescription = new String[] {""} ;
         BC003P3_n1434AuditDescription = new bool[] {false} ;
         BC003P3_A1433AuditShortDescription = new String[] {""} ;
         BC003P3_n1433AuditShortDescription = new bool[] {false} ;
         BC003P3_A1435AuditAction = new String[] {""} ;
         BC003P3_n1435AuditAction = new bool[] {false} ;
         BC003P3_A1Usuario_Codigo = new int[1] ;
         BC003P3_n1Usuario_Codigo = new bool[] {false} ;
         sMode170 = "";
         BC003P2_A1430AuditId = new long[1] ;
         BC003P2_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         BC003P2_n1431AuditDate = new bool[] {false} ;
         BC003P2_A1432AuditTableName = new String[] {""} ;
         BC003P2_n1432AuditTableName = new bool[] {false} ;
         BC003P2_A1434AuditDescription = new String[] {""} ;
         BC003P2_n1434AuditDescription = new bool[] {false} ;
         BC003P2_A1433AuditShortDescription = new String[] {""} ;
         BC003P2_n1433AuditShortDescription = new bool[] {false} ;
         BC003P2_A1435AuditAction = new String[] {""} ;
         BC003P2_n1435AuditAction = new bool[] {false} ;
         BC003P2_A1Usuario_Codigo = new int[1] ;
         BC003P2_n1Usuario_Codigo = new bool[] {false} ;
         BC003P8_A1430AuditId = new long[1] ;
         BC003P11_A57Usuario_PessoaCod = new int[1] ;
         BC003P11_A2Usuario_Nome = new String[] {""} ;
         BC003P11_n2Usuario_Nome = new bool[] {false} ;
         BC003P12_A58Usuario_PessoaNom = new String[] {""} ;
         BC003P12_n58Usuario_PessoaNom = new bool[] {false} ;
         BC003P13_A57Usuario_PessoaCod = new int[1] ;
         BC003P13_A1430AuditId = new long[1] ;
         BC003P13_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         BC003P13_n1431AuditDate = new bool[] {false} ;
         BC003P13_A1432AuditTableName = new String[] {""} ;
         BC003P13_n1432AuditTableName = new bool[] {false} ;
         BC003P13_A1434AuditDescription = new String[] {""} ;
         BC003P13_n1434AuditDescription = new bool[] {false} ;
         BC003P13_A1433AuditShortDescription = new String[] {""} ;
         BC003P13_n1433AuditShortDescription = new bool[] {false} ;
         BC003P13_A58Usuario_PessoaNom = new String[] {""} ;
         BC003P13_n58Usuario_PessoaNom = new bool[] {false} ;
         BC003P13_A2Usuario_Nome = new String[] {""} ;
         BC003P13_n2Usuario_Nome = new bool[] {false} ;
         BC003P13_A1435AuditAction = new String[] {""} ;
         BC003P13_n1435AuditAction = new bool[] {false} ;
         BC003P13_A1Usuario_Codigo = new int[1] ;
         BC003P13_n1Usuario_Codigo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.audit_bc__default(),
            new Object[][] {
                new Object[] {
               BC003P2_A1430AuditId, BC003P2_A1431AuditDate, BC003P2_n1431AuditDate, BC003P2_A1432AuditTableName, BC003P2_n1432AuditTableName, BC003P2_A1434AuditDescription, BC003P2_n1434AuditDescription, BC003P2_A1433AuditShortDescription, BC003P2_n1433AuditShortDescription, BC003P2_A1435AuditAction,
               BC003P2_n1435AuditAction, BC003P2_A1Usuario_Codigo, BC003P2_n1Usuario_Codigo
               }
               , new Object[] {
               BC003P3_A1430AuditId, BC003P3_A1431AuditDate, BC003P3_n1431AuditDate, BC003P3_A1432AuditTableName, BC003P3_n1432AuditTableName, BC003P3_A1434AuditDescription, BC003P3_n1434AuditDescription, BC003P3_A1433AuditShortDescription, BC003P3_n1433AuditShortDescription, BC003P3_A1435AuditAction,
               BC003P3_n1435AuditAction, BC003P3_A1Usuario_Codigo, BC003P3_n1Usuario_Codigo
               }
               , new Object[] {
               BC003P4_A57Usuario_PessoaCod, BC003P4_A2Usuario_Nome, BC003P4_n2Usuario_Nome
               }
               , new Object[] {
               BC003P5_A58Usuario_PessoaNom, BC003P5_n58Usuario_PessoaNom
               }
               , new Object[] {
               BC003P6_A57Usuario_PessoaCod, BC003P6_A1430AuditId, BC003P6_A1431AuditDate, BC003P6_n1431AuditDate, BC003P6_A1432AuditTableName, BC003P6_n1432AuditTableName, BC003P6_A1434AuditDescription, BC003P6_n1434AuditDescription, BC003P6_A1433AuditShortDescription, BC003P6_n1433AuditShortDescription,
               BC003P6_A58Usuario_PessoaNom, BC003P6_n58Usuario_PessoaNom, BC003P6_A2Usuario_Nome, BC003P6_n2Usuario_Nome, BC003P6_A1435AuditAction, BC003P6_n1435AuditAction, BC003P6_A1Usuario_Codigo, BC003P6_n1Usuario_Codigo
               }
               , new Object[] {
               BC003P7_A1430AuditId
               }
               , new Object[] {
               BC003P8_A1430AuditId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003P11_A57Usuario_PessoaCod, BC003P11_A2Usuario_Nome, BC003P11_n2Usuario_Nome
               }
               , new Object[] {
               BC003P12_A58Usuario_PessoaNom, BC003P12_n58Usuario_PessoaNom
               }
               , new Object[] {
               BC003P13_A57Usuario_PessoaCod, BC003P13_A1430AuditId, BC003P13_A1431AuditDate, BC003P13_n1431AuditDate, BC003P13_A1432AuditTableName, BC003P13_n1432AuditTableName, BC003P13_A1434AuditDescription, BC003P13_n1434AuditDescription, BC003P13_A1433AuditShortDescription, BC003P13_n1433AuditShortDescription,
               BC003P13_A58Usuario_PessoaNom, BC003P13_n58Usuario_PessoaNom, BC003P13_A2Usuario_Nome, BC003P13_n2Usuario_Nome, BC003P13_A1435AuditAction, BC003P13_n1435AuditAction, BC003P13_A1Usuario_Codigo, BC003P13_n1Usuario_Codigo
               }
            }
         );
         AV13Pgmname = "Audit_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E123P2 */
         E123P2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound170 ;
      private int trnEnded ;
      private int AV14GXV1 ;
      private int AV11Insert_Usuario_Codigo ;
      private int Z1Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private int Z57Usuario_PessoaCod ;
      private int A57Usuario_PessoaCod ;
      private long Z1430AuditId ;
      private long A1430AuditId ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV13Pgmname ;
      private String Z1432AuditTableName ;
      private String A1432AuditTableName ;
      private String Z1435AuditAction ;
      private String A1435AuditAction ;
      private String Z2Usuario_Nome ;
      private String A2Usuario_Nome ;
      private String Z58Usuario_PessoaNom ;
      private String A58Usuario_PessoaNom ;
      private String sMode170 ;
      private DateTime Z1431AuditDate ;
      private DateTime A1431AuditDate ;
      private bool n1431AuditDate ;
      private bool n1432AuditTableName ;
      private bool n1434AuditDescription ;
      private bool n1433AuditShortDescription ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool n1435AuditAction ;
      private bool n1Usuario_Codigo ;
      private String Z1434AuditDescription ;
      private String A1434AuditDescription ;
      private String Z1433AuditShortDescription ;
      private String A1433AuditShortDescription ;
      private IGxSession AV10WebSession ;
      private SdtAudit bcAudit ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003P6_A57Usuario_PessoaCod ;
      private long[] BC003P6_A1430AuditId ;
      private DateTime[] BC003P6_A1431AuditDate ;
      private bool[] BC003P6_n1431AuditDate ;
      private String[] BC003P6_A1432AuditTableName ;
      private bool[] BC003P6_n1432AuditTableName ;
      private String[] BC003P6_A1434AuditDescription ;
      private bool[] BC003P6_n1434AuditDescription ;
      private String[] BC003P6_A1433AuditShortDescription ;
      private bool[] BC003P6_n1433AuditShortDescription ;
      private String[] BC003P6_A58Usuario_PessoaNom ;
      private bool[] BC003P6_n58Usuario_PessoaNom ;
      private String[] BC003P6_A2Usuario_Nome ;
      private bool[] BC003P6_n2Usuario_Nome ;
      private String[] BC003P6_A1435AuditAction ;
      private bool[] BC003P6_n1435AuditAction ;
      private int[] BC003P6_A1Usuario_Codigo ;
      private bool[] BC003P6_n1Usuario_Codigo ;
      private int[] BC003P4_A57Usuario_PessoaCod ;
      private String[] BC003P4_A2Usuario_Nome ;
      private bool[] BC003P4_n2Usuario_Nome ;
      private String[] BC003P5_A58Usuario_PessoaNom ;
      private bool[] BC003P5_n58Usuario_PessoaNom ;
      private long[] BC003P7_A1430AuditId ;
      private long[] BC003P3_A1430AuditId ;
      private DateTime[] BC003P3_A1431AuditDate ;
      private bool[] BC003P3_n1431AuditDate ;
      private String[] BC003P3_A1432AuditTableName ;
      private bool[] BC003P3_n1432AuditTableName ;
      private String[] BC003P3_A1434AuditDescription ;
      private bool[] BC003P3_n1434AuditDescription ;
      private String[] BC003P3_A1433AuditShortDescription ;
      private bool[] BC003P3_n1433AuditShortDescription ;
      private String[] BC003P3_A1435AuditAction ;
      private bool[] BC003P3_n1435AuditAction ;
      private int[] BC003P3_A1Usuario_Codigo ;
      private bool[] BC003P3_n1Usuario_Codigo ;
      private long[] BC003P2_A1430AuditId ;
      private DateTime[] BC003P2_A1431AuditDate ;
      private bool[] BC003P2_n1431AuditDate ;
      private String[] BC003P2_A1432AuditTableName ;
      private bool[] BC003P2_n1432AuditTableName ;
      private String[] BC003P2_A1434AuditDescription ;
      private bool[] BC003P2_n1434AuditDescription ;
      private String[] BC003P2_A1433AuditShortDescription ;
      private bool[] BC003P2_n1433AuditShortDescription ;
      private String[] BC003P2_A1435AuditAction ;
      private bool[] BC003P2_n1435AuditAction ;
      private int[] BC003P2_A1Usuario_Codigo ;
      private bool[] BC003P2_n1Usuario_Codigo ;
      private long[] BC003P8_A1430AuditId ;
      private int[] BC003P11_A57Usuario_PessoaCod ;
      private String[] BC003P11_A2Usuario_Nome ;
      private bool[] BC003P11_n2Usuario_Nome ;
      private String[] BC003P12_A58Usuario_PessoaNom ;
      private bool[] BC003P12_n58Usuario_PessoaNom ;
      private int[] BC003P13_A57Usuario_PessoaCod ;
      private long[] BC003P13_A1430AuditId ;
      private DateTime[] BC003P13_A1431AuditDate ;
      private bool[] BC003P13_n1431AuditDate ;
      private String[] BC003P13_A1432AuditTableName ;
      private bool[] BC003P13_n1432AuditTableName ;
      private String[] BC003P13_A1434AuditDescription ;
      private bool[] BC003P13_n1434AuditDescription ;
      private String[] BC003P13_A1433AuditShortDescription ;
      private bool[] BC003P13_n1433AuditShortDescription ;
      private String[] BC003P13_A58Usuario_PessoaNom ;
      private bool[] BC003P13_n58Usuario_PessoaNom ;
      private String[] BC003P13_A2Usuario_Nome ;
      private bool[] BC003P13_n2Usuario_Nome ;
      private String[] BC003P13_A1435AuditAction ;
      private bool[] BC003P13_n1435AuditAction ;
      private int[] BC003P13_A1Usuario_Codigo ;
      private bool[] BC003P13_n1Usuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class audit_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003P6 ;
          prmBC003P6 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P4 ;
          prmBC003P4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003P5 ;
          prmBC003P5 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003P7 ;
          prmBC003P7 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P3 ;
          prmBC003P3 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P2 ;
          prmBC003P2 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P8 ;
          prmBC003P8 = new Object[] {
          new Object[] {"@AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AuditDescription",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AuditShortDescription",SqlDbType.VarChar,400,0} ,
          new Object[] {"@AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003P9 ;
          prmBC003P9 = new Object[] {
          new Object[] {"@AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AuditDescription",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AuditShortDescription",SqlDbType.VarChar,400,0} ,
          new Object[] {"@AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P10 ;
          prmBC003P10 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC003P11 ;
          prmBC003P11 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003P12 ;
          prmBC003P12 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003P13 ;
          prmBC003P13 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003P2", "SELECT [AuditId], [AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo] FROM [Audit] WITH (UPDLOCK) WHERE [AuditId] = @AuditId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P2,1,0,true,false )
             ,new CursorDef("BC003P3", "SELECT [AuditId], [AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo] FROM [Audit] WITH (NOLOCK) WHERE [AuditId] = @AuditId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P3,1,0,true,false )
             ,new CursorDef("BC003P4", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P4,1,0,true,false )
             ,new CursorDef("BC003P5", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P5,1,0,true,false )
             ,new CursorDef("BC003P6", "SELECT T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[AuditId], TM1.[AuditDate], TM1.[AuditTableName], TM1.[AuditDescription], TM1.[AuditShortDescription], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], TM1.[AuditAction], TM1.[Usuario_Codigo] FROM (([Audit] TM1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[AuditId] = @AuditId ORDER BY TM1.[AuditId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P6,100,0,true,false )
             ,new CursorDef("BC003P7", "SELECT [AuditId] FROM [Audit] WITH (NOLOCK) WHERE [AuditId] = @AuditId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P7,1,0,true,false )
             ,new CursorDef("BC003P8", "INSERT INTO [Audit]([AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo]) VALUES(@AuditDate, @AuditTableName, @AuditDescription, @AuditShortDescription, @AuditAction, @Usuario_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003P8)
             ,new CursorDef("BC003P9", "UPDATE [Audit] SET [AuditDate]=@AuditDate, [AuditTableName]=@AuditTableName, [AuditDescription]=@AuditDescription, [AuditShortDescription]=@AuditShortDescription, [AuditAction]=@AuditAction, [Usuario_Codigo]=@Usuario_Codigo  WHERE [AuditId] = @AuditId", GxErrorMask.GX_NOMASK,prmBC003P9)
             ,new CursorDef("BC003P10", "DELETE FROM [Audit]  WHERE [AuditId] = @AuditId", GxErrorMask.GX_NOMASK,prmBC003P10)
             ,new CursorDef("BC003P11", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P11,1,0,true,false )
             ,new CursorDef("BC003P12", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P12,1,0,true,false )
             ,new CursorDef("BC003P13", "SELECT T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[AuditId], TM1.[AuditDate], TM1.[AuditTableName], TM1.[AuditDescription], TM1.[AuditShortDescription], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], TM1.[AuditAction], TM1.[Usuario_Codigo] FROM (([Audit] TM1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[AuditId] = @AuditId ORDER BY TM1.[AuditId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003P13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                stmt.SetParameter(7, (long)parms[12]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
