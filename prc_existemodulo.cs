/*
               File: PRC_ExisteModulo
        Description: Existe Modulo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:59:42.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existemodulo : GXProcedure
   {
      public prc_existemodulo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existemodulo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Sistema_Codigo ,
                           String aP1_Modulo_Nome ,
                           String aP2_Modulo_Sigla ,
                           out bool aP3_Existe )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV10Modulo_Nome = aP1_Modulo_Nome;
         this.AV11Modulo_Sigla = aP2_Modulo_Sigla;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP3_Existe=this.AV8Existe;
      }

      public bool executeUdp( ref int aP0_Sistema_Codigo ,
                              String aP1_Modulo_Nome ,
                              String aP2_Modulo_Sigla )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV10Modulo_Nome = aP1_Modulo_Nome;
         this.AV11Modulo_Sigla = aP2_Modulo_Sigla;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP3_Existe=this.AV8Existe;
         return AV8Existe ;
      }

      public void executeSubmit( ref int aP0_Sistema_Codigo ,
                                 String aP1_Modulo_Nome ,
                                 String aP2_Modulo_Sigla ,
                                 out bool aP3_Existe )
      {
         prc_existemodulo objprc_existemodulo;
         objprc_existemodulo = new prc_existemodulo();
         objprc_existemodulo.A127Sistema_Codigo = aP0_Sistema_Codigo;
         objprc_existemodulo.AV10Modulo_Nome = aP1_Modulo_Nome;
         objprc_existemodulo.AV11Modulo_Sigla = aP2_Modulo_Sigla;
         objprc_existemodulo.AV8Existe = false ;
         objprc_existemodulo.context.SetSubmitInitialConfig(context);
         objprc_existemodulo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existemodulo);
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP3_Existe=this.AV8Existe;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existemodulo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11Modulo_Sigla ,
                                              A143Modulo_Nome ,
                                              AV10Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              A127Sistema_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         /* Using cursor P004H2 */
         pr_default.execute(0, new Object[] {A127Sistema_Codigo, AV10Modulo_Nome, AV10Modulo_Nome, AV11Modulo_Sigla});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A145Modulo_Sigla = P004H2_A145Modulo_Sigla[0];
            A143Modulo_Nome = P004H2_A143Modulo_Nome[0];
            A146Modulo_Codigo = P004H2_A146Modulo_Codigo[0];
            AV8Existe = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         P004H2_A127Sistema_Codigo = new int[1] ;
         P004H2_A145Modulo_Sigla = new String[] {""} ;
         P004H2_A143Modulo_Nome = new String[] {""} ;
         P004H2_A146Modulo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existemodulo__default(),
            new Object[][] {
                new Object[] {
               P004H2_A127Sistema_Codigo, P004H2_A145Modulo_Sigla, P004H2_A143Modulo_Nome, P004H2_A146Modulo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A127Sistema_Codigo ;
      private int A146Modulo_Codigo ;
      private String AV10Modulo_Nome ;
      private String AV11Modulo_Sigla ;
      private String scmdbuf ;
      private String A143Modulo_Nome ;
      private String A145Modulo_Sigla ;
      private bool AV8Existe ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Sistema_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004H2_A127Sistema_Codigo ;
      private String[] P004H2_A145Modulo_Sigla ;
      private String[] P004H2_A143Modulo_Nome ;
      private int[] P004H2_A146Modulo_Codigo ;
      private bool aP3_Existe ;
   }

   public class prc_existemodulo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P004H2( IGxContext context ,
                                             String AV11Modulo_Sigla ,
                                             String A143Modulo_Nome ,
                                             String AV10Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             int A127Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Codigo], [Modulo_Sigla], [Modulo_Nome], [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Codigo] = @Sistema_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Modulo_Sigla)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV10Modulo_Nome)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Modulo_Sigla)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV10Modulo_Nome or [Modulo_Sigla] = @AV11Modulo_Sigla)";
         }
         else
         {
            GXv_int1[2] = 1;
            GXv_int1[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P004H2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004H2 ;
          prmP004H2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11Modulo_Sigla",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004H2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004H2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
