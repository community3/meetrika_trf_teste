/*
               File: type_SdtCrcStream
        Description: CrcStream
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 21:5:42.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtCrcStream : GxUserType, IGxExternalObject
   {
      public SdtCrcStream( )
      {
         initialize();
      }

      public SdtCrcStream( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void flush( )
      {
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         CrcStream_externalReference.Flush();
         return  ;
      }

      public long seek( long gxTp_offset ,
                        object gxTp_origin )
      {
         long returnseek ;
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         returnseek = 0;
         returnseek = (long)(CrcStream_externalReference.Seek(gxTp_offset, (System.IO.SeekOrigin)(gxTp_origin)));
         return returnseek ;
      }

      public void setlength( long gxTp_value )
      {
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         CrcStream_externalReference.SetLength(gxTp_value);
         return  ;
      }

      public int read( object gxTp_buffer ,
                       int gxTp_offset ,
                       int gxTp_count )
      {
         int returnread ;
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         returnread = 0;
         returnread = (int)(CrcStream_externalReference.Read((System.Byte[])(gxTp_buffer), gxTp_offset, gxTp_count));
         return returnread ;
      }

      public void write( object gxTp_buffer ,
                         int gxTp_offset ,
                         int gxTp_count )
      {
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         CrcStream_externalReference.Write((System.Byte[])(gxTp_buffer), gxTp_offset, gxTp_count);
         return  ;
      }

      public void generatecrc( String gxTp_FileName ,
                               out String gxTp_crc ,
                               out String gxTp_errors )
      {
         gxTp_crc = "";
         gxTp_errors = "";
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         CrcStream_externalReference.GenerateCRC(gxTp_FileName, out gxTp_crc, out gxTp_errors);
         return  ;
      }

      public void resetchecksum( )
      {
         if ( CrcStream_externalReference == null )
         {
            CrcStream_externalReference = new Eficacia.CrcStream(null);
         }
         CrcStream_externalReference.ResetChecksum();
         return  ;
      }

      public object gxTpr_Stream
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            object intValue ;
            intValue = new object();
            System.IO.Stream externalParm0 ;
            externalParm0 = CrcStream_externalReference.Stream;
            intValue = (object)(externalParm0);
            return intValue ;
         }

      }

      public bool gxTpr_Canread
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference.CanRead ;
         }

      }

      public bool gxTpr_Canseek
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference.CanSeek ;
         }

      }

      public bool gxTpr_Canwrite
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference.CanWrite ;
         }

      }

      public long gxTpr_Length
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference.Length ;
         }

      }

      public long gxTpr_Position
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference.Position ;
         }

         set {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            CrcStream_externalReference.Position = value;
         }

      }

      public object gxTpr_Readcrc
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            object intValue ;
            intValue = new object();
            System.UInt32 externalParm1 ;
            externalParm1 = CrcStream_externalReference.ReadCrc;
            intValue = (object)(externalParm1);
            return intValue ;
         }

      }

      public object gxTpr_Writecrc
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            object intValue ;
            intValue = new object();
            System.UInt32 externalParm2 ;
            externalParm2 = CrcStream_externalReference.WriteCrc;
            intValue = (object)(externalParm2);
            return intValue ;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( CrcStream_externalReference == null )
            {
               CrcStream_externalReference = new Eficacia.CrcStream(null);
            }
            return CrcStream_externalReference ;
         }

         set {
            CrcStream_externalReference = (Eficacia.CrcStream)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Eficacia.CrcStream CrcStream_externalReference=null ;
   }

}
