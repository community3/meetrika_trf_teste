/*
               File: PRC_SsToHhMm
        Description: Segundos para Horas e minutos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:26.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sstohhmm : GXProcedure
   {
      public prc_sstohhmm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sstohhmm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Segundos ,
                           ref int aP1_Horas ,
                           out int aP2_Minutos )
      {
         this.AV8Segundos = aP0_Segundos;
         this.AV9Horas = aP1_Horas;
         this.AV11Minutos = 0 ;
         initialize();
         executePrivate();
         aP0_Segundos=this.AV8Segundos;
         aP1_Horas=this.AV9Horas;
         aP2_Minutos=this.AV11Minutos;
      }

      public int executeUdp( ref int aP0_Segundos ,
                             ref int aP1_Horas )
      {
         this.AV8Segundos = aP0_Segundos;
         this.AV9Horas = aP1_Horas;
         this.AV11Minutos = 0 ;
         initialize();
         executePrivate();
         aP0_Segundos=this.AV8Segundos;
         aP1_Horas=this.AV9Horas;
         aP2_Minutos=this.AV11Minutos;
         return AV11Minutos ;
      }

      public void executeSubmit( ref int aP0_Segundos ,
                                 ref int aP1_Horas ,
                                 out int aP2_Minutos )
      {
         prc_sstohhmm objprc_sstohhmm;
         objprc_sstohhmm = new prc_sstohhmm();
         objprc_sstohhmm.AV8Segundos = aP0_Segundos;
         objprc_sstohhmm.AV9Horas = aP1_Horas;
         objprc_sstohhmm.AV11Minutos = 0 ;
         objprc_sstohhmm.context.SetSubmitInitialConfig(context);
         objprc_sstohhmm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sstohhmm);
         aP0_Segundos=this.AV8Segundos;
         aP1_Horas=this.AV9Horas;
         aP2_Minutos=this.AV11Minutos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sstohhmm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV8Segundos >= 3600 )
         {
            AV13Hor = (decimal)((AV8Segundos/ (decimal)(60)/ (decimal)(60))-AV9Horas);
            AV12Min = (decimal)((AV13Hor-NumberUtil.Int( (long)(AV13Hor)))*60);
            AV9Horas = (int)(NumberUtil.Int( (long)(AV13Hor)));
            AV11Minutos = (int)(NumberUtil.Round( AV12Min, 0));
         }
         else
         {
            if ( AV8Segundos >= 60 )
            {
               AV12Min = (decimal)(AV8Segundos/ (decimal)(60));
               AV11Minutos = (int)(NumberUtil.Round( AV12Min, 0));
            }
         }
         AV11Minutos = (int)(AV11Minutos+(AV9Horas*60));
         if ( (0==AV11Minutos) )
         {
            AV11Minutos = 1;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Segundos ;
      private int AV9Horas ;
      private int AV11Minutos ;
      private decimal AV13Hor ;
      private decimal AV12Min ;
      private int aP0_Segundos ;
      private int aP1_Horas ;
      private int aP2_Minutos ;
   }

}
