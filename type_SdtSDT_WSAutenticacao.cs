/*
               File: type_SdtSDT_WSAutenticacao
        Description: SDT_WSAutenticacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "autenticacao" )]
   [XmlType(TypeName =  "autenticacao" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_WSAutenticacao : GxUserType
   {
      public SdtSDT_WSAutenticacao( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WSAutenticacao_Usuario = "";
         gxTv_SdtSDT_WSAutenticacao_Senha = "";
      }

      public SdtSDT_WSAutenticacao( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WSAutenticacao deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WSAutenticacao)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WSAutenticacao obj ;
         obj = this;
         obj.gxTpr_Usuario = deserialized.gxTpr_Usuario;
         obj.gxTpr_Senha = deserialized.gxTpr_Senha;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "usuario") )
               {
                  gxTv_SdtSDT_WSAutenticacao_Usuario = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "senha") )
               {
                  gxTv_SdtSDT_WSAutenticacao_Senha = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "autenticacao";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("usuario", StringUtil.RTrim( gxTv_SdtSDT_WSAutenticacao_Usuario));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("senha", StringUtil.RTrim( gxTv_SdtSDT_WSAutenticacao_Senha));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("usuario", gxTv_SdtSDT_WSAutenticacao_Usuario, false);
         AddObjectProperty("senha", gxTv_SdtSDT_WSAutenticacao_Senha, false);
         return  ;
      }

      [  SoapElement( ElementName = "usuario" )]
      [  XmlElement( ElementName = "usuario"   )]
      public String gxTpr_Usuario
      {
         get {
            return gxTv_SdtSDT_WSAutenticacao_Usuario ;
         }

         set {
            gxTv_SdtSDT_WSAutenticacao_Usuario = (String)(value);
         }

      }

      [  SoapElement( ElementName = "senha" )]
      [  XmlElement( ElementName = "senha"   )]
      public String gxTpr_Senha
      {
         get {
            return gxTv_SdtSDT_WSAutenticacao_Senha ;
         }

         set {
            gxTv_SdtSDT_WSAutenticacao_Senha = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WSAutenticacao_Usuario = "";
         gxTv_SdtSDT_WSAutenticacao_Senha = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_WSAutenticacao_Senha ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_WSAutenticacao_Usuario ;
   }

   [DataContract(Name = @"SDT_WSAutenticacao", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_WSAutenticacao_RESTInterface : GxGenericCollectionItem<SdtSDT_WSAutenticacao>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WSAutenticacao_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WSAutenticacao_RESTInterface( SdtSDT_WSAutenticacao psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "usuario" , Order = 0 )]
      public String gxTpr_Usuario
      {
         get {
            return sdt.gxTpr_Usuario ;
         }

         set {
            sdt.gxTpr_Usuario = (String)(value);
         }

      }

      [DataMember( Name = "senha" , Order = 1 )]
      public String gxTpr_Senha
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Senha) ;
         }

         set {
            sdt.gxTpr_Senha = (String)(value);
         }

      }

      public SdtSDT_WSAutenticacao sdt
      {
         get {
            return (SdtSDT_WSAutenticacao)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WSAutenticacao() ;
         }
      }

   }

}
