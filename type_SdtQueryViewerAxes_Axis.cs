/*
               File: type_SdtQueryViewerAxes_Axis
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_Filter ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_ExpandCollapse ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_AxisOrder ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_Format ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_Grouping ))]
   [Serializable]
   public class SdtQueryViewerAxes_Axis : GxUserType
   {
      public SdtQueryViewerAxes_Axis( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_Name = "";
         gxTv_SdtQueryViewerAxes_Axis_Title = "";
         gxTv_SdtQueryViewerAxes_Axis_Datafield = "";
         gxTv_SdtQueryViewerAxes_Axis_Type = "";
      }

      public SdtQueryViewerAxes_Axis( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Title = deserialized.gxTpr_Title;
         obj.gxTpr_Datafield = deserialized.gxTpr_Datafield;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Filter = deserialized.gxTpr_Filter;
         obj.gxTpr_Expandcollapse = deserialized.gxTpr_Expandcollapse;
         obj.gxTpr_Axisorder = deserialized.gxTpr_Axisorder;
         obj.gxTpr_Format = deserialized.gxTpr_Format;
         obj.gxTpr_Grouping = deserialized.gxTpr_Grouping;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Title") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Title = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataField") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Datafield = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Type") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Type = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Filter") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Filter == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Filter = new SdtQueryViewerAxes_Axis_Filter(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Filter.readxml(oReader, "Filter");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ExpandCollapse") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Expandcollapse == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Expandcollapse = new SdtQueryViewerAxes_Axis_ExpandCollapse(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Expandcollapse.readxml(oReader, "ExpandCollapse");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AxisOrder") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Axisorder == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Axisorder = new SdtQueryViewerAxes_Axis_AxisOrder(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Axisorder.readxml(oReader, "AxisOrder");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Format") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Format == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Format = new SdtQueryViewerAxes_Axis_Format(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Format.readxml(oReader, "Format");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Grouping") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Grouping == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Grouping = new SdtQueryViewerAxes_Axis_Grouping(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Grouping.readxml(oReader, "Grouping");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Title", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Title));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("DataField", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Datafield));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Type", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Type));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Filter != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_Filter.writexml(oWriter, "Filter", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Expandcollapse != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_Expandcollapse.writexml(oWriter, "ExpandCollapse", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Axisorder != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_Axisorder.writexml(oWriter, "AxisOrder", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Format != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_Format.writexml(oWriter, "Format", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Grouping != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_Grouping.writexml(oWriter, "Grouping", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerAxes_Axis_Name, false);
         AddObjectProperty("Title", gxTv_SdtQueryViewerAxes_Axis_Title, false);
         AddObjectProperty("DataField", gxTv_SdtQueryViewerAxes_Axis_Datafield, false);
         AddObjectProperty("Type", gxTv_SdtQueryViewerAxes_Axis_Type, false);
         if ( gxTv_SdtQueryViewerAxes_Axis_Filter != null )
         {
            AddObjectProperty("Filter", gxTv_SdtQueryViewerAxes_Axis_Filter, false);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Expandcollapse != null )
         {
            AddObjectProperty("ExpandCollapse", gxTv_SdtQueryViewerAxes_Axis_Expandcollapse, false);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Axisorder != null )
         {
            AddObjectProperty("AxisOrder", gxTv_SdtQueryViewerAxes_Axis_Axisorder, false);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Format != null )
         {
            AddObjectProperty("Format", gxTv_SdtQueryViewerAxes_Axis_Format, false);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Grouping != null )
         {
            AddObjectProperty("Grouping", gxTv_SdtQueryViewerAxes_Axis_Grouping, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Name ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Title" )]
      [  XmlElement( ElementName = "Title"   )]
      public String gxTpr_Title
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Title ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Title = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DataField" )]
      [  XmlElement( ElementName = "DataField"   )]
      public String gxTpr_Datafield
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Datafield ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Datafield = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Type" )]
      [  XmlElement( ElementName = "Type"   )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Type ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Type = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Filter" )]
      [  XmlElement( ElementName = "Filter"   )]
      public SdtQueryViewerAxes_Axis_Filter gxTpr_Filter
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Filter == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Filter = new SdtQueryViewerAxes_Axis_Filter(context);
            }
            return gxTv_SdtQueryViewerAxes_Axis_Filter ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Filter = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Filter_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Filter = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Filter_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Filter == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "ExpandCollapse" )]
      [  XmlElement( ElementName = "ExpandCollapse"   )]
      public SdtQueryViewerAxes_Axis_ExpandCollapse gxTpr_Expandcollapse
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Expandcollapse == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Expandcollapse = new SdtQueryViewerAxes_Axis_ExpandCollapse(context);
            }
            return gxTv_SdtQueryViewerAxes_Axis_Expandcollapse ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Expandcollapse = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Expandcollapse_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Expandcollapse = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Expandcollapse_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Expandcollapse == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "AxisOrder" )]
      [  XmlElement( ElementName = "AxisOrder"   )]
      public SdtQueryViewerAxes_Axis_AxisOrder gxTpr_Axisorder
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Axisorder == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Axisorder = new SdtQueryViewerAxes_Axis_AxisOrder(context);
            }
            return gxTv_SdtQueryViewerAxes_Axis_Axisorder ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Axisorder = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Axisorder_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Axisorder = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Axisorder_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Axisorder == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Format" )]
      [  XmlElement( ElementName = "Format"   )]
      public SdtQueryViewerAxes_Axis_Format gxTpr_Format
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format = new SdtQueryViewerAxes_Axis_Format(context);
            }
            return gxTv_SdtQueryViewerAxes_Axis_Format ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Format_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Format_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Format == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Grouping" )]
      [  XmlElement( ElementName = "Grouping"   )]
      public SdtQueryViewerAxes_Axis_Grouping gxTpr_Grouping
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Grouping == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Grouping = new SdtQueryViewerAxes_Axis_Grouping(context);
            }
            return gxTv_SdtQueryViewerAxes_Axis_Grouping ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Grouping_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Grouping = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Grouping_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Grouping == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Name = "";
         gxTv_SdtQueryViewerAxes_Axis_Title = "";
         gxTv_SdtQueryViewerAxes_Axis_Datafield = "";
         gxTv_SdtQueryViewerAxes_Axis_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Name ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Title ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Datafield ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Type ;
      protected String sTagName ;
      protected SdtQueryViewerAxes_Axis_Filter gxTv_SdtQueryViewerAxes_Axis_Filter=null ;
      protected SdtQueryViewerAxes_Axis_ExpandCollapse gxTv_SdtQueryViewerAxes_Axis_Expandcollapse=null ;
      protected SdtQueryViewerAxes_Axis_AxisOrder gxTv_SdtQueryViewerAxes_Axis_Axisorder=null ;
      protected SdtQueryViewerAxes_Axis_Format gxTv_SdtQueryViewerAxes_Axis_Format=null ;
      protected SdtQueryViewerAxes_Axis_Grouping gxTv_SdtQueryViewerAxes_Axis_Grouping=null ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerAxes_Axis_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_RESTInterface( SdtQueryViewerAxes_Axis psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Title" , Order = 1 )]
      public String gxTpr_Title
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Title) ;
         }

         set {
            sdt.gxTpr_Title = (String)(value);
         }

      }

      [DataMember( Name = "DataField" , Order = 2 )]
      public String gxTpr_Datafield
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Datafield) ;
         }

         set {
            sdt.gxTpr_Datafield = (String)(value);
         }

      }

      [DataMember( Name = "Type" , Order = 3 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "Filter" , Order = 4 )]
      public SdtQueryViewerAxes_Axis_Filter_RESTInterface gxTpr_Filter
      {
         get {
            return new SdtQueryViewerAxes_Axis_Filter_RESTInterface(sdt.gxTpr_Filter) ;
         }

         set {
            sdt.gxTpr_Filter = value.sdt;
         }

      }

      [DataMember( Name = "ExpandCollapse" , Order = 5 )]
      public SdtQueryViewerAxes_Axis_ExpandCollapse_RESTInterface gxTpr_Expandcollapse
      {
         get {
            return new SdtQueryViewerAxes_Axis_ExpandCollapse_RESTInterface(sdt.gxTpr_Expandcollapse) ;
         }

         set {
            sdt.gxTpr_Expandcollapse = value.sdt;
         }

      }

      [DataMember( Name = "AxisOrder" , Order = 6 )]
      public SdtQueryViewerAxes_Axis_AxisOrder_RESTInterface gxTpr_Axisorder
      {
         get {
            return new SdtQueryViewerAxes_Axis_AxisOrder_RESTInterface(sdt.gxTpr_Axisorder) ;
         }

         set {
            sdt.gxTpr_Axisorder = value.sdt;
         }

      }

      [DataMember( Name = "Format" , Order = 7 )]
      public SdtQueryViewerAxes_Axis_Format_RESTInterface gxTpr_Format
      {
         get {
            return new SdtQueryViewerAxes_Axis_Format_RESTInterface(sdt.gxTpr_Format) ;
         }

         set {
            sdt.gxTpr_Format = value.sdt;
         }

      }

      [DataMember( Name = "Grouping" , Order = 8 )]
      public SdtQueryViewerAxes_Axis_Grouping_RESTInterface gxTpr_Grouping
      {
         get {
            return new SdtQueryViewerAxes_Axis_Grouping_RESTInterface(sdt.gxTpr_Grouping) ;
         }

         set {
            sdt.gxTpr_Grouping = value.sdt;
         }

      }

      public SdtQueryViewerAxes_Axis sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis() ;
         }
      }

   }

}
