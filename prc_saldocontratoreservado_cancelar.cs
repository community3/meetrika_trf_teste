/*
               File: Prc_SaldoContratoReservado_Cancelar
        Description: Cancela Saldo Reserva
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:55.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratoreservado_cancelar : GXProcedure
   {
      public prc_saldocontratoreservado_cancelar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratoreservado_cancelar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contrato_Codigo )
      {
         prc_saldocontratoreservado_cancelar objprc_saldocontratoreservado_cancelar;
         objprc_saldocontratoreservado_cancelar = new prc_saldocontratoreservado_cancelar();
         objprc_saldocontratoreservado_cancelar.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_saldocontratoreservado_cancelar.context.SetSubmitInitialConfig(context);
         objprc_saldocontratoreservado_cancelar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratoreservado_cancelar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratoreservado_cancelar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00D42 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
         dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
         /* End optimized UPDATE. */
         /* Using cursor P00D43 */
         pr_default.execute(1, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1781SaldoContrato_Ativo = P00D43_A1781SaldoContrato_Ativo[0];
            A74Contrato_Codigo = P00D43_A74Contrato_Codigo[0];
            A1561SaldoContrato_Codigo = P00D43_A1561SaldoContrato_Codigo[0];
            A1574SaldoContrato_Reservado = P00D43_A1574SaldoContrato_Reservado[0];
            A1576SaldoContrato_Saldo = P00D43_A1576SaldoContrato_Saldo[0];
            AV9SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            AV10SaldoContrato_Reservado = A1574SaldoContrato_Reservado;
            new prc_historicoconsumo(context ).execute(  AV9SaldoContrato_Codigo,  AV8Contrato_Codigo,  0,  0,  0,  AV10SaldoContrato_Reservado,  "CRS") ;
            new prc_historicoconsumo(context ).execute(  AV9SaldoContrato_Codigo,  AV8Contrato_Codigo,  0,  0,  0,  AV10SaldoContrato_Reservado,  "CRE") ;
            A1574SaldoContrato_Reservado = (decimal)(A1574SaldoContrato_Reservado-AV10SaldoContrato_Reservado);
            A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo+AV10SaldoContrato_Reservado);
            /* Using cursor P00D44 */
            pr_default.execute(2, new Object[] {A1574SaldoContrato_Reservado, A1576SaldoContrato_Saldo, A1561SaldoContrato_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "Prc_SaldoContratoReservado_Cancelar");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D43_A1781SaldoContrato_Ativo = new bool[] {false} ;
         P00D43_A74Contrato_Codigo = new int[1] ;
         P00D43_A1561SaldoContrato_Codigo = new int[1] ;
         P00D43_A1574SaldoContrato_Reservado = new decimal[1] ;
         P00D43_A1576SaldoContrato_Saldo = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratoreservado_cancelar__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               P00D43_A1781SaldoContrato_Ativo, P00D43_A74Contrato_Codigo, P00D43_A1561SaldoContrato_Codigo, P00D43_A1574SaldoContrato_Reservado, P00D43_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int AV9SaldoContrato_Codigo ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal AV10SaldoContrato_Reservado ;
      private String scmdbuf ;
      private bool A1781SaldoContrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00D43_A1781SaldoContrato_Ativo ;
      private int[] P00D43_A74Contrato_Codigo ;
      private int[] P00D43_A1561SaldoContrato_Codigo ;
      private decimal[] P00D43_A1574SaldoContrato_Reservado ;
      private decimal[] P00D43_A1576SaldoContrato_Saldo ;
   }

   public class prc_saldocontratoreservado_cancelar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D42 ;
          prmP00D42 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00D43 ;
          prmP00D43 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00D44 ;
          prmP00D44 = new Object[] {
          new Object[] {"@SaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D42", "UPDATE [AutorizacaoConsumo] SET [AutorizacaoConsumo_Ativo]=CONVERT(BIT, 0)  WHERE ([Contrato_Codigo] = @AV8Contrato_Codigo) AND ([AutorizacaoConsumo_Ativo] = 1)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00D42)
             ,new CursorDef("P00D43", "SELECT [SaldoContrato_Ativo], [Contrato_Codigo], [SaldoContrato_Codigo], [SaldoContrato_Reservado], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (UPDLOCK) WHERE ([Contrato_Codigo] = @AV8Contrato_Codigo) AND ([SaldoContrato_Ativo] = 1) ORDER BY [Contrato_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D43,1,0,true,false )
             ,new CursorDef("P00D44", "UPDATE [SaldoContrato] SET [SaldoContrato_Reservado]=@SaldoContrato_Reservado, [SaldoContrato_Saldo]=@SaldoContrato_Saldo  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00D44)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
