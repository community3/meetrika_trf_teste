/*
               File: WC_TotaisContador
        Description: Totais Contador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:18.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_totaiscontador : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_totaiscontador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_totaiscontador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_2_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_2_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n52Contratada_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6Codigos);
                  A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  A512ContagemResultado_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
                  n512ContagemResultado_ValorPF = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A512ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5)));
                  AV15TotalDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15TotalDmn), 4, 0)));
                  AV13TotalPF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13TotalPF", StringUtil.LTrim( StringUtil.Str( AV13TotalPF, 14, 5)));
                  AV14TotalValor = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalValor", StringUtil.LTrim( StringUtil.Str( AV14TotalValor, 18, 5)));
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  AV11Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_AreaTrabalhoCod), 6, 0)));
                  A6AreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A52Contratada_AreaTrabalhoCod, A456ContagemResultado_Codigo, AV6Codigos, A574ContagemResultado_PFFinal, A512ContagemResultado_ValorPF, AV15TotalDmn, AV13TotalPF, AV14TotalValor, A5AreaTrabalho_Codigo, AV11Contratada_AreaTrabalhoCod, A6AreaTrabalho_Descricao, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAHD2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavAreatrabalho_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
               edtavQtde_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
               edtavPffinal_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
               edtavValor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
               WSHD2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Totais Contador") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216111886");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_totaiscontador.aspx") +"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_2", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCODIGOS", AV6Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCODIGOS", AV6Codigos);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( A512ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTOTALDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15TotalDmn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTOTALPF", StringUtil.LTrim( StringUtil.NToC( AV13TotalPF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTOTALVALOR", StringUtil.LTrim( StringUtil.NToC( AV14TotalValor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormHD2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_totaiscontador.js", "?20206216111888");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_TotaisContador" ;
      }

      public override String GetPgmdesc( )
      {
         return "Totais Contador" ;
      }

      protected void WBHD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_totaiscontador.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"2\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtde") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "R$") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcoloreven), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV10AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Qtde), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtde_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV9PFFinal, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPffinal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV5Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValor_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 2 )
         {
            wbEnd = 0;
            nRC_GXsfl_2 = (short)(nGXsfl_2_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
         }
         wbLoad = true;
      }

      protected void STARTHD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Totais Contador", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPHD0( ) ;
            }
         }
      }

      protected void WSHD2( )
      {
         STARTHD2( ) ;
         EVTHD2( ) ;
      }

      protected void EVTHD2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHD0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHD0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHD0( ) ;
                              }
                              nGXsfl_2_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
                              SubsflControlProps_22( ) ;
                              AV10AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAreatrabalho_descricao_Internalname, AV10AreaTrabalho_Descricao);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!"))));
                              AV12Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Qtde), 4, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
                              AV9PFFinal = context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV9PFFinal, 14, 5)));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
                              AV5Valor = context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV5Valor, 18, 5)));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vVALOR"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E11HD2 */
                                          E11HD2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E12HD2 */
                                          E12HD2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPHD0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormHD2( ) ;
            }
         }
      }

      protected void PAHD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_22( ) ;
         while ( nGXsfl_2_idx <= nRC_GXsfl_2 )
         {
            sendrow_22( ) ;
            nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
            sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
            SubsflControlProps_22( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A52Contratada_AreaTrabalhoCod ,
                                       int A456ContagemResultado_Codigo ,
                                       IGxCollection AV6Codigos ,
                                       decimal A574ContagemResultado_PFFinal ,
                                       decimal A512ContagemResultado_ValorPF ,
                                       short AV15TotalDmn ,
                                       decimal AV13TotalPF ,
                                       decimal AV14TotalValor ,
                                       int A5AreaTrabalho_Codigo ,
                                       int AV11Contratada_AreaTrabalhoCod ,
                                       String A6AreaTrabalho_Descricao ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFHD2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAREATRABALHO_DESCRICAO", AV10AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vQTDE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Qtde), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFFINAL", StringUtil.LTrim( StringUtil.NToC( AV9PFFinal, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vVALOR", StringUtil.LTrim( StringUtil.NToC( AV5Valor, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
      }

      protected void RFHD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 2;
         /* Execute user event: E11HD2 */
         E11HD2 ();
         nGXsfl_2_idx = 1;
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         nGXsfl_2_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcoloreven), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_22( ) ;
            /* Execute user event: E12HD2 */
            E12HD2 ();
            wbEnd = 2;
            WBHD0( ) ;
         }
         nGXsfl_2_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPHD0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_2 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_2"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void E11HD2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV6Codigos.FromXml(AV7WebSession.Get("Codigos"), "Collection");
         AV15TotalDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15TotalDmn), 4, 0)));
         AV13TotalPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13TotalPF", StringUtil.LTrim( StringUtil.Str( AV13TotalPF, 14, 5)));
         AV14TotalValor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalValor", StringUtil.LTrim( StringUtil.Str( AV14TotalValor, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6Codigos", AV6Codigos);
      }

      private void E12HD2( )
      {
         /* Load Routine */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV6Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00HD2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKHD3 = false;
            A490ContagemResultado_ContratadaCod = H00HD2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00HD2_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00HD2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00HD2_n52Contratada_AreaTrabalhoCod[0];
            A512ContagemResultado_ValorPF = H00HD2_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00HD2_n512ContagemResultado_ValorPF[0];
            A456ContagemResultado_Codigo = H00HD2_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00HD2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00HD2_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV11Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_AreaTrabalhoCod), 6, 0)));
            /* Execute user subroutine: 'GETAREATRABALHO' */
            S113 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV9PFFinal = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV9PFFinal, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
            AV5Valor = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV5Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vVALOR"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            AV12Qtde = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Qtde), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
            while ( (pr_default.getStatus(0) != 101) && ( H00HD2_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) )
            {
               BRKHD3 = false;
               A490ContagemResultado_ContratadaCod = H00HD2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00HD2_n490ContagemResultado_ContratadaCod[0];
               A512ContagemResultado_ValorPF = H00HD2_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = H00HD2_n512ContagemResultado_ValorPF[0];
               A456ContagemResultado_Codigo = H00HD2_A456ContagemResultado_Codigo[0];
               if ( (AV6Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
               {
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  AV9PFFinal = (decimal)(AV9PFFinal+A574ContagemResultado_PFFinal);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV9PFFinal, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
                  AV5Valor = (decimal)(AV5Valor+(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV5Valor, 18, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vVALOR"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                  AV12Qtde = (short)(AV12Qtde+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Qtde), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
               }
               BRKHD3 = true;
               pr_default.readNext(0);
            }
            AV15TotalDmn = (short)(AV15TotalDmn+AV12Qtde);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15TotalDmn), 4, 0)));
            AV13TotalPF = (decimal)(AV13TotalPF+AV9PFFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13TotalPF", StringUtil.LTrim( StringUtil.Str( AV13TotalPF, 14, 5)));
            AV14TotalValor = (decimal)(AV14TotalValor+AV5Valor);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalValor", StringUtil.LTrim( StringUtil.Str( AV14TotalValor, 18, 5)));
            sendrow_22( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_2_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(2, GridRow);
            }
            if ( ! BRKHD3 )
            {
               BRKHD3 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
         AV10AreaTrabalho_Descricao = "T O T A I S";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAreatrabalho_descricao_Internalname, AV10AreaTrabalho_Descricao);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!"))));
         AV12Qtde = AV15TotalDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Qtde), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
         AV9PFFinal = AV13TotalPF;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV9PFFinal, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
         AV5Valor = AV14TotalValor;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV5Valor, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vVALOR"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         sendrow_22( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_2_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(2, GridRow);
         }
      }

      protected void S113( )
      {
         /* 'GETAREATRABALHO' Routine */
         /* Using cursor H00HD3 */
         pr_default.execute(1, new Object[] {AV11Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A5AreaTrabalho_Codigo = H00HD3_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = H00HD3_A6AreaTrabalho_Descricao[0];
            AV10AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAreatrabalho_descricao_Internalname, AV10AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!"))));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHD2( ) ;
         WSHD2( ) ;
         WEHD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAHD2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_totaiscontador");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAHD2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAHD2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSHD2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSHD2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEHD2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216111915");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_totaiscontador.js", "?20206216111915");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_22( )
      {
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO_"+sGXsfl_2_idx;
         edtavQtde_Internalname = sPrefix+"vQTDE_"+sGXsfl_2_idx;
         edtavPffinal_Internalname = sPrefix+"vPFFINAL_"+sGXsfl_2_idx;
         edtavValor_Internalname = sPrefix+"vVALOR_"+sGXsfl_2_idx;
      }

      protected void SubsflControlProps_fel_22( )
      {
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO_"+sGXsfl_2_fel_idx;
         edtavQtde_Internalname = sPrefix+"vQTDE_"+sGXsfl_2_fel_idx;
         edtavPffinal_Internalname = sPrefix+"vPFFINAL_"+sGXsfl_2_fel_idx;
         edtavValor_Internalname = sPrefix+"vVALOR_"+sGXsfl_2_fel_idx;
      }

      protected void sendrow_22( )
      {
         SubsflControlProps_22( ) ;
         WBHD0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF5F5F5);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_2_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF5F5F5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_2_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreatrabalho_descricao_Internalname,(String)AV10AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreatrabalho_descricao_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavAreatrabalho_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtde_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Qtde), 4, 0, ",", "")),((edtavQtde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtde_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavQtde_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPffinal_Internalname,StringUtil.LTrim( StringUtil.NToC( AV9PFFinal, 14, 5, ",", "")),((edtavPffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPffinal_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavPffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavValor_Internalname,StringUtil.LTrim( StringUtil.NToC( AV5Valor, 18, 5, ",", "")),((edtavValor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavValor_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavValor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV10AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV12Qtde), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFFINAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV9PFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVALOR"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( AV5Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GridContainer.AddRow(GridRow);
         nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         /* End function sendrow_22 */
      }

      protected void init_default_properties( )
      {
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO";
         edtavQtde_Internalname = sPrefix+"vQTDE";
         edtavPffinal_Internalname = sPrefix+"vPFFINAL";
         edtavValor_Internalname = sPrefix+"vVALOR";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavValor_Jsonclick = "";
         edtavPffinal_Jsonclick = "";
         edtavQtde_Jsonclick = "";
         edtavAreatrabalho_descricao_Jsonclick = "";
         subGrid_Backcolor = (int)(0x0);
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavValor_Enabled = 0;
         edtavPffinal_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcoloreven = (int)(0xFFFFFF);
         subGrid_Backcolorodd = (int)(0xF5F5F5);
         subGrid_Class = "Grid";
         subGrid_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV15TotalDmn',fld:'vTOTALDMN',pic:'ZZZ9',nv:0},{av:'AV13TotalPF',fld:'vTOTALPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14TotalValor',fld:'vTOTALVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV15TotalDmn',fld:'vTOTALDMN',pic:'ZZZ9',nv:0},{av:'AV13TotalPF',fld:'vTOTALPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14TotalValor',fld:'vTOTALVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV6Codigos = new GxSimpleCollection();
         A6AreaTrabalho_Descricao = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         GridContainer = new GXWebGrid( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         AV10AreaTrabalho_Descricao = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7WebSession = context.GetSession();
         scmdbuf = "";
         H00HD2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00HD2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00HD2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00HD2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00HD2_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00HD2_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00HD2_A456ContagemResultado_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         H00HD3_A5AreaTrabalho_Codigo = new int[1] ;
         H00HD3_A6AreaTrabalho_Descricao = new String[] {""} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_totaiscontador__default(),
            new Object[][] {
                new Object[] {
               H00HD2_A490ContagemResultado_ContratadaCod, H00HD2_n490ContagemResultado_ContratadaCod, H00HD2_A52Contratada_AreaTrabalhoCod, H00HD2_n52Contratada_AreaTrabalhoCod, H00HD2_A512ContagemResultado_ValorPF, H00HD2_n512ContagemResultado_ValorPF, H00HD2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00HD3_A5AreaTrabalho_Codigo, H00HD3_A6AreaTrabalho_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavPffinal_Enabled = 0;
         edtavValor_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_2 ;
      private short nGXsfl_2_idx=1 ;
      private short AV15TotalDmn ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short AV12Qtde ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_2_Refreshing=0 ;
      private short GRID_nEOF ;
      private short subGrid_Backstyle ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A456ContagemResultado_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV11Contratada_AreaTrabalhoCod ;
      private int edtavAreatrabalho_descricao_Enabled ;
      private int edtavQtde_Enabled ;
      private int edtavPffinal_Enabled ;
      private int edtavValor_Enabled ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Backcolorodd ;
      private int subGrid_Backcoloreven ;
      private int subGrid_Titleforecolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int subGrid_Islastpage ;
      private int A490ContagemResultado_ContratadaCod ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV13TotalPF ;
      private decimal AV14TotalValor ;
      private decimal AV9PFFinal ;
      private decimal AV5Valor ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_2_idx="0001" ;
      private String GXKey ;
      private String edtavAreatrabalho_descricao_Internalname ;
      private String edtavQtde_Internalname ;
      private String edtavPffinal_Internalname ;
      private String edtavValor_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sStyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String sGXsfl_2_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavAreatrabalho_descricao_Jsonclick ;
      private String edtavQtde_Jsonclick ;
      private String edtavPffinal_Jsonclick ;
      private String edtavValor_Jsonclick ;
      private bool entryPointCalled ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool gx_refresh_fired ;
      private bool BRKHD3 ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool returnInSub ;
      private String A6AreaTrabalho_Descricao ;
      private String AV10AreaTrabalho_Descricao ;
      private IGxSession AV7WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00HD2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00HD2_n490ContagemResultado_ContratadaCod ;
      private int[] H00HD2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00HD2_n52Contratada_AreaTrabalhoCod ;
      private decimal[] H00HD2_A512ContagemResultado_ValorPF ;
      private bool[] H00HD2_n512ContagemResultado_ValorPF ;
      private int[] H00HD2_A456ContagemResultado_Codigo ;
      private int[] H00HD3_A5AreaTrabalho_Codigo ;
      private String[] H00HD3_A6AreaTrabalho_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV6Codigos ;
   }

   public class wc_totaiscontador__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00HD2( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV6Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod]";
         GXv_Object2[0] = scmdbuf;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00HD2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HD3 ;
          prmH00HD3 = new Object[] {
          new Object[] {"@AV11Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HD2 ;
          prmH00HD2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HD2,100,0,true,false )
             ,new CursorDef("H00HD3", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV11Contratada_AreaTrabalhoCod ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HD3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
