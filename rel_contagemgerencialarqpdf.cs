/*
               File: REL_ContagemGerencialArqPDF
        Description: Contagem Gerencial Arq PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:49.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemgerencialarqpdf : GXProcedure
   {
      public rel_contagemgerencialarqpdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_contagemgerencialarqpdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_LoteAceite ,
                           String aP2_QrCodePath ,
                           String aP3_QrCodeUrl ,
                           String aP4_Verificador ,
                           String aP5_FileName ,
                           String aP6_Filtro ,
                           DateTime aP7_Inicio ,
                           DateTime aP8_Fim )
      {
         this.AV28Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV15ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         this.AV55QrCodePath = aP2_QrCodePath;
         this.AV56QrCodeUrl = aP3_QrCodeUrl;
         this.AV66Verificador = aP4_Verificador;
         this.AV38FileName = aP5_FileName;
         this.AV133Filtro = aP6_Filtro;
         this.AV93Inicio = aP7_Inicio;
         this.AV132Fim = aP8_Fim;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_LoteAceite ,
                                 String aP2_QrCodePath ,
                                 String aP3_QrCodeUrl ,
                                 String aP4_Verificador ,
                                 String aP5_FileName ,
                                 String aP6_Filtro ,
                                 DateTime aP7_Inicio ,
                                 DateTime aP8_Fim )
      {
         rel_contagemgerencialarqpdf objrel_contagemgerencialarqpdf;
         objrel_contagemgerencialarqpdf = new rel_contagemgerencialarqpdf();
         objrel_contagemgerencialarqpdf.AV28Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemgerencialarqpdf.AV15ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         objrel_contagemgerencialarqpdf.AV55QrCodePath = aP2_QrCodePath;
         objrel_contagemgerencialarqpdf.AV56QrCodeUrl = aP3_QrCodeUrl;
         objrel_contagemgerencialarqpdf.AV66Verificador = aP4_Verificador;
         objrel_contagemgerencialarqpdf.AV38FileName = aP5_FileName;
         objrel_contagemgerencialarqpdf.AV133Filtro = aP6_Filtro;
         objrel_contagemgerencialarqpdf.AV93Inicio = aP7_Inicio;
         objrel_contagemgerencialarqpdf.AV132Fim = aP8_Fim;
         objrel_contagemgerencialarqpdf.context.SetSubmitInitialConfig(context);
         objrel_contagemgerencialarqpdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemgerencialarqpdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemgerencialarqpdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName(AV38FileName) ;
         getPrinter().GxSetDocFormat("PDF") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV68WWPContext) ;
            AV73ContratanteArea = StringUtil.Trim( AV68WWPContext.gxTpr_Contratante_nomefantasia);
            GXt_boolean1 = AV50OSAutomatica;
            GXt_int2 = AV68WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_osautomatica(context ).execute( ref  GXt_int2, out  GXt_boolean1) ;
            AV68WWPContext.gxTpr_Areatrabalho_codigo = GXt_int2;
            AV50OSAutomatica = GXt_boolean1;
            AV62TextoLink = StringUtil.Trim( AV56QrCodeUrl) + ", informando o c�digo " + StringUtil.Trim( AV66Verificador);
            AV36DescricaoLength = 50;
            AV89DescricaoGlsLength = 100;
            /* Using cursor P00983 */
            pr_default.execute(0, new Object[] {AV15ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1620ContagemResultado_CntSrvUndCnt = P00983_A1620ContagemResultado_CntSrvUndCnt[0];
               n1620ContagemResultado_CntSrvUndCnt = P00983_n1620ContagemResultado_CntSrvUndCnt[0];
               A456ContagemResultado_Codigo = P00983_A456ContagemResultado_Codigo[0];
               A597ContagemResultado_LoteAceiteCod = P00983_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00983_n597ContagemResultado_LoteAceiteCod[0];
               A490ContagemResultado_ContratadaCod = P00983_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00983_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P00983_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00983_n601ContagemResultado_Servico[0];
               A1603ContagemResultado_CntCod = P00983_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00983_n1603ContagemResultado_CntCod[0];
               A1553ContagemResultado_CntSrvCod = P00983_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00983_n1553ContagemResultado_CntSrvCod[0];
               A1621ContagemResultado_CntSrvUndCntSgl = P00983_A1621ContagemResultado_CntSrvUndCntSgl[0];
               n1621ContagemResultado_CntSrvUndCntSgl = P00983_n1621ContagemResultado_CntSrvUndCntSgl[0];
               A803ContagemResultado_ContratadaSigla = P00983_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = P00983_n803ContagemResultado_ContratadaSigla[0];
               A566ContagemResultado_DataUltCnt = P00983_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00983_n566ContagemResultado_DataUltCnt[0];
               A566ContagemResultado_DataUltCnt = P00983_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00983_n566ContagemResultado_DataUltCnt[0];
               A803ContagemResultado_ContratadaSigla = P00983_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = P00983_n803ContagemResultado_ContratadaSigla[0];
               A1620ContagemResultado_CntSrvUndCnt = P00983_A1620ContagemResultado_CntSrvUndCnt[0];
               n1620ContagemResultado_CntSrvUndCnt = P00983_n1620ContagemResultado_CntSrvUndCnt[0];
               A601ContagemResultado_Servico = P00983_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00983_n601ContagemResultado_Servico[0];
               A1603ContagemResultado_CntCod = P00983_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00983_n1603ContagemResultado_CntCod[0];
               A1621ContagemResultado_CntSrvUndCntSgl = P00983_A1621ContagemResultado_CntSrvUndCntSgl[0];
               n1621ContagemResultado_CntSrvUndCntSgl = P00983_n1621ContagemResultado_CntSrvUndCntSgl[0];
               AV121Contratada = A490ContagemResultado_ContratadaCod;
               AV111Servico = A601ContagemResultado_Servico;
               AV104PrdFtrIni = A566ContagemResultado_DataUltCnt;
               AV103PrdFtrFim = A566ContagemResultado_DataUltCnt;
               AV87Contrato_Codigo = A1603ContagemResultado_CntCod;
               AV128ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
               AV75UnidadeMedicao_Sigla = A1621ContagemResultado_CntSrvUndCntSgl;
               AV130Contratada_Sigla = A803ContagemResultado_ContratadaSigla;
               /* Execute user subroutine: 'DADOSDOCONTRATO' */
               S241 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            AV61SubTitulo = "";
            AV11AssinadoDtHr = context.localUtil.DToC( Gx_date, 2, "/") + " em " + StringUtil.Substring( Gx_time, 1, 5) + ", conforme hor�rio oficial de Brasilia.";
            AV54qrCode_Image = AV55QrCodePath;
            AV155Qrcode_image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV55QrCodePath);
            /* Using cursor P00986 */
            pr_default.execute(1, new Object[] {AV15ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A563Lote_Nome = P00986_A563Lote_Nome[0];
               A562Lote_Numero = P00986_A562Lote_Numero[0];
               A2055Lote_CntUsado = P00986_A2055Lote_CntUsado[0];
               n2055Lote_CntUsado = P00986_n2055Lote_CntUsado[0];
               A2056Lote_CntSaldo = P00986_A2056Lote_CntSaldo[0];
               n2056Lote_CntSaldo = P00986_n2056Lote_CntSaldo[0];
               A596Lote_Codigo = P00986_A596Lote_Codigo[0];
               A567Lote_DataIni = P00986_A567Lote_DataIni[0];
               A568Lote_DataFim = P00986_A568Lote_DataFim[0];
               A567Lote_DataIni = P00986_A567Lote_DataIni[0];
               A568Lote_DataFim = P00986_A568Lote_DataFim[0];
               AV61SubTitulo = A563Lote_Nome;
               AV30d = (decimal)(StringUtil.Len( A562Lote_Numero)-3);
               AV46NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV30d), 4);
               AV30d = (decimal)(AV30d-2);
               AV46NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV30d), 2) + "/" + AV46NumeroRelatorio;
               AV30d = (decimal)(AV30d-1);
               AV46NumeroRelatorio = "Lote " + StringUtil.Substring( AV46NumeroRelatorio, 4, 4) + "/" + StringUtil.PadL( StringUtil.Substring( A562Lote_Numero, 1, (int)(AV30d)), 3, "0");
               AV33DataInicio = A567Lote_DataIni;
               AV32DataFim = A568Lote_DataFim;
               AV138Usado = A2055Lote_CntUsado;
               AV139Saldo = A2056Lote_CntSaldo;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV133Filtro)) )
            {
               AV131Periodo = "Per�odo";
            }
            else
            {
               AV131Periodo = AV133Filtro;
            }
            AV131Periodo = AV131Periodo + " " + context.localUtil.DToC( AV33DataInicio, 2, "/") + " - " + context.localUtil.DToC( AV32DataFim, 2, "/");
            /* Execute user subroutine: 'PRINTDATALOTE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H980( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTDATALOTE' Routine */
         /* Using cursor P00987 */
         pr_default.execute(2, new Object[] {AV15ContagemResultado_LoteAceite});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00987_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00987_n597ContagemResultado_LoteAceiteCod[0];
            A484ContagemResultado_StatusDmn = P00987_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00987_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P00987_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00987_n1854ContagemResultado_VlrCnc[0];
            A456ContagemResultado_Codigo = P00987_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P00987_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00987_n512ContagemResultado_ValorPF[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            AV119Codigos.Add(A456ContagemResultado_Codigo, 0);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV18ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
            }
            else
            {
               AV18ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
            }
            AV19ContagemResultado_PFTotal = (decimal)(AV19ContagemResultado_PFTotal+A574ContagemResultado_PFFinal);
            AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar+AV18ContagemResultado_PFFaturar);
            AV58Quantidade = (short)(AV58Quantidade+1);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV141SaldoPrev = (decimal)(AV139Saldo-AV19ContagemResultado_PFTotal);
         H980( false, 218) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("consumidos/as:", 150, Gx_line+100, 250, Gx_line+118, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("contratados/as:", 152, Gx_line+67, 250, Gx_line+85, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Contratado:", 136, Gx_line+133, 250, Gx_line+151, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV138Usado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+100, 361, Gx_line+118, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 83, Gx_line+67, 141, Gx_line+85, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 83, Gx_line+100, 141, Gx_line+118, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV140Contratado, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+67, 361, Gx_line+85, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV139Saldo, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+133, 361, Gx_line+151, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 83, Gx_line+167, 141, Gx_line+185, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("usado no Lote:", 157, Gx_line+167, 250, Gx_line+185, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV19ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+167, 361, Gx_line+185, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9Contrato_Numero, "")), 233, Gx_line+17, 359, Gx_line+35, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Contrato N�", 150, Gx_line+17, 222, Gx_line+35, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Saldo Atual:", 174, Gx_line+200, 250, Gx_line+218, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV141SaldoPrev, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+200, 361, Gx_line+218, 2+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+218);
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV50OSAutomatica )
         {
            /* Using cursor P00988 */
            pr_default.execute(3, new Object[] {AV15ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A597ContagemResultado_LoteAceiteCod = P00988_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00988_n597ContagemResultado_LoteAceiteCod[0];
               A456ContagemResultado_Codigo = P00988_A456ContagemResultado_Codigo[0];
               A553ContagemResultado_DemandaFM_ORDER = P00988_A553ContagemResultado_DemandaFM_ORDER[0];
               A493ContagemResultado_DemandaFM = P00988_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00988_n493ContagemResultado_DemandaFM[0];
               AV123ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV14ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV49Os = A493ContagemResultado_DemandaFM;
               /* Execute user subroutine: 'LINHASDAOSLOTE' */
               S127 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         else
         {
            /* Using cursor P00989 */
            pr_default.execute(4, new Object[] {AV15ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A597ContagemResultado_LoteAceiteCod = P00989_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00989_n597ContagemResultado_LoteAceiteCod[0];
               A456ContagemResultado_Codigo = P00989_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = P00989_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00989_n493ContagemResultado_DemandaFM[0];
               AV123ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV14ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV49Os = A493ContagemResultado_DemandaFM;
               AV127SemOSFM = String.IsNullOrEmpty(StringUtil.RTrim( AV49Os));
               /* Execute user subroutine: 'LINHASDAOSLOTE' */
               S127 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         /* Execute user subroutine: 'PRINTFREQUENCIA' */
         S131 ();
         if (returnInSub) return;
         H980( false, 133) ;
         getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+22, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(700, Gx_line+116, 967, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+50, 100, Gx_line+133) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV20ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 925, Gx_line+0, 1067, Gx_line+18, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Valor Total do Per�odo R$", 730, Gx_line+0, 917, Gx_line+19, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Gestor(a) Contratual", 775, Gx_line+116, 891, Gx_line+133, 0, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV11AssinadoDtHr, "")), 133, Gx_line+83, 447, Gx_line+100, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Documento assinado eletronicamente por", 133, Gx_line+67, 374, Gx_line+83, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Preposto, "@!")), 375, Gx_line+67, 1001, Gx_line+84, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+133);
         H980( false, 91) ;
         getPrinter().GxDrawBitMap(AV54qrCode_Image, 0, Gx_line+0, 100, Gx_line+83) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("A autenticidade deste documento pode ser conferida no site", 133, Gx_line+17, 474, Gx_line+33, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV62TextoLink, "")), 133, Gx_line+33, 1072, Gx_line+50, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+91);
         if ( AV115TemIndicadorPntLote )
         {
            if ( AV105Previstas > 0 )
            {
               AV97PntLoteIndice = (decimal)((AV117Atrasadas/ (decimal)(AV105Previstas))*100);
               AV99PntLotePrcSemAtraso = (decimal)((AV88Cumpridas/ (decimal)(AV105Previstas))*100);
            }
            /* Execute user subroutine: 'GETSANCAOINDPNTLOTE' */
            S141 ();
            if (returnInSub) return;
            H980( false, 351) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96PntLoteIndicador, "")), 150, Gx_line+17, 880, Gx_line+35, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("-", 358, Gx_line+83, 366, Gx_line+97, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 433, Gx_line+100, 491, Gx_line+118, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV101PntLoteUndBruto, "ZZ,ZZZ,ZZ9.999")), 317, Gx_line+100, 425, Gx_line+118, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV33DataInicio, "99/99/99"), 300, Gx_line+83, 350, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV32DataFim, "99/99/99"), 375, Gx_line+83, 425, Gx_line+101, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Per�odo:", 100, Gx_line+83, 154, Gx_line+101, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Bruto apurado:", 100, Gx_line+100, 229, Gx_line+118, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV99PntLotePrcSemAtraso, "(ZZ9.99 %)")), 433, Gx_line+183, 507, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("San��o:", 100, Gx_line+233, 153, Gx_line+251, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV109Reduz, "ZZ9.99 %")), 383, Gx_line+233, 442, Gx_line+251, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV97PntLoteIndice, "ZZ9.99 %")), 383, Gx_line+217, 442, Gx_line+235, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117Atrasadas), "ZZZ9")), 383, Gx_line+200, 413, Gx_line+218, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV88Cumpridas), "ZZZ9")), 383, Gx_line+183, 413, Gx_line+201, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105Previstas), "ZZZ9")), 383, Gx_line+167, 413, Gx_line+185, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Indice do indicador:", 100, Gx_line+217, 223, Gx_line+235, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues com atraso:", 100, Gx_line+200, 241, Gx_line+218, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Entregues sem atraso:", 100, Gx_line+183, 241, Gx_line+201, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Previstas:", 100, Gx_line+167, 162, Gx_line+185, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV20ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 283, Gx_line+300, 425, Gx_line+318, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Glosa                                          R$:", 100, Gx_line+317, 292, Gx_line+335, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV98PntLoteLiquido, "ZZZ,ZZZ,ZZ9.99")), 325, Gx_line+333, 428, Gx_line+351, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV100PntLoteSancao, "ZZZZZZZZ9.99")), 333, Gx_line+317, 422, Gx_line+335, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Liquido a ser faturado R$:", 100, Gx_line+333, 296, Gx_line+351, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor Total do Per�odo         R$:", 100, Gx_line+300, 292, Gx_line+318, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Indicador", 75, Gx_line+17, 134, Gx_line+34, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText("Base de C�lculo:", 92, Gx_line+50, 199, Gx_line+67, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Quantidade de Demandas:", 92, Gx_line+133, 262, Gx_line+150, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Valor do Faturamento:", 92, Gx_line+267, 231, Gx_line+284, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+351);
         }
         /* Execute user subroutine: 'PRINTFAIXAS' */
         S151 ();
         if (returnInSub) return;
      }

      protected void S127( )
      {
         /* 'LINHASDAOSLOTE' Routine */
         AV65TotalPontos = 0;
         AV64TotalFinal = 0;
         AV94Linhas = 1;
         if ( AV50OSAutomatica )
         {
            /* Using cursor P009811 */
            pr_default.execute(5, new Object[] {AV123ContagemResultado_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A489ContagemResultado_SistemaCod = P009811_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P009811_n489ContagemResultado_SistemaCod[0];
               A1553ContagemResultado_CntSrvCod = P009811_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009811_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P009811_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P009811_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P009811_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P009811_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P009811_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P009811_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P009811_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P009811_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P009811_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009811_n601ContagemResultado_Servico[0];
               A1237ContagemResultado_PrazoMaisDias = P009811_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P009811_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P009811_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P009811_n1227ContagemResultado_PrazoInicialDias[0];
               A2017ContagemResultado_DataEntregaReal = P009811_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P009811_n2017ContagemResultado_DataEntregaReal[0];
               A457ContagemResultado_Demanda = P009811_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P009811_n457ContagemResultado_Demanda[0];
               A509ContagemrResultado_SistemaSigla = P009811_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P009811_n509ContagemrResultado_SistemaSigla[0];
               A2008ContagemResultado_CntSrvAls = P009811_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P009811_n2008ContagemResultado_CntSrvAls[0];
               A471ContagemResultado_DataDmn = P009811_A471ContagemResultado_DataDmn[0];
               A1051ContagemResultado_GlsValor = P009811_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P009811_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P009811_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P009811_n1050ContagemResultado_GlsDescricao[0];
               A566ContagemResultado_DataUltCnt = P009811_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P009811_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P009811_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P009811_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P009811_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P009811_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P009811_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = P009811_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009811_n601ContagemResultado_Servico[0];
               A2008ContagemResultado_CntSrvAls = P009811_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P009811_n2008ContagemResultado_CntSrvAls[0];
               A566ContagemResultado_DataUltCnt = P009811_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P009811_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV18ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV18ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV31DataCnt = A566ContagemResultado_DataUltCnt;
               AV44i = 1;
               AV37DmnDescricao = A494ContagemResultado_Descricao;
               AV124Codigo = A456ContagemResultado_Codigo;
               AV121Contratada = A490ContagemResultado_ContratadaCod;
               AV111Servico = A601ContagemResultado_Servico;
               AV102Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV84GlsTxt = "";
               AV65TotalPontos = (decimal)(AV65TotalPontos+(A574ContagemResultado_PFFinal-AV40GlsPF));
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S169 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDPNT' */
               S179 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDFRQ' */
               S189 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV70strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV70strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               AV82Entregue = DateTimeUtil.ResetTime(A2017ContagemResultado_DataEntregaReal);
               H980( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 580, Gx_line+0, 910, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV60Sistema_Coordenacao, "@!")), 275, Gx_line+0, 397, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV82Entregue, "99/99/99"), 220, Gx_line+0, 269, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49Os, "")), 0, Gx_line+0, 53, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 58, Gx_line+0, 216, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV18ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S199 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               AV30d = (decimal)(1);
               AV39GlsDescricao = "Data: " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + " Servi�o: " + A2008ContagemResultado_CntSrvAls;
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S209 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               AV65TotalPontos = (decimal)(AV65TotalPontos+A574ContagemResultado_PFFinal);
               AV64TotalFinal = (decimal)(AV64TotalFinal+AV18ContagemResultado_PFFaturar);
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  AV85GlsValor = A1051ContagemResultado_GlsValor;
                  AV64TotalFinal = (decimal)(AV64TotalFinal-AV85GlsValor);
                  AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar-AV85GlsValor);
                  AV113StrGlsValor = "";
                  AV84GlsTxt = "";
                  AV85GlsValor = (decimal)(AV85GlsValor*-1);
                  AV113StrGlsValor = StringUtil.Str( AV85GlsValor, 14, 2);
                  AV84GlsTxt = "Glosa";
                  AV39GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  AV30d = (decimal)(1);
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S219 ();
                  if ( returnInSub )
                  {
                     pr_default.close(5);
                     returnInSub = true;
                     if (true) return;
                  }
                  H980( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84GlsTxt, "")), 83, Gx_line+0, 136, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S209 ();
                  if ( returnInSub )
                  {
                     pr_default.close(5);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV94Linhas = (short)(AV94Linhas+1);
               }
               if ( AV114TemIndicadorDePontoalidade )
               {
                  AV112Solicitada = A471ContagemResultado_DataDmn;
                  AV116ValorPF = A512ContagemResultado_ValorPF;
                  /* Execute user subroutine: 'INDICADORPONTUALIDADE' */
                  S229 ();
                  if ( returnInSub )
                  {
                     pr_default.close(5);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if ( AV115TemIndicadorPntLote )
               {
                  if ( ( AV106Previsto >= AV104PrdFtrIni ) && ( AV106Previsto <= AV103PrdFtrFim ) )
                  {
                     AV105Previstas = (short)(AV105Previstas+1);
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                     {
                        AV95PntLoteBaseCalculo = (decimal)(AV95PntLoteBaseCalculo+A1854ContagemResultado_VlrCnc);
                     }
                     else
                     {
                        AV95PntLoteBaseCalculo = (decimal)(AV95PntLoteBaseCalculo+A606ContagemResultado_ValorFinal);
                     }
                     AV101PntLoteUndBruto = (decimal)(AV101PntLoteUndBruto+A574ContagemResultado_PFFinal);
                     if ( AV82Entregue <= AV106Previsto )
                     {
                        AV88Cumpridas = (short)(AV88Cumpridas+1);
                     }
                     else
                     {
                        AV117Atrasadas = (short)(AV117Atrasadas+1);
                     }
                  }
               }
               if ( AV144TemIndicadorDeFrequencia )
               {
                  /* Execute user subroutine: 'INDICADORFREQUENCIA' */
                  S239 ();
                  if ( returnInSub )
                  {
                     pr_default.close(5);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV49Os = "";
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         else
         {
            /* Using cursor P009813 */
            pr_default.execute(6, new Object[] {AV14ContagemResultado_DemandaFM, AV15ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A489ContagemResultado_SistemaCod = P009813_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P009813_n489ContagemResultado_SistemaCod[0];
               A1553ContagemResultado_CntSrvCod = P009813_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009813_n1553ContagemResultado_CntSrvCod[0];
               A493ContagemResultado_DemandaFM = P009813_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P009813_n493ContagemResultado_DemandaFM[0];
               A597ContagemResultado_LoteAceiteCod = P009813_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P009813_n597ContagemResultado_LoteAceiteCod[0];
               A484ContagemResultado_StatusDmn = P009813_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P009813_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P009813_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P009813_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P009813_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P009813_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P009813_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P009813_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P009813_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009813_n601ContagemResultado_Servico[0];
               A1237ContagemResultado_PrazoMaisDias = P009813_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P009813_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P009813_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P009813_n1227ContagemResultado_PrazoInicialDias[0];
               A2017ContagemResultado_DataEntregaReal = P009813_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P009813_n2017ContagemResultado_DataEntregaReal[0];
               A509ContagemrResultado_SistemaSigla = P009813_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P009813_n509ContagemrResultado_SistemaSigla[0];
               A457ContagemResultado_Demanda = P009813_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P009813_n457ContagemResultado_Demanda[0];
               A2008ContagemResultado_CntSrvAls = P009813_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P009813_n2008ContagemResultado_CntSrvAls[0];
               A471ContagemResultado_DataDmn = P009813_A471ContagemResultado_DataDmn[0];
               A1051ContagemResultado_GlsValor = P009813_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P009813_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P009813_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P009813_n1050ContagemResultado_GlsDescricao[0];
               A566ContagemResultado_DataUltCnt = P009813_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P009813_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P009813_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P009813_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P009813_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P009813_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P009813_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = P009813_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009813_n601ContagemResultado_Servico[0];
               A2008ContagemResultado_CntSrvAls = P009813_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = P009813_n2008ContagemResultado_CntSrvAls[0];
               A566ContagemResultado_DataUltCnt = P009813_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P009813_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV18ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV18ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV31DataCnt = A566ContagemResultado_DataUltCnt;
               AV44i = 1;
               AV37DmnDescricao = A494ContagemResultado_Descricao;
               AV124Codigo = A456ContagemResultado_Codigo;
               AV121Contratada = A490ContagemResultado_ContratadaCod;
               AV111Servico = A601ContagemResultado_Servico;
               AV102Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV84GlsTxt = "";
               AV65TotalPontos = (decimal)(AV65TotalPontos+(A574ContagemResultado_PFFinal-AV40GlsPF));
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S169 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDPNT' */
               S179 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'TEMINDFRQ' */
               S189 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV70strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV70strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               AV82Entregue = DateTimeUtil.ResetTime(A2017ContagemResultado_DataEntregaReal);
               H980( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 567, Gx_line+0, 897, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV82Entregue, "99/99/99"), 345, Gx_line+0, 394, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 100, Gx_line+0, 350, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49Os, "")), 0, Gx_line+0, 93, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV18ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 970, Gx_line+0, 1005, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70strPFFinal, "")), 885, Gx_line+0, 959, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S199 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               AV30d = (decimal)(1);
               AV39GlsDescricao = "Data: " + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + " Servi�o: " + A2008ContagemResultado_CntSrvAls;
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S209 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               AV65TotalPontos = (decimal)(AV65TotalPontos+A574ContagemResultado_PFFinal);
               AV64TotalFinal = (decimal)(AV64TotalFinal+AV18ContagemResultado_PFFaturar);
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  AV85GlsValor = A1051ContagemResultado_GlsValor;
                  AV64TotalFinal = (decimal)(AV64TotalFinal-AV85GlsValor);
                  AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar-AV85GlsValor);
                  AV113StrGlsValor = "";
                  AV84GlsTxt = "";
                  AV85GlsValor = (decimal)(AV85GlsValor*-1);
                  AV113StrGlsValor = StringUtil.Str( AV85GlsValor, 14, 2);
                  AV84GlsTxt = "Glosa";
                  AV39GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  AV30d = (decimal)(1);
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S219 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     returnInSub = true;
                     if (true) return;
                  }
                  H980( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84GlsTxt, "")), 83, Gx_line+0, 136, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S209 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV94Linhas = (short)(AV94Linhas+1);
               }
               if ( AV114TemIndicadorDePontoalidade )
               {
                  AV112Solicitada = A471ContagemResultado_DataDmn;
                  AV116ValorPF = A512ContagemResultado_ValorPF;
                  /* Execute user subroutine: 'INDICADORPONTUALIDADE' */
                  S229 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if ( AV115TemIndicadorPntLote )
               {
                  if ( ( AV106Previsto >= AV104PrdFtrIni ) && ( AV106Previsto <= AV103PrdFtrFim ) )
                  {
                     AV105Previstas = (short)(AV105Previstas+1);
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                     {
                        AV95PntLoteBaseCalculo = (decimal)(AV95PntLoteBaseCalculo+A1854ContagemResultado_VlrCnc);
                     }
                     else
                     {
                        AV95PntLoteBaseCalculo = (decimal)(AV95PntLoteBaseCalculo+A606ContagemResultado_ValorFinal);
                     }
                     AV101PntLoteUndBruto = (decimal)(AV101PntLoteUndBruto+A574ContagemResultado_PFFinal);
                     if ( AV82Entregue <= AV106Previsto )
                     {
                        AV88Cumpridas = (short)(AV88Cumpridas+1);
                     }
                     else
                     {
                        AV117Atrasadas = (short)(AV117Atrasadas+1);
                     }
                  }
               }
               if ( AV114TemIndicadorDePontoalidade )
               {
                  /* Execute user subroutine: 'INDICADORFREQUENCIA' */
                  S239 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV49Os = "";
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         if ( AV65TotalPontos < 0.001m )
         {
            AV71strPFTotal = StringUtil.Str( AV65TotalPontos, 14, 4);
         }
         else
         {
            AV71strPFTotal = StringUtil.Str( AV65TotalPontos, 14, 3);
         }
         if ( AV64TotalFinal < 0.001m )
         {
            AV63TotalFaturar = NumberUtil.Round( AV64TotalFinal, 4);
            AV72strPFTotalFaturar = StringUtil.Str( AV63TotalFaturar, 14, 4);
         }
         else if ( AV64TotalFinal < 0.01m )
         {
            AV63TotalFaturar = NumberUtil.Round( AV64TotalFinal, 3);
            AV72strPFTotalFaturar = StringUtil.Str( AV63TotalFaturar, 14, 3);
         }
         else
         {
            AV72strPFTotalFaturar = StringUtil.Str( AV64TotalFinal, 14, 2);
         }
         AV63TotalFaturar = NumberUtil.Round( AV64TotalFinal, 2);
         if ( ( ! AV127SemOSFM && ( AV94Linhas > 1 ) ) || ( AV85GlsValor < Convert.ToDecimal( 0 )) )
         {
            H980( false, 15) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sub total:", 867, Gx_line+0, 915, Gx_line+14, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV72strPFTotalFaturar, "")), 959, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+15);
         }
      }

      protected void S199( )
      {
         /* 'PRINTDESCRICAO' Routine */
         while ( StringUtil.Len( AV37DmnDescricao) >= AV44i )
         {
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S169 ();
            if (returnInSub) return;
            H980( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 575, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
         AV44i = 0;
      }

      protected void S209( )
      {
         /* 'PRINTGLOSADESC' Routine */
         while ( (Convert.ToDecimal( StringUtil.Len( AV39GlsDescricao) ) >= AV30d ) )
         {
            /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
            S219 ();
            if (returnInSub) return;
            H980( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
      }

      protected void S169( )
      {
         /* 'LINHADADMNDESCRICAO' Routine */
         AV51p = AV36DescricaoLength;
         AV34Descricao = StringUtil.Substring( AV37DmnDescricao, AV44i, AV51p);
         if ( ( ( AV44i < StringUtil.Len( AV37DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV51p, 1), 1) == 0 ) ) || ( ( AV44i + AV51p <= StringUtil.Len( AV37DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV51p+1, 1), 1) == 0 ) ) )
         {
            AV52p2 = AV36DescricaoLength;
            while ( AV52p2 >= 1 )
            {
               AV51p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV52p2, 1), 1));
               if ( AV51p > 0 )
               {
                  if ( AV51p == 1 )
                  {
                     AV51p = (short)(AV52p2-1);
                     AV34Descricao = StringUtil.Substring( AV37DmnDescricao, AV44i, AV51p);
                  }
                  else
                  {
                     AV51p = AV52p2;
                     AV34Descricao = StringUtil.Substring( AV37DmnDescricao, AV44i, AV51p);
                  }
                  AV44i = (short)(AV44i+AV52p2);
                  if (true) break;
               }
               else
               {
                  AV51p = AV52p2;
               }
               AV52p2 = (short)(AV52p2+-1);
            }
            if ( AV51p < 2 )
            {
               AV34Descricao = StringUtil.Substring( AV37DmnDescricao, AV44i, AV36DescricaoLength);
               AV44i = (short)(AV44i+AV36DescricaoLength);
            }
         }
         else
         {
            AV44i = (short)(AV44i+AV51p);
         }
      }

      protected void S219( )
      {
         /* 'LINHADAGLSDESCRICAO' Routine */
         AV51p = AV89DescricaoGlsLength;
         AV34Descricao = StringUtil.Substring( AV39GlsDescricao, (int)(AV30d), AV51p);
         if ( ( ( AV30d < Convert.ToDecimal( StringUtil.Len( AV39GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV51p, 1), 1) == 0 ) ) || ( ( AV30d + AV51p <= Convert.ToDecimal( StringUtil.Len( AV39GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV51p+1, 1), 1) == 0 ) ) )
         {
            AV52p2 = AV89DescricaoGlsLength;
            while ( AV52p2 >= 1 )
            {
               AV51p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV34Descricao, AV52p2, 1), 1));
               if ( AV51p > 0 )
               {
                  if ( AV51p == 1 )
                  {
                     AV51p = (short)(AV52p2-1);
                     AV34Descricao = StringUtil.Substring( AV39GlsDescricao, (int)(AV30d), AV51p);
                  }
                  else
                  {
                     AV51p = AV52p2;
                     AV34Descricao = StringUtil.Substring( AV39GlsDescricao, (int)(AV30d), AV51p);
                  }
                  AV30d = (decimal)(AV30d+AV52p2);
                  if (true) break;
               }
               else
               {
                  AV51p = AV52p2;
               }
               AV52p2 = (short)(AV52p2+-1);
            }
            if ( AV51p < 2 )
            {
               AV34Descricao = StringUtil.Substring( AV39GlsDescricao, (int)(AV30d), AV89DescricaoGlsLength);
               AV30d = (decimal)(AV30d+AV89DescricaoGlsLength);
            }
         }
         else
         {
            AV30d = (decimal)(AV30d+AV51p);
         }
      }

      protected void S179( )
      {
         /* 'TEMINDPNT' Routine */
         new prc_temindicadorpnt(context ).execute( ref  AV128ContratoServicos_Codigo, out  AV114TemIndicadorDePontoalidade, out  AV115TemIndicadorPntLote) ;
      }

      protected void S189( )
      {
         /* 'TEMINDFRQ' Routine */
         new prc_temindicadorfrq(context ).execute( ref  AV128ContratoServicos_Codigo, out  AV144TemIndicadorDeFrequencia) ;
      }

      protected void S229( )
      {
         /* 'INDICADORPONTUALIDADE' Routine */
         /* Using cursor P009814 */
         pr_default.execute(7, new Object[] {AV124Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1404ContagemResultadoExecucao_OSCod = P009814_A1404ContagemResultadoExecucao_OSCod[0];
            A1405ContagemResultadoExecucao_Codigo = P009814_A1405ContagemResultadoExecucao_Codigo[0];
            A1406ContagemResultadoExecucao_Inicio = P009814_A1406ContagemResultadoExecucao_Inicio[0];
            new prc_pontualidadedaos(context ).execute(  A1405ContagemResultadoExecucao_Codigo,  AV18ContagemResultado_PFFaturar, out  AV109Reduz, out  AV85GlsValor, out  AV40GlsPF, out  AV39GlsDescricao) ;
            AV64TotalFinal = (decimal)(AV64TotalFinal-(NumberUtil.Round( AV85GlsValor, 3)));
            AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar-(NumberUtil.Round( AV85GlsValor, 3)));
            AV113StrGlsValor = "";
            AV84GlsTxt = "";
            if ( ( AV85GlsValor > Convert.ToDecimal( 0 )) )
            {
               AV85GlsValor = (decimal)(AV85GlsValor*-1);
               AV113StrGlsValor = StringUtil.Str( AV85GlsValor, 14, 2);
               AV84GlsTxt = "Glosa";
               AV94Linhas = (short)(AV94Linhas+1);
            }
            AV30d = (decimal)(1);
            /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
            S219 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            H980( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84GlsTxt, "")), 83, Gx_line+0, 136, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
            /* Execute user subroutine: 'PRINTGLOSADESC' */
            S209 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void S239( )
      {
         /* 'INDICADORFREQUENCIA' Routine */
         AV113StrGlsValor = "";
         AV84GlsTxt = "";
         GXt_int4 = AV145Count;
         new prc_countnaocnf(context ).execute(  AV91Indicador,  AV124Codigo,  AV148SemCodigos,  context.localUtil.CToD( "01/11/2018", 2), out  GXt_int4) ;
         AV145Count = GXt_int4;
         GXt_decimal3 = AV109Reduz;
         new prc_frequenciademandareduz(context ).execute( ref  AV128ContratoServicos_Codigo,  AV145Count,  AV92IndP, out  AV91Indicador, out  AV143Indicador_Sigla, out  AV142Faixa, out  GXt_decimal3) ;
         AV109Reduz = GXt_decimal3;
         AV39GlsDescricao = AV143Indicador_Sigla + ": " + StringUtil.Trim( StringUtil.Str( (decimal)(AV145Count), 4, 0)) + " recusa(s).";
         AV94Linhas = (short)(AV94Linhas+1);
         AV30d = (decimal)(1);
         /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
         S219 ();
         if (returnInSub) return;
         H980( false, 16) ;
         getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34Descricao, "")), 133, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113StrGlsValor, "")), 994, Gx_line+0, 1068, Gx_line+15, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84GlsTxt, "")), 83, Gx_line+0, 136, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+16);
         /* Execute user subroutine: 'PRINTGLOSADESC' */
         S209 ();
         if (returnInSub) return;
      }

      protected void S141( )
      {
         /* 'GETSANCAOINDPNTLOTE' Routine */
         AV109Reduz = 0;
         GXt_decimal3 = AV109Reduz;
         new prc_pontualidadelotereduz(context ).execute( ref  AV128ContratoServicos_Codigo,  0,  (short)(AV97PntLoteIndice), out  GXt_decimal3) ;
         AV109Reduz = GXt_decimal3;
         AV98PntLoteLiquido = AV20ContagemResultado_PFTotalFaturar;
         AV95PntLoteBaseCalculo = AV20ContagemResultado_PFTotalFaturar;
         if ( ( AV109Reduz > Convert.ToDecimal( 0 )) )
         {
            AV100PntLoteSancao = (decimal)(AV95PntLoteBaseCalculo*(AV109Reduz/ (decimal)(100)));
            AV98PntLoteLiquido = (decimal)(AV98PntLoteLiquido-AV100PntLoteSancao);
         }
      }

      protected void S241( )
      {
         /* 'DADOSDOCONTRATO' Routine */
         /* Using cursor P009815 */
         pr_default.execute(8, new Object[] {AV87Contrato_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A1013Contrato_PrepostoCod = P009815_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P009815_n1013Contrato_PrepostoCod[0];
            A1016Contrato_PrepostoPesCod = P009815_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P009815_n1016Contrato_PrepostoPesCod[0];
            A74Contrato_Codigo = P009815_A74Contrato_Codigo[0];
            A77Contrato_Numero = P009815_A77Contrato_Numero[0];
            A1015Contrato_PrepostoNom = P009815_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P009815_n1015Contrato_PrepostoNom[0];
            A81Contrato_Quantidade = P009815_A81Contrato_Quantidade[0];
            A1354Contrato_PrdFtrCada = P009815_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = P009815_n1354Contrato_PrdFtrCada[0];
            A1357Contrato_PrdFtrIni = P009815_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = P009815_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = P009815_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = P009815_n1358Contrato_PrdFtrFim[0];
            A1016Contrato_PrepostoPesCod = P009815_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P009815_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P009815_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P009815_n1015Contrato_PrepostoNom[0];
            AV9Contrato_Numero = A77Contrato_Numero;
            AV53Preposto = StringUtil.Upper( A1015Contrato_PrepostoNom);
            AV140Contratado = (decimal)(A81Contrato_Quantidade);
            if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "S") == 0 )
            {
               if ( DateTimeUtil.Dow( AV104PrdFtrIni) != 2 )
               {
                  while ( DateTimeUtil.Dow( AV104PrdFtrIni) != 2 )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV104PrdFtrIni ) ;
                     AV104PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(-1)));
                  }
               }
               if ( DateTimeUtil.Dow( AV103PrdFtrFim) != 1 )
               {
                  while ( DateTimeUtil.Dow( AV103PrdFtrFim) != 1 )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV103PrdFtrFim ) ;
                     AV103PrdFtrFim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(1)));
                  }
               }
            }
            else if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "M") == 0 )
            {
               AV44i = (short)(1-DateTimeUtil.Day( AV104PrdFtrIni));
               GXt_dtime5 = DateTimeUtil.ResetTime( AV104PrdFtrIni ) ;
               AV104PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(AV44i)));
               AV103PrdFtrFim = DateTimeUtil.DateEndOfMonth( AV103PrdFtrFim);
            }
            else if ( StringUtil.StrCmp(A1354Contrato_PrdFtrCada, "P") == 0 )
            {
               if ( DateTimeUtil.Day( AV104PrdFtrIni) != A1357Contrato_PrdFtrIni )
               {
                  while ( DateTimeUtil.Day( AV104PrdFtrIni) != A1357Contrato_PrdFtrIni )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV104PrdFtrIni ) ;
                     AV104PrdFtrIni = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(-1)));
                  }
               }
               if ( DateTimeUtil.Day( AV103PrdFtrFim) != A1358Contrato_PrdFtrFim )
               {
                  while ( DateTimeUtil.Day( AV103PrdFtrFim) != A1358Contrato_PrdFtrFim )
                  {
                     GXt_dtime5 = DateTimeUtil.ResetTime( AV103PrdFtrFim ) ;
                     AV103PrdFtrFim = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime5, 86400*(1)));
                  }
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
         /* Using cursor P009817 */
         pr_default.execute(9, new Object[] {AV121Contratada});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A40Contratada_PessoaCod = P009817_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P009817_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = P009817_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = P009817_n349Contratada_MunicipioCod[0];
            A29Contratante_Codigo = P009817_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P009817_n29Contratante_Codigo[0];
            A39Contratada_Codigo = P009817_A39Contratada_Codigo[0];
            A42Contratada_PessoaCNPJ = P009817_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P009817_n42Contratada_PessoaCNPJ[0];
            A519Pessoa_Endereco = P009817_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P009817_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P009817_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P009817_n521Pessoa_CEP[0];
            A350Contratada_UF = P009817_A350Contratada_UF[0];
            n350Contratada_UF = P009817_n350Contratada_UF[0];
            A2034Contratante_TtlRltGerencial = P009817_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P009817_n2034Contratante_TtlRltGerencial[0];
            A40001GXC2 = P009817_A40001GXC2[0];
            n40001GXC2 = P009817_n40001GXC2[0];
            A42Contratada_PessoaCNPJ = P009817_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P009817_n42Contratada_PessoaCNPJ[0];
            A519Pessoa_Endereco = P009817_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P009817_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P009817_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P009817_n521Pessoa_CEP[0];
            A29Contratante_Codigo = P009817_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P009817_n29Contratante_Codigo[0];
            A2034Contratante_TtlRltGerencial = P009817_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = P009817_n2034Contratante_TtlRltGerencial[0];
            A350Contratada_UF = P009817_A350Contratada_UF[0];
            n350Contratada_UF = P009817_n350Contratada_UF[0];
            A40001GXC2 = P009817_A40001GXC2[0];
            n40001GXC2 = P009817_n40001GXC2[0];
            OV137contratante_TtlRltGerencial = AV137contratante_TtlRltGerencial;
            AV125CNPJ = A42Contratada_PessoaCNPJ;
            AV134Endereco = StringUtil.Trim( A519Pessoa_Endereco) + ", ";
            AV134Endereco = AV134Endereco + A40001GXC2;
            AV134Endereco = AV134Endereco + " - " + A350Contratada_UF + " CEP " + A521Pessoa_CEP;
            AV137contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(9);
      }

      protected void S131( )
      {
         /* 'PRINTFREQUENCIA' Routine */
         if ( AV144TemIndicadorDeFrequencia )
         {
            AV145Count = 0;
            AV85GlsValor = 0;
            GXt_int2 = AV91Indicador;
            GXt_char6 = "FP";
            new prc_getindicador(context ).execute( ref  AV128ContratoServicos_Codigo, ref  GXt_char6, out  AV143Indicador_Sigla, out  AV147CalculoSob, out  GXt_int2) ;
            AV91Indicador = GXt_int2;
            GXt_int4 = AV145Count;
            new prc_countnaocnf(context ).execute(  AV91Indicador,  0,  AV119Codigos,  context.localUtil.CToD( "01/11/2018", 2), out  GXt_int4) ;
            AV145Count = GXt_int4;
            AV58Quantidade = (short)(AV119Codigos.Count);
            if ( StringUtil.StrCmp(AV147CalculoSob, "D") == 0 )
            {
               GXt_decimal3 = AV109Reduz;
               new prc_frequenciademandareduz(context ).execute( ref  AV128ContratoServicos_Codigo,  AV145Count,  (decimal)(0), out  AV91Indicador, out  AV143Indicador_Sigla, out  AV142Faixa, out  GXt_decimal3) ;
               AV109Reduz = GXt_decimal3;
            }
            else
            {
               if ( AV58Quantidade > 0 )
               {
                  AV92IndP = (decimal)(AV145Count/ (decimal)(AV58Quantidade));
                  GXt_decimal3 = AV109Reduz;
                  new prc_frequenciademandareduz(context ).execute( ref  AV128ContratoServicos_Codigo,  0,  AV92IndP, out  AV91Indicador, out  AV143Indicador_Sigla, out  AV142Faixa, out  GXt_decimal3) ;
                  AV109Reduz = GXt_decimal3;
               }
            }
            if ( ( AV109Reduz > Convert.ToDecimal( 0 )) )
            {
               AV85GlsValor = (decimal)(AV18ContagemResultado_PFFaturar*(AV109Reduz/ (decimal)(100)));
               AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar-AV85GlsValor);
               AV64TotalFinal = (decimal)(AV64TotalFinal-(NumberUtil.Round( AV85GlsValor, 3)));
               AV20ContagemResultado_PFTotalFaturar = (decimal)(AV20ContagemResultado_PFTotalFaturar-(NumberUtil.Round( AV85GlsValor, 3)));
               AV85GlsValor = (decimal)(AV85GlsValor*-1);
            }
            AV113StrGlsValor = StringUtil.Str( AV85GlsValor, 14, 2);
            H980( false, 65) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV92IndP, "ZZ9.99")), 175, Gx_line+17, 226, Gx_line+34, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58Quantidade), "ZZZ9")), 442, Gx_line+17, 475, Gx_line+34, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV109Reduz, "ZZ9.99 %")), 708, Gx_line+33, 776, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113StrGlsValor, "")), 978, Gx_line+33, 1067, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV142Faixa), "ZZZ9")), 667, Gx_line+33, 701, Gx_line+50, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV145Count), "ZZZ9")), 692, Gx_line+17, 726, Gx_line+34, 2+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("%", 233, Gx_line+17, 247, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Glosa R$:", 850, Gx_line+33, 912, Gx_line+51, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Faixa de apura��o:", 542, Gx_line+33, 664, Gx_line+51, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("IARC:", 142, Gx_line+17, 178, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Somat�ria de recusas:", 542, Gx_line+17, 684, Gx_line+35, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Quantidade de OS:", 308, Gx_line+17, 428, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+65);
         }
      }

      protected void S151( )
      {
         /* 'PRINTFAIXAS' Routine */
         /* Eject command */
         Gx_OldLine = Gx_line;
         Gx_line = (int)(P_lines+1);
         if ( AV114TemIndicadorDePontoalidade )
         {
            AV146ContratoServicosIndicador_Tipo = "P";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S251 ();
            if (returnInSub) return;
         }
         if ( AV115TemIndicadorPntLote )
         {
            AV146ContratoServicosIndicador_Tipo = "PL";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S251 ();
            if (returnInSub) return;
         }
         if ( AV144TemIndicadorDeFrequencia )
         {
            AV146ContratoServicosIndicador_Tipo = "FP";
            /* Execute user subroutine: 'PRINTFAIXA' */
            S251 ();
            if (returnInSub) return;
         }
      }

      protected void S251( )
      {
         /* 'PRINTFAIXA' Routine */
         /* Using cursor P009818 */
         pr_default.execute(10, new Object[] {AV128ContratoServicos_Codigo, AV146ContratoServicosIndicador_Tipo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1270ContratoServicosIndicador_CntSrvCod = P009818_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1308ContratoServicosIndicador_Tipo = P009818_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P009818_n1308ContratoServicosIndicador_Tipo[0];
            A1269ContratoServicosIndicador_Codigo = P009818_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P009818_A1274ContratoServicosIndicador_Indicador[0];
            A1271ContratoServicosIndicador_Numero = P009818_A1271ContratoServicosIndicador_Numero[0];
            AV96PntLoteIndicador = "N� " + StringUtil.Trim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)) + " / " + A1274ContratoServicosIndicador_Indicador;
            H980( false, 84) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Regras de Apura��o", 150, Gx_line+67, 298, Gx_line+84, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96PntLoteIndicador, "")), 150, Gx_line+33, 880, Gx_line+51, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, true, true, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Indicador", 75, Gx_line+33, 134, Gx_line+50, 0+256, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+84);
            AV44i = 1;
            /* Using cursor P009819 */
            pr_default.execute(11, new Object[] {A1269ContratoServicosIndicador_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1300ContratoServicosIndicadorFaixa_Numero = P009819_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1301ContratoServicosIndicadorFaixa_Desde = P009819_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1302ContratoServicosIndicadorFaixa_Ate = P009819_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1303ContratoServicosIndicadorFaixa_Reduz = P009819_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = P009819_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               if ( AV44i == 1 )
               {
                  H980( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("at�", 200, Gx_line+0, 221, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 267, Gx_line+0, 326, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 217, Gx_line+0, 262, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 158, Gx_line+0, 203, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 100, Gx_line+0, 130, Gx_line+18, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else if ( AV44i == 2 )
               {
                  /* Noskip command */
                  Gx_line = Gx_OldLine;
                  H980( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 375, Gx_line+0, 405, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 425, Gx_line+0, 470, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 492, Gx_line+0, 537, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 542, Gx_line+0, 601, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("at�", 475, Gx_line+0, 496, Gx_line+18, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else if ( AV44i == 3 )
               {
                  /* Noskip command */
                  Gx_line = Gx_OldLine;
                  H980( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("at�", 758, Gx_line+0, 779, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 825, Gx_line+0, 884, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 775, Gx_line+0, 820, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 708, Gx_line+0, 753, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 658, Gx_line+0, 688, Gx_line+18, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               else
               {
                  H980( false, 18) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("at�", 200, Gx_line+0, 221, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), 267, Gx_line+0, 326, Gx_line+18, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), 217, Gx_line+0, 262, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), 158, Gx_line+0, 203, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")), 100, Gx_line+0, 130, Gx_line+18, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
                  AV44i = 1;
               }
               AV44i = (short)(AV44i+1);
               pr_default.readNext(11);
            }
            pr_default.close(11);
            pr_default.readNext(10);
         }
         pr_default.close(10);
      }

      protected void H980( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(8, Gx_line+5, 1084, Gx_line+5, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV86Hoje, "99/99/99 99:99"), 508, Gx_line+9, 588, Gx_line+24, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(AV68WWPContext.gxTpr_Username, 8, Gx_line+9, 348, Gx_line+24, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 975, Gx_line+9, 1014, Gx_line+24, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 1025, Gx_line+9, 1074, Gx_line+23, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 1008, Gx_line+9, 1022, Gx_line+23, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 942, Gx_line+9, 977, Gx_line+23, 0+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46NumeroRelatorio, "")), 808, Gx_line+7, 934, Gx_line+25, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+34);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if ( AV50OSAutomatica )
               {
                  if ( Gx_page == 1 )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+183, 1073, Gx_line+204, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(0, Gx_line+133, 1073, Gx_line+154, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58Quantidade), "ZZZ9")), 667, Gx_line+162, 717, Gx_line+178, 2, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("SUM�RIO A FATURAR", 483, Gx_line+133, 622, Gx_line+152, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV137contratante_TtlRltGerencial, "")), 167, Gx_line+5, 942, Gx_line+30, 1, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Qtd de Demandas Executadas para o Faturamento:", 392, Gx_line+162, 649, Gx_line+176, 0, 0, 0, 0) ;
                     getPrinter().GxDrawBitMap(AV129Contratada_Logo, 0, Gx_line+17, 120, Gx_line+117) ;
                     getPrinter().GxDrawText("CNPJ:", 133, Gx_line+50, 165, Gx_line+64, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV134Endereco, "")), 133, Gx_line+67, 408, Gx_line+117, 0+16, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV125CNPJ, "")), 167, Gx_line+50, 241, Gx_line+65, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV61SubTitulo, "")), 500, Gx_line+33, 595, Gx_line+51, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV131Periodo, "")), 425, Gx_line+50, 676, Gx_line+68, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV130Contratada_Sigla, "@!")), 133, Gx_line+33, 228, Gx_line+51, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46NumeroRelatorio, "")), 483, Gx_line+67, 609, Gx_line+85, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73ContratanteArea, "")), 683, Gx_line+33, 950, Gx_line+51, 2, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+210);
                  }
                  else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV146ContratoServicosIndicador_Tipo)) )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+35, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N� OS", 2, Gx_line+17, 44, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Total", 1033, Gx_line+17, 1059, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Unit�rio", 967, Gx_line+17, 1006, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Exec.", 925, Gx_line+17, 954, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Descri��o da OS", 575, Gx_line+17, 667, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 1033, Gx_line+4, 1058, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 975, Gx_line+4, 1005, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Sistema", 400, Gx_line+17, 461, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("UO", 275, Gx_line+17, 360, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("OS Refer�ncia", 58, Gx_line+17, 139, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("entrega", 225, Gx_line+17, 264, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Data de", 225, Gx_line+4, 270, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 908, Gx_line+4, 966, Gx_line+19, 1, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+43);
                     if ( AV44i > AV36DescricaoLength * 1.1m )
                     {
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV82Entregue, "99/99/99"), 217, Gx_line+0, 266, Gx_line+15, 0+256, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV60Sistema_Coordenacao, "@!")), 267, Gx_line+0, 389, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 400, Gx_line+0, 574, Gx_line+15, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+15);
                        /* Noskip command */
                        Gx_line = Gx_OldLine;
                     }
                  }
               }
               else
               {
                  if ( Gx_page == 1 )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+183, 1073, Gx_line+204, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxDrawRect(0, Gx_line+133, 1073, Gx_line+154, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58Quantidade), "ZZZ9")), 667, Gx_line+162, 717, Gx_line+178, 2, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("SUM�RIO A FATURAR", 483, Gx_line+133, 622, Gx_line+152, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV137contratante_TtlRltGerencial, "")), 158, Gx_line+5, 933, Gx_line+30, 1, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Qtd de Demandas Executadas para o Faturamento:", 392, Gx_line+162, 649, Gx_line+176, 0, 0, 0, 0) ;
                     getPrinter().GxDrawBitMap(AV129Contratada_Logo, 0, Gx_line+17, 120, Gx_line+117) ;
                     getPrinter().GxDrawText("CNPJ:", 133, Gx_line+50, 165, Gx_line+64, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV134Endereco, "")), 133, Gx_line+67, 408, Gx_line+117, 0+16, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV125CNPJ, "")), 167, Gx_line+50, 241, Gx_line+65, 0+256, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV61SubTitulo, "")), 500, Gx_line+33, 595, Gx_line+51, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV131Periodo, "")), 425, Gx_line+50, 676, Gx_line+68, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV130Contratada_Sigla, "@!")), 133, Gx_line+33, 228, Gx_line+51, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46NumeroRelatorio, "")), 483, Gx_line+67, 609, Gx_line+85, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73ContratanteArea, "")), 683, Gx_line+33, 950, Gx_line+51, 2, 0, 0, 1) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+210);
                  }
                  else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV146ContratoServicosIndicador_Tipo)) )
                  {
                     getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+35, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N� OS", 2, Gx_line+17, 44, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Total", 1033, Gx_line+17, 1059, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Unit�rio", 967, Gx_line+17, 1006, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Descri��o da OS", 575, Gx_line+17, 667, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 1033, Gx_line+4, 1058, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Valor", 975, Gx_line+4, 1005, Gx_line+26, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Sistema", 400, Gx_line+17, 461, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("OS Refer�ncia", 100, Gx_line+17, 183, Gx_line+31, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("entrega", 350, Gx_line+17, 389, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText("Data de", 350, Gx_line+4, 411, Gx_line+18, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText("Exec.", 925, Gx_line+17, 954, Gx_line+31, 0+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV75UnidadeMedicao_Sigla, "@!")), 908, Gx_line+4, 966, Gx_line+19, 1, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+43);
                  }
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
         add_metrics5( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Courier New", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics5( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV68WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV73ContratanteArea = "";
         AV62TextoLink = "";
         scmdbuf = "";
         P00983_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         P00983_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         P00983_A456ContagemResultado_Codigo = new int[1] ;
         P00983_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00983_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00983_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00983_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00983_A601ContagemResultado_Servico = new int[1] ;
         P00983_n601ContagemResultado_Servico = new bool[] {false} ;
         P00983_A1603ContagemResultado_CntCod = new int[1] ;
         P00983_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00983_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00983_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00983_A1621ContagemResultado_CntSrvUndCntSgl = new String[] {""} ;
         P00983_n1621ContagemResultado_CntSrvUndCntSgl = new bool[] {false} ;
         P00983_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00983_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00983_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00983_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         A1621ContagemResultado_CntSrvUndCntSgl = "";
         A803ContagemResultado_ContratadaSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV104PrdFtrIni = DateTime.MinValue;
         AV103PrdFtrFim = DateTime.MinValue;
         AV75UnidadeMedicao_Sigla = "";
         AV130Contratada_Sigla = "";
         AV61SubTitulo = "";
         AV11AssinadoDtHr = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         AV54qrCode_Image = "";
         AV155Qrcode_image_GXI = "";
         P00986_A563Lote_Nome = new String[] {""} ;
         P00986_A562Lote_Numero = new String[] {""} ;
         P00986_A2055Lote_CntUsado = new decimal[1] ;
         P00986_n2055Lote_CntUsado = new bool[] {false} ;
         P00986_A2056Lote_CntSaldo = new decimal[1] ;
         P00986_n2056Lote_CntSaldo = new bool[] {false} ;
         P00986_A596Lote_Codigo = new int[1] ;
         P00986_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00986_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         A563Lote_Nome = "";
         A562Lote_Numero = "";
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         AV46NumeroRelatorio = "";
         AV33DataInicio = DateTime.MinValue;
         AV32DataFim = DateTime.MinValue;
         AV131Periodo = "";
         P00987_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00987_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00987_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00987_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00987_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00987_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00987_A456ContagemResultado_Codigo = new int[1] ;
         P00987_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00987_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         AV119Codigos = new GxSimpleCollection();
         AV9Contrato_Numero = "";
         P00988_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00988_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00988_A456ContagemResultado_Codigo = new int[1] ;
         P00988_A553ContagemResultado_DemandaFM_ORDER = new long[1] ;
         P00988_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00988_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         A493ContagemResultado_DemandaFM = "";
         AV14ContagemResultado_DemandaFM = "";
         AV49Os = "";
         P00989_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00989_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00989_A456ContagemResultado_Codigo = new int[1] ;
         P00989_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00989_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         AV53Preposto = "";
         AV54qrCode_Image = "";
         AV96PntLoteIndicador = "";
         P009811_A489ContagemResultado_SistemaCod = new int[1] ;
         P009811_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P009811_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009811_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009811_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P009811_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P009811_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P009811_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P009811_A494ContagemResultado_Descricao = new String[] {""} ;
         P009811_n494ContagemResultado_Descricao = new bool[] {false} ;
         P009811_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009811_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009811_A601ContagemResultado_Servico = new int[1] ;
         P009811_n601ContagemResultado_Servico = new bool[] {false} ;
         P009811_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P009811_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P009811_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P009811_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P009811_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P009811_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P009811_A457ContagemResultado_Demanda = new String[] {""} ;
         P009811_n457ContagemResultado_Demanda = new bool[] {false} ;
         P009811_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P009811_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P009811_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P009811_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P009811_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P009811_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P009811_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P009811_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P009811_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P009811_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P009811_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P009811_A456ContagemResultado_Codigo = new int[1] ;
         P009811_A512ContagemResultado_ValorPF = new decimal[1] ;
         P009811_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A494ContagemResultado_Descricao = "";
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A457ContagemResultado_Demanda = "";
         A509ContagemrResultado_SistemaSigla = "";
         A2008ContagemResultado_CntSrvAls = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1050ContagemResultado_GlsDescricao = "";
         AV31DataCnt = DateTime.MinValue;
         AV37DmnDescricao = "";
         AV84GlsTxt = "";
         AV70strPFFinal = "";
         AV82Entregue = DateTime.MinValue;
         AV34Descricao = "";
         AV60Sistema_Coordenacao = "";
         AV39GlsDescricao = "";
         AV113StrGlsValor = "";
         AV112Solicitada = DateTime.MinValue;
         AV106Previsto = DateTime.MinValue;
         P009813_A489ContagemResultado_SistemaCod = new int[1] ;
         P009813_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P009813_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009813_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009813_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P009813_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P009813_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P009813_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P009813_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P009813_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P009813_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P009813_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P009813_A494ContagemResultado_Descricao = new String[] {""} ;
         P009813_n494ContagemResultado_Descricao = new bool[] {false} ;
         P009813_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009813_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009813_A601ContagemResultado_Servico = new int[1] ;
         P009813_n601ContagemResultado_Servico = new bool[] {false} ;
         P009813_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P009813_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P009813_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P009813_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P009813_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P009813_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P009813_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P009813_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P009813_A457ContagemResultado_Demanda = new String[] {""} ;
         P009813_n457ContagemResultado_Demanda = new bool[] {false} ;
         P009813_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P009813_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P009813_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P009813_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P009813_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P009813_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P009813_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P009813_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P009813_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P009813_A456ContagemResultado_Codigo = new int[1] ;
         P009813_A512ContagemResultado_ValorPF = new decimal[1] ;
         P009813_n512ContagemResultado_ValorPF = new bool[] {false} ;
         AV71strPFTotal = "";
         AV72strPFTotalFaturar = "";
         P009814_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P009814_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P009814_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         AV148SemCodigos = new GxSimpleCollection();
         AV143Indicador_Sigla = "";
         P009815_A1013Contrato_PrepostoCod = new int[1] ;
         P009815_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P009815_A1016Contrato_PrepostoPesCod = new int[1] ;
         P009815_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         P009815_A74Contrato_Codigo = new int[1] ;
         P009815_A77Contrato_Numero = new String[] {""} ;
         P009815_A1015Contrato_PrepostoNom = new String[] {""} ;
         P009815_n1015Contrato_PrepostoNom = new bool[] {false} ;
         P009815_A81Contrato_Quantidade = new int[1] ;
         P009815_A1354Contrato_PrdFtrCada = new String[] {""} ;
         P009815_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         P009815_A1357Contrato_PrdFtrIni = new short[1] ;
         P009815_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         P009815_A1358Contrato_PrdFtrFim = new short[1] ;
         P009815_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         A77Contrato_Numero = "";
         A1015Contrato_PrepostoNom = "";
         A1354Contrato_PrdFtrCada = "";
         GXt_dtime5 = (DateTime)(DateTime.MinValue);
         P009817_A40Contratada_PessoaCod = new int[1] ;
         P009817_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P009817_A349Contratada_MunicipioCod = new int[1] ;
         P009817_n349Contratada_MunicipioCod = new bool[] {false} ;
         P009817_A29Contratante_Codigo = new int[1] ;
         P009817_n29Contratante_Codigo = new bool[] {false} ;
         P009817_A39Contratada_Codigo = new int[1] ;
         P009817_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P009817_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P009817_A519Pessoa_Endereco = new String[] {""} ;
         P009817_n519Pessoa_Endereco = new bool[] {false} ;
         P009817_A521Pessoa_CEP = new String[] {""} ;
         P009817_n521Pessoa_CEP = new bool[] {false} ;
         P009817_A350Contratada_UF = new String[] {""} ;
         P009817_n350Contratada_UF = new bool[] {false} ;
         P009817_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         P009817_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         P009817_A40001GXC2 = new String[] {""} ;
         P009817_n40001GXC2 = new bool[] {false} ;
         A42Contratada_PessoaCNPJ = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A350Contratada_UF = "";
         A2034Contratante_TtlRltGerencial = "";
         A40001GXC2 = "";
         OV137contratante_TtlRltGerencial = "";
         AV137contratante_TtlRltGerencial = "Relat�rio Gerencial";
         AV125CNPJ = "";
         AV134Endereco = "";
         GXt_char6 = "";
         AV147CalculoSob = "";
         AV146ContratoServicosIndicador_Tipo = "";
         P009818_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P009818_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P009818_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P009818_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P009818_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P009818_A1271ContratoServicosIndicador_Numero = new short[1] ;
         A1308ContratoServicosIndicador_Tipo = "";
         A1274ContratoServicosIndicador_Indicador = "";
         P009819_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P009819_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P009819_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P009819_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P009819_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P009819_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         AV86Hoje = (DateTime)(DateTime.MinValue);
         AV129Contratada_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_contagemgerencialarqpdf__default(),
            new Object[][] {
                new Object[] {
               P00983_A1620ContagemResultado_CntSrvUndCnt, P00983_n1620ContagemResultado_CntSrvUndCnt, P00983_A456ContagemResultado_Codigo, P00983_A597ContagemResultado_LoteAceiteCod, P00983_n597ContagemResultado_LoteAceiteCod, P00983_A490ContagemResultado_ContratadaCod, P00983_n490ContagemResultado_ContratadaCod, P00983_A601ContagemResultado_Servico, P00983_n601ContagemResultado_Servico, P00983_A1603ContagemResultado_CntCod,
               P00983_n1603ContagemResultado_CntCod, P00983_A1553ContagemResultado_CntSrvCod, P00983_n1553ContagemResultado_CntSrvCod, P00983_A1621ContagemResultado_CntSrvUndCntSgl, P00983_n1621ContagemResultado_CntSrvUndCntSgl, P00983_A803ContagemResultado_ContratadaSigla, P00983_n803ContagemResultado_ContratadaSigla, P00983_A566ContagemResultado_DataUltCnt, P00983_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00986_A563Lote_Nome, P00986_A562Lote_Numero, P00986_A2055Lote_CntUsado, P00986_n2055Lote_CntUsado, P00986_A2056Lote_CntSaldo, P00986_n2056Lote_CntSaldo, P00986_A596Lote_Codigo, P00986_A567Lote_DataIni, P00986_A568Lote_DataFim
               }
               , new Object[] {
               P00987_A597ContagemResultado_LoteAceiteCod, P00987_n597ContagemResultado_LoteAceiteCod, P00987_A484ContagemResultado_StatusDmn, P00987_n484ContagemResultado_StatusDmn, P00987_A1854ContagemResultado_VlrCnc, P00987_n1854ContagemResultado_VlrCnc, P00987_A456ContagemResultado_Codigo, P00987_A512ContagemResultado_ValorPF, P00987_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P00988_A597ContagemResultado_LoteAceiteCod, P00988_n597ContagemResultado_LoteAceiteCod, P00988_A456ContagemResultado_Codigo, P00988_A553ContagemResultado_DemandaFM_ORDER, P00988_A493ContagemResultado_DemandaFM, P00988_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P00989_A597ContagemResultado_LoteAceiteCod, P00989_n597ContagemResultado_LoteAceiteCod, P00989_A456ContagemResultado_Codigo, P00989_A493ContagemResultado_DemandaFM, P00989_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P009811_A489ContagemResultado_SistemaCod, P009811_n489ContagemResultado_SistemaCod, P009811_A1553ContagemResultado_CntSrvCod, P009811_n1553ContagemResultado_CntSrvCod, P009811_A484ContagemResultado_StatusDmn, P009811_n484ContagemResultado_StatusDmn, P009811_A1854ContagemResultado_VlrCnc, P009811_n1854ContagemResultado_VlrCnc, P009811_A494ContagemResultado_Descricao, P009811_n494ContagemResultado_Descricao,
               P009811_A490ContagemResultado_ContratadaCod, P009811_n490ContagemResultado_ContratadaCod, P009811_A601ContagemResultado_Servico, P009811_n601ContagemResultado_Servico, P009811_A1237ContagemResultado_PrazoMaisDias, P009811_n1237ContagemResultado_PrazoMaisDias, P009811_A1227ContagemResultado_PrazoInicialDias, P009811_n1227ContagemResultado_PrazoInicialDias, P009811_A2017ContagemResultado_DataEntregaReal, P009811_n2017ContagemResultado_DataEntregaReal,
               P009811_A457ContagemResultado_Demanda, P009811_n457ContagemResultado_Demanda, P009811_A509ContagemrResultado_SistemaSigla, P009811_n509ContagemrResultado_SistemaSigla, P009811_A2008ContagemResultado_CntSrvAls, P009811_n2008ContagemResultado_CntSrvAls, P009811_A471ContagemResultado_DataDmn, P009811_A1051ContagemResultado_GlsValor, P009811_n1051ContagemResultado_GlsValor, P009811_A1050ContagemResultado_GlsDescricao,
               P009811_n1050ContagemResultado_GlsDescricao, P009811_A566ContagemResultado_DataUltCnt, P009811_n566ContagemResultado_DataUltCnt, P009811_A456ContagemResultado_Codigo, P009811_A512ContagemResultado_ValorPF, P009811_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P009813_A489ContagemResultado_SistemaCod, P009813_n489ContagemResultado_SistemaCod, P009813_A1553ContagemResultado_CntSrvCod, P009813_n1553ContagemResultado_CntSrvCod, P009813_A493ContagemResultado_DemandaFM, P009813_n493ContagemResultado_DemandaFM, P009813_A597ContagemResultado_LoteAceiteCod, P009813_n597ContagemResultado_LoteAceiteCod, P009813_A484ContagemResultado_StatusDmn, P009813_n484ContagemResultado_StatusDmn,
               P009813_A1854ContagemResultado_VlrCnc, P009813_n1854ContagemResultado_VlrCnc, P009813_A494ContagemResultado_Descricao, P009813_n494ContagemResultado_Descricao, P009813_A490ContagemResultado_ContratadaCod, P009813_n490ContagemResultado_ContratadaCod, P009813_A601ContagemResultado_Servico, P009813_n601ContagemResultado_Servico, P009813_A1237ContagemResultado_PrazoMaisDias, P009813_n1237ContagemResultado_PrazoMaisDias,
               P009813_A1227ContagemResultado_PrazoInicialDias, P009813_n1227ContagemResultado_PrazoInicialDias, P009813_A2017ContagemResultado_DataEntregaReal, P009813_n2017ContagemResultado_DataEntregaReal, P009813_A509ContagemrResultado_SistemaSigla, P009813_n509ContagemrResultado_SistemaSigla, P009813_A457ContagemResultado_Demanda, P009813_n457ContagemResultado_Demanda, P009813_A2008ContagemResultado_CntSrvAls, P009813_n2008ContagemResultado_CntSrvAls,
               P009813_A471ContagemResultado_DataDmn, P009813_A1051ContagemResultado_GlsValor, P009813_n1051ContagemResultado_GlsValor, P009813_A1050ContagemResultado_GlsDescricao, P009813_n1050ContagemResultado_GlsDescricao, P009813_A566ContagemResultado_DataUltCnt, P009813_n566ContagemResultado_DataUltCnt, P009813_A456ContagemResultado_Codigo, P009813_A512ContagemResultado_ValorPF, P009813_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P009814_A1404ContagemResultadoExecucao_OSCod, P009814_A1405ContagemResultadoExecucao_Codigo, P009814_A1406ContagemResultadoExecucao_Inicio
               }
               , new Object[] {
               P009815_A1013Contrato_PrepostoCod, P009815_n1013Contrato_PrepostoCod, P009815_A1016Contrato_PrepostoPesCod, P009815_n1016Contrato_PrepostoPesCod, P009815_A74Contrato_Codigo, P009815_A77Contrato_Numero, P009815_A1015Contrato_PrepostoNom, P009815_n1015Contrato_PrepostoNom, P009815_A81Contrato_Quantidade, P009815_A1354Contrato_PrdFtrCada,
               P009815_n1354Contrato_PrdFtrCada, P009815_A1357Contrato_PrdFtrIni, P009815_n1357Contrato_PrdFtrIni, P009815_A1358Contrato_PrdFtrFim, P009815_n1358Contrato_PrdFtrFim
               }
               , new Object[] {
               P009817_A40Contratada_PessoaCod, P009817_A52Contratada_AreaTrabalhoCod, P009817_A349Contratada_MunicipioCod, P009817_n349Contratada_MunicipioCod, P009817_A29Contratante_Codigo, P009817_n29Contratante_Codigo, P009817_A39Contratada_Codigo, P009817_A42Contratada_PessoaCNPJ, P009817_n42Contratada_PessoaCNPJ, P009817_A519Pessoa_Endereco,
               P009817_n519Pessoa_Endereco, P009817_A521Pessoa_CEP, P009817_n521Pessoa_CEP, P009817_A350Contratada_UF, P009817_n350Contratada_UF, P009817_A2034Contratante_TtlRltGerencial, P009817_n2034Contratante_TtlRltGerencial, P009817_A40001GXC2, P009817_n40001GXC2
               }
               , new Object[] {
               P009818_A1270ContratoServicosIndicador_CntSrvCod, P009818_A1308ContratoServicosIndicador_Tipo, P009818_n1308ContratoServicosIndicador_Tipo, P009818_A1269ContratoServicosIndicador_Codigo, P009818_A1274ContratoServicosIndicador_Indicador, P009818_A1271ContratoServicosIndicador_Numero
               }
               , new Object[] {
               P009819_A1269ContratoServicosIndicador_Codigo, P009819_A1300ContratoServicosIndicadorFaixa_Numero, P009819_A1301ContratoServicosIndicadorFaixa_Desde, P009819_A1302ContratoServicosIndicadorFaixa_Ate, P009819_A1303ContratoServicosIndicadorFaixa_Reduz, P009819_A1299ContratoServicosIndicadorFaixa_Codigo
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV36DescricaoLength ;
      private short AV89DescricaoGlsLength ;
      private short AV58Quantidade ;
      private short AV105Previstas ;
      private short AV117Atrasadas ;
      private short AV88Cumpridas ;
      private short AV94Linhas ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV44i ;
      private short AV102Prazo ;
      private short AV51p ;
      private short AV52p2 ;
      private short AV145Count ;
      private short AV142Faixa ;
      private short A1357Contrato_PrdFtrIni ;
      private short A1358Contrato_PrdFtrFim ;
      private short GXt_int4 ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private int AV28Contratada_AreaTrabalhoCod ;
      private int AV15ContagemResultado_LoteAceite ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A1620ContagemResultado_CntSrvUndCnt ;
      private int A456ContagemResultado_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1603ContagemResultado_CntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV121Contratada ;
      private int AV111Servico ;
      private int AV87Contrato_Codigo ;
      private int AV128ContratoServicos_Codigo ;
      private int A596Lote_Codigo ;
      private int Gx_OldLine ;
      private int AV123ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV124Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int AV91Indicador ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int A74Contrato_Codigo ;
      private int A81Contrato_Quantidade ;
      private int A40Contratada_PessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A349Contratada_MunicipioCod ;
      private int A29Contratante_Codigo ;
      private int A39Contratada_Codigo ;
      private int GXt_int2 ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private long A553ContagemResultado_DemandaFM_ORDER ;
      private decimal A2055Lote_CntUsado ;
      private decimal A2056Lote_CntSaldo ;
      private decimal AV30d ;
      private decimal AV138Usado ;
      private decimal AV139Saldo ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV18ContagemResultado_PFFaturar ;
      private decimal AV19ContagemResultado_PFTotal ;
      private decimal AV20ContagemResultado_PFTotalFaturar ;
      private decimal AV141SaldoPrev ;
      private decimal AV140Contratado ;
      private decimal AV97PntLoteIndice ;
      private decimal AV99PntLotePrcSemAtraso ;
      private decimal AV101PntLoteUndBruto ;
      private decimal AV109Reduz ;
      private decimal AV98PntLoteLiquido ;
      private decimal AV100PntLoteSancao ;
      private decimal AV65TotalPontos ;
      private decimal AV64TotalFinal ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal AV40GlsPF ;
      private decimal AV85GlsValor ;
      private decimal AV116ValorPF ;
      private decimal AV95PntLoteBaseCalculo ;
      private decimal AV63TotalFaturar ;
      private decimal AV92IndP ;
      private decimal GXt_decimal3 ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String AV66Verificador ;
      private String AV38FileName ;
      private String AV133Filtro ;
      private String AV73ContratanteArea ;
      private String AV62TextoLink ;
      private String scmdbuf ;
      private String A1621ContagemResultado_CntSrvUndCntSgl ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String AV75UnidadeMedicao_Sigla ;
      private String AV130Contratada_Sigla ;
      private String AV61SubTitulo ;
      private String AV11AssinadoDtHr ;
      private String Gx_time ;
      private String A563Lote_Nome ;
      private String A562Lote_Numero ;
      private String AV46NumeroRelatorio ;
      private String AV131Periodo ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV9Contrato_Numero ;
      private String AV49Os ;
      private String AV53Preposto ;
      private String AV96PntLoteIndicador ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A2008ContagemResultado_CntSrvAls ;
      private String AV84GlsTxt ;
      private String AV70strPFFinal ;
      private String AV34Descricao ;
      private String AV113StrGlsValor ;
      private String AV71strPFTotal ;
      private String AV72strPFTotalFaturar ;
      private String AV143Indicador_Sigla ;
      private String A77Contrato_Numero ;
      private String A1015Contrato_PrepostoNom ;
      private String A1354Contrato_PrdFtrCada ;
      private String A521Pessoa_CEP ;
      private String A350Contratada_UF ;
      private String A40001GXC2 ;
      private String AV125CNPJ ;
      private String AV134Endereco ;
      private String GXt_char6 ;
      private String AV147CalculoSob ;
      private String AV146ContratoServicosIndicador_Tipo ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime GXt_dtime5 ;
      private DateTime AV86Hoje ;
      private DateTime AV93Inicio ;
      private DateTime AV132Fim ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime AV104PrdFtrIni ;
      private DateTime AV103PrdFtrFim ;
      private DateTime Gx_date ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private DateTime AV33DataInicio ;
      private DateTime AV32DataFim ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV31DataCnt ;
      private DateTime AV82Entregue ;
      private DateTime AV112Solicitada ;
      private DateTime AV106Previsto ;
      private bool AV50OSAutomatica ;
      private bool GXt_boolean1 ;
      private bool n1620ContagemResultado_CntSrvUndCnt ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1621ContagemResultado_CntSrvUndCntSgl ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool returnInSub ;
      private bool n2055Lote_CntUsado ;
      private bool n2056Lote_CntSaldo ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool AV127SemOSFM ;
      private bool AV115TemIndicadorPntLote ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n457ContagemResultado_Demanda ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n2008ContagemResultado_CntSrvAls ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool AV114TemIndicadorDePontoalidade ;
      private bool AV144TemIndicadorDeFrequencia ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n349Contratada_MunicipioCod ;
      private bool n29Contratante_Codigo ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n350Contratada_UF ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n40001GXC2 ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private String AV55QrCodePath ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String AV39GlsDescricao ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String AV56QrCodeUrl ;
      private String AV155Qrcode_image_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV14ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String AV37DmnDescricao ;
      private String AV60Sistema_Coordenacao ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String A2034Contratante_TtlRltGerencial ;
      private String OV137contratante_TtlRltGerencial ;
      private String AV137contratante_TtlRltGerencial ;
      private String AV54qrCode_Image ;
      private String Qrcode_image ;
      private String Contratada_logo ;
      private String AV129Contratada_Logo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00983_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] P00983_n1620ContagemResultado_CntSrvUndCnt ;
      private int[] P00983_A456ContagemResultado_Codigo ;
      private int[] P00983_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00983_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00983_A490ContagemResultado_ContratadaCod ;
      private bool[] P00983_n490ContagemResultado_ContratadaCod ;
      private int[] P00983_A601ContagemResultado_Servico ;
      private bool[] P00983_n601ContagemResultado_Servico ;
      private int[] P00983_A1603ContagemResultado_CntCod ;
      private bool[] P00983_n1603ContagemResultado_CntCod ;
      private int[] P00983_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00983_n1553ContagemResultado_CntSrvCod ;
      private String[] P00983_A1621ContagemResultado_CntSrvUndCntSgl ;
      private bool[] P00983_n1621ContagemResultado_CntSrvUndCntSgl ;
      private String[] P00983_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00983_n803ContagemResultado_ContratadaSigla ;
      private DateTime[] P00983_A566ContagemResultado_DataUltCnt ;
      private bool[] P00983_n566ContagemResultado_DataUltCnt ;
      private String[] P00986_A563Lote_Nome ;
      private String[] P00986_A562Lote_Numero ;
      private decimal[] P00986_A2055Lote_CntUsado ;
      private bool[] P00986_n2055Lote_CntUsado ;
      private decimal[] P00986_A2056Lote_CntSaldo ;
      private bool[] P00986_n2056Lote_CntSaldo ;
      private int[] P00986_A596Lote_Codigo ;
      private DateTime[] P00986_A567Lote_DataIni ;
      private DateTime[] P00986_A568Lote_DataFim ;
      private int[] P00987_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00987_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00987_A484ContagemResultado_StatusDmn ;
      private bool[] P00987_n484ContagemResultado_StatusDmn ;
      private decimal[] P00987_A1854ContagemResultado_VlrCnc ;
      private bool[] P00987_n1854ContagemResultado_VlrCnc ;
      private int[] P00987_A456ContagemResultado_Codigo ;
      private decimal[] P00987_A512ContagemResultado_ValorPF ;
      private bool[] P00987_n512ContagemResultado_ValorPF ;
      private int[] P00988_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00988_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00988_A456ContagemResultado_Codigo ;
      private long[] P00988_A553ContagemResultado_DemandaFM_ORDER ;
      private String[] P00988_A493ContagemResultado_DemandaFM ;
      private bool[] P00988_n493ContagemResultado_DemandaFM ;
      private int[] P00989_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00989_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00989_A456ContagemResultado_Codigo ;
      private String[] P00989_A493ContagemResultado_DemandaFM ;
      private bool[] P00989_n493ContagemResultado_DemandaFM ;
      private int[] P009811_A489ContagemResultado_SistemaCod ;
      private bool[] P009811_n489ContagemResultado_SistemaCod ;
      private int[] P009811_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009811_n1553ContagemResultado_CntSrvCod ;
      private String[] P009811_A484ContagemResultado_StatusDmn ;
      private bool[] P009811_n484ContagemResultado_StatusDmn ;
      private decimal[] P009811_A1854ContagemResultado_VlrCnc ;
      private bool[] P009811_n1854ContagemResultado_VlrCnc ;
      private String[] P009811_A494ContagemResultado_Descricao ;
      private bool[] P009811_n494ContagemResultado_Descricao ;
      private int[] P009811_A490ContagemResultado_ContratadaCod ;
      private bool[] P009811_n490ContagemResultado_ContratadaCod ;
      private int[] P009811_A601ContagemResultado_Servico ;
      private bool[] P009811_n601ContagemResultado_Servico ;
      private short[] P009811_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P009811_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P009811_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P009811_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P009811_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P009811_n2017ContagemResultado_DataEntregaReal ;
      private String[] P009811_A457ContagemResultado_Demanda ;
      private bool[] P009811_n457ContagemResultado_Demanda ;
      private String[] P009811_A509ContagemrResultado_SistemaSigla ;
      private bool[] P009811_n509ContagemrResultado_SistemaSigla ;
      private String[] P009811_A2008ContagemResultado_CntSrvAls ;
      private bool[] P009811_n2008ContagemResultado_CntSrvAls ;
      private DateTime[] P009811_A471ContagemResultado_DataDmn ;
      private decimal[] P009811_A1051ContagemResultado_GlsValor ;
      private bool[] P009811_n1051ContagemResultado_GlsValor ;
      private String[] P009811_A1050ContagemResultado_GlsDescricao ;
      private bool[] P009811_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P009811_A566ContagemResultado_DataUltCnt ;
      private bool[] P009811_n566ContagemResultado_DataUltCnt ;
      private int[] P009811_A456ContagemResultado_Codigo ;
      private decimal[] P009811_A512ContagemResultado_ValorPF ;
      private bool[] P009811_n512ContagemResultado_ValorPF ;
      private int[] P009813_A489ContagemResultado_SistemaCod ;
      private bool[] P009813_n489ContagemResultado_SistemaCod ;
      private int[] P009813_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009813_n1553ContagemResultado_CntSrvCod ;
      private String[] P009813_A493ContagemResultado_DemandaFM ;
      private bool[] P009813_n493ContagemResultado_DemandaFM ;
      private int[] P009813_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P009813_n597ContagemResultado_LoteAceiteCod ;
      private String[] P009813_A484ContagemResultado_StatusDmn ;
      private bool[] P009813_n484ContagemResultado_StatusDmn ;
      private decimal[] P009813_A1854ContagemResultado_VlrCnc ;
      private bool[] P009813_n1854ContagemResultado_VlrCnc ;
      private String[] P009813_A494ContagemResultado_Descricao ;
      private bool[] P009813_n494ContagemResultado_Descricao ;
      private int[] P009813_A490ContagemResultado_ContratadaCod ;
      private bool[] P009813_n490ContagemResultado_ContratadaCod ;
      private int[] P009813_A601ContagemResultado_Servico ;
      private bool[] P009813_n601ContagemResultado_Servico ;
      private short[] P009813_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P009813_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P009813_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P009813_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P009813_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P009813_n2017ContagemResultado_DataEntregaReal ;
      private String[] P009813_A509ContagemrResultado_SistemaSigla ;
      private bool[] P009813_n509ContagemrResultado_SistemaSigla ;
      private String[] P009813_A457ContagemResultado_Demanda ;
      private bool[] P009813_n457ContagemResultado_Demanda ;
      private String[] P009813_A2008ContagemResultado_CntSrvAls ;
      private bool[] P009813_n2008ContagemResultado_CntSrvAls ;
      private DateTime[] P009813_A471ContagemResultado_DataDmn ;
      private decimal[] P009813_A1051ContagemResultado_GlsValor ;
      private bool[] P009813_n1051ContagemResultado_GlsValor ;
      private String[] P009813_A1050ContagemResultado_GlsDescricao ;
      private bool[] P009813_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P009813_A566ContagemResultado_DataUltCnt ;
      private bool[] P009813_n566ContagemResultado_DataUltCnt ;
      private int[] P009813_A456ContagemResultado_Codigo ;
      private decimal[] P009813_A512ContagemResultado_ValorPF ;
      private bool[] P009813_n512ContagemResultado_ValorPF ;
      private int[] P009814_A1404ContagemResultadoExecucao_OSCod ;
      private int[] P009814_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] P009814_A1406ContagemResultadoExecucao_Inicio ;
      private int[] P009815_A1013Contrato_PrepostoCod ;
      private bool[] P009815_n1013Contrato_PrepostoCod ;
      private int[] P009815_A1016Contrato_PrepostoPesCod ;
      private bool[] P009815_n1016Contrato_PrepostoPesCod ;
      private int[] P009815_A74Contrato_Codigo ;
      private String[] P009815_A77Contrato_Numero ;
      private String[] P009815_A1015Contrato_PrepostoNom ;
      private bool[] P009815_n1015Contrato_PrepostoNom ;
      private int[] P009815_A81Contrato_Quantidade ;
      private String[] P009815_A1354Contrato_PrdFtrCada ;
      private bool[] P009815_n1354Contrato_PrdFtrCada ;
      private short[] P009815_A1357Contrato_PrdFtrIni ;
      private bool[] P009815_n1357Contrato_PrdFtrIni ;
      private short[] P009815_A1358Contrato_PrdFtrFim ;
      private bool[] P009815_n1358Contrato_PrdFtrFim ;
      private int[] P009817_A40Contratada_PessoaCod ;
      private int[] P009817_A52Contratada_AreaTrabalhoCod ;
      private int[] P009817_A349Contratada_MunicipioCod ;
      private bool[] P009817_n349Contratada_MunicipioCod ;
      private int[] P009817_A29Contratante_Codigo ;
      private bool[] P009817_n29Contratante_Codigo ;
      private int[] P009817_A39Contratada_Codigo ;
      private String[] P009817_A42Contratada_PessoaCNPJ ;
      private bool[] P009817_n42Contratada_PessoaCNPJ ;
      private String[] P009817_A519Pessoa_Endereco ;
      private bool[] P009817_n519Pessoa_Endereco ;
      private String[] P009817_A521Pessoa_CEP ;
      private bool[] P009817_n521Pessoa_CEP ;
      private String[] P009817_A350Contratada_UF ;
      private bool[] P009817_n350Contratada_UF ;
      private String[] P009817_A2034Contratante_TtlRltGerencial ;
      private bool[] P009817_n2034Contratante_TtlRltGerencial ;
      private String[] P009817_A40001GXC2 ;
      private bool[] P009817_n40001GXC2 ;
      private int[] P009818_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P009818_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P009818_n1308ContratoServicosIndicador_Tipo ;
      private int[] P009818_A1269ContratoServicosIndicador_Codigo ;
      private String[] P009818_A1274ContratoServicosIndicador_Indicador ;
      private short[] P009818_A1271ContratoServicosIndicador_Numero ;
      private int[] P009819_A1269ContratoServicosIndicador_Codigo ;
      private short[] P009819_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P009819_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P009819_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P009819_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private int[] P009819_A1299ContratoServicosIndicadorFaixa_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV119Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV148SemCodigos ;
      private wwpbaseobjects.SdtWWPContext AV68WWPContext ;
   }

   public class rel_contagemgerencialarqpdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00983 ;
          prmP00983 = new Object[] {
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00986 ;
          prmP00986 = new Object[] {
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00987 ;
          prmP00987 = new Object[] {
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00988 ;
          prmP00988 = new Object[] {
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00989 ;
          prmP00989 = new Object[] {
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009811 ;
          prmP009811 = new Object[] {
          new Object[] {"@AV123ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009813 ;
          prmP009813 = new Object[] {
          new Object[] {"@AV14ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009814 ;
          prmP009814 = new Object[] {
          new Object[] {"@AV124Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009815 ;
          prmP009815 = new Object[] {
          new Object[] {"@AV87Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009817 ;
          prmP009817 = new Object[] {
          new Object[] {"@AV121Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009818 ;
          prmP009818 = new Object[] {
          new Object[] {"@AV128ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV146ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP009819 ;
          prmP009819 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00983", "SELECT TOP 1 T4.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[UnidadeMedicao_Sigla] AS ContagemResultado_CntSrvUndCntSgl, T3.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, COALESCE( T2.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T5 WITH (NOLOCK) ON T5.[UnidadeMedicao_Codigo] = T4.[ContratoServicos_UnidadeContratada]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV15ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00983,1,0,true,true )
             ,new CursorDef("P00986", "SELECT T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_CntUsado], T1.[Lote_CntSaldo], T1.[Lote_Codigo], COALESCE( T2.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T2.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim FROM ([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T3.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T3 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T3.[ContagemResultado_Codigo]) GROUP BY T3.[ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV15ContagemResultado_LoteAceite ORDER BY T1.[Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00986,1,0,false,true )
             ,new CursorDef("P00987", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_StatusDmn], [ContagemResultado_VlrCnc], [ContagemResultado_Codigo], [ContagemResultado_ValorPF] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV15ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00987,100,0,true,false )
             ,new CursorDef("P00988", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo], CONVERT( DECIMAL(28,14), SUBSTRING(COALESCE( [ContagemResultado_DemandaFM], ''), 1, 18)) AS ContagemResultado_DemandaFM_ORDER, [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV15ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_DemandaFM_ORDER] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00988,100,0,true,false )
             ,new CursorDef("P00989", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV15ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00989,100,0,true,false )
             ,new CursorDef("P009811", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_Demanda], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV123ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009811,1,0,true,true )
             ,new CursorDef("P009813", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntregaReal], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Demanda], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_DemandaFM] = @AV14ContagemResultado_DemandaFM) AND (T1.[ContagemResultado_LoteAceiteCod] = @AV15ContagemResultado_LoteAceite) ORDER BY T1.[ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009813,100,0,true,false )
             ,new CursorDef("P009814", "SELECT [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Inicio] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @AV124Codigo ORDER BY [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009814,100,0,true,false )
             ,new CursorDef("P009815", "SELECT TOP 1 T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T2.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T1.[Contrato_Codigo], T1.[Contrato_Numero], T3.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_Quantidade], T1.[Contrato_PrdFtrCada], T1.[Contrato_PrdFtrIni], T1.[Contrato_PrdFtrFim] FROM (([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV87Contrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009815,1,0,false,true )
             ,new CursorDef("P009817", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T3.[Contratante_Codigo], T1.[Contratada_Codigo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Endereco], T2.[Pessoa_CEP], T5.[Estado_UF] AS Contratada_UF, T4.[Contratante_TtlRltGerencial], COALESCE( T6.[GXC2], 0) AS GXC2 FROM ((((([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T3.[Contratante_Codigo]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T1.[Contratada_MunicipioCod]) LEFT JOIN (SELECT RTRIM(LTRIM([Municipio_Nome])) AS GXC2, [Municipio_Codigo] FROM [Municipio] WITH (NOLOCK) ) T6 ON T6.[Municipio_Codigo] = T1.[Contratada_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV121Contratada ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009817,1,0,false,true )
             ,new CursorDef("P009818", "SELECT [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Numero] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @AV128ContratoServicos_Codigo and [ContratoServicosIndicador_Tipo] = @AV146ContratoServicosIndicador_Tipo ORDER BY [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009818,100,0,true,false )
             ,new CursorDef("P009819", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009819,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((short[]) buf[18])[0] = rslt.getShort(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((short[]) buf[20])[0] = rslt.getShort(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getString(13, 25) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[35])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((int[]) buf[37])[0] = rslt.getInt(20) ;
                ((decimal[]) buf[38])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 2) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 0) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
