/*
               File: PromptFeriados
        Description: Selecione Feriados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:43:29.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptferiados : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptferiados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptferiados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref DateTime aP0_InOutFeriado_Data ,
                           ref String aP1_InOutFeriado_Nome )
      {
         this.AV7InOutFeriado_Data = aP0_InOutFeriado_Data;
         this.AV8InOutFeriado_Nome = aP1_InOutFeriado_Nome;
         executePrivate();
         aP0_InOutFeriado_Data=this.AV7InOutFeriado_Data;
         aP1_InOutFeriado_Nome=this.AV8InOutFeriado_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkFeriado_Fixo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_92 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_92_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_92_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV29Ano1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Ano1), 4, 0)));
               AV16Feriado_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
               AV27Feriado_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Feriado_Data1", context.localUtil.Format(AV27Feriado_Data1, "99/99/99"));
               AV28Feriado_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data_To1", context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"));
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV30Ano2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano2), 4, 0)));
               AV19Feriado_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
               AV31Feriado_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Feriado_Data2", context.localUtil.Format(AV31Feriado_Data2, "99/99/99"));
               AV32Feriado_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data_To2", context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"));
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV33Ano3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Ano3), 4, 0)));
               AV22Feriado_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
               AV34Feriado_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Feriado_Data3", context.localUtil.Format(AV34Feriado_Data3, "99/99/99"));
               AV35Feriado_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data_To3", context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"));
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV37TFFeriado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFeriado_Data", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
               AV38TFFeriado_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFeriado_Data_To", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
               AV43TFFeriado_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
               AV44TFFeriado_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFeriado_Nome_Sel", AV44TFFeriado_Nome_Sel);
               AV47TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0));
               AV41ddo_Feriado_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Feriado_DataTitleControlIdToReplace", AV41ddo_Feriado_DataTitleControlIdToReplace);
               AV45ddo_Feriado_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Feriado_NomeTitleControlIdToReplace", AV45ddo_Feriado_NomeTitleControlIdToReplace);
               AV48ddo_Feriado_FixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_FixoTitleControlIdToReplace", AV48ddo_Feriado_FixoTitleControlIdToReplace);
               AV56Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutFeriado_Data = context.localUtil.ParseDateParm( gxfirstwebparm);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFeriado_Data", context.localUtil.Format(AV7InOutFeriado_Data, "99/99/99"));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutFeriado_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFeriado_Nome", AV8InOutFeriado_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAIA2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV56Pgmname = "PromptFeriados";
               context.Gx_err = 0;
               WSIA2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEIA2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823432956");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptferiados.aspx") + "?" + UrlEncode(DateTimeUtil.FormatDateParm(AV7InOutFeriado_Data)) + "," + UrlEncode(StringUtil.RTrim(AV8InOutFeriado_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vANO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Ano1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME1", StringUtil.RTrim( AV16Feriado_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA1", context.localUtil.Format(AV27Feriado_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO1", context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vANO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30Ano2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME2", StringUtil.RTrim( AV19Feriado_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA2", context.localUtil.Format(AV31Feriado_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO2", context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vANO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Ano3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME3", StringUtil.RTrim( AV22Feriado_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA3", context.localUtil.Format(AV34Feriado_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO3", context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_DATA", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_DATA_TO", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_NOME", StringUtil.RTrim( AV43TFFeriado_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_NOME_SEL", StringUtil.RTrim( AV44TFFeriado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_FIXO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_92", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_92), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_DATATITLEFILTERDATA", AV36Feriado_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_DATATITLEFILTERDATA", AV36Feriado_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_NOMETITLEFILTERDATA", AV42Feriado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_NOMETITLEFILTERDATA", AV42Feriado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_FIXOTITLEFILTERDATA", AV46Feriado_FixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_FIXOTITLEFILTERDATA", AV46Feriado_FixoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV56Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTFERIADO_DATA", context.localUtil.DToC( AV7InOutFeriado_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vINOUTFERIADO_NOME", StringUtil.RTrim( AV8InOutFeriado_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Caption", StringUtil.RTrim( Ddo_feriado_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Tooltip", StringUtil.RTrim( Ddo_feriado_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Cls", StringUtil.RTrim( Ddo_feriado_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_feriado_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_feriado_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_feriado_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_feriado_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filtertype", StringUtil.RTrim( Ddo_feriado_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_feriado_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Datalistfixedvalues", StringUtil.RTrim( Ddo_feriado_data_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_feriado_data_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortasc", StringUtil.RTrim( Ddo_feriado_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortdsc", StringUtil.RTrim( Ddo_feriado_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Loadingdata", StringUtil.RTrim( Ddo_feriado_data_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_feriado_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_feriado_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_feriado_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Noresultsfound", StringUtil.RTrim( Ddo_feriado_data_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Caption", StringUtil.RTrim( Ddo_feriado_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Tooltip", StringUtil.RTrim( Ddo_feriado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Cls", StringUtil.RTrim( Ddo_feriado_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_feriado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_feriado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_feriado_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_feriado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filtertype", StringUtil.RTrim( Ddo_feriado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_feriado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_feriado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_feriado_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_feriado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_feriado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Sortasc", StringUtil.RTrim( Ddo_feriado_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Sortdsc", StringUtil.RTrim( Ddo_feriado_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_feriado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_feriado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_feriado_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_feriado_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_feriado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Caption", StringUtil.RTrim( Ddo_feriado_fixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Tooltip", StringUtil.RTrim( Ddo_feriado_fixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Cls", StringUtil.RTrim( Ddo_feriado_fixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Selectedvalue_set", StringUtil.RTrim( Ddo_feriado_fixo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_fixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_fixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_fixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_fixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Sortedstatus", StringUtil.RTrim( Ddo_feriado_fixo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includefilter", StringUtil.BoolToStr( Ddo_feriado_fixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Filterisrange", StringUtil.BoolToStr( Ddo_feriado_fixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_fixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Datalisttype", StringUtil.RTrim( Ddo_feriado_fixo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Datalistfixedvalues", StringUtil.RTrim( Ddo_feriado_fixo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_feriado_fixo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Sortasc", StringUtil.RTrim( Ddo_feriado_fixo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Sortdsc", StringUtil.RTrim( Ddo_feriado_fixo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Loadingdata", StringUtil.RTrim( Ddo_feriado_fixo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Cleanfilter", StringUtil.RTrim( Ddo_feriado_fixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_feriado_fixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Rangefilterto", StringUtil.RTrim( Ddo_feriado_fixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Noresultsfound", StringUtil.RTrim( Ddo_feriado_fixo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_fixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_feriado_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_feriado_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_feriado_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_feriado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_feriado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_feriado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Activeeventkey", StringUtil.RTrim( Ddo_feriado_fixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Selectedvalue_get", StringUtil.RTrim( Ddo_feriado_fixo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIA2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptFeriados" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Feriados" ;
      }

      protected void WBIA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_IA2( true) ;
         }
         else
         {
            wb_table1_2_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfferiado_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_data_Internalname, context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"), context.localUtil.Format( AV37TFFeriado_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavTfferiado_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfferiado_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfferiado_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_data_to_Internalname, context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"), context.localUtil.Format( AV38TFFeriado_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavTfferiado_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfferiado_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_feriado_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_feriado_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_feriado_dataauxdate_Internalname, context.localUtil.Format(AV39DDO_Feriado_DataAuxDate, "99/99/99"), context.localUtil.Format( AV39DDO_Feriado_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_feriado_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_feriado_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_feriado_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_feriado_dataauxdateto_Internalname, context.localUtil.Format(AV40DDO_Feriado_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV40DDO_Feriado_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_feriado_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_feriado_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_nome_Internalname, StringUtil.RTrim( AV43TFFeriado_Nome), StringUtil.RTrim( context.localUtil.Format( AV43TFFeriado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_nome_sel_Internalname, StringUtil.RTrim( AV44TFFeriado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV44TFFeriado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_fixo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFFeriado_Fixo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_fixo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_fixo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname, AV41ddo_Feriado_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_feriado_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname, AV45ddo_Feriado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_feriado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_FIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname, AV48ddo_Feriado_FixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFeriados.htm");
         }
         wbLoad = true;
      }

      protected void STARTIA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Feriados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIA0( ) ;
      }

      protected void WSIA2( )
      {
         STARTIA2( ) ;
         EVTIA2( ) ;
      }

      protected void EVTIA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11IA2 */
                           E11IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12IA2 */
                           E12IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13IA2 */
                           E13IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_FIXO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14IA2 */
                           E14IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15IA2 */
                           E15IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16IA2 */
                           E16IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17IA2 */
                           E17IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18IA2 */
                           E18IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19IA2 */
                           E19IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20IA2 */
                           E20IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21IA2 */
                           E21IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22IA2 */
                           E22IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23IA2 */
                           E23IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24IA2 */
                           E24IA2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_92_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
                           SubsflControlProps_922( ) ;
                           AV25Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV55Select_GXI : context.convertURL( context.PathToRelativeUrl( AV25Select))));
                           A1175Feriado_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtFeriado_Data_Internalname), 0));
                           A1176Feriado_Nome = StringUtil.Upper( cgiGet( edtFeriado_Nome_Internalname));
                           A1195Feriado_Fixo = StringUtil.StrToBool( cgiGet( chkFeriado_Fixo_Internalname));
                           n1195Feriado_Fixo = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25IA2 */
                                 E25IA2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26IA2 */
                                 E26IA2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27IA2 */
                                 E27IA2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ano1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO1"), ",", ".") != Convert.ToDecimal( AV29Ano1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME1"), AV16Feriado_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA1"), 0) != AV27Feriado_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO1"), 0) != AV28Feriado_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ano2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO2"), ",", ".") != Convert.ToDecimal( AV30Ano2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME2"), AV19Feriado_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA2"), 0) != AV31Feriado_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO2"), 0) != AV32Feriado_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ano3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO3"), ",", ".") != Convert.ToDecimal( AV33Ano3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME3"), AV22Feriado_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA3"), 0) != AV34Feriado_Data3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Feriado_data_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO3"), 0) != AV35Feriado_Data_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfferiado_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA"), 0) != AV37TFFeriado_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfferiado_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA_TO"), 0) != AV38TFFeriado_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfferiado_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME"), AV43TFFeriado_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfferiado_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME_SEL"), AV44TFFeriado_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfferiado_fixo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFERIADO_FIXO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFFeriado_Fixo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E28IA2 */
                                       E28IA2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEIA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIA2( ) ;
            }
         }
      }

      protected void PAIA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector1.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector2.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector3.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector3.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            GXCCtl = "FERIADO_FIXO_" + sGXsfl_92_idx;
            chkFeriado_Fixo.Name = GXCCtl;
            chkFeriado_Fixo.WebTags = "";
            chkFeriado_Fixo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "TitleCaption", chkFeriado_Fixo.Caption);
            chkFeriado_Fixo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_922( ) ;
         while ( nGXsfl_92_idx <= nRC_GXsfl_92 )
         {
            sendrow_922( ) ;
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV29Ano1 ,
                                       String AV16Feriado_Nome1 ,
                                       DateTime AV27Feriado_Data1 ,
                                       DateTime AV28Feriado_Data_To1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       short AV30Ano2 ,
                                       String AV19Feriado_Nome2 ,
                                       DateTime AV31Feriado_Data2 ,
                                       DateTime AV32Feriado_Data_To2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       short AV33Ano3 ,
                                       String AV22Feriado_Nome3 ,
                                       DateTime AV34Feriado_Data3 ,
                                       DateTime AV35Feriado_Data_To3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       DateTime AV37TFFeriado_Data ,
                                       DateTime AV38TFFeriado_Data_To ,
                                       String AV43TFFeriado_Nome ,
                                       String AV44TFFeriado_Nome_Sel ,
                                       short AV47TFFeriado_Fixo_Sel ,
                                       String AV41ddo_Feriado_DataTitleControlIdToReplace ,
                                       String AV45ddo_Feriado_NomeTitleControlIdToReplace ,
                                       String AV48ddo_Feriado_FixoTitleControlIdToReplace ,
                                       String AV56Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_DATA", GetSecureSignedToken( "", A1175Feriado_Data));
         GxWebStd.gx_hidden_field( context, "FERIADO_DATA", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "FERIADO_NOME", StringUtil.RTrim( A1176Feriado_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_FIXO", GetSecureSignedToken( "", A1195Feriado_Fixo));
         GxWebStd.gx_hidden_field( context, "FERIADO_FIXO", StringUtil.BoolToStr( A1195Feriado_Fixo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV56Pgmname = "PromptFeriados";
         context.Gx_err = 0;
      }

      protected void RFIA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 92;
         /* Execute user event: E26IA2 */
         E26IA2 ();
         nGXsfl_92_idx = 1;
         sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
         SubsflControlProps_922( ) ;
         nGXsfl_92_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_922( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV29Ano1 ,
                                                 AV16Feriado_Nome1 ,
                                                 AV27Feriado_Data1 ,
                                                 AV28Feriado_Data_To1 ,
                                                 AV17DynamicFiltersEnabled2 ,
                                                 AV18DynamicFiltersSelector2 ,
                                                 AV30Ano2 ,
                                                 AV19Feriado_Nome2 ,
                                                 AV31Feriado_Data2 ,
                                                 AV32Feriado_Data_To2 ,
                                                 AV20DynamicFiltersEnabled3 ,
                                                 AV21DynamicFiltersSelector3 ,
                                                 AV33Ano3 ,
                                                 AV22Feriado_Nome3 ,
                                                 AV34Feriado_Data3 ,
                                                 AV35Feriado_Data_To3 ,
                                                 AV37TFFeriado_Data ,
                                                 AV38TFFeriado_Data_To ,
                                                 AV44TFFeriado_Nome_Sel ,
                                                 AV43TFFeriado_Nome ,
                                                 AV47TFFeriado_Fixo_Sel ,
                                                 A1175Feriado_Data ,
                                                 A1176Feriado_Nome ,
                                                 A1195Feriado_Fixo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV16Feriado_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV16Feriado_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
            lV19Feriado_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV19Feriado_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
            lV22Feriado_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV22Feriado_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
            lV43TFFeriado_Nome = StringUtil.PadR( StringUtil.RTrim( AV43TFFeriado_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
            /* Using cursor H00IA2 */
            pr_default.execute(0, new Object[] {AV29Ano1, lV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV30Ano2, lV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV33Ano3, lV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, lV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_92_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1195Feriado_Fixo = H00IA2_A1195Feriado_Fixo[0];
               n1195Feriado_Fixo = H00IA2_n1195Feriado_Fixo[0];
               A1176Feriado_Nome = H00IA2_A1176Feriado_Nome[0];
               A1175Feriado_Data = H00IA2_A1175Feriado_Data[0];
               /* Execute user event: E27IA2 */
               E27IA2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 92;
            WBIA0( ) ;
         }
         nGXsfl_92_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV29Ano1 ,
                                              AV16Feriado_Nome1 ,
                                              AV27Feriado_Data1 ,
                                              AV28Feriado_Data_To1 ,
                                              AV17DynamicFiltersEnabled2 ,
                                              AV18DynamicFiltersSelector2 ,
                                              AV30Ano2 ,
                                              AV19Feriado_Nome2 ,
                                              AV31Feriado_Data2 ,
                                              AV32Feriado_Data_To2 ,
                                              AV20DynamicFiltersEnabled3 ,
                                              AV21DynamicFiltersSelector3 ,
                                              AV33Ano3 ,
                                              AV22Feriado_Nome3 ,
                                              AV34Feriado_Data3 ,
                                              AV35Feriado_Data_To3 ,
                                              AV37TFFeriado_Data ,
                                              AV38TFFeriado_Data_To ,
                                              AV44TFFeriado_Nome_Sel ,
                                              AV43TFFeriado_Nome ,
                                              AV47TFFeriado_Fixo_Sel ,
                                              A1175Feriado_Data ,
                                              A1176Feriado_Nome ,
                                              A1195Feriado_Fixo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV16Feriado_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV16Feriado_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
         lV19Feriado_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV19Feriado_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
         lV22Feriado_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV22Feriado_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
         lV43TFFeriado_Nome = StringUtil.PadR( StringUtil.RTrim( AV43TFFeriado_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
         /* Using cursor H00IA3 */
         pr_default.execute(1, new Object[] {AV29Ano1, lV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV30Ano2, lV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV33Ano3, lV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, lV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel});
         GRID_nRecordCount = H00IA3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIA0( )
      {
         /* Before Start, stand alone formulas. */
         AV56Pgmname = "PromptFeriados";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25IA2 */
         E25IA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV49DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_DATATITLEFILTERDATA"), AV36Feriado_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_NOMETITLEFILTERDATA"), AV42Feriado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_FIXOTITLEFILTERDATA"), AV46Feriado_FixoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO1");
               GX_FocusControl = edtavAno1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29Ano1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Ano1), 4, 0)));
            }
            else
            {
               AV29Ano1 = (short)(context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Ano1), 4, 0)));
            }
            AV16Feriado_Nome1 = StringUtil.Upper( cgiGet( edtavFeriado_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data1"}), 1, "vFERIADO_DATA1");
               GX_FocusControl = edtavFeriado_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27Feriado_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Feriado_Data1", context.localUtil.Format(AV27Feriado_Data1, "99/99/99"));
            }
            else
            {
               AV27Feriado_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Feriado_Data1", context.localUtil.Format(AV27Feriado_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To1"}), 1, "vFERIADO_DATA_TO1");
               GX_FocusControl = edtavFeriado_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28Feriado_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data_To1", context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"));
            }
            else
            {
               AV28Feriado_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data_To1", context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO2");
               GX_FocusControl = edtavAno2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30Ano2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano2), 4, 0)));
            }
            else
            {
               AV30Ano2 = (short)(context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano2), 4, 0)));
            }
            AV19Feriado_Nome2 = StringUtil.Upper( cgiGet( edtavFeriado_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data2"}), 1, "vFERIADO_DATA2");
               GX_FocusControl = edtavFeriado_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31Feriado_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Feriado_Data2", context.localUtil.Format(AV31Feriado_Data2, "99/99/99"));
            }
            else
            {
               AV31Feriado_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Feriado_Data2", context.localUtil.Format(AV31Feriado_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To2"}), 1, "vFERIADO_DATA_TO2");
               GX_FocusControl = edtavFeriado_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32Feriado_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data_To2", context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"));
            }
            else
            {
               AV32Feriado_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data_To2", context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO3");
               GX_FocusControl = edtavAno3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Ano3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Ano3), 4, 0)));
            }
            else
            {
               AV33Ano3 = (short)(context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Ano3), 4, 0)));
            }
            AV22Feriado_Nome3 = StringUtil.Upper( cgiGet( edtavFeriado_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data3"}), 1, "vFERIADO_DATA3");
               GX_FocusControl = edtavFeriado_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Feriado_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Feriado_Data3", context.localUtil.Format(AV34Feriado_Data3, "99/99/99"));
            }
            else
            {
               AV34Feriado_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Feriado_Data3", context.localUtil.Format(AV34Feriado_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To3"}), 1, "vFERIADO_DATA_TO3");
               GX_FocusControl = edtavFeriado_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Feriado_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data_To3", context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"));
            }
            else
            {
               AV35Feriado_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data_To3", context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"));
            }
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfferiado_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFFeriado_Data"}), 1, "vTFFERIADO_DATA");
               GX_FocusControl = edtavTfferiado_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFFeriado_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFeriado_Data", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
            }
            else
            {
               AV37TFFeriado_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfferiado_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFeriado_Data", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfferiado_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFFeriado_Data_To"}), 1, "vTFFERIADO_DATA_TO");
               GX_FocusControl = edtavTfferiado_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFFeriado_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFeriado_Data_To", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
            }
            else
            {
               AV38TFFeriado_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfferiado_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFeriado_Data_To", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_feriado_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Feriado_Data Aux Date"}), 1, "vDDO_FERIADO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_feriado_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39DDO_Feriado_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_Feriado_DataAuxDate", context.localUtil.Format(AV39DDO_Feriado_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV39DDO_Feriado_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_feriado_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_Feriado_DataAuxDate", context.localUtil.Format(AV39DDO_Feriado_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_feriado_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Feriado_Data Aux Date To"}), 1, "vDDO_FERIADO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_feriado_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40DDO_Feriado_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_Feriado_DataAuxDateTo", context.localUtil.Format(AV40DDO_Feriado_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV40DDO_Feriado_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_feriado_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_Feriado_DataAuxDateTo", context.localUtil.Format(AV40DDO_Feriado_DataAuxDateTo, "99/99/99"));
            }
            AV43TFFeriado_Nome = StringUtil.Upper( cgiGet( edtavTfferiado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
            AV44TFFeriado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfferiado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFeriado_Nome_Sel", AV44TFFeriado_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFERIADO_FIXO_SEL");
               GX_FocusControl = edtavTfferiado_fixo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFFeriado_Fixo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0));
            }
            else
            {
               AV47TFFeriado_Fixo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0));
            }
            AV41ddo_Feriado_DataTitleControlIdToReplace = cgiGet( edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Feriado_DataTitleControlIdToReplace", AV41ddo_Feriado_DataTitleControlIdToReplace);
            AV45ddo_Feriado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Feriado_NomeTitleControlIdToReplace", AV45ddo_Feriado_NomeTitleControlIdToReplace);
            AV48ddo_Feriado_FixoTitleControlIdToReplace = cgiGet( edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_FixoTitleControlIdToReplace", AV48ddo_Feriado_FixoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_92 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_92"), ",", "."));
            AV51GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV52GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_feriado_data_Caption = cgiGet( "DDO_FERIADO_DATA_Caption");
            Ddo_feriado_data_Tooltip = cgiGet( "DDO_FERIADO_DATA_Tooltip");
            Ddo_feriado_data_Cls = cgiGet( "DDO_FERIADO_DATA_Cls");
            Ddo_feriado_data_Filteredtext_set = cgiGet( "DDO_FERIADO_DATA_Filteredtext_set");
            Ddo_feriado_data_Filteredtextto_set = cgiGet( "DDO_FERIADO_DATA_Filteredtextto_set");
            Ddo_feriado_data_Dropdownoptionstype = cgiGet( "DDO_FERIADO_DATA_Dropdownoptionstype");
            Ddo_feriado_data_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_DATA_Titlecontrolidtoreplace");
            Ddo_feriado_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includesortasc"));
            Ddo_feriado_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includesortdsc"));
            Ddo_feriado_data_Sortedstatus = cgiGet( "DDO_FERIADO_DATA_Sortedstatus");
            Ddo_feriado_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includefilter"));
            Ddo_feriado_data_Filtertype = cgiGet( "DDO_FERIADO_DATA_Filtertype");
            Ddo_feriado_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Filterisrange"));
            Ddo_feriado_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includedatalist"));
            Ddo_feriado_data_Datalistfixedvalues = cgiGet( "DDO_FERIADO_DATA_Datalistfixedvalues");
            Ddo_feriado_data_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FERIADO_DATA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_feriado_data_Sortasc = cgiGet( "DDO_FERIADO_DATA_Sortasc");
            Ddo_feriado_data_Sortdsc = cgiGet( "DDO_FERIADO_DATA_Sortdsc");
            Ddo_feriado_data_Loadingdata = cgiGet( "DDO_FERIADO_DATA_Loadingdata");
            Ddo_feriado_data_Cleanfilter = cgiGet( "DDO_FERIADO_DATA_Cleanfilter");
            Ddo_feriado_data_Rangefilterfrom = cgiGet( "DDO_FERIADO_DATA_Rangefilterfrom");
            Ddo_feriado_data_Rangefilterto = cgiGet( "DDO_FERIADO_DATA_Rangefilterto");
            Ddo_feriado_data_Noresultsfound = cgiGet( "DDO_FERIADO_DATA_Noresultsfound");
            Ddo_feriado_data_Searchbuttontext = cgiGet( "DDO_FERIADO_DATA_Searchbuttontext");
            Ddo_feriado_nome_Caption = cgiGet( "DDO_FERIADO_NOME_Caption");
            Ddo_feriado_nome_Tooltip = cgiGet( "DDO_FERIADO_NOME_Tooltip");
            Ddo_feriado_nome_Cls = cgiGet( "DDO_FERIADO_NOME_Cls");
            Ddo_feriado_nome_Filteredtext_set = cgiGet( "DDO_FERIADO_NOME_Filteredtext_set");
            Ddo_feriado_nome_Selectedvalue_set = cgiGet( "DDO_FERIADO_NOME_Selectedvalue_set");
            Ddo_feriado_nome_Dropdownoptionstype = cgiGet( "DDO_FERIADO_NOME_Dropdownoptionstype");
            Ddo_feriado_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_NOME_Titlecontrolidtoreplace");
            Ddo_feriado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includesortasc"));
            Ddo_feriado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includesortdsc"));
            Ddo_feriado_nome_Sortedstatus = cgiGet( "DDO_FERIADO_NOME_Sortedstatus");
            Ddo_feriado_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includefilter"));
            Ddo_feriado_nome_Filtertype = cgiGet( "DDO_FERIADO_NOME_Filtertype");
            Ddo_feriado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Filterisrange"));
            Ddo_feriado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includedatalist"));
            Ddo_feriado_nome_Datalisttype = cgiGet( "DDO_FERIADO_NOME_Datalisttype");
            Ddo_feriado_nome_Datalistfixedvalues = cgiGet( "DDO_FERIADO_NOME_Datalistfixedvalues");
            Ddo_feriado_nome_Datalistproc = cgiGet( "DDO_FERIADO_NOME_Datalistproc");
            Ddo_feriado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FERIADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_feriado_nome_Sortasc = cgiGet( "DDO_FERIADO_NOME_Sortasc");
            Ddo_feriado_nome_Sortdsc = cgiGet( "DDO_FERIADO_NOME_Sortdsc");
            Ddo_feriado_nome_Loadingdata = cgiGet( "DDO_FERIADO_NOME_Loadingdata");
            Ddo_feriado_nome_Cleanfilter = cgiGet( "DDO_FERIADO_NOME_Cleanfilter");
            Ddo_feriado_nome_Rangefilterfrom = cgiGet( "DDO_FERIADO_NOME_Rangefilterfrom");
            Ddo_feriado_nome_Rangefilterto = cgiGet( "DDO_FERIADO_NOME_Rangefilterto");
            Ddo_feriado_nome_Noresultsfound = cgiGet( "DDO_FERIADO_NOME_Noresultsfound");
            Ddo_feriado_nome_Searchbuttontext = cgiGet( "DDO_FERIADO_NOME_Searchbuttontext");
            Ddo_feriado_fixo_Caption = cgiGet( "DDO_FERIADO_FIXO_Caption");
            Ddo_feriado_fixo_Tooltip = cgiGet( "DDO_FERIADO_FIXO_Tooltip");
            Ddo_feriado_fixo_Cls = cgiGet( "DDO_FERIADO_FIXO_Cls");
            Ddo_feriado_fixo_Selectedvalue_set = cgiGet( "DDO_FERIADO_FIXO_Selectedvalue_set");
            Ddo_feriado_fixo_Dropdownoptionstype = cgiGet( "DDO_FERIADO_FIXO_Dropdownoptionstype");
            Ddo_feriado_fixo_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_FIXO_Titlecontrolidtoreplace");
            Ddo_feriado_fixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includesortasc"));
            Ddo_feriado_fixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includesortdsc"));
            Ddo_feriado_fixo_Sortedstatus = cgiGet( "DDO_FERIADO_FIXO_Sortedstatus");
            Ddo_feriado_fixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includefilter"));
            Ddo_feriado_fixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Filterisrange"));
            Ddo_feriado_fixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includedatalist"));
            Ddo_feriado_fixo_Datalisttype = cgiGet( "DDO_FERIADO_FIXO_Datalisttype");
            Ddo_feriado_fixo_Datalistfixedvalues = cgiGet( "DDO_FERIADO_FIXO_Datalistfixedvalues");
            Ddo_feriado_fixo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FERIADO_FIXO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_feriado_fixo_Sortasc = cgiGet( "DDO_FERIADO_FIXO_Sortasc");
            Ddo_feriado_fixo_Sortdsc = cgiGet( "DDO_FERIADO_FIXO_Sortdsc");
            Ddo_feriado_fixo_Loadingdata = cgiGet( "DDO_FERIADO_FIXO_Loadingdata");
            Ddo_feriado_fixo_Cleanfilter = cgiGet( "DDO_FERIADO_FIXO_Cleanfilter");
            Ddo_feriado_fixo_Rangefilterfrom = cgiGet( "DDO_FERIADO_FIXO_Rangefilterfrom");
            Ddo_feriado_fixo_Rangefilterto = cgiGet( "DDO_FERIADO_FIXO_Rangefilterto");
            Ddo_feriado_fixo_Noresultsfound = cgiGet( "DDO_FERIADO_FIXO_Noresultsfound");
            Ddo_feriado_fixo_Searchbuttontext = cgiGet( "DDO_FERIADO_FIXO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_feriado_data_Activeeventkey = cgiGet( "DDO_FERIADO_DATA_Activeeventkey");
            Ddo_feriado_data_Filteredtext_get = cgiGet( "DDO_FERIADO_DATA_Filteredtext_get");
            Ddo_feriado_data_Filteredtextto_get = cgiGet( "DDO_FERIADO_DATA_Filteredtextto_get");
            Ddo_feriado_nome_Activeeventkey = cgiGet( "DDO_FERIADO_NOME_Activeeventkey");
            Ddo_feriado_nome_Filteredtext_get = cgiGet( "DDO_FERIADO_NOME_Filteredtext_get");
            Ddo_feriado_nome_Selectedvalue_get = cgiGet( "DDO_FERIADO_NOME_Selectedvalue_get");
            Ddo_feriado_fixo_Activeeventkey = cgiGet( "DDO_FERIADO_FIXO_Activeeventkey");
            Ddo_feriado_fixo_Selectedvalue_get = cgiGet( "DDO_FERIADO_FIXO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO1"), ",", ".") != Convert.ToDecimal( AV29Ano1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME1"), AV16Feriado_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA1"), 0) != AV27Feriado_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO1"), 0) != AV28Feriado_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO2"), ",", ".") != Convert.ToDecimal( AV30Ano2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME2"), AV19Feriado_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA2"), 0) != AV31Feriado_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO2"), 0) != AV32Feriado_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO3"), ",", ".") != Convert.ToDecimal( AV33Ano3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME3"), AV22Feriado_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA3"), 0) != AV34Feriado_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO3"), 0) != AV35Feriado_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA"), 0) != AV37TFFeriado_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA_TO"), 0) != AV38TFFeriado_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME"), AV43TFFeriado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME_SEL"), AV44TFFeriado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFERIADO_FIXO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFFeriado_Fixo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25IA2 */
         E25IA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25IA2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfferiado_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_data_Visible), 5, 0)));
         edtavTfferiado_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_data_to_Visible), 5, 0)));
         edtavTfferiado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_nome_Visible), 5, 0)));
         edtavTfferiado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_nome_sel_Visible), 5, 0)));
         edtavTfferiado_fixo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_fixo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_fixo_sel_Visible), 5, 0)));
         Ddo_feriado_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "TitleControlIdToReplace", Ddo_feriado_data_Titlecontrolidtoreplace);
         AV41ddo_Feriado_DataTitleControlIdToReplace = Ddo_feriado_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Feriado_DataTitleControlIdToReplace", AV41ddo_Feriado_DataTitleControlIdToReplace);
         edtavDdo_feriado_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_feriado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "TitleControlIdToReplace", Ddo_feriado_nome_Titlecontrolidtoreplace);
         AV45ddo_Feriado_NomeTitleControlIdToReplace = Ddo_feriado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Feriado_NomeTitleControlIdToReplace", AV45ddo_Feriado_NomeTitleControlIdToReplace);
         edtavDdo_feriado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_feriado_fixo_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Fixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "TitleControlIdToReplace", Ddo_feriado_fixo_Titlecontrolidtoreplace);
         AV48ddo_Feriado_FixoTitleControlIdToReplace = Ddo_feriado_fixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_FixoTitleControlIdToReplace", AV48ddo_Feriado_FixoTitleControlIdToReplace);
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Feriados";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "Fixo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV49DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV49DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26IA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36Feriado_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Feriado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Feriado_FixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFeriado_Data_Titleformat = 2;
         edtFeriado_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV41ddo_Feriado_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Title", edtFeriado_Data_Title);
         edtFeriado_Nome_Titleformat = 2;
         edtFeriado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV45ddo_Feriado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Nome_Internalname, "Title", edtFeriado_Nome_Title);
         chkFeriado_Fixo_Titleformat = 2;
         chkFeriado_Fixo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fixo", AV48ddo_Feriado_FixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "Title", chkFeriado_Fixo.Title.Text);
         AV51GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridCurrentPage), 10, 0)));
         AV52GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36Feriado_DataTitleFilterData", AV36Feriado_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Feriado_NomeTitleFilterData", AV42Feriado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Feriado_FixoTitleFilterData", AV46Feriado_FixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11IA2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV50PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV50PageToGo) ;
         }
      }

      protected void E12IA2( )
      {
         /* Ddo_feriado_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFFeriado_Data = context.localUtil.CToD( Ddo_feriado_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFeriado_Data", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
            AV38TFFeriado_Data_To = context.localUtil.CToD( Ddo_feriado_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFeriado_Data_To", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13IA2( )
      {
         /* Ddo_feriado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SortedStatus", Ddo_feriado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SortedStatus", Ddo_feriado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFeriado_Nome = Ddo_feriado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
            AV44TFFeriado_Nome_Sel = Ddo_feriado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFeriado_Nome_Sel", AV44TFFeriado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14IA2( )
      {
         /* Ddo_feriado_fixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_fixo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_fixo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SortedStatus", Ddo_feriado_fixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_fixo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_fixo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SortedStatus", Ddo_feriado_fixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_fixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( Ddo_feriado_fixo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27IA2( )
      {
         /* Grid_Load Routine */
         AV25Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV25Select);
         AV55Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 92;
         }
         sendrow_922( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_92_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(92, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E28IA2 */
         E28IA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28IA2( )
      {
         /* Enter Routine */
         AV7InOutFeriado_Data = A1175Feriado_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFeriado_Data", context.localUtil.Format(AV7InOutFeriado_Data, "99/99/99"));
         AV8InOutFeriado_Nome = A1176Feriado_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFeriado_Nome", AV8InOutFeriado_Nome);
         context.setWebReturnParms(new Object[] {context.localUtil.Format( AV7InOutFeriado_Data, "99/99/99"),(String)AV8InOutFeriado_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15IA2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20IA2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16IA2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21IA2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22IA2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17IA2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23IA2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18IA2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV29Ano1, AV16Feriado_Nome1, AV27Feriado_Data1, AV28Feriado_Data_To1, AV18DynamicFiltersSelector2, AV30Ano2, AV19Feriado_Nome2, AV31Feriado_Data2, AV32Feriado_Data_To2, AV21DynamicFiltersSelector3, AV33Ano3, AV22Feriado_Nome3, AV34Feriado_Data3, AV35Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFeriado_Data, AV38TFFeriado_Data_To, AV43TFFeriado_Nome, AV44TFFeriado_Nome_Sel, AV47TFFeriado_Fixo_Sel, AV41ddo_Feriado_DataTitleControlIdToReplace, AV45ddo_Feriado_NomeTitleControlIdToReplace, AV48ddo_Feriado_FixoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24IA2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19IA2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_feriado_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
         Ddo_feriado_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SortedStatus", Ddo_feriado_nome_Sortedstatus);
         Ddo_feriado_fixo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SortedStatus", Ddo_feriado_fixo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_feriado_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_feriado_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SortedStatus", Ddo_feriado_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_feriado_fixo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SortedStatus", Ddo_feriado_fixo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAno1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno1_Visible), 5, 0)));
         edtavFeriado_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome1_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 )
         {
            edtavAno1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAno2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno2_Visible), 5, 0)));
         edtavFeriado_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome2_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 )
         {
            edtavAno2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAno3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno3_Visible), 5, 0)));
         edtavFeriado_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome3_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 )
         {
            edtavAno3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV30Ano2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV33Ano3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Ano3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFFeriado_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFeriado_Data", context.localUtil.Format(AV37TFFeriado_Data, "99/99/99"));
         Ddo_feriado_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredText_set", Ddo_feriado_data_Filteredtext_set);
         AV38TFFeriado_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFeriado_Data_To", context.localUtil.Format(AV38TFFeriado_Data_To, "99/99/99"));
         Ddo_feriado_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredTextTo_set", Ddo_feriado_data_Filteredtextto_set);
         AV43TFFeriado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFeriado_Nome", AV43TFFeriado_Nome);
         Ddo_feriado_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "FilteredText_set", Ddo_feriado_nome_Filteredtext_set);
         AV44TFFeriado_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFeriado_Nome_Sel", AV44TFFeriado_Nome_Sel);
         Ddo_feriado_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SelectedValue_set", Ddo_feriado_nome_Selectedvalue_set);
         AV47TFFeriado_Fixo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0));
         Ddo_feriado_fixo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SelectedValue_set", Ddo_feriado_fixo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV29Ano1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Ano1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 )
            {
               AV29Ano1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Ano1), 4, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 )
            {
               AV16Feriado_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 )
            {
               AV27Feriado_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Feriado_Data1", context.localUtil.Format(AV27Feriado_Data1, "99/99/99"));
               AV28Feriado_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data_To1", context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 )
               {
                  AV30Ano2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano2), 4, 0)));
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 )
               {
                  AV19Feriado_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 )
               {
                  AV31Feriado_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Feriado_Data2", context.localUtil.Format(AV31Feriado_Data2, "99/99/99"));
                  AV32Feriado_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data_To2", context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 )
                  {
                     AV33Ano3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Ano3), 4, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 )
                  {
                     AV22Feriado_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 )
                  {
                     AV34Feriado_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Feriado_Data3", context.localUtil.Format(AV34Feriado_Data3, "99/99/99"));
                     AV35Feriado_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data_To3", context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV37TFFeriado_Data) && (DateTime.MinValue==AV38TFFeriado_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV37TFFeriado_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV38TFFeriado_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFeriado_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFFeriado_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFeriado_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFFeriado_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV47TFFeriado_Fixo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_FIXO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFFeriado_Fixo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV56Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 ) && ! (0==AV29Ano1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV29Ano1), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Feriado_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16Feriado_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV27Feriado_Data1) && (DateTime.MinValue==AV28Feriado_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV27Feriado_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV28Feriado_Data_To1, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 ) && ! (0==AV30Ano2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV30Ano2), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Feriado_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Feriado_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV31Feriado_Data2) && (DateTime.MinValue==AV32Feriado_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV31Feriado_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV32Feriado_Data_To2, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 ) && ! (0==AV33Ano3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV33Ano3), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Feriado_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Feriado_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV34Feriado_Data3) && (DateTime.MinValue==AV35Feriado_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV34Feriado_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV35Feriado_Data_To3, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_IA2( true) ;
         }
         else
         {
            wb_table2_5_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_86_IA2( true) ;
         }
         else
         {
            wb_table3_86_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_86_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IA2e( true) ;
         }
         else
         {
            wb_table1_2_IA2e( false) ;
         }
      }

      protected void wb_table3_86_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_89_IA2( true) ;
         }
         else
         {
            wb_table4_89_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_89_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_86_IA2e( true) ;
         }
         else
         {
            wb_table3_86_IA2e( false) ;
         }
      }

      protected void wb_table4_89_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"92\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFeriado_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtFeriado_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFeriado_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFeriado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFeriado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFeriado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkFeriado_Fixo_Titleformat == 0 )
               {
                  context.SendWebValue( chkFeriado_Fixo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkFeriado_Fixo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFeriado_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFeriado_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1176Feriado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFeriado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFeriado_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1195Feriado_Fixo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkFeriado_Fixo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkFeriado_Fixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 92 )
         {
            wbEnd = 0;
            nRC_GXsfl_92 = (short)(nGXsfl_92_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_89_IA2e( true) ;
         }
         else
         {
            wb_table4_89_IA2e( false) ;
         }
      }

      protected void wb_table2_5_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptFeriados.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_IA2( true) ;
         }
         else
         {
            wb_table5_14_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IA2e( true) ;
         }
         else
         {
            wb_table2_5_IA2e( false) ;
         }
      }

      protected void wb_table5_14_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_IA2( true) ;
         }
         else
         {
            wb_table6_19_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_IA2e( true) ;
         }
         else
         {
            wb_table5_14_IA2e( false) ;
         }
      }

      protected void wb_table6_19_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptFeriados.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Ano1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29Ano1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno1_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_PromptFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome1_Internalname, StringUtil.RTrim( AV16Feriado_Nome1), StringUtil.RTrim( context.localUtil.Format( AV16Feriado_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFeriados.htm");
            wb_table7_30_IA2( true) ;
         }
         else
         {
            wb_table7_30_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_PromptFeriados.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30Ano2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30Ano2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno2_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_PromptFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome2_Internalname, StringUtil.RTrim( AV19Feriado_Nome2), StringUtil.RTrim( context.localUtil.Format( AV19Feriado_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFeriados.htm");
            wb_table8_51_IA2( true) ;
         }
         else
         {
            wb_table8_51_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table8_51_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_PromptFeriados.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Ano3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33Ano3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno3_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_PromptFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome3_Internalname, StringUtil.RTrim( AV22Feriado_Nome3), StringUtil.RTrim( context.localUtil.Format( AV22Feriado_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFeriados.htm");
            wb_table9_72_IA2( true) ;
         }
         else
         {
            wb_table9_72_IA2( false) ;
         }
         return  ;
      }

      protected void wb_table9_72_IA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_IA2e( true) ;
         }
         else
         {
            wb_table6_19_IA2e( false) ;
         }
      }

      protected void wb_table9_72_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data3_Internalname, tblTablemergeddynamicfiltersferiado_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data3_Internalname, context.localUtil.Format(AV34Feriado_Data3, "99/99/99"), context.localUtil.Format( AV34Feriado_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to3_Internalname, context.localUtil.Format(AV35Feriado_Data_To3, "99/99/99"), context.localUtil.Format( AV35Feriado_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_72_IA2e( true) ;
         }
         else
         {
            wb_table9_72_IA2e( false) ;
         }
      }

      protected void wb_table8_51_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data2_Internalname, tblTablemergeddynamicfiltersferiado_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data2_Internalname, context.localUtil.Format(AV31Feriado_Data2, "99/99/99"), context.localUtil.Format( AV31Feriado_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to2_Internalname, context.localUtil.Format(AV32Feriado_Data_To2, "99/99/99"), context.localUtil.Format( AV32Feriado_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_51_IA2e( true) ;
         }
         else
         {
            wb_table8_51_IA2e( false) ;
         }
      }

      protected void wb_table7_30_IA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data1_Internalname, tblTablemergeddynamicfiltersferiado_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data1_Internalname, context.localUtil.Format(AV27Feriado_Data1, "99/99/99"), context.localUtil.Format( AV27Feriado_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_92_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to1_Internalname, context.localUtil.Format(AV28Feriado_Data_To1, "99/99/99"), context.localUtil.Format( AV28Feriado_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_IA2e( true) ;
         }
         else
         {
            wb_table7_30_IA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutFeriado_Data = (DateTime)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFeriado_Data", context.localUtil.Format(AV7InOutFeriado_Data, "99/99/99"));
         AV8InOutFeriado_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFeriado_Nome", AV8InOutFeriado_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIA2( ) ;
         WSIA2( ) ;
         WEIA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823433481");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptferiados.js", "?202042823433481");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_922( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_92_idx;
         edtFeriado_Data_Internalname = "FERIADO_DATA_"+sGXsfl_92_idx;
         edtFeriado_Nome_Internalname = "FERIADO_NOME_"+sGXsfl_92_idx;
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO_"+sGXsfl_92_idx;
      }

      protected void SubsflControlProps_fel_922( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_92_fel_idx;
         edtFeriado_Data_Internalname = "FERIADO_DATA_"+sGXsfl_92_fel_idx;
         edtFeriado_Nome_Internalname = "FERIADO_NOME_"+sGXsfl_92_fel_idx;
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO_"+sGXsfl_92_fel_idx;
      }

      protected void sendrow_922( )
      {
         SubsflControlProps_922( ) ;
         WBIA0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_92_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_92_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_92_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 93,'',false,'',92)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV25Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV55Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV55Select_GXI : context.PathToRelativeUrl( AV25Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_92_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV25Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFeriado_Data_Internalname,context.localUtil.Format(A1175Feriado_Data, "99/99/99"),context.localUtil.Format( A1175Feriado_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFeriado_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFeriado_Nome_Internalname,StringUtil.RTrim( A1176Feriado_Nome),StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFeriado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkFeriado_Fixo_Internalname,StringUtil.BoolToStr( A1195Feriado_Fixo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_DATA"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, A1175Feriado_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_NOME"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_FIXO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, A1195Feriado_Fixo));
            GridContainer.AddRow(GridRow);
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         /* End function sendrow_922 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAno1_Internalname = "vANO1";
         edtavFeriado_nome1_Internalname = "vFERIADO_NOME1";
         edtavFeriado_data1_Internalname = "vFERIADO_DATA1";
         lblDynamicfiltersferiado_data_rangemiddletext1_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT1";
         edtavFeriado_data_to1_Internalname = "vFERIADO_DATA_TO1";
         tblTablemergeddynamicfiltersferiado_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAno2_Internalname = "vANO2";
         edtavFeriado_nome2_Internalname = "vFERIADO_NOME2";
         edtavFeriado_data2_Internalname = "vFERIADO_DATA2";
         lblDynamicfiltersferiado_data_rangemiddletext2_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT2";
         edtavFeriado_data_to2_Internalname = "vFERIADO_DATA_TO2";
         tblTablemergeddynamicfiltersferiado_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAno3_Internalname = "vANO3";
         edtavFeriado_nome3_Internalname = "vFERIADO_NOME3";
         edtavFeriado_data3_Internalname = "vFERIADO_DATA3";
         lblDynamicfiltersferiado_data_rangemiddletext3_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT3";
         edtavFeriado_data_to3_Internalname = "vFERIADO_DATA_TO3";
         tblTablemergeddynamicfiltersferiado_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtFeriado_Data_Internalname = "FERIADO_DATA";
         edtFeriado_Nome_Internalname = "FERIADO_NOME";
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfferiado_data_Internalname = "vTFFERIADO_DATA";
         edtavTfferiado_data_to_Internalname = "vTFFERIADO_DATA_TO";
         edtavDdo_feriado_dataauxdate_Internalname = "vDDO_FERIADO_DATAAUXDATE";
         edtavDdo_feriado_dataauxdateto_Internalname = "vDDO_FERIADO_DATAAUXDATETO";
         divDdo_feriado_dataauxdates_Internalname = "DDO_FERIADO_DATAAUXDATES";
         edtavTfferiado_nome_Internalname = "vTFFERIADO_NOME";
         edtavTfferiado_nome_sel_Internalname = "vTFFERIADO_NOME_SEL";
         edtavTfferiado_fixo_sel_Internalname = "vTFFERIADO_FIXO_SEL";
         Ddo_feriado_data_Internalname = "DDO_FERIADO_DATA";
         edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE";
         Ddo_feriado_nome_Internalname = "DDO_FERIADO_NOME";
         edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_feriado_fixo_Internalname = "DDO_FERIADO_FIXO";
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFeriado_Nome_Jsonclick = "";
         edtFeriado_Data_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavFeriado_data_to1_Jsonclick = "";
         edtavFeriado_data1_Jsonclick = "";
         edtavFeriado_data_to2_Jsonclick = "";
         edtavFeriado_data2_Jsonclick = "";
         edtavFeriado_data_to3_Jsonclick = "";
         edtavFeriado_data3_Jsonclick = "";
         edtavFeriado_nome3_Jsonclick = "";
         edtavAno3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavFeriado_nome2_Jsonclick = "";
         edtavAno2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavFeriado_nome1_Jsonclick = "";
         edtavAno1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkFeriado_Fixo_Titleformat = 0;
         edtFeriado_Nome_Titleformat = 0;
         edtFeriado_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfiltersferiado_data3_Visible = 1;
         edtavFeriado_nome3_Visible = 1;
         edtavAno3_Visible = 1;
         tblTablemergeddynamicfiltersferiado_data2_Visible = 1;
         edtavFeriado_nome2_Visible = 1;
         edtavAno2_Visible = 1;
         tblTablemergeddynamicfiltersferiado_data1_Visible = 1;
         edtavFeriado_nome1_Visible = 1;
         edtavAno1_Visible = 1;
         chkFeriado_Fixo.Title.Text = "Fixo";
         edtFeriado_Nome_Title = "Descri��o";
         edtFeriado_Data_Title = "Data";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkFeriado_Fixo.Caption = "";
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_feriado_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_feriado_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfferiado_fixo_sel_Jsonclick = "";
         edtavTfferiado_fixo_sel_Visible = 1;
         edtavTfferiado_nome_sel_Jsonclick = "";
         edtavTfferiado_nome_sel_Visible = 1;
         edtavTfferiado_nome_Jsonclick = "";
         edtavTfferiado_nome_Visible = 1;
         edtavDdo_feriado_dataauxdateto_Jsonclick = "";
         edtavDdo_feriado_dataauxdate_Jsonclick = "";
         edtavTfferiado_data_to_Jsonclick = "";
         edtavTfferiado_data_to_Visible = 1;
         edtavTfferiado_data_Jsonclick = "";
         edtavTfferiado_data_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_feriado_fixo_Searchbuttontext = "Pesquisar";
         Ddo_feriado_fixo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_feriado_fixo_Rangefilterto = "At�";
         Ddo_feriado_fixo_Rangefilterfrom = "Desde";
         Ddo_feriado_fixo_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_fixo_Loadingdata = "Carregando dados...";
         Ddo_feriado_fixo_Sortdsc = "Ordenar de Z � A";
         Ddo_feriado_fixo_Sortasc = "Ordenar de A � Z";
         Ddo_feriado_fixo_Datalistupdateminimumcharacters = 0;
         Ddo_feriado_fixo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_feriado_fixo_Datalisttype = "FixedValues";
         Ddo_feriado_fixo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_feriado_fixo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_feriado_fixo_Includefilter = Convert.ToBoolean( 0);
         Ddo_feriado_fixo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_feriado_fixo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_feriado_fixo_Titlecontrolidtoreplace = "";
         Ddo_feriado_fixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_fixo_Cls = "ColumnSettings";
         Ddo_feriado_fixo_Tooltip = "Op��es";
         Ddo_feriado_fixo_Caption = "";
         Ddo_feriado_nome_Searchbuttontext = "Pesquisar";
         Ddo_feriado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_feriado_nome_Rangefilterto = "At�";
         Ddo_feriado_nome_Rangefilterfrom = "Desde";
         Ddo_feriado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_nome_Loadingdata = "Carregando dados...";
         Ddo_feriado_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_feriado_nome_Sortasc = "Ordenar de A � Z";
         Ddo_feriado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_feriado_nome_Datalistproc = "GetPromptFeriadosFilterData";
         Ddo_feriado_nome_Datalistfixedvalues = "";
         Ddo_feriado_nome_Datalisttype = "Dynamic";
         Ddo_feriado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_feriado_nome_Filtertype = "Character";
         Ddo_feriado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Titlecontrolidtoreplace = "";
         Ddo_feriado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_nome_Cls = "ColumnSettings";
         Ddo_feriado_nome_Tooltip = "Op��es";
         Ddo_feriado_nome_Caption = "";
         Ddo_feriado_data_Searchbuttontext = "Pesquisar";
         Ddo_feriado_data_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_feriado_data_Rangefilterto = "At�";
         Ddo_feriado_data_Rangefilterfrom = "Desde";
         Ddo_feriado_data_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_data_Loadingdata = "Carregando dados...";
         Ddo_feriado_data_Sortdsc = "Ordenar de Z � A";
         Ddo_feriado_data_Sortasc = "Ordenar de A � Z";
         Ddo_feriado_data_Datalistupdateminimumcharacters = 0;
         Ddo_feriado_data_Datalistfixedvalues = "";
         Ddo_feriado_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_feriado_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_feriado_data_Filtertype = "Date";
         Ddo_feriado_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_feriado_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_feriado_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_feriado_data_Titlecontrolidtoreplace = "";
         Ddo_feriado_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_data_Cls = "ColumnSettings";
         Ddo_feriado_data_Tooltip = "Op��es";
         Ddo_feriado_data_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Feriados";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''}],oparms:[{av:'AV36Feriado_DataTitleFilterData',fld:'vFERIADO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV42Feriado_NomeTitleFilterData',fld:'vFERIADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV46Feriado_FixoTitleFilterData',fld:'vFERIADO_FIXOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFeriado_Data_Titleformat',ctrl:'FERIADO_DATA',prop:'Titleformat'},{av:'edtFeriado_Data_Title',ctrl:'FERIADO_DATA',prop:'Title'},{av:'edtFeriado_Nome_Titleformat',ctrl:'FERIADO_NOME',prop:'Titleformat'},{av:'edtFeriado_Nome_Title',ctrl:'FERIADO_NOME',prop:'Title'},{av:'chkFeriado_Fixo_Titleformat',ctrl:'FERIADO_FIXO',prop:'Titleformat'},{av:'chkFeriado_Fixo.Title.Text',ctrl:'FERIADO_FIXO',prop:'Title'},{av:'AV51GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV52GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FERIADO_DATA.ONOPTIONCLICKED","{handler:'E12IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_feriado_data_Activeeventkey',ctrl:'DDO_FERIADO_DATA',prop:'ActiveEventKey'},{av:'Ddo_feriado_data_Filteredtext_get',ctrl:'DDO_FERIADO_DATA',prop:'FilteredText_get'},{av:'Ddo_feriado_data_Filteredtextto_get',ctrl:'DDO_FERIADO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_feriado_data_Sortedstatus',ctrl:'DDO_FERIADO_DATA',prop:'SortedStatus'},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'Ddo_feriado_nome_Sortedstatus',ctrl:'DDO_FERIADO_NOME',prop:'SortedStatus'},{av:'Ddo_feriado_fixo_Sortedstatus',ctrl:'DDO_FERIADO_FIXO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FERIADO_NOME.ONOPTIONCLICKED","{handler:'E13IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_feriado_nome_Activeeventkey',ctrl:'DDO_FERIADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_feriado_nome_Filteredtext_get',ctrl:'DDO_FERIADO_NOME',prop:'FilteredText_get'},{av:'Ddo_feriado_nome_Selectedvalue_get',ctrl:'DDO_FERIADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_feriado_nome_Sortedstatus',ctrl:'DDO_FERIADO_NOME',prop:'SortedStatus'},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_feriado_data_Sortedstatus',ctrl:'DDO_FERIADO_DATA',prop:'SortedStatus'},{av:'Ddo_feriado_fixo_Sortedstatus',ctrl:'DDO_FERIADO_FIXO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FERIADO_FIXO.ONOPTIONCLICKED","{handler:'E14IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_feriado_fixo_Activeeventkey',ctrl:'DDO_FERIADO_FIXO',prop:'ActiveEventKey'},{av:'Ddo_feriado_fixo_Selectedvalue_get',ctrl:'DDO_FERIADO_FIXO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_feriado_fixo_Sortedstatus',ctrl:'DDO_FERIADO_FIXO',prop:'SortedStatus'},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'Ddo_feriado_data_Sortedstatus',ctrl:'DDO_FERIADO_DATA',prop:'SortedStatus'},{av:'Ddo_feriado_nome_Sortedstatus',ctrl:'DDO_FERIADO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27IA2',iparms:[],oparms:[{av:'AV25Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E28IA2',iparms:[{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'A1176Feriado_Nome',fld:'FERIADO_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutFeriado_Data',fld:'vINOUTFERIADO_DATA',pic:'',nv:''},{av:'AV8InOutFeriado_Nome',fld:'vINOUTFERIADO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20IA2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21IA2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22IA2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23IA2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24IA2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19IA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV41ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV37TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'Ddo_feriado_data_Filteredtext_set',ctrl:'DDO_FERIADO_DATA',prop:'FilteredText_set'},{av:'AV38TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'Ddo_feriado_data_Filteredtextto_set',ctrl:'DDO_FERIADO_DATA',prop:'FilteredTextTo_set'},{av:'AV43TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'Ddo_feriado_nome_Filteredtext_set',ctrl:'DDO_FERIADO_NOME',prop:'FilteredText_set'},{av:'AV44TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_feriado_nome_Selectedvalue_set',ctrl:'DDO_FERIADO_NOME',prop:'SelectedValue_set'},{av:'AV47TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'Ddo_feriado_fixo_Selectedvalue_set',ctrl:'DDO_FERIADO_FIXO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV29Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV27Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV28Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV31Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV32Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV34Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV35Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7InOutFeriado_Data = DateTime.MinValue;
         wcpOAV8InOutFeriado_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_feriado_data_Activeeventkey = "";
         Ddo_feriado_data_Filteredtext_get = "";
         Ddo_feriado_data_Filteredtextto_get = "";
         Ddo_feriado_nome_Activeeventkey = "";
         Ddo_feriado_nome_Filteredtext_get = "";
         Ddo_feriado_nome_Selectedvalue_get = "";
         Ddo_feriado_fixo_Activeeventkey = "";
         Ddo_feriado_fixo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Feriado_Nome1 = "";
         AV27Feriado_Data1 = DateTime.MinValue;
         AV28Feriado_Data_To1 = DateTime.MinValue;
         AV18DynamicFiltersSelector2 = "";
         AV19Feriado_Nome2 = "";
         AV31Feriado_Data2 = DateTime.MinValue;
         AV32Feriado_Data_To2 = DateTime.MinValue;
         AV21DynamicFiltersSelector3 = "";
         AV22Feriado_Nome3 = "";
         AV34Feriado_Data3 = DateTime.MinValue;
         AV35Feriado_Data_To3 = DateTime.MinValue;
         AV37TFFeriado_Data = DateTime.MinValue;
         AV38TFFeriado_Data_To = DateTime.MinValue;
         AV43TFFeriado_Nome = "";
         AV44TFFeriado_Nome_Sel = "";
         AV41ddo_Feriado_DataTitleControlIdToReplace = "";
         AV45ddo_Feriado_NomeTitleControlIdToReplace = "";
         AV48ddo_Feriado_FixoTitleControlIdToReplace = "";
         AV56Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV49DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36Feriado_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Feriado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Feriado_FixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_feriado_data_Filteredtext_set = "";
         Ddo_feriado_data_Filteredtextto_set = "";
         Ddo_feriado_data_Sortedstatus = "";
         Ddo_feriado_nome_Filteredtext_set = "";
         Ddo_feriado_nome_Selectedvalue_set = "";
         Ddo_feriado_nome_Sortedstatus = "";
         Ddo_feriado_fixo_Selectedvalue_set = "";
         Ddo_feriado_fixo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV39DDO_Feriado_DataAuxDate = DateTime.MinValue;
         AV40DDO_Feriado_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Select = "";
         AV55Select_GXI = "";
         A1175Feriado_Data = DateTime.MinValue;
         A1176Feriado_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16Feriado_Nome1 = "";
         lV19Feriado_Nome2 = "";
         lV22Feriado_Nome3 = "";
         lV43TFFeriado_Nome = "";
         H00IA2_A1195Feriado_Fixo = new bool[] {false} ;
         H00IA2_n1195Feriado_Fixo = new bool[] {false} ;
         H00IA2_A1176Feriado_Nome = new String[] {""} ;
         H00IA2_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         H00IA3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptferiados__default(),
            new Object[][] {
                new Object[] {
               H00IA2_A1195Feriado_Fixo, H00IA2_n1195Feriado_Fixo, H00IA2_A1176Feriado_Nome, H00IA2_A1175Feriado_Data
               }
               , new Object[] {
               H00IA3_AGRID_nRecordCount
               }
            }
         );
         AV56Pgmname = "PromptFeriados";
         /* GeneXus formulas. */
         AV56Pgmname = "PromptFeriados";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_92 ;
      private short nGXsfl_92_idx=1 ;
      private short AV13OrderedBy ;
      private short AV29Ano1 ;
      private short AV30Ano2 ;
      private short AV33Ano3 ;
      private short AV47TFFeriado_Fixo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_92_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFeriado_Data_Titleformat ;
      private short edtFeriado_Nome_Titleformat ;
      private short chkFeriado_Fixo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_feriado_data_Datalistupdateminimumcharacters ;
      private int Ddo_feriado_nome_Datalistupdateminimumcharacters ;
      private int Ddo_feriado_fixo_Datalistupdateminimumcharacters ;
      private int edtavTfferiado_data_Visible ;
      private int edtavTfferiado_data_to_Visible ;
      private int edtavTfferiado_nome_Visible ;
      private int edtavTfferiado_nome_sel_Visible ;
      private int edtavTfferiado_fixo_sel_Visible ;
      private int edtavDdo_feriado_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_feriado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV50PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAno1_Visible ;
      private int edtavFeriado_nome1_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data1_Visible ;
      private int edtavAno2_Visible ;
      private int edtavFeriado_nome2_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data2_Visible ;
      private int edtavAno3_Visible ;
      private int edtavFeriado_nome3_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV51GridCurrentPage ;
      private long AV52GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutFeriado_Nome ;
      private String wcpOAV8InOutFeriado_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_feriado_data_Activeeventkey ;
      private String Ddo_feriado_data_Filteredtext_get ;
      private String Ddo_feriado_data_Filteredtextto_get ;
      private String Ddo_feriado_nome_Activeeventkey ;
      private String Ddo_feriado_nome_Filteredtext_get ;
      private String Ddo_feriado_nome_Selectedvalue_get ;
      private String Ddo_feriado_fixo_Activeeventkey ;
      private String Ddo_feriado_fixo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_92_idx="0001" ;
      private String AV16Feriado_Nome1 ;
      private String AV19Feriado_Nome2 ;
      private String AV22Feriado_Nome3 ;
      private String AV43TFFeriado_Nome ;
      private String AV44TFFeriado_Nome_Sel ;
      private String AV56Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_feriado_data_Caption ;
      private String Ddo_feriado_data_Tooltip ;
      private String Ddo_feriado_data_Cls ;
      private String Ddo_feriado_data_Filteredtext_set ;
      private String Ddo_feriado_data_Filteredtextto_set ;
      private String Ddo_feriado_data_Dropdownoptionstype ;
      private String Ddo_feriado_data_Titlecontrolidtoreplace ;
      private String Ddo_feriado_data_Sortedstatus ;
      private String Ddo_feriado_data_Filtertype ;
      private String Ddo_feriado_data_Datalistfixedvalues ;
      private String Ddo_feriado_data_Sortasc ;
      private String Ddo_feriado_data_Sortdsc ;
      private String Ddo_feriado_data_Loadingdata ;
      private String Ddo_feriado_data_Cleanfilter ;
      private String Ddo_feriado_data_Rangefilterfrom ;
      private String Ddo_feriado_data_Rangefilterto ;
      private String Ddo_feriado_data_Noresultsfound ;
      private String Ddo_feriado_data_Searchbuttontext ;
      private String Ddo_feriado_nome_Caption ;
      private String Ddo_feriado_nome_Tooltip ;
      private String Ddo_feriado_nome_Cls ;
      private String Ddo_feriado_nome_Filteredtext_set ;
      private String Ddo_feriado_nome_Selectedvalue_set ;
      private String Ddo_feriado_nome_Dropdownoptionstype ;
      private String Ddo_feriado_nome_Titlecontrolidtoreplace ;
      private String Ddo_feriado_nome_Sortedstatus ;
      private String Ddo_feriado_nome_Filtertype ;
      private String Ddo_feriado_nome_Datalisttype ;
      private String Ddo_feriado_nome_Datalistfixedvalues ;
      private String Ddo_feriado_nome_Datalistproc ;
      private String Ddo_feriado_nome_Sortasc ;
      private String Ddo_feriado_nome_Sortdsc ;
      private String Ddo_feriado_nome_Loadingdata ;
      private String Ddo_feriado_nome_Cleanfilter ;
      private String Ddo_feriado_nome_Rangefilterfrom ;
      private String Ddo_feriado_nome_Rangefilterto ;
      private String Ddo_feriado_nome_Noresultsfound ;
      private String Ddo_feriado_nome_Searchbuttontext ;
      private String Ddo_feriado_fixo_Caption ;
      private String Ddo_feriado_fixo_Tooltip ;
      private String Ddo_feriado_fixo_Cls ;
      private String Ddo_feriado_fixo_Selectedvalue_set ;
      private String Ddo_feriado_fixo_Dropdownoptionstype ;
      private String Ddo_feriado_fixo_Titlecontrolidtoreplace ;
      private String Ddo_feriado_fixo_Sortedstatus ;
      private String Ddo_feriado_fixo_Datalisttype ;
      private String Ddo_feriado_fixo_Datalistfixedvalues ;
      private String Ddo_feriado_fixo_Sortasc ;
      private String Ddo_feriado_fixo_Sortdsc ;
      private String Ddo_feriado_fixo_Loadingdata ;
      private String Ddo_feriado_fixo_Cleanfilter ;
      private String Ddo_feriado_fixo_Rangefilterfrom ;
      private String Ddo_feriado_fixo_Rangefilterto ;
      private String Ddo_feriado_fixo_Noresultsfound ;
      private String Ddo_feriado_fixo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfferiado_data_Internalname ;
      private String edtavTfferiado_data_Jsonclick ;
      private String edtavTfferiado_data_to_Internalname ;
      private String edtavTfferiado_data_to_Jsonclick ;
      private String divDdo_feriado_dataauxdates_Internalname ;
      private String edtavDdo_feriado_dataauxdate_Internalname ;
      private String edtavDdo_feriado_dataauxdate_Jsonclick ;
      private String edtavDdo_feriado_dataauxdateto_Internalname ;
      private String edtavDdo_feriado_dataauxdateto_Jsonclick ;
      private String edtavTfferiado_nome_Internalname ;
      private String edtavTfferiado_nome_Jsonclick ;
      private String edtavTfferiado_nome_sel_Internalname ;
      private String edtavTfferiado_nome_sel_Jsonclick ;
      private String edtavTfferiado_fixo_sel_Internalname ;
      private String edtavTfferiado_fixo_sel_Jsonclick ;
      private String edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtFeriado_Data_Internalname ;
      private String A1176Feriado_Nome ;
      private String edtFeriado_Nome_Internalname ;
      private String chkFeriado_Fixo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV16Feriado_Nome1 ;
      private String lV19Feriado_Nome2 ;
      private String lV22Feriado_Nome3 ;
      private String lV43TFFeriado_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAno1_Internalname ;
      private String edtavFeriado_nome1_Internalname ;
      private String edtavFeriado_data1_Internalname ;
      private String edtavFeriado_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAno2_Internalname ;
      private String edtavFeriado_nome2_Internalname ;
      private String edtavFeriado_data2_Internalname ;
      private String edtavFeriado_data_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAno3_Internalname ;
      private String edtavFeriado_nome3_Internalname ;
      private String edtavFeriado_data3_Internalname ;
      private String edtavFeriado_data_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_feriado_data_Internalname ;
      private String Ddo_feriado_nome_Internalname ;
      private String Ddo_feriado_fixo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFeriado_Data_Title ;
      private String edtFeriado_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfiltersferiado_data1_Internalname ;
      private String tblTablemergeddynamicfiltersferiado_data2_Internalname ;
      private String tblTablemergeddynamicfiltersferiado_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavAno1_Jsonclick ;
      private String edtavFeriado_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavAno2_Jsonclick ;
      private String edtavFeriado_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAno3_Jsonclick ;
      private String edtavFeriado_nome3_Jsonclick ;
      private String edtavFeriado_data3_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick ;
      private String edtavFeriado_data_to3_Jsonclick ;
      private String edtavFeriado_data2_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick ;
      private String edtavFeriado_data_to2_Jsonclick ;
      private String edtavFeriado_data1_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick ;
      private String edtavFeriado_data_to1_Jsonclick ;
      private String sGXsfl_92_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtFeriado_Data_Jsonclick ;
      private String edtFeriado_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV7InOutFeriado_Data ;
      private DateTime wcpOAV7InOutFeriado_Data ;
      private DateTime AV27Feriado_Data1 ;
      private DateTime AV28Feriado_Data_To1 ;
      private DateTime AV31Feriado_Data2 ;
      private DateTime AV32Feriado_Data_To2 ;
      private DateTime AV34Feriado_Data3 ;
      private DateTime AV35Feriado_Data_To3 ;
      private DateTime AV37TFFeriado_Data ;
      private DateTime AV38TFFeriado_Data_To ;
      private DateTime AV39DDO_Feriado_DataAuxDate ;
      private DateTime AV40DDO_Feriado_DataAuxDateTo ;
      private DateTime A1175Feriado_Data ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_feriado_data_Includesortasc ;
      private bool Ddo_feriado_data_Includesortdsc ;
      private bool Ddo_feriado_data_Includefilter ;
      private bool Ddo_feriado_data_Filterisrange ;
      private bool Ddo_feriado_data_Includedatalist ;
      private bool Ddo_feriado_nome_Includesortasc ;
      private bool Ddo_feriado_nome_Includesortdsc ;
      private bool Ddo_feriado_nome_Includefilter ;
      private bool Ddo_feriado_nome_Filterisrange ;
      private bool Ddo_feriado_nome_Includedatalist ;
      private bool Ddo_feriado_fixo_Includesortasc ;
      private bool Ddo_feriado_fixo_Includesortdsc ;
      private bool Ddo_feriado_fixo_Includefilter ;
      private bool Ddo_feriado_fixo_Filterisrange ;
      private bool Ddo_feriado_fixo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1195Feriado_Fixo ;
      private bool n1195Feriado_Fixo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV41ddo_Feriado_DataTitleControlIdToReplace ;
      private String AV45ddo_Feriado_NomeTitleControlIdToReplace ;
      private String AV48ddo_Feriado_FixoTitleControlIdToReplace ;
      private String AV55Select_GXI ;
      private String AV25Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private DateTime aP0_InOutFeriado_Data ;
      private String aP1_InOutFeriado_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkFeriado_Fixo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00IA2_A1195Feriado_Fixo ;
      private bool[] H00IA2_n1195Feriado_Fixo ;
      private String[] H00IA2_A1176Feriado_Nome ;
      private DateTime[] H00IA2_A1175Feriado_Data ;
      private long[] H00IA3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36Feriado_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Feriado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Feriado_FixoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV49DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptferiados__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IA2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV29Ano1 ,
                                             String AV16Feriado_Nome1 ,
                                             DateTime AV27Feriado_Data1 ,
                                             DateTime AV28Feriado_Data_To1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             short AV30Ano2 ,
                                             String AV19Feriado_Nome2 ,
                                             DateTime AV31Feriado_Data2 ,
                                             DateTime AV32Feriado_Data_To2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             short AV33Ano3 ,
                                             String AV22Feriado_Nome3 ,
                                             DateTime AV34Feriado_Data3 ,
                                             DateTime AV35Feriado_Data_To3 ,
                                             DateTime AV37TFFeriado_Data ,
                                             DateTime AV38TFFeriado_Data_To ,
                                             String AV44TFFeriado_Nome_Sel ,
                                             String AV43TFFeriado_Nome ,
                                             short AV47TFFeriado_Fixo_Sel ,
                                             DateTime A1175Feriado_Data ,
                                             String A1176Feriado_Nome ,
                                             bool A1195Feriado_Fixo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Feriado_Fixo], [Feriado_Nome], [Feriado_Data]";
         sFromString = " FROM [Feriados] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 ) && ( ! (0==AV29Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV29Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV29Ano1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Feriado_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV16Feriado_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV16Feriado_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV27Feriado_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV27Feriado_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV27Feriado_Data1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV28Feriado_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV28Feriado_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV28Feriado_Data_To1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 ) && ( ! (0==AV30Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV30Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV30Ano2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Feriado_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV19Feriado_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV19Feriado_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV31Feriado_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV31Feriado_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV31Feriado_Data2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV32Feriado_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV32Feriado_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV32Feriado_Data_To2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 ) && ( ! (0==AV33Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV33Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV33Ano3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Feriado_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV22Feriado_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV22Feriado_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV34Feriado_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV34Feriado_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV34Feriado_Data3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV35Feriado_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV35Feriado_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV35Feriado_Data_To3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV37TFFeriado_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV37TFFeriado_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV37TFFeriado_Data)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV38TFFeriado_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV38TFFeriado_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV38TFFeriado_Data_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFeriado_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFeriado_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like @lV43TFFeriado_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like @lV43TFFeriado_Nome)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFeriado_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] = @AV44TFFeriado_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] = @AV44TFFeriado_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV47TFFeriado_Fixo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 1)";
            }
         }
         if ( AV47TFFeriado_Fixo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Fixo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Fixo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IA3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV29Ano1 ,
                                             String AV16Feriado_Nome1 ,
                                             DateTime AV27Feriado_Data1 ,
                                             DateTime AV28Feriado_Data_To1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             short AV30Ano2 ,
                                             String AV19Feriado_Nome2 ,
                                             DateTime AV31Feriado_Data2 ,
                                             DateTime AV32Feriado_Data_To2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             short AV33Ano3 ,
                                             String AV22Feriado_Nome3 ,
                                             DateTime AV34Feriado_Data3 ,
                                             DateTime AV35Feriado_Data_To3 ,
                                             DateTime AV37TFFeriado_Data ,
                                             DateTime AV38TFFeriado_Data_To ,
                                             String AV44TFFeriado_Nome_Sel ,
                                             String AV43TFFeriado_Nome ,
                                             short AV47TFFeriado_Fixo_Sel ,
                                             DateTime A1175Feriado_Data ,
                                             String A1176Feriado_Nome ,
                                             bool A1195Feriado_Fixo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Feriados] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 ) && ( ! (0==AV29Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV29Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV29Ano1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Feriado_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV16Feriado_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV16Feriado_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV27Feriado_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV27Feriado_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV27Feriado_Data1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV28Feriado_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV28Feriado_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV28Feriado_Data_To1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 ) && ( ! (0==AV30Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV30Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV30Ano2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Feriado_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV19Feriado_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV19Feriado_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV31Feriado_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV31Feriado_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV31Feriado_Data2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV32Feriado_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV32Feriado_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV32Feriado_Data_To2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 ) && ( ! (0==AV33Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV33Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV33Ano3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Feriado_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV22Feriado_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV22Feriado_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV34Feriado_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV34Feriado_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV34Feriado_Data3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV35Feriado_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV35Feriado_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV35Feriado_Data_To3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV37TFFeriado_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV37TFFeriado_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV37TFFeriado_Data)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV38TFFeriado_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV38TFFeriado_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV38TFFeriado_Data_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFeriado_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFeriado_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like @lV43TFFeriado_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like @lV43TFFeriado_Nome)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFeriado_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] = @AV44TFFeriado_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] = @AV44TFFeriado_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV47TFFeriado_Fixo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 1)";
            }
         }
         if ( AV47TFFeriado_Fixo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IA2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] );
               case 1 :
                     return conditional_H00IA3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IA2 ;
          prmH00IA2 = new Object[] {
          new Object[] {"@AV29Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16Feriado_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV27Feriado_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28Feriado_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV19Feriado_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV31Feriado_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32Feriado_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22Feriado_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34Feriado_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35Feriado_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37TFFeriado_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV38TFFeriado_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43TFFeriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFFeriado_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IA3 ;
          prmH00IA3 = new Object[] {
          new Object[] {"@AV29Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16Feriado_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV27Feriado_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28Feriado_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV19Feriado_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV31Feriado_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32Feriado_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22Feriado_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34Feriado_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35Feriado_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37TFFeriado_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV38TFFeriado_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43TFFeriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFFeriado_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IA2,11,0,true,false )
             ,new CursorDef("H00IA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IA3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
