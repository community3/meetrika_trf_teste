/*
               File: GAMExampleEntryRepository
        Description: Repository
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:12:1.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentryrepository : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentryrepository( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentryrepository( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref int aP1_Id )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV19Id = aP1_Id;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Id=this.AV19Id;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavAllowoauthaccess = new GXCheckbox();
         chkavCanregisterusers = new GXCheckbox();
         chkavGiveanonymoussession = new GXCheckbox();
         chkavUpdateconnectionfile = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV19Id = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Id), 9, 0)));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA292( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START292( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282312457");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentryrepository.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV19Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Id), 9, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE292( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT292( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentryrepository.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV19Id) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntryRepository" ;
      }

      public override String GetPgmdesc( )
      {
         return "Repository " ;
      }

      protected void WB290( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_292( true) ;
         }
         else
         {
            wb_table1_3_292( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_292e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START292( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Repository ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP290( ) ;
      }

      protected void WS292( )
      {
         START292( ) ;
         EVT292( ) ;
      }

      protected void EVT292( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11292 */
                              E11292 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12292 */
                                    E12292 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CLOSE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13292 */
                              E13292 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14292 */
                              E14292 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE292( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA292( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            chkavAllowoauthaccess.Name = "vALLOWOAUTHACCESS";
            chkavAllowoauthaccess.WebTags = "";
            chkavAllowoauthaccess.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAllowoauthaccess_Internalname, "TitleCaption", chkavAllowoauthaccess.Caption);
            chkavAllowoauthaccess.CheckedValue = "false";
            chkavCanregisterusers.Name = "vCANREGISTERUSERS";
            chkavCanregisterusers.WebTags = "";
            chkavCanregisterusers.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCanregisterusers_Internalname, "TitleCaption", chkavCanregisterusers.Caption);
            chkavCanregisterusers.CheckedValue = "false";
            chkavGiveanonymoussession.Name = "vGIVEANONYMOUSSESSION";
            chkavGiveanonymoussession.WebTags = "";
            chkavGiveanonymoussession.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavGiveanonymoussession_Internalname, "TitleCaption", chkavGiveanonymoussession.Caption);
            chkavGiveanonymoussession.CheckedValue = "false";
            chkavUpdateconnectionfile.Name = "vUPDATECONNECTIONFILE";
            chkavUpdateconnectionfile.WebTags = "";
            chkavUpdateconnectionfile.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUpdateconnectionfile_Internalname, "TitleCaption", chkavUpdateconnectionfile.Caption);
            chkavUpdateconnectionfile.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGuid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF292( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF292( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14292 */
            E14292 ();
            WB290( ) ;
         }
      }

      protected void STRUP290( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11292 */
         E11292 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV18GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GUID", AV18GUID);
            AV21Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Name", AV21Name);
            AV22NameSpace = cgiGet( edtavNamespace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NameSpace", AV22NameSpace);
            AV11Description = cgiGet( edtavDescription_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Description", AV11Description);
            AV7AllowOauthAccess = StringUtil.StrToBool( cgiGet( chkavAllowoauthaccess_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AllowOauthAccess", AV7AllowOauthAccess);
            AV8CanRegisterUsers = StringUtil.StrToBool( cgiGet( chkavCanregisterusers_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8CanRegisterUsers", AV8CanRegisterUsers);
            AV17GiveAnonymousSession = StringUtil.StrToBool( cgiGet( chkavGiveanonymoussession_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GiveAnonymousSession", AV17GiveAnonymousSession);
            AV9ConnectionUserName = cgiGet( edtavConnectionusername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ConnectionUserName", AV9ConnectionUserName);
            AV10ConnectionUserPassword = cgiGet( edtavConnectionuserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ConnectionUserPassword", AV10ConnectionUserPassword);
            AV26ConfConnectionUserPassword = cgiGet( edtavConfconnectionuserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ConfConnectionUserPassword", AV26ConfConnectionUserPassword);
            AV5AdministratorUserName = cgiGet( edtavAdministratorusername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdministratorUserName", AV5AdministratorUserName);
            AV6AdministratorUserPassword = cgiGet( edtavAdministratoruserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AdministratorUserPassword", AV6AdministratorUserPassword);
            AV27ConfAdministratorUserPassword = cgiGet( edtavConfadministratoruserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ConfAdministratorUserPassword", AV27ConfAdministratorUserPassword);
            AV25UpdateConnectionFile = StringUtil.StrToBool( cgiGet( chkavUpdateconnectionfile_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UpdateConnectionFile", AV25UpdateConnectionFile);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11292 */
         E11292 ();
         if (returnInSub) return;
      }

      protected void E11292( )
      {
         /* Start Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV23Repository.load( AV19Id);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Id), 9, 0)));
            if ( AV23Repository.success() )
            {
               AV18GUID = AV23Repository.gxTpr_Guid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GUID", AV18GUID);
               AV21Name = AV23Repository.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Name", AV21Name);
               AV22NameSpace = AV23Repository.gxTpr_Namespace;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NameSpace", AV22NameSpace);
               AV11Description = AV23Repository.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Description", AV11Description);
               AV7AllowOauthAccess = AV23Repository.gxTpr_Allowoauthaccess;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AllowOauthAccess", AV7AllowOauthAccess);
               AV8CanRegisterUsers = AV23Repository.gxTpr_Canregisterusers;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8CanRegisterUsers", AV8CanRegisterUsers);
               AV17GiveAnonymousSession = AV23Repository.gxTpr_Giveanonymoussession;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GiveAnonymousSession", AV17GiveAnonymousSession);
               edtavGuid_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
               edtavName_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
               edtavNamespace_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNamespace_Enabled), 5, 0)));
               edtavDescription_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescription_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescription_Enabled), 5, 0)));
               edtavAdministratorusername_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAdministratorusername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAdministratorusername_Enabled), 5, 0)));
               edtavAdministratoruserpassword_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAdministratoruserpassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAdministratoruserpassword_Enabled), 5, 0)));
               chkavAllowoauthaccess.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAllowoauthaccess_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAllowoauthaccess.Enabled), 5, 0)));
               chkavCanregisterusers.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCanregisterusers_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCanregisterusers.Enabled), 5, 0)));
               edtavConnectionusername_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionusername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionusername_Enabled), 5, 0)));
               edtavConnectionuserpassword_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionuserpassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionuserpassword_Enabled), 5, 0)));
               chkavGiveanonymoussession.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavGiveanonymoussession_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavGiveanonymoussession.Enabled), 5, 0)));
               tblTbluserssettings_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbluserssettings_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbluserssettings_Visible), 5, 0)));
               lblTbupdconnfile_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbupdconnfile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbupdconnfile_Visible), 5, 0)));
               chkavUpdateconnectionfile.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUpdateconnectionfile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavUpdateconnectionfile.Visible), 5, 0)));
               bttBtnconfirm_Caption = "Delete";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
            }
            else
            {
               AV13Errors = AV23Repository.geterrors();
               /* Execute user subroutine: 'DISPLAYERRORS' */
               S112 ();
               if (returnInSub) return;
            }
         }
         else
         {
            edtavGuid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Visible), 5, 0)));
            lblTbguid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbguid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbguid_Visible), 5, 0)));
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12292 */
         E12292 ();
         if (returnInSub) return;
      }

      protected void E12292( )
      {
         /* Enter Routine */
         AV20isOK = true;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            if ( StringUtil.StrCmp(StringUtil.Trim( AV6AdministratorUserPassword), StringUtil.Trim( AV27ConfAdministratorUserPassword)) != 0 )
            {
               GX_msglist.addItem("The administrator password and confirmation do not match");
               AV20isOK = false;
            }
            if ( StringUtil.StrCmp(StringUtil.Trim( AV10ConnectionUserPassword), StringUtil.Trim( AV26ConfConnectionUserPassword)) != 0 )
            {
               GX_msglist.addItem("The connection password and confirmation do not match");
               AV20isOK = false;
            }
         }
         if ( AV20isOK )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               AV18GUID = Guid.NewGuid( ).ToString();
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GUID", AV18GUID);
               AV24RepositoryCreate.gxTpr_Guid = AV18GUID;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Name = AV21Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Namespace = AV22NameSpace;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Description = AV11Description;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Administratorusername = AV5AdministratorUserName;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Administratoruserpassword = AV6AdministratorUserPassword;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Allowoauthaccess = AV7AllowOauthAccess;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Canregisterusers = AV8CanRegisterUsers;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Connectionusername = AV9ConnectionUserName;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Connectionuserpassword = AV10ConnectionUserPassword;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV24RepositoryCreate.gxTpr_Giveanonymoussession = AV17GiveAnonymousSession;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24RepositoryCreate", AV24RepositoryCreate);
               AV20isOK = AV16GAM.createrepository(AV24RepositoryCreate, AV25UpdateConnectionFile, out  AV13Errors);
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               AV20isOK = new SdtGAM(context).deleterepository(AV23Repository.gxTpr_Guid, out  AV13Errors);
            }
         }
         if ( AV20isOK )
         {
            context.CommitDataStores( "GAMExampleEntryRepository");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(int)AV19Id});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'DISPLAYERRORS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E13292( )
      {
         /* 'Close' Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(int)AV19Id});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'DISPLAYERRORS' Routine */
         AV31GXV1 = 1;
         while ( AV31GXV1 <= AV13Errors.Count )
         {
            AV12Error = ((SdtGAMError)AV13Errors.Item(AV31GXV1));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV12Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV31GXV1 = (int)(AV31GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E14292( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_292( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(420), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid_Internalname, "GUID", "", "", lblTbguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbguid_Visible, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV18GUID), StringUtil.RTrim( context.localUtil.Format( AV18GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", edtavGuid_Visible, edtavGuid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV21Name), StringUtil.RTrim( context.localUtil.Format( AV21Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnamespace_Internalname, "Namespace", "", "", lblTbnamespace_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNamespace_Internalname, StringUtil.RTrim( AV22NameSpace), StringUtil.RTrim( context.localUtil.Format( AV22NameSpace, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNamespace_Jsonclick, 0, "Attribute", "", "", "", 1, edtavNamespace_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, 0, true, "GAMRepositoryNameSpace", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc_Internalname, "Description", "", "", lblTbdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDescription_Internalname, StringUtil.RTrim( AV11Description), "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtavDescription_Enabled, 1, 80, "chr", 4, "row", StyleString, ClassString, "", "254", -1, "", "", -1, true, "GAMDescriptionLong", "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowoauthaccess_Internalname, "Allow Oauth Access", "", "", lblTballowoauthaccess_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAllowoauthaccess_Internalname, StringUtil.BoolToStr( AV7AllowOauthAccess), "", "", 1, chkavAllowoauthaccess.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcanregisterusers_Internalname, "Can Register Users", "", "", lblTbcanregisterusers_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCanregisterusers_Internalname, StringUtil.BoolToStr( AV8CanRegisterUsers), "", "", 1, chkavCanregisterusers.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(33, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgiveanonymousesession_Internalname, "Give Anonymous Session", "", "", lblTbgiveanonymousesession_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "&nbsp; &nbsp;&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavGiveanonymoussession_Internalname, StringUtil.BoolToStr( AV17GiveAnonymousSession), "", "", 1, chkavGiveanonymoussession.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  style=\""+CSSHelper.Prettify( "height:1px")+"\">") ;
            wb_table2_41_292( true) ;
         }
         else
         {
            wb_table2_41_292( false) ;
         }
         return  ;
      }

      protected void wb_table2_41_292e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbupdconnfile_Internalname, "Update Connection File", "", "", lblTbupdconnfile_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbupdconnfile_Visible, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUpdateconnectionfile_Internalname, StringUtil.BoolToStr( AV25UpdateConnectionFile), "", "", chkavUpdateconnectionfile.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(82, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Close", bttBtnclose_Jsonclick, 5, "Close", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_292e( true) ;
         }
         else
         {
            wb_table1_3_292e( false) ;
         }
      }

      protected void wb_table2_41_292( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTbluserssettings_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTbluserssettings_Internalname, tblTbluserssettings_Internalname, "", "Table", 0, "", "", 2, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:29px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconnusername_Internalname, "Connection User Name", "", "", lblTbconnusername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConnectionusername_Internalname, StringUtil.RTrim( AV9ConnectionUserName), StringUtil.RTrim( context.localUtil.Format( AV9ConnectionUserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConnectionusername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavConnectionusername_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMConnectionUser", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:1px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconnuserpwd_Internalname, "Connection User Password", "", "", lblTbconnuserpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConnectionuserpassword_Internalname, StringUtil.RTrim( AV10ConnectionUserPassword), StringUtil.RTrim( context.localUtil.Format( AV10ConnectionUserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConnectionuserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavConnectionuserpassword_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, -1, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconfuserpwd_Internalname, "Confirm Password", "", "", lblTbconfuserpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConfconnectionuserpassword_Internalname, StringUtil.RTrim( AV26ConfConnectionUserPassword), StringUtil.RTrim( context.localUtil.Format( AV26ConfConnectionUserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConfconnectionuserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 254, -1, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:10px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbadminname_Internalname, "Administrator Name", "", "", lblTbadminname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAdministratorusername_Internalname, StringUtil.RTrim( AV5AdministratorUserName), StringUtil.RTrim( context.localUtil.Format( AV5AdministratorUserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAdministratorusername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavAdministratorusername_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMConnectionUser", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbadminpwd_Internalname, "Administrator Password", "", "", lblTbadminpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAdministratoruserpassword_Internalname, StringUtil.RTrim( AV6AdministratorUserPassword), StringUtil.RTrim( context.localUtil.Format( AV6AdministratorUserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAdministratoruserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavAdministratoruserpassword_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, -1, 0, 0, 1, 0, 0, true, "GAMPasswordDB", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconfadminpwd_Internalname, "Confirm Password", "", "", lblTbconfadminpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConfadministratoruserpassword_Internalname, StringUtil.RTrim( AV27ConfAdministratorUserPassword), StringUtil.RTrim( context.localUtil.Format( AV27ConfAdministratorUserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConfadministratoruserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 254, -1, 0, 0, 1, 0, 0, true, "GAMPasswordDB", "left", true, "HLP_GAMExampleEntryRepository.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_41_292e( true) ;
         }
         else
         {
            wb_table2_41_292e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV19Id = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Id), 9, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA292( ) ;
         WS292( ) ;
         WE292( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428231273");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentryrepository.js", "?2020428231274");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbguid_Internalname = "TBGUID";
         edtavGuid_Internalname = "vGUID";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbnamespace_Internalname = "TBNAMESPACE";
         edtavNamespace_Internalname = "vNAMESPACE";
         lblTbdsc_Internalname = "TBDSC";
         edtavDescription_Internalname = "vDESCRIPTION";
         lblTballowoauthaccess_Internalname = "TBALLOWOAUTHACCESS";
         chkavAllowoauthaccess_Internalname = "vALLOWOAUTHACCESS";
         lblTbcanregisterusers_Internalname = "TBCANREGISTERUSERS";
         chkavCanregisterusers_Internalname = "vCANREGISTERUSERS";
         lblTbgiveanonymousesession_Internalname = "TBGIVEANONYMOUSESESSION";
         chkavGiveanonymoussession_Internalname = "vGIVEANONYMOUSSESSION";
         lblTbconnusername_Internalname = "TBCONNUSERNAME";
         edtavConnectionusername_Internalname = "vCONNECTIONUSERNAME";
         lblTbconnuserpwd_Internalname = "TBCONNUSERPWD";
         edtavConnectionuserpassword_Internalname = "vCONNECTIONUSERPASSWORD";
         lblTbconfuserpwd_Internalname = "TBCONFUSERPWD";
         edtavConfconnectionuserpassword_Internalname = "vCONFCONNECTIONUSERPASSWORD";
         lblTbadminname_Internalname = "TBADMINNAME";
         edtavAdministratorusername_Internalname = "vADMINISTRATORUSERNAME";
         lblTbadminpwd_Internalname = "TBADMINPWD";
         edtavAdministratoruserpassword_Internalname = "vADMINISTRATORUSERPASSWORD";
         lblTbconfadminpwd_Internalname = "TBCONFADMINPWD";
         edtavConfadministratoruserpassword_Internalname = "vCONFADMINISTRATORUSERPASSWORD";
         tblTbluserssettings_Internalname = "TBLUSERSSETTINGS";
         lblTbupdconnfile_Internalname = "TBUPDCONNFILE";
         chkavUpdateconnectionfile_Internalname = "vUPDATECONNECTIONFILE";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavConfadministratoruserpassword_Jsonclick = "";
         edtavAdministratoruserpassword_Jsonclick = "";
         edtavAdministratorusername_Jsonclick = "";
         edtavConfconnectionuserpassword_Jsonclick = "";
         edtavConnectionuserpassword_Jsonclick = "";
         edtavConnectionusername_Jsonclick = "";
         lblTbupdconnfile_Visible = 1;
         edtavNamespace_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavGuid_Jsonclick = "";
         lblTbguid_Visible = 1;
         edtavGuid_Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         chkavUpdateconnectionfile.Visible = 1;
         tblTbluserssettings_Visible = 1;
         chkavGiveanonymoussession.Enabled = 1;
         edtavConnectionuserpassword_Enabled = 1;
         edtavConnectionusername_Enabled = 1;
         chkavCanregisterusers.Enabled = 1;
         chkavAllowoauthaccess.Enabled = 1;
         edtavAdministratoruserpassword_Enabled = 1;
         edtavAdministratorusername_Enabled = 1;
         edtavDescription_Enabled = 1;
         edtavNamespace_Enabled = 1;
         edtavName_Enabled = 1;
         edtavGuid_Enabled = 1;
         chkavUpdateconnectionfile.Caption = "";
         chkavGiveanonymoussession.Caption = "";
         chkavCanregisterusers.Caption = "";
         chkavAllowoauthaccess.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Repository ";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV18GUID = "";
         AV21Name = "";
         AV22NameSpace = "";
         AV11Description = "";
         AV9ConnectionUserName = "";
         AV10ConnectionUserPassword = "";
         AV26ConfConnectionUserPassword = "";
         AV5AdministratorUserName = "";
         AV6AdministratorUserPassword = "";
         AV27ConfAdministratorUserPassword = "";
         AV23Repository = new SdtGAMRepository(context);
         AV13Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV24RepositoryCreate = new SdtGAMRepositoryCreate(context);
         AV16GAM = new SdtGAM(context);
         AV12Error = new SdtGAMError(context);
         sStyleString = "";
         lblTbguid_Jsonclick = "";
         TempTags = "";
         lblTbname_Jsonclick = "";
         lblTbnamespace_Jsonclick = "";
         lblTbdsc_Jsonclick = "";
         lblTballowoauthaccess_Jsonclick = "";
         lblTbcanregisterusers_Jsonclick = "";
         lblTbgiveanonymousesession_Jsonclick = "";
         lblTbupdconnfile_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         lblTbconnusername_Jsonclick = "";
         lblTbconnuserpwd_Jsonclick = "";
         lblTbconfuserpwd_Jsonclick = "";
         lblTbadminname_Jsonclick = "";
         lblTbadminpwd_Jsonclick = "";
         lblTbconfadminpwd_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentryrepository__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV19Id ;
      private int wcpOAV19Id ;
      private int edtavGuid_Enabled ;
      private int edtavName_Enabled ;
      private int edtavNamespace_Enabled ;
      private int edtavDescription_Enabled ;
      private int edtavAdministratorusername_Enabled ;
      private int edtavAdministratoruserpassword_Enabled ;
      private int edtavConnectionusername_Enabled ;
      private int edtavConnectionuserpassword_Enabled ;
      private int tblTbluserssettings_Visible ;
      private int lblTbupdconnfile_Visible ;
      private int edtavGuid_Visible ;
      private int lblTbguid_Visible ;
      private int AV31GXV1 ;
      private int idxLst ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavAllowoauthaccess_Internalname ;
      private String chkavCanregisterusers_Internalname ;
      private String chkavGiveanonymoussession_Internalname ;
      private String chkavUpdateconnectionfile_Internalname ;
      private String edtavGuid_Internalname ;
      private String AV18GUID ;
      private String AV21Name ;
      private String edtavName_Internalname ;
      private String AV22NameSpace ;
      private String edtavNamespace_Internalname ;
      private String AV11Description ;
      private String edtavDescription_Internalname ;
      private String AV9ConnectionUserName ;
      private String edtavConnectionusername_Internalname ;
      private String AV10ConnectionUserPassword ;
      private String edtavConnectionuserpassword_Internalname ;
      private String AV26ConfConnectionUserPassword ;
      private String edtavConfconnectionuserpassword_Internalname ;
      private String AV5AdministratorUserName ;
      private String edtavAdministratorusername_Internalname ;
      private String AV6AdministratorUserPassword ;
      private String edtavAdministratoruserpassword_Internalname ;
      private String AV27ConfAdministratorUserPassword ;
      private String edtavConfadministratoruserpassword_Internalname ;
      private String tblTbluserssettings_Internalname ;
      private String lblTbupdconnfile_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String bttBtnconfirm_Internalname ;
      private String lblTbguid_Internalname ;
      private String sStyleString ;
      private String tblTblmain_Internalname ;
      private String lblTbguid_Jsonclick ;
      private String TempTags ;
      private String edtavGuid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbnamespace_Internalname ;
      private String lblTbnamespace_Jsonclick ;
      private String edtavNamespace_Jsonclick ;
      private String lblTbdsc_Internalname ;
      private String lblTbdsc_Jsonclick ;
      private String lblTballowoauthaccess_Internalname ;
      private String lblTballowoauthaccess_Jsonclick ;
      private String lblTbcanregisterusers_Internalname ;
      private String lblTbcanregisterusers_Jsonclick ;
      private String lblTbgiveanonymousesession_Internalname ;
      private String lblTbgiveanonymousesession_Jsonclick ;
      private String lblTbupdconnfile_Jsonclick ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private String lblTbconnusername_Internalname ;
      private String lblTbconnusername_Jsonclick ;
      private String edtavConnectionusername_Jsonclick ;
      private String lblTbconnuserpwd_Internalname ;
      private String lblTbconnuserpwd_Jsonclick ;
      private String edtavConnectionuserpassword_Jsonclick ;
      private String lblTbconfuserpwd_Internalname ;
      private String lblTbconfuserpwd_Jsonclick ;
      private String edtavConfconnectionuserpassword_Jsonclick ;
      private String lblTbadminname_Internalname ;
      private String lblTbadminname_Jsonclick ;
      private String edtavAdministratorusername_Jsonclick ;
      private String lblTbadminpwd_Internalname ;
      private String lblTbadminpwd_Jsonclick ;
      private String edtavAdministratoruserpassword_Jsonclick ;
      private String lblTbconfadminpwd_Internalname ;
      private String lblTbconfadminpwd_Jsonclick ;
      private String edtavConfadministratoruserpassword_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV7AllowOauthAccess ;
      private bool AV8CanRegisterUsers ;
      private bool AV17GiveAnonymousSession ;
      private bool AV25UpdateConnectionFile ;
      private bool returnInSub ;
      private bool AV20isOK ;
      private SdtGAM AV16GAM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private int aP1_Id ;
      private GXCheckbox chkavAllowoauthaccess ;
      private GXCheckbox chkavCanregisterusers ;
      private GXCheckbox chkavGiveanonymoussession ;
      private GXCheckbox chkavUpdateconnectionfile ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV13Errors ;
      private GXWebForm Form ;
      private SdtGAMError AV12Error ;
      private SdtGAMRepositoryCreate AV24RepositoryCreate ;
      private SdtGAMRepository AV23Repository ;
   }

   public class gamexampleentryrepository__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
