/*
               File: ViewContagemResultadoContagens
        Description: View Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:18:15.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewcontagemresultadocontagens : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_ContagemResultado_DataCnt ,
                           String aP2_ContagemResultado_HoraCnt ,
                           String aP3_TabCode )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV13ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         this.AV7TabCode = aP3_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_DataCnt", context.localUtil.Format(AV10ContagemResultado_DataCnt, "99/99/99"));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV10ContagemResultado_DataCnt));
                  AV13ContagemResultado_HoraCnt = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_HoraCnt", AV13ContagemResultado_HoraCnt);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_HoraCnt, ""))));
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAB12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTB12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216181519");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewcontagemresultadocontagens.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV10ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV13ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( AV10ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( AV13ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV10ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV10ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEB12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTB12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewcontagemresultadocontagens.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV10ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV13ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewContagemResultadoContagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contagem Resultado Contagens" ;
      }

      protected void WBB10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_B12( true) ;
         }
         else
         {
            wb_table1_2_B12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B12e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTB12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Contagem Resultado Contagens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPB10( ) ;
      }

      protected void WSB12( )
      {
         STARTB12( ) ;
         EVTB12( ) ;
      }

      protected void EVTB12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11B12 */
                              E11B12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12B12 */
                              E12B12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 20 )
                        {
                           OldTabbedview = cgiGet( "W0020");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0020", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEB12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAB12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFB12( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00B12 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A493ContagemResultado_DemandaFM = H00B12_A493ContagemResultado_DemandaFM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
               n493ContagemResultado_DemandaFM = H00B12_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00B12_A457ContagemResultado_Demanda[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
               n457ContagemResultado_Demanda = H00B12_n457ContagemResultado_Demanda[0];
               /* Execute user event: E12B12 */
               E12B12 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBB10( ) ;
         }
      }

      protected void STRUPB10( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11B12 */
         E11B12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
            n457ContagemResultado_Demanda = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
            A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
            n493ContagemResultado_DemandaFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11B12 */
         E11B12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11B12( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwcontagemresultadocontagens.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV16GXLvl9 = 0;
         /* Using cursor H00B13 */
         pr_default.execute(1, new Object[] {AV9ContagemResultado_Codigo, AV10ContagemResultado_DataCnt, AV13ContagemResultado_HoraCnt});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A511ContagemResultado_HoraCnt = H00B13_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = H00B13_A473ContagemResultado_DataCnt[0];
            A456ContagemResultado_Codigo = H00B13_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = H00B13_A457ContagemResultado_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
            n457ContagemResultado_Demanda = H00B13_n457ContagemResultado_Demanda[0];
            A457ContagemResultado_Demanda = H00B13_A457ContagemResultado_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
            n457ContagemResultado_Demanda = H00B13_n457ContagemResultado_Demanda[0];
            AV16GXLvl9 = 1;
            Form.Caption = A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV16GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0020",(String)"",(IGxCollection)AV11Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         lblWorkwithlink_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         lblWorkwithlink_Jsonclick = "window.history.back()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Jsonclick", lblWorkwithlink_Jsonclick);
      }

      protected void nextLoad( )
      {
      }

      protected void E12B12( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV11Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV12Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV12Tab.gxTpr_Code = "General";
         AV12Tab.gxTpr_Description = "Contagem";
         AV12Tab.gxTpr_Webcomponent = formatLink("contagemresultadocontagensgeneral.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV10ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV13ContagemResultado_HoraCnt));
         AV12Tab.gxTpr_Link = formatLink("viewcontagemresultadocontagens.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV10ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV13ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(AV12Tab.gxTpr_Code));
         AV12Tab.gxTpr_Includeinpanel = 0;
         AV12Tab.gxTpr_Collapsable = true;
         AV12Tab.gxTpr_Collapsedbydefault = false;
         AV11Tabs.Add(AV12Tab, 0);
      }

      protected void wb_table1_2_B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_B12( true) ;
         }
         else
         {
            wb_table2_5_B12( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_B12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0020"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0020"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0020"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B12e( true) ;
         }
         else
         {
            wb_table1_2_B12e( false) ;
         }
      }

      protected void wb_table2_5_B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Demanda FS/FM ::", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Demanda_Internalname, A457ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Demanda_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewbarra_Internalname, "/", "", "", lblViewbarra_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AttributeTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DemandaFM_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_B12e( true) ;
         }
         else
         {
            wb_table2_5_B12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         AV10ContagemResultado_DataCnt = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_DataCnt", context.localUtil.Format(AV10ContagemResultado_DataCnt, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV10ContagemResultado_DataCnt));
         AV13ContagemResultado_HoraCnt = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_HoraCnt", AV13ContagemResultado_HoraCnt);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_HoraCnt, ""))));
         AV7TabCode = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB12( ) ;
         WSB12( ) ;
         WEB12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216181543");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewcontagemresultadocontagens.js", "?20206216181544");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         lblViewbarra_Internalname = "VIEWBARRA";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Contagem Resultado Contagens";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV10ContagemResultado_DataCnt = DateTime.MinValue;
         wcpOAV13ContagemResultado_HoraCnt = "";
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A493ContagemResultado_DemandaFM = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00B12_A456ContagemResultado_Codigo = new int[1] ;
         H00B12_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00B12_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00B12_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B12_n457ContagemResultado_Demanda = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00B13_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00B13_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00B13_A456ContagemResultado_Codigo = new int[1] ;
         H00B13_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B13_n457ContagemResultado_Demanda = new bool[] {false} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV11Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         lblWorkwithlink_Jsonclick = "";
         AV12Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         sStyleString = "";
         lblViewtitle_Jsonclick = "";
         lblViewbarra_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewcontagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               H00B12_A456ContagemResultado_Codigo, H00B12_A493ContagemResultado_DemandaFM, H00B12_n493ContagemResultado_DemandaFM, H00B12_A457ContagemResultado_Demanda, H00B12_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00B13_A511ContagemResultado_HoraCnt, H00B13_A473ContagemResultado_DataCnt, H00B13_A456ContagemResultado_Codigo, H00B13_A457ContagemResultado_Demanda, H00B13_n457ContagemResultado_Demanda
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV16GXLvl9 ;
      private short nGXWrapped ;
      private int AV9ContagemResultado_Codigo ;
      private int wcpOAV9ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int idxLst ;
      private String AV13ContagemResultado_HoraCnt ;
      private String AV7TabCode ;
      private String wcpOAV13ContagemResultado_HoraCnt ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String A511ContagemResultado_HoraCnt ;
      private String lblWorkwithlink_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String lblViewbarra_Internalname ;
      private String lblViewbarra_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private DateTime AV10ContagemResultado_DataCnt ;
      private DateTime wcpOAV10ContagemResultado_DataCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00B12_A456ContagemResultado_Codigo ;
      private String[] H00B12_A493ContagemResultado_DemandaFM ;
      private bool[] H00B12_n493ContagemResultado_DemandaFM ;
      private String[] H00B12_A457ContagemResultado_Demanda ;
      private bool[] H00B12_n457ContagemResultado_Demanda ;
      private String[] H00B13_A511ContagemResultado_HoraCnt ;
      private DateTime[] H00B13_A473ContagemResultado_DataCnt ;
      private int[] H00B13_A456ContagemResultado_Codigo ;
      private String[] H00B13_A457ContagemResultado_Demanda ;
      private bool[] H00B13_n457ContagemResultado_Demanda ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV11Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV12Tab ;
   }

   public class viewcontagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B12 ;
          prmH00B12 = new Object[] {
          } ;
          Object[] prmH00B13 ;
          prmH00B13 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B12", "SELECT [ContagemResultado_Codigo], [ContagemResultado_DemandaFM], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B12,100,0,true,false )
             ,new CursorDef("H00B13", "SELECT T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_Demanda] FROM ([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo and T1.[ContagemResultado_DataCnt] = @AV10ContagemResultado_DataCnt and T1.[ContagemResultado_HoraCnt] = @AV13ContagemResultado_HoraCnt) AND (T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo and T1.[ContagemResultado_DataCnt] = @AV10ContagemResultado_DataCnt and T1.[ContagemResultado_HoraCnt] = @AV13ContagemResultado_HoraCnt) ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B13,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
