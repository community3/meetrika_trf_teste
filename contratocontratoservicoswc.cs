/*
               File: ContratoContratoServicosWC
        Description: Contrato Contrato Servicos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:40.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratocontratoservicoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratocontratoservicoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratocontratoservicoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV7Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavServico_tipohierarquia = new GXCombobox();
         cmbavServico_ativo = new GXCombobox();
         cmbServicoContrato_Faturamento = new GXCombobox();
         cmbContratoServicos_HmlSemCnf = new GXCombobox();
         chkContratoServicos_Ativo = new GXCheckbox();
         cmbServico_IsOrigemReferencia = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_21 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_21_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_21_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV68OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
                  AV21OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21OrderedDsc", AV21OrderedDsc);
                  AV53TFContratoServicos_PrazoAnalise = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0)));
                  AV54TFContratoServicos_PrazoAnalise_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoServicos_PrazoAnalise_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0)));
                  AV57TFContratoServicos_Indicadores = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0)));
                  AV58TFContratoServicos_Indicadores_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicos_Indicadores_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0)));
                  AV70TFContratoServicos_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFContratoServicos_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0));
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace", AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace);
                  AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace", AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace);
                  AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace", AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV97Pgmname = GetNextPar( );
                  AV24Servico_TipoHierarquia = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
                  AV13Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Ativo", AV13Servico_Ativo);
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A911ContratoServicos_Prazos = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n911ContratoServicos_Prazos = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A911ContratoServicos_Prazos", StringUtil.LTrim( StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0)));
                  A638ContratoServicos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  A558Servico_Percentual = NumberUtil.Val( GetNextPar( ), ".");
                  n558Servico_Percentual = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
                  A913ContratoServicos_PrazoTipo = GetNextPar( );
                  n913ContratoServicos_PrazoTipo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
                  A914ContratoServicos_PrazoDias = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n914ContratoServicos_PrazoDias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAIR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV97Pgmname = "ContratoContratoServicosWC";
               context.Gx_err = 0;
               edtavServico_percentual_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
               edtavPrazotipo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrazotipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazotipo_Enabled), 5, 0)));
               WSIR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Contrato Servicos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299304093");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratocontratoservicoswc.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV21OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOS_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_21", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_21), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOS_PRAZOANALISETITLEFILTERDATA", AV52ContratoServicos_PrazoAnaliseTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOS_PRAZOANALISETITLEFILTERDATA", AV52ContratoServicos_PrazoAnaliseTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOS_INDICADORESTITLEFILTERDATA", AV56ContratoServicos_IndicadoresTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOS_INDICADORESTITLEFILTERDATA", AV56ContratoServicos_IndicadoresTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOS_ATIVOTITLEFILTERDATA", AV69ContratoServicos_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOS_ATIVOTITLEFILTERDATA", AV69ContratoServicos_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV97Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_PRAZOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A911ContratoServicos_Prazos), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_PRAZOTIPO", StringUtil.RTrim( A913ContratoServicos_PrazoTipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A914ContratoServicos_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSRMN_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vTEMUSCCADASTRADAS", AV20TemUSCCadastradas);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Confirmationtext", StringUtil.RTrim( Confirmpanel2_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel2_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Nobuttoncaption", StringUtil.RTrim( Confirmpanel2_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL2_Yesbuttonposition", StringUtil.RTrim( Confirmpanel2_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Caption", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Tooltip", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Cls", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicos_prazoanalise_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicos_prazoanalise_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicos_prazoanalise_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filtertype", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicos_prazoanalise_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicos_prazoanalise_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Caption", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Tooltip", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Cls", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicos_indicadores_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicos_indicadores_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicos_indicadores_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filtertype", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicos_indicadores_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicos_indicadores_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Caption", StringUtil.RTrim( Ddo_contratoservicos_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contratoservicos_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Cls", StringUtil.RTrim( Ddo_contratoservicos_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicos_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicos_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicos_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicos_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicos_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicos_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicos_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicos_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicos_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicos_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contratoservicos_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicos_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicos_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicos_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicos_prazoanalise_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicos_indicadores_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicos_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicos_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIR2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratocontratoservicoswc.js", "?20205299304265");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoContratoServicosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Contrato Servicos WC" ;
      }

      protected void WBIR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratocontratoservicoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_IR2( true) ;
         }
         else
         {
            wb_table1_2_IR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV21OrderedDsc), StringUtil.BoolToStr( AV21OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_prazoanalise_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53TFContratoServicos_PrazoAnalise), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_prazoanalise_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_prazoanalise_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_prazoanalise_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_prazoanalise_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_prazoanalise_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_indicadores_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57TFContratoServicos_Indicadores), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_indicadores_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_indicadores_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_indicadores_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFContratoServicos_Indicadores_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_indicadores_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_indicadores_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV70TFContratoServicos_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoServicosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Internalname, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoServicosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOS_INDICADORESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Internalname, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoServicosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Internalname, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoServicosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTIR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Contrato Servicos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPIR0( ) ;
            }
         }
      }

      protected void WSIR2( )
      {
         STARTIR2( ) ;
         EVTIR2( ) ;
      }

      protected void EVTIR2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11IR2 */
                                    E11IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12IR2 */
                                    E12IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL2.ONYES") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13IR2 */
                                    E13IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOS_PRAZOANALISE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14IR2 */
                                    E14IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOS_INDICADORES.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15IR2 */
                                    E15IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOS_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16IR2 */
                                    E16IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17IR2 */
                                    E17IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLONAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18IR2 */
                                    E18IR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavServico_percentual_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VAPAGAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "'DOASSOCIARSISTEMA'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "GRID.ONLINEACTIVATE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VAPAGAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "'DOASSOCIARSISTEMA'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIR0( ) ;
                              }
                              nGXsfl_21_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
                              SubsflControlProps_212( ) ;
                              AV14Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV92Update_GXI : context.convertURL( context.PathToRelativeUrl( AV14Update))));
                              AV73Apagar = cgiGet( edtavApagar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavApagar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV73Apagar)) ? AV93Apagar_GXI : context.convertURL( context.PathToRelativeUrl( AV73Apagar))));
                              AV15Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)) ? AV94Display_GXI : context.convertURL( context.PathToRelativeUrl( AV15Display))));
                              A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
                              A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                              A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                              A1858ContratoServicos_Alias = StringUtil.Upper( cgiGet( edtContratoServicos_Alias_Internalname));
                              n1858ContratoServicos_Alias = false;
                              A557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".");
                              A1212ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_UnidadeContratada_Internalname), ",", "."));
                              A2132ContratoServicos_UndCntNome = StringUtil.Upper( cgiGet( edtContratoServicos_UndCntNome_Internalname));
                              n2132ContratoServicos_UndCntNome = false;
                              A1340ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".");
                              n1340ContratoServicos_QntUntCns = false;
                              A555Servico_QtdContratada = (long)(context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", "."));
                              n555Servico_QtdContratada = false;
                              cmbServicoContrato_Faturamento.Name = cmbServicoContrato_Faturamento_Internalname;
                              cmbServicoContrato_Faturamento.CurrentValue = cgiGet( cmbServicoContrato_Faturamento_Internalname);
                              A607ServicoContrato_Faturamento = cgiGet( cmbServicoContrato_Faturamento_Internalname);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") > 999.999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCENTUAL");
                                 GX_FocusControl = edtavServico_percentual_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV18Servico_Percentual = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.Str( AV18Servico_Percentual, 7, 3)));
                              }
                              else
                              {
                                 AV18Servico_Percentual = context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.Str( AV18Servico_Percentual, 7, 3)));
                              }
                              cmbContratoServicos_HmlSemCnf.Name = cmbContratoServicos_HmlSemCnf_Internalname;
                              cmbContratoServicos_HmlSemCnf.CurrentValue = cgiGet( cmbContratoServicos_HmlSemCnf_Internalname);
                              A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cgiGet( cmbContratoServicos_HmlSemCnf_Internalname));
                              n888ContratoServicos_HmlSemCnf = false;
                              A1152ContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", "."));
                              n1152ContratoServicos_PrazoAnalise = false;
                              AV19PrazoTipo = StringUtil.Upper( cgiGet( edtavPrazotipo_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrazotipo_Internalname, AV19PrazoTipo);
                              A1377ContratoServicos_Indicadores = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_Indicadores_Internalname), ",", "."));
                              n1377ContratoServicos_Indicadores = false;
                              A638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicos_Ativo_Internalname));
                              cmbServico_IsOrigemReferencia.Name = cmbServico_IsOrigemReferencia_Internalname;
                              cmbServico_IsOrigemReferencia.CurrentValue = cgiGet( cmbServico_IsOrigemReferencia_Internalname);
                              A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( cmbServico_IsOrigemReferencia_Internalname));
                              AV17AssociarSistema = cgiGet( edtavAssociarsistema_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17AssociarSistema)) ? AV95Associarsistema_GXI : context.convertURL( context.PathToRelativeUrl( AV17AssociarSistema))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19IR2 */
                                          E19IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20IR2 */
                                          E20IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21IR2 */
                                          E21IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VAPAGAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22IR2 */
                                          E22IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOASSOCIARSISTEMA'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23IR2 */
                                          E23IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.ONLINEACTIVATE") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24IR2 */
                                          E24IR2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV68OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV21OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicos_prazoanalise Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE"), ",", ".") != Convert.ToDecimal( AV53TFContratoServicos_PrazoAnalise )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicos_prazoanalise_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE_TO"), ",", ".") != Convert.ToDecimal( AV54TFContratoServicos_PrazoAnalise_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicos_indicadores Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES"), ",", ".") != Convert.ToDecimal( AV57TFContratoServicos_Indicadores )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicos_indicadores_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES_TO"), ",", ".") != Convert.ToDecimal( AV58TFContratoServicos_Indicadores_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicos_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV70TFContratoServicos_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPIR0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavServico_percentual_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIR2( ) ;
            }
         }
      }

      protected void PAIR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavServico_tipohierarquia.Name = "vSERVICO_TIPOHIERARQUIA";
            cmbavServico_tipohierarquia.WebTags = "";
            cmbavServico_tipohierarquia.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Todos", 0);
            cmbavServico_tipohierarquia.addItem("1", "Prim�rio", 0);
            cmbavServico_tipohierarquia.addItem("2", "Secund�rio", 0);
            if ( cmbavServico_tipohierarquia.ItemCount > 0 )
            {
               AV24Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbavServico_tipohierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
            }
            cmbavServico_ativo.Name = "vSERVICO_ATIVO";
            cmbavServico_ativo.WebTags = "";
            cmbavServico_ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbavServico_ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbavServico_ativo.ItemCount > 0 )
            {
               AV13Servico_Ativo = StringUtil.StrToBool( cmbavServico_ativo.getValidValue(StringUtil.BoolToStr( AV13Servico_Ativo)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Ativo", AV13Servico_Ativo);
            }
            GXCCtl = "SERVICOCONTRATO_FATURAMENTO_" + sGXsfl_21_idx;
            cmbServicoContrato_Faturamento.Name = GXCCtl;
            cmbServicoContrato_Faturamento.WebTags = "";
            cmbServicoContrato_Faturamento.addItem("B", "Bruto", 0);
            cmbServicoContrato_Faturamento.addItem("L", "Liquido", 0);
            if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
            {
               A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
            }
            GXCCtl = "CONTRATOSERVICOS_HMLSEMCNF_" + sGXsfl_21_idx;
            cmbContratoServicos_HmlSemCnf.Name = GXCCtl;
            cmbContratoServicos_HmlSemCnf.WebTags = "";
            cmbContratoServicos_HmlSemCnf.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoServicos_HmlSemCnf.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoServicos_HmlSemCnf.ItemCount > 0 )
            {
               A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cmbContratoServicos_HmlSemCnf.getValidValue(StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf)));
               n888ContratoServicos_HmlSemCnf = false;
            }
            GXCCtl = "CONTRATOSERVICOS_ATIVO_" + sGXsfl_21_idx;
            chkContratoServicos_Ativo.Name = GXCCtl;
            chkContratoServicos_Ativo.WebTags = "";
            chkContratoServicos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_Ativo_Internalname, "TitleCaption", chkContratoServicos_Ativo.Caption);
            chkContratoServicos_Ativo.CheckedValue = "false";
            GXCCtl = "SERVICO_ISORIGEMREFERENCIA_" + sGXsfl_21_idx;
            cmbServico_IsOrigemReferencia.Name = GXCCtl;
            cmbServico_IsOrigemReferencia.WebTags = "";
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
            {
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavServico_tipohierarquia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_212( ) ;
         while ( nGXsfl_21_idx <= nRC_GXsfl_21 )
         {
            sendrow_212( ) ;
            nGXsfl_21_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_21_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_21_idx+1));
            sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
            SubsflControlProps_212( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV68OrderedBy ,
                                       bool AV21OrderedDsc ,
                                       short AV53TFContratoServicos_PrazoAnalise ,
                                       short AV54TFContratoServicos_PrazoAnalise_To ,
                                       short AV57TFContratoServicos_Indicadores ,
                                       short AV58TFContratoServicos_Indicadores_To ,
                                       short AV70TFContratoServicos_Ativo_Sel ,
                                       int AV7Contrato_Codigo ,
                                       String AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace ,
                                       String AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace ,
                                       String AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV97Pgmname ,
                                       short AV24Servico_TipoHierarquia ,
                                       bool AV13Servico_Ativo ,
                                       int A160ContratoServicos_Codigo ,
                                       short A911ContratoServicos_Prazos ,
                                       bool A638ContratoServicos_Ativo ,
                                       decimal A558Servico_Percentual ,
                                       String A913ContratoServicos_PrazoTipo ,
                                       short A914ContratoServicos_PrazoDias ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ALIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_ALIAS", StringUtil.RTrim( A1858ContratoServicos_Alias));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VLRUNIDADECONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_VLRUNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_UNIDADECONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1212ContratoServicos_UnidadeContratada), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_QNTUNTCNS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_QNTUNTCNS", StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_QTDCONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_QTDCONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A555Servico_QtdContratada), 10, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOCONTRATO_FATURAMENTO", A607ServicoContrato_Faturamento);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_HMLSEMCNF", GetSecureSignedToken( sPrefix, A888ContratoServicos_HmlSemCnf));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_HMLSEMCNF", StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOANALISE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_PRAZOANALISE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ATIVO", GetSecureSignedToken( sPrefix, A638ContratoServicos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_ATIVO", StringUtil.BoolToStr( A638ContratoServicos_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavServico_tipohierarquia.ItemCount > 0 )
         {
            AV24Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbavServico_tipohierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
         }
         if ( cmbavServico_ativo.ItemCount > 0 )
         {
            AV13Servico_Ativo = StringUtil.StrToBool( cmbavServico_ativo.getValidValue(StringUtil.BoolToStr( AV13Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Ativo", AV13Servico_Ativo);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV97Pgmname = "ContratoContratoServicosWC";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
         edtavPrazotipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrazotipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazotipo_Enabled), 5, 0)));
      }

      protected void RFIR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 21;
         /* Execute user event: E20IR2 */
         E20IR2 ();
         nGXsfl_21_idx = 1;
         sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
         SubsflControlProps_212( ) ;
         nGXsfl_21_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_212( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV53TFContratoServicos_PrazoAnalise ,
                                                 AV54TFContratoServicos_PrazoAnalise_To ,
                                                 AV70TFContratoServicos_Ativo_Sel ,
                                                 A1152ContratoServicos_PrazoAnalise ,
                                                 A638ContratoServicos_Ativo ,
                                                 AV68OrderedBy ,
                                                 AV21OrderedDsc ,
                                                 A74Contrato_Codigo ,
                                                 AV7Contrato_Codigo ,
                                                 A1530Servico_TipoHierarquia ,
                                                 A632Servico_Ativo ,
                                                 AV57TFContratoServicos_Indicadores ,
                                                 A1377ContratoServicos_Indicadores ,
                                                 AV58TFContratoServicos_Indicadores_To },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                                 }
            });
            /* Using cursor H00IR4 */
            pr_default.execute(0, new Object[] {AV7Contrato_Codigo, AV57TFContratoServicos_Indicadores, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV58TFContratoServicos_Indicadores_To, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_21_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1530Servico_TipoHierarquia = H00IR4_A1530Servico_TipoHierarquia[0];
               n1530Servico_TipoHierarquia = H00IR4_n1530Servico_TipoHierarquia[0];
               A632Servico_Ativo = H00IR4_A632Servico_Ativo[0];
               A558Servico_Percentual = H00IR4_A558Servico_Percentual[0];
               n558Servico_Percentual = H00IR4_n558Servico_Percentual[0];
               A74Contrato_Codigo = H00IR4_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
               A2092Servico_IsOrigemReferencia = H00IR4_A2092Servico_IsOrigemReferencia[0];
               A638ContratoServicos_Ativo = H00IR4_A638ContratoServicos_Ativo[0];
               A1152ContratoServicos_PrazoAnalise = H00IR4_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = H00IR4_n1152ContratoServicos_PrazoAnalise[0];
               A888ContratoServicos_HmlSemCnf = H00IR4_A888ContratoServicos_HmlSemCnf[0];
               n888ContratoServicos_HmlSemCnf = H00IR4_n888ContratoServicos_HmlSemCnf[0];
               A607ServicoContrato_Faturamento = H00IR4_A607ServicoContrato_Faturamento[0];
               A555Servico_QtdContratada = H00IR4_A555Servico_QtdContratada[0];
               n555Servico_QtdContratada = H00IR4_n555Servico_QtdContratada[0];
               A1340ContratoServicos_QntUntCns = H00IR4_A1340ContratoServicos_QntUntCns[0];
               n1340ContratoServicos_QntUntCns = H00IR4_n1340ContratoServicos_QntUntCns[0];
               A2132ContratoServicos_UndCntNome = H00IR4_A2132ContratoServicos_UndCntNome[0];
               n2132ContratoServicos_UndCntNome = H00IR4_n2132ContratoServicos_UndCntNome[0];
               A1212ContratoServicos_UnidadeContratada = H00IR4_A1212ContratoServicos_UnidadeContratada[0];
               A557Servico_VlrUnidadeContratada = H00IR4_A557Servico_VlrUnidadeContratada[0];
               A1858ContratoServicos_Alias = H00IR4_A1858ContratoServicos_Alias[0];
               n1858ContratoServicos_Alias = H00IR4_n1858ContratoServicos_Alias[0];
               A608Servico_Nome = H00IR4_A608Servico_Nome[0];
               A155Servico_Codigo = H00IR4_A155Servico_Codigo[0];
               A911ContratoServicos_Prazos = H00IR4_A911ContratoServicos_Prazos[0];
               n911ContratoServicos_Prazos = H00IR4_n911ContratoServicos_Prazos[0];
               A914ContratoServicos_PrazoDias = H00IR4_A914ContratoServicos_PrazoDias[0];
               n914ContratoServicos_PrazoDias = H00IR4_n914ContratoServicos_PrazoDias[0];
               A913ContratoServicos_PrazoTipo = H00IR4_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = H00IR4_n913ContratoServicos_PrazoTipo[0];
               A160ContratoServicos_Codigo = H00IR4_A160ContratoServicos_Codigo[0];
               A1377ContratoServicos_Indicadores = H00IR4_A1377ContratoServicos_Indicadores[0];
               n1377ContratoServicos_Indicadores = H00IR4_n1377ContratoServicos_Indicadores[0];
               A2132ContratoServicos_UndCntNome = H00IR4_A2132ContratoServicos_UndCntNome[0];
               n2132ContratoServicos_UndCntNome = H00IR4_n2132ContratoServicos_UndCntNome[0];
               A1530Servico_TipoHierarquia = H00IR4_A1530Servico_TipoHierarquia[0];
               n1530Servico_TipoHierarquia = H00IR4_n1530Servico_TipoHierarquia[0];
               A632Servico_Ativo = H00IR4_A632Servico_Ativo[0];
               A2092Servico_IsOrigemReferencia = H00IR4_A2092Servico_IsOrigemReferencia[0];
               A608Servico_Nome = H00IR4_A608Servico_Nome[0];
               A911ContratoServicos_Prazos = H00IR4_A911ContratoServicos_Prazos[0];
               n911ContratoServicos_Prazos = H00IR4_n911ContratoServicos_Prazos[0];
               A914ContratoServicos_PrazoDias = H00IR4_A914ContratoServicos_PrazoDias[0];
               n914ContratoServicos_PrazoDias = H00IR4_n914ContratoServicos_PrazoDias[0];
               A913ContratoServicos_PrazoTipo = H00IR4_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = H00IR4_n913ContratoServicos_PrazoTipo[0];
               A1377ContratoServicos_Indicadores = H00IR4_A1377ContratoServicos_Indicadores[0];
               n1377ContratoServicos_Indicadores = H00IR4_n1377ContratoServicos_Indicadores[0];
               /* Execute user event: E21IR2 */
               E21IR2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 21;
            WBIR0( ) ;
         }
         nGXsfl_21_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV53TFContratoServicos_PrazoAnalise ,
                                              AV54TFContratoServicos_PrazoAnalise_To ,
                                              AV70TFContratoServicos_Ativo_Sel ,
                                              A1152ContratoServicos_PrazoAnalise ,
                                              A638ContratoServicos_Ativo ,
                                              AV68OrderedBy ,
                                              AV21OrderedDsc ,
                                              A74Contrato_Codigo ,
                                              AV7Contrato_Codigo ,
                                              A1530Servico_TipoHierarquia ,
                                              A632Servico_Ativo ,
                                              AV57TFContratoServicos_Indicadores ,
                                              A1377ContratoServicos_Indicadores ,
                                              AV58TFContratoServicos_Indicadores_To },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00IR7 */
         pr_default.execute(1, new Object[] {AV7Contrato_Codigo, AV57TFContratoServicos_Indicadores, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV58TFContratoServicos_Indicadores_To, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To});
         GRID_nRecordCount = H00IR7_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIR0( )
      {
         /* Before Start, stand alone formulas. */
         AV97Pgmname = "ContratoContratoServicosWC";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
         edtavPrazotipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrazotipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazotipo_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E19IR2 */
         E19IR2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV60DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOS_PRAZOANALISETITLEFILTERDATA"), AV52ContratoServicos_PrazoAnaliseTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOS_INDICADORESTITLEFILTERDATA"), AV56ContratoServicos_IndicadoresTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOS_ATIVOTITLEFILTERDATA"), AV69ContratoServicos_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavServico_tipohierarquia.Name = cmbavServico_tipohierarquia_Internalname;
            cmbavServico_tipohierarquia.CurrentValue = cgiGet( cmbavServico_tipohierarquia_Internalname);
            AV24Servico_TipoHierarquia = (short)(NumberUtil.Val( cgiGet( cmbavServico_tipohierarquia_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
            cmbavServico_ativo.Name = cmbavServico_ativo_Internalname;
            cmbavServico_ativo.CurrentValue = cgiGet( cmbavServico_ativo_Internalname);
            AV13Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbavServico_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Ativo", AV13Servico_Ativo);
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
            }
            else
            {
               AV68OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
            }
            AV21OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21OrderedDsc", AV21OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_PRAZOANALISE");
               GX_FocusControl = edtavTfcontratoservicos_prazoanalise_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContratoServicos_PrazoAnalise = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0)));
            }
            else
            {
               AV53TFContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_PRAZOANALISE_TO");
               GX_FocusControl = edtavTfcontratoservicos_prazoanalise_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoServicos_PrazoAnalise_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoServicos_PrazoAnalise_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0)));
            }
            else
            {
               AV54TFContratoServicos_PrazoAnalise_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_prazoanalise_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoServicos_PrazoAnalise_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_INDICADORES");
               GX_FocusControl = edtavTfcontratoservicos_indicadores_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFContratoServicos_Indicadores = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0)));
            }
            else
            {
               AV57TFContratoServicos_Indicadores = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_INDICADORES_TO");
               GX_FocusControl = edtavTfcontratoservicos_indicadores_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFContratoServicos_Indicadores_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicos_Indicadores_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0)));
            }
            else
            {
               AV58TFContratoServicos_Indicadores_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_indicadores_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicos_Indicadores_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_ATIVO_SEL");
               GX_FocusControl = edtavTfcontratoservicos_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContratoServicos_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFContratoServicos_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0));
            }
            else
            {
               AV70TFContratoServicos_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFContratoServicos_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0));
            }
            AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace", AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace);
            AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace", AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace);
            AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace", AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_21 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_21"), ",", "."));
            AV62GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV63GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtype");
            Confirmpanel2_Title = cgiGet( sPrefix+"CONFIRMPANEL2_Title");
            Confirmpanel2_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL2_Confirmationtext");
            Confirmpanel2_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL2_Yesbuttoncaption");
            Confirmpanel2_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL2_Nobuttoncaption");
            Confirmpanel2_Yesbuttonposition = cgiGet( sPrefix+"CONFIRMPANEL2_Yesbuttonposition");
            Ddo_contratoservicos_prazoanalise_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Caption");
            Ddo_contratoservicos_prazoanalise_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Tooltip");
            Ddo_contratoservicos_prazoanalise_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Cls");
            Ddo_contratoservicos_prazoanalise_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtext_set");
            Ddo_contratoservicos_prazoanalise_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtextto_set");
            Ddo_contratoservicos_prazoanalise_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Dropdownoptionstype");
            Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Titlecontrolidtoreplace");
            Ddo_contratoservicos_prazoanalise_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includesortasc"));
            Ddo_contratoservicos_prazoanalise_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includesortdsc"));
            Ddo_contratoservicos_prazoanalise_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includefilter"));
            Ddo_contratoservicos_prazoanalise_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filtertype");
            Ddo_contratoservicos_prazoanalise_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filterisrange"));
            Ddo_contratoservicos_prazoanalise_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Includedatalist"));
            Ddo_contratoservicos_prazoanalise_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Cleanfilter");
            Ddo_contratoservicos_prazoanalise_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Rangefilterfrom");
            Ddo_contratoservicos_prazoanalise_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Rangefilterto");
            Ddo_contratoservicos_prazoanalise_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Searchbuttontext");
            Ddo_contratoservicos_indicadores_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Caption");
            Ddo_contratoservicos_indicadores_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Tooltip");
            Ddo_contratoservicos_indicadores_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Cls");
            Ddo_contratoservicos_indicadores_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtext_set");
            Ddo_contratoservicos_indicadores_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtextto_set");
            Ddo_contratoservicos_indicadores_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Dropdownoptionstype");
            Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Titlecontrolidtoreplace");
            Ddo_contratoservicos_indicadores_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includesortasc"));
            Ddo_contratoservicos_indicadores_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includesortdsc"));
            Ddo_contratoservicos_indicadores_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includefilter"));
            Ddo_contratoservicos_indicadores_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filtertype");
            Ddo_contratoservicos_indicadores_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filterisrange"));
            Ddo_contratoservicos_indicadores_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Includedatalist"));
            Ddo_contratoservicos_indicadores_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Cleanfilter");
            Ddo_contratoservicos_indicadores_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Rangefilterfrom");
            Ddo_contratoservicos_indicadores_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Rangefilterto");
            Ddo_contratoservicos_indicadores_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Searchbuttontext");
            Ddo_contratoservicos_ativo_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Caption");
            Ddo_contratoservicos_ativo_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Tooltip");
            Ddo_contratoservicos_ativo_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Cls");
            Ddo_contratoservicos_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Selectedvalue_set");
            Ddo_contratoservicos_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Dropdownoptionstype");
            Ddo_contratoservicos_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Titlecontrolidtoreplace");
            Ddo_contratoservicos_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includesortasc"));
            Ddo_contratoservicos_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includesortdsc"));
            Ddo_contratoservicos_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortedstatus");
            Ddo_contratoservicos_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includefilter"));
            Ddo_contratoservicos_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Includedatalist"));
            Ddo_contratoservicos_ativo_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Datalisttype");
            Ddo_contratoservicos_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Datalistfixedvalues");
            Ddo_contratoservicos_ativo_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortasc");
            Ddo_contratoservicos_ativo_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Sortdsc");
            Ddo_contratoservicos_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Cleanfilter");
            Ddo_contratoservicos_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicos_prazoanalise_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Activeeventkey");
            Ddo_contratoservicos_prazoanalise_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtext_get");
            Ddo_contratoservicos_prazoanalise_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE_Filteredtextto_get");
            Ddo_contratoservicos_indicadores_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Activeeventkey");
            Ddo_contratoservicos_indicadores_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtext_get");
            Ddo_contratoservicos_indicadores_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES_Filteredtextto_get");
            Ddo_contratoservicos_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Activeeventkey");
            Ddo_contratoservicos_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOS_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            nGXsfl_21_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
            SubsflControlProps_212( ) ;
            if ( nGXsfl_21_idx > 0 )
            {
               AV14Update = cgiGet( edtavUpdate_Internalname);
               AV73Apagar = cgiGet( edtavApagar_Internalname);
               AV15Display = cgiGet( edtavDisplay_Internalname);
               A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
               A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
               A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
               A1858ContratoServicos_Alias = StringUtil.Upper( cgiGet( edtContratoServicos_Alias_Internalname));
               n1858ContratoServicos_Alias = false;
               A557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".");
               A1212ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_UnidadeContratada_Internalname), ",", "."));
               A2132ContratoServicos_UndCntNome = StringUtil.Upper( cgiGet( edtContratoServicos_UndCntNome_Internalname));
               n2132ContratoServicos_UndCntNome = false;
               A1340ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".");
               n1340ContratoServicos_QntUntCns = false;
               A555Servico_QtdContratada = (long)(context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", "."));
               n555Servico_QtdContratada = false;
               cmbServicoContrato_Faturamento.Name = cmbServicoContrato_Faturamento_Internalname;
               cmbServicoContrato_Faturamento.CurrentValue = cgiGet( cmbServicoContrato_Faturamento_Internalname);
               A607ServicoContrato_Faturamento = cgiGet( cmbServicoContrato_Faturamento_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") > 999.999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCENTUAL");
                  GX_FocusControl = edtavServico_percentual_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV18Servico_Percentual = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.Str( AV18Servico_Percentual, 7, 3)));
               }
               else
               {
                  AV18Servico_Percentual = context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.Str( AV18Servico_Percentual, 7, 3)));
               }
               cmbContratoServicos_HmlSemCnf.Name = cmbContratoServicos_HmlSemCnf_Internalname;
               cmbContratoServicos_HmlSemCnf.CurrentValue = cgiGet( cmbContratoServicos_HmlSemCnf_Internalname);
               A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cgiGet( cmbContratoServicos_HmlSemCnf_Internalname));
               n888ContratoServicos_HmlSemCnf = false;
               A1152ContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", "."));
               n1152ContratoServicos_PrazoAnalise = false;
               AV19PrazoTipo = StringUtil.Upper( cgiGet( edtavPrazotipo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrazotipo_Internalname, AV19PrazoTipo);
               A1377ContratoServicos_Indicadores = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_Indicadores_Internalname), ",", "."));
               n1377ContratoServicos_Indicadores = false;
               A638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicos_Ativo_Internalname));
               cmbServico_IsOrigemReferencia.Name = cmbServico_IsOrigemReferencia_Internalname;
               cmbServico_IsOrigemReferencia.CurrentValue = cgiGet( cmbServico_IsOrigemReferencia_Internalname);
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( cmbServico_IsOrigemReferencia_Internalname));
               AV17AssociarSistema = cgiGet( edtavAssociarsistema_Internalname);
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV68OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV21OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE"), ",", ".") != Convert.ToDecimal( AV53TFContratoServicos_PrazoAnalise )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_PRAZOANALISE_TO"), ",", ".") != Convert.ToDecimal( AV54TFContratoServicos_PrazoAnalise_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES"), ",", ".") != Convert.ToDecimal( AV57TFContratoServicos_Indicadores )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_INDICADORES_TO"), ",", ".") != Convert.ToDecimal( AV58TFContratoServicos_Indicadores_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV70TFContratoServicos_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E19IR2 */
         E19IR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19IR2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV24Servico_TipoHierarquia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
         edtavTfcontratoservicos_prazoanalise_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicos_prazoanalise_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_prazoanalise_Visible), 5, 0)));
         edtavTfcontratoservicos_prazoanalise_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicos_prazoanalise_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_prazoanalise_to_Visible), 5, 0)));
         edtavTfcontratoservicos_indicadores_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicos_indicadores_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_indicadores_Visible), 5, 0)));
         edtavTfcontratoservicos_indicadores_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicos_indicadores_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_indicadores_to_Visible), 5, 0)));
         edtavTfcontratoservicos_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicos_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_ativo_sel_Visible), 5, 0)));
         Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicos_PrazoAnalise";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_prazoanalise_Internalname, "TitleControlIdToReplace", Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace);
         AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace = Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace", AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace);
         edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicos_Indicadores";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_indicadores_Internalname, "TitleControlIdToReplace", Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace);
         AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace = Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace", AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace);
         edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicos_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicos_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicos_ativo_Titlecontrolidtoreplace);
         AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace = Ddo_contratoservicos_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace", AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace);
         edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV68OrderedBy < 1 )
         {
            AV68OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV60DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV60DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E20IR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV52ContratoServicos_PrazoAnaliseTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56ContratoServicos_IndicadoresTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoServicos_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicos_PrazoAnalise_Titleformat = 2;
         edtContratoServicos_PrazoAnalise_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "An�lise", AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_PrazoAnalise_Internalname, "Title", edtContratoServicos_PrazoAnalise_Title);
         edtContratoServicos_Indicadores_Titleformat = 2;
         edtContratoServicos_Indicadores_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Indicadores", AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_Indicadores_Internalname, "Title", edtContratoServicos_Indicadores_Title);
         chkContratoServicos_Ativo_Titleformat = 2;
         chkContratoServicos_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_Ativo_Internalname, "Title", chkContratoServicos_Ativo.Title.Text);
         cmbServico_IsOrigemReferencia_Titleformat = 2;
         cmbServico_IsOrigemReferencia.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV68OrderedBy==2) ? (AV21OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Exige Origem de Refer�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_IsOrigemReferencia_Internalname, "Title", cmbServico_IsOrigemReferencia.Title.Text);
         AV62GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridCurrentPage), 10, 0)));
         AV63GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridPageCount), 10, 0)));
         edtavAssociarsistema_Title = "Sistemas";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Title", edtavAssociarsistema_Title);
         AV22ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV52ContratoServicos_PrazoAnaliseTitleFilterData", AV52ContratoServicos_PrazoAnaliseTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV56ContratoServicos_IndicadoresTitleFilterData", AV56ContratoServicos_IndicadoresTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV69ContratoServicos_AtivoTitleFilterData", AV69ContratoServicos_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11IR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV61PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV61PageToGo) ;
         }
      }

      protected void E14IR2( )
      {
         /* Ddo_contratoservicos_prazoanalise_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicos_prazoanalise_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoServicos_PrazoAnalise = (short)(NumberUtil.Val( Ddo_contratoservicos_prazoanalise_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0)));
            AV54TFContratoServicos_PrazoAnalise_To = (short)(NumberUtil.Val( Ddo_contratoservicos_prazoanalise_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoServicos_PrazoAnalise_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15IR2( )
      {
         /* Ddo_contratoservicos_indicadores_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicos_indicadores_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFContratoServicos_Indicadores = (short)(NumberUtil.Val( Ddo_contratoservicos_indicadores_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0)));
            AV58TFContratoServicos_Indicadores_To = (short)(NumberUtil.Val( Ddo_contratoservicos_indicadores_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicos_Indicadores_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16IR2( )
      {
         /* Ddo_contratoservicos_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicos_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV68OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
            AV21OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21OrderedDsc", AV21OrderedDsc);
            Ddo_contratoservicos_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "SortedStatus", Ddo_contratoservicos_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicos_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV68OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
            AV21OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21OrderedDsc", AV21OrderedDsc);
            Ddo_contratoservicos_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "SortedStatus", Ddo_contratoservicos_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicos_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContratoServicos_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contratoservicos_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFContratoServicos_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E21IR2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +AV7Contrato_Codigo);
            AV14Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV14Update);
            AV92Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV14Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV14Update);
            AV92Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavApagar_Tooltiptext = "";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV73Apagar = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavApagar_Internalname, AV73Apagar);
            AV93Apagar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavApagar_Enabled = 1;
         }
         else
         {
            edtavApagar_Enabled = 0;
         }
         AV15Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV15Display);
         AV94Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV17AssociarSistema = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarsistema_Internalname, AV17AssociarSistema);
         AV95Associarsistema_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarsistema_Tooltiptext = "Clique aqui p/ associar os sistemas deste servi�o!";
         edtavPrazotipo_Link = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(((A911ContratoServicos_Prazos==0) ? "INS" : "UPD"))) + "," + UrlEncode("" +A160ContratoServicos_Codigo);
         edtContratoServicos_Indicadores_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim("Indicadores"));
         if ( A638ContratoServicos_Ativo )
         {
            AV72Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV72Color = GXUtil.RGB( 255, 0, 0);
         }
         edtServico_Nome_Forecolor = (int)(AV72Color);
         edtContratoServicos_Alias_Forecolor = (int)(AV72Color);
         edtServico_VlrUnidadeContratada_Forecolor = (int)(AV72Color);
         edtServico_QtdContratada_Forecolor = (int)(AV72Color);
         cmbServicoContrato_Faturamento.ForeColor = (int)(AV72Color);
         edtavServico_percentual_Forecolor = (int)(AV72Color);
         cmbContratoServicos_HmlSemCnf.ForeColor = (int)(AV72Color);
         edtContratoServicos_PrazoAnalise_Forecolor = (int)(AV72Color);
         edtavPrazotipo_Forecolor = (int)(AV72Color);
         edtContratoServicos_Indicadores_Forecolor = (int)(AV72Color);
         GXt_boolean2 = false;
         new prc_existeintegridadereferencial(context ).execute(  A160ContratoServicos_Codigo,  "Servico", out  GXt_boolean2) ;
         edtavApagar_Visible = (AV6WWPContext.gxTpr_Delete&&!GXt_boolean2 ? 1 : 0);
         edtContratoServicos_Indicadores_Linktarget = "_blank";
         AV18Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.Str( AV18Servico_Percentual, 7, 3)));
         AV19PrazoTipo = gxdomaintipodeprazo.getDescription(context,A913ContratoServicos_PrazoTipo) + ((StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F")==0) ? " ("+StringUtil.Trim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0))+")" : "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrazotipo_Internalname, AV19PrazoTipo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 21;
         }
         sendrow_212( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_21_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(21, GridRow);
         }
      }

      protected void E17IR2( )
      {
         /* 'DoInsert' Routine */
         AV20TemUSCCadastradas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TemUSCCadastradas", AV20TemUSCCadastradas);
         AV96GXLvl205 = 0;
         /* Using cursor H00IR8 */
         pr_default.execute(2, new Object[] {AV7Contrato_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1207ContratoUnidades_ContratoCod = H00IR8_A1207ContratoUnidades_ContratoCod[0];
            AV96GXLvl205 = 1;
            AV20TemUSCCadastradas = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TemUSCCadastradas", AV20TemUSCCadastradas);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV96GXLvl205 == 0 )
         {
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         if ( AV20TemUSCCadastradas )
         {
            context.wjLoc = formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Contrato_Codigo);
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E18IR2( )
      {
         /* 'DoClonar' Routine */
         if ( (0==AV22ContratoServicos_Codigo) )
         {
            GX_msglist.addItem("Selecione a linha com o servi�o que deseja clonar!");
         }
         else
         {
            context.PopUp(formatLink("wp_clonarservico.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo), new Object[] {});
            context.DoAjaxRefreshCmp(sPrefix);
         }
      }

      protected void E23IR2( )
      {
         /* 'DoAssociarSistema' Routine */
         AV22ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratoServicos_Codigo), 6, 0)));
         context.PopUp(formatLink("wp_associarcontratoservicossistemas.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A155Servico_Codigo), new Object[] {});
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicos_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "SortedStatus", Ddo_contratoservicos_ativo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV68OrderedBy == 1 )
         {
            Ddo_contratoservicos_ativo_Sortedstatus = (AV21OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "SortedStatus", Ddo_contratoservicos_ativo_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgClonar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgClonar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgClonar_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV97Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV97Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV16Session.Get(AV97Pgmname+"GridState"), "");
         }
         AV68OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68OrderedBy), 4, 0)));
         AV21OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21OrderedDsc", AV21OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV98GXV1 = 1;
         while ( AV98GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV98GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "SERVICO_TIPOHIERARQUIA") == 0 )
            {
               AV24Servico_TipoHierarquia = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "SERVICO_ATIVO") == 0 )
            {
               AV13Servico_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Ativo", AV13Servico_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_PRAZOANALISE") == 0 )
            {
               AV53TFContratoServicos_PrazoAnalise = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0)));
               AV54TFContratoServicos_PrazoAnalise_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoServicos_PrazoAnalise_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0)));
               if ( ! (0==AV53TFContratoServicos_PrazoAnalise) )
               {
                  Ddo_contratoservicos_prazoanalise_Filteredtext_set = StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_prazoanalise_Internalname, "FilteredText_set", Ddo_contratoservicos_prazoanalise_Filteredtext_set);
               }
               if ( ! (0==AV54TFContratoServicos_PrazoAnalise_To) )
               {
                  Ddo_contratoservicos_prazoanalise_Filteredtextto_set = StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_prazoanalise_Internalname, "FilteredTextTo_set", Ddo_contratoservicos_prazoanalise_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_INDICADORES") == 0 )
            {
               AV57TFContratoServicos_Indicadores = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0)));
               AV58TFContratoServicos_Indicadores_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicos_Indicadores_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0)));
               if ( ! (0==AV57TFContratoServicos_Indicadores) )
               {
                  Ddo_contratoservicos_indicadores_Filteredtext_set = StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_indicadores_Internalname, "FilteredText_set", Ddo_contratoservicos_indicadores_Filteredtext_set);
               }
               if ( ! (0==AV58TFContratoServicos_Indicadores_To) )
               {
                  Ddo_contratoservicos_indicadores_Filteredtextto_set = StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_indicadores_Internalname, "FilteredTextTo_set", Ddo_contratoservicos_indicadores_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_ATIVO_SEL") == 0 )
            {
               AV70TFContratoServicos_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFContratoServicos_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0));
               if ( ! (0==AV70TFContratoServicos_Ativo_Sel) )
               {
                  Ddo_contratoservicos_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicos_ativo_Internalname, "SelectedValue_set", Ddo_contratoservicos_ativo_Selectedvalue_set);
               }
            }
            AV98GXV1 = (int)(AV98GXV1+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV16Session.Get(AV97Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV68OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV21OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV24Servico_TipoHierarquia) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "SERVICO_TIPOHIERARQUIA";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (false==AV13Servico_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "SERVICO_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV13Servico_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV53TFContratoServicos_PrazoAnalise) && (0==AV54TFContratoServicos_PrazoAnalise_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOS_PRAZOANALISE";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV53TFContratoServicos_PrazoAnalise), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV54TFContratoServicos_PrazoAnalise_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV57TFContratoServicos_Indicadores) && (0==AV58TFContratoServicos_Indicadores_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOS_INDICADORES";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV57TFContratoServicos_Indicadores), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV58TFContratoServicos_Indicadores_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV70TFContratoServicos_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOS_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV70TFContratoServicos_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV97Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV97Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E22IR2( )
      {
         /* Apagar_Click Routine */
         AV22ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratoServicos_Codigo), 6, 0)));
         /* Execute user subroutine: 'CONFIRMARDELETE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24IR2( )
      {
         /* Grid_Onlineactivate Routine */
         AV22ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratoServicos_Codigo), 6, 0)));
      }

      protected void E12IR2( )
      {
         /* Confirmpanel_Close Routine */
         if ( ! AV20TemUSCCadastradas )
         {
            context.wjLoc = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratoUnidades"));
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E13IR2( )
      {
         /* Confirmpanel2_Onyes Routine */
         new prc_dltcontratoservicos(context ).execute( ref  A160ContratoServicos_Codigo) ;
         gxgrGrid_refresh( subGrid_Rows, AV68OrderedBy, AV21OrderedDsc, AV53TFContratoServicos_PrazoAnalise, AV54TFContratoServicos_PrazoAnalise_To, AV57TFContratoServicos_Indicadores, AV58TFContratoServicos_Indicadores_To, AV70TFContratoServicos_Ativo_Sel, AV7Contrato_Codigo, AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace, AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace, AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace, AV6WWPContext, AV97Pgmname, AV24Servico_TipoHierarquia, AV13Servico_Ativo, A160ContratoServicos_Codigo, A911ContratoServicos_Prazos, A638ContratoServicos_Ativo, A558Servico_Percentual, A913ContratoServicos_PrazoTipo, A914ContratoServicos_PrazoDias, sPrefix) ;
      }

      protected void S172( )
      {
         /* 'CONFIRMARDELETE' Routine */
         AV75Tabelas = "";
         /* Using cursor H00IR9 */
         pr_default.execute(3, new Object[] {AV22ContratoServicos_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A160ContratoServicos_Codigo = H00IR9_A160ContratoServicos_Codigo[0];
            /* Using cursor H00IR10 */
            pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A903ContratoServicosPrazo_CntSrvCod = H00IR10_A903ContratoServicosPrazo_CntSrvCod[0];
               AV75Tabelas = AV75Tabelas + "Prazo, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            /* Using cursor H00IR11 */
            pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A915ContratoSrvVnc_CntSrvCod = H00IR11_A915ContratoSrvVnc_CntSrvCod[0];
               AV75Tabelas = AV75Tabelas + "Regras, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(5);
            }
            pr_default.close(5);
            /* Using cursor H00IR12 */
            pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A926ContratoServicosTelas_ContratoCod = H00IR12_A926ContratoServicosTelas_ContratoCod[0];
               AV75Tabelas = AV75Tabelas + "Telas, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(6);
            }
            pr_default.close(6);
            /* Using cursor H00IR13 */
            pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A1335ContratoServicosPrioridade_CntSrvCod = H00IR13_A1335ContratoServicosPrioridade_CntSrvCod[0];
               AV75Tabelas = AV75Tabelas + "Prioridades, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(7);
            }
            pr_default.close(7);
            /* Using cursor H00IR14 */
            pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A1270ContratoServicosIndicador_CntSrvCod = H00IR14_A1270ContratoServicosIndicador_CntSrvCod[0];
               AV75Tabelas = AV75Tabelas + "Indicadores, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(8);
            }
            pr_default.close(8);
            /* Using cursor H00IR15 */
            pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1471ContratoServicosCusto_CntSrvCod = H00IR15_A1471ContratoServicosCusto_CntSrvCod[0];
               AV75Tabelas = AV75Tabelas + "Custos, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Using cursor H00IR16 */
            pr_default.execute(10, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               AV75Tabelas = AV75Tabelas + "Artefatos, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(10);
            }
            pr_default.close(10);
            /* Using cursor H00IR17 */
            pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A2069ContratoServicosRmn_Sequencial = H00IR17_A2069ContratoServicosRmn_Sequencial[0];
               AV75Tabelas = AV75Tabelas + "Remunera��o, ";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(11);
            }
            pr_default.close(11);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75Tabelas)) )
         {
            new prc_dltcontratoservicos(context ).execute( ref  A160ContratoServicos_Codigo) ;
         }
         else
         {
            AV75Tabelas = StringUtil.Substring( AV75Tabelas, 1, StringUtil.StringSearchRev( AV75Tabelas, ",", -1)-1);
            Confirmpanel2_Confirmationtext = "Existem dados nas tabelas "+AV75Tabelas;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
         }
      }

      protected void wb_table1_2_IR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_5_IR2( true) ;
         }
         else
         {
            wb_table2_5_IR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_18_IR2( true) ;
         }
         else
         {
            wb_table3_18_IR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_IR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_47_IR2( true) ;
         }
         else
         {
            wb_table4_47_IR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_47_IR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_56_IR2( true) ;
         }
         else
         {
            wb_table5_56_IR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_56_IR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IR2e( true) ;
         }
         else
         {
            wb_table1_2_IR2e( false) ;
         }
      }

      protected void wb_table5_56_IR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANEL2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANEL2Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_56_IR2e( true) ;
         }
         else
         {
            wb_table5_56_IR2e( false) ;
         }
      }

      protected void wb_table4_47_IR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Novo servi�o", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoContratoServicosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgClonar_Internalname, context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, imgClonar_Enabled, "", "Clonar servi�o selecionado", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgClonar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLONAR\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoContratoServicosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_47_IR2e( true) ;
         }
         else
         {
            wb_table4_47_IR2e( false) ;
         }
      }

      protected void wb_table3_18_IR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"21\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo do Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Alias") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(90), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Valor Uni.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Unidade Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Unidade Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtde. Unit. de Consumo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(88), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtde.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Faturamento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(67), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Percentual") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sem Cnf") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicos_PrazoAnalise_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicos_PrazoAnalise_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicos_PrazoAnalise_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Execu��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicos_Indicadores_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicos_Indicadores_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicos_Indicadores_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratoServicos_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratoServicos_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratoServicos_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_IsOrigemReferencia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_IsOrigemReferencia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_IsOrigemReferencia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarsistema_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV14Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV73Apagar));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavApagar_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavApagar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavApagar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1858ContratoServicos_Alias));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_Alias_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_VlrUnidadeContratada_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2132ContratoServicos_UndCntNome));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A555Servico_QtdContratada), 13, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_QtdContratada_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A607ServicoContrato_Faturamento);
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServicoContrato_Faturamento.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV18Servico_Percentual, 7, 3, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavServico_percentual_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavServico_percentual_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicos_HmlSemCnf.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicos_PrazoAnalise_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_PrazoAnalise_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_PrazoAnalise_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV19PrazoTipo));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrazotipo_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrazotipo_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavPrazotipo_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1377ContratoServicos_Indicadores), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicos_Indicadores_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_Indicadores_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_Indicadores_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoServicos_Indicadores_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContratoServicos_Indicadores_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A638ContratoServicos_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratoServicos_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratoServicos_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_IsOrigemReferencia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_IsOrigemReferencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17AssociarSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarsistema_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarsistema_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 21 )
         {
            wbEnd = 0;
            nRC_GXsfl_21 = (short)(nGXsfl_21_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_IR2e( true) ;
         }
         else
         {
            wb_table3_18_IR2e( false) ;
         }
      }

      protected void wb_table2_5_IR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextservico_tipohierarquia_Internalname, "Tipo de Hierarquia", "", "", lblFiltertextservico_tipohierarquia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoContratoServicosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_tipohierarquia, cmbavServico_tipohierarquia_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0)), 1, cmbavServico_tipohierarquia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_ContratoContratoServicosWC.htm");
            cmbavServico_tipohierarquia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24Servico_TipoHierarquia), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_tipohierarquia_Internalname, "Values", (String)(cmbavServico_tipohierarquia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextservico_ativo_Internalname, "Ativo?", "", "", lblFiltertextservico_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoContratoServicosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_ativo, cmbavServico_ativo_Internalname, StringUtil.BoolToStr( AV13Servico_Ativo), 1, cmbavServico_ativo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_ContratoContratoServicosWC.htm");
            cmbavServico_ativo.CurrentValue = StringUtil.BoolToStr( AV13Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_ativo_Internalname, "Values", (String)(cmbavServico_ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IR2e( true) ;
         }
         else
         {
            wb_table2_5_IR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIR2( ) ;
         WSIR2( ) ;
         WEIR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAIR2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratocontratoservicoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAIR2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contrato_Codigo != wcpOAV7Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contrato_Codigo = AV7Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contrato_Codigo = cgiGet( sPrefix+"AV7Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contrato_Codigo) > 0 )
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         else
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAIR2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSIR2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSIR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEIR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299304922");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratocontratoservicoswc.js", "?20205299304922");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_212( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_21_idx;
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_21_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_21_idx;
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO_"+sGXsfl_21_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_21_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_21_idx;
         edtContratoServicos_Alias_Internalname = sPrefix+"CONTRATOSERVICOS_ALIAS_"+sGXsfl_21_idx;
         edtServico_VlrUnidadeContratada_Internalname = sPrefix+"SERVICO_VLRUNIDADECONTRATADA_"+sGXsfl_21_idx;
         edtContratoServicos_UnidadeContratada_Internalname = sPrefix+"CONTRATOSERVICOS_UNIDADECONTRATADA_"+sGXsfl_21_idx;
         edtContratoServicos_UndCntNome_Internalname = sPrefix+"CONTRATOSERVICOS_UNDCNTNOME_"+sGXsfl_21_idx;
         edtContratoServicos_QntUntCns_Internalname = sPrefix+"CONTRATOSERVICOS_QNTUNTCNS_"+sGXsfl_21_idx;
         edtServico_QtdContratada_Internalname = sPrefix+"SERVICO_QTDCONTRATADA_"+sGXsfl_21_idx;
         cmbServicoContrato_Faturamento_Internalname = sPrefix+"SERVICOCONTRATO_FATURAMENTO_"+sGXsfl_21_idx;
         edtavServico_percentual_Internalname = sPrefix+"vSERVICO_PERCENTUAL_"+sGXsfl_21_idx;
         cmbContratoServicos_HmlSemCnf_Internalname = sPrefix+"CONTRATOSERVICOS_HMLSEMCNF_"+sGXsfl_21_idx;
         edtContratoServicos_PrazoAnalise_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOANALISE_"+sGXsfl_21_idx;
         edtavPrazotipo_Internalname = sPrefix+"vPRAZOTIPO_"+sGXsfl_21_idx;
         edtContratoServicos_Indicadores_Internalname = sPrefix+"CONTRATOSERVICOS_INDICADORES_"+sGXsfl_21_idx;
         chkContratoServicos_Ativo_Internalname = sPrefix+"CONTRATOSERVICOS_ATIVO_"+sGXsfl_21_idx;
         cmbServico_IsOrigemReferencia_Internalname = sPrefix+"SERVICO_ISORIGEMREFERENCIA_"+sGXsfl_21_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_21_idx;
      }

      protected void SubsflControlProps_fel_212( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_21_fel_idx;
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_21_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_21_fel_idx;
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO_"+sGXsfl_21_fel_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_21_fel_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_21_fel_idx;
         edtContratoServicos_Alias_Internalname = sPrefix+"CONTRATOSERVICOS_ALIAS_"+sGXsfl_21_fel_idx;
         edtServico_VlrUnidadeContratada_Internalname = sPrefix+"SERVICO_VLRUNIDADECONTRATADA_"+sGXsfl_21_fel_idx;
         edtContratoServicos_UnidadeContratada_Internalname = sPrefix+"CONTRATOSERVICOS_UNIDADECONTRATADA_"+sGXsfl_21_fel_idx;
         edtContratoServicos_UndCntNome_Internalname = sPrefix+"CONTRATOSERVICOS_UNDCNTNOME_"+sGXsfl_21_fel_idx;
         edtContratoServicos_QntUntCns_Internalname = sPrefix+"CONTRATOSERVICOS_QNTUNTCNS_"+sGXsfl_21_fel_idx;
         edtServico_QtdContratada_Internalname = sPrefix+"SERVICO_QTDCONTRATADA_"+sGXsfl_21_fel_idx;
         cmbServicoContrato_Faturamento_Internalname = sPrefix+"SERVICOCONTRATO_FATURAMENTO_"+sGXsfl_21_fel_idx;
         edtavServico_percentual_Internalname = sPrefix+"vSERVICO_PERCENTUAL_"+sGXsfl_21_fel_idx;
         cmbContratoServicos_HmlSemCnf_Internalname = sPrefix+"CONTRATOSERVICOS_HMLSEMCNF_"+sGXsfl_21_fel_idx;
         edtContratoServicos_PrazoAnalise_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOANALISE_"+sGXsfl_21_fel_idx;
         edtavPrazotipo_Internalname = sPrefix+"vPRAZOTIPO_"+sGXsfl_21_fel_idx;
         edtContratoServicos_Indicadores_Internalname = sPrefix+"CONTRATOSERVICOS_INDICADORES_"+sGXsfl_21_fel_idx;
         chkContratoServicos_Ativo_Internalname = sPrefix+"CONTRATOSERVICOS_ATIVO_"+sGXsfl_21_fel_idx;
         cmbServico_IsOrigemReferencia_Internalname = sPrefix+"SERVICO_ISORIGEMREFERENCIA_"+sGXsfl_21_fel_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_21_fel_idx;
      }

      protected void sendrow_212( )
      {
         SubsflControlProps_212( ) ;
         WBIR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_21_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_21_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_21_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV14Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV92Update_GXI : context.PathToRelativeUrl( AV14Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV14Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavApagar_Enabled!=0)&&(edtavApagar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'"+sPrefix+"',false,'',21)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV73Apagar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV73Apagar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Apagar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV73Apagar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavApagar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV73Apagar)) ? AV93Apagar_GXI : context.PathToRelativeUrl( AV73Apagar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavApagar_Visible,(int)edtavApagar_Enabled,(String)"",(String)edtavApagar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavApagar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVAPAGAR.CLICK."+sGXsfl_21_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV73Apagar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV94Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)) ? AV94Display_GXI : context.PathToRelativeUrl( AV15Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtServico_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_Alias_Internalname,StringUtil.RTrim( A1858ContratoServicos_Alias),StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_Alias_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratoServicos_Alias_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_VlrUnidadeContratada_Internalname,StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")),context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_VlrUnidadeContratada_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtServico_VlrUnidadeContratada_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)90,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_UnidadeContratada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1212ContratoServicos_UnidadeContratada), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_UnidadeContratada_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_UndCntNome_Internalname,StringUtil.RTrim( A2132ContratoServicos_UndCntNome),StringUtil.RTrim( context.localUtil.Format( A2132ContratoServicos_UndCntNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_UndCntNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_QntUntCns_Internalname,StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ",", "")),context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_QntUntCns_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_QtdContratada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A555Servico_QtdContratada), 13, 0, ",", "")),context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_QtdContratada_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtServico_QtdContratada_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)88,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_21_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICOCONTRATO_FATURAMENTO_" + sGXsfl_21_idx;
               cmbServicoContrato_Faturamento.Name = GXCCtl;
               cmbServicoContrato_Faturamento.WebTags = "";
               cmbServicoContrato_Faturamento.addItem("B", "Bruto", 0);
               cmbServicoContrato_Faturamento.addItem("L", "Liquido", 0);
               if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
               {
                  A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServicoContrato_Faturamento,(String)cmbServicoContrato_Faturamento_Internalname,StringUtil.RTrim( A607ServicoContrato_Faturamento),(short)1,(String)cmbServicoContrato_Faturamento_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbServicoContrato_Faturamento.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServicoContrato_Faturamento.CurrentValue = StringUtil.RTrim( A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServicoContrato_Faturamento_Internalname, "Values", (String)(cmbServicoContrato_Faturamento.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavServico_percentual_Enabled!=0)&&(edtavServico_percentual_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 35,'"+sPrefix+"',false,'"+sGXsfl_21_idx+"',21)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavServico_percentual_Internalname,StringUtil.LTrim( StringUtil.NToC( AV18Servico_Percentual, 7, 3, ",", "")),((edtavServico_percentual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV18Servico_Percentual, "ZZ9.999")) : context.localUtil.Format( AV18Servico_Percentual, "ZZ9.999")),TempTags+((edtavServico_percentual_Enabled!=0)&&(edtavServico_percentual_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavServico_percentual_Enabled!=0)&&(edtavServico_percentual_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,35);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavServico_percentual_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavServico_percentual_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavServico_percentual_Enabled,(short)0,(String)"text",(String)"",(short)67,(String)"px",(short)17,(String)"px",(short)7,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_21_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOS_HMLSEMCNF_" + sGXsfl_21_idx;
               cmbContratoServicos_HmlSemCnf.Name = GXCCtl;
               cmbContratoServicos_HmlSemCnf.WebTags = "";
               cmbContratoServicos_HmlSemCnf.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratoServicos_HmlSemCnf.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratoServicos_HmlSemCnf.ItemCount > 0 )
               {
                  A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cmbContratoServicos_HmlSemCnf.getValidValue(StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf)));
                  n888ContratoServicos_HmlSemCnf = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicos_HmlSemCnf,(String)cmbContratoServicos_HmlSemCnf_Internalname,StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf),(short)1,(String)cmbContratoServicos_HmlSemCnf_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContratoServicos_HmlSemCnf.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicos_HmlSemCnf.CurrentValue = StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_HmlSemCnf_Internalname, "Values", (String)(cmbContratoServicos_HmlSemCnf.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_PrazoAnalise_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_PrazoAnalise_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratoServicos_PrazoAnalise_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPrazotipo_Enabled!=0)&&(edtavPrazotipo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 38,'"+sPrefix+"',false,'"+sGXsfl_21_idx+"',21)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrazotipo_Internalname,StringUtil.RTrim( AV19PrazoTipo),StringUtil.RTrim( context.localUtil.Format( AV19PrazoTipo, "@!")),TempTags+((edtavPrazotipo_Enabled!=0)&&(edtavPrazotipo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPrazotipo_Enabled!=0)&&(edtavPrazotipo_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtavPrazotipo_Link,(String)"",(String)"",(String)"",(String)edtavPrazotipo_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavPrazotipo_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavPrazotipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_Indicadores_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1377ContratoServicos_Indicadores), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1377ContratoServicos_Indicadores), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratoServicos_Indicadores_Link,(String)edtContratoServicos_Indicadores_Linktarget,(String)"",(String)"",(String)edtContratoServicos_Indicadores_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratoServicos_Indicadores_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratoServicos_Ativo_Internalname,StringUtil.BoolToStr( A638ContratoServicos_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "SERVICO_ISORIGEMREFERENCIA_" + sGXsfl_21_idx;
            cmbServico_IsOrigemReferencia.Name = GXCCtl;
            cmbServico_IsOrigemReferencia.WebTags = "";
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
            {
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_IsOrigemReferencia,(String)cmbServico_IsOrigemReferencia_Internalname,StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia),(short)1,(String)cmbServico_IsOrigemReferencia_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_IsOrigemReferencia.CurrentValue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_IsOrigemReferencia_Internalname, "Values", (String)(cmbServico_IsOrigemReferencia.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarsistema_Enabled!=0)&&(edtavAssociarsistema_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 42,'"+sPrefix+"',false,'',21)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV17AssociarSistema_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17AssociarSistema))&&String.IsNullOrEmpty(StringUtil.RTrim( AV95Associarsistema_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17AssociarSistema)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarsistema_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17AssociarSistema)) ? AV95Associarsistema_GXI : context.PathToRelativeUrl( AV17AssociarSistema)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarsistema_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAssociarsistema_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOASSOCIARSISTEMA\\'."+sGXsfl_21_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17AssociarSistema_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_CODIGO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ALIAS"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VLRUNIDADECONTRATADA"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_UNIDADECONTRATADA"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( (decimal)(A1212ContratoServicos_UnidadeContratada), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_QNTUNTCNS"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_QTDCONTRATADA"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOCONTRATO_FATURAMENTO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_HMLSEMCNF"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, A888ContratoServicos_HmlSemCnf));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOANALISE"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ATIVO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sPrefix+sGXsfl_21_idx, A638ContratoServicos_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_21_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_21_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_21_idx+1));
            sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
            SubsflControlProps_212( ) ;
         }
         /* End function sendrow_212 */
      }

      protected void init_default_properties( )
      {
         lblFiltertextservico_tipohierarquia_Internalname = sPrefix+"FILTERTEXTSERVICO_TIPOHIERARQUIA";
         cmbavServico_tipohierarquia_Internalname = sPrefix+"vSERVICO_TIPOHIERARQUIA";
         lblFiltertextservico_ativo_Internalname = sPrefix+"FILTERTEXTSERVICO_ATIVO";
         cmbavServico_ativo_Internalname = sPrefix+"vSERVICO_ATIVO";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavApagar_Internalname = sPrefix+"vAPAGAR";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         edtContratoServicos_Alias_Internalname = sPrefix+"CONTRATOSERVICOS_ALIAS";
         edtServico_VlrUnidadeContratada_Internalname = sPrefix+"SERVICO_VLRUNIDADECONTRATADA";
         edtContratoServicos_UnidadeContratada_Internalname = sPrefix+"CONTRATOSERVICOS_UNIDADECONTRATADA";
         edtContratoServicos_UndCntNome_Internalname = sPrefix+"CONTRATOSERVICOS_UNDCNTNOME";
         edtContratoServicos_QntUntCns_Internalname = sPrefix+"CONTRATOSERVICOS_QNTUNTCNS";
         edtServico_QtdContratada_Internalname = sPrefix+"SERVICO_QTDCONTRATADA";
         cmbServicoContrato_Faturamento_Internalname = sPrefix+"SERVICOCONTRATO_FATURAMENTO";
         edtavServico_percentual_Internalname = sPrefix+"vSERVICO_PERCENTUAL";
         cmbContratoServicos_HmlSemCnf_Internalname = sPrefix+"CONTRATOSERVICOS_HMLSEMCNF";
         edtContratoServicos_PrazoAnalise_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOANALISE";
         edtavPrazotipo_Internalname = sPrefix+"vPRAZOTIPO";
         edtContratoServicos_Indicadores_Internalname = sPrefix+"CONTRATOSERVICOS_INDICADORES";
         chkContratoServicos_Ativo_Internalname = sPrefix+"CONTRATOSERVICOS_ATIVO";
         cmbServico_IsOrigemReferencia_Internalname = sPrefix+"SERVICO_ISORIGEMREFERENCIA";
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgClonar_Internalname = sPrefix+"CLONAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         Confirmpanel2_Internalname = sPrefix+"CONFIRMPANEL2";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicos_prazoanalise_Internalname = sPrefix+"vTFCONTRATOSERVICOS_PRAZOANALISE";
         edtavTfcontratoservicos_prazoanalise_to_Internalname = sPrefix+"vTFCONTRATOSERVICOS_PRAZOANALISE_TO";
         edtavTfcontratoservicos_indicadores_Internalname = sPrefix+"vTFCONTRATOSERVICOS_INDICADORES";
         edtavTfcontratoservicos_indicadores_to_Internalname = sPrefix+"vTFCONTRATOSERVICOS_INDICADORES_TO";
         edtavTfcontratoservicos_ativo_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOS_ATIVO_SEL";
         Ddo_contratoservicos_prazoanalise_Internalname = sPrefix+"DDO_CONTRATOSERVICOS_PRAZOANALISE";
         edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicos_indicadores_Internalname = sPrefix+"DDO_CONTRATOSERVICOS_INDICADORES";
         edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicos_ativo_Internalname = sPrefix+"DDO_CONTRATOSERVICOS_ATIVO";
         edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavAssociarsistema_Jsonclick = "";
         edtavAssociarsistema_Visible = -1;
         edtavAssociarsistema_Enabled = 1;
         cmbServico_IsOrigemReferencia_Jsonclick = "";
         edtContratoServicos_Indicadores_Jsonclick = "";
         edtavPrazotipo_Jsonclick = "";
         edtavPrazotipo_Visible = -1;
         edtContratoServicos_PrazoAnalise_Jsonclick = "";
         cmbContratoServicos_HmlSemCnf_Jsonclick = "";
         edtavServico_percentual_Jsonclick = "";
         edtavServico_percentual_Visible = -1;
         cmbServicoContrato_Faturamento_Jsonclick = "";
         edtServico_QtdContratada_Jsonclick = "";
         edtContratoServicos_QntUntCns_Jsonclick = "";
         edtContratoServicos_UndCntNome_Jsonclick = "";
         edtContratoServicos_UnidadeContratada_Jsonclick = "";
         edtServico_VlrUnidadeContratada_Jsonclick = "";
         edtContratoServicos_Alias_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Jsonclick = "";
         edtavApagar_Jsonclick = "";
         cmbavServico_ativo_Jsonclick = "";
         cmbavServico_tipohierarquia_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Hoveringcolor = (int)(0xEEFAEE);
         subGrid_Allowhovering = -1;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavAssociarsistema_Tooltiptext = "Clique aqui p/ associar os sistemas deste servi�o!";
         edtContratoServicos_Indicadores_Linktarget = "";
         edtContratoServicos_Indicadores_Link = "";
         edtContratoServicos_Indicadores_Forecolor = (int)(0xFFFFFF);
         edtavPrazotipo_Link = "";
         edtavPrazotipo_Enabled = 1;
         edtavPrazotipo_Forecolor = (int)(0xFFFFFF);
         edtContratoServicos_PrazoAnalise_Forecolor = (int)(0xFFFFFF);
         cmbContratoServicos_HmlSemCnf.ForeColor = (int)(0xFFFFFF);
         edtavServico_percentual_Enabled = 1;
         edtavServico_percentual_Forecolor = (int)(0xFFFFFF);
         cmbServicoContrato_Faturamento.ForeColor = (int)(0xFFFFFF);
         edtServico_QtdContratada_Forecolor = (int)(0xFFFFFF);
         edtServico_VlrUnidadeContratada_Forecolor = (int)(0xFFFFFF);
         edtContratoServicos_Alias_Forecolor = (int)(0xFFFFFF);
         edtServico_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavApagar_Tooltiptext = "";
         edtavApagar_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         cmbServico_IsOrigemReferencia_Titleformat = 0;
         chkContratoServicos_Ativo_Titleformat = 0;
         edtContratoServicos_Indicadores_Titleformat = 0;
         edtContratoServicos_PrazoAnalise_Titleformat = 0;
         edtavApagar_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgClonar_Enabled = 1;
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavAssociarsistema_Title = "";
         cmbServico_IsOrigemReferencia.Title.Text = "Exige Origem de Refer�ncia";
         chkContratoServicos_Ativo.Title.Text = "Ativo";
         edtContratoServicos_Indicadores_Title = "Indicadores";
         edtContratoServicos_PrazoAnalise_Title = "An�lise";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratoServicos_Ativo.Caption = "";
         edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicos_ativo_sel_Jsonclick = "";
         edtavTfcontratoservicos_ativo_sel_Visible = 1;
         edtavTfcontratoservicos_indicadores_to_Jsonclick = "";
         edtavTfcontratoservicos_indicadores_to_Visible = 1;
         edtavTfcontratoservicos_indicadores_Jsonclick = "";
         edtavTfcontratoservicos_indicadores_Visible = 1;
         edtavTfcontratoservicos_prazoanalise_to_Jsonclick = "";
         edtavTfcontratoservicos_prazoanalise_to_Visible = 1;
         edtavTfcontratoservicos_prazoanalise_Jsonclick = "";
         edtavTfcontratoservicos_prazoanalise_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Visible = 1;
         Ddo_contratoservicos_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicos_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicos_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicos_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicos_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratoservicos_ativo_Datalisttype = "FixedValues";
         Ddo_contratoservicos_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicos_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicos_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicos_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicos_ativo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicos_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicos_ativo_Cls = "ColumnSettings";
         Ddo_contratoservicos_ativo_Tooltip = "Op��es";
         Ddo_contratoservicos_ativo_Caption = "";
         Ddo_contratoservicos_indicadores_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicos_indicadores_Rangefilterto = "At�";
         Ddo_contratoservicos_indicadores_Rangefilterfrom = "Desde";
         Ddo_contratoservicos_indicadores_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicos_indicadores_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicos_indicadores_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicos_indicadores_Filtertype = "Numeric";
         Ddo_contratoservicos_indicadores_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicos_indicadores_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contratoservicos_indicadores_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace = "";
         Ddo_contratoservicos_indicadores_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicos_indicadores_Cls = "ColumnSettings";
         Ddo_contratoservicos_indicadores_Tooltip = "Op��es";
         Ddo_contratoservicos_indicadores_Caption = "";
         Ddo_contratoservicos_prazoanalise_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicos_prazoanalise_Rangefilterto = "At�";
         Ddo_contratoservicos_prazoanalise_Rangefilterfrom = "Desde";
         Ddo_contratoservicos_prazoanalise_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicos_prazoanalise_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicos_prazoanalise_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicos_prazoanalise_Filtertype = "Numeric";
         Ddo_contratoservicos_prazoanalise_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicos_prazoanalise_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contratoservicos_prazoanalise_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace = "";
         Ddo_contratoservicos_prazoanalise_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicos_prazoanalise_Cls = "ColumnSettings";
         Ddo_contratoservicos_prazoanalise_Tooltip = "Op��es";
         Ddo_contratoservicos_prazoanalise_Caption = "";
         Confirmpanel2_Yesbuttonposition = "left";
         Confirmpanel2_Nobuttoncaption = "Cancelar";
         Confirmpanel2_Yesbuttoncaption = "Apagar";
         Confirmpanel2_Confirmationtext = "Existem dados nas tabelas";
         Confirmpanel2_Title = "Confirma��o";
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Yesbuttoncaption = "Cadastrar";
         Confirmpanel_Confirmationtext = "Antes deve cadastrar as Unidades de Servi�o Contratadas (USC)";
         Confirmpanel_Title = "Aten��o";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0}],oparms:[{av:'AV52ContratoServicos_PrazoAnaliseTitleFilterData',fld:'vCONTRATOSERVICOS_PRAZOANALISETITLEFILTERDATA',pic:'',nv:null},{av:'AV56ContratoServicos_IndicadoresTitleFilterData',fld:'vCONTRATOSERVICOS_INDICADORESTITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContratoServicos_AtivoTitleFilterData',fld:'vCONTRATOSERVICOS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoServicos_PrazoAnalise_Titleformat',ctrl:'CONTRATOSERVICOS_PRAZOANALISE',prop:'Titleformat'},{av:'edtContratoServicos_PrazoAnalise_Title',ctrl:'CONTRATOSERVICOS_PRAZOANALISE',prop:'Title'},{av:'edtContratoServicos_Indicadores_Titleformat',ctrl:'CONTRATOSERVICOS_INDICADORES',prop:'Titleformat'},{av:'edtContratoServicos_Indicadores_Title',ctrl:'CONTRATOSERVICOS_INDICADORES',prop:'Title'},{av:'chkContratoServicos_Ativo_Titleformat',ctrl:'CONTRATOSERVICOS_ATIVO',prop:'Titleformat'},{av:'chkContratoServicos_Ativo.Title.Text',ctrl:'CONTRATOSERVICOS_ATIVO',prop:'Title'},{av:'cmbServico_IsOrigemReferencia'},{av:'AV62GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV63GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'imgClonar_Enabled',ctrl:'CLONAR',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOS_PRAZOANALISE.ONOPTIONCLICKED","{handler:'E14IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicos_prazoanalise_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOS_PRAZOANALISE',prop:'ActiveEventKey'},{av:'Ddo_contratoservicos_prazoanalise_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOS_PRAZOANALISE',prop:'FilteredText_get'},{av:'Ddo_contratoservicos_prazoanalise_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOS_PRAZOANALISE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_CONTRATOSERVICOS_INDICADORES.ONOPTIONCLICKED","{handler:'E15IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicos_indicadores_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOS_INDICADORES',prop:'ActiveEventKey'},{av:'Ddo_contratoservicos_indicadores_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOS_INDICADORES',prop:'FilteredText_get'},{av:'Ddo_contratoservicos_indicadores_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOS_INDICADORES',prop:'FilteredTextTo_get'}],oparms:[{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_CONTRATOSERVICOS_ATIVO.ONOPTIONCLICKED","{handler:'E16IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicos_ativo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicos_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicos_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_ATIVO',prop:'SortedStatus'},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E21IR2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV14Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavApagar_Tooltiptext',ctrl:'vAPAGAR',prop:'Tooltiptext'},{av:'AV73Apagar',fld:'vAPAGAR',pic:'',nv:''},{av:'edtavApagar_Enabled',ctrl:'vAPAGAR',prop:'Enabled'},{av:'AV15Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV17AssociarSistema',fld:'vASSOCIARSISTEMA',pic:'',nv:''},{av:'edtavAssociarsistema_Tooltiptext',ctrl:'vASSOCIARSISTEMA',prop:'Tooltiptext'},{av:'edtavPrazotipo_Link',ctrl:'vPRAZOTIPO',prop:'Link'},{av:'edtContratoServicos_Indicadores_Link',ctrl:'CONTRATOSERVICOS_INDICADORES',prop:'Link'},{av:'edtServico_Nome_Forecolor',ctrl:'SERVICO_NOME',prop:'Forecolor'},{av:'edtContratoServicos_Alias_Forecolor',ctrl:'CONTRATOSERVICOS_ALIAS',prop:'Forecolor'},{av:'edtServico_VlrUnidadeContratada_Forecolor',ctrl:'SERVICO_VLRUNIDADECONTRATADA',prop:'Forecolor'},{av:'edtServico_QtdContratada_Forecolor',ctrl:'SERVICO_QTDCONTRATADA',prop:'Forecolor'},{av:'cmbServicoContrato_Faturamento'},{av:'edtavServico_percentual_Forecolor',ctrl:'vSERVICO_PERCENTUAL',prop:'Forecolor'},{av:'cmbContratoServicos_HmlSemCnf'},{av:'edtContratoServicos_PrazoAnalise_Forecolor',ctrl:'CONTRATOSERVICOS_PRAZOANALISE',prop:'Forecolor'},{av:'edtavPrazotipo_Forecolor',ctrl:'vPRAZOTIPO',prop:'Forecolor'},{av:'edtContratoServicos_Indicadores_Forecolor',ctrl:'CONTRATOSERVICOS_INDICADORES',prop:'Forecolor'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtContratoServicos_Indicadores_Linktarget',ctrl:'CONTRATOSERVICOS_INDICADORES',prop:'Linktarget'},{av:'AV18Servico_Percentual',fld:'vSERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'AV19PrazoTipo',fld:'vPRAZOTIPO',pic:'@!',nv:''}]}");
         setEventMetadata("'DOINSERT'","{handler:'E17IR2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20TemUSCCadastradas',fld:'vTEMUSCCADASTRADAS',pic:'',nv:false},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOCLONAR'","{handler:'E18IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARSISTEMA'","{handler:'E23IR2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VAPAGAR.CLICK","{handler:'E22IR2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A915ContratoSrvVnc_CntSrvCod',fld:'CONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1471ContratoServicosCusto_CntSrvCod',fld:'CONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A2069ContratoServicosRmn_Sequencial',fld:'CONTRATOSERVICOSRMN_SEQUENCIAL',pic:'ZZZ9',nv:0}],oparms:[{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'}]}");
         setEventMetadata("GRID.ONLINEACTIVATE","{handler:'E24IR2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E12IR2',iparms:[{av:'AV20TemUSCCadastradas',fld:'vTEMUSCCADASTRADAS',pic:'',nv:false},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL2.ONYES","{handler:'E13IR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV68OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV21OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53TFContratoServicos_PrazoAnalise',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'AV54TFContratoServicos_PrazoAnalise_To',fld:'vTFCONTRATOSERVICOS_PRAZOANALISE_TO',pic:'ZZZ9',nv:0},{av:'AV57TFContratoServicos_Indicadores',fld:'vTFCONTRATOSERVICOS_INDICADORES',pic:'ZZZ9',nv:0},{av:'AV58TFContratoServicos_Indicadores_To',fld:'vTFCONTRATOSERVICOS_INDICADORES_TO',pic:'ZZZ9',nv:0},{av:'AV70TFContratoServicos_Ativo_Sel',fld:'vTFCONTRATOSERVICOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_PRAZOANALISETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_INDICADORESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV24Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV13Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',hsh:true,nv:false},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A914ContratoServicos_PrazoDias',fld:'CONTRATOSERVICOS_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicos_prazoanalise_Activeeventkey = "";
         Ddo_contratoservicos_prazoanalise_Filteredtext_get = "";
         Ddo_contratoservicos_prazoanalise_Filteredtextto_get = "";
         Ddo_contratoservicos_indicadores_Activeeventkey = "";
         Ddo_contratoservicos_indicadores_Filteredtext_get = "";
         Ddo_contratoservicos_indicadores_Filteredtextto_get = "";
         Ddo_contratoservicos_ativo_Activeeventkey = "";
         Ddo_contratoservicos_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace = "";
         AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace = "";
         AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV97Pgmname = "";
         AV24Servico_TipoHierarquia = 1;
         AV13Servico_Ativo = true;
         A913ContratoServicos_PrazoTipo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV60DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV52ContratoServicos_PrazoAnaliseTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56ContratoServicos_IndicadoresTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoServicos_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicos_prazoanalise_Filteredtext_set = "";
         Ddo_contratoservicos_prazoanalise_Filteredtextto_set = "";
         Ddo_contratoservicos_indicadores_Filteredtext_set = "";
         Ddo_contratoservicos_indicadores_Filteredtextto_set = "";
         Ddo_contratoservicos_ativo_Selectedvalue_set = "";
         Ddo_contratoservicos_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Update = "";
         AV92Update_GXI = "";
         AV73Apagar = "";
         AV93Apagar_GXI = "";
         AV15Display = "";
         AV94Display_GXI = "";
         A608Servico_Nome = "";
         A1858ContratoServicos_Alias = "";
         A2132ContratoServicos_UndCntNome = "";
         A607ServicoContrato_Faturamento = "";
         AV19PrazoTipo = "";
         AV17AssociarSistema = "";
         AV95Associarsistema_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00IR4_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00IR4_A1530Servico_TipoHierarquia = new short[1] ;
         H00IR4_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00IR4_A632Servico_Ativo = new bool[] {false} ;
         H00IR4_A558Servico_Percentual = new decimal[1] ;
         H00IR4_n558Servico_Percentual = new bool[] {false} ;
         H00IR4_A74Contrato_Codigo = new int[1] ;
         H00IR4_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         H00IR4_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IR4_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         H00IR4_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         H00IR4_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         H00IR4_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         H00IR4_A607ServicoContrato_Faturamento = new String[] {""} ;
         H00IR4_A555Servico_QtdContratada = new long[1] ;
         H00IR4_n555Servico_QtdContratada = new bool[] {false} ;
         H00IR4_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         H00IR4_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         H00IR4_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         H00IR4_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         H00IR4_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         H00IR4_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         H00IR4_A1858ContratoServicos_Alias = new String[] {""} ;
         H00IR4_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00IR4_A608Servico_Nome = new String[] {""} ;
         H00IR4_A155Servico_Codigo = new int[1] ;
         H00IR4_A911ContratoServicos_Prazos = new short[1] ;
         H00IR4_n911ContratoServicos_Prazos = new bool[] {false} ;
         H00IR4_A914ContratoServicos_PrazoDias = new short[1] ;
         H00IR4_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         H00IR4_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00IR4_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         H00IR4_A160ContratoServicos_Codigo = new int[1] ;
         H00IR4_A1377ContratoServicos_Indicadores = new short[1] ;
         H00IR4_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         H00IR7_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         H00IR8_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00IR8_A1207ContratoUnidades_ContratoCod = new int[1] ;
         imgInsert_Link = "";
         AV16Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV75Tabelas = "";
         H00IR9_A160ContratoServicos_Codigo = new int[1] ;
         H00IR10_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00IR11_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00IR11_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00IR12_A938ContratoServicosTelas_Sequencial = new short[1] ;
         H00IR12_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         H00IR13_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00IR13_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00IR14_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00IR14_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00IR15_A1473ContratoServicosCusto_Codigo = new int[1] ;
         H00IR15_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         H00IR16_A1749Artefatos_Codigo = new int[1] ;
         H00IR16_A160ContratoServicos_Codigo = new int[1] ;
         H00IR17_A160ContratoServicos_Codigo = new int[1] ;
         H00IR17_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         sStyleString = "";
         imgInsert_Jsonclick = "";
         imgClonar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextservico_tipohierarquia_Jsonclick = "";
         lblFiltertextservico_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contrato_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratocontratoservicoswc__default(),
            new Object[][] {
                new Object[] {
               H00IR4_A903ContratoServicosPrazo_CntSrvCod, H00IR4_A1530Servico_TipoHierarquia, H00IR4_n1530Servico_TipoHierarquia, H00IR4_A632Servico_Ativo, H00IR4_A558Servico_Percentual, H00IR4_n558Servico_Percentual, H00IR4_A74Contrato_Codigo, H00IR4_A2092Servico_IsOrigemReferencia, H00IR4_A638ContratoServicos_Ativo, H00IR4_A1152ContratoServicos_PrazoAnalise,
               H00IR4_n1152ContratoServicos_PrazoAnalise, H00IR4_A888ContratoServicos_HmlSemCnf, H00IR4_n888ContratoServicos_HmlSemCnf, H00IR4_A607ServicoContrato_Faturamento, H00IR4_A555Servico_QtdContratada, H00IR4_n555Servico_QtdContratada, H00IR4_A1340ContratoServicos_QntUntCns, H00IR4_n1340ContratoServicos_QntUntCns, H00IR4_A2132ContratoServicos_UndCntNome, H00IR4_n2132ContratoServicos_UndCntNome,
               H00IR4_A1212ContratoServicos_UnidadeContratada, H00IR4_A557Servico_VlrUnidadeContratada, H00IR4_A1858ContratoServicos_Alias, H00IR4_n1858ContratoServicos_Alias, H00IR4_A608Servico_Nome, H00IR4_A155Servico_Codigo, H00IR4_A911ContratoServicos_Prazos, H00IR4_n911ContratoServicos_Prazos, H00IR4_A914ContratoServicos_PrazoDias, H00IR4_n914ContratoServicos_PrazoDias,
               H00IR4_A913ContratoServicos_PrazoTipo, H00IR4_n913ContratoServicos_PrazoTipo, H00IR4_A160ContratoServicos_Codigo, H00IR4_A1377ContratoServicos_Indicadores, H00IR4_n1377ContratoServicos_Indicadores
               }
               , new Object[] {
               H00IR7_AGRID_nRecordCount
               }
               , new Object[] {
               H00IR8_A1204ContratoUnidades_UndMedCod, H00IR8_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               H00IR9_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00IR10_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               H00IR11_A917ContratoSrvVnc_Codigo, H00IR11_A915ContratoSrvVnc_CntSrvCod
               }
               , new Object[] {
               H00IR12_A938ContratoServicosTelas_Sequencial, H00IR12_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               H00IR13_A1336ContratoServicosPrioridade_Codigo, H00IR13_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               H00IR14_A1269ContratoServicosIndicador_Codigo, H00IR14_A1270ContratoServicosIndicador_CntSrvCod
               }
               , new Object[] {
               H00IR15_A1473ContratoServicosCusto_Codigo, H00IR15_A1471ContratoServicosCusto_CntSrvCod
               }
               , new Object[] {
               H00IR16_A1749Artefatos_Codigo, H00IR16_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00IR17_A160ContratoServicos_Codigo, H00IR17_A2069ContratoServicosRmn_Sequencial
               }
            }
         );
         AV97Pgmname = "ContratoContratoServicosWC";
         /* GeneXus formulas. */
         AV97Pgmname = "ContratoContratoServicosWC";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
         edtavPrazotipo_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_21 ;
      private short nGXsfl_21_idx=1 ;
      private short AV68OrderedBy ;
      private short AV53TFContratoServicos_PrazoAnalise ;
      private short AV54TFContratoServicos_PrazoAnalise_To ;
      private short AV57TFContratoServicos_Indicadores ;
      private short AV58TFContratoServicos_Indicadores_To ;
      private short AV70TFContratoServicos_Ativo_Sel ;
      private short AV24Servico_TipoHierarquia ;
      private short A911ContratoServicos_Prazos ;
      private short A914ContratoServicos_PrazoDias ;
      private short initialized ;
      private short GRID_nEOF ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1377ContratoServicos_Indicadores ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_21_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short A1530Servico_TipoHierarquia ;
      private short edtContratoServicos_PrazoAnalise_Titleformat ;
      private short edtContratoServicos_Indicadores_Titleformat ;
      private short chkContratoServicos_Ativo_Titleformat ;
      private short cmbServico_IsOrigemReferencia_Titleformat ;
      private short AV96GXLvl205 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contrato_Codigo ;
      private int wcpOAV7Contrato_Codigo ;
      private int subGrid_Rows ;
      private int A160ContratoServicos_Codigo ;
      private int edtavServico_percentual_Enabled ;
      private int edtavPrazotipo_Enabled ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int AV22ContratoServicos_Codigo ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtContrato_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicos_prazoanalise_Visible ;
      private int edtavTfcontratoservicos_prazoanalise_to_Visible ;
      private int edtavTfcontratoservicos_indicadores_Visible ;
      private int edtavTfcontratoservicos_indicadores_to_Visible ;
      private int edtavTfcontratoservicos_ativo_sel_Visible ;
      private int edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Visible ;
      private int A155Servico_Codigo ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV61PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavApagar_Enabled ;
      private int edtServico_Nome_Forecolor ;
      private int edtContratoServicos_Alias_Forecolor ;
      private int edtServico_VlrUnidadeContratada_Forecolor ;
      private int edtServico_QtdContratada_Forecolor ;
      private int edtavServico_percentual_Forecolor ;
      private int edtContratoServicos_PrazoAnalise_Forecolor ;
      private int edtavPrazotipo_Forecolor ;
      private int edtContratoServicos_Indicadores_Forecolor ;
      private int edtavApagar_Visible ;
      private int imgInsert_Enabled ;
      private int imgClonar_Enabled ;
      private int AV98GXV1 ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavServico_percentual_Visible ;
      private int edtavPrazotipo_Visible ;
      private int edtavAssociarsistema_Enabled ;
      private int edtavAssociarsistema_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV62GridCurrentPage ;
      private long AV63GridPageCount ;
      private long A555Servico_QtdContratada ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV72Color ;
      private decimal A558Servico_Percentual ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal AV18Servico_Percentual ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicos_prazoanalise_Activeeventkey ;
      private String Ddo_contratoservicos_prazoanalise_Filteredtext_get ;
      private String Ddo_contratoservicos_prazoanalise_Filteredtextto_get ;
      private String Ddo_contratoservicos_indicadores_Activeeventkey ;
      private String Ddo_contratoservicos_indicadores_Filteredtext_get ;
      private String Ddo_contratoservicos_indicadores_Filteredtextto_get ;
      private String Ddo_contratoservicos_ativo_Activeeventkey ;
      private String Ddo_contratoservicos_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_21_idx="0001" ;
      private String AV97Pgmname ;
      private String A913ContratoServicos_PrazoTipo ;
      private String GXKey ;
      private String edtavServico_percentual_Internalname ;
      private String edtavPrazotipo_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Confirmtype ;
      private String Confirmpanel2_Title ;
      private String Confirmpanel2_Confirmationtext ;
      private String Confirmpanel2_Yesbuttoncaption ;
      private String Confirmpanel2_Nobuttoncaption ;
      private String Confirmpanel2_Yesbuttonposition ;
      private String Ddo_contratoservicos_prazoanalise_Caption ;
      private String Ddo_contratoservicos_prazoanalise_Tooltip ;
      private String Ddo_contratoservicos_prazoanalise_Cls ;
      private String Ddo_contratoservicos_prazoanalise_Filteredtext_set ;
      private String Ddo_contratoservicos_prazoanalise_Filteredtextto_set ;
      private String Ddo_contratoservicos_prazoanalise_Dropdownoptionstype ;
      private String Ddo_contratoservicos_prazoanalise_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicos_prazoanalise_Filtertype ;
      private String Ddo_contratoservicos_prazoanalise_Cleanfilter ;
      private String Ddo_contratoservicos_prazoanalise_Rangefilterfrom ;
      private String Ddo_contratoservicos_prazoanalise_Rangefilterto ;
      private String Ddo_contratoservicos_prazoanalise_Searchbuttontext ;
      private String Ddo_contratoservicos_indicadores_Caption ;
      private String Ddo_contratoservicos_indicadores_Tooltip ;
      private String Ddo_contratoservicos_indicadores_Cls ;
      private String Ddo_contratoservicos_indicadores_Filteredtext_set ;
      private String Ddo_contratoservicos_indicadores_Filteredtextto_set ;
      private String Ddo_contratoservicos_indicadores_Dropdownoptionstype ;
      private String Ddo_contratoservicos_indicadores_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicos_indicadores_Filtertype ;
      private String Ddo_contratoservicos_indicadores_Cleanfilter ;
      private String Ddo_contratoservicos_indicadores_Rangefilterfrom ;
      private String Ddo_contratoservicos_indicadores_Rangefilterto ;
      private String Ddo_contratoservicos_indicadores_Searchbuttontext ;
      private String Ddo_contratoservicos_ativo_Caption ;
      private String Ddo_contratoservicos_ativo_Tooltip ;
      private String Ddo_contratoservicos_ativo_Cls ;
      private String Ddo_contratoservicos_ativo_Selectedvalue_set ;
      private String Ddo_contratoservicos_ativo_Dropdownoptionstype ;
      private String Ddo_contratoservicos_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicos_ativo_Sortedstatus ;
      private String Ddo_contratoservicos_ativo_Datalisttype ;
      private String Ddo_contratoservicos_ativo_Datalistfixedvalues ;
      private String Ddo_contratoservicos_ativo_Sortasc ;
      private String Ddo_contratoservicos_ativo_Sortdsc ;
      private String Ddo_contratoservicos_ativo_Cleanfilter ;
      private String Ddo_contratoservicos_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicos_prazoanalise_Internalname ;
      private String edtavTfcontratoservicos_prazoanalise_Jsonclick ;
      private String edtavTfcontratoservicos_prazoanalise_to_Internalname ;
      private String edtavTfcontratoservicos_prazoanalise_to_Jsonclick ;
      private String edtavTfcontratoservicos_indicadores_Internalname ;
      private String edtavTfcontratoservicos_indicadores_Jsonclick ;
      private String edtavTfcontratoservicos_indicadores_to_Internalname ;
      private String edtavTfcontratoservicos_indicadores_to_Jsonclick ;
      private String edtavTfcontratoservicos_ativo_sel_Internalname ;
      private String edtavTfcontratoservicos_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicos_prazoanalisetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicos_indicadorestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicos_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavApagar_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String A1858ContratoServicos_Alias ;
      private String edtContratoServicos_Alias_Internalname ;
      private String edtServico_VlrUnidadeContratada_Internalname ;
      private String edtContratoServicos_UnidadeContratada_Internalname ;
      private String A2132ContratoServicos_UndCntNome ;
      private String edtContratoServicos_UndCntNome_Internalname ;
      private String edtContratoServicos_QntUntCns_Internalname ;
      private String edtServico_QtdContratada_Internalname ;
      private String cmbServicoContrato_Faturamento_Internalname ;
      private String cmbContratoServicos_HmlSemCnf_Internalname ;
      private String edtContratoServicos_PrazoAnalise_Internalname ;
      private String AV19PrazoTipo ;
      private String edtContratoServicos_Indicadores_Internalname ;
      private String chkContratoServicos_Ativo_Internalname ;
      private String cmbServico_IsOrigemReferencia_Internalname ;
      private String edtavAssociarsistema_Internalname ;
      private String GXCCtl ;
      private String cmbavServico_tipohierarquia_Internalname ;
      private String scmdbuf ;
      private String cmbavServico_ativo_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicos_prazoanalise_Internalname ;
      private String Ddo_contratoservicos_indicadores_Internalname ;
      private String Ddo_contratoservicos_ativo_Internalname ;
      private String edtContratoServicos_PrazoAnalise_Title ;
      private String edtContratoServicos_Indicadores_Title ;
      private String edtavAssociarsistema_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavApagar_Tooltiptext ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavAssociarsistema_Tooltiptext ;
      private String edtavPrazotipo_Link ;
      private String edtContratoServicos_Indicadores_Link ;
      private String edtContratoServicos_Indicadores_Linktarget ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String imgClonar_Internalname ;
      private String AV75Tabelas ;
      private String Confirmpanel2_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgClonar_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblFiltertextservico_tipohierarquia_Internalname ;
      private String lblFiltertextservico_tipohierarquia_Jsonclick ;
      private String cmbavServico_tipohierarquia_Jsonclick ;
      private String lblFiltertextservico_ativo_Internalname ;
      private String lblFiltertextservico_ativo_Jsonclick ;
      private String cmbavServico_ativo_Jsonclick ;
      private String sCtrlAV7Contrato_Codigo ;
      private String sGXsfl_21_fel_idx="0001" ;
      private String edtavApagar_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtContratoServicos_Alias_Jsonclick ;
      private String edtServico_VlrUnidadeContratada_Jsonclick ;
      private String edtContratoServicos_UnidadeContratada_Jsonclick ;
      private String edtContratoServicos_UndCntNome_Jsonclick ;
      private String edtContratoServicos_QntUntCns_Jsonclick ;
      private String edtServico_QtdContratada_Jsonclick ;
      private String cmbServicoContrato_Faturamento_Jsonclick ;
      private String edtavServico_percentual_Jsonclick ;
      private String cmbContratoServicos_HmlSemCnf_Jsonclick ;
      private String edtContratoServicos_PrazoAnalise_Jsonclick ;
      private String edtavPrazotipo_Jsonclick ;
      private String edtContratoServicos_Indicadores_Jsonclick ;
      private String cmbServico_IsOrigemReferencia_Jsonclick ;
      private String edtavAssociarsistema_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Confirmpanel_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV21OrderedDsc ;
      private bool AV13Servico_Ativo ;
      private bool n911ContratoServicos_Prazos ;
      private bool A638ContratoServicos_Ativo ;
      private bool n558Servico_Percentual ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool n914ContratoServicos_PrazoDias ;
      private bool toggleJsOutput ;
      private bool AV20TemUSCCadastradas ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicos_prazoanalise_Includesortasc ;
      private bool Ddo_contratoservicos_prazoanalise_Includesortdsc ;
      private bool Ddo_contratoservicos_prazoanalise_Includefilter ;
      private bool Ddo_contratoservicos_prazoanalise_Filterisrange ;
      private bool Ddo_contratoservicos_prazoanalise_Includedatalist ;
      private bool Ddo_contratoservicos_indicadores_Includesortasc ;
      private bool Ddo_contratoservicos_indicadores_Includesortdsc ;
      private bool Ddo_contratoservicos_indicadores_Includefilter ;
      private bool Ddo_contratoservicos_indicadores_Filterisrange ;
      private bool Ddo_contratoservicos_indicadores_Includedatalist ;
      private bool Ddo_contratoservicos_ativo_Includesortasc ;
      private bool Ddo_contratoservicos_ativo_Includesortdsc ;
      private bool Ddo_contratoservicos_ativo_Includefilter ;
      private bool Ddo_contratoservicos_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1858ContratoServicos_Alias ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n555Servico_QtdContratada ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1377ContratoServicos_Indicadores ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A632Servico_Ativo ;
      private bool n1530Servico_TipoHierarquia ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool GXt_boolean2 ;
      private bool AV14Update_IsBlob ;
      private bool AV73Apagar_IsBlob ;
      private bool AV15Display_IsBlob ;
      private bool AV17AssociarSistema_IsBlob ;
      private String AV55ddo_ContratoServicos_PrazoAnaliseTitleControlIdToReplace ;
      private String AV59ddo_ContratoServicos_IndicadoresTitleControlIdToReplace ;
      private String AV71ddo_ContratoServicos_AtivoTitleControlIdToReplace ;
      private String AV92Update_GXI ;
      private String AV93Apagar_GXI ;
      private String AV94Display_GXI ;
      private String A607ServicoContrato_Faturamento ;
      private String AV95Associarsistema_GXI ;
      private String AV14Update ;
      private String AV73Apagar ;
      private String AV15Display ;
      private String AV17AssociarSistema ;
      private String imgInsert_Bitmap ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavServico_tipohierarquia ;
      private GXCombobox cmbavServico_ativo ;
      private GXCombobox cmbServicoContrato_Faturamento ;
      private GXCombobox cmbContratoServicos_HmlSemCnf ;
      private GXCheckbox chkContratoServicos_Ativo ;
      private GXCombobox cmbServico_IsOrigemReferencia ;
      private IDataStoreProvider pr_default ;
      private int[] H00IR4_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] H00IR4_A1530Servico_TipoHierarquia ;
      private bool[] H00IR4_n1530Servico_TipoHierarquia ;
      private bool[] H00IR4_A632Servico_Ativo ;
      private decimal[] H00IR4_A558Servico_Percentual ;
      private bool[] H00IR4_n558Servico_Percentual ;
      private int[] H00IR4_A74Contrato_Codigo ;
      private bool[] H00IR4_A2092Servico_IsOrigemReferencia ;
      private bool[] H00IR4_A638ContratoServicos_Ativo ;
      private short[] H00IR4_A1152ContratoServicos_PrazoAnalise ;
      private bool[] H00IR4_n1152ContratoServicos_PrazoAnalise ;
      private bool[] H00IR4_A888ContratoServicos_HmlSemCnf ;
      private bool[] H00IR4_n888ContratoServicos_HmlSemCnf ;
      private String[] H00IR4_A607ServicoContrato_Faturamento ;
      private long[] H00IR4_A555Servico_QtdContratada ;
      private bool[] H00IR4_n555Servico_QtdContratada ;
      private decimal[] H00IR4_A1340ContratoServicos_QntUntCns ;
      private bool[] H00IR4_n1340ContratoServicos_QntUntCns ;
      private String[] H00IR4_A2132ContratoServicos_UndCntNome ;
      private bool[] H00IR4_n2132ContratoServicos_UndCntNome ;
      private int[] H00IR4_A1212ContratoServicos_UnidadeContratada ;
      private decimal[] H00IR4_A557Servico_VlrUnidadeContratada ;
      private String[] H00IR4_A1858ContratoServicos_Alias ;
      private bool[] H00IR4_n1858ContratoServicos_Alias ;
      private String[] H00IR4_A608Servico_Nome ;
      private int[] H00IR4_A155Servico_Codigo ;
      private short[] H00IR4_A911ContratoServicos_Prazos ;
      private bool[] H00IR4_n911ContratoServicos_Prazos ;
      private short[] H00IR4_A914ContratoServicos_PrazoDias ;
      private bool[] H00IR4_n914ContratoServicos_PrazoDias ;
      private String[] H00IR4_A913ContratoServicos_PrazoTipo ;
      private bool[] H00IR4_n913ContratoServicos_PrazoTipo ;
      private int[] H00IR4_A160ContratoServicos_Codigo ;
      private short[] H00IR4_A1377ContratoServicos_Indicadores ;
      private bool[] H00IR4_n1377ContratoServicos_Indicadores ;
      private long[] H00IR7_AGRID_nRecordCount ;
      private int[] H00IR8_A1204ContratoUnidades_UndMedCod ;
      private int[] H00IR8_A1207ContratoUnidades_ContratoCod ;
      private int[] H00IR9_A160ContratoServicos_Codigo ;
      private int[] H00IR10_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] H00IR11_A917ContratoSrvVnc_Codigo ;
      private int[] H00IR11_A915ContratoSrvVnc_CntSrvCod ;
      private short[] H00IR12_A938ContratoServicosTelas_Sequencial ;
      private int[] H00IR12_A926ContratoServicosTelas_ContratoCod ;
      private int[] H00IR13_A1336ContratoServicosPrioridade_Codigo ;
      private int[] H00IR13_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] H00IR14_A1269ContratoServicosIndicador_Codigo ;
      private int[] H00IR14_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] H00IR15_A1473ContratoServicosCusto_Codigo ;
      private int[] H00IR15_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] H00IR16_A1749Artefatos_Codigo ;
      private int[] H00IR16_A160ContratoServicos_Codigo ;
      private int[] H00IR17_A160ContratoServicos_Codigo ;
      private short[] H00IR17_A2069ContratoServicosRmn_Sequencial ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoServicos_PrazoAnaliseTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56ContratoServicos_IndicadoresTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContratoServicos_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV60DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratocontratoservicoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IR4( IGxContext context ,
                                             short AV53TFContratoServicos_PrazoAnalise ,
                                             short AV54TFContratoServicos_PrazoAnalise_To ,
                                             short AV70TFContratoServicos_Ativo_Sel ,
                                             short A1152ContratoServicos_PrazoAnalise ,
                                             bool A638ContratoServicos_Ativo ,
                                             short AV68OrderedBy ,
                                             bool AV21OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo ,
                                             short A1530Servico_TipoHierarquia ,
                                             bool A632Servico_Ativo ,
                                             short AV57TFContratoServicos_Indicadores ,
                                             short A1377ContratoServicos_Indicadores ,
                                             short AV58TFContratoServicos_Indicadores_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T5.[ContratoServicosPrazo_CntSrvCod], T3.[Servico_TipoHierarquia], T3.[Servico_Ativo], T1.[Servico_Percentual], T1.[Contrato_Codigo], T3.[Servico_IsOrigemReferencia], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_HmlSemCnf], T1.[ServicoContrato_Faturamento], T1.[Servico_QtdContratada], T1.[ContratoServicos_QntUntCns], T2.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T1.[Servico_VlrUnidadeContratada], T1.[ContratoServicos_Alias], T3.[Servico_Nome], T1.[Servico_Codigo], COALESCE( T4.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos, COALESCE( T5.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T5.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, T1.[ContratoServicos_Codigo], COALESCE( T6.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores";
         sFromString = " FROM ((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Prazos, T8.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T7 WITH (NOLOCK),  [ContratoServicos] T8 WITH (NOLOCK) WHERE T7.[ContratoServicosPrazo_CntSrvCod] = T8.[ContratoServicos_Codigo] GROUP BY T8.[ContratoServicos_Codigo] ) T4 ON T4.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) LEFT JOIN [ContratoServicosPrazo] T5 WITH (NOLOCK) ON T5.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T6 ON T6.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         sWhereString = sWhereString + " and (T3.[Servico_TipoHierarquia] = 1)";
         sWhereString = sWhereString + " and (T3.[Servico_Ativo] = 1)";
         sWhereString = sWhereString + " and ((@AV57TFContratoServicos_Indicadores = convert(int, 0)) or ( COALESCE( T6.[ContratoServicos_Indicadores], 0) >= @AV57TFContratoServicos_Indicadores))";
         sWhereString = sWhereString + " and ((@AV58TFContratoServicos_Indicadores_To = convert(int, 0)) or ( COALESCE( T6.[ContratoServicos_Indicadores], 0) <= @AV58TFContratoServicos_Indicadores_To))";
         if ( ! (0==AV53TFContratoServicos_PrazoAnalise) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] >= @AV53TFContratoServicos_PrazoAnalise)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV54TFContratoServicos_PrazoAnalise_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] <= @AV54TFContratoServicos_PrazoAnalise_To)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV70TFContratoServicos_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 1)";
         }
         if ( AV70TFContratoServicos_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 0)";
         }
         if ( ( AV68OrderedBy == 1 ) && ! AV21OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[ContratoServicos_Ativo]";
         }
         else if ( ( AV68OrderedBy == 1 ) && ( AV21OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[ContratoServicos_Ativo] DESC";
         }
         else if ( ( AV68OrderedBy == 2 ) && ! AV21OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T3.[Servico_IsOrigemReferencia]";
         }
         else if ( ( AV68OrderedBy == 2 ) && ( AV21OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T3.[Servico_IsOrigemReferencia] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00IR7( IGxContext context ,
                                             short AV53TFContratoServicos_PrazoAnalise ,
                                             short AV54TFContratoServicos_PrazoAnalise_To ,
                                             short AV70TFContratoServicos_Ativo_Sel ,
                                             short A1152ContratoServicos_PrazoAnalise ,
                                             bool A638ContratoServicos_Ativo ,
                                             short AV68OrderedBy ,
                                             bool AV21OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo ,
                                             short A1530Servico_TipoHierarquia ,
                                             bool A632Servico_Ativo ,
                                             short AV57TFContratoServicos_Indicadores ,
                                             short A1377ContratoServicos_Indicadores ,
                                             short AV58TFContratoServicos_Indicadores_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [7] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Prazos, T8.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T7 WITH (NOLOCK),  [ContratoServicos] T8 WITH (NOLOCK) WHERE T7.[ContratoServicosPrazo_CntSrvCod] = T8.[ContratoServicos_Codigo] GROUP BY T8.[ContratoServicos_Codigo] ) T4 ON T4.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) LEFT JOIN [ContratoServicosPrazo] T5 WITH (NOLOCK) ON T5.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T6 ON T6.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Servico_TipoHierarquia] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Servico_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ((@AV57TFContratoServicos_Indicadores = convert(int, 0)) or ( COALESCE( T6.[ContratoServicos_Indicadores], 0) >= @AV57TFContratoServicos_Indicadores))";
         scmdbuf = scmdbuf + " and ((@AV58TFContratoServicos_Indicadores_To = convert(int, 0)) or ( COALESCE( T6.[ContratoServicos_Indicadores], 0) <= @AV58TFContratoServicos_Indicadores_To))";
         if ( ! (0==AV53TFContratoServicos_PrazoAnalise) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] >= @AV53TFContratoServicos_PrazoAnalise)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV54TFContratoServicos_PrazoAnalise_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] <= @AV54TFContratoServicos_PrazoAnalise_To)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV70TFContratoServicos_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 1)";
         }
         if ( AV70TFContratoServicos_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV68OrderedBy == 1 ) && ! AV21OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV68OrderedBy == 1 ) && ( AV21OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV68OrderedBy == 2 ) && ! AV21OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV68OrderedBy == 2 ) && ( AV21OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IR4(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (short)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (short)dynConstraints[5] , (bool)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] );
               case 1 :
                     return conditional_H00IR7(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (short)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (short)dynConstraints[5] , (bool)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IR8 ;
          prmH00IR8 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR9 ;
          prmH00IR9 = new Object[] {
          new Object[] {"@AV22ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR10 ;
          prmH00IR10 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR11 ;
          prmH00IR11 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR12 ;
          prmH00IR12 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR13 ;
          prmH00IR13 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR14 ;
          prmH00IR14 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR15 ;
          prmH00IR15 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR16 ;
          prmH00IR16 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR17 ;
          prmH00IR17 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IR4 ;
          prmH00IR4 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV57TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV58TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV58TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV53TFContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54TFContratoServicos_PrazoAnalise_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IR7 ;
          prmH00IR7 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV57TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV58TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV58TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV53TFContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54TFContratoServicos_PrazoAnalise_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IR4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR4,11,0,true,false )
             ,new CursorDef("H00IR7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR7,1,0,true,false )
             ,new CursorDef("H00IR8", "SELECT TOP 1 [ContratoUnidades_UndMedCod], [ContratoUnidades_ContratoCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @AV7Contrato_Codigo ORDER BY [ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR8,1,0,false,true )
             ,new CursorDef("H00IR9", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV22ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR9,1,0,true,true )
             ,new CursorDef("H00IR10", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR10,1,0,false,true )
             ,new CursorDef("H00IR11", "SELECT TOP 1 [ContratoSrvVnc_Codigo], [ContratoSrvVnc_CntSrvCod] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR11,1,0,false,true )
             ,new CursorDef("H00IR12", "SELECT TOP 1 [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_ContratoCod] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosTelas_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR12,1,0,false,true )
             ,new CursorDef("H00IR13", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_CntSrvCod] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR13,1,0,false,true )
             ,new CursorDef("H00IR14", "SELECT TOP 1 [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR14,1,0,false,true )
             ,new CursorDef("H00IR15", "SELECT TOP 1 [ContratoServicosCusto_Codigo], [ContratoServicosCusto_CntSrvCod] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosCusto_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR15,1,0,false,true )
             ,new CursorDef("H00IR16", "SELECT TOP 1 [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR16,1,0,false,true )
             ,new CursorDef("H00IR17", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosRmn_Sequencial] > 0 ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IR17,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((short[]) buf[9])[0] = rslt.getShort(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((bool[]) buf[11])[0] = rslt.getBool(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((long[]) buf[14])[0] = rslt.getLong(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((int[]) buf[20])[0] = rslt.getInt(14) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(15) ;
                ((String[]) buf[22])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getString(17, 50) ;
                ((int[]) buf[25])[0] = rslt.getInt(18) ;
                ((short[]) buf[26])[0] = rslt.getShort(19) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                ((short[]) buf[28])[0] = rslt.getShort(20) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(20);
                ((String[]) buf[30])[0] = rslt.getString(21, 20) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(21);
                ((int[]) buf[32])[0] = rslt.getInt(22) ;
                ((short[]) buf[33])[0] = rslt.getShort(23) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(23);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
