/*
               File: PRC_ExportUserToGam
        Description: Export User To Gam
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:0.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_exportusertogam : GXProcedure
   {
      public prc_exportusertogam( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_exportusertogam( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      public int executeUdp( )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         return A1Usuario_Codigo ;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo )
      {
         prc_exportusertogam objprc_exportusertogam;
         objprc_exportusertogam = new prc_exportusertogam();
         objprc_exportusertogam.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_exportusertogam.context.SetSubmitInitialConfig(context);
         objprc_exportusertogam.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_exportusertogam);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_exportusertogam)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AP2 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A57Usuario_PessoaCod = P00AP2_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00AP2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00AP2_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00AP2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00AP2_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = P00AP2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00AP2_n58Usuario_PessoaNom[0];
            AV9GamUser = new SdtGAMUser(context);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV11PessoaNom = A58Usuario_PessoaNom;
               /* Execute user subroutine: 'MONTAUSERNAME' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               AV9GamUser.gxTpr_Name = AV11PessoaNom;
            }
            else
            {
               AV9GamUser.gxTpr_Name = A2Usuario_Nome;
            }
            AV9GamUser.gxTpr_Firstname = A58Usuario_PessoaNom;
            AV9GamUser.gxTpr_Password = "1";
            AV9GamUser.gxTpr_Isactive = true;
            AV9GamUser.gxTpr_Gender = "N";
            AV9GamUser.gxTpr_Mustchangepassword = true;
            AV9GamUser.save();
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV9GamUser.success() )
         {
            context.CommitDataStores( "PRC_ExportUserToGam");
            new prc_updusuario_usergamguid(context ).execute( ref  A1Usuario_Codigo,  AV9GamUser.gxTpr_Guid) ;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'MONTAUSERNAME' Routine */
         AV12i = (short)(StringUtil.StringSearch( AV11PessoaNom, " ", 1)-1);
         if ( AV12i <= 0 )
         {
            AV12i = (short)(StringUtil.Len( AV11PessoaNom));
         }
         AV13d = (short)(StringUtil.StringSearchRev( AV11PessoaNom, " ", -1)+1);
         AV11PessoaNom = StringUtil.Substring( AV11PessoaNom, 1, AV12i) + "." + StringUtil.Substring( AV11PessoaNom, AV13d, StringUtil.Len( AV11PessoaNom));
         GXt_char1 = AV11PessoaNom;
         new prc_padronizastring(context ).execute(  AV11PessoaNom, out  GXt_char1) ;
         AV11PessoaNom = StringUtil.Lower( GXt_char1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AP2_A57Usuario_PessoaCod = new int[1] ;
         P00AP2_A1Usuario_Codigo = new int[1] ;
         P00AP2_A2Usuario_Nome = new String[] {""} ;
         P00AP2_n2Usuario_Nome = new bool[] {false} ;
         P00AP2_A58Usuario_PessoaNom = new String[] {""} ;
         P00AP2_n58Usuario_PessoaNom = new bool[] {false} ;
         A2Usuario_Nome = "";
         A58Usuario_PessoaNom = "";
         AV9GamUser = new SdtGAMUser(context);
         AV11PessoaNom = "";
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_exportusertogam__default(),
            new Object[][] {
                new Object[] {
               P00AP2_A57Usuario_PessoaCod, P00AP2_A1Usuario_Codigo, P00AP2_A2Usuario_Nome, P00AP2_n2Usuario_Nome, P00AP2_A58Usuario_PessoaNom, P00AP2_n58Usuario_PessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12i ;
      private short AV13d ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private String scmdbuf ;
      private String A2Usuario_Nome ;
      private String A58Usuario_PessoaNom ;
      private String AV11PessoaNom ;
      private String GXt_char1 ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AP2_A57Usuario_PessoaCod ;
      private int[] P00AP2_A1Usuario_Codigo ;
      private String[] P00AP2_A2Usuario_Nome ;
      private bool[] P00AP2_n2Usuario_Nome ;
      private String[] P00AP2_A58Usuario_PessoaNom ;
      private bool[] P00AP2_n58Usuario_PessoaNom ;
      private SdtGAMUser AV9GamUser ;
   }

   public class prc_exportusertogam__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AP2 ;
          prmP00AP2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AP2", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T1.[Usuario_Nome], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AP2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
