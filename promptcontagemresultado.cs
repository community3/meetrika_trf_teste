/*
               File: PromptContagemResultado
        Description: Prompt Contagem Resultado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:21:58.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultado : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagemResultado_Codigo ,
                           ref String aP1_InOutContagemResultado_DemandaFM ,
                           ref int aP2_InOutContagemResultado_SistemaCod ,
                           ref int aP3_InOutModulo_Codigo ,
                           ref int aP4_InOutContagemResultado_ContratadaCod )
      {
         this.AV7InOutContagemResultado_Codigo = aP0_InOutContagemResultado_Codigo;
         this.AV72InOutContagemResultado_DemandaFM = aP1_InOutContagemResultado_DemandaFM;
         this.AV69InOutContagemResultado_SistemaCod = aP2_InOutContagemResultado_SistemaCod;
         this.AV70InOutModulo_Codigo = aP3_InOutModulo_Codigo;
         this.AV73InOutContagemResultado_ContratadaCod = aP4_InOutContagemResultado_ContratadaCod;
         executePrivate();
         aP0_InOutContagemResultado_Codigo=this.AV7InOutContagemResultado_Codigo;
         aP1_InOutContagemResultado_DemandaFM=this.AV72InOutContagemResultado_DemandaFM;
         aP2_InOutContagemResultado_SistemaCod=this.AV69InOutContagemResultado_SistemaCod;
         aP3_InOutModulo_Codigo=this.AV70InOutModulo_Codigo;
         aP4_InOutContagemResultado_ContratadaCod=this.AV73InOutContagemResultado_ContratadaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavContagemresultado_contratadacod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavContagemresultado_statusdmn1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavContagemresultado_statusdmn2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavContagemresultado_statusdmn3 = new GXCombobox();
         dynContagemResultado_Servico = new GXCombobox();
         cmbContagemResultado_StatusDmn = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODB42( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLACONTAGEMRESULTADO_SERVICOB42( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_113 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_113_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_113_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV30ContagemResultado_Demanda1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
               AV42ContagemResultado_DataDmn1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DataDmn1", context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"));
               AV43ContagemResultado_DataDmn_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn_To1", context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"));
               AV54ContagemResultado_StatusDmn1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV34ContagemResultado_Demanda2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
               AV46ContagemResultado_DataDmn2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataDmn2", context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"));
               AV47ContagemResultado_DataDmn_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_DataDmn_To2", context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"));
               AV55ContagemResultado_StatusDmn2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV38ContagemResultado_Demanda3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
               AV50ContagemResultado_DataDmn3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataDmn3", context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"));
               AV51ContagemResultado_DataDmn_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_DataDmn_To3", context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"));
               AV56ContagemResultado_StatusDmn3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV75TFContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
               AV76TFContagemResultado_DemandaFM_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultado_DemandaFM_Sel", AV76TFContagemResultado_DemandaFM_Sel);
               AV79TFContagemResultado_Demanda = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
               AV80TFContagemResultado_Demanda_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemResultado_Demanda_Sel", AV80TFContagemResultado_Demanda_Sel);
               AV83TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_DataDmn", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
               AV84TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_DataDmn_To", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
               AV89TFContagemResultado_ContratadaPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
               AV90TFContagemResultado_ContratadaPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContagemResultado_ContratadaPessoaNom_Sel", AV90TFContagemResultado_ContratadaPessoaNom_Sel);
               AV93TFContagemResultado_Servico = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
               AV94TFContagemResultado_Servico_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFContagemResultado_Servico_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0)));
               AV97TFContagemrResultado_SistemaSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
               AV98TFContagemrResultado_SistemaSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFContagemrResultado_SistemaSigla_Sel", AV98TFContagemrResultado_SistemaSigla_Sel);
               AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
               AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace);
               AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
               AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
               AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace", AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace);
               AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace", AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace);
               AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace", AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace);
               AV68Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
               AV71ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)));
               AV105TFContagemResultado_Servico_SelDsc = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContagemResultado_Servico_SelDsc", AV105TFContagemResultado_Servico_SelDsc);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV102TFContagemResultado_StatusDmn_Sels);
               AV112Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV72InOutContagemResultado_DemandaFM = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72InOutContagemResultado_DemandaFM", AV72InOutContagemResultado_DemandaFM);
                  AV69InOutContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69InOutContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69InOutContagemResultado_SistemaCod), 6, 0)));
                  AV70InOutModulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70InOutModulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70InOutModulo_Codigo), 6, 0)));
                  AV73InOutContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73InOutContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73InOutContagemResultado_ContratadaCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAB42( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV112Pgmname = "PromptContagemResultado";
               context.Gx_err = 0;
               GXACONTAGEMRESULTADO_SERVICO_htmlB42( ) ;
               WSB42( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEB42( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020625215931");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultado.aspx") + "?" + UrlEncode("" +AV7InOutContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV72InOutContagemResultado_DemandaFM)) + "," + UrlEncode("" +AV69InOutContagemResultado_SistemaCod) + "," + UrlEncode("" +AV70InOutModulo_Codigo) + "," + UrlEncode("" +AV73InOutContagemResultado_ContratadaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA1", StringUtil.RTrim( AV30ContagemResultado_Demanda1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN1", context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO1", context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN1", StringUtil.RTrim( AV54ContagemResultado_StatusDmn1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA2", StringUtil.RTrim( AV34ContagemResultado_Demanda2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN2", context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO2", context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN2", StringUtil.RTrim( AV55ContagemResultado_StatusDmn2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA3", StringUtil.RTrim( AV38ContagemResultado_Demanda3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN3", context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO3", context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN3", StringUtil.RTrim( AV56ContagemResultado_StatusDmn3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM", AV75TFContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL", AV76TFContagemResultado_DemandaFM_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDA", AV79TFContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL", AV80TFContagemResultado_Demanda_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATADMN", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM", StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL", StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_SERVICO", StringUtil.RTrim( AV93TFContagemResultado_Servico));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_SERVICO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA", StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL", StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_113", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_113), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV107GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV104DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV104DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV74ContagemResultado_DemandaFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV74ContagemResultado_DemandaFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV78ContagemResultado_DemandaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV78ContagemResultado_DemandaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA", AV82ContagemResultado_DataDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA", AV82ContagemResultado_DataDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV88ContagemResultado_ContratadaPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV88ContagemResultado_ContratadaPessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_SERVICOTITLEFILTERDATA", AV92ContagemResultado_ServicoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_SERVICOTITLEFILTERDATA", AV92ContagemResultado_ServicoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRRESULTADO_SISTEMASIGLATITLEFILTERDATA", AV96ContagemrResultado_SistemaSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRRESULTADO_SISTEMASIGLATITLEFILTERDATA", AV96ContagemrResultado_SistemaSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_STATUSDMNTITLEFILTERDATA", AV100ContagemResultado_StatusDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_STATUSDMNTITLEFILTERDATA", AV100ContagemResultado_StatusDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTAGEMRESULTADO_STATUSDMN_SELS", AV102TFContagemResultado_StatusDmn_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTAGEMRESULTADO_STATUSDMN_SELS", AV102TFContagemResultado_StatusDmn_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV112Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_DEMANDAFM", AV72InOutContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69InOutContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTMODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70InOutModulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73InOutContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Caption", StringUtil.RTrim( Ddo_contagemresultado_demanda_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demanda_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Cls", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demanda_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demanda_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demanda_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demanda_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Caption", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Cls", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Caption", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cls", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Caption", StringUtil.RTrim( Ddo_contagemresultado_servico_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_servico_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Cls", StringUtil.RTrim( Ddo_contagemresultado_servico_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_servico_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_servico_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_servico_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_servico_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_servico_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_servico_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_servico_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_servico_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_servico_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_servico_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_servico_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_servico_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_servico_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_servico_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_servico_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_servico_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_servico_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_servico_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_servico_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_servico_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Caption", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Tooltip", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Cls", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemrresultado_sistemasigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemrresultado_sistemasigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contagemrresultado_sistemasigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filtertype", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemrresultado_sistemasigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemrresultado_sistemasigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalisttype", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalistproc", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemrresultado_sistemasigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortasc", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortdsc", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Loadingdata", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Caption", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Cls", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_statusdmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_statusdmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_statusdmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_statusdmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagemresultado_statusdmn_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demanda_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_servico_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_servico_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_servico_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICO_Selectedtext_get", StringUtil.RTrim( Ddo_contagemresultado_servico_Selectedtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemrresultado_sistemasigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_STATUSDMN_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_statusdmn_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormB42( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultado" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prompt Contagem Resultado" ;
      }

      protected void WBB40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_B42( true) ;
         }
         else
         {
            wb_table1_2_B42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(134, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(135, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_Internalname, AV75TFContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV75TFContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_sel_Internalname, AV76TFContagemResultado_DemandaFM_Sel, StringUtil.RTrim( context.localUtil.Format( AV76TFContagemResultado_DemandaFM_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_sel_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_Internalname, AV79TFContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV79TFContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_sel_Internalname, AV80TFContagemResultado_Demanda_Sel, StringUtil.RTrim( context.localUtil.Format( AV80TFContagemResultado_Demanda_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datadmn_Internalname, context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( AV83TFContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datadmn_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datadmn_Visible, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datadmn_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datadmn_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datadmn_to_Internalname, context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"), context.localUtil.Format( AV84TFContagemResultado_DataDmn_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datadmn_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datadmn_to_Visible, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datadmn_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datadmn_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultado_datadmnauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datadmnauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datadmnauxdate_Internalname, context.localUtil.Format(AV85DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"), context.localUtil.Format( AV85DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datadmnauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datadmnauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datadmnauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datadmnauxdateto_Internalname, context.localUtil.Format(AV86DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"), context.localUtil.Format( AV86DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datadmnauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contratadapessoanom_Internalname, StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV89TFContagemResultado_ContratadaPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contratadapessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultado_contratadapessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contratadapessoanom_sel_Internalname, StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV90TFContagemResultado_ContratadaPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultado_contratadapessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_servico_Internalname, StringUtil.RTrim( AV93TFContagemResultado_Servico), StringUtil.RTrim( context.localUtil.Format( AV93TFContagemResultado_Servico, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_servico_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_servico_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_servico_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV94TFContagemResultado_Servico_Sel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,148);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_servico_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_servico_sel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_servico_seldsc_Internalname, AV105TFContagemResultado_Servico_SelDsc, StringUtil.RTrim( context.localUtil.Format( AV105TFContagemResultado_Servico_SelDsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_servico_seldsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_servico_seldsc_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemrresultado_sistemasigla_Internalname, StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( AV97TFContagemrResultado_SistemaSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemrresultado_sistemasigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemrresultado_sistemasigla_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemrresultado_sistemasigla_sel_Internalname, StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV98TFContagemrResultado_SistemaSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemrresultado_sistemasigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemrresultado_sistemasigla_sel_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DEMANDAFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DEMANDAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DATADMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_SERVICOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Internalname, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", 0, edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRRESULTADO_SISTEMASIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Internalname, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,163);\"", 0, edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_STATUSDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Internalname, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", 0, edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultado.htm");
         }
         wbLoad = true;
      }

      protected void STARTB42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prompt Contagem Resultado", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPB40( ) ;
      }

      protected void WSB42( )
      {
         STARTB42( ) ;
         EVTB42( ) ;
      }

      protected void EVTB42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11B42 */
                           E11B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12B42 */
                           E12B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13B42 */
                           E13B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14B42 */
                           E14B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15B42 */
                           E15B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_SERVICO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16B42 */
                           E16B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17B42 */
                           E17B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_STATUSDMN.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18B42 */
                           E18B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19B42 */
                           E19B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20B42 */
                           E20B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21B42 */
                           E21B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22B42 */
                           E22B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23B42 */
                           E23B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24B42 */
                           E24B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25B42 */
                           E25B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26B42 */
                           E26B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27B42 */
                           E27B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28B42 */
                           E28B42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_113_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1132( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV111Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                           A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                           n493ContagemResultado_DemandaFM = false;
                           A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                           n457ContagemResultado_Demanda = false;
                           A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                           A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_ContratadaCod_Internalname), ",", "."));
                           n490ContagemResultado_ContratadaCod = false;
                           A499ContagemResultado_ContratadaPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_ContratadaPessoaCod_Internalname), ",", "."));
                           n499ContagemResultado_ContratadaPessoaCod = false;
                           A500ContagemResultado_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaPessoaNom_Internalname));
                           n500ContagemResultado_ContratadaPessoaNom = false;
                           A489ContagemResultado_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_SistemaCod_Internalname), ",", "."));
                           n489ContagemResultado_SistemaCod = false;
                           A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
                           n146Modulo_Codigo = false;
                           dynContagemResultado_Servico.Name = dynContagemResultado_Servico_Internalname;
                           dynContagemResultado_Servico.CurrentValue = cgiGet( dynContagemResultado_Servico_Internalname);
                           A601ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_Servico_Internalname), "."));
                           n601ContagemResultado_Servico = false;
                           A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
                           n509ContagemrResultado_SistemaSigla = false;
                           cmbContagemResultado_StatusDmn.Name = cmbContagemResultado_StatusDmn_Internalname;
                           cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                           A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                           n484ContagemResultado_StatusDmn = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29B42 */
                                 E29B42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E30B42 */
                                 E30B42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E31B42 */
                                 E31B42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_demanda1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV30ContagemResultado_Demanda1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN1"), 0) != AV42ContagemResultado_DataDmn1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO1"), 0) != AV43ContagemResultado_DataDmn_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_statusdmn1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN1"), AV54ContagemResultado_StatusDmn1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_demanda2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV34ContagemResultado_Demanda2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN2"), 0) != AV46ContagemResultado_DataDmn2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO2"), 0) != AV47ContagemResultado_DataDmn_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_statusdmn2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN2"), AV55ContagemResultado_StatusDmn2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_demanda3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV38ContagemResultado_Demanda3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN3"), 0) != AV50ContagemResultado_DataDmn3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_datadmn_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO3"), 0) != AV51ContagemResultado_DataDmn_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultado_statusdmn3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN3"), AV56ContagemResultado_StatusDmn3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_demandafm Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV75TFContagemResultado_DemandaFM) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_demandafm_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV76TFContagemResultado_DemandaFM_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_demanda Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV79TFContagemResultado_Demanda) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_demanda_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV80TFContagemResultado_Demanda_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_datadmn Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN"), 0) != AV83TFContagemResultado_DataDmn )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_datadmn_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO"), 0) != AV84TFContagemResultado_DataDmn_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_contratadapessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM"), AV89TFContagemResultado_ContratadaPessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_contratadapessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL"), AV90TFContagemResultado_ContratadaPessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_servico Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICO"), AV93TFContagemResultado_Servico) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_servico_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICO_SEL"), ",", ".") != Convert.ToDecimal( AV94TFContagemResultado_Servico_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemrresultado_sistemasigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA"), AV97TFContagemrResultado_SistemaSigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemrresultado_sistemasigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL"), AV98TFContagemrResultado_SistemaSigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E32B42 */
                                       E32B42 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEB42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormB42( ) ;
            }
         }
      }

      protected void PAB42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATADMN", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavContagemresultado_statusdmn1.Name = "vCONTAGEMRESULTADO_STATUSDMN1";
            cmbavContagemresultado_statusdmn1.WebTags = "";
            cmbavContagemresultado_statusdmn1.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn1.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn1.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn1.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn1.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn1.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn1.addItem("D", "Retornada", 0);
            cmbavContagemresultado_statusdmn1.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn1.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn1.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn1.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn1.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn1.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn1.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn1.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn1.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
            {
               AV54ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV54ContagemResultado_StatusDmn1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATADMN", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavContagemresultado_statusdmn2.Name = "vCONTAGEMRESULTADO_STATUSDMN2";
            cmbavContagemresultado_statusdmn2.WebTags = "";
            cmbavContagemresultado_statusdmn2.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn2.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn2.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn2.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn2.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn2.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn2.addItem("D", "Retornada", 0);
            cmbavContagemresultado_statusdmn2.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn2.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn2.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn2.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn2.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn2.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn2.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn2.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn2.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
            {
               AV55ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV55ContagemResultado_StatusDmn2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATADMN", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            cmbavContagemresultado_statusdmn3.Name = "vCONTAGEMRESULTADO_STATUSDMN3";
            cmbavContagemresultado_statusdmn3.WebTags = "";
            cmbavContagemresultado_statusdmn3.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn3.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn3.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn3.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn3.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn3.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn3.addItem("D", "Retornada", 0);
            cmbavContagemresultado_statusdmn3.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn3.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn3.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn3.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn3.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn3.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn3.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn3.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn3.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
            {
               AV56ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV56ContagemResultado_StatusDmn3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
            }
            GXCCtl = "CONTAGEMRESULTADO_SERVICO_" + sGXsfl_113_idx;
            dynContagemResultado_Servico.Name = GXCCtl;
            dynContagemResultado_Servico.WebTags = "";
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_113_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODB42( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataB42( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlB42( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataB42( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV71ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataB42( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00B42 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00B42_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00B42_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLACONTAGEMRESULTADO_SERVICOB42( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_SERVICO_dataB42( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_SERVICO_htmlB42( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_SERVICO_dataB42( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_Servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_Servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_SERVICO_dataB42( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Desconhecido)");
         /* Using cursor H00B43 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00B43_A601ContagemResultado_Servico[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00B43_A801ContagemResultado_ServicoSigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1132( ) ;
         while ( nGXsfl_113_idx <= nRC_GXsfl_113 )
         {
            sendrow_1132( ) ;
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV30ContagemResultado_Demanda1 ,
                                       DateTime AV42ContagemResultado_DataDmn1 ,
                                       DateTime AV43ContagemResultado_DataDmn_To1 ,
                                       String AV54ContagemResultado_StatusDmn1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV34ContagemResultado_Demanda2 ,
                                       DateTime AV46ContagemResultado_DataDmn2 ,
                                       DateTime AV47ContagemResultado_DataDmn_To2 ,
                                       String AV55ContagemResultado_StatusDmn2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV38ContagemResultado_Demanda3 ,
                                       DateTime AV50ContagemResultado_DataDmn3 ,
                                       DateTime AV51ContagemResultado_DataDmn_To3 ,
                                       String AV56ContagemResultado_StatusDmn3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV75TFContagemResultado_DemandaFM ,
                                       String AV76TFContagemResultado_DemandaFM_Sel ,
                                       String AV79TFContagemResultado_Demanda ,
                                       String AV80TFContagemResultado_Demanda_Sel ,
                                       DateTime AV83TFContagemResultado_DataDmn ,
                                       DateTime AV84TFContagemResultado_DataDmn_To ,
                                       String AV89TFContagemResultado_ContratadaPessoaNom ,
                                       String AV90TFContagemResultado_ContratadaPessoaNom_Sel ,
                                       String AV93TFContagemResultado_Servico ,
                                       int AV94TFContagemResultado_Servico_Sel ,
                                       String AV97TFContagemrResultado_SistemaSigla ,
                                       String AV98TFContagemrResultado_SistemaSigla_Sel ,
                                       String AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ,
                                       String AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace ,
                                       String AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace ,
                                       String AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace ,
                                       String AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace ,
                                       String AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace ,
                                       String AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace ,
                                       int AV68Contratada_AreaTrabalhoCod ,
                                       int AV71ContagemResultado_ContratadaCod ,
                                       String AV105TFContagemResultado_Servico_SelDsc ,
                                       IGxCollection AV102TFContagemResultado_StatusDmn_Sels ,
                                       String AV112Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFB42( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A489ContagemResultado_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MODULO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV71ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
         {
            AV54ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV54ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
         {
            AV55ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV55ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
         {
            AV56ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV56ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV112Pgmname = "PromptContagemResultado";
         context.Gx_err = 0;
      }

      protected void RFB42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 113;
         /* Execute user event: E30B42 */
         E30B42 ();
         nGXsfl_113_idx = 1;
         sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
         SubsflControlProps_1132( ) ;
         nGXsfl_113_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1132( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 A484ContagemResultado_StatusDmn ,
                                                 AV102TFContagemResultado_StatusDmn_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV30ContagemResultado_Demanda1 ,
                                                 AV42ContagemResultado_DataDmn1 ,
                                                 AV43ContagemResultado_DataDmn_To1 ,
                                                 AV54ContagemResultado_StatusDmn1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV34ContagemResultado_Demanda2 ,
                                                 AV46ContagemResultado_DataDmn2 ,
                                                 AV47ContagemResultado_DataDmn_To2 ,
                                                 AV55ContagemResultado_StatusDmn2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV38ContagemResultado_Demanda3 ,
                                                 AV50ContagemResultado_DataDmn3 ,
                                                 AV51ContagemResultado_DataDmn_To3 ,
                                                 AV56ContagemResultado_StatusDmn3 ,
                                                 AV76TFContagemResultado_DemandaFM_Sel ,
                                                 AV75TFContagemResultado_DemandaFM ,
                                                 AV80TFContagemResultado_Demanda_Sel ,
                                                 AV79TFContagemResultado_Demanda ,
                                                 AV83TFContagemResultado_DataDmn ,
                                                 AV84TFContagemResultado_DataDmn_To ,
                                                 AV90TFContagemResultado_ContratadaPessoaNom_Sel ,
                                                 AV89TFContagemResultado_ContratadaPessoaNom ,
                                                 AV94TFContagemResultado_Servico_Sel ,
                                                 AV93TFContagemResultado_Servico ,
                                                 AV98TFContagemrResultado_SistemaSigla_Sel ,
                                                 AV97TFContagemrResultado_SistemaSigla ,
                                                 AV102TFContagemResultado_StatusDmn_Sels.Count ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 A457ContagemResultado_Demanda ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A471ContagemResultado_DataDmn ,
                                                 A500ContagemResultado_ContratadaPessoaNom ,
                                                 A605Servico_Sigla ,
                                                 A601ContagemResultado_Servico ,
                                                 A509ContagemrResultado_SistemaSigla ,
                                                 A490ContagemResultado_ContratadaCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 A1583ContagemResultado_TipoRegistro },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                                 }
            });
            lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
            lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
            lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
            lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
            lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
            lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
            lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
            lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
            lV75TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV75TFContagemResultado_DemandaFM), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
            lV79TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV79TFContagemResultado_Demanda), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
            lV89TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
            lV93TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV93TFContagemResultado_Servico), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
            lV97TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
            /* Using cursor H00B44 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV30ContagemResultado_Demanda1, AV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV34ContagemResultado_Demanda2, AV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV38ContagemResultado_Demanda3, AV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, lV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, lV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, lV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, lV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, lV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV6WWPContext.gxTpr_Contratada_codigo, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_113_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A127Sistema_Codigo = H00B44_A127Sistema_Codigo[0];
               A135Sistema_AreaTrabalhoCod = H00B44_A135Sistema_AreaTrabalhoCod[0];
               A830AreaTrabalho_ServicoPadrao = H00B44_A830AreaTrabalho_ServicoPadrao[0];
               n830AreaTrabalho_ServicoPadrao = H00B44_n830AreaTrabalho_ServicoPadrao[0];
               A1553ContagemResultado_CntSrvCod = H00B44_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00B44_n1553ContagemResultado_CntSrvCod[0];
               A605Servico_Sigla = H00B44_A605Servico_Sigla[0];
               n605Servico_Sigla = H00B44_n605Servico_Sigla[0];
               A52Contratada_AreaTrabalhoCod = H00B44_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00B44_n52Contratada_AreaTrabalhoCod[0];
               A1583ContagemResultado_TipoRegistro = H00B44_A1583ContagemResultado_TipoRegistro[0];
               A484ContagemResultado_StatusDmn = H00B44_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00B44_n484ContagemResultado_StatusDmn[0];
               A509ContagemrResultado_SistemaSigla = H00B44_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00B44_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = H00B44_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00B44_n601ContagemResultado_Servico[0];
               A146Modulo_Codigo = H00B44_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00B44_n146Modulo_Codigo[0];
               A489ContagemResultado_SistemaCod = H00B44_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00B44_n489ContagemResultado_SistemaCod[0];
               A500ContagemResultado_ContratadaPessoaNom = H00B44_A500ContagemResultado_ContratadaPessoaNom[0];
               n500ContagemResultado_ContratadaPessoaNom = H00B44_n500ContagemResultado_ContratadaPessoaNom[0];
               A499ContagemResultado_ContratadaPessoaCod = H00B44_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00B44_n499ContagemResultado_ContratadaPessoaCod[0];
               A490ContagemResultado_ContratadaCod = H00B44_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00B44_n490ContagemResultado_ContratadaCod[0];
               A471ContagemResultado_DataDmn = H00B44_A471ContagemResultado_DataDmn[0];
               A457ContagemResultado_Demanda = H00B44_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00B44_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00B44_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00B44_n493ContagemResultado_DemandaFM[0];
               A456ContagemResultado_Codigo = H00B44_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = H00B44_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00B44_n601ContagemResultado_Servico[0];
               A127Sistema_Codigo = H00B44_A127Sistema_Codigo[0];
               A135Sistema_AreaTrabalhoCod = H00B44_A135Sistema_AreaTrabalhoCod[0];
               A830AreaTrabalho_ServicoPadrao = H00B44_A830AreaTrabalho_ServicoPadrao[0];
               n830AreaTrabalho_ServicoPadrao = H00B44_n830AreaTrabalho_ServicoPadrao[0];
               A605Servico_Sigla = H00B44_A605Servico_Sigla[0];
               n605Servico_Sigla = H00B44_n605Servico_Sigla[0];
               A509ContagemrResultado_SistemaSigla = H00B44_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00B44_n509ContagemrResultado_SistemaSigla[0];
               A52Contratada_AreaTrabalhoCod = H00B44_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00B44_n52Contratada_AreaTrabalhoCod[0];
               A499ContagemResultado_ContratadaPessoaCod = H00B44_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00B44_n499ContagemResultado_ContratadaPessoaCod[0];
               A500ContagemResultado_ContratadaPessoaNom = H00B44_A500ContagemResultado_ContratadaPessoaNom[0];
               n500ContagemResultado_ContratadaPessoaNom = H00B44_n500ContagemResultado_ContratadaPessoaNom[0];
               /* Execute user event: E31B42 */
               E31B42 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 113;
            WBB40( ) ;
         }
         nGXsfl_113_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV102TFContagemResultado_StatusDmn_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV30ContagemResultado_Demanda1 ,
                                              AV42ContagemResultado_DataDmn1 ,
                                              AV43ContagemResultado_DataDmn_To1 ,
                                              AV54ContagemResultado_StatusDmn1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV34ContagemResultado_Demanda2 ,
                                              AV46ContagemResultado_DataDmn2 ,
                                              AV47ContagemResultado_DataDmn_To2 ,
                                              AV55ContagemResultado_StatusDmn2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV38ContagemResultado_Demanda3 ,
                                              AV50ContagemResultado_DataDmn3 ,
                                              AV51ContagemResultado_DataDmn_To3 ,
                                              AV56ContagemResultado_StatusDmn3 ,
                                              AV76TFContagemResultado_DemandaFM_Sel ,
                                              AV75TFContagemResultado_DemandaFM ,
                                              AV80TFContagemResultado_Demanda_Sel ,
                                              AV79TFContagemResultado_Demanda ,
                                              AV83TFContagemResultado_DataDmn ,
                                              AV84TFContagemResultado_DataDmn_To ,
                                              AV90TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV89TFContagemResultado_ContratadaPessoaNom ,
                                              AV94TFContagemResultado_Servico_Sel ,
                                              AV93TFContagemResultado_Servico ,
                                              AV98TFContagemrResultado_SistemaSigla_Sel ,
                                              AV97TFContagemrResultado_SistemaSigla ,
                                              AV102TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV6WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
         lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
         lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
         lV30ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV30ContagemResultado_Demanda1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
         lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
         lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
         lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
         lV34ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultado_Demanda2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
         lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
         lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
         lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
         lV38ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV38ContagemResultado_Demanda3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
         lV75TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV75TFContagemResultado_DemandaFM), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
         lV79TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV79TFContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
         lV89TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
         lV93TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV93TFContagemResultado_Servico), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
         lV97TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
         /* Using cursor H00B45 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV30ContagemResultado_Demanda1, AV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, lV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV34ContagemResultado_Demanda2, AV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, lV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV38ContagemResultado_Demanda3, AV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, lV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, lV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, lV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, lV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, lV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, lV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV6WWPContext.gxTpr_Contratada_codigo});
         GRID_nRecordCount = H00B45_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPB40( )
      {
         /* Before Start, stand alone formulas. */
         AV112Pgmname = "PromptContagemResultado";
         context.Gx_err = 0;
         GXACONTAGEMRESULTADO_SERVICO_htmlB42( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29B42 */
         E29B42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV104DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA"), AV74ContagemResultado_DemandaFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA"), AV78ContagemResultado_DemandaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA"), AV82ContagemResultado_DataDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA"), AV88ContagemResultado_ContratadaPessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_SERVICOTITLEFILTERDATA"), AV92ContagemResultado_ServicoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRRESULTADO_SISTEMASIGLATITLEFILTERDATA"), AV96ContagemrResultado_SistemaSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_STATUSDMNTITLEFILTERDATA"), AV100ContagemResultado_StatusDmnTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_AREATRABALHOCOD");
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68Contratada_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV68Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
            }
            dynavContagemresultado_contratadacod.Name = dynavContagemresultado_contratadacod_Internalname;
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV71ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV30ContagemResultado_Demanda1 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn1"}), 1, "vCONTAGEMRESULTADO_DATADMN1");
               GX_FocusControl = edtavContagemresultado_datadmn1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42ContagemResultado_DataDmn1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DataDmn1", context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"));
            }
            else
            {
               AV42ContagemResultado_DataDmn1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DataDmn1", context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To1"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO1");
               GX_FocusControl = edtavContagemresultado_datadmn_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43ContagemResultado_DataDmn_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn_To1", context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else
            {
               AV43ContagemResultado_DataDmn_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn_To1", context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            cmbavContagemresultado_statusdmn1.Name = cmbavContagemresultado_statusdmn1_Internalname;
            cmbavContagemresultado_statusdmn1.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            AV54ContagemResultado_StatusDmn1 = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV34ContagemResultado_Demanda2 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn2"}), 1, "vCONTAGEMRESULTADO_DATADMN2");
               GX_FocusControl = edtavContagemresultado_datadmn2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46ContagemResultado_DataDmn2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataDmn2", context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"));
            }
            else
            {
               AV46ContagemResultado_DataDmn2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataDmn2", context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To2"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO2");
               GX_FocusControl = edtavContagemresultado_datadmn_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47ContagemResultado_DataDmn_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_DataDmn_To2", context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            else
            {
               AV47ContagemResultado_DataDmn_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_DataDmn_To2", context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            cmbavContagemresultado_statusdmn2.Name = cmbavContagemresultado_statusdmn2_Internalname;
            cmbavContagemresultado_statusdmn2.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            AV55ContagemResultado_StatusDmn2 = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV38ContagemResultado_Demanda3 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn3"}), 1, "vCONTAGEMRESULTADO_DATADMN3");
               GX_FocusControl = edtavContagemresultado_datadmn3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50ContagemResultado_DataDmn3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataDmn3", context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"));
            }
            else
            {
               AV50ContagemResultado_DataDmn3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataDmn3", context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To3"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO3");
               GX_FocusControl = edtavContagemresultado_datadmn_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51ContagemResultado_DataDmn_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_DataDmn_To3", context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            else
            {
               AV51ContagemResultado_DataDmn_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_DataDmn_To3", context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            cmbavContagemresultado_statusdmn3.Name = cmbavContagemresultado_statusdmn3_Internalname;
            cmbavContagemresultado_statusdmn3.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            AV56ContagemResultado_StatusDmn3 = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV75TFContagemResultado_DemandaFM = cgiGet( edtavTfcontagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
            AV76TFContagemResultado_DemandaFM_Sel = cgiGet( edtavTfcontagemresultado_demandafm_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultado_DemandaFM_Sel", AV76TFContagemResultado_DemandaFM_Sel);
            AV79TFContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
            AV80TFContagemResultado_Demanda_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemResultado_Demanda_Sel", AV80TFContagemResultado_Demanda_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Dmn"}), 1, "vTFCONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavTfcontagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFContagemResultado_DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_DataDmn", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
            }
            else
            {
               AV83TFContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_DataDmn", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datadmn_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Dmn_To"}), 1, "vTFCONTAGEMRESULTADO_DATADMN_TO");
               GX_FocusControl = edtavTfcontagemresultado_datadmn_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFContagemResultado_DataDmn_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_DataDmn_To", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
            }
            else
            {
               AV84TFContagemResultado_DataDmn_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datadmn_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_DataDmn_To", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datadmnauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Dmn Aux Date"}), 1, "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultado_datadmnauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85DDO_ContagemResultado_DataDmnAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85DDO_ContagemResultado_DataDmnAuxDate", context.localUtil.Format(AV85DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"));
            }
            else
            {
               AV85DDO_ContagemResultado_DataDmnAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datadmnauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85DDO_ContagemResultado_DataDmnAuxDate", context.localUtil.Format(AV85DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datadmnauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Dmn Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultado_datadmnauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86DDO_ContagemResultado_DataDmnAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86DDO_ContagemResultado_DataDmnAuxDateTo", context.localUtil.Format(AV86DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"));
            }
            else
            {
               AV86DDO_ContagemResultado_DataDmnAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datadmnauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86DDO_ContagemResultado_DataDmnAuxDateTo", context.localUtil.Format(AV86DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"));
            }
            AV89TFContagemResultado_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_contratadapessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
            AV90TFContagemResultado_ContratadaPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_contratadapessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContagemResultado_ContratadaPessoaNom_Sel", AV90TFContagemResultado_ContratadaPessoaNom_Sel);
            AV93TFContagemResultado_Servico = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_servico_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_servico_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_servico_sel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_SERVICO_SEL");
               GX_FocusControl = edtavTfcontagemresultado_servico_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV94TFContagemResultado_Servico_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFContagemResultado_Servico_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0)));
            }
            else
            {
               AV94TFContagemResultado_Servico_Sel = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_servico_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFContagemResultado_Servico_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0)));
            }
            AV105TFContagemResultado_Servico_SelDsc = cgiGet( edtavTfcontagemresultado_servico_seldsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContagemResultado_Servico_SelDsc", AV105TFContagemResultado_Servico_SelDsc);
            AV97TFContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtavTfcontagemrresultado_sistemasigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
            AV98TFContagemrResultado_SistemaSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemrresultado_sistemasigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFContagemrResultado_SistemaSigla_Sel", AV98TFContagemrResultado_SistemaSigla_Sel);
            AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
            AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace);
            AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
            AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
            AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace", AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace);
            AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace", AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace);
            AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace", AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_113 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_113"), ",", "."));
            AV107GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV108GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_demandafm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption");
            Ddo_contagemresultado_demandafm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip");
            Ddo_contagemresultado_demandafm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls");
            Ddo_contagemresultado_demandafm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set");
            Ddo_contagemresultado_demandafm_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set");
            Ddo_contagemresultado_demandafm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype");
            Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demandafm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc"));
            Ddo_contagemresultado_demandafm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc"));
            Ddo_contagemresultado_demandafm_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus");
            Ddo_contagemresultado_demandafm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter"));
            Ddo_contagemresultado_demandafm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype");
            Ddo_contagemresultado_demandafm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange"));
            Ddo_contagemresultado_demandafm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist"));
            Ddo_contagemresultado_demandafm_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype");
            Ddo_contagemresultado_demandafm_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc");
            Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demandafm_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc");
            Ddo_contagemresultado_demandafm_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc");
            Ddo_contagemresultado_demandafm_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata");
            Ddo_contagemresultado_demandafm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter");
            Ddo_contagemresultado_demandafm_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound");
            Ddo_contagemresultado_demandafm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext");
            Ddo_contagemresultado_demanda_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Caption");
            Ddo_contagemresultado_demanda_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip");
            Ddo_contagemresultado_demanda_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Cls");
            Ddo_contagemresultado_demanda_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set");
            Ddo_contagemresultado_demanda_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set");
            Ddo_contagemresultado_demanda_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype");
            Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demanda_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc"));
            Ddo_contagemresultado_demanda_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc"));
            Ddo_contagemresultado_demanda_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus");
            Ddo_contagemresultado_demanda_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter"));
            Ddo_contagemresultado_demanda_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype");
            Ddo_contagemresultado_demanda_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange"));
            Ddo_contagemresultado_demanda_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist"));
            Ddo_contagemresultado_demanda_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype");
            Ddo_contagemresultado_demanda_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc");
            Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demanda_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc");
            Ddo_contagemresultado_demanda_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc");
            Ddo_contagemresultado_demanda_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata");
            Ddo_contagemresultado_demanda_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter");
            Ddo_contagemresultado_demanda_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound");
            Ddo_contagemresultado_demanda_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext");
            Ddo_contagemresultado_datadmn_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Caption");
            Ddo_contagemresultado_datadmn_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Tooltip");
            Ddo_contagemresultado_datadmn_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Cls");
            Ddo_contagemresultado_datadmn_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_set");
            Ddo_contagemresultado_datadmn_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_set");
            Ddo_contagemresultado_datadmn_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Dropdownoptionstype");
            Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Titlecontrolidtoreplace");
            Ddo_contagemresultado_datadmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includesortasc"));
            Ddo_contagemresultado_datadmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includesortdsc"));
            Ddo_contagemresultado_datadmn_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Sortedstatus");
            Ddo_contagemresultado_datadmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includefilter"));
            Ddo_contagemresultado_datadmn_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filtertype");
            Ddo_contagemresultado_datadmn_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filterisrange"));
            Ddo_contagemresultado_datadmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includedatalist"));
            Ddo_contagemresultado_datadmn_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Sortasc");
            Ddo_contagemresultado_datadmn_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Sortdsc");
            Ddo_contagemresultado_datadmn_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Cleanfilter");
            Ddo_contagemresultado_datadmn_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterfrom");
            Ddo_contagemresultado_datadmn_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterto");
            Ddo_contagemresultado_datadmn_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Searchbuttontext");
            Ddo_contagemresultado_contratadapessoanom_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Caption");
            Ddo_contagemresultado_contratadapessoanom_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Tooltip");
            Ddo_contagemresultado_contratadapessoanom_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cls");
            Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_set");
            Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_set");
            Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Dropdownoptionstype");
            Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_contratadapessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortasc"));
            Ddo_contagemresultado_contratadapessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortdsc"));
            Ddo_contagemresultado_contratadapessoanom_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortedstatus");
            Ddo_contagemresultado_contratadapessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includefilter"));
            Ddo_contagemresultado_contratadapessoanom_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filtertype");
            Ddo_contagemresultado_contratadapessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filterisrange"));
            Ddo_contagemresultado_contratadapessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includedatalist"));
            Ddo_contagemresultado_contratadapessoanom_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalisttype");
            Ddo_contagemresultado_contratadapessoanom_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistproc");
            Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_contratadapessoanom_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortasc");
            Ddo_contagemresultado_contratadapessoanom_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Sortdsc");
            Ddo_contagemresultado_contratadapessoanom_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Loadingdata");
            Ddo_contagemresultado_contratadapessoanom_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cleanfilter");
            Ddo_contagemresultado_contratadapessoanom_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Noresultsfound");
            Ddo_contagemresultado_contratadapessoanom_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Searchbuttontext");
            Ddo_contagemresultado_servico_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Caption");
            Ddo_contagemresultado_servico_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Tooltip");
            Ddo_contagemresultado_servico_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Cls");
            Ddo_contagemresultado_servico_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Filteredtext_set");
            Ddo_contagemresultado_servico_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Selectedvalue_set");
            Ddo_contagemresultado_servico_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Dropdownoptionstype");
            Ddo_contagemresultado_servico_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Titlecontrolidtoreplace");
            Ddo_contagemresultado_servico_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Includesortasc"));
            Ddo_contagemresultado_servico_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Includesortdsc"));
            Ddo_contagemresultado_servico_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Sortedstatus");
            Ddo_contagemresultado_servico_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Includefilter"));
            Ddo_contagemresultado_servico_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Filtertype");
            Ddo_contagemresultado_servico_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Filterisrange"));
            Ddo_contagemresultado_servico_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Includedatalist"));
            Ddo_contagemresultado_servico_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Datalisttype");
            Ddo_contagemresultado_servico_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Datalistproc");
            Ddo_contagemresultado_servico_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_servico_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Sortasc");
            Ddo_contagemresultado_servico_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Sortdsc");
            Ddo_contagemresultado_servico_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Loadingdata");
            Ddo_contagemresultado_servico_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Cleanfilter");
            Ddo_contagemresultado_servico_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Noresultsfound");
            Ddo_contagemresultado_servico_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Searchbuttontext");
            Ddo_contagemrresultado_sistemasigla_Caption = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Caption");
            Ddo_contagemrresultado_sistemasigla_Tooltip = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Tooltip");
            Ddo_contagemrresultado_sistemasigla_Cls = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Cls");
            Ddo_contagemrresultado_sistemasigla_Filteredtext_set = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filteredtext_set");
            Ddo_contagemrresultado_sistemasigla_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Selectedvalue_set");
            Ddo_contagemrresultado_sistemasigla_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Dropdownoptionstype");
            Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Titlecontrolidtoreplace");
            Ddo_contagemrresultado_sistemasigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includesortasc"));
            Ddo_contagemrresultado_sistemasigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includesortdsc"));
            Ddo_contagemrresultado_sistemasigla_Sortedstatus = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortedstatus");
            Ddo_contagemrresultado_sistemasigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includefilter"));
            Ddo_contagemrresultado_sistemasigla_Filtertype = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filtertype");
            Ddo_contagemrresultado_sistemasigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filterisrange"));
            Ddo_contagemrresultado_sistemasigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Includedatalist"));
            Ddo_contagemrresultado_sistemasigla_Datalisttype = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalisttype");
            Ddo_contagemrresultado_sistemasigla_Datalistproc = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalistproc");
            Ddo_contagemrresultado_sistemasigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemrresultado_sistemasigla_Sortasc = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortasc");
            Ddo_contagemrresultado_sistemasigla_Sortdsc = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Sortdsc");
            Ddo_contagemrresultado_sistemasigla_Loadingdata = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Loadingdata");
            Ddo_contagemrresultado_sistemasigla_Cleanfilter = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Cleanfilter");
            Ddo_contagemrresultado_sistemasigla_Noresultsfound = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Noresultsfound");
            Ddo_contagemrresultado_sistemasigla_Searchbuttontext = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Searchbuttontext");
            Ddo_contagemresultado_statusdmn_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Caption");
            Ddo_contagemresultado_statusdmn_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Tooltip");
            Ddo_contagemresultado_statusdmn_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Cls");
            Ddo_contagemresultado_statusdmn_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Selectedvalue_set");
            Ddo_contagemresultado_statusdmn_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Dropdownoptionstype");
            Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Titlecontrolidtoreplace");
            Ddo_contagemresultado_statusdmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Includesortasc"));
            Ddo_contagemresultado_statusdmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Includesortdsc"));
            Ddo_contagemresultado_statusdmn_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortedstatus");
            Ddo_contagemresultado_statusdmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Includefilter"));
            Ddo_contagemresultado_statusdmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Includedatalist"));
            Ddo_contagemresultado_statusdmn_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Datalisttype");
            Ddo_contagemresultado_statusdmn_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Allowmultipleselection"));
            Ddo_contagemresultado_statusdmn_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Datalistfixedvalues");
            Ddo_contagemresultado_statusdmn_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortasc");
            Ddo_contagemresultado_statusdmn_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Sortdsc");
            Ddo_contagemresultado_statusdmn_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Cleanfilter");
            Ddo_contagemresultado_statusdmn_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_demandafm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey");
            Ddo_contagemresultado_demandafm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get");
            Ddo_contagemresultado_demandafm_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get");
            Ddo_contagemresultado_demanda_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey");
            Ddo_contagemresultado_demanda_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get");
            Ddo_contagemresultado_demanda_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get");
            Ddo_contagemresultado_datadmn_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Activeeventkey");
            Ddo_contagemresultado_datadmn_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_get");
            Ddo_contagemresultado_datadmn_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_get");
            Ddo_contagemresultado_contratadapessoanom_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Activeeventkey");
            Ddo_contagemresultado_contratadapessoanom_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_get");
            Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_get");
            Ddo_contagemresultado_servico_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Activeeventkey");
            Ddo_contagemresultado_servico_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Filteredtext_get");
            Ddo_contagemresultado_servico_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Selectedvalue_get");
            Ddo_contagemresultado_servico_Selectedtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICO_Selectedtext_get");
            Ddo_contagemrresultado_sistemasigla_Activeeventkey = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Activeeventkey");
            Ddo_contagemrresultado_sistemasigla_Filteredtext_get = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Filteredtext_get");
            Ddo_contagemrresultado_sistemasigla_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA_Selectedvalue_get");
            Ddo_contagemresultado_statusdmn_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Activeeventkey");
            Ddo_contagemresultado_statusdmn_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_STATUSDMN_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV30ContagemResultado_Demanda1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN1"), 0) != AV42ContagemResultado_DataDmn1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO1"), 0) != AV43ContagemResultado_DataDmn_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN1"), AV54ContagemResultado_StatusDmn1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV34ContagemResultado_Demanda2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN2"), 0) != AV46ContagemResultado_DataDmn2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO2"), 0) != AV47ContagemResultado_DataDmn_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN2"), AV55ContagemResultado_StatusDmn2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV38ContagemResultado_Demanda3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN3"), 0) != AV50ContagemResultado_DataDmn3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO3"), 0) != AV51ContagemResultado_DataDmn_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN3"), AV56ContagemResultado_StatusDmn3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV75TFContagemResultado_DemandaFM) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV76TFContagemResultado_DemandaFM_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV79TFContagemResultado_Demanda) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV80TFContagemResultado_Demanda_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN"), 0) != AV83TFContagemResultado_DataDmn )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO"), 0) != AV84TFContagemResultado_DataDmn_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM"), AV89TFContagemResultado_ContratadaPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL"), AV90TFContagemResultado_ContratadaPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICO"), AV93TFContagemResultado_Servico) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICO_SEL"), ",", ".") != Convert.ToDecimal( AV94TFContagemResultado_Servico_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA"), AV97TFContagemrResultado_SistemaSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL"), AV98TFContagemrResultado_SistemaSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29B42 */
         E29B42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29B42( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV54ContagemResultado_StatusDmn1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV55ContagemResultado_StatusDmn2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
         AV19DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV56ContagemResultado_StatusDmn3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
         AV23DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemresultado_demandafm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_demanda_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demanda_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_Visible), 5, 0)));
         edtavTfcontagemresultado_demanda_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demanda_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_datadmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datadmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datadmn_Visible), 5, 0)));
         edtavTfcontagemresultado_datadmn_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datadmn_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datadmn_to_Visible), 5, 0)));
         edtavTfcontagemresultado_contratadapessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contratadapessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contratadapessoanom_Visible), 5, 0)));
         edtavTfcontagemresultado_contratadapessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contratadapessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contratadapessoanom_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_servico_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_servico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_servico_Visible), 5, 0)));
         edtavTfcontagemresultado_servico_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_servico_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_servico_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_servico_seldsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_servico_seldsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_servico_seldsc_Visible), 5, 0)));
         edtavTfcontagemrresultado_sistemasigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemrresultado_sistemasigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemrresultado_sistemasigla_Visible), 5, 0)));
         edtavTfcontagemrresultado_sistemasigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemrresultado_sistemasigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemrresultado_sistemasigla_sel_Visible), 5, 0)));
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DemandaFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace);
         AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Demanda";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demanda_Titlecontrolidtoreplace);
         AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace = Ddo_contagemresultado_demanda_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DataDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace);
         AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace = Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_ContratadaPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace);
         AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_servico_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Servico";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_servico_Titlecontrolidtoreplace);
         AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace = Ddo_contagemresultado_servico_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace", AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace);
         edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemrResultado_SistemaSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "TitleControlIdToReplace", Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace);
         AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace = Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace", AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace);
         edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_StatusDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace);
         AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace = Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace", AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace);
         edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione OS";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "OS", 0);
         cmbavOrderedby.addItem("2", "dATA", 0);
         cmbavOrderedby.addItem("3", "OS", 0);
         cmbavOrderedby.addItem("4", "Contratada", 0);
         cmbavOrderedby.addItem("5", "Servi�o", 0);
         cmbavOrderedby.addItem("6", "Sistema", 0);
         cmbavOrderedby.addItem("7", "Status", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV104DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV104DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         subgrid_gotopage( 1) ;
         GX_FocusControl = edtavContagemresultado_demanda1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E30B42( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV74ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82ContagemResultado_DataDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88ContagemResultado_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV92ContagemResultado_ServicoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96ContagemrResultado_SistemaSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100ContagemResultado_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_DemandaFM_Titleformat = 2;
         edtContagemResultado_DemandaFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "OS", AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Title", edtContagemResultado_DemandaFM_Title);
         edtContagemResultado_Demanda_Titleformat = 2;
         edtContagemResultado_Demanda_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "OS Ref.", AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Demanda_Internalname, "Title", edtContagemResultado_Demanda_Title);
         edtContagemResultado_DataDmn_Titleformat = 2;
         edtContagemResultado_DataDmn_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Title", edtContagemResultado_DataDmn_Title);
         edtContagemResultado_ContratadaPessoaNom_Titleformat = 2;
         edtContagemResultado_ContratadaPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ContratadaPessoaNom_Internalname, "Title", edtContagemResultado_ContratadaPessoaNom_Title);
         dynContagemResultado_Servico_Titleformat = 2;
         dynContagemResultado_Servico.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_Servico_Internalname, "Title", dynContagemResultado_Servico.Title.Text);
         edtContagemrResultado_SistemaSigla_Titleformat = 2;
         edtContagemrResultado_SistemaSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemrResultado_SistemaSigla_Internalname, "Title", edtContagemrResultado_SistemaSigla_Title);
         cmbContagemResultado_StatusDmn_Titleformat = 2;
         cmbContagemResultado_StatusDmn.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Title", cmbContagemResultado_StatusDmn.Title.Text);
         AV107GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107GridCurrentPage), 10, 0)));
         AV108GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74ContagemResultado_DemandaFMTitleFilterData", AV74ContagemResultado_DemandaFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78ContagemResultado_DemandaTitleFilterData", AV78ContagemResultado_DemandaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82ContagemResultado_DataDmnTitleFilterData", AV82ContagemResultado_DataDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV88ContagemResultado_ContratadaPessoaNomTitleFilterData", AV88ContagemResultado_ContratadaPessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV92ContagemResultado_ServicoTitleFilterData", AV92ContagemResultado_ServicoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96ContagemrResultado_SistemaSiglaTitleFilterData", AV96ContagemrResultado_SistemaSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV100ContagemResultado_StatusDmnTitleFilterData", AV100ContagemResultado_StatusDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11B42( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV106PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV106PageToGo) ;
         }
      }

      protected void E12B42( )
      {
         /* Ddo_contagemresultado_demandafm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFContagemResultado_DemandaFM = Ddo_contagemresultado_demandafm_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
            AV76TFContagemResultado_DemandaFM_Sel = Ddo_contagemresultado_demandafm_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultado_DemandaFM_Sel", AV76TFContagemResultado_DemandaFM_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13B42( )
      {
         /* Ddo_contagemresultado_demanda_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFContagemResultado_Demanda = Ddo_contagemresultado_demanda_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
            AV80TFContagemResultado_Demanda_Sel = Ddo_contagemresultado_demanda_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemResultado_Demanda_Sel", AV80TFContagemResultado_Demanda_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14B42( )
      {
         /* Ddo_contagemresultado_datadmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_datadmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_datadmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "SortedStatus", Ddo_contagemresultado_datadmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_datadmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_datadmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "SortedStatus", Ddo_contagemresultado_datadmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_datadmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFContagemResultado_DataDmn = context.localUtil.CToD( Ddo_contagemresultado_datadmn_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_DataDmn", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
            AV84TFContagemResultado_DataDmn_To = context.localUtil.CToD( Ddo_contagemresultado_datadmn_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_DataDmn_To", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15B42( )
      {
         /* Ddo_contagemresultado_contratadapessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_contratadapessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_contratadapessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SortedStatus", Ddo_contagemresultado_contratadapessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_contratadapessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_contratadapessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SortedStatus", Ddo_contagemresultado_contratadapessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_contratadapessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV89TFContagemResultado_ContratadaPessoaNom = Ddo_contagemresultado_contratadapessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
            AV90TFContagemResultado_ContratadaPessoaNom_Sel = Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContagemResultado_ContratadaPessoaNom_Sel", AV90TFContagemResultado_ContratadaPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16B42( )
      {
         /* Ddo_contagemresultado_servico_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_servico_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_servico_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "SortedStatus", Ddo_contagemresultado_servico_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_servico_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_servico_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "SortedStatus", Ddo_contagemresultado_servico_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_servico_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV93TFContagemResultado_Servico = Ddo_contagemresultado_servico_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
            AV94TFContagemResultado_Servico_Sel = (int)(NumberUtil.Val( Ddo_contagemresultado_servico_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFContagemResultado_Servico_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0)));
            AV105TFContagemResultado_Servico_SelDsc = Ddo_contagemresultado_servico_Selectedtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContagemResultado_Servico_SelDsc", AV105TFContagemResultado_Servico_SelDsc);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17B42( )
      {
         /* Ddo_contagemrresultado_sistemasigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemrresultado_sistemasigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemrresultado_sistemasigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "SortedStatus", Ddo_contagemrresultado_sistemasigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemrresultado_sistemasigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemrresultado_sistemasigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "SortedStatus", Ddo_contagemrresultado_sistemasigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemrresultado_sistemasigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV97TFContagemrResultado_SistemaSigla = Ddo_contagemrresultado_sistemasigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
            AV98TFContagemrResultado_SistemaSigla_Sel = Ddo_contagemrresultado_sistemasigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFContagemrResultado_SistemaSigla_Sel", AV98TFContagemrResultado_SistemaSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18B42( )
      {
         /* Ddo_contagemresultado_statusdmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_statusdmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_statusdmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "SortedStatus", Ddo_contagemresultado_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_statusdmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_statusdmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "SortedStatus", Ddo_contagemresultado_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_statusdmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV101TFContagemResultado_StatusDmn_SelsJson = Ddo_contagemresultado_statusdmn_Selectedvalue_get;
            AV102TFContagemResultado_StatusDmn_Sels.FromJSonString(AV101TFContagemResultado_StatusDmn_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV102TFContagemResultado_StatusDmn_Sels", AV102TFContagemResultado_StatusDmn_Sels);
      }

      private void E31B42( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV111Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 113;
         }
         sendrow_1132( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_113_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(113, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E32B42 */
         E32B42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E32B42( )
      {
         /* Enter Routine */
         AV7InOutContagemResultado_Codigo = A456ContagemResultado_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
         AV72InOutContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72InOutContagemResultado_DemandaFM", AV72InOutContagemResultado_DemandaFM);
         AV69InOutContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69InOutContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69InOutContagemResultado_SistemaCod), 6, 0)));
         AV70InOutModulo_Codigo = A146Modulo_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70InOutModulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70InOutModulo_Codigo), 6, 0)));
         AV73InOutContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73InOutContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73InOutContagemResultado_ContratadaCod), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContagemResultado_Codigo,(String)AV72InOutContagemResultado_DemandaFM,(int)AV69InOutContagemResultado_SistemaCod,(int)AV70InOutModulo_Codigo,(int)AV73InOutContagemResultado_ContratadaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E19B42( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E24B42( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20B42( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV54ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV55ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV56ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E25B42( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26B42( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21B42( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV54ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV55ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV56ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E27B42( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22B42( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30ContagemResultado_Demanda1, AV42ContagemResultado_DataDmn1, AV43ContagemResultado_DataDmn_To1, AV54ContagemResultado_StatusDmn1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV34ContagemResultado_Demanda2, AV46ContagemResultado_DataDmn2, AV47ContagemResultado_DataDmn_To2, AV55ContagemResultado_StatusDmn2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV38ContagemResultado_Demanda3, AV50ContagemResultado_DataDmn3, AV51ContagemResultado_DataDmn_To3, AV56ContagemResultado_StatusDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV75TFContagemResultado_DemandaFM, AV76TFContagemResultado_DemandaFM_Sel, AV79TFContagemResultado_Demanda, AV80TFContagemResultado_Demanda_Sel, AV83TFContagemResultado_DataDmn, AV84TFContagemResultado_DataDmn_To, AV89TFContagemResultado_ContratadaPessoaNom, AV90TFContagemResultado_ContratadaPessoaNom_Sel, AV93TFContagemResultado_Servico, AV94TFContagemResultado_Servico_Sel, AV97TFContagemrResultado_SistemaSigla, AV98TFContagemrResultado_SistemaSigla_Sel, AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace, AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace, AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV71ContagemResultado_ContratadaCod, AV105TFContagemResultado_Servico_SelDsc, AV102TFContagemResultado_StatusDmn_Sels, AV112Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV54ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV55ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV56ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E28B42( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23B42( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", dynavContagemresultado_contratadacod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV102TFContagemResultado_StatusDmn_Sels", AV102TFContagemResultado_StatusDmn_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV54ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV55ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV56ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         Ddo_contagemresultado_datadmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "SortedStatus", Ddo_contagemresultado_datadmn_Sortedstatus);
         Ddo_contagemresultado_contratadapessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SortedStatus", Ddo_contagemresultado_contratadapessoanom_Sortedstatus);
         Ddo_contagemresultado_servico_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "SortedStatus", Ddo_contagemresultado_servico_Sortedstatus);
         Ddo_contagemrresultado_sistemasigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "SortedStatus", Ddo_contagemrresultado_sistemasigla_Sortedstatus);
         Ddo_contagemresultado_statusdmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "SortedStatus", Ddo_contagemresultado_statusdmn_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemresultado_demandafm_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contagemresultado_demanda_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contagemresultado_datadmn_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "SortedStatus", Ddo_contagemresultado_datadmn_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemresultado_contratadapessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SortedStatus", Ddo_contagemresultado_contratadapessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemresultado_servico_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "SortedStatus", Ddo_contagemresultado_servico_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contagemrresultado_sistemasigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "SortedStatus", Ddo_contagemrresultado_sistemasigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contagemresultado_statusdmn_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "SortedStatus", Ddo_contagemresultado_statusdmn_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemresultado_demanda1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemresultado_demanda2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemresultado_demanda3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV34ContagemResultado_Demanda2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV38ContagemResultado_Demanda3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV68Contratada_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
         AV71ContagemResultado_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)));
         AV75TFContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultado_DemandaFM", AV75TFContagemResultado_DemandaFM);
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "FilteredText_set", Ddo_contagemresultado_demandafm_Filteredtext_set);
         AV76TFContagemResultado_DemandaFM_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultado_DemandaFM_Sel", AV76TFContagemResultado_DemandaFM_Sel);
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SelectedValue_set", Ddo_contagemresultado_demandafm_Selectedvalue_set);
         AV79TFContagemResultado_Demanda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContagemResultado_Demanda", AV79TFContagemResultado_Demanda);
         Ddo_contagemresultado_demanda_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "FilteredText_set", Ddo_contagemresultado_demanda_Filteredtext_set);
         AV80TFContagemResultado_Demanda_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContagemResultado_Demanda_Sel", AV80TFContagemResultado_Demanda_Sel);
         Ddo_contagemresultado_demanda_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SelectedValue_set", Ddo_contagemresultado_demanda_Selectedvalue_set);
         AV83TFContagemResultado_DataDmn = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_DataDmn", context.localUtil.Format(AV83TFContagemResultado_DataDmn, "99/99/99"));
         Ddo_contagemresultado_datadmn_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "FilteredText_set", Ddo_contagemresultado_datadmn_Filteredtext_set);
         AV84TFContagemResultado_DataDmn_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_DataDmn_To", context.localUtil.Format(AV84TFContagemResultado_DataDmn_To, "99/99/99"));
         Ddo_contagemresultado_datadmn_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_datadmn_Filteredtextto_set);
         AV89TFContagemResultado_ContratadaPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFContagemResultado_ContratadaPessoaNom", AV89TFContagemResultado_ContratadaPessoaNom);
         Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "FilteredText_set", Ddo_contagemresultado_contratadapessoanom_Filteredtext_set);
         AV90TFContagemResultado_ContratadaPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContagemResultado_ContratadaPessoaNom_Sel", AV90TFContagemResultado_ContratadaPessoaNom_Sel);
         Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SelectedValue_set", Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set);
         AV93TFContagemResultado_Servico = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFContagemResultado_Servico", AV93TFContagemResultado_Servico);
         Ddo_contagemresultado_servico_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "FilteredText_set", Ddo_contagemresultado_servico_Filteredtext_set);
         AV94TFContagemResultado_Servico_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFContagemResultado_Servico_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0)));
         Ddo_contagemresultado_servico_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servico_Internalname, "SelectedValue_set", Ddo_contagemresultado_servico_Selectedvalue_set);
         AV97TFContagemrResultado_SistemaSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContagemrResultado_SistemaSigla", AV97TFContagemrResultado_SistemaSigla);
         Ddo_contagemrresultado_sistemasigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "FilteredText_set", Ddo_contagemrresultado_sistemasigla_Filteredtext_set);
         AV98TFContagemrResultado_SistemaSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFContagemrResultado_SistemaSigla_Sel", AV98TFContagemrResultado_SistemaSigla_Sel);
         Ddo_contagemrresultado_sistemasigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemrresultado_sistemasigla_Internalname, "SelectedValue_set", Ddo_contagemrresultado_sistemasigla_Selectedvalue_set);
         AV102TFContagemResultado_StatusDmn_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contagemresultado_statusdmn_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_statusdmn_Internalname, "SelectedValue_set", Ddo_contagemresultado_statusdmn_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DEMANDA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV30ContagemResultado_Demanda1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV30ContagemResultado_Demanda1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_Demanda1", AV30ContagemResultado_Demanda1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV42ContagemResultado_DataDmn1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DataDmn1", context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"));
               AV43ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn_To1", context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV54ContagemResultado_StatusDmn1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_StatusDmn1", AV54ContagemResultado_StatusDmn1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV34ContagemResultado_Demanda2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_Demanda2", AV34ContagemResultado_Demanda2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV46ContagemResultado_DataDmn2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataDmn2", context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"));
                  AV47ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_DataDmn_To2", context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV55ContagemResultado_StatusDmn2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_StatusDmn2", AV55ContagemResultado_StatusDmn2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV38ContagemResultado_Demanda3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Demanda3", AV38ContagemResultado_Demanda3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV50ContagemResultado_DataDmn3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataDmn3", context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"));
                     AV51ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_DataDmn_To3", context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV56ContagemResultado_StatusDmn3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_StatusDmn3", AV56ContagemResultado_StatusDmn3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV68Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV71ContagemResultado_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFContagemResultado_DemandaFM)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFContagemResultado_DemandaFM;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContagemResultado_DemandaFM_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFContagemResultado_DemandaFM_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFContagemResultado_Demanda)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFContagemResultado_Demanda;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContagemResultado_Demanda_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFContagemResultado_Demanda_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV83TFContagemResultado_DataDmn) && (DateTime.MinValue==AV84TFContagemResultado_DataDmn_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DATADMN";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV83TFContagemResultado_DataDmn, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV84TFContagemResultado_DataDmn_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV89TFContagemResultado_ContratadaPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV90TFContagemResultado_ContratadaPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFContagemResultado_Servico)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_SERVICO";
            AV11GridStateFilterValue.gxTpr_Value = AV93TFContagemResultado_Servico;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV94TFContagemResultado_Servico_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_SERVICO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV94TFContagemResultado_Servico_Sel), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = AV105TFContagemResultado_Servico_SelDsc;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRRESULTADO_SISTEMASIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV97TFContagemrResultado_SistemaSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV98TFContagemrResultado_SistemaSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV102TFContagemResultado_StatusDmn_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_STATUSDMN_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV102TFContagemResultado_StatusDmn_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV112Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30ContagemResultado_Demanda1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV42ContagemResultado_DataDmn1) && (DateTime.MinValue==AV43ContagemResultado_DataDmn_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV42ContagemResultado_DataDmn1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV43ContagemResultado_DataDmn_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContagemResultado_StatusDmn1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV54ContagemResultado_StatusDmn1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34ContagemResultado_Demanda2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV46ContagemResultado_DataDmn2) && (DateTime.MinValue==AV47ContagemResultado_DataDmn_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV46ContagemResultado_DataDmn2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV47ContagemResultado_DataDmn_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_StatusDmn2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV55ContagemResultado_StatusDmn2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38ContagemResultado_Demanda3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV50ContagemResultado_DataDmn3) && (DateTime.MinValue==AV51ContagemResultado_DataDmn_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV50ContagemResultado_DataDmn3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV51ContagemResultado_DataDmn_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56ContagemResultado_StatusDmn3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_B42( true) ;
         }
         else
         {
            wb_table2_5_B42( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_107_B42( true) ;
         }
         else
         {
            wb_table3_107_B42( false) ;
         }
         return  ;
      }

      protected void wb_table3_107_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B42e( true) ;
         }
         else
         {
            wb_table1_2_B42e( false) ;
         }
      }

      protected void wb_table3_107_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_110_B42( true) ;
         }
         else
         {
            wb_table4_110_B42( false) ;
         }
         return  ;
      }

      protected void wb_table4_110_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_107_B42e( true) ;
         }
         else
         {
            wb_table3_107_B42e( false) ;
         }
      }

      protected void wb_table4_110_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"113\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DemandaFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DemandaFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DemandaFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Demanda_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Demanda_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Demanda_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(56), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DataDmn_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DataDmn_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DataDmn_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_ContratadaPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ContratadaPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ContratadaPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynContagemResultado_Servico_Titleformat == 0 )
               {
                  context.SendWebValue( dynContagemResultado_Servico.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynContagemResultado_Servico.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemrResultado_SistemaSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemrResultado_SistemaSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemrResultado_SistemaSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultado_StatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultado_StatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultado_StatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DemandaFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Demanda_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DataDmn_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DataDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ContratadaPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContratadaPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynContagemResultado_Servico.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynContagemResultado_Servico_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemrResultado_SistemaSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemrResultado_SistemaSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultado_StatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultado_StatusDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 113 )
         {
            wbEnd = 0;
            nRC_GXsfl_113 = (short)(nGXsfl_113_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_110_B42e( true) ;
         }
         else
         {
            wb_table4_110_B42e( false) ;
         }
      }

      protected void wb_table2_5_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_13_B42( true) ;
         }
         else
         {
            wb_table5_13_B42( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_B42e( true) ;
         }
         else
         {
            wb_table2_5_B42e( false) ;
         }
      }

      protected void wb_table5_13_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68Contratada_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_contratadacod_Internalname, "Contratada", "", "", lblFiltertextcontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultado.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_B42( true) ;
         }
         else
         {
            wb_table6_26_B42( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_B42e( true) ;
         }
         else
         {
            wb_table5_13_B42e( false) ;
         }
      }

      protected void wb_table6_26_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_35_B42( true) ;
         }
         else
         {
            wb_table7_35_B42( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_61_B42( true) ;
         }
         else
         {
            wb_table8_61_B42( false) ;
         }
         return  ;
      }

      protected void wb_table8_61_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_87_B42( true) ;
         }
         else
         {
            wb_table9_87_B42( false) ;
         }
         return  ;
      }

      protected void wb_table9_87_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_B42e( true) ;
         }
         else
         {
            wb_table6_26_B42e( false) ;
         }
      }

      protected void wb_table9_87_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda3_Internalname, StringUtil.RTrim( AV38ContagemResultado_Demanda3), StringUtil.RTrim( context.localUtil.Format( AV38ContagemResultado_Demanda3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_PromptContagemResultado.htm");
            wb_table10_93_B42( true) ;
         }
         else
         {
            wb_table10_93_B42( false) ;
         }
         return  ;
      }

      protected void wb_table10_93_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn3, cmbavContagemresultado_statusdmn3_Internalname, StringUtil.RTrim( AV56ContagemResultado_StatusDmn3), 1, cmbavContagemresultado_statusdmn3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV56ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_87_B42e( true) ;
         }
         else
         {
            wb_table9_87_B42e( false) ;
         }
      }

      protected void wb_table10_93_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn3_Internalname, context.localUtil.Format(AV50ContagemResultado_DataDmn3, "99/99/99"), context.localUtil.Format( AV50ContagemResultado_DataDmn3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to3_Internalname, context.localUtil.Format(AV51ContagemResultado_DataDmn_To3, "99/99/99"), context.localUtil.Format( AV51ContagemResultado_DataDmn_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_93_B42e( true) ;
         }
         else
         {
            wb_table10_93_B42e( false) ;
         }
      }

      protected void wb_table8_61_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda2_Internalname, StringUtil.RTrim( AV34ContagemResultado_Demanda2), StringUtil.RTrim( context.localUtil.Format( AV34ContagemResultado_Demanda2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_PromptContagemResultado.htm");
            wb_table11_67_B42( true) ;
         }
         else
         {
            wb_table11_67_B42( false) ;
         }
         return  ;
      }

      protected void wb_table11_67_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn2, cmbavContagemresultado_statusdmn2_Internalname, StringUtil.RTrim( AV55ContagemResultado_StatusDmn2), 1, cmbavContagemresultado_statusdmn2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV55ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_61_B42e( true) ;
         }
         else
         {
            wb_table8_61_B42e( false) ;
         }
      }

      protected void wb_table11_67_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn2_Internalname, context.localUtil.Format(AV46ContagemResultado_DataDmn2, "99/99/99"), context.localUtil.Format( AV46ContagemResultado_DataDmn2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to2_Internalname, context.localUtil.Format(AV47ContagemResultado_DataDmn_To2, "99/99/99"), context.localUtil.Format( AV47ContagemResultado_DataDmn_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_67_B42e( true) ;
         }
         else
         {
            wb_table11_67_B42e( false) ;
         }
      }

      protected void wb_table7_35_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda1_Internalname, StringUtil.RTrim( AV30ContagemResultado_Demanda1), StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_Demanda1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_PromptContagemResultado.htm");
            wb_table12_41_B42( true) ;
         }
         else
         {
            wb_table12_41_B42( false) ;
         }
         return  ;
      }

      protected void wb_table12_41_B42e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn1, cmbavContagemresultado_statusdmn1_Internalname, StringUtil.RTrim( AV54ContagemResultado_StatusDmn1), 1, cmbavContagemresultado_statusdmn1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptContagemResultado.htm");
            cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV54ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_B42e( true) ;
         }
         else
         {
            wb_table7_35_B42e( false) ;
         }
      }

      protected void wb_table12_41_B42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn1_Internalname, context.localUtil.Format(AV42ContagemResultado_DataDmn1, "99/99/99"), context.localUtil.Format( AV42ContagemResultado_DataDmn1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_113_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to1_Internalname, context.localUtil.Format(AV43ContagemResultado_DataDmn_To1, "99/99/99"), context.localUtil.Format( AV43ContagemResultado_DataDmn_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultado.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_41_B42e( true) ;
         }
         else
         {
            wb_table12_41_B42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultado_Codigo), 6, 0)));
         AV72InOutContagemResultado_DemandaFM = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72InOutContagemResultado_DemandaFM", AV72InOutContagemResultado_DemandaFM);
         AV69InOutContagemResultado_SistemaCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69InOutContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69InOutContagemResultado_SistemaCod), 6, 0)));
         AV70InOutModulo_Codigo = Convert.ToInt32(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70InOutModulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70InOutModulo_Codigo), 6, 0)));
         AV73InOutContagemResultado_ContratadaCod = Convert.ToInt32(getParm(obj,4));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73InOutContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73InOutContagemResultado_ContratadaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB42( ) ;
         WSB42( ) ;
         WEB42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020625221077");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultado.js", "?2020625221077");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1132( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_113_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_113_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_113_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_113_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_113_idx;
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_113_idx;
         edtContagemResultado_ContratadaPessoaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD_"+sGXsfl_113_idx;
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM_"+sGXsfl_113_idx;
         edtContagemResultado_SistemaCod_Internalname = "CONTAGEMRESULTADO_SISTEMACOD_"+sGXsfl_113_idx;
         edtModulo_Codigo_Internalname = "MODULO_CODIGO_"+sGXsfl_113_idx;
         dynContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_113_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_113_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_113_idx;
      }

      protected void SubsflControlProps_fel_1132( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_113_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_113_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_113_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_113_fel_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_113_fel_idx;
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_113_fel_idx;
         edtContagemResultado_ContratadaPessoaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD_"+sGXsfl_113_fel_idx;
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM_"+sGXsfl_113_fel_idx;
         edtContagemResultado_SistemaCod_Internalname = "CONTAGEMRESULTADO_SISTEMACOD_"+sGXsfl_113_fel_idx;
         edtModulo_Codigo_Internalname = "MODULO_CODIGO_"+sGXsfl_113_fel_idx;
         dynContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO_"+sGXsfl_113_fel_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_113_fel_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_113_fel_idx;
      }

      protected void sendrow_1132( )
      {
         SubsflControlProps_1132( ) ;
         WBB40( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_113_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_113_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_113_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 114,'',false,'',113)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV111Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_113_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)56,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A499ContagemResultado_ContratadaPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaPessoaNom_Internalname,StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom),StringUtil.RTrim( context.localUtil.Format( A500ContagemResultado_ContratadaPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A489ContagemResultado_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GXACONTAGEMRESULTADO_SERVICO_htmlB42( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_113_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_SERVICO_" + sGXsfl_113_idx;
               dynContagemResultado_Servico.Name = GXCCtl;
               dynContagemResultado_Servico.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynContagemResultado_Servico,(String)dynContagemResultado_Servico_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)),(short)1,(String)dynContagemResultado_Servico_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynContagemResultado_Servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_Servico_Internalname, "Values", (String)(dynContagemResultado_Servico.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemrResultado_SistemaSigla_Internalname,StringUtil.RTrim( A509ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemrResultado_SistemaSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_113_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_113_idx;
               cmbContagemResultado_StatusDmn.Name = GXCCtl;
               cmbContagemResultado_StatusDmn.WebTags = "";
               cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
               cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
               cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
               cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
               cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
               cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
               cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
               cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
               cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
               cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
               cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
               cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
               cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
               cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
               {
                  A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
                  n484ContagemResultado_StatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusDmn,(String)cmbContagemResultado_StatusDmn_Internalname,StringUtil.RTrim( A484ContagemResultado_StatusDmn),(short)1,(String)cmbContagemResultado_StatusDmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATADMN"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, A471ContagemResultado_DataDmn));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTRATADACOD"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_SISTEMACOD"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A489ContagemResultado_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MODULO_CODIGO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         /* End function sendrow_1132 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         edtavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblFiltertextcontagemresultado_contratadacod_Internalname = "FILTERTEXTCONTAGEMRESULTADO_CONTRATADACOD";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultado_demanda1_Internalname = "vCONTAGEMRESULTADO_DEMANDA1";
         edtavContagemresultado_datadmn1_Internalname = "vCONTAGEMRESULTADO_DATADMN1";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT1";
         edtavContagemresultado_datadmn_to1_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO1";
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1";
         cmbavContagemresultado_statusdmn1_Internalname = "vCONTAGEMRESULTADO_STATUSDMN1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultado_demanda2_Internalname = "vCONTAGEMRESULTADO_DEMANDA2";
         edtavContagemresultado_datadmn2_Internalname = "vCONTAGEMRESULTADO_DATADMN2";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT2";
         edtavContagemresultado_datadmn_to2_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO2";
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2";
         cmbavContagemresultado_statusdmn2_Internalname = "vCONTAGEMRESULTADO_STATUSDMN2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultado_demanda3_Internalname = "vCONTAGEMRESULTADO_DEMANDA3";
         edtavContagemresultado_datadmn3_Internalname = "vCONTAGEMRESULTADO_DATADMN3";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT3";
         edtavContagemresultado_datadmn_to3_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO3";
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3";
         cmbavContagemresultado_statusdmn3_Internalname = "vCONTAGEMRESULTADO_STATUSDMN3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD";
         edtContagemResultado_ContratadaPessoaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD";
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtContagemResultado_SistemaCod_Internalname = "CONTAGEMRESULTADO_SISTEMACOD";
         edtModulo_Codigo_Internalname = "MODULO_CODIGO";
         dynContagemResultado_Servico_Internalname = "CONTAGEMRESULTADO_SERVICO";
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA";
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontagemresultado_demandafm_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM";
         edtavTfcontagemresultado_demandafm_sel_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM_SEL";
         edtavTfcontagemresultado_demanda_Internalname = "vTFCONTAGEMRESULTADO_DEMANDA";
         edtavTfcontagemresultado_demanda_sel_Internalname = "vTFCONTAGEMRESULTADO_DEMANDA_SEL";
         edtavTfcontagemresultado_datadmn_Internalname = "vTFCONTAGEMRESULTADO_DATADMN";
         edtavTfcontagemresultado_datadmn_to_Internalname = "vTFCONTAGEMRESULTADO_DATADMN_TO";
         edtavDdo_contagemresultado_datadmnauxdate_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE";
         edtavDdo_contagemresultado_datadmnauxdateto_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO";
         divDdo_contagemresultado_datadmnauxdates_Internalname = "DDO_CONTAGEMRESULTADO_DATADMNAUXDATES";
         edtavTfcontagemresultado_contratadapessoanom_Internalname = "vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtavTfcontagemresultado_contratadapessoanom_sel_Internalname = "vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL";
         edtavTfcontagemresultado_servico_Internalname = "vTFCONTAGEMRESULTADO_SERVICO";
         edtavTfcontagemresultado_servico_sel_Internalname = "vTFCONTAGEMRESULTADO_SERVICO_SEL";
         edtavTfcontagemresultado_servico_seldsc_Internalname = "vTFCONTAGEMRESULTADO_SERVICO_SELDSC";
         edtavTfcontagemrresultado_sistemasigla_Internalname = "vTFCONTAGEMRRESULTADO_SISTEMASIGLA";
         edtavTfcontagemrresultado_sistemasigla_sel_Internalname = "vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL";
         Ddo_contagemresultado_demandafm_Internalname = "DDO_CONTAGEMRESULTADO_DEMANDAFM";
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_demanda_Internalname = "DDO_CONTAGEMRESULTADO_DEMANDA";
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_datadmn_Internalname = "DDO_CONTAGEMRESULTADO_DATADMN";
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_contratadapessoanom_Internalname = "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_servico_Internalname = "DDO_CONTAGEMRESULTADO_SERVICO";
         edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE";
         Ddo_contagemrresultado_sistemasigla_Internalname = "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA";
         edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_statusdmn_Internalname = "DDO_CONTAGEMRESULTADO_STATUSDMN";
         edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         dynContagemResultado_Servico_Jsonclick = "";
         edtModulo_Codigo_Jsonclick = "";
         edtContagemResultado_SistemaCod_Jsonclick = "";
         edtContagemResultado_ContratadaPessoaNom_Jsonclick = "";
         edtContagemResultado_ContratadaPessoaCod_Jsonclick = "";
         edtContagemResultado_ContratadaCod_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultado_datadmn_to1_Jsonclick = "";
         edtavContagemresultado_datadmn1_Jsonclick = "";
         cmbavContagemresultado_statusdmn1_Jsonclick = "";
         edtavContagemresultado_demanda1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultado_datadmn_to2_Jsonclick = "";
         edtavContagemresultado_datadmn2_Jsonclick = "";
         cmbavContagemresultado_statusdmn2_Jsonclick = "";
         edtavContagemresultado_demanda2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultado_datadmn_to3_Jsonclick = "";
         edtavContagemresultado_datadmn3_Jsonclick = "";
         cmbavContagemresultado_statusdmn3_Jsonclick = "";
         edtavContagemresultado_demanda3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContagemresultado_contratadacod_Jsonclick = "";
         edtavContratada_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbContagemResultado_StatusDmn_Titleformat = 0;
         edtContagemrResultado_SistemaSigla_Titleformat = 0;
         dynContagemResultado_Servico_Titleformat = 0;
         edtContagemResultado_ContratadaPessoaNom_Titleformat = 0;
         edtContagemResultado_DataDmn_Titleformat = 0;
         edtContagemResultado_Demanda_Titleformat = 0;
         edtContagemResultado_DemandaFM_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblTbjava_Visible = 1;
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavContagemresultado_statusdmn3.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
         edtavContagemresultado_demanda3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavContagemresultado_statusdmn2.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
         edtavContagemresultado_demanda2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavContagemresultado_statusdmn1.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
         edtavContagemresultado_demanda1_Visible = 1;
         cmbContagemResultado_StatusDmn.Title.Text = "Status";
         edtContagemrResultado_SistemaSigla_Title = "Sistema";
         dynContagemResultado_Servico.Title.Text = "Servi�o";
         edtContagemResultado_ContratadaPessoaNom_Title = "Contratada";
         edtContagemResultado_DataDmn_Title = "Data";
         edtContagemResultado_Demanda_Title = "OS Ref.";
         edtContagemResultado_DemandaFM_Title = "OS";
         lblTbjava_Caption = "tbJava";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemrresultado_sistemasigla_sel_Jsonclick = "";
         edtavTfcontagemrresultado_sistemasigla_sel_Visible = 1;
         edtavTfcontagemrresultado_sistemasigla_Jsonclick = "";
         edtavTfcontagemrresultado_sistemasigla_Visible = 1;
         edtavTfcontagemresultado_servico_seldsc_Jsonclick = "";
         edtavTfcontagemresultado_servico_seldsc_Visible = 1;
         edtavTfcontagemresultado_servico_sel_Jsonclick = "";
         edtavTfcontagemresultado_servico_sel_Visible = 1;
         edtavTfcontagemresultado_servico_Jsonclick = "";
         edtavTfcontagemresultado_servico_Visible = 1;
         edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick = "";
         edtavTfcontagemresultado_contratadapessoanom_sel_Visible = 1;
         edtavTfcontagemresultado_contratadapessoanom_Jsonclick = "";
         edtavTfcontagemresultado_contratadapessoanom_Visible = 1;
         edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick = "";
         edtavDdo_contagemresultado_datadmnauxdate_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_to_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_to_Visible = 1;
         edtavTfcontagemresultado_datadmn_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_Visible = 1;
         edtavTfcontagemresultado_demanda_sel_Jsonclick = "";
         edtavTfcontagemresultado_demanda_sel_Visible = 1;
         edtavTfcontagemresultado_demanda_Jsonclick = "";
         edtavTfcontagemresultado_demanda_Visible = 1;
         edtavTfcontagemresultado_demandafm_sel_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_sel_Visible = 1;
         edtavTfcontagemresultado_demandafm_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contagemresultado_statusdmn_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagemresultado_statusdmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_statusdmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_statusdmn_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_statusdmn_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contagemresultado_statusdmn_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statusdmn_Datalisttype = "FixedValues";
         Ddo_contagemresultado_statusdmn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statusdmn_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemresultado_statusdmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statusdmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_statusdmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_statusdmn_Cls = "ColumnSettings";
         Ddo_contagemresultado_statusdmn_Tooltip = "Op��es";
         Ddo_contagemresultado_statusdmn_Caption = "";
         Ddo_contagemrresultado_sistemasigla_Searchbuttontext = "Pesquisar";
         Ddo_contagemrresultado_sistemasigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemrresultado_sistemasigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemrresultado_sistemasigla_Loadingdata = "Carregando dados...";
         Ddo_contagemrresultado_sistemasigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemrresultado_sistemasigla_Sortasc = "Ordenar de A � Z";
         Ddo_contagemrresultado_sistemasigla_Datalistupdateminimumcharacters = 0;
         Ddo_contagemrresultado_sistemasigla_Datalistproc = "GetPromptContagemResultadoFilterData";
         Ddo_contagemrresultado_sistemasigla_Datalisttype = "Dynamic";
         Ddo_contagemrresultado_sistemasigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemrresultado_sistemasigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemrresultado_sistemasigla_Filtertype = "Character";
         Ddo_contagemrresultado_sistemasigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemrresultado_sistemasigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemrresultado_sistemasigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace = "";
         Ddo_contagemrresultado_sistemasigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemrresultado_sistemasigla_Cls = "ColumnSettings";
         Ddo_contagemrresultado_sistemasigla_Tooltip = "Op��es";
         Ddo_contagemrresultado_sistemasigla_Caption = "";
         Ddo_contagemresultado_servico_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_servico_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_servico_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_servico_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_servico_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_servico_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_servico_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_servico_Datalistproc = "GetPromptContagemResultadoFilterData";
         Ddo_contagemresultado_servico_Datalisttype = "Dynamic";
         Ddo_contagemresultado_servico_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servico_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_servico_Filtertype = "Character";
         Ddo_contagemresultado_servico_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servico_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servico_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servico_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_servico_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_servico_Cls = "ColumnSettings";
         Ddo_contagemresultado_servico_Tooltip = "Op��es";
         Ddo_contagemresultado_servico_Caption = "";
         Ddo_contagemresultado_contratadapessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_contratadapessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_contratadapessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_contratadapessoanom_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_contratadapessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_contratadapessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_contratadapessoanom_Datalistproc = "GetPromptContagemResultadoFilterData";
         Ddo_contagemresultado_contratadapessoanom_Datalisttype = "Dynamic";
         Ddo_contagemresultado_contratadapessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_contratadapessoanom_Filtertype = "Character";
         Ddo_contagemresultado_contratadapessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_contratadapessoanom_Cls = "ColumnSettings";
         Ddo_contagemresultado_contratadapessoanom_Tooltip = "Op��es";
         Ddo_contagemresultado_contratadapessoanom_Caption = "";
         Ddo_contagemresultado_datadmn_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_datadmn_Rangefilterto = "At�";
         Ddo_contagemresultado_datadmn_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_datadmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_datadmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_datadmn_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_datadmn_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_datadmn_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Filtertype = "Date";
         Ddo_contagemresultado_datadmn_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_datadmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_datadmn_Cls = "ColumnSettings";
         Ddo_contagemresultado_datadmn_Tooltip = "Op��es";
         Ddo_contagemresultado_datadmn_Caption = "";
         Ddo_contagemresultado_demanda_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demanda_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demanda_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demanda_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demanda_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demanda_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demanda_Datalistproc = "GetPromptContagemResultadoFilterData";
         Ddo_contagemresultado_demanda_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demanda_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demanda_Filtertype = "Character";
         Ddo_contagemresultado_demanda_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demanda_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demanda_Cls = "ColumnSettings";
         Ddo_contagemresultado_demanda_Tooltip = "Op��es";
         Ddo_contagemresultado_demanda_Caption = "";
         Ddo_contagemresultado_demandafm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demandafm_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demandafm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demandafm_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demandafm_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demandafm_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demandafm_Datalistproc = "GetPromptContagemResultadoFilterData";
         Ddo_contagemresultado_demandafm_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demandafm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Filtertype = "Character";
         Ddo_contagemresultado_demandafm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demandafm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demandafm_Cls = "ColumnSettings";
         Ddo_contagemresultado_demandafm_Tooltip = "Op��es";
         Ddo_contagemresultado_demandafm_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Prompt Contagem Resultado";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''}],oparms:[{av:'AV74ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV78ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV82ContagemResultado_DataDmnTitleFilterData',fld:'vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV88ContagemResultado_ContratadaPessoaNomTitleFilterData',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV92ContagemResultado_ServicoTitleFilterData',fld:'vCONTAGEMRESULTADO_SERVICOTITLEFILTERDATA',pic:'',nv:null},{av:'AV96ContagemrResultado_SistemaSiglaTitleFilterData',fld:'vCONTAGEMRRESULTADO_SISTEMASIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV100ContagemResultado_StatusDmnTitleFilterData',fld:'vCONTAGEMRESULTADO_STATUSDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_DataDmn_Titleformat',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Titleformat'},{av:'edtContagemResultado_DataDmn_Title',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Title'},{av:'edtContagemResultado_ContratadaPessoaNom_Titleformat',ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Titleformat'},{av:'edtContagemResultado_ContratadaPessoaNom_Title',ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Title'},{av:'dynContagemResultado_Servico'},{av:'edtContagemrResultado_SistemaSigla_Titleformat',ctrl:'CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'Titleformat'},{av:'edtContagemrResultado_SistemaSigla_Title',ctrl:'CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'Title'},{av:'cmbContagemResultado_StatusDmn'},{av:'AV107GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV108GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED","{handler:'E12B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_demandafm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demandafm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED","{handler:'E13B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_demanda_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demanda_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demanda_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED","{handler:'E14B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_datadmn_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_datadmn_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_datadmn_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED","{handler:'E15B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_contratadapessoanom_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_contratadapessoanom_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_SERVICO.ONOPTIONCLICKED","{handler:'E16B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_servico_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_servico_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_servico_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SelectedValue_get'},{av:'Ddo_contagemresultado_servico_Selectedtext_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SelectedText_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRRESULTADO_SISTEMASIGLA.ONOPTIONCLICKED","{handler:'E17B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemrresultado_sistemasigla_Activeeventkey',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'ActiveEventKey'},{av:'Ddo_contagemrresultado_sistemasigla_Filteredtext_get',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'FilteredText_get'},{av:'Ddo_contagemrresultado_sistemasigla_Selectedvalue_get',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_STATUSDMN.ONOPTIONCLICKED","{handler:'E18B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_statusdmn_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_statusdmn_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_statusdmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SortedStatus'},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_datadmn_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_servico_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SortedStatus'},{av:'Ddo_contagemrresultado_sistemasigla_Sortedstatus',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31B42',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E32B42',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',hsh:true,nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContagemResultado_Codigo',fld:'vINOUTCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV72InOutContagemResultado_DemandaFM',fld:'vINOUTCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV69InOutContagemResultado_SistemaCod',fld:'vINOUTCONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV70InOutModulo_Codigo',fld:'vINOUTMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73InOutContagemResultado_ContratadaCod',fld:'vINOUTCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E24B42',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E25B42',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E26B42',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E27B42',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E28B42',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23B42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRRESULTADO_SISTEMASIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFContagemResultado_Servico_SelDsc',fld:'vTFCONTAGEMRESULTADO_SERVICO_SELDSC',pic:'',nv:''},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'AV112Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV75TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_set'},{av:'AV76TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_set'},{av:'AV79TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demanda_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'FilteredText_set'},{av:'AV80TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demanda_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SelectedValue_set'},{av:'AV83TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'Ddo_contagemresultado_datadmn_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredText_set'},{av:'AV84TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'Ddo_contagemresultado_datadmn_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredTextTo_set'},{av:'AV89TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contagemresultado_contratadapessoanom_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'FilteredText_set'},{av:'AV90TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SelectedValue_set'},{av:'AV93TFContagemResultado_Servico',fld:'vTFCONTAGEMRESULTADO_SERVICO',pic:'@!',nv:''},{av:'Ddo_contagemresultado_servico_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'FilteredText_set'},{av:'AV94TFContagemResultado_Servico_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICO_SEL',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_servico_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_SERVICO',prop:'SelectedValue_set'},{av:'AV97TFContagemrResultado_SistemaSigla',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'Ddo_contagemrresultado_sistemasigla_Filteredtext_set',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'FilteredText_set'},{av:'AV98TFContagemrResultado_SistemaSigla_Sel',fld:'vTFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemrresultado_sistemasigla_Selectedvalue_set',ctrl:'DDO_CONTAGEMRRESULTADO_SISTEMASIGLA',prop:'SelectedValue_set'},{av:'AV102TFContagemResultado_StatusDmn_Sels',fld:'vTFCONTAGEMRESULTADO_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contagemresultado_statusdmn_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_STATUSDMN',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV42ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV54ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV46ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV47ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV55ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV50ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV51ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV56ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV72InOutContagemResultado_DemandaFM = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_demandafm_Activeeventkey = "";
         Ddo_contagemresultado_demandafm_Filteredtext_get = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_get = "";
         Ddo_contagemresultado_demanda_Activeeventkey = "";
         Ddo_contagemresultado_demanda_Filteredtext_get = "";
         Ddo_contagemresultado_demanda_Selectedvalue_get = "";
         Ddo_contagemresultado_datadmn_Activeeventkey = "";
         Ddo_contagemresultado_datadmn_Filteredtext_get = "";
         Ddo_contagemresultado_datadmn_Filteredtextto_get = "";
         Ddo_contagemresultado_contratadapessoanom_Activeeventkey = "";
         Ddo_contagemresultado_contratadapessoanom_Filteredtext_get = "";
         Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get = "";
         Ddo_contagemresultado_servico_Activeeventkey = "";
         Ddo_contagemresultado_servico_Filteredtext_get = "";
         Ddo_contagemresultado_servico_Selectedvalue_get = "";
         Ddo_contagemresultado_servico_Selectedtext_get = "";
         Ddo_contagemrresultado_sistemasigla_Activeeventkey = "";
         Ddo_contagemrresultado_sistemasigla_Filteredtext_get = "";
         Ddo_contagemrresultado_sistemasigla_Selectedvalue_get = "";
         Ddo_contagemresultado_statusdmn_Activeeventkey = "";
         Ddo_contagemresultado_statusdmn_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15DynamicFiltersSelector1 = "";
         AV30ContagemResultado_Demanda1 = "";
         AV42ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV43ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV54ContagemResultado_StatusDmn1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV34ContagemResultado_Demanda2 = "";
         AV46ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV47ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV55ContagemResultado_StatusDmn2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV38ContagemResultado_Demanda3 = "";
         AV50ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV51ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV56ContagemResultado_StatusDmn3 = "";
         AV75TFContagemResultado_DemandaFM = "";
         AV76TFContagemResultado_DemandaFM_Sel = "";
         AV79TFContagemResultado_Demanda = "";
         AV80TFContagemResultado_Demanda_Sel = "";
         AV83TFContagemResultado_DataDmn = DateTime.MinValue;
         AV84TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV89TFContagemResultado_ContratadaPessoaNom = "";
         AV90TFContagemResultado_ContratadaPessoaNom_Sel = "";
         AV93TFContagemResultado_Servico = "";
         AV97TFContagemrResultado_SistemaSigla = "";
         AV98TFContagemrResultado_SistemaSigla_Sel = "";
         AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "";
         AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace = "";
         AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "";
         AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "";
         AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace = "";
         AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace = "";
         AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace = "";
         AV105TFContagemResultado_Servico_SelDsc = "";
         AV102TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV112Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV104DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV74ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82ContagemResultado_DataDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88ContagemResultado_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV92ContagemResultado_ServicoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96ContagemrResultado_SistemaSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100ContagemResultado_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         Ddo_contagemresultado_demanda_Filteredtext_set = "";
         Ddo_contagemresultado_demanda_Selectedvalue_set = "";
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         Ddo_contagemresultado_datadmn_Filteredtext_set = "";
         Ddo_contagemresultado_datadmn_Filteredtextto_set = "";
         Ddo_contagemresultado_datadmn_Sortedstatus = "";
         Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = "";
         Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = "";
         Ddo_contagemresultado_contratadapessoanom_Sortedstatus = "";
         Ddo_contagemresultado_servico_Filteredtext_set = "";
         Ddo_contagemresultado_servico_Selectedvalue_set = "";
         Ddo_contagemresultado_servico_Sortedstatus = "";
         Ddo_contagemrresultado_sistemasigla_Filteredtext_set = "";
         Ddo_contagemrresultado_sistemasigla_Selectedvalue_set = "";
         Ddo_contagemrresultado_sistemasigla_Sortedstatus = "";
         Ddo_contagemresultado_statusdmn_Selectedvalue_set = "";
         Ddo_contagemresultado_statusdmn_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV85DDO_ContagemResultado_DataDmnAuxDate = DateTime.MinValue;
         AV86DDO_ContagemResultado_DataDmnAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV111Select_GXI = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A500ContagemResultado_ContratadaPessoaNom = "";
         A509ContagemrResultado_SistemaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00B42_A40Contratada_PessoaCod = new int[1] ;
         H00B42_A39Contratada_Codigo = new int[1] ;
         H00B42_A41Contratada_PessoaNom = new String[] {""} ;
         H00B42_n41Contratada_PessoaNom = new bool[] {false} ;
         H00B42_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00B42_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00B43_A601ContagemResultado_Servico = new int[1] ;
         H00B43_n601ContagemResultado_Servico = new bool[] {false} ;
         H00B43_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00B43_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV30ContagemResultado_Demanda1 = "";
         lV34ContagemResultado_Demanda2 = "";
         lV38ContagemResultado_Demanda3 = "";
         lV75TFContagemResultado_DemandaFM = "";
         lV79TFContagemResultado_Demanda = "";
         lV89TFContagemResultado_ContratadaPessoaNom = "";
         lV93TFContagemResultado_Servico = "";
         lV97TFContagemrResultado_SistemaSigla = "";
         A605Servico_Sigla = "";
         H00B44_A127Sistema_Codigo = new int[1] ;
         H00B44_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00B44_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00B44_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00B44_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00B44_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00B44_A605Servico_Sigla = new String[] {""} ;
         H00B44_n605Servico_Sigla = new bool[] {false} ;
         H00B44_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00B44_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00B44_A1583ContagemResultado_TipoRegistro = new short[1] ;
         H00B44_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00B44_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00B44_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00B44_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00B44_A601ContagemResultado_Servico = new int[1] ;
         H00B44_n601ContagemResultado_Servico = new bool[] {false} ;
         H00B44_A146Modulo_Codigo = new int[1] ;
         H00B44_n146Modulo_Codigo = new bool[] {false} ;
         H00B44_A489ContagemResultado_SistemaCod = new int[1] ;
         H00B44_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00B44_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00B44_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00B44_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00B44_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00B44_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00B44_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00B44_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00B44_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B44_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B44_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00B44_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00B44_A456ContagemResultado_Codigo = new int[1] ;
         H00B45_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV101TFContagemResultado_StatusDmn_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblFiltertextcontagemresultado_contratadacod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultado__default(),
            new Object[][] {
                new Object[] {
               H00B42_A40Contratada_PessoaCod, H00B42_A39Contratada_Codigo, H00B42_A41Contratada_PessoaNom, H00B42_n41Contratada_PessoaNom, H00B42_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00B43_A601ContagemResultado_Servico, H00B43_A801ContagemResultado_ServicoSigla, H00B43_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               H00B44_A127Sistema_Codigo, H00B44_A135Sistema_AreaTrabalhoCod, H00B44_A830AreaTrabalho_ServicoPadrao, H00B44_n830AreaTrabalho_ServicoPadrao, H00B44_A1553ContagemResultado_CntSrvCod, H00B44_n1553ContagemResultado_CntSrvCod, H00B44_A605Servico_Sigla, H00B44_n605Servico_Sigla, H00B44_A52Contratada_AreaTrabalhoCod, H00B44_n52Contratada_AreaTrabalhoCod,
               H00B44_A1583ContagemResultado_TipoRegistro, H00B44_A484ContagemResultado_StatusDmn, H00B44_n484ContagemResultado_StatusDmn, H00B44_A509ContagemrResultado_SistemaSigla, H00B44_n509ContagemrResultado_SistemaSigla, H00B44_A601ContagemResultado_Servico, H00B44_n601ContagemResultado_Servico, H00B44_A146Modulo_Codigo, H00B44_n146Modulo_Codigo, H00B44_A489ContagemResultado_SistemaCod,
               H00B44_n489ContagemResultado_SistemaCod, H00B44_A500ContagemResultado_ContratadaPessoaNom, H00B44_n500ContagemResultado_ContratadaPessoaNom, H00B44_A499ContagemResultado_ContratadaPessoaCod, H00B44_n499ContagemResultado_ContratadaPessoaCod, H00B44_A490ContagemResultado_ContratadaCod, H00B44_n490ContagemResultado_ContratadaCod, H00B44_A471ContagemResultado_DataDmn, H00B44_A457ContagemResultado_Demanda, H00B44_n457ContagemResultado_Demanda,
               H00B44_A493ContagemResultado_DemandaFM, H00B44_n493ContagemResultado_DemandaFM, H00B44_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00B45_AGRID_nRecordCount
               }
            }
         );
         AV112Pgmname = "PromptContagemResultado";
         /* GeneXus formulas. */
         AV112Pgmname = "PromptContagemResultado";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_113 ;
      private short nGXsfl_113_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_113_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short edtContagemResultado_DemandaFM_Titleformat ;
      private short edtContagemResultado_Demanda_Titleformat ;
      private short edtContagemResultado_DataDmn_Titleformat ;
      private short edtContagemResultado_ContratadaPessoaNom_Titleformat ;
      private short dynContagemResultado_Servico_Titleformat ;
      private short edtContagemrResultado_SistemaSigla_Titleformat ;
      private short cmbContagemResultado_StatusDmn_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContagemResultado_Codigo ;
      private int AV69InOutContagemResultado_SistemaCod ;
      private int AV70InOutModulo_Codigo ;
      private int AV73InOutContagemResultado_ContratadaCod ;
      private int wcpOAV7InOutContagemResultado_Codigo ;
      private int wcpOAV69InOutContagemResultado_SistemaCod ;
      private int wcpOAV70InOutModulo_Codigo ;
      private int wcpOAV73InOutContagemResultado_ContratadaCod ;
      private int subGrid_Rows ;
      private int AV94TFContagemResultado_Servico_Sel ;
      private int AV68Contratada_AreaTrabalhoCod ;
      private int AV71ContagemResultado_ContratadaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_servico_Datalistupdateminimumcharacters ;
      private int Ddo_contagemrresultado_sistemasigla_Datalistupdateminimumcharacters ;
      private int edtavTfcontagemresultado_demandafm_Visible ;
      private int edtavTfcontagemresultado_demandafm_sel_Visible ;
      private int edtavTfcontagemresultado_demanda_Visible ;
      private int edtavTfcontagemresultado_demanda_sel_Visible ;
      private int edtavTfcontagemresultado_datadmn_Visible ;
      private int edtavTfcontagemresultado_datadmn_to_Visible ;
      private int edtavTfcontagemresultado_contratadapessoanom_Visible ;
      private int edtavTfcontagemresultado_contratadapessoanom_sel_Visible ;
      private int edtavTfcontagemresultado_servico_Visible ;
      private int edtavTfcontagemresultado_servico_sel_Visible ;
      private int edtavTfcontagemresultado_servico_seldsc_Visible ;
      private int edtavTfcontagemrresultado_sistemasigla_Visible ;
      private int edtavTfcontagemrresultado_sistemasigla_sel_Visible ;
      private int edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV102TFContagemResultado_StatusDmn_Sels_Count ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int edtavOrdereddsc_Visible ;
      private int lblTbjava_Visible ;
      private int AV106PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagemresultado_demanda1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible ;
      private int edtavContagemresultado_demanda2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible ;
      private int edtavContagemresultado_demanda3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV107GridCurrentPage ;
      private long AV108GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_demandafm_Activeeventkey ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_get ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_get ;
      private String Ddo_contagemresultado_demanda_Activeeventkey ;
      private String Ddo_contagemresultado_demanda_Filteredtext_get ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_get ;
      private String Ddo_contagemresultado_datadmn_Activeeventkey ;
      private String Ddo_contagemresultado_datadmn_Filteredtext_get ;
      private String Ddo_contagemresultado_datadmn_Filteredtextto_get ;
      private String Ddo_contagemresultado_contratadapessoanom_Activeeventkey ;
      private String Ddo_contagemresultado_contratadapessoanom_Filteredtext_get ;
      private String Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get ;
      private String Ddo_contagemresultado_servico_Activeeventkey ;
      private String Ddo_contagemresultado_servico_Filteredtext_get ;
      private String Ddo_contagemresultado_servico_Selectedvalue_get ;
      private String Ddo_contagemresultado_servico_Selectedtext_get ;
      private String Ddo_contagemrresultado_sistemasigla_Activeeventkey ;
      private String Ddo_contagemrresultado_sistemasigla_Filteredtext_get ;
      private String Ddo_contagemrresultado_sistemasigla_Selectedvalue_get ;
      private String Ddo_contagemresultado_statusdmn_Activeeventkey ;
      private String Ddo_contagemresultado_statusdmn_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_113_idx="0001" ;
      private String AV30ContagemResultado_Demanda1 ;
      private String AV54ContagemResultado_StatusDmn1 ;
      private String AV34ContagemResultado_Demanda2 ;
      private String AV55ContagemResultado_StatusDmn2 ;
      private String AV38ContagemResultado_Demanda3 ;
      private String AV56ContagemResultado_StatusDmn3 ;
      private String AV89TFContagemResultado_ContratadaPessoaNom ;
      private String AV90TFContagemResultado_ContratadaPessoaNom_Sel ;
      private String AV93TFContagemResultado_Servico ;
      private String AV97TFContagemrResultado_SistemaSigla ;
      private String AV98TFContagemrResultado_SistemaSigla_Sel ;
      private String AV112Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_demandafm_Caption ;
      private String Ddo_contagemresultado_demandafm_Tooltip ;
      private String Ddo_contagemresultado_demandafm_Cls ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_set ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_set ;
      private String Ddo_contagemresultado_demandafm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demandafm_Sortedstatus ;
      private String Ddo_contagemresultado_demandafm_Filtertype ;
      private String Ddo_contagemresultado_demandafm_Datalisttype ;
      private String Ddo_contagemresultado_demandafm_Datalistproc ;
      private String Ddo_contagemresultado_demandafm_Sortasc ;
      private String Ddo_contagemresultado_demandafm_Sortdsc ;
      private String Ddo_contagemresultado_demandafm_Loadingdata ;
      private String Ddo_contagemresultado_demandafm_Cleanfilter ;
      private String Ddo_contagemresultado_demandafm_Noresultsfound ;
      private String Ddo_contagemresultado_demandafm_Searchbuttontext ;
      private String Ddo_contagemresultado_demanda_Caption ;
      private String Ddo_contagemresultado_demanda_Tooltip ;
      private String Ddo_contagemresultado_demanda_Cls ;
      private String Ddo_contagemresultado_demanda_Filteredtext_set ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_set ;
      private String Ddo_contagemresultado_demanda_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demanda_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demanda_Sortedstatus ;
      private String Ddo_contagemresultado_demanda_Filtertype ;
      private String Ddo_contagemresultado_demanda_Datalisttype ;
      private String Ddo_contagemresultado_demanda_Datalistproc ;
      private String Ddo_contagemresultado_demanda_Sortasc ;
      private String Ddo_contagemresultado_demanda_Sortdsc ;
      private String Ddo_contagemresultado_demanda_Loadingdata ;
      private String Ddo_contagemresultado_demanda_Cleanfilter ;
      private String Ddo_contagemresultado_demanda_Noresultsfound ;
      private String Ddo_contagemresultado_demanda_Searchbuttontext ;
      private String Ddo_contagemresultado_datadmn_Caption ;
      private String Ddo_contagemresultado_datadmn_Tooltip ;
      private String Ddo_contagemresultado_datadmn_Cls ;
      private String Ddo_contagemresultado_datadmn_Filteredtext_set ;
      private String Ddo_contagemresultado_datadmn_Filteredtextto_set ;
      private String Ddo_contagemresultado_datadmn_Dropdownoptionstype ;
      private String Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_datadmn_Sortedstatus ;
      private String Ddo_contagemresultado_datadmn_Filtertype ;
      private String Ddo_contagemresultado_datadmn_Sortasc ;
      private String Ddo_contagemresultado_datadmn_Sortdsc ;
      private String Ddo_contagemresultado_datadmn_Cleanfilter ;
      private String Ddo_contagemresultado_datadmn_Rangefilterfrom ;
      private String Ddo_contagemresultado_datadmn_Rangefilterto ;
      private String Ddo_contagemresultado_datadmn_Searchbuttontext ;
      private String Ddo_contagemresultado_contratadapessoanom_Caption ;
      private String Ddo_contagemresultado_contratadapessoanom_Tooltip ;
      private String Ddo_contagemresultado_contratadapessoanom_Cls ;
      private String Ddo_contagemresultado_contratadapessoanom_Filteredtext_set ;
      private String Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set ;
      private String Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype ;
      private String Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_contratadapessoanom_Sortedstatus ;
      private String Ddo_contagemresultado_contratadapessoanom_Filtertype ;
      private String Ddo_contagemresultado_contratadapessoanom_Datalisttype ;
      private String Ddo_contagemresultado_contratadapessoanom_Datalistproc ;
      private String Ddo_contagemresultado_contratadapessoanom_Sortasc ;
      private String Ddo_contagemresultado_contratadapessoanom_Sortdsc ;
      private String Ddo_contagemresultado_contratadapessoanom_Loadingdata ;
      private String Ddo_contagemresultado_contratadapessoanom_Cleanfilter ;
      private String Ddo_contagemresultado_contratadapessoanom_Noresultsfound ;
      private String Ddo_contagemresultado_contratadapessoanom_Searchbuttontext ;
      private String Ddo_contagemresultado_servico_Caption ;
      private String Ddo_contagemresultado_servico_Tooltip ;
      private String Ddo_contagemresultado_servico_Cls ;
      private String Ddo_contagemresultado_servico_Filteredtext_set ;
      private String Ddo_contagemresultado_servico_Selectedvalue_set ;
      private String Ddo_contagemresultado_servico_Dropdownoptionstype ;
      private String Ddo_contagemresultado_servico_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_servico_Sortedstatus ;
      private String Ddo_contagemresultado_servico_Filtertype ;
      private String Ddo_contagemresultado_servico_Datalisttype ;
      private String Ddo_contagemresultado_servico_Datalistproc ;
      private String Ddo_contagemresultado_servico_Sortasc ;
      private String Ddo_contagemresultado_servico_Sortdsc ;
      private String Ddo_contagemresultado_servico_Loadingdata ;
      private String Ddo_contagemresultado_servico_Cleanfilter ;
      private String Ddo_contagemresultado_servico_Noresultsfound ;
      private String Ddo_contagemresultado_servico_Searchbuttontext ;
      private String Ddo_contagemrresultado_sistemasigla_Caption ;
      private String Ddo_contagemrresultado_sistemasigla_Tooltip ;
      private String Ddo_contagemrresultado_sistemasigla_Cls ;
      private String Ddo_contagemrresultado_sistemasigla_Filteredtext_set ;
      private String Ddo_contagemrresultado_sistemasigla_Selectedvalue_set ;
      private String Ddo_contagemrresultado_sistemasigla_Dropdownoptionstype ;
      private String Ddo_contagemrresultado_sistemasigla_Titlecontrolidtoreplace ;
      private String Ddo_contagemrresultado_sistemasigla_Sortedstatus ;
      private String Ddo_contagemrresultado_sistemasigla_Filtertype ;
      private String Ddo_contagemrresultado_sistemasigla_Datalisttype ;
      private String Ddo_contagemrresultado_sistemasigla_Datalistproc ;
      private String Ddo_contagemrresultado_sistemasigla_Sortasc ;
      private String Ddo_contagemrresultado_sistemasigla_Sortdsc ;
      private String Ddo_contagemrresultado_sistemasigla_Loadingdata ;
      private String Ddo_contagemrresultado_sistemasigla_Cleanfilter ;
      private String Ddo_contagemrresultado_sistemasigla_Noresultsfound ;
      private String Ddo_contagemrresultado_sistemasigla_Searchbuttontext ;
      private String Ddo_contagemresultado_statusdmn_Caption ;
      private String Ddo_contagemresultado_statusdmn_Tooltip ;
      private String Ddo_contagemresultado_statusdmn_Cls ;
      private String Ddo_contagemresultado_statusdmn_Selectedvalue_set ;
      private String Ddo_contagemresultado_statusdmn_Dropdownoptionstype ;
      private String Ddo_contagemresultado_statusdmn_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_statusdmn_Sortedstatus ;
      private String Ddo_contagemresultado_statusdmn_Datalisttype ;
      private String Ddo_contagemresultado_statusdmn_Datalistfixedvalues ;
      private String Ddo_contagemresultado_statusdmn_Sortasc ;
      private String Ddo_contagemresultado_statusdmn_Sortdsc ;
      private String Ddo_contagemresultado_statusdmn_Cleanfilter ;
      private String Ddo_contagemresultado_statusdmn_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontagemresultado_demandafm_Internalname ;
      private String edtavTfcontagemresultado_demandafm_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_sel_Internalname ;
      private String edtavTfcontagemresultado_demandafm_sel_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_Internalname ;
      private String edtavTfcontagemresultado_demanda_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_sel_Internalname ;
      private String edtavTfcontagemresultado_demanda_sel_Jsonclick ;
      private String edtavTfcontagemresultado_datadmn_Internalname ;
      private String edtavTfcontagemresultado_datadmn_Jsonclick ;
      private String edtavTfcontagemresultado_datadmn_to_Internalname ;
      private String edtavTfcontagemresultado_datadmn_to_Jsonclick ;
      private String divDdo_contagemresultado_datadmnauxdates_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdate_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdate_Jsonclick ;
      private String edtavDdo_contagemresultado_datadmnauxdateto_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick ;
      private String edtavTfcontagemresultado_contratadapessoanom_Internalname ;
      private String edtavTfcontagemresultado_contratadapessoanom_Jsonclick ;
      private String edtavTfcontagemresultado_contratadapessoanom_sel_Internalname ;
      private String edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick ;
      private String edtavTfcontagemresultado_servico_Internalname ;
      private String edtavTfcontagemresultado_servico_Jsonclick ;
      private String edtavTfcontagemresultado_servico_sel_Internalname ;
      private String edtavTfcontagemresultado_servico_sel_Jsonclick ;
      private String edtavTfcontagemresultado_servico_seldsc_Internalname ;
      private String edtavTfcontagemresultado_servico_seldsc_Jsonclick ;
      private String edtavTfcontagemrresultado_sistemasigla_Internalname ;
      private String edtavTfcontagemrresultado_sistemasigla_Jsonclick ;
      private String edtavTfcontagemrresultado_sistemasigla_sel_Internalname ;
      private String edtavTfcontagemrresultado_sistemasigla_sel_Jsonclick ;
      private String edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_servicotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemrresultado_sistemasiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_statusdmntitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_ContratadaCod_Internalname ;
      private String edtContagemResultado_ContratadaPessoaCod_Internalname ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String edtContagemResultado_ContratadaPessoaNom_Internalname ;
      private String edtContagemResultado_SistemaCod_Internalname ;
      private String edtModulo_Codigo_Internalname ;
      private String dynContagemResultado_Servico_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV30ContagemResultado_Demanda1 ;
      private String lV34ContagemResultado_Demanda2 ;
      private String lV38ContagemResultado_Demanda3 ;
      private String lV89TFContagemResultado_ContratadaPessoaNom ;
      private String lV93TFContagemResultado_Servico ;
      private String lV97TFContagemrResultado_SistemaSigla ;
      private String A605Servico_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavContratada_areatrabalhocod_Internalname ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultado_demanda1_Internalname ;
      private String edtavContagemresultado_datadmn1_Internalname ;
      private String edtavContagemresultado_datadmn_to1_Internalname ;
      private String cmbavContagemresultado_statusdmn1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultado_demanda2_Internalname ;
      private String edtavContagemresultado_datadmn2_Internalname ;
      private String edtavContagemresultado_datadmn_to2_Internalname ;
      private String cmbavContagemresultado_statusdmn2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultado_demanda3_Internalname ;
      private String edtavContagemresultado_datadmn3_Internalname ;
      private String edtavContagemresultado_datadmn_to3_Internalname ;
      private String cmbavContagemresultado_statusdmn3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_demandafm_Internalname ;
      private String Ddo_contagemresultado_demanda_Internalname ;
      private String Ddo_contagemresultado_datadmn_Internalname ;
      private String Ddo_contagemresultado_contratadapessoanom_Internalname ;
      private String Ddo_contagemresultado_servico_Internalname ;
      private String Ddo_contagemrresultado_sistemasigla_Internalname ;
      private String Ddo_contagemresultado_statusdmn_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String edtContagemResultado_DemandaFM_Title ;
      private String edtContagemResultado_Demanda_Title ;
      private String edtContagemResultado_DataDmn_Title ;
      private String edtContagemResultado_ContratadaPessoaNom_Title ;
      private String edtContagemrResultado_SistemaSigla_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String edtavContratada_areatrabalhocod_Jsonclick ;
      private String lblFiltertextcontagemresultado_contratadacod_Internalname ;
      private String lblFiltertextcontagemresultado_contratadacod_Jsonclick ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultado_demanda3_Jsonclick ;
      private String cmbavContagemresultado_statusdmn3_Jsonclick ;
      private String edtavContagemresultado_datadmn3_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultado_datadmn_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultado_demanda2_Jsonclick ;
      private String cmbavContagemresultado_statusdmn2_Jsonclick ;
      private String edtavContagemresultado_datadmn2_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultado_datadmn_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultado_demanda1_Jsonclick ;
      private String cmbavContagemresultado_statusdmn1_Jsonclick ;
      private String edtavContagemresultado_datadmn1_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultado_datadmn_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultado_datadmn_to1_Jsonclick ;
      private String sGXsfl_113_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_ContratadaCod_Jsonclick ;
      private String edtContagemResultado_ContratadaPessoaCod_Jsonclick ;
      private String edtContagemResultado_ContratadaPessoaNom_Jsonclick ;
      private String edtContagemResultado_SistemaCod_Jsonclick ;
      private String edtModulo_Codigo_Jsonclick ;
      private String dynContagemResultado_Servico_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV42ContagemResultado_DataDmn1 ;
      private DateTime AV43ContagemResultado_DataDmn_To1 ;
      private DateTime AV46ContagemResultado_DataDmn2 ;
      private DateTime AV47ContagemResultado_DataDmn_To2 ;
      private DateTime AV50ContagemResultado_DataDmn3 ;
      private DateTime AV51ContagemResultado_DataDmn_To3 ;
      private DateTime AV83TFContagemResultado_DataDmn ;
      private DateTime AV84TFContagemResultado_DataDmn_To ;
      private DateTime AV85DDO_ContagemResultado_DataDmnAuxDate ;
      private DateTime AV86DDO_ContagemResultado_DataDmnAuxDateTo ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_demandafm_Includesortasc ;
      private bool Ddo_contagemresultado_demandafm_Includesortdsc ;
      private bool Ddo_contagemresultado_demandafm_Includefilter ;
      private bool Ddo_contagemresultado_demandafm_Filterisrange ;
      private bool Ddo_contagemresultado_demandafm_Includedatalist ;
      private bool Ddo_contagemresultado_demanda_Includesortasc ;
      private bool Ddo_contagemresultado_demanda_Includesortdsc ;
      private bool Ddo_contagemresultado_demanda_Includefilter ;
      private bool Ddo_contagemresultado_demanda_Filterisrange ;
      private bool Ddo_contagemresultado_demanda_Includedatalist ;
      private bool Ddo_contagemresultado_datadmn_Includesortasc ;
      private bool Ddo_contagemresultado_datadmn_Includesortdsc ;
      private bool Ddo_contagemresultado_datadmn_Includefilter ;
      private bool Ddo_contagemresultado_datadmn_Filterisrange ;
      private bool Ddo_contagemresultado_datadmn_Includedatalist ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includesortasc ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includesortdsc ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includefilter ;
      private bool Ddo_contagemresultado_contratadapessoanom_Filterisrange ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includedatalist ;
      private bool Ddo_contagemresultado_servico_Includesortasc ;
      private bool Ddo_contagemresultado_servico_Includesortdsc ;
      private bool Ddo_contagemresultado_servico_Includefilter ;
      private bool Ddo_contagemresultado_servico_Filterisrange ;
      private bool Ddo_contagemresultado_servico_Includedatalist ;
      private bool Ddo_contagemrresultado_sistemasigla_Includesortasc ;
      private bool Ddo_contagemrresultado_sistemasigla_Includesortdsc ;
      private bool Ddo_contagemrresultado_sistemasigla_Includefilter ;
      private bool Ddo_contagemrresultado_sistemasigla_Filterisrange ;
      private bool Ddo_contagemrresultado_sistemasigla_Includedatalist ;
      private bool Ddo_contagemresultado_statusdmn_Includesortasc ;
      private bool Ddo_contagemresultado_statusdmn_Includesortdsc ;
      private bool Ddo_contagemresultado_statusdmn_Includefilter ;
      private bool Ddo_contagemresultado_statusdmn_Includedatalist ;
      private bool Ddo_contagemresultado_statusdmn_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n601ContagemResultado_Servico ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n605Servico_Sigla ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV101TFContagemResultado_StatusDmn_SelsJson ;
      private String AV72InOutContagemResultado_DemandaFM ;
      private String wcpOAV72InOutContagemResultado_DemandaFM ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV75TFContagemResultado_DemandaFM ;
      private String AV76TFContagemResultado_DemandaFM_Sel ;
      private String AV79TFContagemResultado_Demanda ;
      private String AV80TFContagemResultado_Demanda_Sel ;
      private String AV77ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ;
      private String AV81ddo_ContagemResultado_DemandaTitleControlIdToReplace ;
      private String AV87ddo_ContagemResultado_DataDmnTitleControlIdToReplace ;
      private String AV91ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace ;
      private String AV95ddo_ContagemResultado_ServicoTitleControlIdToReplace ;
      private String AV99ddo_ContagemrResultado_SistemaSiglaTitleControlIdToReplace ;
      private String AV103ddo_ContagemResultado_StatusDmnTitleControlIdToReplace ;
      private String AV105TFContagemResultado_Servico_SelDsc ;
      private String AV111Select_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String lV75TFContagemResultado_DemandaFM ;
      private String lV79TFContagemResultado_Demanda ;
      private String AV28Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagemResultado_Codigo ;
      private String aP1_InOutContagemResultado_DemandaFM ;
      private int aP2_InOutContagemResultado_SistemaCod ;
      private int aP3_InOutModulo_Codigo ;
      private int aP4_InOutContagemResultado_ContratadaCod ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavContagemresultado_statusdmn1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavContagemresultado_statusdmn2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavContagemresultado_statusdmn3 ;
      private GXCombobox dynContagemResultado_Servico ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00B42_A40Contratada_PessoaCod ;
      private int[] H00B42_A39Contratada_Codigo ;
      private String[] H00B42_A41Contratada_PessoaNom ;
      private bool[] H00B42_n41Contratada_PessoaNom ;
      private int[] H00B42_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00B42_n52Contratada_AreaTrabalhoCod ;
      private int[] H00B43_A601ContagemResultado_Servico ;
      private bool[] H00B43_n601ContagemResultado_Servico ;
      private String[] H00B43_A801ContagemResultado_ServicoSigla ;
      private bool[] H00B43_n801ContagemResultado_ServicoSigla ;
      private int[] H00B44_A127Sistema_Codigo ;
      private int[] H00B44_A135Sistema_AreaTrabalhoCod ;
      private int[] H00B44_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00B44_n830AreaTrabalho_ServicoPadrao ;
      private int[] H00B44_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00B44_n1553ContagemResultado_CntSrvCod ;
      private String[] H00B44_A605Servico_Sigla ;
      private bool[] H00B44_n605Servico_Sigla ;
      private int[] H00B44_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00B44_n52Contratada_AreaTrabalhoCod ;
      private short[] H00B44_A1583ContagemResultado_TipoRegistro ;
      private String[] H00B44_A484ContagemResultado_StatusDmn ;
      private bool[] H00B44_n484ContagemResultado_StatusDmn ;
      private String[] H00B44_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00B44_n509ContagemrResultado_SistemaSigla ;
      private int[] H00B44_A601ContagemResultado_Servico ;
      private bool[] H00B44_n601ContagemResultado_Servico ;
      private int[] H00B44_A146Modulo_Codigo ;
      private bool[] H00B44_n146Modulo_Codigo ;
      private int[] H00B44_A489ContagemResultado_SistemaCod ;
      private bool[] H00B44_n489ContagemResultado_SistemaCod ;
      private String[] H00B44_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00B44_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] H00B44_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00B44_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00B44_A490ContagemResultado_ContratadaCod ;
      private bool[] H00B44_n490ContagemResultado_ContratadaCod ;
      private DateTime[] H00B44_A471ContagemResultado_DataDmn ;
      private String[] H00B44_A457ContagemResultado_Demanda ;
      private bool[] H00B44_n457ContagemResultado_Demanda ;
      private String[] H00B44_A493ContagemResultado_DemandaFM ;
      private bool[] H00B44_n493ContagemResultado_DemandaFM ;
      private int[] H00B44_A456ContagemResultado_Codigo ;
      private long[] H00B45_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV102TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74ContagemResultado_DemandaFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78ContagemResultado_DemandaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82ContagemResultado_DataDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV88ContagemResultado_ContratadaPessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV92ContagemResultado_ServicoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV96ContagemrResultado_SistemaSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV100ContagemResultado_StatusDmnTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV104DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontagemresultado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00B44( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV102TFContagemResultado_StatusDmn_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV30ContagemResultado_Demanda1 ,
                                             DateTime AV42ContagemResultado_DataDmn1 ,
                                             DateTime AV43ContagemResultado_DataDmn_To1 ,
                                             String AV54ContagemResultado_StatusDmn1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV34ContagemResultado_Demanda2 ,
                                             DateTime AV46ContagemResultado_DataDmn2 ,
                                             DateTime AV47ContagemResultado_DataDmn_To2 ,
                                             String AV55ContagemResultado_StatusDmn2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV38ContagemResultado_Demanda3 ,
                                             DateTime AV50ContagemResultado_DataDmn3 ,
                                             DateTime AV51ContagemResultado_DataDmn_To3 ,
                                             String AV56ContagemResultado_StatusDmn3 ,
                                             String AV76TFContagemResultado_DemandaFM_Sel ,
                                             String AV75TFContagemResultado_DemandaFM ,
                                             String AV80TFContagemResultado_Demanda_Sel ,
                                             String AV79TFContagemResultado_Demanda ,
                                             DateTime AV83TFContagemResultado_DataDmn ,
                                             DateTime AV84TFContagemResultado_DataDmn_To ,
                                             String AV90TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV89TFContagemResultado_ContratadaPessoaNom ,
                                             int AV94TFContagemResultado_Servico_Sel ,
                                             String AV93TFContagemResultado_Servico ,
                                             String AV98TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV97TFContagemrResultado_SistemaSigla ,
                                             int AV102TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [46] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Sistema_Codigo], T4.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T5.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Servico_Sigla], T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_StatusDmn], T7.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[Modulo_Codigo], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPe, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo]";
         sFromString = " FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T4.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T5.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T7 WITH (NOLOCK) ON T7.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         sWhereString = sWhereString + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int2[1] = 1;
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int2[3] = 1;
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int2[5] = 1;
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV42ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV42ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV43ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV43ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV54ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int2[10] = 1;
            GXv_int2[11] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int2[12] = 1;
            GXv_int2[13] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int2[14] = 1;
            GXv_int2[15] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV46ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV46ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV47ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV55ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int2[19] = 1;
            GXv_int2[20] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int2[21] = 1;
            GXv_int2[22] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int2[23] = 1;
            GXv_int2[24] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV50ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV51ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV51ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV75TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV76TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV79TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV80TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV83TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV83TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV84TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV84TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV89TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV90TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( (0==AV94TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV93TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( ! (0==AV94TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV94TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] like @lV97TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] = @AV98TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( AV102TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV102TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int2[40] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Demanda]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Demanda] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_DataDmn]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_DemandaFM] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T9.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T9.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Codigo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T7.[Sistema_Sigla]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T7.[Sistema_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00B45( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV102TFContagemResultado_StatusDmn_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV30ContagemResultado_Demanda1 ,
                                             DateTime AV42ContagemResultado_DataDmn1 ,
                                             DateTime AV43ContagemResultado_DataDmn_To1 ,
                                             String AV54ContagemResultado_StatusDmn1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV34ContagemResultado_Demanda2 ,
                                             DateTime AV46ContagemResultado_DataDmn2 ,
                                             DateTime AV47ContagemResultado_DataDmn_To2 ,
                                             String AV55ContagemResultado_StatusDmn2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV38ContagemResultado_Demanda3 ,
                                             DateTime AV50ContagemResultado_DataDmn3 ,
                                             DateTime AV51ContagemResultado_DataDmn_To3 ,
                                             String AV56ContagemResultado_StatusDmn3 ,
                                             String AV76TFContagemResultado_DemandaFM_Sel ,
                                             String AV75TFContagemResultado_DemandaFM ,
                                             String AV80TFContagemResultado_Demanda_Sel ,
                                             String AV79TFContagemResultado_Demanda ,
                                             DateTime AV83TFContagemResultado_DataDmn ,
                                             DateTime AV84TFContagemResultado_DataDmn_To ,
                                             String AV90TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV89TFContagemResultado_ContratadaPessoaNom ,
                                             int AV94TFContagemResultado_Servico_Sel ,
                                             String AV93TFContagemResultado_Servico ,
                                             String AV98TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV97TFContagemrResultado_SistemaSigla ,
                                             int AV102TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [41] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T9 WITH (NOLOCK) ON T9.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T8 WITH (NOLOCK) ON T8.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T6.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int4[1] = 1;
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int4[3] = 1;
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV30ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV30ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int4[5] = 1;
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV42ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV42ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV43ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV43ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV54ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int4[10] = 1;
            GXv_int4[11] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int4[12] = 1;
            GXv_int4[13] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV34ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV34ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int4[14] = 1;
            GXv_int4[15] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV46ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV46ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV47ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV55ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int4[19] = 1;
            GXv_int4[20] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int4[21] = 1;
            GXv_int4[22] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV38ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV38ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int4[23] = 1;
            GXv_int4[24] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV50ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV51ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV51ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV75TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV76TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV79TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV80TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV83TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV83TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV84TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV84TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Pessoa_Nome] like @lV89TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Pessoa_Nome] = @AV90TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( (0==AV94TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV93TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( ! (0==AV94TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T9.[Servico_Codigo] = @AV94TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int4[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] like @lV97TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int4[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] = @AV98TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int4[39] = 1;
         }
         if ( AV102TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV102TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int4[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00B44(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (short)dynConstraints[44] , (bool)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (short)dynConstraints[48] );
               case 3 :
                     return conditional_H00B45(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (short)dynConstraints[44] , (bool)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (short)dynConstraints[48] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B42 ;
          prmH00B42 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B43 ;
          prmH00B43 = new Object[] {
          } ;
          Object[] prmH00B44 ;
          prmH00B44 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV50ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV51ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV75TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV76TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV79TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV80TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV89TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV90TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV94TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV98TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00B45 ;
          prmH00B45 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV43ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV50ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV51ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV75TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV76TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV79TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV80TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV89TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV90TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV94TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV98TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B42", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B42,0,0,true,false )
             ,new CursorDef("H00B43", "SELECT [Servico_Codigo] AS ContagemResultado_Servico, [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B43,0,0,true,false )
             ,new CursorDef("H00B44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B44,11,0,true,false )
             ,new CursorDef("H00B45", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B45,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[88]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[89]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[90]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[91]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
       }
    }

 }

}
