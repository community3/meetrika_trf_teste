/*
               File: PRC_RetornaSS
        Description: Retorna SS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:27.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornass : GXProcedure
   {
      public prc_retornass( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornass( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           out bool aP1_RetornaSS )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8RetornaSS = false ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_RetornaSS=this.AV8RetornaSS;
      }

      public bool executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8RetornaSS = false ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_RetornaSS=this.AV8RetornaSS;
         return AV8RetornaSS ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 out bool aP1_RetornaSS )
      {
         prc_retornass objprc_retornass;
         objprc_retornass = new prc_retornass();
         objprc_retornass.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_retornass.AV8RetornaSS = false ;
         objprc_retornass.context.SetSubmitInitialConfig(context);
         objprc_retornass.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornass);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_RetornaSS=this.AV8RetornaSS;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornass)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CE2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1729Contratante_RetornaSS = P00CE2_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = P00CE2_n1729Contratante_RetornaSS[0];
            AV8RetornaSS = A1729Contratante_RetornaSS;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CE2_A29Contratante_Codigo = new int[1] ;
         P00CE2_A1729Contratante_RetornaSS = new bool[] {false} ;
         P00CE2_n1729Contratante_RetornaSS = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornass__default(),
            new Object[][] {
                new Object[] {
               P00CE2_A29Contratante_Codigo, P00CE2_A1729Contratante_RetornaSS, P00CE2_n1729Contratante_RetornaSS
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8RetornaSS ;
      private bool A1729Contratante_RetornaSS ;
      private bool n1729Contratante_RetornaSS ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CE2_A29Contratante_Codigo ;
      private bool[] P00CE2_A1729Contratante_RetornaSS ;
      private bool[] P00CE2_n1729Contratante_RetornaSS ;
      private bool aP1_RetornaSS ;
   }

   public class prc_retornass__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CE2 ;
          prmP00CE2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CE2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_RetornaSS] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CE2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
