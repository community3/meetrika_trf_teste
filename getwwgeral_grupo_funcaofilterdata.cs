/*
               File: GetWWGeral_Grupo_FuncaoFilterData
        Description: Get WWGeral_Grupo_Funcao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/14/2020 23:58:10.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwgeral_grupo_funcaofilterdata : GXProcedure
   {
      public getwwgeral_grupo_funcaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwgeral_grupo_funcaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwgeral_grupo_funcaofilterdata objgetwwgeral_grupo_funcaofilterdata;
         objgetwwgeral_grupo_funcaofilterdata = new getwwgeral_grupo_funcaofilterdata();
         objgetwwgeral_grupo_funcaofilterdata.AV15DDOName = aP0_DDOName;
         objgetwwgeral_grupo_funcaofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwwgeral_grupo_funcaofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwwgeral_grupo_funcaofilterdata.AV19OptionsJson = "" ;
         objgetwwgeral_grupo_funcaofilterdata.AV22OptionsDescJson = "" ;
         objgetwwgeral_grupo_funcaofilterdata.AV24OptionIndexesJson = "" ;
         objgetwwgeral_grupo_funcaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwgeral_grupo_funcaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwgeral_grupo_funcaofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwgeral_grupo_funcaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_GRUPOFUNCAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGRUPOFUNCAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWGeral_Grupo_FuncaoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGeral_Grupo_FuncaoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWGeral_Grupo_FuncaoGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "GRUPOFUNCAO_AREATRABALHOCOD") == 0 )
            {
               AV40GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_NOME") == 0 )
            {
               AV10TFGrupoFuncao_Nome = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_NOME_SEL") == 0 )
            {
               AV11TFGrupoFuncao_Nome_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_ATIVO_SEL") == 0 )
            {
               AV12TFGrupoFuncao_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 )
            {
               AV32GrupoFuncao_Nome1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 )
               {
                  AV35GrupoFuncao_Nome2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 )
                  {
                     AV38GrupoFuncao_Nome3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADGRUPOFUNCAO_NOMEOPTIONS' Routine */
         AV10TFGrupoFuncao_Nome = AV13SearchTxt;
         AV11TFGrupoFuncao_Nome_Sel = "";
         AV45WWGeral_Grupo_FuncaoDS_1_Grupofuncao_areatrabalhocod = AV40GrupoFuncao_AreaTrabalhoCod;
         AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1 = AV31DynamicFiltersSelector1;
         AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 = AV32GrupoFuncao_Nome1;
         AV48WWGeral_Grupo_FuncaoDS_4_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 = AV35GrupoFuncao_Nome2;
         AV51WWGeral_Grupo_FuncaoDS_7_Dynamicfiltersenabled3 = AV36DynamicFiltersEnabled3;
         AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3 = AV37DynamicFiltersSelector3;
         AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 = AV38GrupoFuncao_Nome3;
         AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome = AV10TFGrupoFuncao_Nome;
         AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel = AV11TFGrupoFuncao_Nome_Sel;
         AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel = AV12TFGrupoFuncao_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1 ,
                                              AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 ,
                                              AV48WWGeral_Grupo_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                              AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2 ,
                                              AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 ,
                                              AV51WWGeral_Grupo_FuncaoDS_7_Dynamicfiltersenabled3 ,
                                              AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3 ,
                                              AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 ,
                                              AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel ,
                                              AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome ,
                                              AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel ,
                                              A620GrupoFuncao_Nome ,
                                              A625GrupoFuncao_Ativo ,
                                              A2179GrupoFuncao_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 = StringUtil.Concat( StringUtil.RTrim( AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1), "%", "");
         lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 = StringUtil.Concat( StringUtil.RTrim( AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2), "%", "");
         lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 = StringUtil.Concat( StringUtil.RTrim( AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3), "%", "");
         lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome = StringUtil.Concat( StringUtil.RTrim( AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome), "%", "");
         /* Using cursor P00N52 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1, lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2, lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3, lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome, AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKN52 = false;
            A2179GrupoFuncao_AreaTrabalhoCod = P00N52_A2179GrupoFuncao_AreaTrabalhoCod[0];
            n2179GrupoFuncao_AreaTrabalhoCod = P00N52_n2179GrupoFuncao_AreaTrabalhoCod[0];
            A620GrupoFuncao_Nome = P00N52_A620GrupoFuncao_Nome[0];
            A625GrupoFuncao_Ativo = P00N52_A625GrupoFuncao_Ativo[0];
            A619GrupoFuncao_Codigo = P00N52_A619GrupoFuncao_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00N52_A620GrupoFuncao_Nome[0], A620GrupoFuncao_Nome) == 0 ) )
            {
               BRKN52 = false;
               A619GrupoFuncao_Codigo = P00N52_A619GrupoFuncao_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKN52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A620GrupoFuncao_Nome)) )
            {
               AV17Option = A620GrupoFuncao_Nome;
               AV20OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A620GrupoFuncao_Nome, "@!")));
               AV18Options.Add(AV17Option, 0);
               AV21OptionsDesc.Add(AV20OptionDesc, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKN52 )
            {
               BRKN52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFGrupoFuncao_Nome = "";
         AV11TFGrupoFuncao_Nome_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32GrupoFuncao_Nome1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35GrupoFuncao_Nome2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38GrupoFuncao_Nome3 = "";
         AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1 = "";
         AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 = "";
         AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2 = "";
         AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 = "";
         AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3 = "";
         AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 = "";
         AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome = "";
         AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel = "";
         scmdbuf = "";
         lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 = "";
         lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 = "";
         lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 = "";
         lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome = "";
         A620GrupoFuncao_Nome = "";
         P00N52_A2179GrupoFuncao_AreaTrabalhoCod = new int[1] ;
         P00N52_n2179GrupoFuncao_AreaTrabalhoCod = new bool[] {false} ;
         P00N52_A620GrupoFuncao_Nome = new String[] {""} ;
         P00N52_A625GrupoFuncao_Ativo = new bool[] {false} ;
         P00N52_A619GrupoFuncao_Codigo = new int[1] ;
         AV17Option = "";
         AV20OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwgeral_grupo_funcaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00N52_A2179GrupoFuncao_AreaTrabalhoCod, P00N52_n2179GrupoFuncao_AreaTrabalhoCod, P00N52_A620GrupoFuncao_Nome, P00N52_A625GrupoFuncao_Ativo, P00N52_A619GrupoFuncao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFGrupoFuncao_Ativo_Sel ;
      private short AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel ;
      private int AV43GXV1 ;
      private int AV40GrupoFuncao_AreaTrabalhoCod ;
      private int AV45WWGeral_Grupo_FuncaoDS_1_Grupofuncao_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A2179GrupoFuncao_AreaTrabalhoCod ;
      private int A619GrupoFuncao_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool AV48WWGeral_Grupo_FuncaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV51WWGeral_Grupo_FuncaoDS_7_Dynamicfiltersenabled3 ;
      private bool A625GrupoFuncao_Ativo ;
      private bool BRKN52 ;
      private bool n2179GrupoFuncao_AreaTrabalhoCod ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFGrupoFuncao_Nome ;
      private String AV11TFGrupoFuncao_Nome_Sel ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV32GrupoFuncao_Nome1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV35GrupoFuncao_Nome2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV38GrupoFuncao_Nome3 ;
      private String AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1 ;
      private String AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 ;
      private String AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2 ;
      private String AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 ;
      private String AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3 ;
      private String AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 ;
      private String AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome ;
      private String AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel ;
      private String lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 ;
      private String lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 ;
      private String lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 ;
      private String lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome ;
      private String A620GrupoFuncao_Nome ;
      private String AV17Option ;
      private String AV20OptionDesc ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00N52_A2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] P00N52_n2179GrupoFuncao_AreaTrabalhoCod ;
      private String[] P00N52_A620GrupoFuncao_Nome ;
      private bool[] P00N52_A625GrupoFuncao_Ativo ;
      private int[] P00N52_A619GrupoFuncao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwwgeral_grupo_funcaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00N52( IGxContext context ,
                                             String AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1 ,
                                             String AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 ,
                                             bool AV48WWGeral_Grupo_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2 ,
                                             String AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 ,
                                             bool AV51WWGeral_Grupo_FuncaoDS_7_Dynamicfiltersenabled3 ,
                                             String AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3 ,
                                             String AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 ,
                                             String AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel ,
                                             String AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome ,
                                             short AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel ,
                                             String A620GrupoFuncao_Nome ,
                                             bool A625GrupoFuncao_Ativo ,
                                             int A2179GrupoFuncao_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [GrupoFuncao_AreaTrabalhoCod], [GrupoFuncao_Nome], [GrupoFuncao_Ativo], [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([GrupoFuncao_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV46WWGeral_Grupo_FuncaoDS_2_Dynamicfiltersselector1, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV48WWGeral_Grupo_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWGeral_Grupo_FuncaoDS_5_Dynamicfiltersselector2, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV51WWGeral_Grupo_FuncaoDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV52WWGeral_Grupo_FuncaoDS_8_Dynamicfiltersselector3, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like @lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] = @AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Ativo] = 1)";
         }
         if ( AV56WWGeral_Grupo_FuncaoDS_12_Tfgrupofuncao_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [GrupoFuncao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00N52(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00N52 ;
          prmP00N52 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47WWGeral_Grupo_FuncaoDS_3_Grupofuncao_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV50WWGeral_Grupo_FuncaoDS_6_Grupofuncao_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV53WWGeral_Grupo_FuncaoDS_9_Grupofuncao_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV54WWGeral_Grupo_FuncaoDS_10_Tfgrupofuncao_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV55WWGeral_Grupo_FuncaoDS_11_Tfgrupofuncao_nome_sel",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00N52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00N52,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwgeral_grupo_funcaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwgeral_grupo_funcaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwgeral_grupo_funcaofilterdata") )
          {
             return  ;
          }
          getwwgeral_grupo_funcaofilterdata worker = new getwwgeral_grupo_funcaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
