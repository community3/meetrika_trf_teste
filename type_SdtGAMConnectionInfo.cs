/*
               File: type_SdtGAMConnectionInfo
        Description: GAMConnectionInfo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:43.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMConnectionInfo : GxUserType, IGxExternalObject
   {
      public SdtGAMConnectionInfo( )
      {
         initialize();
      }

      public SdtGAMConnectionInfo( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMConnectionInfo_externalReference == null )
         {
            GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
         }
         returntostring = "";
         returntostring = (String)(GAMConnectionInfo_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.Name ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.Name = value;
         }

      }

      public String gxTpr_Repositoryname
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.RepositoryName ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.RepositoryName = value;
         }

      }

      public String gxTpr_Repositoryguid
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.RepositoryGUID ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.RepositoryGUID = value;
         }

      }

      public String gxTpr_Repositorynamespace
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.RepositoryNameSpace ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.RepositoryNameSpace = value;
         }

      }

      public String gxTpr_Type
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.Type ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.Type = value;
         }

      }

      public String gxTpr_Language
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.Language ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.Language = value;
         }

      }

      public String gxTpr_Username
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.UserName ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.UserName = value;
         }

      }

      public String gxTpr_Userpassword
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference.UserPassword ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            GAMConnectionInfo_externalReference.UserPassword = value;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMConnectionInfoProperties", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMConnectionInfoProperties> externalParm0 ;
            externalParm0 = GAMConnectionInfo_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMConnectionInfoProperties>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMConnectionInfoProperties> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMConnectionInfoProperties>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMConnectionInfoProperties>), intValue.ExternalInstance);
            GAMConnectionInfo_externalReference.Properties = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMConnectionInfo_externalReference == null )
            {
               GAMConnectionInfo_externalReference = new Artech.Security.GAMConnectionInfo(context);
            }
            return GAMConnectionInfo_externalReference ;
         }

         set {
            GAMConnectionInfo_externalReference = (Artech.Security.GAMConnectionInfo)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMConnectionInfo GAMConnectionInfo_externalReference=null ;
   }

}
