/*
               File: GetPromptMetodologiaFasesFilterData
        Description: Get Prompt Metodologia Fases Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:41.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptmetodologiafasesfilterdata : GXProcedure
   {
      public getpromptmetodologiafasesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptmetodologiafasesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptmetodologiafasesfilterdata objgetpromptmetodologiafasesfilterdata;
         objgetpromptmetodologiafasesfilterdata = new getpromptmetodologiafasesfilterdata();
         objgetpromptmetodologiafasesfilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptmetodologiafasesfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptmetodologiafasesfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptmetodologiafasesfilterdata.AV22OptionsJson = "" ;
         objgetpromptmetodologiafasesfilterdata.AV25OptionsDescJson = "" ;
         objgetpromptmetodologiafasesfilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptmetodologiafasesfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptmetodologiafasesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptmetodologiafasesfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptmetodologiafasesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_METODOLOGIAFASES_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIAFASES_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_METODOLOGIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptMetodologiaFasesGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptMetodologiaFasesGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptMetodologiaFasesGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME") == 0 )
            {
               AV10TFMetodologiaFases_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME_SEL") == 0 )
            {
               AV11TFMetodologiaFases_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_PERCENTUAL") == 0 )
            {
               AV12TFMetodologiaFases_Percentual = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFMetodologiaFases_Percentual_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_PRAZO") == 0 )
            {
               AV43TFMetodologiaFases_Prazo = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV44TFMetodologiaFases_Prazo_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV14TFMetodologia_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV15TFMetodologia_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36MetodologiaFases_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37Metodologia_Descricao1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV41MetodologiaFases_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42Metodologia_Descricao2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMETODOLOGIAFASES_NOMEOPTIONS' Routine */
         AV10TFMetodologiaFases_Nome = AV16SearchTxt;
         AV11TFMetodologiaFases_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36MetodologiaFases_Nome1 ,
                                              AV37Metodologia_Descricao1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40DynamicFiltersOperator2 ,
                                              AV41MetodologiaFases_Nome2 ,
                                              AV42Metodologia_Descricao2 ,
                                              AV11TFMetodologiaFases_Nome_Sel ,
                                              AV10TFMetodologiaFases_Nome ,
                                              AV12TFMetodologiaFases_Percentual ,
                                              AV13TFMetodologiaFases_Percentual_To ,
                                              AV43TFMetodologiaFases_Prazo ,
                                              AV44TFMetodologiaFases_Prazo_To ,
                                              AV15TFMetodologia_Descricao_Sel ,
                                              AV14TFMetodologia_Descricao ,
                                              A148MetodologiaFases_Nome ,
                                              A138Metodologia_Descricao ,
                                              A149MetodologiaFases_Percentual ,
                                              A2135MetodologiaFases_Prazo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV36MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36MetodologiaFases_Nome1), 50, "%");
         lV36MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36MetodologiaFases_Nome1), 50, "%");
         lV37Metodologia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37Metodologia_Descricao1), "%", "");
         lV37Metodologia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37Metodologia_Descricao1), "%", "");
         lV41MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41MetodologiaFases_Nome2), 50, "%");
         lV41MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41MetodologiaFases_Nome2), 50, "%");
         lV42Metodologia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42Metodologia_Descricao2), "%", "");
         lV42Metodologia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42Metodologia_Descricao2), "%", "");
         lV10TFMetodologiaFases_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFMetodologiaFases_Nome), 50, "%");
         lV14TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFMetodologia_Descricao), "%", "");
         /* Using cursor P00KW2 */
         pr_default.execute(0, new Object[] {lV36MetodologiaFases_Nome1, lV36MetodologiaFases_Nome1, lV37Metodologia_Descricao1, lV37Metodologia_Descricao1, lV41MetodologiaFases_Nome2, lV41MetodologiaFases_Nome2, lV42Metodologia_Descricao2, lV42Metodologia_Descricao2, lV10TFMetodologiaFases_Nome, AV11TFMetodologiaFases_Nome_Sel, AV12TFMetodologiaFases_Percentual, AV13TFMetodologiaFases_Percentual_To, AV43TFMetodologiaFases_Prazo, AV44TFMetodologiaFases_Prazo_To, lV14TFMetodologia_Descricao, AV15TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKW2 = false;
            A137Metodologia_Codigo = P00KW2_A137Metodologia_Codigo[0];
            A148MetodologiaFases_Nome = P00KW2_A148MetodologiaFases_Nome[0];
            A2135MetodologiaFases_Prazo = P00KW2_A2135MetodologiaFases_Prazo[0];
            n2135MetodologiaFases_Prazo = P00KW2_n2135MetodologiaFases_Prazo[0];
            A149MetodologiaFases_Percentual = P00KW2_A149MetodologiaFases_Percentual[0];
            A138Metodologia_Descricao = P00KW2_A138Metodologia_Descricao[0];
            A147MetodologiaFases_Codigo = P00KW2_A147MetodologiaFases_Codigo[0];
            A138Metodologia_Descricao = P00KW2_A138Metodologia_Descricao[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KW2_A148MetodologiaFases_Nome[0], A148MetodologiaFases_Nome) == 0 ) )
            {
               BRKKW2 = false;
               A147MetodologiaFases_Codigo = P00KW2_A147MetodologiaFases_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKKW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A148MetodologiaFases_Nome)) )
            {
               AV20Option = A148MetodologiaFases_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKW2 )
            {
               BRKKW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMETODOLOGIA_DESCRICAOOPTIONS' Routine */
         AV14TFMetodologia_Descricao = AV16SearchTxt;
         AV15TFMetodologia_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36MetodologiaFases_Nome1 ,
                                              AV37Metodologia_Descricao1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40DynamicFiltersOperator2 ,
                                              AV41MetodologiaFases_Nome2 ,
                                              AV42Metodologia_Descricao2 ,
                                              AV11TFMetodologiaFases_Nome_Sel ,
                                              AV10TFMetodologiaFases_Nome ,
                                              AV12TFMetodologiaFases_Percentual ,
                                              AV13TFMetodologiaFases_Percentual_To ,
                                              AV43TFMetodologiaFases_Prazo ,
                                              AV44TFMetodologiaFases_Prazo_To ,
                                              AV15TFMetodologia_Descricao_Sel ,
                                              AV14TFMetodologia_Descricao ,
                                              A148MetodologiaFases_Nome ,
                                              A138Metodologia_Descricao ,
                                              A149MetodologiaFases_Percentual ,
                                              A2135MetodologiaFases_Prazo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV36MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36MetodologiaFases_Nome1), 50, "%");
         lV36MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36MetodologiaFases_Nome1), 50, "%");
         lV37Metodologia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37Metodologia_Descricao1), "%", "");
         lV37Metodologia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37Metodologia_Descricao1), "%", "");
         lV41MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41MetodologiaFases_Nome2), 50, "%");
         lV41MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41MetodologiaFases_Nome2), 50, "%");
         lV42Metodologia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42Metodologia_Descricao2), "%", "");
         lV42Metodologia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42Metodologia_Descricao2), "%", "");
         lV10TFMetodologiaFases_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFMetodologiaFases_Nome), 50, "%");
         lV14TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFMetodologia_Descricao), "%", "");
         /* Using cursor P00KW3 */
         pr_default.execute(1, new Object[] {lV36MetodologiaFases_Nome1, lV36MetodologiaFases_Nome1, lV37Metodologia_Descricao1, lV37Metodologia_Descricao1, lV41MetodologiaFases_Nome2, lV41MetodologiaFases_Nome2, lV42Metodologia_Descricao2, lV42Metodologia_Descricao2, lV10TFMetodologiaFases_Nome, AV11TFMetodologiaFases_Nome_Sel, AV12TFMetodologiaFases_Percentual, AV13TFMetodologiaFases_Percentual_To, AV43TFMetodologiaFases_Prazo, AV44TFMetodologiaFases_Prazo_To, lV14TFMetodologia_Descricao, AV15TFMetodologia_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKW4 = false;
            A137Metodologia_Codigo = P00KW3_A137Metodologia_Codigo[0];
            A2135MetodologiaFases_Prazo = P00KW3_A2135MetodologiaFases_Prazo[0];
            n2135MetodologiaFases_Prazo = P00KW3_n2135MetodologiaFases_Prazo[0];
            A149MetodologiaFases_Percentual = P00KW3_A149MetodologiaFases_Percentual[0];
            A138Metodologia_Descricao = P00KW3_A138Metodologia_Descricao[0];
            A148MetodologiaFases_Nome = P00KW3_A148MetodologiaFases_Nome[0];
            A147MetodologiaFases_Codigo = P00KW3_A147MetodologiaFases_Codigo[0];
            A138Metodologia_Descricao = P00KW3_A138Metodologia_Descricao[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00KW3_A137Metodologia_Codigo[0] == A137Metodologia_Codigo ) )
            {
               BRKKW4 = false;
               A147MetodologiaFases_Codigo = P00KW3_A147MetodologiaFases_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKKW4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A138Metodologia_Descricao)) )
            {
               AV20Option = A138Metodologia_Descricao;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKW4 )
            {
               BRKKW4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFMetodologiaFases_Nome = "";
         AV11TFMetodologiaFases_Nome_Sel = "";
         AV14TFMetodologia_Descricao = "";
         AV15TFMetodologia_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36MetodologiaFases_Nome1 = "";
         AV37Metodologia_Descricao1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV41MetodologiaFases_Nome2 = "";
         AV42Metodologia_Descricao2 = "";
         scmdbuf = "";
         lV36MetodologiaFases_Nome1 = "";
         lV37Metodologia_Descricao1 = "";
         lV41MetodologiaFases_Nome2 = "";
         lV42Metodologia_Descricao2 = "";
         lV10TFMetodologiaFases_Nome = "";
         lV14TFMetodologia_Descricao = "";
         A148MetodologiaFases_Nome = "";
         A138Metodologia_Descricao = "";
         P00KW2_A137Metodologia_Codigo = new int[1] ;
         P00KW2_A148MetodologiaFases_Nome = new String[] {""} ;
         P00KW2_A2135MetodologiaFases_Prazo = new decimal[1] ;
         P00KW2_n2135MetodologiaFases_Prazo = new bool[] {false} ;
         P00KW2_A149MetodologiaFases_Percentual = new decimal[1] ;
         P00KW2_A138Metodologia_Descricao = new String[] {""} ;
         P00KW2_A147MetodologiaFases_Codigo = new int[1] ;
         AV20Option = "";
         P00KW3_A137Metodologia_Codigo = new int[1] ;
         P00KW3_A2135MetodologiaFases_Prazo = new decimal[1] ;
         P00KW3_n2135MetodologiaFases_Prazo = new bool[] {false} ;
         P00KW3_A149MetodologiaFases_Percentual = new decimal[1] ;
         P00KW3_A138Metodologia_Descricao = new String[] {""} ;
         P00KW3_A148MetodologiaFases_Nome = new String[] {""} ;
         P00KW3_A147MetodologiaFases_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptmetodologiafasesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KW2_A137Metodologia_Codigo, P00KW2_A148MetodologiaFases_Nome, P00KW2_A2135MetodologiaFases_Prazo, P00KW2_n2135MetodologiaFases_Prazo, P00KW2_A149MetodologiaFases_Percentual, P00KW2_A138Metodologia_Descricao, P00KW2_A147MetodologiaFases_Codigo
               }
               , new Object[] {
               P00KW3_A137Metodologia_Codigo, P00KW3_A2135MetodologiaFases_Prazo, P00KW3_n2135MetodologiaFases_Prazo, P00KW3_A149MetodologiaFases_Percentual, P00KW3_A138Metodologia_Descricao, P00KW3_A148MetodologiaFases_Nome, P00KW3_A147MetodologiaFases_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private int AV47GXV1 ;
      private int A137Metodologia_Codigo ;
      private int A147MetodologiaFases_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private decimal AV12TFMetodologiaFases_Percentual ;
      private decimal AV13TFMetodologiaFases_Percentual_To ;
      private decimal AV43TFMetodologiaFases_Prazo ;
      private decimal AV44TFMetodologiaFases_Prazo_To ;
      private decimal A149MetodologiaFases_Percentual ;
      private decimal A2135MetodologiaFases_Prazo ;
      private String AV10TFMetodologiaFases_Nome ;
      private String AV11TFMetodologiaFases_Nome_Sel ;
      private String AV36MetodologiaFases_Nome1 ;
      private String AV41MetodologiaFases_Nome2 ;
      private String scmdbuf ;
      private String lV36MetodologiaFases_Nome1 ;
      private String lV41MetodologiaFases_Nome2 ;
      private String lV10TFMetodologiaFases_Nome ;
      private String A148MetodologiaFases_Nome ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool BRKKW2 ;
      private bool n2135MetodologiaFases_Prazo ;
      private bool BRKKW4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFMetodologia_Descricao ;
      private String AV15TFMetodologia_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV37Metodologia_Descricao1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV42Metodologia_Descricao2 ;
      private String lV37Metodologia_Descricao1 ;
      private String lV42Metodologia_Descricao2 ;
      private String lV14TFMetodologia_Descricao ;
      private String A138Metodologia_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KW2_A137Metodologia_Codigo ;
      private String[] P00KW2_A148MetodologiaFases_Nome ;
      private decimal[] P00KW2_A2135MetodologiaFases_Prazo ;
      private bool[] P00KW2_n2135MetodologiaFases_Prazo ;
      private decimal[] P00KW2_A149MetodologiaFases_Percentual ;
      private String[] P00KW2_A138Metodologia_Descricao ;
      private int[] P00KW2_A147MetodologiaFases_Codigo ;
      private int[] P00KW3_A137Metodologia_Codigo ;
      private decimal[] P00KW3_A2135MetodologiaFases_Prazo ;
      private bool[] P00KW3_n2135MetodologiaFases_Prazo ;
      private decimal[] P00KW3_A149MetodologiaFases_Percentual ;
      private String[] P00KW3_A138Metodologia_Descricao ;
      private String[] P00KW3_A148MetodologiaFases_Nome ;
      private int[] P00KW3_A147MetodologiaFases_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptmetodologiafasesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KW2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36MetodologiaFases_Nome1 ,
                                             String AV37Metodologia_Descricao1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             short AV40DynamicFiltersOperator2 ,
                                             String AV41MetodologiaFases_Nome2 ,
                                             String AV42Metodologia_Descricao2 ,
                                             String AV11TFMetodologiaFases_Nome_Sel ,
                                             String AV10TFMetodologiaFases_Nome ,
                                             decimal AV12TFMetodologiaFases_Percentual ,
                                             decimal AV13TFMetodologiaFases_Percentual_To ,
                                             decimal AV43TFMetodologiaFases_Prazo ,
                                             decimal AV44TFMetodologiaFases_Prazo_To ,
                                             String AV15TFMetodologia_Descricao_Sel ,
                                             String AV14TFMetodologia_Descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual ,
                                             decimal A2135MetodologiaFases_Prazo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[MetodologiaFases_Nome], T1.[MetodologiaFases_Prazo], T1.[MetodologiaFases_Percentual], T2.[Metodologia_Descricao], T1.[MetodologiaFases_Codigo] FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV36MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV36MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV36MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV36MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Metodologia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV37Metodologia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV37Metodologia_Descricao1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Metodologia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV37Metodologia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV37Metodologia_Descricao1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV41MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV41MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV41MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV41MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Metodologia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV42Metodologia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV42Metodologia_Descricao2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Metodologia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV42Metodologia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV42Metodologia_Descricao2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFMetodologiaFases_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFMetodologiaFases_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV10TFMetodologiaFases_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV10TFMetodologiaFases_Nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFMetodologiaFases_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV11TFMetodologiaFases_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV11TFMetodologiaFases_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFMetodologiaFases_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV12TFMetodologiaFases_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV12TFMetodologiaFases_Percentual)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFMetodologiaFases_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV13TFMetodologiaFases_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV13TFMetodologiaFases_Percentual_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43TFMetodologiaFases_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] >= @AV43TFMetodologiaFases_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] >= @AV43TFMetodologiaFases_Prazo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFMetodologiaFases_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] <= @AV44TFMetodologiaFases_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] <= @AV44TFMetodologiaFases_Prazo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFMetodologia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV14TFMetodologia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV14TFMetodologia_Descricao)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFMetodologia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV15TFMetodologia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV15TFMetodologia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[MetodologiaFases_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KW3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36MetodologiaFases_Nome1 ,
                                             String AV37Metodologia_Descricao1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             short AV40DynamicFiltersOperator2 ,
                                             String AV41MetodologiaFases_Nome2 ,
                                             String AV42Metodologia_Descricao2 ,
                                             String AV11TFMetodologiaFases_Nome_Sel ,
                                             String AV10TFMetodologiaFases_Nome ,
                                             decimal AV12TFMetodologiaFases_Percentual ,
                                             decimal AV13TFMetodologiaFases_Percentual_To ,
                                             decimal AV43TFMetodologiaFases_Prazo ,
                                             decimal AV44TFMetodologiaFases_Prazo_To ,
                                             String AV15TFMetodologia_Descricao_Sel ,
                                             String AV14TFMetodologia_Descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual ,
                                             decimal A2135MetodologiaFases_Prazo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[MetodologiaFases_Prazo], T1.[MetodologiaFases_Percentual], T2.[Metodologia_Descricao], T1.[MetodologiaFases_Nome], T1.[MetodologiaFases_Codigo] FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV36MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV36MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV36MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV36MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Metodologia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV37Metodologia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV37Metodologia_Descricao1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Metodologia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV37Metodologia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV37Metodologia_Descricao1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV41MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV41MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV41MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV41MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Metodologia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV42Metodologia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV42Metodologia_Descricao2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Metodologia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV42Metodologia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV42Metodologia_Descricao2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFMetodologiaFases_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFMetodologiaFases_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV10TFMetodologiaFases_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV10TFMetodologiaFases_Nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFMetodologiaFases_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV11TFMetodologiaFases_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV11TFMetodologiaFases_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFMetodologiaFases_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV12TFMetodologiaFases_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV12TFMetodologiaFases_Percentual)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFMetodologiaFases_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV13TFMetodologiaFases_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV13TFMetodologiaFases_Percentual_To)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43TFMetodologiaFases_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] >= @AV43TFMetodologiaFases_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] >= @AV43TFMetodologiaFases_Prazo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFMetodologiaFases_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] <= @AV44TFMetodologiaFases_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] <= @AV44TFMetodologiaFases_Prazo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFMetodologia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV14TFMetodologia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV14TFMetodologia_Descricao)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFMetodologia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV15TFMetodologia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV15TFMetodologia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Metodologia_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KW2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] );
               case 1 :
                     return conditional_P00KW3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KW2 ;
          prmP00KW2 = new Object[] {
          new Object[] {"@lV36MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Metodologia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV37Metodologia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV41MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Metodologia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42Metodologia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFMetodologiaFases_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFMetodologiaFases_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFMetodologiaFases_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV13TFMetodologiaFases_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV43TFMetodologiaFases_Prazo",SqlDbType.Decimal,7,2} ,
          new Object[] {"@AV44TFMetodologiaFases_Prazo_To",SqlDbType.Decimal,7,2} ,
          new Object[] {"@lV14TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00KW3 ;
          prmP00KW3 = new Object[] {
          new Object[] {"@lV36MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Metodologia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV37Metodologia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV41MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Metodologia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42Metodologia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFMetodologiaFases_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFMetodologiaFases_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFMetodologiaFases_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV13TFMetodologiaFases_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV43TFMetodologiaFases_Prazo",SqlDbType.Decimal,7,2} ,
          new Object[] {"@AV44TFMetodologiaFases_Prazo_To",SqlDbType.Decimal,7,2} ,
          new Object[] {"@lV14TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KW2,100,0,true,false )
             ,new CursorDef("P00KW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KW3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptmetodologiafasesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptmetodologiafasesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptmetodologiafasesfilterdata") )
          {
             return  ;
          }
          getpromptmetodologiafasesfilterdata worker = new getpromptmetodologiafasesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
