/*
               File: WP_FuncaoDadosDetalhes
        Description: Detalhes da Fun��o de Dados:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:18:0.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_funcaodadosdetalhes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_funcaodadosdetalhes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_funcaodadosdetalhes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           int aP1_FuncaoDados_SistemaCod )
      {
         this.A368FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.A370FuncaoDados_SistemaCod = aP1_FuncaoDados_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A368FuncaoDados_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAA22( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTA22( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282318057");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_funcaodadosdetalhes.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAODADOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( "", A397FuncaoDados_Descricao));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wctabelas == null ) )
         {
            WebComp_Wctabelas.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEA22( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTA22( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_funcaodadosdetalhes.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "WP_FuncaoDadosDetalhes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Detalhes da Fun��o de Dados:" ;
      }

      protected void WBA20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_A22( true) ;
         }
         else
         {
            wb_table1_2_A22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A22e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Detalhes da Fun��o de Dados:", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPA20( ) ;
      }

      protected void WSA22( )
      {
         STARTA22( ) ;
         EVTA22( ) ;
      }

      protected void EVTA22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11A22 */
                              E11A22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12A22 */
                              E12A22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13A22 */
                              E13A22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 60 )
                        {
                           OldWctabelas = cgiGet( "W0060");
                           if ( ( StringUtil.Len( OldWctabelas) == 0 ) || ( StringUtil.StrCmp(OldWctabelas, WebComp_Wctabelas_Component) != 0 ) )
                           {
                              WebComp_Wctabelas = getWebComponent(GetType(), "GeneXus.Programs", OldWctabelas, new Object[] {context} );
                              WebComp_Wctabelas.ComponentInit();
                              WebComp_Wctabelas.Name = "OldWctabelas";
                              WebComp_Wctabelas_Component = OldWctabelas;
                           }
                           WebComp_Wctabelas.componentprocess("W0060", "", sEvt);
                           WebComp_Wctabelas_Component = OldWctabelas;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAA22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbFuncaoDados_Tipo.Name = "FUNCAODADOS_TIPO";
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            }
            cmbFuncaoDados_Complexidade.Name = "FUNCAODADOS_COMPLEXIDADE";
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFuncaodados_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
         {
            A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         }
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFA22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavFuncaodados_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
      }

      protected void RFA22( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Wctabelas_Component, "") == 0 )
            {
               WebComp_Wctabelas = getWebComponent(GetType(), "GeneXus.Programs", "funcaodadostabelaconsultawc", new Object[] {context} );
               WebComp_Wctabelas.ComponentInit();
               WebComp_Wctabelas.Name = "FuncaoDadosTabelaConsultaWC";
               WebComp_Wctabelas_Component = "FuncaoDadosTabelaConsultaWC";
            }
            WebComp_Wctabelas.setjustcreated();
            WebComp_Wctabelas.componentprepare(new Object[] {(String)"W0060",(String)"",(int)A370FuncaoDados_SistemaCod,(int)A368FuncaoDados_Codigo});
            WebComp_Wctabelas.componentbind(new Object[] {(String)"",(String)""});
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0060"+"");
               WebComp_Wctabelas.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               WebComp_Wctabelas.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00A22 */
            pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A397FuncaoDados_Descricao = H00A22_A397FuncaoDados_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( "", A397FuncaoDados_Descricao));
               n397FuncaoDados_Descricao = H00A22_n397FuncaoDados_Descricao[0];
               A373FuncaoDados_Tipo = H00A22_A373FuncaoDados_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
               A755FuncaoDados_Contar = H00A22_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H00A22_n755FuncaoDados_Contar[0];
               if ( A755FuncaoDados_Contar )
               {
                  GXt_int1 = (short)(A377FuncaoDados_PF);
                  new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
                  A377FuncaoDados_PF = (decimal)(GXt_int1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
               }
               else
               {
                  A377FuncaoDados_PF = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
               }
               /* Execute user event: E13A22 */
               E13A22 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBA20( ) ;
         }
      }

      protected void STRUPA20( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavFuncaodados_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Enabled), 5, 0)));
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GXt_int1 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         A374FuncaoDados_DER = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GXt_int1 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         A375FuncaoDados_RLR = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11A22 */
         E11A22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV15FuncaoDados_Nome = cgiGet( edtavFuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncaoDados_Nome", AV15FuncaoDados_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15FuncaoDados_Nome, ""))));
            cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
            A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
            A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
            A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
            A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
            A397FuncaoDados_Descricao = cgiGet( edtFuncaoDados_Descricao_Internalname);
            n397FuncaoDados_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_DESCRICAO", GetSecureSignedToken( "", A397FuncaoDados_Descricao));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11A22 */
         E11A22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11A22( )
      {
         /* Start Routine */
         /* Using cursor H00A23 */
         pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A369FuncaoDados_Nome = H00A23_A369FuncaoDados_Nome[0];
            AV15FuncaoDados_Nome = A369FuncaoDados_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncaoDados_Nome", AV15FuncaoDados_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15FuncaoDados_Nome, ""))));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E12A22( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E13A22( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_8_A22( true) ;
         }
         else
         {
            wb_table2_8_A22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_20_A22( true) ;
         }
         else
         {
            wb_table3_20_A22( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A22e( true) ;
         }
         else
         {
            wb_table1_2_A22e( false) ;
         }
      }

      protected void wb_table3_20_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table4_23_A22( true) ;
         }
         else
         {
            wb_table4_23_A22( false) ;
         }
         return  ;
      }

      protected void wb_table4_23_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table5_57_A22( true) ;
         }
         else
         {
            wb_table5_57_A22( false) ;
         }
         return  ;
      }

      protected void wb_table5_57_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_A22e( true) ;
         }
         else
         {
            wb_table3_20_A22e( false) ;
         }
      }

      protected void wb_table5_57_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0060"+"", StringUtil.RTrim( WebComp_Wctabelas_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0060"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWctabelas), StringUtil.Lower( WebComp_Wctabelas_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0060"+"");
               }
               WebComp_Wctabelas.componentdraw();
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWctabelas), StringUtil.Lower( WebComp_Wctabelas_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_57_A22e( true) ;
         }
         else
         {
            wb_table5_57_A22e( false) ;
         }
      }

      protected void wb_table4_23_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;vertical-align:top")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo_Internalname, "Tipo:", "", "", lblTextblockfuncaodados_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table6_28_A22( true) ;
         }
         else
         {
            wb_table6_28_A22( false) ;
         }
         return  ;
      }

      protected void wb_table6_28_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo6_Internalname, "Descri��o:", "", "", lblTextblockfuncaodados_tipo6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Descricao_Internalname, A397FuncaoDados_Descricao, "", "", 0, 1, 0, 0, 110, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", 0, true, "DescricaoLonga", "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_23_A22e( true) ;
         }
         else
         {
            wb_table4_23_A22e( false) ;
         }
      }

      protected void wb_table6_28_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(18), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Tipo, cmbFuncaoDados_Tipo_Internalname, StringUtil.RTrim( A373FuncaoDados_Tipo), 1, cmbFuncaoDados_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "BootstrapAttribute", "", "", "", true, "HLP_WP_FuncaoDadosDetalhes.htm");
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo2_Internalname, "Complexidade:", "", "", lblTextblockfuncaodados_tipo2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Complexidade, cmbFuncaoDados_Complexidade_Internalname, StringUtil.RTrim( A376FuncaoDados_Complexidade), 1, cmbFuncaoDados_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "Attribute", "", "", "", true, "HLP_WP_FuncaoDadosDetalhes.htm");
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo3_Internalname, "DER:", "", "", lblTextblockfuncaodados_tipo3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_DER_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo4_Internalname, "RLR:", "", "", lblTextblockfuncaodados_tipo4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_RLR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_RLR_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo5_Internalname, "Pontos de Fun��o:", "", "", lblTextblockfuncaodados_tipo5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")), context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_PF_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_28_A22e( true) ;
         }
         else
         {
            wb_table6_28_A22e( false) ;
         }
      }

      protected void wb_table2_8_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(1000), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table7_12_A22( true) ;
         }
         else
         {
            wb_table7_12_A22( false) ;
         }
         return  ;
      }

      protected void wb_table7_12_A22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A22e( true) ;
         }
         else
         {
            wb_table2_8_A22e( false) ;
         }
      }

      protected void wb_table7_12_A22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle2_Internalname, tblTablemergedviewtitle2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Fun��o de Dados ::", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:500px")+"\" class='Table'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "DataDescription";
            StyleString = "font-family:'Microsoft Sans Serif'; font-size:14.0pt; font-weight:normal; font-style:normal;";
            ClassString = "DataDescription";
            StyleString = "font-family:'Microsoft Sans Serif'; font-size:14.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome_Internalname, AV15FuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", 0, 1, edtavFuncaodados_nome_Enabled, 0, 100, "chr", 3, "row", StyleString, ClassString, "", "200", 1, "", "", -1, true, "", "HLP_WP_FuncaoDadosDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_12_A22e( true) ;
         }
         else
         {
            wb_table7_12_A22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A368FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         A370FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA22( ) ;
         WSA22( ) ;
         WEA22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( StringUtil.StrCmp(WebComp_Wctabelas_Component, "") == 0 )
         {
            WebComp_Wctabelas = getWebComponent(GetType(), "GeneXus.Programs", "funcaodadostabelaconsultawc", new Object[] {context} );
            WebComp_Wctabelas.ComponentInit();
            WebComp_Wctabelas.Name = "FuncaoDadosTabelaConsultaWC";
            WebComp_Wctabelas_Component = "FuncaoDadosTabelaConsultaWC";
         }
         if ( ! ( WebComp_Wctabelas == null ) )
         {
            WebComp_Wctabelas.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282318091");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_funcaodadosdetalhes.js", "?20204282318091");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtavFuncaodados_nome_Internalname = "vFUNCAODADOS_NOME";
         tblTablemergedviewtitle2_Internalname = "TABLEMERGEDVIEWTITLE2";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblTextblockfuncaodados_tipo_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO";
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO";
         lblTextblockfuncaodados_tipo2_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO2";
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE";
         lblTextblockfuncaodados_tipo3_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO3";
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER";
         lblTextblockfuncaodados_tipo4_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO4";
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR";
         lblTextblockfuncaodados_tipo5_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO5";
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF";
         tblTable3_Internalname = "TABLE3";
         lblTextblockfuncaodados_tipo6_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO6";
         edtFuncaoDados_Descricao_Internalname = "FUNCAODADOS_DESCRICAO";
         tblTable2_Internalname = "TABLE2";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFuncaodados_nome_Enabled = 1;
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Tipo_Jsonclick = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Detalhes da Fun��o de Dados:";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E12A22',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         A397FuncaoDados_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWctabelas = "";
         WebComp_Wctabelas_Component = "";
         scmdbuf = "";
         H00A22_A370FuncaoDados_SistemaCod = new int[1] ;
         H00A22_A397FuncaoDados_Descricao = new String[] {""} ;
         H00A22_n397FuncaoDados_Descricao = new bool[] {false} ;
         H00A22_A373FuncaoDados_Tipo = new String[] {""} ;
         H00A22_A368FuncaoDados_Codigo = new int[1] ;
         H00A22_A755FuncaoDados_Contar = new bool[] {false} ;
         H00A22_n755FuncaoDados_Contar = new bool[] {false} ;
         GXt_char2 = "";
         H00A23_A368FuncaoDados_Codigo = new int[1] ;
         H00A23_A370FuncaoDados_SistemaCod = new int[1] ;
         H00A23_A369FuncaoDados_Nome = new String[] {""} ;
         A369FuncaoDados_Nome = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_tipo6_Jsonclick = "";
         lblTextblockfuncaodados_tipo2_Jsonclick = "";
         lblTextblockfuncaodados_tipo3_Jsonclick = "";
         lblTextblockfuncaodados_tipo4_Jsonclick = "";
         lblTextblockfuncaodados_tipo5_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_funcaodadosdetalhes__default(),
            new Object[][] {
                new Object[] {
               H00A22_A370FuncaoDados_SistemaCod, H00A22_A397FuncaoDados_Descricao, H00A22_n397FuncaoDados_Descricao, H00A22_A373FuncaoDados_Tipo, H00A22_A368FuncaoDados_Codigo, H00A22_A755FuncaoDados_Contar, H00A22_n755FuncaoDados_Contar
               }
               , new Object[] {
               H00A23_A368FuncaoDados_Codigo, H00A23_A370FuncaoDados_SistemaCod, H00A23_A369FuncaoDados_Nome
               }
            }
         );
         WebComp_Wctabelas = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavFuncaodados_nome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GXt_int1 ;
      private short nGXWrapped ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int wcpOA368FuncaoDados_Codigo ;
      private int wcpOA370FuncaoDados_SistemaCod ;
      private int edtavFuncaodados_nome_Enabled ;
      private int idxLst ;
      private decimal A377FuncaoDados_PF ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A373FuncaoDados_Tipo ;
      private String A376FuncaoDados_Complexidade ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWctabelas ;
      private String WebComp_Wctabelas_Component ;
      private String edtavFuncaodados_nome_Internalname ;
      private String scmdbuf ;
      private String GXt_char2 ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String edtFuncaoDados_Descricao_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableattributes_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String TempTags ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String lblTextblockfuncaodados_tipo6_Internalname ;
      private String lblTextblockfuncaodados_tipo6_Jsonclick ;
      private String tblTable3_Internalname ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String lblTextblockfuncaodados_tipo2_Internalname ;
      private String lblTextblockfuncaodados_tipo2_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String lblTextblockfuncaodados_tipo3_Internalname ;
      private String lblTextblockfuncaodados_tipo3_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String lblTextblockfuncaodados_tipo4_Internalname ;
      private String lblTextblockfuncaodados_tipo4_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String lblTextblockfuncaodados_tipo5_Internalname ;
      private String lblTextblockfuncaodados_tipo5_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String tblTablemergedviewtitle2_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n397FuncaoDados_Descricao ;
      private bool n755FuncaoDados_Contar ;
      private bool returnInSub ;
      private String A397FuncaoDados_Descricao ;
      private String AV15FuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private GXWebComponent WebComp_Wctabelas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private IDataStoreProvider pr_default ;
      private int[] H00A22_A370FuncaoDados_SistemaCod ;
      private String[] H00A22_A397FuncaoDados_Descricao ;
      private bool[] H00A22_n397FuncaoDados_Descricao ;
      private String[] H00A22_A373FuncaoDados_Tipo ;
      private int[] H00A22_A368FuncaoDados_Codigo ;
      private bool[] H00A22_A755FuncaoDados_Contar ;
      private bool[] H00A22_n755FuncaoDados_Contar ;
      private int[] H00A23_A368FuncaoDados_Codigo ;
      private int[] H00A23_A370FuncaoDados_SistemaCod ;
      private String[] H00A23_A369FuncaoDados_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_funcaodadosdetalhes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A22 ;
          prmH00A22 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A23 ;
          prmH00A23 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A22", "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_Codigo], [FuncaoDados_Contar] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_Codigo] = @FuncaoDados_Codigo) AND ([FuncaoDados_SistemaCod] = @FuncaoDados_SistemaCod) ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A22,1,0,true,true )
             ,new CursorDef("H00A23", "SELECT [FuncaoDados_Codigo], [FuncaoDados_SistemaCod], [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_Codigo] = @FuncaoDados_Codigo) AND ([FuncaoDados_SistemaCod] = @FuncaoDados_SistemaCod) ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A23,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
