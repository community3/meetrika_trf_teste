/*
               File: GetWWPerfilFilterData
        Description: Get WWPerfil Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/3/2020 0:36:53.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwperfilfilterdata : GXProcedure
   {
      public getwwperfilfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwperfilfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwperfilfilterdata objgetwwperfilfilterdata;
         objgetwwperfilfilterdata = new getwwperfilfilterdata();
         objgetwwperfilfilterdata.AV15DDOName = aP0_DDOName;
         objgetwwperfilfilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwwperfilfilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwwperfilfilterdata.AV19OptionsJson = "" ;
         objgetwwperfilfilterdata.AV22OptionsDescJson = "" ;
         objgetwwperfilfilterdata.AV24OptionIndexesJson = "" ;
         objgetwwperfilfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwperfilfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwperfilfilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwperfilfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_PERFIL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPERFIL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWPerfilGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWPerfilGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWPerfilGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "PERFIL_AREATRABALHOCOD") == 0 )
            {
               AV31Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV10TFPerfil_Nome = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV11TFPerfil_Nome_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFPERFIL_ATIVO_SEL") == 0 )
            {
               AV12TFPerfil_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV33Perfil_Nome1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPERFIL_NOMEOPTIONS' Routine */
         AV10TFPerfil_Nome = AV13SearchTxt;
         AV11TFPerfil_Nome_Sel = "";
         AV39WWPerfilDS_1_Perfil_areatrabalhocod = AV31Perfil_AreaTrabalhoCod;
         AV40WWPerfilDS_2_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV41WWPerfilDS_3_Perfil_nome1 = AV33Perfil_Nome1;
         AV42WWPerfilDS_4_Tfperfil_nome = AV10TFPerfil_Nome;
         AV43WWPerfilDS_5_Tfperfil_nome_sel = AV11TFPerfil_Nome_Sel;
         AV44WWPerfilDS_6_Tfperfil_ativo_sel = AV12TFPerfil_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A3Perfil_Codigo ,
                                              AV34Perfis ,
                                              AV40WWPerfilDS_2_Dynamicfiltersselector1 ,
                                              AV41WWPerfilDS_3_Perfil_nome1 ,
                                              AV43WWPerfilDS_5_Tfperfil_nome_sel ,
                                              AV42WWPerfilDS_4_Tfperfil_nome ,
                                              AV44WWPerfilDS_6_Tfperfil_ativo_sel ,
                                              AV34Perfis.Count ,
                                              A4Perfil_Nome ,
                                              A276Perfil_Ativo ,
                                              A7Perfil_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV41WWPerfilDS_3_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV41WWPerfilDS_3_Perfil_nome1), 50, "%");
         lV42WWPerfilDS_4_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV42WWPerfilDS_4_Tfperfil_nome), 50, "%");
         /* Using cursor P00DY2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV41WWPerfilDS_3_Perfil_nome1, lV42WWPerfilDS_4_Tfperfil_nome, AV43WWPerfilDS_5_Tfperfil_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKDY2 = false;
            A7Perfil_AreaTrabalhoCod = P00DY2_A7Perfil_AreaTrabalhoCod[0];
            A4Perfil_Nome = P00DY2_A4Perfil_Nome[0];
            A3Perfil_Codigo = P00DY2_A3Perfil_Codigo[0];
            A276Perfil_Ativo = P00DY2_A276Perfil_Ativo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00DY2_A4Perfil_Nome[0], A4Perfil_Nome) == 0 ) )
            {
               BRKDY2 = false;
               A3Perfil_Codigo = P00DY2_A3Perfil_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKDY2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A4Perfil_Nome)) )
            {
               AV17Option = A4Perfil_Nome;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKDY2 )
            {
               BRKDY2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFPerfil_Nome = "";
         AV11TFPerfil_Nome_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33Perfil_Nome1 = "";
         AV40WWPerfilDS_2_Dynamicfiltersselector1 = "";
         AV41WWPerfilDS_3_Perfil_nome1 = "";
         AV42WWPerfilDS_4_Tfperfil_nome = "";
         AV43WWPerfilDS_5_Tfperfil_nome_sel = "";
         AV34Perfis = new GxSimpleCollection();
         scmdbuf = "";
         lV41WWPerfilDS_3_Perfil_nome1 = "";
         lV42WWPerfilDS_4_Tfperfil_nome = "";
         A4Perfil_Nome = "";
         P00DY2_A7Perfil_AreaTrabalhoCod = new int[1] ;
         P00DY2_A4Perfil_Nome = new String[] {""} ;
         P00DY2_A3Perfil_Codigo = new int[1] ;
         P00DY2_A276Perfil_Ativo = new bool[] {false} ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwperfilfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00DY2_A7Perfil_AreaTrabalhoCod, P00DY2_A4Perfil_Nome, P00DY2_A3Perfil_Codigo, P00DY2_A276Perfil_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFPerfil_Ativo_Sel ;
      private short AV44WWPerfilDS_6_Tfperfil_ativo_sel ;
      private int AV37GXV1 ;
      private int AV31Perfil_AreaTrabalhoCod ;
      private int AV39WWPerfilDS_1_Perfil_areatrabalhocod ;
      private int AV34Perfis_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A3Perfil_Codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private long AV25count ;
      private String AV10TFPerfil_Nome ;
      private String AV11TFPerfil_Nome_Sel ;
      private String AV33Perfil_Nome1 ;
      private String AV41WWPerfilDS_3_Perfil_nome1 ;
      private String AV42WWPerfilDS_4_Tfperfil_nome ;
      private String AV43WWPerfilDS_5_Tfperfil_nome_sel ;
      private String scmdbuf ;
      private String lV41WWPerfilDS_3_Perfil_nome1 ;
      private String lV42WWPerfilDS_4_Tfperfil_nome ;
      private String A4Perfil_Nome ;
      private bool returnInSub ;
      private bool A276Perfil_Ativo ;
      private bool BRKDY2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV40WWPerfilDS_2_Dynamicfiltersselector1 ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00DY2_A7Perfil_AreaTrabalhoCod ;
      private String[] P00DY2_A4Perfil_Nome ;
      private int[] P00DY2_A3Perfil_Codigo ;
      private bool[] P00DY2_A276Perfil_Ativo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV34Perfis ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwwperfilfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00DY2( IGxContext context ,
                                             int A3Perfil_Codigo ,
                                             IGxCollection AV34Perfis ,
                                             String AV40WWPerfilDS_2_Dynamicfiltersselector1 ,
                                             String AV41WWPerfilDS_3_Perfil_nome1 ,
                                             String AV43WWPerfilDS_5_Tfperfil_nome_sel ,
                                             String AV42WWPerfilDS_4_Tfperfil_nome ,
                                             short AV44WWPerfilDS_6_Tfperfil_ativo_sel ,
                                             int AV34Perfis_Count ,
                                             String A4Perfil_Nome ,
                                             bool A276Perfil_Ativo ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Perfil_AreaTrabalhoCod], [Perfil_Nome], [Perfil_Codigo], [Perfil_Ativo] FROM [Perfil] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Perfil_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV40WWPerfilDS_2_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWPerfilDS_3_Perfil_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like '%' + @lV41WWPerfilDS_3_Perfil_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43WWPerfilDS_5_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42WWPerfilDS_4_Tfperfil_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like @lV42WWPerfilDS_4_Tfperfil_nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43WWPerfilDS_5_Tfperfil_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] = @AV43WWPerfilDS_5_Tfperfil_nome_sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44WWPerfilDS_6_Tfperfil_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 1)";
         }
         if ( AV44WWPerfilDS_6_Tfperfil_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 0)";
         }
         if ( AV34Perfis_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV34Perfis, "[Perfil_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Perfil_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00DY2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DY2 ;
          prmP00DY2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41WWPerfilDS_3_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42WWPerfilDS_4_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43WWPerfilDS_5_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DY2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwperfilfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwperfilfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwperfilfilterdata") )
          {
             return  ;
          }
          getwwperfilfilterdata worker = new getwwperfilfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
