/*
               File: ProjetoDesenvolvimento
        Description: Projeto Desenvolvimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:2:59.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetodesenvolvimento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel2"+"_"+"PROJETODESENVOLVIMENTO_SISTEMACOD") == 0 )
         {
            AV15ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETODESENVOLVIMENTO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX2ASAPROJETODESENVOLVIMENTO_SISTEMACOD2A88( AV15ProjetoDesenvolvimento_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A670ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A670ProjetoDesenvolvimento_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV14ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETODESENVOLVIMENTO_PROJETOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
               AV15ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETODESENVOLVIMENTO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
               AV21Sistema_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Tipo", AV21Sistema_Tipo);
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbavSistema_tecnica.Name = "vSISTEMA_TECNICA";
         cmbavSistema_tecnica.WebTags = "";
         cmbavSistema_tecnica.addItem("", "(Nenhum)", 0);
         cmbavSistema_tecnica.addItem("I", "Indicativa", 0);
         cmbavSistema_tecnica.addItem("E", "Estimada", 0);
         cmbavSistema_tecnica.addItem("D", "Detalhada", 0);
         if ( cmbavSistema_tecnica.ItemCount > 0 )
         {
            AV23Sistema_Tecnica = cmbavSistema_tecnica.getValidValue(AV23Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_Tecnica", AV23Sistema_Tecnica);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto Desenvolvimento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public projetodesenvolvimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projetodesenvolvimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ProjetoDesenvolvimento_ProjetoCod ,
                           int aP2_ProjetoDesenvolvimento_SistemaCod ,
                           ref String aP3_Sistema_Tipo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV14ProjetoDesenvolvimento_ProjetoCod = aP1_ProjetoDesenvolvimento_ProjetoCod;
         this.AV15ProjetoDesenvolvimento_SistemaCod = aP2_ProjetoDesenvolvimento_SistemaCod;
         this.AV21Sistema_Tipo = aP3_Sistema_Tipo;
         executePrivate();
         aP3_Sistema_Tipo=this.AV21Sistema_Tipo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavSistema_tecnica = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavSistema_tecnica.ItemCount > 0 )
         {
            AV23Sistema_Tecnica = cmbavSistema_tecnica.getValidValue(AV23Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_Tecnica", AV23Sistema_Tecnica);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Event 'Enter' not assigned to any button. */
            /* Form start */
            wb_table1_2_2A88( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2A88e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjetoDesenvolvimento_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoDesenvolvimento_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", edtProjetoDesenvolvimento_SistemaCod_Visible, edtProjetoDesenvolvimento_SistemaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoDesenvolvimento.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2A88( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2A88( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2A88e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_52_2A88( true) ;
         }
         return  ;
      }

      protected void wb_table3_52_2A88e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2A88e( true) ;
         }
         else
         {
            wb_table1_2_2A88e( false) ;
         }
      }

      protected void wb_table3_52_2A88( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, bttBtnfechar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_52_2A88e( true) ;
         }
         else
         {
            wb_table3_52_2A88e( false) ;
         }
      }

      protected void wb_table2_5_2A88( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2A88( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2A88e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2A88e( true) ;
         }
         else
         {
            wb_table2_5_2A88e( false) ;
         }
      }

      protected void wb_table4_13_2A88( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Projeto", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Nome_Internalname, StringUtil.RTrim( A649Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Nome_Enabled, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "Nome", "left", true, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetodesenvolvimento_sistemanom_Internalname, "Sub Projeto", "", "", lblTextblockprojetodesenvolvimento_sistemanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjetodesenvolvimento_sistemanom_Internalname, AV16ProjetoDesenvolvimento_SistemaNom, StringUtil.RTrim( context.localUtil.Format( AV16ProjetoDesenvolvimento_SistemaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjetodesenvolvimento_sistemanom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavProjetodesenvolvimento_sistemanom_Enabled, 0, "text", "", 450, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_tecnica_Internalname, "T�cnica", "", "", lblTextblocksistema_tecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tecnica, cmbavSistema_tecnica_Internalname, StringUtil.RTrim( AV23Sistema_Tecnica), 1, cmbavSistema_tecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavSistema_tecnica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_ProjetoDesenvolvimento.htm");
            cmbavSistema_tecnica.CurrentValue = StringUtil.RTrim( AV23Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica_Internalname, "Values", (String)(cmbavSistema_tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_fatorajuste_Internalname, "Fator de Ajuste", "", "", lblTextblocksistema_fatorajuste_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_fatorajuste_Internalname, StringUtil.LTrim( StringUtil.NToC( AV17Sistema_FatorAjuste, 6, 2, ",", "")), ((edtavSistema_fatorajuste_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV17Sistema_FatorAjuste, "ZZ9.99")) : context.localUtil.Format( AV17Sistema_FatorAjuste, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_fatorajuste_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSistema_fatorajuste_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_prazo_Internalname, "Prazo", "", "", lblTextblocksistema_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Sistema_Prazo), 4, 0, ",", "")), ((edtavSistema_prazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18Sistema_Prazo), "ZZZ9")) : context.localUtil.Format( (decimal)(AV18Sistema_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_prazo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSistema_prazo_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_custo_Internalname, "Custo", "", "", lblTextblocksistema_custo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_custo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19Sistema_Custo, 18, 5, ",", "")), ((edtavSistema_custo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV19Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_custo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSistema_custo_Enabled, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_esforco_Internalname, "Esforco", "", "", lblTextblocksistema_esforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_44_2A88( true) ;
         }
         return  ;
      }

      protected void wb_table5_44_2A88e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2A88e( true) ;
         }
         else
         {
            wb_table4_13_2A88e( false) ;
         }
      }

      protected void wb_table5_44_2A88( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedsistema_esforco_Internalname, tblTablemergedsistema_esforco_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Sistema_Esforco), 4, 0, ",", "")), ((edtavSistema_esforco_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Sistema_Esforco), "ZZZ9")) : context.localUtil.Format( (decimal)(AV20Sistema_Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_esforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSistema_esforco_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgTrn_enter_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgTrn_enter_Visible, imgTrn_enter_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgTrn_enter_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_44_2A88e( true) ;
         }
         else
         {
            wb_table5_44_2A88e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112A2 */
         E112A2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
               n649Projeto_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
               AV16ProjetoDesenvolvimento_SistemaNom = StringUtil.Upper( cgiGet( edtavProjetodesenvolvimento_sistemanom_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ProjetoDesenvolvimento_SistemaNom", AV16ProjetoDesenvolvimento_SistemaNom);
               cmbavSistema_tecnica.CurrentValue = cgiGet( cmbavSistema_tecnica_Internalname);
               AV23Sistema_Tecnica = cgiGet( cmbavSistema_tecnica_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_Tecnica", AV23Sistema_Tecnica);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_FATORAJUSTE");
                  AnyError = 1;
                  GX_FocusControl = edtavSistema_fatorajuste_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV17Sistema_FatorAjuste = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV17Sistema_FatorAjuste, 6, 2)));
               }
               else
               {
                  AV17Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV17Sistema_FatorAjuste, 6, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtavSistema_prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV18Sistema_Prazo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Sistema_Prazo), 4, 0)));
               }
               else
               {
                  AV18Sistema_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Sistema_Prazo), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_CUSTO");
                  AnyError = 1;
                  GX_FocusControl = edtavSistema_custo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV19Sistema_Custo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV19Sistema_Custo, 18, 5)));
               }
               else
               {
                  AV19Sistema_Custo = context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV19Sistema_Custo, 18, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_ESFORCO");
                  AnyError = 1;
                  GX_FocusControl = edtavSistema_esforco_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV20Sistema_Esforco = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Esforco), 4, 0)));
               }
               else
               {
                  AV20Sistema_Esforco = (short)(context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Esforco), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A670ProjetoDesenvolvimento_SistemaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
               }
               else
               {
                  A670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_SistemaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
               }
               /* Read saved values. */
               Z669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z669ProjetoDesenvolvimento_ProjetoCod"), ",", "."));
               Z670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z670ProjetoDesenvolvimento_SistemaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV14ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "vPROJETODESENVOLVIMENTO_PROJETOCOD"), ",", "."));
               A669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "PROJETODESENVOLVIMENTO_PROJETOCOD"), ",", "."));
               AV15ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vPROJETODESENVOLVIMENTO_SISTEMACOD"), ",", "."));
               AV21Sistema_Tipo = cgiGet( "vSISTEMA_TIPO");
               A671ProjetoDesenvolvimento_SistemaNom = cgiGet( "PROJETODESENVOLVIMENTO_SISTEMANOM");
               n671ProjetoDesenvolvimento_SistemaNom = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ProjetoDesenvolvimento";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A670ProjetoDesenvolvimento_SistemaCod != Z670ProjetoDesenvolvimento_SistemaCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("projetodesenvolvimento:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("projetodesenvolvimento:[SecurityCheckFailed value for]"+"ProjetoDesenvolvimento_ProjetoCod:"+context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A669ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
                  A670ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode88 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode88;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound88 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2A0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = imgTrn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
                        AnyError = 1;
                        GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112A2 */
                           E112A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122A2 */
                           E122A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132A2 */
                           E132A2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122A2 */
            E122A2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2A88( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               imgTrn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTrn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTrn_enter_Visible), 5, 0)));
            }
            DisableAttributes2A88( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjetodesenvolvimento_sistemanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjetodesenvolvimento_sistemanom_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_fatorajuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_fatorajuste_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_prazo_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_custo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_custo_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_esforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_esforco_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2A0( )
      {
         BeforeValidate2A88( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2A88( ) ;
            }
            else
            {
               CheckExtendedTable2A88( ) ;
               CloseExtendedTableCursors2A88( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2A0( )
      {
      }

      protected void E112A2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtProjetoDesenvolvimento_SistemaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Visible), 5, 0)));
         GXt_decimal1 = AV17Sistema_FatorAjuste;
         new prc_fapadrao(context ).execute( out  GXt_decimal1) ;
         AV17Sistema_FatorAjuste = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV17Sistema_FatorAjuste, 6, 2)));
      }

      protected void E122A2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +AV14ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(AV21Sistema_Tipo));
            context.wjLocDisableFrm = 1;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwprojetodesenvolvimento.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {(String)AV21Sistema_Tipo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E132A2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwsistema.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void ZM2A88( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -8 )
         {
            Z669ProjetoDesenvolvimento_ProjetoCod = A669ProjetoDesenvolvimento_ProjetoCod;
            Z670ProjetoDesenvolvimento_SistemaCod = A670ProjetoDesenvolvimento_SistemaCod;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z671ProjetoDesenvolvimento_SistemaNom = A671ProjetoDesenvolvimento_SistemaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV14ProjetoDesenvolvimento_ProjetoCod) )
         {
            A669ProjetoDesenvolvimento_ProjetoCod = AV14ProjetoDesenvolvimento_ProjetoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         }
         /* Using cursor T002A4 */
         pr_default.execute(2, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Desenvolvimento Projeto'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A649Projeto_Nome = T002A4_A649Projeto_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
         n649Projeto_Nome = T002A4_n649Projeto_Nome[0];
         pr_default.close(2);
         if ( ! (0==AV15ProjetoDesenvolvimento_SistemaCod) )
         {
            A670ProjetoDesenvolvimento_SistemaCod = AV15ProjetoDesenvolvimento_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
         if ( ! (0==AV15ProjetoDesenvolvimento_SistemaCod) )
         {
            edtProjetoDesenvolvimento_SistemaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Enabled), 5, 0)));
         }
         else
         {
            edtProjetoDesenvolvimento_SistemaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV15ProjetoDesenvolvimento_SistemaCod) )
         {
            edtProjetoDesenvolvimento_SistemaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgTrn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTrn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTrn_enter_Enabled), 5, 0)));
         }
         else
         {
            imgTrn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTrn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTrn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002A5 */
            pr_default.execute(3, new Object[] {A670ProjetoDesenvolvimento_SistemaCod});
            A671ProjetoDesenvolvimento_SistemaNom = T002A5_A671ProjetoDesenvolvimento_SistemaNom[0];
            n671ProjetoDesenvolvimento_SistemaNom = T002A5_n671ProjetoDesenvolvimento_SistemaNom[0];
            pr_default.close(3);
         }
      }

      protected void Load2A88( )
      {
         /* Using cursor T002A6 */
         pr_default.execute(4, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound88 = 1;
            A649Projeto_Nome = T002A6_A649Projeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
            n649Projeto_Nome = T002A6_n649Projeto_Nome[0];
            A671ProjetoDesenvolvimento_SistemaNom = T002A6_A671ProjetoDesenvolvimento_SistemaNom[0];
            n671ProjetoDesenvolvimento_SistemaNom = T002A6_n671ProjetoDesenvolvimento_SistemaNom[0];
            ZM2A88( -8) ;
         }
         pr_default.close(4);
         OnLoadActions2A88( ) ;
      }

      protected void OnLoadActions2A88( )
      {
      }

      protected void CheckExtendedTable2A88( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002A5 */
         pr_default.execute(3, new Object[] {A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Desenvolvimento_Sistema'.", "ForeignKeyNotFound", 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A671ProjetoDesenvolvimento_SistemaNom = T002A5_A671ProjetoDesenvolvimento_SistemaNom[0];
         n671ProjetoDesenvolvimento_SistemaNom = T002A5_n671ProjetoDesenvolvimento_SistemaNom[0];
         pr_default.close(3);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom)) )
         {
            GX_msglist.addItem("Nome do Sub Projeto a Desenvolver � obrigat�rio!", 1, "vPROJETODESENVOLVIMENTO_SISTEMANOM");
            AnyError = 1;
            GX_FocusControl = edtavProjetodesenvolvimento_sistemanom_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2A88( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A670ProjetoDesenvolvimento_SistemaCod )
      {
         /* Using cursor T002A7 */
         pr_default.execute(5, new Object[] {A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Desenvolvimento_Sistema'.", "ForeignKeyNotFound", 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A671ProjetoDesenvolvimento_SistemaNom = T002A7_A671ProjetoDesenvolvimento_SistemaNom[0];
         n671ProjetoDesenvolvimento_SistemaNom = T002A7_n671ProjetoDesenvolvimento_SistemaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A671ProjetoDesenvolvimento_SistemaNom)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey2A88( )
      {
         /* Using cursor T002A8 */
         pr_default.execute(6, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound88 = 1;
         }
         else
         {
            RcdFound88 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002A3 */
         pr_default.execute(1, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2A88( 8) ;
            RcdFound88 = 1;
            A669ProjetoDesenvolvimento_ProjetoCod = T002A3_A669ProjetoDesenvolvimento_ProjetoCod[0];
            A670ProjetoDesenvolvimento_SistemaCod = T002A3_A670ProjetoDesenvolvimento_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
            Z669ProjetoDesenvolvimento_ProjetoCod = A669ProjetoDesenvolvimento_ProjetoCod;
            Z670ProjetoDesenvolvimento_SistemaCod = A670ProjetoDesenvolvimento_SistemaCod;
            sMode88 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2A88( ) ;
            if ( AnyError == 1 )
            {
               RcdFound88 = 0;
               InitializeNonKey2A88( ) ;
            }
            Gx_mode = sMode88;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound88 = 0;
            InitializeNonKey2A88( ) ;
            sMode88 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode88;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2A88( ) ;
         if ( RcdFound88 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound88 = 0;
         /* Using cursor T002A9 */
         pr_default.execute(7, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T002A9_A669ProjetoDesenvolvimento_ProjetoCod[0] < A669ProjetoDesenvolvimento_ProjetoCod ) || ( T002A9_A669ProjetoDesenvolvimento_ProjetoCod[0] == A669ProjetoDesenvolvimento_ProjetoCod ) && ( T002A9_A670ProjetoDesenvolvimento_SistemaCod[0] < A670ProjetoDesenvolvimento_SistemaCod ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T002A9_A669ProjetoDesenvolvimento_ProjetoCod[0] > A669ProjetoDesenvolvimento_ProjetoCod ) || ( T002A9_A669ProjetoDesenvolvimento_ProjetoCod[0] == A669ProjetoDesenvolvimento_ProjetoCod ) && ( T002A9_A670ProjetoDesenvolvimento_SistemaCod[0] > A670ProjetoDesenvolvimento_SistemaCod ) ) )
            {
               A669ProjetoDesenvolvimento_ProjetoCod = T002A9_A669ProjetoDesenvolvimento_ProjetoCod[0];
               A670ProjetoDesenvolvimento_SistemaCod = T002A9_A670ProjetoDesenvolvimento_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
               RcdFound88 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound88 = 0;
         /* Using cursor T002A10 */
         pr_default.execute(8, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002A10_A669ProjetoDesenvolvimento_ProjetoCod[0] > A669ProjetoDesenvolvimento_ProjetoCod ) || ( T002A10_A669ProjetoDesenvolvimento_ProjetoCod[0] == A669ProjetoDesenvolvimento_ProjetoCod ) && ( T002A10_A670ProjetoDesenvolvimento_SistemaCod[0] > A670ProjetoDesenvolvimento_SistemaCod ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002A10_A669ProjetoDesenvolvimento_ProjetoCod[0] < A669ProjetoDesenvolvimento_ProjetoCod ) || ( T002A10_A669ProjetoDesenvolvimento_ProjetoCod[0] == A669ProjetoDesenvolvimento_ProjetoCod ) && ( T002A10_A670ProjetoDesenvolvimento_SistemaCod[0] < A670ProjetoDesenvolvimento_SistemaCod ) ) )
            {
               A669ProjetoDesenvolvimento_ProjetoCod = T002A10_A669ProjetoDesenvolvimento_ProjetoCod[0];
               A670ProjetoDesenvolvimento_SistemaCod = T002A10_A670ProjetoDesenvolvimento_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
               RcdFound88 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2A88( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2A88( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound88 == 1 )
            {
               if ( ( A669ProjetoDesenvolvimento_ProjetoCod != Z669ProjetoDesenvolvimento_ProjetoCod ) || ( A670ProjetoDesenvolvimento_SistemaCod != Z670ProjetoDesenvolvimento_SistemaCod ) )
               {
                  A669ProjetoDesenvolvimento_ProjetoCod = Z669ProjetoDesenvolvimento_ProjetoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
                  A670ProjetoDesenvolvimento_SistemaCod = Z670ProjetoDesenvolvimento_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2A88( ) ;
                  GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A669ProjetoDesenvolvimento_ProjetoCod != Z669ProjetoDesenvolvimento_ProjetoCod ) || ( A670ProjetoDesenvolvimento_SistemaCod != Z670ProjetoDesenvolvimento_SistemaCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2A88( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
                     AnyError = 1;
                     GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2A88( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A669ProjetoDesenvolvimento_ProjetoCod != Z669ProjetoDesenvolvimento_ProjetoCod ) || ( A670ProjetoDesenvolvimento_SistemaCod != Z670ProjetoDesenvolvimento_SistemaCod ) )
         {
            A669ProjetoDesenvolvimento_ProjetoCod = Z669ProjetoDesenvolvimento_ProjetoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            A670ProjetoDesenvolvimento_SistemaCod = Z670ProjetoDesenvolvimento_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2A88( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002A2 */
            pr_default.execute(0, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoDesenvolvimento"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ProjetoDesenvolvimento"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2A88( )
      {
         BeforeValidate2A88( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2A88( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2A88( 0) ;
            CheckOptimisticConcurrency2A88( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2A88( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2A88( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002A11 */
                     pr_default.execute(9, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoDesenvolvimento") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2A0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2A88( ) ;
            }
            EndLevel2A88( ) ;
         }
         CloseExtendedTableCursors2A88( ) ;
      }

      protected void Update2A88( )
      {
         BeforeValidate2A88( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2A88( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2A88( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2A88( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2A88( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ProjetoDesenvolvimento] */
                     DeferredUpdate2A88( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2A88( ) ;
         }
         CloseExtendedTableCursors2A88( ) ;
      }

      protected void DeferredUpdate2A88( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2A88( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2A88( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2A88( ) ;
            AfterConfirm2A88( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2A88( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002A12 */
                  pr_default.execute(10, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ProjetoDesenvolvimento") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode88 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2A88( ) ;
         Gx_mode = sMode88;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2A88( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002A13 */
            pr_default.execute(11, new Object[] {A670ProjetoDesenvolvimento_SistemaCod});
            A671ProjetoDesenvolvimento_SistemaNom = T002A13_A671ProjetoDesenvolvimento_SistemaNom[0];
            n671ProjetoDesenvolvimento_SistemaNom = T002A13_n671ProjetoDesenvolvimento_SistemaNom[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel2A88( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2A88( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "ProjetoDesenvolvimento");
            if ( AnyError == 0 )
            {
               ConfirmValues2A0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "ProjetoDesenvolvimento");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2A88( )
      {
         /* Scan By routine */
         /* Using cursor T002A14 */
         pr_default.execute(12);
         RcdFound88 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound88 = 1;
            A669ProjetoDesenvolvimento_ProjetoCod = T002A14_A669ProjetoDesenvolvimento_ProjetoCod[0];
            A670ProjetoDesenvolvimento_SistemaCod = T002A14_A670ProjetoDesenvolvimento_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2A88( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound88 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound88 = 1;
            A669ProjetoDesenvolvimento_ProjetoCod = T002A14_A669ProjetoDesenvolvimento_ProjetoCod[0];
            A670ProjetoDesenvolvimento_SistemaCod = T002A14_A670ProjetoDesenvolvimento_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
      }

      protected void ScanEnd2A88( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm2A88( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2A88( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2A88( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2A88( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2A88( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2A88( )
      {
         /* Before Validate Rules */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom)) )
         {
            GXt_int2 = A670ProjetoDesenvolvimento_SistemaCod;
            new prc_newsistema(context ).execute(  AV14ProjetoDesenvolvimento_ProjetoCod,  AV16ProjetoDesenvolvimento_SistemaNom,  AV9WWPContext.gxTpr_Areatrabalho_codigo, ref  AV17Sistema_FatorAjuste, ref  AV18Sistema_Prazo, ref  AV19Sistema_Custo, ref  AV20Sistema_Esforco, ref  AV21Sistema_Tipo, ref  AV23Sistema_Tecnica, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETODESENVOLVIMENTO_PROJETOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ProjetoDesenvolvimento_SistemaNom", AV16ProjetoDesenvolvimento_SistemaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV17Sistema_FatorAjuste, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Sistema_Prazo), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV19Sistema_Custo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Esforco), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Tipo", AV21Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_Tecnica", AV23Sistema_Tecnica);
            A670ProjetoDesenvolvimento_SistemaCod = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
      }

      protected void DisableAttributes2A88( )
      {
         edtProjeto_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Nome_Enabled), 5, 0)));
         edtavProjetodesenvolvimento_sistemanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjetodesenvolvimento_sistemanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjetodesenvolvimento_sistemanom_Enabled), 5, 0)));
         cmbavSistema_tecnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica.Enabled), 5, 0)));
         edtavSistema_fatorajuste_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_fatorajuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_fatorajuste_Enabled), 5, 0)));
         edtavSistema_prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_prazo_Enabled), 5, 0)));
         edtavSistema_custo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_custo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_custo_Enabled), 5, 0)));
         edtavSistema_esforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_esforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_esforco_Enabled), 5, 0)));
         edtProjetoDesenvolvimento_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2A0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428233094");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +AV15ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV21Sistema_Tipo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPROJETODESENVOLVIMENTO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_TIPO", StringUtil.RTrim( AV21Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "PROJETODESENVOLVIMENTO_SISTEMANOM", A671ProjetoDesenvolvimento_SistemaNom);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETODESENVOLVIMENTO_PROJETOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETODESENVOLVIMENTO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ProjetoDesenvolvimento";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("projetodesenvolvimento:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("projetodesenvolvimento:[SendSecurityCheck value for]"+"ProjetoDesenvolvimento_ProjetoCod:"+context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +AV15ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV21Sistema_Tipo)) ;
      }

      public override String GetPgmname( )
      {
         return "ProjetoDesenvolvimento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Desenvolvimento" ;
      }

      protected void InitializeNonKey2A88( )
      {
         AV16ProjetoDesenvolvimento_SistemaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ProjetoDesenvolvimento_SistemaNom", AV16ProjetoDesenvolvimento_SistemaNom);
         AV23Sistema_Tecnica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_Tecnica", AV23Sistema_Tecnica);
         AV18Sistema_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Sistema_Prazo), 4, 0)));
         AV19Sistema_Custo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV19Sistema_Custo, 18, 5)));
         AV20Sistema_Esforco = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Esforco), 4, 0)));
         A671ProjetoDesenvolvimento_SistemaNom = "";
         n671ProjetoDesenvolvimento_SistemaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A671ProjetoDesenvolvimento_SistemaNom", A671ProjetoDesenvolvimento_SistemaNom);
      }

      protected void InitAll2A88( )
      {
         A669ProjetoDesenvolvimento_ProjetoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         A670ProjetoDesenvolvimento_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         InitializeNonKey2A88( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428233117");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("projetodesenvolvimento.js", "?2020428233117");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_nome_Internalname = "TEXTBLOCKPROJETO_NOME";
         edtProjeto_Nome_Internalname = "PROJETO_NOME";
         lblTextblockprojetodesenvolvimento_sistemanom_Internalname = "TEXTBLOCKPROJETODESENVOLVIMENTO_SISTEMANOM";
         edtavProjetodesenvolvimento_sistemanom_Internalname = "vPROJETODESENVOLVIMENTO_SISTEMANOM";
         lblTextblocksistema_tecnica_Internalname = "TEXTBLOCKSISTEMA_TECNICA";
         cmbavSistema_tecnica_Internalname = "vSISTEMA_TECNICA";
         lblTextblocksistema_fatorajuste_Internalname = "TEXTBLOCKSISTEMA_FATORAJUSTE";
         edtavSistema_fatorajuste_Internalname = "vSISTEMA_FATORAJUSTE";
         lblTextblocksistema_prazo_Internalname = "TEXTBLOCKSISTEMA_PRAZO";
         edtavSistema_prazo_Internalname = "vSISTEMA_PRAZO";
         lblTextblocksistema_custo_Internalname = "TEXTBLOCKSISTEMA_CUSTO";
         edtavSistema_custo_Internalname = "vSISTEMA_CUSTO";
         lblTextblocksistema_esforco_Internalname = "TEXTBLOCKSISTEMA_ESFORCO";
         edtavSistema_esforco_Internalname = "vSISTEMA_ESFORCO";
         imgTrn_enter_Internalname = "TRN_ENTER";
         tblTablemergedsistema_esforco_Internalname = "TABLEMERGEDSISTEMA_ESFORCO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnfechar_Internalname = "BTNFECHAR";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtProjetoDesenvolvimento_SistemaCod_Internalname = "PROJETODESENVOLVIMENTO_SISTEMACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Projeto Desenvolvimento";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto Desenvolvimento";
         imgTrn_enter_Enabled = 1;
         imgTrn_enter_Visible = 1;
         edtavSistema_esforco_Jsonclick = "";
         edtavSistema_esforco_Enabled = 1;
         edtavSistema_custo_Jsonclick = "";
         edtavSistema_custo_Enabled = 1;
         edtavSistema_prazo_Jsonclick = "";
         edtavSistema_prazo_Enabled = 1;
         edtavSistema_fatorajuste_Jsonclick = "";
         edtavSistema_fatorajuste_Enabled = 1;
         cmbavSistema_tecnica_Jsonclick = "";
         cmbavSistema_tecnica.Enabled = 1;
         edtavProjetodesenvolvimento_sistemanom_Jsonclick = "";
         edtavProjetodesenvolvimento_sistemanom_Enabled = 1;
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Nome_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtnfechar_Visible = 1;
         edtProjetoDesenvolvimento_SistemaCod_Jsonclick = "";
         edtProjetoDesenvolvimento_SistemaCod_Enabled = 1;
         edtProjetoDesenvolvimento_SistemaCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX2ASAPROJETODESENVOLVIMENTO_SISTEMACOD2A88( int AV15ProjetoDesenvolvimento_SistemaCod )
      {
         if ( ! (0==AV15ProjetoDesenvolvimento_SistemaCod) )
         {
            A670ProjetoDesenvolvimento_SistemaCod = AV15ProjetoDesenvolvimento_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Projetodesenvolvimento_sistemacod( int GX_Parm1 ,
                                                           String GX_Parm2 )
      {
         A670ProjetoDesenvolvimento_SistemaCod = GX_Parm1;
         A671ProjetoDesenvolvimento_SistemaNom = GX_Parm2;
         n671ProjetoDesenvolvimento_SistemaNom = false;
         /* Using cursor T002A13 */
         pr_default.execute(11, new Object[] {A670ProjetoDesenvolvimento_SistemaCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Projeto Desenvolvimento_Sistema'.", "ForeignKeyNotFound", 1, "PROJETODESENVOLVIMENTO_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtProjetoDesenvolvimento_SistemaCod_Internalname;
         }
         A671ProjetoDesenvolvimento_SistemaNom = T002A13_A671ProjetoDesenvolvimento_SistemaNom[0];
         n671ProjetoDesenvolvimento_SistemaNom = T002A13_n671ProjetoDesenvolvimento_SistemaNom[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A671ProjetoDesenvolvimento_SistemaNom = "";
            n671ProjetoDesenvolvimento_SistemaNom = false;
         }
         isValidOutput.Add(A671ProjetoDesenvolvimento_SistemaNom);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14ProjetoDesenvolvimento_ProjetoCod',fld:'vPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15ProjetoDesenvolvimento_SistemaCod',fld:'vPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV21Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122A2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14ProjetoDesenvolvimento_ProjetoCod',fld:'vPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV21Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV21Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E132A2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV21Sistema_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         AV23Sistema_Tecnica = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnfechar_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         A649Projeto_Nome = "";
         lblTextblockprojetodesenvolvimento_sistemanom_Jsonclick = "";
         AV16ProjetoDesenvolvimento_SistemaNom = "";
         lblTextblocksistema_tecnica_Jsonclick = "";
         lblTextblocksistema_fatorajuste_Jsonclick = "";
         lblTextblocksistema_prazo_Jsonclick = "";
         lblTextblocksistema_custo_Jsonclick = "";
         lblTextblocksistema_esforco_Jsonclick = "";
         imgTrn_enter_Jsonclick = "";
         A671ProjetoDesenvolvimento_SistemaNom = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode88 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z649Projeto_Nome = "";
         Z671ProjetoDesenvolvimento_SistemaNom = "";
         T002A4_A649Projeto_Nome = new String[] {""} ;
         T002A4_n649Projeto_Nome = new bool[] {false} ;
         T002A5_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         T002A5_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         T002A6_A649Projeto_Nome = new String[] {""} ;
         T002A6_n649Projeto_Nome = new bool[] {false} ;
         T002A6_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         T002A6_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         T002A6_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A6_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A7_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         T002A7_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         T002A8_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A8_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A3_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A3_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A9_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A9_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A10_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A10_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A2_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A2_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002A13_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         T002A13_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         T002A14_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002A14_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetodesenvolvimento__default(),
            new Object[][] {
                new Object[] {
               T002A2_A669ProjetoDesenvolvimento_ProjetoCod, T002A2_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002A3_A669ProjetoDesenvolvimento_ProjetoCod, T002A3_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002A4_A649Projeto_Nome, T002A4_n649Projeto_Nome
               }
               , new Object[] {
               T002A5_A671ProjetoDesenvolvimento_SistemaNom, T002A5_n671ProjetoDesenvolvimento_SistemaNom
               }
               , new Object[] {
               T002A6_A649Projeto_Nome, T002A6_n649Projeto_Nome, T002A6_A671ProjetoDesenvolvimento_SistemaNom, T002A6_n671ProjetoDesenvolvimento_SistemaNom, T002A6_A669ProjetoDesenvolvimento_ProjetoCod, T002A6_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002A7_A671ProjetoDesenvolvimento_SistemaNom, T002A7_n671ProjetoDesenvolvimento_SistemaNom
               }
               , new Object[] {
               T002A8_A669ProjetoDesenvolvimento_ProjetoCod, T002A8_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002A9_A669ProjetoDesenvolvimento_ProjetoCod, T002A9_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002A10_A669ProjetoDesenvolvimento_ProjetoCod, T002A10_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002A13_A671ProjetoDesenvolvimento_SistemaNom, T002A13_n671ProjetoDesenvolvimento_SistemaNom
               }
               , new Object[] {
               T002A14_A669ProjetoDesenvolvimento_ProjetoCod, T002A14_A670ProjetoDesenvolvimento_SistemaCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short AV18Sistema_Prazo ;
      private short AV20Sistema_Esforco ;
      private short RcdFound88 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV14ProjetoDesenvolvimento_ProjetoCod ;
      private int wcpOAV15ProjetoDesenvolvimento_SistemaCod ;
      private int Z669ProjetoDesenvolvimento_ProjetoCod ;
      private int Z670ProjetoDesenvolvimento_SistemaCod ;
      private int AV15ProjetoDesenvolvimento_SistemaCod ;
      private int A670ProjetoDesenvolvimento_SistemaCod ;
      private int AV14ProjetoDesenvolvimento_ProjetoCod ;
      private int trnEnded ;
      private int edtProjetoDesenvolvimento_SistemaCod_Visible ;
      private int edtProjetoDesenvolvimento_SistemaCod_Enabled ;
      private int bttBtnfechar_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtProjeto_Nome_Enabled ;
      private int edtavProjetodesenvolvimento_sistemanom_Enabled ;
      private int edtavSistema_fatorajuste_Enabled ;
      private int edtavSistema_prazo_Enabled ;
      private int edtavSistema_custo_Enabled ;
      private int edtavSistema_esforco_Enabled ;
      private int imgTrn_enter_Visible ;
      private int imgTrn_enter_Enabled ;
      private int A669ProjetoDesenvolvimento_ProjetoCod ;
      private int GXt_int2 ;
      private int idxLst ;
      private decimal AV17Sistema_FatorAjuste ;
      private decimal AV19Sistema_Custo ;
      private decimal GXt_decimal1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String wcpOAV21Sistema_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String AV21Sistema_Tipo ;
      private String GXKey ;
      private String AV23Sistema_Tecnica ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProjetoDesenvolvimento_SistemaCod_Internalname ;
      private String TempTags ;
      private String edtProjetoDesenvolvimento_SistemaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtProjeto_Nome_Internalname ;
      private String A649Projeto_Nome ;
      private String edtProjeto_Nome_Jsonclick ;
      private String lblTextblockprojetodesenvolvimento_sistemanom_Internalname ;
      private String lblTextblockprojetodesenvolvimento_sistemanom_Jsonclick ;
      private String edtavProjetodesenvolvimento_sistemanom_Internalname ;
      private String edtavProjetodesenvolvimento_sistemanom_Jsonclick ;
      private String lblTextblocksistema_tecnica_Internalname ;
      private String lblTextblocksistema_tecnica_Jsonclick ;
      private String cmbavSistema_tecnica_Internalname ;
      private String cmbavSistema_tecnica_Jsonclick ;
      private String lblTextblocksistema_fatorajuste_Internalname ;
      private String lblTextblocksistema_fatorajuste_Jsonclick ;
      private String edtavSistema_fatorajuste_Internalname ;
      private String edtavSistema_fatorajuste_Jsonclick ;
      private String lblTextblocksistema_prazo_Internalname ;
      private String lblTextblocksistema_prazo_Jsonclick ;
      private String edtavSistema_prazo_Internalname ;
      private String edtavSistema_prazo_Jsonclick ;
      private String lblTextblocksistema_custo_Internalname ;
      private String lblTextblocksistema_custo_Jsonclick ;
      private String edtavSistema_custo_Internalname ;
      private String edtavSistema_custo_Jsonclick ;
      private String lblTextblocksistema_esforco_Internalname ;
      private String lblTextblocksistema_esforco_Jsonclick ;
      private String tblTablemergedsistema_esforco_Internalname ;
      private String edtavSistema_esforco_Internalname ;
      private String edtavSistema_esforco_Jsonclick ;
      private String imgTrn_enter_Internalname ;
      private String imgTrn_enter_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode88 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z649Projeto_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n649Projeto_Nome ;
      private bool n671ProjetoDesenvolvimento_SistemaNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String AV16ProjetoDesenvolvimento_SistemaNom ;
      private String A671ProjetoDesenvolvimento_SistemaNom ;
      private String Z671ProjetoDesenvolvimento_SistemaNom ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP3_Sistema_Tipo ;
      private GXCombobox cmbavSistema_tecnica ;
      private IDataStoreProvider pr_default ;
      private String[] T002A4_A649Projeto_Nome ;
      private bool[] T002A4_n649Projeto_Nome ;
      private String[] T002A5_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] T002A5_n671ProjetoDesenvolvimento_SistemaNom ;
      private String[] T002A6_A649Projeto_Nome ;
      private bool[] T002A6_n649Projeto_Nome ;
      private String[] T002A6_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] T002A6_n671ProjetoDesenvolvimento_SistemaNom ;
      private int[] T002A6_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A6_A670ProjetoDesenvolvimento_SistemaCod ;
      private String[] T002A7_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] T002A7_n671ProjetoDesenvolvimento_SistemaNom ;
      private int[] T002A8_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A8_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] T002A3_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A3_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] T002A9_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A9_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] T002A10_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A10_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] T002A2_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A2_A670ProjetoDesenvolvimento_SistemaCod ;
      private String[] T002A13_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] T002A13_n671ProjetoDesenvolvimento_SistemaNom ;
      private int[] T002A14_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002A14_A670ProjetoDesenvolvimento_SistemaCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class projetodesenvolvimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002A4 ;
          prmT002A4 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A6 ;
          prmT002A6 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A5 ;
          prmT002A5 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A7 ;
          prmT002A7 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A8 ;
          prmT002A8 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A3 ;
          prmT002A3 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A9 ;
          prmT002A9 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A10 ;
          prmT002A10 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A2 ;
          prmT002A2 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A11 ;
          prmT002A11 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A12 ;
          prmT002A12 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002A14 ;
          prmT002A14 = new Object[] {
          } ;
          Object[] prmT002A13 ;
          prmT002A13 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002A2", "SELECT [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (UPDLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod AND [ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A2,1,0,true,false )
             ,new CursorDef("T002A3", "SELECT [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod AND [ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A3,1,0,true,false )
             ,new CursorDef("T002A4", "SELECT [Projeto_Nome] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @ProjetoDesenvolvimento_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A4,1,0,true,false )
             ,new CursorDef("T002A5", "SELECT [Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoDesenvolvimento_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A5,1,0,true,false )
             ,new CursorDef("T002A6", "SELECT T2.[Projeto_Nome], T3.[Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom, TM1.[ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, TM1.[ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM (([ProjetoDesenvolvimento] TM1 WITH (NOLOCK) INNER JOIN [Projeto] T2 WITH (NOLOCK) ON T2.[Projeto_Codigo] = TM1.[ProjetoDesenvolvimento_ProjetoCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = TM1.[ProjetoDesenvolvimento_SistemaCod]) WHERE TM1.[ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod and TM1.[ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod ORDER BY TM1.[ProjetoDesenvolvimento_ProjetoCod], TM1.[ProjetoDesenvolvimento_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002A6,100,0,true,false )
             ,new CursorDef("T002A7", "SELECT [Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoDesenvolvimento_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A7,1,0,true,false )
             ,new CursorDef("T002A8", "SELECT [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod AND [ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002A8,1,0,true,false )
             ,new CursorDef("T002A9", "SELECT TOP 1 [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE ( [ProjetoDesenvolvimento_ProjetoCod] > @ProjetoDesenvolvimento_ProjetoCod or [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod and [ProjetoDesenvolvimento_SistemaCod] > @ProjetoDesenvolvimento_SistemaCod) ORDER BY [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002A9,1,0,true,true )
             ,new CursorDef("T002A10", "SELECT TOP 1 [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE ( [ProjetoDesenvolvimento_ProjetoCod] < @ProjetoDesenvolvimento_ProjetoCod or [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod and [ProjetoDesenvolvimento_SistemaCod] < @ProjetoDesenvolvimento_SistemaCod) ORDER BY [ProjetoDesenvolvimento_ProjetoCod] DESC, [ProjetoDesenvolvimento_SistemaCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002A10,1,0,true,true )
             ,new CursorDef("T002A11", "INSERT INTO [ProjetoDesenvolvimento]([ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod]) VALUES(@ProjetoDesenvolvimento_ProjetoCod, @ProjetoDesenvolvimento_SistemaCod)", GxErrorMask.GX_NOMASK,prmT002A11)
             ,new CursorDef("T002A12", "DELETE FROM [ProjetoDesenvolvimento]  WHERE [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod AND [ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod", GxErrorMask.GX_NOMASK,prmT002A12)
             ,new CursorDef("T002A13", "SELECT [Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ProjetoDesenvolvimento_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002A13,1,0,true,false )
             ,new CursorDef("T002A14", "SELECT [ProjetoDesenvolvimento_ProjetoCod] AS ProjetoDesenvolvimento_ProjetoCod, [ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod FROM [ProjetoDesenvolvimento] WITH (NOLOCK) ORDER BY [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002A14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
