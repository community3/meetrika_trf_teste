/*
               File: ContagemResultado_QAWC
        Description: Contagem Resultado_QAWC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:18:44.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultado_qawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultado_qawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultado_qawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_OS_Codigo )
      {
         this.AV53OS_Codigo = aP0_OS_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV53OS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53OS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53OS_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV53OS_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  AV53OS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53OS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53OS_Codigo), 6, 0)));
                  A1603ContagemResultado_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1603ContagemResultado_CntCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1603ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1603ContagemResultado_CntCod), 6, 0)));
                  A1078ContratoGestor_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
                  AV22Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Contrato_Codigo), 6, 0)));
                  A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A1985ContagemResultadoQA_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
                  A1996ContagemResultadoQA_RespostaDe = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1996ContagemResultadoQA_RespostaDe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
                  A484ContagemResultado_StatusDmn = GetNextPar( );
                  n484ContagemResultado_StatusDmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
                  A1984ContagemResultadoQA_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
                  A1986ContagemResultadoQA_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
                  A1990ContagemResultadoQA_UserPesNom = GetNextPar( );
                  n1990ContagemResultadoQA_UserPesNom = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
                  A1994ContagemResultadoQA_ParaPesNom = GetNextPar( );
                  n1994ContagemResultadoQA_ParaPesNom = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
                  A1987ContagemResultadoQA_Texto = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1987ContagemResultadoQA_Texto", A1987ContagemResultadoQA_Texto);
                  AV23Respondida = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Respondida", AV23Respondida);
                  AV44RContagemResultadoQA_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44RContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44RContagemResultadoQA_Codigo), 6, 0)));
                  AV45RContagemResultadoQA_RespostaDe = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45RContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45RContagemResultadoQA_RespostaDe), 6, 0)));
                  AV46RContagemResultadoQA_DataHora = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46RContagemResultadoQA_DataHora", AV46RContagemResultadoQA_DataHora);
                  AV47RContagemResultadoQA_UserPesNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47RContagemResultadoQA_UserPesNom", AV47RContagemResultadoQA_UserPesNom);
                  AV48RContagemResultadoQA_ParaPesNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48RContagemResultadoQA_ParaPesNom", AV48RContagemResultadoQA_ParaPesNom);
                  AV49RContagemResultadoQA_Texto = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49RContagemResultadoQA_Texto", AV49RContagemResultadoQA_Texto);
                  A1992ContagemResultadoQA_ParaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
                  AV51Finalizada = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51Finalizada", AV51Finalizada);
                  AV32EhColega = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32EhColega", AV32EhColega);
                  AV20UserEhGestor = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UserEhGestor", AV20UserEhGestor);
                  AV38ContagemResultadoQA_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
                  A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  AV50ContagemResultadoQA_ParaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultadoQA_ParaCod), 6, 0)));
                  A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
                  A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A456ContagemResultado_Codigo, AV53OS_Codigo, A1603ContagemResultado_CntCod, A1078ContratoGestor_ContratoCod, AV22Contrato_Codigo, A1079ContratoGestor_UsuarioCod, AV6WWPContext, A1985ContagemResultadoQA_OSCod, A1996ContagemResultadoQA_RespostaDe, A484ContagemResultado_StatusDmn, A1984ContagemResultadoQA_Codigo, A1986ContagemResultadoQA_DataHora, A1990ContagemResultadoQA_UserPesNom, A1994ContagemResultadoQA_ParaPesNom, A1987ContagemResultadoQA_Texto, AV23Respondida, AV44RContagemResultadoQA_Codigo, AV45RContagemResultadoQA_RespostaDe, AV46RContagemResultadoQA_DataHora, AV47RContagemResultadoQA_UserPesNom, AV48RContagemResultadoQA_ParaPesNom, AV49RContagemResultadoQA_Texto, A1992ContagemResultadoQA_ParaCod, AV51Finalizada, AV32EhColega, AV20UserEhGestor, AV38ContagemResultadoQA_Codigo, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, AV50ContagemResultadoQA_ParaCod, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAR22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV58Pgmname = "ContagemResultado_QAWC";
               context.Gx_err = 0;
               edtavContagemresultadoqa_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_codigo_Enabled), 5, 0)));
               edtavContagemresultadoqa_datahora_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_datahora_Enabled), 5, 0)));
               edtavContagemresultadoqa_userpesnom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_userpesnom_Enabled), 5, 0)));
               edtavContagemresultadoqa_parapesnom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_parapesnom_Enabled), 5, 0)));
               edtavContagemresultadoqa_texto_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_texto_Enabled), 5, 0)));
               WSR22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado_QAWC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020625184456");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultado_qawc.aspx") + "?" + UrlEncode("" +AV53OS_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV53OS_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV53OS_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53OS_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1603ContagemResultado_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_RESPOSTADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_DATAHORA", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_USERPESNOM", StringUtil.RTrim( A1990ContagemResultadoQA_UserPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_PARAPESNOM", StringUtil.RTrim( A1994ContagemResultadoQA_ParaPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_TEXTO", A1987ContagemResultadoQA_Texto);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vRESPONDIDA", AV23Respondida);
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44RContagemResultadoQA_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_RESPOSTADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45RContagemResultadoQA_RespostaDe), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_DATAHORA", StringUtil.RTrim( AV46RContagemResultadoQA_DataHora));
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_USERPESNOM", StringUtil.RTrim( AV47RContagemResultadoQA_UserPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_PARAPESNOM", StringUtil.RTrim( AV48RContagemResultadoQA_ParaPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"vRCONTAGEMRESULTADOQA_TEXTO", AV49RContagemResultadoQA_Texto);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOQA_PARACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFINALIZADA", StringUtil.RTrim( AV51Finalizada));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vEHCOLEGA", AV32EhColega);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vUSEREHGESTOR", AV20UserEhGestor);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_PARACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50ContagemResultadoQA_ParaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormR22( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultado_qawc.js", "?2020625184460");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultado_QAWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado_QAWC" ;
      }

      protected void WBR20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultado_qawc.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_R22( true) ;
         }
         else
         {
            wb_table1_2_R22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_R22e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTR22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado_QAWC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPR20( ) ;
            }
         }
      }

      protected void WSR22( )
      {
         STARTR22( ) ;
         EVTR22( ) ;
      }

      protected void EVTR22( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DONOVAQA'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11R22 */
                                    E11R22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VRESPONDER.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VRESPONDER.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPR20( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV36Warning = cgiGet( edtavWarning_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavWarning_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning)) ? AV61Warning_GXI : context.convertURL( context.PathToRelativeUrl( AV36Warning))));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoqa_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoqa_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADOQA_CODIGO");
                                 GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV38ContagemResultadoQA_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
                              }
                              else
                              {
                                 AV38ContagemResultadoQA_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadoqa_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
                              }
                              AV40ContagemResultadoQA_DataHora = cgiGet( edtavContagemresultadoqa_datahora_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, AV40ContagemResultadoQA_DataHora);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_DATAHORA"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV40ContagemResultadoQA_DataHora, ""))));
                              AV41ContagemResultadoQA_UserPesNom = StringUtil.Upper( cgiGet( edtavContagemresultadoqa_userpesnom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, AV41ContagemResultadoQA_UserPesNom);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_USERPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!"))));
                              AV42ContagemResultadoQA_ParaPesNom = StringUtil.Upper( cgiGet( edtavContagemresultadoqa_parapesnom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, AV42ContagemResultadoQA_ParaPesNom);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_PARAPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!"))));
                              AV43ContagemResultadoQA_Texto = cgiGet( edtavContagemresultadoqa_texto_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, AV43ContagemResultadoQA_Texto);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_TEXTO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, AV43ContagemResultadoQA_Texto));
                              AV19Responder = cgiGet( edtavResponder_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponder_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder)) ? AV59Responder_GXI : context.convertURL( context.PathToRelativeUrl( AV19Responder))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12R22 */
                                          E12R22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13R22 */
                                          E13R22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14R22 */
                                          E14R22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VRESPONDER.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15R22 */
                                          E15R22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPR20( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContagemresultadoqa_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WER22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormR22( ) ;
            }
         }
      }

      protected void PAR22( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A456ContagemResultado_Codigo ,
                                       int AV53OS_Codigo ,
                                       int A1603ContagemResultado_CntCod ,
                                       int A1078ContratoGestor_ContratoCod ,
                                       int AV22Contrato_Codigo ,
                                       int A1079ContratoGestor_UsuarioCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A1985ContagemResultadoQA_OSCod ,
                                       int A1996ContagemResultadoQA_RespostaDe ,
                                       String A484ContagemResultado_StatusDmn ,
                                       int A1984ContagemResultadoQA_Codigo ,
                                       DateTime A1986ContagemResultadoQA_DataHora ,
                                       String A1990ContagemResultadoQA_UserPesNom ,
                                       String A1994ContagemResultadoQA_ParaPesNom ,
                                       String A1987ContagemResultadoQA_Texto ,
                                       bool AV23Respondida ,
                                       int AV44RContagemResultadoQA_Codigo ,
                                       int AV45RContagemResultadoQA_RespostaDe ,
                                       String AV46RContagemResultadoQA_DataHora ,
                                       String AV47RContagemResultadoQA_UserPesNom ,
                                       String AV48RContagemResultadoQA_ParaPesNom ,
                                       String AV49RContagemResultadoQA_Texto ,
                                       int A1992ContagemResultadoQA_ParaCod ,
                                       String AV51Finalizada ,
                                       bool AV32EhColega ,
                                       bool AV20UserEhGestor ,
                                       int AV38ContagemResultadoQA_Codigo ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int AV50ContagemResultadoQA_ParaCod ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFR22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_DATAHORA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV40ContagemResultadoQA_DataHora, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_DATAHORA", StringUtil.RTrim( AV40ContagemResultadoQA_DataHora));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_USERPESNOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_USERPESNOM", StringUtil.RTrim( AV41ContagemResultadoQA_UserPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_PARAPESNOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_PARAPESNOM", StringUtil.RTrim( AV42ContagemResultadoQA_ParaPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_TEXTO", GetSecureSignedToken( sPrefix, AV43ContagemResultadoQA_Texto));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOQA_TEXTO", AV43ContagemResultadoQA_Texto);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFR22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV58Pgmname = "ContagemResultado_QAWC";
         context.Gx_err = 0;
         edtavContagemresultadoqa_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_codigo_Enabled), 5, 0)));
         edtavContagemresultadoqa_datahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_datahora_Enabled), 5, 0)));
         edtavContagemresultadoqa_userpesnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_userpesnom_Enabled), 5, 0)));
         edtavContagemresultadoqa_parapesnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_parapesnom_Enabled), 5, 0)));
         edtavContagemresultadoqa_texto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_texto_Enabled), 5, 0)));
      }

      protected void RFR22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E14R22 */
         E14R22 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            /* Execute user event: E13R22 */
            E13R22 ();
            wbEnd = 11;
            WBR20( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPR20( )
      {
         /* Before Start, stand alone formulas. */
         AV58Pgmname = "ContagemResultado_QAWC";
         context.Gx_err = 0;
         edtavContagemresultadoqa_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_codigo_Enabled), 5, 0)));
         edtavContagemresultadoqa_datahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_datahora_Enabled), 5, 0)));
         edtavContagemresultadoqa_userpesnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_userpesnom_Enabled), 5, 0)));
         edtavContagemresultadoqa_parapesnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_parapesnom_Enabled), 5, 0)));
         edtavContagemresultadoqa_texto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoqa_texto_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12R22 */
         E12R22 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            wcpOAV53OS_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV53OS_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12R22 */
         E12R22 ();
         if (returnInSub) return;
      }

      protected void E12R22( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if (returnInSub) return;
      }

      private void E13R22( )
      {
         /* Load Routine */
         AV56GXLvl15 = 0;
         /* Using cursor H00R22 */
         pr_default.execute(0, new Object[] {AV53OS_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1988ContagemResultadoQA_UserCod = H00R22_A1988ContagemResultadoQA_UserCod[0];
            A1989ContagemResultadoQA_UserPesCod = H00R22_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = H00R22_n1989ContagemResultadoQA_UserPesCod[0];
            A1993ContagemResultadoQA_ParaPesCod = H00R22_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = H00R22_n1993ContagemResultadoQA_ParaPesCod[0];
            A1996ContagemResultadoQA_RespostaDe = H00R22_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = H00R22_n1996ContagemResultadoQA_RespostaDe[0];
            A1985ContagemResultadoQA_OSCod = H00R22_A1985ContagemResultadoQA_OSCod[0];
            A484ContagemResultado_StatusDmn = H00R22_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00R22_n484ContagemResultado_StatusDmn[0];
            A1984ContagemResultadoQA_Codigo = H00R22_A1984ContagemResultadoQA_Codigo[0];
            A1986ContagemResultadoQA_DataHora = H00R22_A1986ContagemResultadoQA_DataHora[0];
            A1990ContagemResultadoQA_UserPesNom = H00R22_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = H00R22_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = H00R22_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = H00R22_n1994ContagemResultadoQA_ParaPesNom[0];
            A1987ContagemResultadoQA_Texto = H00R22_A1987ContagemResultadoQA_Texto[0];
            A1992ContagemResultadoQA_ParaCod = H00R22_A1992ContagemResultadoQA_ParaCod[0];
            A1989ContagemResultadoQA_UserPesCod = H00R22_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = H00R22_n1989ContagemResultadoQA_UserPesCod[0];
            A1990ContagemResultadoQA_UserPesNom = H00R22_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = H00R22_n1990ContagemResultadoQA_UserPesNom[0];
            A484ContagemResultado_StatusDmn = H00R22_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00R22_n484ContagemResultado_StatusDmn[0];
            A1993ContagemResultadoQA_ParaPesCod = H00R22_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = H00R22_n1993ContagemResultadoQA_ParaPesCod[0];
            A1994ContagemResultadoQA_ParaPesNom = H00R22_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = H00R22_n1994ContagemResultadoQA_ParaPesNom[0];
            AV56GXLvl15 = 1;
            AV52StatusDmn = A484ContagemResultado_StatusDmn;
            AV38ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
            AV39ContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            AV40ContagemResultadoQA_DataHora = context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " ");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, AV40ContagemResultadoQA_DataHora);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_DATAHORA"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV40ContagemResultadoQA_DataHora, ""))));
            AV41ContagemResultadoQA_UserPesNom = A1990ContagemResultadoQA_UserPesNom;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, AV41ContagemResultadoQA_UserPesNom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_USERPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!"))));
            AV42ContagemResultadoQA_ParaPesNom = A1994ContagemResultadoQA_ParaPesNom;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, AV42ContagemResultadoQA_ParaPesNom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_PARAPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!"))));
            AV43ContagemResultadoQA_Texto = A1987ContagemResultadoQA_Texto;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, AV43ContagemResultadoQA_Texto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_TEXTO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, AV43ContagemResultadoQA_Texto));
            edtavWarning_Visible = 0;
            edtavContagemresultadoqa_datahora_Forecolor = GXUtil.RGB( 0, 0, 0);
            edtavContagemresultadoqa_userpesnom_Forecolor = GXUtil.RGB( 0, 0, 0);
            edtavContagemresultadoqa_parapesnom_Forecolor = GXUtil.RGB( 0, 0, 0);
            edtavContagemresultadoqa_texto_Forecolor = GXUtil.RGB( 0, 0, 0);
            edtavContagemresultadoqa_datahora_Fontbold = 1;
            edtavContagemresultadoqa_userpesnom_Fontbold = 1;
            edtavContagemresultadoqa_parapesnom_Fontbold = 1;
            edtavContagemresultadoqa_texto_Fontbold = 1;
            edtavContagemresultadoqa_datahora_Fontitalic = 0;
            edtavContagemresultadoqa_userpesnom_Fontitalic = 0;
            edtavContagemresultadoqa_parapesnom_Fontitalic = 0;
            edtavContagemresultadoqa_texto_Fontitalic = 0;
            edtavContagemresultadoqa_datahora_Fontsize = 12;
            edtavContagemresultadoqa_userpesnom_Fontsize = 11;
            edtavContagemresultadoqa_parapesnom_Fontsize = 11;
            edtavContagemresultadoqa_texto_Fontsize = 10;
            /* Execute user subroutine: 'RESPOSTA' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            if ( AV23Respondida )
            {
               edtavResponder_Visible = 0;
               sendrow_112( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(11, GridRow);
               }
               AV38ContagemResultadoQA_Codigo = AV44RContagemResultadoQA_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
               AV39ContagemResultadoQA_RespostaDe = AV45RContagemResultadoQA_RespostaDe;
               AV40ContagemResultadoQA_DataHora = AV46RContagemResultadoQA_DataHora;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_datahora_Internalname, AV40ContagemResultadoQA_DataHora);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_DATAHORA"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV40ContagemResultadoQA_DataHora, ""))));
               AV41ContagemResultadoQA_UserPesNom = AV47RContagemResultadoQA_UserPesNom;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_userpesnom_Internalname, AV41ContagemResultadoQA_UserPesNom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_USERPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!"))));
               AV42ContagemResultadoQA_ParaPesNom = AV48RContagemResultadoQA_ParaPesNom;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_parapesnom_Internalname, AV42ContagemResultadoQA_ParaPesNom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_PARAPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!"))));
               AV43ContagemResultadoQA_Texto = AV49RContagemResultadoQA_Texto;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultadoqa_texto_Internalname, AV43ContagemResultadoQA_Texto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADOQA_TEXTO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, AV43ContagemResultadoQA_Texto));
               edtavContagemresultadoqa_datahora_Fontbold = 0;
               edtavContagemresultadoqa_userpesnom_Fontbold = 0;
               edtavContagemresultadoqa_parapesnom_Fontbold = 0;
               edtavContagemresultadoqa_texto_Fontbold = 0;
               edtavContagemresultadoqa_datahora_Fontitalic = 1;
               edtavContagemresultadoqa_userpesnom_Fontitalic = 1;
               edtavContagemresultadoqa_parapesnom_Fontitalic = 1;
               edtavContagemresultadoqa_texto_Fontitalic = 1;
               edtavContagemresultadoqa_datahora_Fontsize = 9;
               edtavContagemresultadoqa_userpesnom_Fontsize = 9;
               edtavContagemresultadoqa_parapesnom_Fontsize = 9;
               edtavContagemresultadoqa_texto_Fontsize = 9;
               sendrow_112( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(11, GridRow);
               }
            }
            else
            {
               edtavWarning_Visible = 1;
               AV50ContagemResultadoQA_ParaCod = A1992ContagemResultadoQA_ParaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultadoQA_ParaCod), 6, 0)));
               /* Execute user subroutine: 'EHCOLEGA' */
               S133 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               edtavResponder_Visible = ((0==StringUtil.StringSearch( AV51Finalizada, AV52StatusDmn, 1))&&(H00R22_n1996ContagemResultadoQA_RespostaDe[0]&&((A1992ContagemResultadoQA_ParaCod==AV6WWPContext.gxTpr_Userid)||(AV32EhColega&&AV20UserEhGestor))) ? 1 : 0);
               sendrow_112( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(11, GridRow);
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV56GXLvl15 == 0 )
         {
            /* Using cursor H00R23 */
            pr_default.execute(1, new Object[] {AV53OS_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A456ContagemResultado_Codigo = H00R23_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00R23_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00R23_n484ContagemResultado_StatusDmn[0];
               AV52StatusDmn = A484ContagemResultado_StatusDmn;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         imgNovaqa_Visible = ((0==StringUtil.StringSearch( AV51Finalizada, AV52StatusDmn, 1))&&((AV6WWPContext.gxTpr_Contratada_codigo>0)||(AV6WWPContext.gxTpr_Contratante_codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNovaqa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNovaqa_Visible), 5, 0)));
      }

      protected void E11R22( )
      {
         /* 'DoNovaQA' Routine */
         context.PopUp(formatLink("wp_qa.aspx") + "?" + UrlEncode("" +AV53OS_Codigo) + "," + UrlEncode("" +AV6WWPContext.gxTpr_Userid) + "," + UrlEncode(StringUtil.BoolToStr(AV6WWPContext.gxTpr_Userehcontratante)) + "," + UrlEncode("" +0), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV58Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E14R22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgNovaqa_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNovaqa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNovaqa_Visible), 5, 0)));
         AV19Responder = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponder_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder)) ? AV59Responder_GXI : context.convertURL( context.PathToRelativeUrl( AV19Responder))));
         AV59Responder_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponder_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder)) ? AV59Responder_GXI : context.convertURL( context.PathToRelativeUrl( AV19Responder))));
         edtavResponder_Tooltiptext = "Responder";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponder_Internalname, "Tooltiptext", edtavResponder_Tooltiptext);
         /* Using cursor H00R24 */
         pr_default.execute(2, new Object[] {AV53OS_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00R24_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00R24_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00R24_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = H00R24_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00R24_n1603ContagemResultado_CntCod[0];
            A1603ContagemResultado_CntCod = H00R24_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00R24_n1603ContagemResultado_CntCod[0];
            AV22Contrato_Codigo = A1603ContagemResultado_CntCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Contrato_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         AV36Warning = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavWarning_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning)) ? AV61Warning_GXI : context.convertURL( context.PathToRelativeUrl( AV36Warning))));
         AV61Warning_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavWarning_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning)) ? AV61Warning_GXI : context.convertURL( context.PathToRelativeUrl( AV36Warning))));
         edtavWarning_Tooltiptext = "Pergunta ainda n�o respondida";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavWarning_Internalname, "Tooltiptext", edtavWarning_Tooltiptext);
         AV51Finalizada = "RCHOPLX";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51Finalizada", AV51Finalizada);
         /* Execute user subroutine: 'USUARIOEHGESTOR' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E15R22( )
      {
         /* Responder_Click Routine */
         context.PopUp(formatLink("wp_qa.aspx") + "?" + UrlEncode("" +AV53OS_Codigo) + "," + UrlEncode("" +AV6WWPContext.gxTpr_Userid) + "," + UrlEncode(StringUtil.BoolToStr(AV6WWPContext.gxTpr_Userehcontratante)) + "," + UrlEncode("" +AV38ContagemResultadoQA_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S142( )
      {
         /* 'USUARIOEHGESTOR' Routine */
         AV62GXLvl159 = 0;
         /* Using cursor H00R25 */
         pr_default.execute(3, new Object[] {AV22Contrato_Codigo, AV6WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1079ContratoGestor_UsuarioCod = H00R25_A1079ContratoGestor_UsuarioCod[0];
            A1078ContratoGestor_ContratoCod = H00R25_A1078ContratoGestor_ContratoCod[0];
            AV62GXLvl159 = 1;
            AV20UserEhGestor = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UserEhGestor", AV20UserEhGestor);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         if ( AV62GXLvl159 == 0 )
         {
            AV20UserEhGestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UserEhGestor", AV20UserEhGestor);
         }
      }

      protected void S123( )
      {
         /* 'RESPOSTA' Routine */
         AV63GXLvl172 = 0;
         /* Using cursor H00R26 */
         pr_default.execute(4, new Object[] {AV38ContagemResultadoQA_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1988ContagemResultadoQA_UserCod = H00R26_A1988ContagemResultadoQA_UserCod[0];
            A1989ContagemResultadoQA_UserPesCod = H00R26_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = H00R26_n1989ContagemResultadoQA_UserPesCod[0];
            A1992ContagemResultadoQA_ParaCod = H00R26_A1992ContagemResultadoQA_ParaCod[0];
            A1993ContagemResultadoQA_ParaPesCod = H00R26_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = H00R26_n1993ContagemResultadoQA_ParaPesCod[0];
            A1996ContagemResultadoQA_RespostaDe = H00R26_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = H00R26_n1996ContagemResultadoQA_RespostaDe[0];
            A1984ContagemResultadoQA_Codigo = H00R26_A1984ContagemResultadoQA_Codigo[0];
            A1986ContagemResultadoQA_DataHora = H00R26_A1986ContagemResultadoQA_DataHora[0];
            A1990ContagemResultadoQA_UserPesNom = H00R26_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = H00R26_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = H00R26_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = H00R26_n1994ContagemResultadoQA_ParaPesNom[0];
            A1987ContagemResultadoQA_Texto = H00R26_A1987ContagemResultadoQA_Texto[0];
            A1989ContagemResultadoQA_UserPesCod = H00R26_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = H00R26_n1989ContagemResultadoQA_UserPesCod[0];
            A1990ContagemResultadoQA_UserPesNom = H00R26_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = H00R26_n1990ContagemResultadoQA_UserPesNom[0];
            A1993ContagemResultadoQA_ParaPesCod = H00R26_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = H00R26_n1993ContagemResultadoQA_ParaPesCod[0];
            A1994ContagemResultadoQA_ParaPesNom = H00R26_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = H00R26_n1994ContagemResultadoQA_ParaPesNom[0];
            AV63GXLvl172 = 1;
            AV44RContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44RContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44RContagemResultadoQA_Codigo), 6, 0)));
            AV45RContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45RContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45RContagemResultadoQA_RespostaDe), 6, 0)));
            AV46RContagemResultadoQA_DataHora = "����" + context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " ");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46RContagemResultadoQA_DataHora", AV46RContagemResultadoQA_DataHora);
            AV47RContagemResultadoQA_UserPesNom = A1990ContagemResultadoQA_UserPesNom;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47RContagemResultadoQA_UserPesNom", AV47RContagemResultadoQA_UserPesNom);
            AV48RContagemResultadoQA_ParaPesNom = A1994ContagemResultadoQA_ParaPesNom;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48RContagemResultadoQA_ParaPesNom", AV48RContagemResultadoQA_ParaPesNom);
            AV49RContagemResultadoQA_Texto = A1987ContagemResultadoQA_Texto;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49RContagemResultadoQA_Texto", AV49RContagemResultadoQA_Texto);
            AV23Respondida = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Respondida", AV23Respondida);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV63GXLvl172 == 0 )
         {
            AV23Respondida = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Respondida", AV23Respondida);
         }
      }

      protected void S133( )
      {
         /* 'EHCOLEGA' Routine */
         AV32EhColega = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32EhColega", AV32EhColega);
         if ( AV6WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            /* Using cursor H00R27 */
            pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV50ContagemResultadoQA_ParaCod});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00R27_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = H00R27_A66ContratadaUsuario_ContratadaCod[0];
               AV32EhColega = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32EhColega", AV32EhColega);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         else if ( AV6WWPContext.gxTpr_Contratante_codigo > 0 )
         {
            /* Using cursor H00R28 */
            pr_default.execute(6, new Object[] {AV6WWPContext.gxTpr_Contratante_codigo, AV50ContagemResultadoQA_ParaCod});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A60ContratanteUsuario_UsuarioCod = H00R28_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00R28_A63ContratanteUsuario_ContratanteCod[0];
               AV32EhColega = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32EhColega", AV32EhColega);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
      }

      protected void wb_table1_2_R22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_R22( true) ;
         }
         else
         {
            wb_table2_5_R22( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_R22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgNovaqa_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgNovaqa_Visible, 1, "", "Nova pergunta / resposta", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgNovaqa_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DONOVAQA\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultado_QAWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_R22e( true) ;
         }
         else
         {
            wb_table1_2_R22e( false) ;
         }
      }

      protected void wb_table2_5_R22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_8_R22( true) ;
         }
         else
         {
            wb_table3_8_R22( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_R22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_R22e( true) ;
         }
         else
         {
            wb_table2_5_R22e( false) ;
         }
      }

      protected void wb_table3_8_R22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtable_Internalname, tblGridtable_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado QA_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "De") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Para") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Texto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavResponder_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Responder") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Warning));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavWarning_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV40ContagemResultadoQA_DataHora));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_datahora_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontbold", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_datahora_Fontbold), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontitalic", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_datahora_Fontitalic), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_datahora_Fontsize), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_datahora_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV41ContagemResultadoQA_UserPesNom));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_userpesnom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontbold", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_userpesnom_Fontbold), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontitalic", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_userpesnom_Fontitalic), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_userpesnom_Fontsize), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_userpesnom_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV42ContagemResultadoQA_ParaPesNom));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_parapesnom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontbold", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_parapesnom_Fontbold), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontitalic", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_parapesnom_Fontitalic), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_parapesnom_Fontsize), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_parapesnom_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV43ContagemResultadoQA_Texto);
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_texto_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontbold", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_texto_Fontbold), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontitalic", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_texto_Fontitalic), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_texto_Fontsize), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultadoqa_texto_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Responder));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavResponder_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavResponder_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_R22e( true) ;
         }
         else
         {
            wb_table3_8_R22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV53OS_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53OS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53OS_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAR22( ) ;
         WSR22( ) ;
         WER22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV53OS_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAR22( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultado_qawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAR22( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV53OS_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53OS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53OS_Codigo), 6, 0)));
         }
         wcpOAV53OS_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV53OS_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV53OS_Codigo != wcpOAV53OS_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV53OS_Codigo = AV53OS_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV53OS_Codigo = cgiGet( sPrefix+"AV53OS_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV53OS_Codigo) > 0 )
         {
            AV53OS_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV53OS_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53OS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53OS_Codigo), 6, 0)));
         }
         else
         {
            AV53OS_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV53OS_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAR22( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSR22( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSR22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV53OS_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53OS_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV53OS_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV53OS_Codigo_CTRL", StringUtil.RTrim( sCtrlAV53OS_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WER22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020625184531");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultado_qawc.js", "?2020625184531");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavWarning_Internalname = sPrefix+"vWARNING_"+sGXsfl_11_idx;
         edtavContagemresultadoqa_codigo_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_CODIGO_"+sGXsfl_11_idx;
         edtavContagemresultadoqa_datahora_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_DATAHORA_"+sGXsfl_11_idx;
         edtavContagemresultadoqa_userpesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_USERPESNOM_"+sGXsfl_11_idx;
         edtavContagemresultadoqa_parapesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_PARAPESNOM_"+sGXsfl_11_idx;
         edtavContagemresultadoqa_texto_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_TEXTO_"+sGXsfl_11_idx;
         edtavResponder_Internalname = sPrefix+"vRESPONDER_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavWarning_Internalname = sPrefix+"vWARNING_"+sGXsfl_11_fel_idx;
         edtavContagemresultadoqa_codigo_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_CODIGO_"+sGXsfl_11_fel_idx;
         edtavContagemresultadoqa_datahora_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_DATAHORA_"+sGXsfl_11_fel_idx;
         edtavContagemresultadoqa_userpesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_USERPESNOM_"+sGXsfl_11_fel_idx;
         edtavContagemresultadoqa_parapesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_PARAPESNOM_"+sGXsfl_11_fel_idx;
         edtavContagemresultadoqa_texto_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_TEXTO_"+sGXsfl_11_fel_idx;
         edtavResponder_Internalname = sPrefix+"vRESPONDER_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBR20( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Static Bitmap Variable */
         ClassString = "Image";
         StyleString = "color:#00FFFFFF;";
         AV36Warning_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Warning_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Warning)) ? AV61Warning_GXI : context.PathToRelativeUrl( AV36Warning)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavWarning_Visible,(short)0,(String)"",(String)edtavWarning_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Warning_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultadoqa_codigo_Enabled!=0)&&(edtavContagemresultadoqa_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'"+sPrefix+"',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultadoqa_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ContagemResultadoQA_Codigo), 6, 0, ",", "")),((edtavContagemresultadoqa_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")),TempTags+((edtavContagemresultadoqa_codigo_Enabled!=0)&&(edtavContagemresultadoqa_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultadoqa_codigo_Enabled!=0)&&(edtavContagemresultadoqa_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,13);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultadoqa_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"color:#00FFFFFF;",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultadoqa_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultadoqa_datahora_Enabled!=0)&&(edtavContagemresultadoqa_datahora_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'"+sPrefix+"',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultadoqa_datahora_Internalname,StringUtil.RTrim( AV40ContagemResultadoQA_DataHora),(String)"",TempTags+((edtavContagemresultadoqa_datahora_Enabled!=0)&&(edtavContagemresultadoqa_datahora_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultadoqa_datahora_Enabled!=0)&&(edtavContagemresultadoqa_datahora_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,14);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultadoqa_datahora_Jsonclick,(short)0,(String)"Attribute","font-size:"+StringUtil.Str( (decimal)(edtavContagemresultadoqa_datahora_Fontsize), 3, 0)+"pt;"+((edtavContagemresultadoqa_datahora_Fontbold==1) ? "font-weight:bold;" : "font-weight:normal;")+((edtavContagemresultadoqa_datahora_Fontitalic==1) ? "font-style:italic;" : "font-style:normal;")+"color:"+context.BuildHTMLColor( edtavContagemresultadoqa_datahora_Forecolor)+";"+"font-family:'Arial';",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultadoqa_datahora_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultadoqa_userpesnom_Enabled!=0)&&(edtavContagemresultadoqa_userpesnom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'"+sPrefix+"',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultadoqa_userpesnom_Internalname,StringUtil.RTrim( AV41ContagemResultadoQA_UserPesNom),StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!")),TempTags+((edtavContagemresultadoqa_userpesnom_Enabled!=0)&&(edtavContagemresultadoqa_userpesnom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultadoqa_userpesnom_Enabled!=0)&&(edtavContagemresultadoqa_userpesnom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,15);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultadoqa_userpesnom_Jsonclick,(short)0,(String)"BootstrapAttribute100","font-size:"+StringUtil.Str( (decimal)(edtavContagemresultadoqa_userpesnom_Fontsize), 3, 0)+"pt;"+((edtavContagemresultadoqa_userpesnom_Fontbold==1) ? "font-weight:bold;" : "font-weight:normal;")+((edtavContagemresultadoqa_userpesnom_Fontitalic==1) ? "font-style:italic;" : "font-style:normal;")+"color:"+context.BuildHTMLColor( edtavContagemresultadoqa_userpesnom_Forecolor)+";"+"font-family:'Arial';",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultadoqa_userpesnom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultadoqa_parapesnom_Enabled!=0)&&(edtavContagemresultadoqa_parapesnom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'"+sPrefix+"',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultadoqa_parapesnom_Internalname,StringUtil.RTrim( AV42ContagemResultadoQA_ParaPesNom),StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!")),TempTags+((edtavContagemresultadoqa_parapesnom_Enabled!=0)&&(edtavContagemresultadoqa_parapesnom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultadoqa_parapesnom_Enabled!=0)&&(edtavContagemresultadoqa_parapesnom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,16);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultadoqa_parapesnom_Jsonclick,(short)0,(String)"BootstrapAttribute100","font-size:"+StringUtil.Str( (decimal)(edtavContagemresultadoqa_parapesnom_Fontsize), 3, 0)+"pt;"+((edtavContagemresultadoqa_parapesnom_Fontbold==1) ? "font-weight:bold;" : "font-weight:normal;")+((edtavContagemresultadoqa_parapesnom_Fontitalic==1) ? "font-style:italic;" : "font-style:normal;")+"color:"+context.BuildHTMLColor( edtavContagemresultadoqa_parapesnom_Forecolor)+";"+"font-family:'Arial';",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultadoqa_parapesnom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultadoqa_texto_Enabled!=0)&&(edtavContagemresultadoqa_texto_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'"+sPrefix+"',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultadoqa_texto_Internalname,(String)AV43ContagemResultadoQA_Texto,(String)AV43ContagemResultadoQA_Texto,TempTags+((edtavContagemresultadoqa_texto_Enabled!=0)&&(edtavContagemresultadoqa_texto_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultadoqa_texto_Enabled!=0)&&(edtavContagemresultadoqa_texto_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,17);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultadoqa_texto_Jsonclick,(short)0,(String)"Attribute","font-size:"+StringUtil.Str( (decimal)(edtavContagemresultadoqa_texto_Fontsize), 3, 0)+"pt;"+((edtavContagemresultadoqa_texto_Fontbold==1) ? "font-weight:bold;" : "font-weight:normal;")+((edtavContagemresultadoqa_texto_Fontitalic==1) ? "font-style:italic;" : "font-style:normal;")+"color:"+context.BuildHTMLColor( edtavContagemresultadoqa_texto_Forecolor)+";"+"font-family:'Arial';",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultadoqa_texto_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)11,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavResponder_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavResponder_Enabled!=0)&&(edtavResponder_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'"+sPrefix+"',false,'',11)\"" : " ");
         ClassString = "Image";
         StyleString = "color:#00FFFFFF;";
         AV19Responder_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Responder_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavResponder_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Responder)) ? AV59Responder_GXI : context.PathToRelativeUrl( AV19Responder)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavResponder_Visible,(short)1,(String)"",(String)edtavResponder_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavResponder_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVRESPONDER.CLICK."+sGXsfl_11_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV19Responder_IsBlob,(bool)false});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_CODIGO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(AV38ContagemResultadoQA_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_DATAHORA"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV40ContagemResultadoQA_DataHora, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_USERPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultadoQA_UserPesNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_PARAPESNOM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultadoQA_ParaPesNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADOQA_TEXTO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, AV43ContagemResultadoQA_Texto));
         GridContainer.AddRow(GridRow);
         nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         edtavWarning_Internalname = sPrefix+"vWARNING";
         edtavContagemresultadoqa_codigo_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_CODIGO";
         edtavContagemresultadoqa_datahora_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_DATAHORA";
         edtavContagemresultadoqa_userpesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_USERPESNOM";
         edtavContagemresultadoqa_parapesnom_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_PARAPESNOM";
         edtavContagemresultadoqa_texto_Internalname = sPrefix+"vCONTAGEMRESULTADOQA_TEXTO";
         edtavResponder_Internalname = sPrefix+"vRESPONDER";
         tblGridtable_Internalname = sPrefix+"GRIDTABLE";
         tblUsrtable_Internalname = sPrefix+"USRTABLE";
         imgNovaqa_Internalname = sPrefix+"NOVAQA";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavResponder_Jsonclick = "";
         edtavResponder_Enabled = 1;
         edtavContagemresultadoqa_texto_Jsonclick = "";
         edtavContagemresultadoqa_texto_Visible = -1;
         edtavContagemresultadoqa_parapesnom_Jsonclick = "";
         edtavContagemresultadoqa_parapesnom_Visible = -1;
         edtavContagemresultadoqa_userpesnom_Jsonclick = "";
         edtavContagemresultadoqa_userpesnom_Visible = -1;
         edtavContagemresultadoqa_datahora_Jsonclick = "";
         edtavContagemresultadoqa_datahora_Visible = -1;
         edtavContagemresultadoqa_codigo_Jsonclick = "";
         edtavContagemresultadoqa_codigo_Visible = 0;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavContagemresultadoqa_texto_Enabled = 1;
         edtavContagemresultadoqa_texto_Fontsize = (int)(8.0m);
         edtavContagemresultadoqa_texto_Fontitalic = 0;
         edtavContagemresultadoqa_texto_Fontbold = 0;
         edtavContagemresultadoqa_texto_Forecolor = (int)(0x00FFFFFF);
         edtavContagemresultadoqa_parapesnom_Enabled = 1;
         edtavContagemresultadoqa_parapesnom_Fontsize = (int)(8.0m);
         edtavContagemresultadoqa_parapesnom_Fontitalic = 0;
         edtavContagemresultadoqa_parapesnom_Fontbold = 0;
         edtavContagemresultadoqa_parapesnom_Forecolor = (int)(0x00FFFFFF);
         edtavContagemresultadoqa_userpesnom_Enabled = 1;
         edtavContagemresultadoqa_userpesnom_Fontsize = (int)(8.0m);
         edtavContagemresultadoqa_userpesnom_Fontitalic = 0;
         edtavContagemresultadoqa_userpesnom_Fontbold = 0;
         edtavContagemresultadoqa_userpesnom_Forecolor = (int)(0x00FFFFFF);
         edtavContagemresultadoqa_datahora_Enabled = 1;
         edtavContagemresultadoqa_datahora_Fontsize = (int)(8.0m);
         edtavContagemresultadoqa_datahora_Fontitalic = 0;
         edtavContagemresultadoqa_datahora_Fontbold = 0;
         edtavContagemresultadoqa_datahora_Forecolor = (int)(0x00FFFFFF);
         edtavContagemresultadoqa_codigo_Enabled = 1;
         edtavResponder_Visible = -1;
         edtavWarning_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgNovaqa_Visible = 1;
         edtavWarning_Tooltiptext = "";
         edtavResponder_Tooltiptext = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'imgNovaqa_Visible',ctrl:'NOVAQA',prop:'Visible'},{av:'AV19Responder',fld:'vRESPONDER',pic:'',nv:''},{av:'edtavResponder_Tooltiptext',ctrl:'vRESPONDER',prop:'Tooltiptext'},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Warning',fld:'vWARNING',pic:'',nv:''},{av:'edtavWarning_Tooltiptext',ctrl:'vWARNING',prop:'Tooltiptext'},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false}]}");
         setEventMetadata("'DONOVAQA'","{handler:'E11R22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VRESPONDER.CLICK","{handler:'E15R22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A484ContagemResultado_StatusDmn = "";
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         A1990ContagemResultadoQA_UserPesNom = "";
         A1994ContagemResultadoQA_ParaPesNom = "";
         A1987ContagemResultadoQA_Texto = "";
         AV46RContagemResultadoQA_DataHora = "";
         AV47RContagemResultadoQA_UserPesNom = "";
         AV48RContagemResultadoQA_ParaPesNom = "";
         AV49RContagemResultadoQA_Texto = "";
         AV51Finalizada = "";
         GXKey = "";
         AV58Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV36Warning = "";
         AV61Warning_GXI = "";
         AV40ContagemResultadoQA_DataHora = "";
         AV41ContagemResultadoQA_UserPesNom = "";
         AV42ContagemResultadoQA_ParaPesNom = "";
         AV43ContagemResultadoQA_Texto = "";
         AV19Responder = "";
         AV59Responder_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00R22_A1988ContagemResultadoQA_UserCod = new int[1] ;
         H00R22_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         H00R22_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         H00R22_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         H00R22_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         H00R22_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         H00R22_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         H00R22_A1985ContagemResultadoQA_OSCod = new int[1] ;
         H00R22_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00R22_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00R22_A1984ContagemResultadoQA_Codigo = new int[1] ;
         H00R22_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00R22_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         H00R22_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         H00R22_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         H00R22_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         H00R22_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         H00R22_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         AV52StatusDmn = "";
         GridRow = new GXWebRow();
         H00R23_A456ContagemResultado_Codigo = new int[1] ;
         H00R23_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00R23_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV10Session = context.GetSession();
         H00R24_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00R24_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00R24_A456ContagemResultado_Codigo = new int[1] ;
         H00R24_A1603ContagemResultado_CntCod = new int[1] ;
         H00R24_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00R25_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00R25_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00R26_A1988ContagemResultadoQA_UserCod = new int[1] ;
         H00R26_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         H00R26_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         H00R26_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         H00R26_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         H00R26_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         H00R26_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         H00R26_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         H00R26_A1984ContagemResultadoQA_Codigo = new int[1] ;
         H00R26_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00R26_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         H00R26_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         H00R26_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         H00R26_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         H00R26_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         H00R27_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00R27_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00R28_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00R28_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgNovaqa_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV53OS_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultado_qawc__default(),
            new Object[][] {
                new Object[] {
               H00R22_A1988ContagemResultadoQA_UserCod, H00R22_A1989ContagemResultadoQA_UserPesCod, H00R22_n1989ContagemResultadoQA_UserPesCod, H00R22_A1993ContagemResultadoQA_ParaPesCod, H00R22_n1993ContagemResultadoQA_ParaPesCod, H00R22_A1996ContagemResultadoQA_RespostaDe, H00R22_n1996ContagemResultadoQA_RespostaDe, H00R22_A1985ContagemResultadoQA_OSCod, H00R22_A484ContagemResultado_StatusDmn, H00R22_n484ContagemResultado_StatusDmn,
               H00R22_A1984ContagemResultadoQA_Codigo, H00R22_A1986ContagemResultadoQA_DataHora, H00R22_A1990ContagemResultadoQA_UserPesNom, H00R22_n1990ContagemResultadoQA_UserPesNom, H00R22_A1994ContagemResultadoQA_ParaPesNom, H00R22_n1994ContagemResultadoQA_ParaPesNom, H00R22_A1987ContagemResultadoQA_Texto, H00R22_A1992ContagemResultadoQA_ParaCod
               }
               , new Object[] {
               H00R23_A456ContagemResultado_Codigo, H00R23_A484ContagemResultado_StatusDmn, H00R23_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               H00R24_A1553ContagemResultado_CntSrvCod, H00R24_n1553ContagemResultado_CntSrvCod, H00R24_A456ContagemResultado_Codigo, H00R24_A1603ContagemResultado_CntCod, H00R24_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               H00R25_A1079ContratoGestor_UsuarioCod, H00R25_A1078ContratoGestor_ContratoCod
               }
               , new Object[] {
               H00R26_A1988ContagemResultadoQA_UserCod, H00R26_A1989ContagemResultadoQA_UserPesCod, H00R26_n1989ContagemResultadoQA_UserPesCod, H00R26_A1992ContagemResultadoQA_ParaCod, H00R26_A1993ContagemResultadoQA_ParaPesCod, H00R26_n1993ContagemResultadoQA_ParaPesCod, H00R26_A1996ContagemResultadoQA_RespostaDe, H00R26_n1996ContagemResultadoQA_RespostaDe, H00R26_A1984ContagemResultadoQA_Codigo, H00R26_A1986ContagemResultadoQA_DataHora,
               H00R26_A1990ContagemResultadoQA_UserPesNom, H00R26_n1990ContagemResultadoQA_UserPesNom, H00R26_A1994ContagemResultadoQA_ParaPesNom, H00R26_n1994ContagemResultadoQA_ParaPesNom, H00R26_A1987ContagemResultadoQA_Texto
               }
               , new Object[] {
               H00R27_A69ContratadaUsuario_UsuarioCod, H00R27_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00R28_A60ContratanteUsuario_UsuarioCod, H00R28_A63ContratanteUsuario_ContratanteCod
               }
            }
         );
         AV58Pgmname = "ContagemResultado_QAWC";
         /* GeneXus formulas. */
         AV58Pgmname = "ContagemResultado_QAWC";
         context.Gx_err = 0;
         edtavContagemresultadoqa_codigo_Enabled = 0;
         edtavContagemresultadoqa_datahora_Enabled = 0;
         edtavContagemresultadoqa_userpesnom_Enabled = 0;
         edtavContagemresultadoqa_parapesnom_Enabled = 0;
         edtavContagemresultadoqa_texto_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV56GXLvl15 ;
      private short edtavContagemresultadoqa_datahora_Fontbold ;
      private short edtavContagemresultadoqa_userpesnom_Fontbold ;
      private short edtavContagemresultadoqa_parapesnom_Fontbold ;
      private short edtavContagemresultadoqa_texto_Fontbold ;
      private short edtavContagemresultadoqa_datahora_Fontitalic ;
      private short edtavContagemresultadoqa_userpesnom_Fontitalic ;
      private short edtavContagemresultadoqa_parapesnom_Fontitalic ;
      private short edtavContagemresultadoqa_texto_Fontitalic ;
      private short GRID_nEOF ;
      private short AV62GXLvl159 ;
      private short AV63GXLvl172 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV53OS_Codigo ;
      private int wcpOAV53OS_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A1603ContagemResultado_CntCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int AV22Contrato_Codigo ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1985ContagemResultadoQA_OSCod ;
      private int A1996ContagemResultadoQA_RespostaDe ;
      private int A1984ContagemResultadoQA_Codigo ;
      private int AV44RContagemResultadoQA_Codigo ;
      private int AV45RContagemResultadoQA_RespostaDe ;
      private int A1992ContagemResultadoQA_ParaCod ;
      private int AV38ContagemResultadoQA_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV50ContagemResultadoQA_ParaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int edtavContagemresultadoqa_codigo_Enabled ;
      private int edtavContagemresultadoqa_datahora_Enabled ;
      private int edtavContagemresultadoqa_userpesnom_Enabled ;
      private int edtavContagemresultadoqa_parapesnom_Enabled ;
      private int edtavContagemresultadoqa_texto_Enabled ;
      private int subGrid_Islastpage ;
      private int A1988ContagemResultadoQA_UserCod ;
      private int A1989ContagemResultadoQA_UserPesCod ;
      private int A1993ContagemResultadoQA_ParaPesCod ;
      private int AV39ContagemResultadoQA_RespostaDe ;
      private int edtavWarning_Visible ;
      private int edtavContagemresultadoqa_datahora_Forecolor ;
      private int edtavContagemresultadoqa_userpesnom_Forecolor ;
      private int edtavContagemresultadoqa_parapesnom_Forecolor ;
      private int edtavContagemresultadoqa_texto_Forecolor ;
      private int edtavContagemresultadoqa_datahora_Fontsize ;
      private int edtavContagemresultadoqa_userpesnom_Fontsize ;
      private int edtavContagemresultadoqa_parapesnom_Fontsize ;
      private int edtavContagemresultadoqa_texto_Fontsize ;
      private int edtavResponder_Visible ;
      private int imgNovaqa_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavContagemresultadoqa_codigo_Visible ;
      private int edtavContagemresultadoqa_datahora_Visible ;
      private int edtavContagemresultadoqa_userpesnom_Visible ;
      private int edtavContagemresultadoqa_parapesnom_Visible ;
      private int edtavContagemresultadoqa_texto_Visible ;
      private int edtavResponder_Enabled ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1990ContagemResultadoQA_UserPesNom ;
      private String A1994ContagemResultadoQA_ParaPesNom ;
      private String AV46RContagemResultadoQA_DataHora ;
      private String AV47RContagemResultadoQA_UserPesNom ;
      private String AV48RContagemResultadoQA_ParaPesNom ;
      private String AV51Finalizada ;
      private String edtavContagemresultadoqa_codigo_Internalname ;
      private String GXKey ;
      private String AV58Pgmname ;
      private String edtavContagemresultadoqa_datahora_Internalname ;
      private String edtavContagemresultadoqa_userpesnom_Internalname ;
      private String edtavContagemresultadoqa_parapesnom_Internalname ;
      private String edtavContagemresultadoqa_texto_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavWarning_Internalname ;
      private String AV40ContagemResultadoQA_DataHora ;
      private String AV41ContagemResultadoQA_UserPesNom ;
      private String AV42ContagemResultadoQA_ParaPesNom ;
      private String edtavResponder_Internalname ;
      private String scmdbuf ;
      private String AV52StatusDmn ;
      private String imgNovaqa_Internalname ;
      private String edtavResponder_Tooltiptext ;
      private String edtavWarning_Tooltiptext ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String imgNovaqa_Jsonclick ;
      private String tblUsrtable_Internalname ;
      private String tblGridtable_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV53OS_Codigo ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultadoqa_codigo_Jsonclick ;
      private String edtavContagemresultadoqa_datahora_Jsonclick ;
      private String edtavContagemresultadoqa_userpesnom_Jsonclick ;
      private String edtavContagemresultadoqa_parapesnom_Jsonclick ;
      private String edtavContagemresultadoqa_texto_Jsonclick ;
      private String edtavResponder_Jsonclick ;
      private DateTime A1986ContagemResultadoQA_DataHora ;
      private bool entryPointCalled ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1996ContagemResultadoQA_RespostaDe ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1990ContagemResultadoQA_UserPesNom ;
      private bool n1994ContagemResultadoQA_ParaPesNom ;
      private bool AV23Respondida ;
      private bool AV32EhColega ;
      private bool AV20UserEhGestor ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1989ContagemResultadoQA_UserPesCod ;
      private bool n1993ContagemResultadoQA_ParaPesCod ;
      private bool gx_refresh_fired ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool AV36Warning_IsBlob ;
      private bool AV19Responder_IsBlob ;
      private String A1987ContagemResultadoQA_Texto ;
      private String AV49RContagemResultadoQA_Texto ;
      private String AV43ContagemResultadoQA_Texto ;
      private String AV61Warning_GXI ;
      private String AV59Responder_GXI ;
      private String AV36Warning ;
      private String AV19Responder ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00R22_A1988ContagemResultadoQA_UserCod ;
      private int[] H00R22_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] H00R22_n1989ContagemResultadoQA_UserPesCod ;
      private int[] H00R22_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] H00R22_n1993ContagemResultadoQA_ParaPesCod ;
      private int[] H00R22_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] H00R22_n1996ContagemResultadoQA_RespostaDe ;
      private int[] H00R22_A1985ContagemResultadoQA_OSCod ;
      private String[] H00R22_A484ContagemResultado_StatusDmn ;
      private bool[] H00R22_n484ContagemResultado_StatusDmn ;
      private int[] H00R22_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] H00R22_A1986ContagemResultadoQA_DataHora ;
      private String[] H00R22_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] H00R22_n1990ContagemResultadoQA_UserPesNom ;
      private String[] H00R22_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] H00R22_n1994ContagemResultadoQA_ParaPesNom ;
      private String[] H00R22_A1987ContagemResultadoQA_Texto ;
      private int[] H00R22_A1992ContagemResultadoQA_ParaCod ;
      private int[] H00R23_A456ContagemResultado_Codigo ;
      private String[] H00R23_A484ContagemResultado_StatusDmn ;
      private bool[] H00R23_n484ContagemResultado_StatusDmn ;
      private int[] H00R24_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00R24_n1553ContagemResultado_CntSrvCod ;
      private int[] H00R24_A456ContagemResultado_Codigo ;
      private int[] H00R24_A1603ContagemResultado_CntCod ;
      private bool[] H00R24_n1603ContagemResultado_CntCod ;
      private int[] H00R25_A1079ContratoGestor_UsuarioCod ;
      private int[] H00R25_A1078ContratoGestor_ContratoCod ;
      private int[] H00R26_A1988ContagemResultadoQA_UserCod ;
      private int[] H00R26_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] H00R26_n1989ContagemResultadoQA_UserPesCod ;
      private int[] H00R26_A1992ContagemResultadoQA_ParaCod ;
      private int[] H00R26_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] H00R26_n1993ContagemResultadoQA_ParaPesCod ;
      private int[] H00R26_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] H00R26_n1996ContagemResultadoQA_RespostaDe ;
      private int[] H00R26_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] H00R26_A1986ContagemResultadoQA_DataHora ;
      private String[] H00R26_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] H00R26_n1990ContagemResultadoQA_UserPesNom ;
      private String[] H00R26_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] H00R26_n1994ContagemResultadoQA_ParaPesNom ;
      private String[] H00R26_A1987ContagemResultadoQA_Texto ;
      private int[] H00R27_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00R27_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00R28_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00R28_A63ContratanteUsuario_ContratanteCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultado_qawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00R22 ;
          prmH00R22 = new Object[] {
          new Object[] {"@AV53OS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R23 ;
          prmH00R23 = new Object[] {
          new Object[] {"@AV53OS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R24 ;
          prmH00R24 = new Object[] {
          new Object[] {"@AV53OS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R25 ;
          prmH00R25 = new Object[] {
          new Object[] {"@AV22Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00R26 ;
          prmH00R26 = new Object[] {
          new Object[] {"@AV38ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R27 ;
          prmH00R27 = new Object[] {
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R28 ;
          prmH00R28 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratante_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00R22", "SELECT T1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, T2.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T5.[Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod, T1.[ContagemResultadoQA_RespostaDe], T1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, T4.[ContagemResultado_StatusDmn], T1.[ContagemResultadoQA_Codigo], T1.[ContagemResultadoQA_DataHora], T3.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T6.[Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom, T1.[ContagemResultadoQA_Texto], T1.[ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod FROM ((((([ContagemResultadoQA] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultadoQA_OSCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[ContagemResultadoQA_ParaCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE (T1.[ContagemResultadoQA_OSCod] = @AV53OS_Codigo) AND (T1.[ContagemResultadoQA_RespostaDe] IS NULL) ORDER BY T1.[ContagemResultadoQA_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R22,100,0,true,false )
             ,new CursorDef("H00R23", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV53OS_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R23,1,0,false,true )
             ,new CursorDef("H00R24", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV53OS_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R24,1,0,false,true )
             ,new CursorDef("H00R25", "SELECT TOP 1 [ContratoGestor_UsuarioCod], [ContratoGestor_ContratoCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV22Contrato_Codigo and [ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R25,1,0,false,true )
             ,new CursorDef("H00R26", "SELECT TOP 1 T1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, T2.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T1.[ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, T4.[Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod, T1.[ContagemResultadoQA_RespostaDe], T1.[ContagemResultadoQA_Codigo], T1.[ContagemResultadoQA_DataHora], T3.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T5.[Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom, T1.[ContagemResultadoQA_Texto] FROM (((([ContagemResultadoQA] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultadoQA_ParaCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoQA_RespostaDe] = @AV38ContagemResultadoQA_Codigo ORDER BY T1.[ContagemResultadoQA_RespostaDe] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R26,1,0,false,true )
             ,new CursorDef("H00R27", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV6WWPCo_1Contratada_codigo and [ContratadaUsuario_UsuarioCod] = @AV50ContagemResultadoQA_ParaCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R27,1,0,false,true )
             ,new CursorDef("H00R28", "SELECT TOP 1 [ContratanteUsuario_UsuarioCod], [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV6WWPCo_2Contratante_codigo and [ContratanteUsuario_UsuarioCod] = @AV50ContagemResultadoQA_ParaCod ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R28,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(10) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
