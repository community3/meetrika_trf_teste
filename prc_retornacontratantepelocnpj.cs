/*
               File: PRC_RetornaContratantePeloCNPJ
        Description: Retorna Contratante pelo CNPJ
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:25.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacontratantepelocnpj : GXProcedure
   {
      public prc_retornacontratantepelocnpj( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacontratantepelocnpj( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Contratante_CNPJ ,
                           out String aP1_Contratante_Email ,
                           out String aP2_Contratante_Fax ,
                           out String aP3_Contratante_IE ,
                           out String aP4_Contratante_NomeFantasia ,
                           out String aP5_Contratante_Ramal ,
                           out String aP6_Contratante_RazaoSocial ,
                           out String aP7_Contratante_Telefone ,
                           out String aP8_Contratante_WebSite ,
                           ref int aP9_Municipio_Codigo ,
                           ref String aP10_Estado_UF ,
                           out int aP11_Contratante_Codigo )
      {
         this.AV8Contratante_CNPJ = aP0_Contratante_CNPJ;
         this.AV10Contratante_Email = "" ;
         this.AV12Contratante_Fax = "" ;
         this.AV13Contratante_IE = "" ;
         this.AV14Contratante_NomeFantasia = "" ;
         this.AV15Contratante_Ramal = "" ;
         this.AV16Contratante_RazaoSocial = "" ;
         this.AV17Contratante_Telefone = "" ;
         this.AV11Contratante_WebSite = "" ;
         this.AV18Municipio_Codigo = aP9_Municipio_Codigo;
         this.AV19Estado_UF = aP10_Estado_UF;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_Contratante_Email=this.AV10Contratante_Email;
         aP2_Contratante_Fax=this.AV12Contratante_Fax;
         aP3_Contratante_IE=this.AV13Contratante_IE;
         aP4_Contratante_NomeFantasia=this.AV14Contratante_NomeFantasia;
         aP5_Contratante_Ramal=this.AV15Contratante_Ramal;
         aP6_Contratante_RazaoSocial=this.AV16Contratante_RazaoSocial;
         aP7_Contratante_Telefone=this.AV17Contratante_Telefone;
         aP8_Contratante_WebSite=this.AV11Contratante_WebSite;
         aP9_Municipio_Codigo=this.AV18Municipio_Codigo;
         aP10_Estado_UF=this.AV19Estado_UF;
         aP11_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      public int executeUdp( String aP0_Contratante_CNPJ ,
                             out String aP1_Contratante_Email ,
                             out String aP2_Contratante_Fax ,
                             out String aP3_Contratante_IE ,
                             out String aP4_Contratante_NomeFantasia ,
                             out String aP5_Contratante_Ramal ,
                             out String aP6_Contratante_RazaoSocial ,
                             out String aP7_Contratante_Telefone ,
                             out String aP8_Contratante_WebSite ,
                             ref int aP9_Municipio_Codigo ,
                             ref String aP10_Estado_UF )
      {
         this.AV8Contratante_CNPJ = aP0_Contratante_CNPJ;
         this.AV10Contratante_Email = "" ;
         this.AV12Contratante_Fax = "" ;
         this.AV13Contratante_IE = "" ;
         this.AV14Contratante_NomeFantasia = "" ;
         this.AV15Contratante_Ramal = "" ;
         this.AV16Contratante_RazaoSocial = "" ;
         this.AV17Contratante_Telefone = "" ;
         this.AV11Contratante_WebSite = "" ;
         this.AV18Municipio_Codigo = aP9_Municipio_Codigo;
         this.AV19Estado_UF = aP10_Estado_UF;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_Contratante_Email=this.AV10Contratante_Email;
         aP2_Contratante_Fax=this.AV12Contratante_Fax;
         aP3_Contratante_IE=this.AV13Contratante_IE;
         aP4_Contratante_NomeFantasia=this.AV14Contratante_NomeFantasia;
         aP5_Contratante_Ramal=this.AV15Contratante_Ramal;
         aP6_Contratante_RazaoSocial=this.AV16Contratante_RazaoSocial;
         aP7_Contratante_Telefone=this.AV17Contratante_Telefone;
         aP8_Contratante_WebSite=this.AV11Contratante_WebSite;
         aP9_Municipio_Codigo=this.AV18Municipio_Codigo;
         aP10_Estado_UF=this.AV19Estado_UF;
         aP11_Contratante_Codigo=this.AV9Contratante_Codigo;
         return AV9Contratante_Codigo ;
      }

      public void executeSubmit( String aP0_Contratante_CNPJ ,
                                 out String aP1_Contratante_Email ,
                                 out String aP2_Contratante_Fax ,
                                 out String aP3_Contratante_IE ,
                                 out String aP4_Contratante_NomeFantasia ,
                                 out String aP5_Contratante_Ramal ,
                                 out String aP6_Contratante_RazaoSocial ,
                                 out String aP7_Contratante_Telefone ,
                                 out String aP8_Contratante_WebSite ,
                                 ref int aP9_Municipio_Codigo ,
                                 ref String aP10_Estado_UF ,
                                 out int aP11_Contratante_Codigo )
      {
         prc_retornacontratantepelocnpj objprc_retornacontratantepelocnpj;
         objprc_retornacontratantepelocnpj = new prc_retornacontratantepelocnpj();
         objprc_retornacontratantepelocnpj.AV8Contratante_CNPJ = aP0_Contratante_CNPJ;
         objprc_retornacontratantepelocnpj.AV10Contratante_Email = "" ;
         objprc_retornacontratantepelocnpj.AV12Contratante_Fax = "" ;
         objprc_retornacontratantepelocnpj.AV13Contratante_IE = "" ;
         objprc_retornacontratantepelocnpj.AV14Contratante_NomeFantasia = "" ;
         objprc_retornacontratantepelocnpj.AV15Contratante_Ramal = "" ;
         objprc_retornacontratantepelocnpj.AV16Contratante_RazaoSocial = "" ;
         objprc_retornacontratantepelocnpj.AV17Contratante_Telefone = "" ;
         objprc_retornacontratantepelocnpj.AV11Contratante_WebSite = "" ;
         objprc_retornacontratantepelocnpj.AV18Municipio_Codigo = aP9_Municipio_Codigo;
         objprc_retornacontratantepelocnpj.AV19Estado_UF = aP10_Estado_UF;
         objprc_retornacontratantepelocnpj.AV9Contratante_Codigo = 0 ;
         objprc_retornacontratantepelocnpj.context.SetSubmitInitialConfig(context);
         objprc_retornacontratantepelocnpj.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacontratantepelocnpj);
         aP1_Contratante_Email=this.AV10Contratante_Email;
         aP2_Contratante_Fax=this.AV12Contratante_Fax;
         aP3_Contratante_IE=this.AV13Contratante_IE;
         aP4_Contratante_NomeFantasia=this.AV14Contratante_NomeFantasia;
         aP5_Contratante_Ramal=this.AV15Contratante_Ramal;
         aP6_Contratante_RazaoSocial=this.AV16Contratante_RazaoSocial;
         aP7_Contratante_Telefone=this.AV17Contratante_Telefone;
         aP8_Contratante_WebSite=this.AV11Contratante_WebSite;
         aP9_Municipio_Codigo=this.AV18Municipio_Codigo;
         aP10_Estado_UF=this.AV19Estado_UF;
         aP11_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacontratantepelocnpj)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Contratante_Codigo = 0;
         AV16Contratante_RazaoSocial = "";
         AV14Contratante_NomeFantasia = "";
         AV13Contratante_IE = "";
         AV17Contratante_Telefone = "";
         AV15Contratante_Ramal = "";
         AV12Contratante_Fax = "";
         AV10Contratante_Email = "";
         AV11Contratante_WebSite = "";
         AV18Municipio_Codigo = 0;
         AV19Estado_UF = "";
         /* Using cursor P001P2 */
         pr_default.execute(0, new Object[] {AV8Contratante_CNPJ});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A34Pessoa_Codigo = P001P2_A34Pessoa_Codigo[0];
            A36Pessoa_TipoPessoa = P001P2_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = P001P2_A37Pessoa_Docto[0];
            A35Pessoa_Nome = P001P2_A35Pessoa_Nome[0];
            AV16Contratante_RazaoSocial = A35Pessoa_Nome;
            /* Using cursor P001P3 */
            pr_default.execute(1, new Object[] {A34Pessoa_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A335Contratante_PessoaCod = P001P3_A335Contratante_PessoaCod[0];
               A29Contratante_Codigo = P001P3_A29Contratante_Codigo[0];
               A9Contratante_RazaoSocial = P001P3_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P001P3_n9Contratante_RazaoSocial[0];
               A10Contratante_NomeFantasia = P001P3_A10Contratante_NomeFantasia[0];
               A11Contratante_IE = P001P3_A11Contratante_IE[0];
               A31Contratante_Telefone = P001P3_A31Contratante_Telefone[0];
               A32Contratante_Ramal = P001P3_A32Contratante_Ramal[0];
               n32Contratante_Ramal = P001P3_n32Contratante_Ramal[0];
               A33Contratante_Fax = P001P3_A33Contratante_Fax[0];
               n33Contratante_Fax = P001P3_n33Contratante_Fax[0];
               A14Contratante_Email = P001P3_A14Contratante_Email[0];
               n14Contratante_Email = P001P3_n14Contratante_Email[0];
               A13Contratante_WebSite = P001P3_A13Contratante_WebSite[0];
               n13Contratante_WebSite = P001P3_n13Contratante_WebSite[0];
               A25Municipio_Codigo = P001P3_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P001P3_n25Municipio_Codigo[0];
               A23Estado_UF = P001P3_A23Estado_UF[0];
               A9Contratante_RazaoSocial = P001P3_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P001P3_n9Contratante_RazaoSocial[0];
               A23Estado_UF = P001P3_A23Estado_UF[0];
               AV9Contratante_Codigo = A29Contratante_Codigo;
               AV16Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               AV14Contratante_NomeFantasia = A10Contratante_NomeFantasia;
               AV13Contratante_IE = A11Contratante_IE;
               AV17Contratante_Telefone = A31Contratante_Telefone;
               AV15Contratante_Ramal = A32Contratante_Ramal;
               AV12Contratante_Fax = A33Contratante_Fax;
               AV10Contratante_Email = A14Contratante_Email;
               AV11Contratante_WebSite = A13Contratante_WebSite;
               AV18Municipio_Codigo = A25Municipio_Codigo;
               AV19Estado_UF = A23Estado_UF;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001P2_A34Pessoa_Codigo = new int[1] ;
         P001P2_A36Pessoa_TipoPessoa = new String[] {""} ;
         P001P2_A37Pessoa_Docto = new String[] {""} ;
         P001P2_A35Pessoa_Nome = new String[] {""} ;
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         A35Pessoa_Nome = "";
         P001P3_A335Contratante_PessoaCod = new int[1] ;
         P001P3_A29Contratante_Codigo = new int[1] ;
         P001P3_A9Contratante_RazaoSocial = new String[] {""} ;
         P001P3_n9Contratante_RazaoSocial = new bool[] {false} ;
         P001P3_A10Contratante_NomeFantasia = new String[] {""} ;
         P001P3_A11Contratante_IE = new String[] {""} ;
         P001P3_A31Contratante_Telefone = new String[] {""} ;
         P001P3_A32Contratante_Ramal = new String[] {""} ;
         P001P3_n32Contratante_Ramal = new bool[] {false} ;
         P001P3_A33Contratante_Fax = new String[] {""} ;
         P001P3_n33Contratante_Fax = new bool[] {false} ;
         P001P3_A14Contratante_Email = new String[] {""} ;
         P001P3_n14Contratante_Email = new bool[] {false} ;
         P001P3_A13Contratante_WebSite = new String[] {""} ;
         P001P3_n13Contratante_WebSite = new bool[] {false} ;
         P001P3_A25Municipio_Codigo = new int[1] ;
         P001P3_n25Municipio_Codigo = new bool[] {false} ;
         P001P3_A23Estado_UF = new String[] {""} ;
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A11Contratante_IE = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A33Contratante_Fax = "";
         A14Contratante_Email = "";
         A13Contratante_WebSite = "";
         A23Estado_UF = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornacontratantepelocnpj__default(),
            new Object[][] {
                new Object[] {
               P001P2_A34Pessoa_Codigo, P001P2_A36Pessoa_TipoPessoa, P001P2_A37Pessoa_Docto, P001P2_A35Pessoa_Nome
               }
               , new Object[] {
               P001P3_A335Contratante_PessoaCod, P001P3_A29Contratante_Codigo, P001P3_A9Contratante_RazaoSocial, P001P3_n9Contratante_RazaoSocial, P001P3_A10Contratante_NomeFantasia, P001P3_A11Contratante_IE, P001P3_A31Contratante_Telefone, P001P3_A32Contratante_Ramal, P001P3_n32Contratante_Ramal, P001P3_A33Contratante_Fax,
               P001P3_n33Contratante_Fax, P001P3_A14Contratante_Email, P001P3_n14Contratante_Email, P001P3_A13Contratante_WebSite, P001P3_n13Contratante_WebSite, P001P3_A25Municipio_Codigo, P001P3_n25Municipio_Codigo, P001P3_A23Estado_UF
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV18Municipio_Codigo ;
      private int AV9Contratante_Codigo ;
      private int A34Pessoa_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private String AV19Estado_UF ;
      private String AV16Contratante_RazaoSocial ;
      private String AV14Contratante_NomeFantasia ;
      private String AV13Contratante_IE ;
      private String AV17Contratante_Telefone ;
      private String AV15Contratante_Ramal ;
      private String AV12Contratante_Fax ;
      private String scmdbuf ;
      private String A36Pessoa_TipoPessoa ;
      private String A35Pessoa_Nome ;
      private String A9Contratante_RazaoSocial ;
      private String A10Contratante_NomeFantasia ;
      private String A11Contratante_IE ;
      private String A31Contratante_Telefone ;
      private String A32Contratante_Ramal ;
      private String A33Contratante_Fax ;
      private String A23Estado_UF ;
      private bool n9Contratante_RazaoSocial ;
      private bool n32Contratante_Ramal ;
      private bool n33Contratante_Fax ;
      private bool n14Contratante_Email ;
      private bool n13Contratante_WebSite ;
      private bool n25Municipio_Codigo ;
      private String AV8Contratante_CNPJ ;
      private String AV10Contratante_Email ;
      private String AV11Contratante_WebSite ;
      private String A37Pessoa_Docto ;
      private String A14Contratante_Email ;
      private String A13Contratante_WebSite ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP9_Municipio_Codigo ;
      private String aP10_Estado_UF ;
      private IDataStoreProvider pr_default ;
      private int[] P001P2_A34Pessoa_Codigo ;
      private String[] P001P2_A36Pessoa_TipoPessoa ;
      private String[] P001P2_A37Pessoa_Docto ;
      private String[] P001P2_A35Pessoa_Nome ;
      private int[] P001P3_A335Contratante_PessoaCod ;
      private int[] P001P3_A29Contratante_Codigo ;
      private String[] P001P3_A9Contratante_RazaoSocial ;
      private bool[] P001P3_n9Contratante_RazaoSocial ;
      private String[] P001P3_A10Contratante_NomeFantasia ;
      private String[] P001P3_A11Contratante_IE ;
      private String[] P001P3_A31Contratante_Telefone ;
      private String[] P001P3_A32Contratante_Ramal ;
      private bool[] P001P3_n32Contratante_Ramal ;
      private String[] P001P3_A33Contratante_Fax ;
      private bool[] P001P3_n33Contratante_Fax ;
      private String[] P001P3_A14Contratante_Email ;
      private bool[] P001P3_n14Contratante_Email ;
      private String[] P001P3_A13Contratante_WebSite ;
      private bool[] P001P3_n13Contratante_WebSite ;
      private int[] P001P3_A25Municipio_Codigo ;
      private bool[] P001P3_n25Municipio_Codigo ;
      private String[] P001P3_A23Estado_UF ;
      private String aP1_Contratante_Email ;
      private String aP2_Contratante_Fax ;
      private String aP3_Contratante_IE ;
      private String aP4_Contratante_NomeFantasia ;
      private String aP5_Contratante_Ramal ;
      private String aP6_Contratante_RazaoSocial ;
      private String aP7_Contratante_Telefone ;
      private String aP8_Contratante_WebSite ;
      private int aP11_Contratante_Codigo ;
   }

   public class prc_retornacontratantepelocnpj__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001P2 ;
          prmP001P2 = new Object[] {
          new Object[] {"@AV8Contratante_CNPJ",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP001P3 ;
          prmP001P3 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001P2", "SELECT [Pessoa_Codigo], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_Nome] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @AV8Contratante_CNPJ) AND ([Pessoa_TipoPessoa] = 'J') ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001P2,1,0,true,true )
             ,new CursorDef("P001P3", "SELECT TOP 1 T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contratante_Codigo], T2.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[Contratante_NomeFantasia], T1.[Contratante_IE], T1.[Contratante_Telefone], T1.[Contratante_Ramal], T1.[Contratante_Fax], T1.[Contratante_Email], T1.[Contratante_WebSite], T1.[Municipio_Codigo], T3.[Estado_UF] FROM (([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) WHERE T1.[Contratante_PessoaCod] = @Pessoa_Codigo ORDER BY T1.[Contratante_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001P3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
