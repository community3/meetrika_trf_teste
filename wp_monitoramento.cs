/*
               File: WP_Monitoramento
        Description: Monitoramento do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:45:18.52
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_monitoramento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_monitoramento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_monitoramento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_37 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_37_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_37_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5Context);
               A43Contratada_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
               A438Contratada_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
               A71ContratadaUsuario_UsuarioPessoaNom = GetNextPar( );
               n71ContratadaUsuario_UsuarioPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
               A1394ContratadaUsuario_UsuarioAtivo = (bool)(BooleanUtil.Val(GetNextPar( )));
               n1394ContratadaUsuario_UsuarioAtivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1394ContratadaUsuario_UsuarioAtivo", A1394ContratadaUsuario_UsuarioAtivo);
               AV12sp1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12sp1", AV12sp1);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               AV13sp2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13sp2", AV13sp2);
               A828UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
               A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               A576ContratadaUsuario_CstUntPrdNrm = NumberUtil.Val( GetNextPar( ), ".");
               n576ContratadaUsuario_CstUntPrdNrm = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A576ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A576ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               A577ContratadaUsuario_CstUntPrdExt = NumberUtil.Val( GetNextPar( ), ".");
               n577ContratadaUsuario_CstUntPrdExt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A577ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A577ContratadaUsuario_CstUntPrdExt, 18, 5)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A77Contrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1013Contrato_PrepostoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n74Contrato_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A116Contrato_ValorUnidadeContratacao = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               A1869Contrato_DataTermino = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               A453Contrato_IndiceDivergencia = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
               A638ContratoServicos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               A1135ContratoGestor_UsuarioEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               n54Usuario_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
               AV26Printed = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
               AV32Contrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Numero", AV32Contrato_Numero);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid3") == 0 )
            {
               nRC_GXsfl_55 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_55_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_55_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid3_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid3") == 0 )
            {
               subGrid3_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A75Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n74Contrato_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5Context);
               A638ContratoServicos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A43Contratada_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
               AV40Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo), 6, 0)));
               AV8Count = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Count), 4, 0)));
               A438Contratada_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
               A77Contrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               AV12sp1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12sp1", AV12sp1);
               A605Servico_Sigla = GetNextPar( );
               n605Servico_Sigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               A1858ContratoServicos_Alias = GetNextPar( );
               n1858ContratoServicos_Alias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1858ContratoServicos_Alias", A1858ContratoServicos_Alias);
               A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A557Servico_VlrUnidadeContratada = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               A116Contrato_ValorUnidadeContratacao = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               A558Servico_Percentual = NumberUtil.Val( GetNextPar( ), ".");
               n558Servico_Percentual = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
               A1455ContratoServicos_IndiceDivergencia = NumberUtil.Val( GetNextPar( ), ".");
               n1455ContratoServicos_IndiceDivergencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
               A453Contrato_IndiceDivergencia = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
               AV37TpVnc = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TpVnc", AV37TpVnc);
               A868ContratoServicos_TipoVnc = GetNextPar( );
               n868ContratoServicos_TipoVnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
               A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               A907ContratoServicosPrazoRegra_Inicio = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A907ContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( A907ContratoServicosPrazoRegra_Inicio, 14, 5)));
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               n54Usuario_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
               A1325ContratoServicos_StatusPagFnc = GetNextPar( );
               n1325ContratoServicos_StatusPagFnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
               AV26Printed = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid2_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid2") == 0 )
            {
               subGrid2_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A330ParametrosSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
               A532ParametrosSistema_EmailSdaHost = GetNextPar( );
               n532ParametrosSistema_EmailSdaHost = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
               A537ParametrosSistema_EmailSdaKey = GetNextPar( );
               n537ParametrosSistema_EmailSdaKey = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A537ParametrosSistema_EmailSdaKey", A537ParametrosSistema_EmailSdaKey);
               A534ParametrosSistema_EmailSdaPass = GetNextPar( );
               n534ParametrosSistema_EmailSdaPass = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
               A536ParametrosSistema_EmailSdaPort = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n536ParametrosSistema_EmailSdaPort = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
               A533ParametrosSistema_EmailSdaUser = GetNextPar( );
               n533ParametrosSistema_EmailSdaUser = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
               A1021ParametrosSistema_PathCrtf = GetNextPar( );
               n1021ParametrosSistema_PathCrtf = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
               A1499ParametrosSistema_FlsEvd = GetNextPar( );
               n1499ParametrosSistema_FlsEvd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
               A1163ParametrosSistema_HostMensuracao = GetNextPar( );
               n1163ParametrosSistema_HostMensuracao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
               AV44DirNome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DirNome", AV44DirNome);
               AV20Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAQE2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTQE2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299451894");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_monitoramento.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_37", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_37), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_55", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_55), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTEXT", AV5Context);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTEXT", AV5Context);
         }
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_SIGLA", StringUtil.RTrim( A438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_hidden_field( context, "vSP1", StringUtil.RTrim( AV12sp1));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSP2", StringUtil.RTrim( AV13sp2));
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( A576ContratadaUsuario_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( A577ContratadaUsuario_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_VALORUNIDADECONTRATACAO", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.DToC( A1869Contrato_DataTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_INDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_ATIVO", A54Usuario_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "vPRINTED", AV26Printed);
         GxWebStd.gx_hidden_field( context, "vCONTRATO_NUMERO", StringUtil.RTrim( AV32Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAHOST", A532ParametrosSistema_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAKEY", StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAPASS", A534ParametrosSistema_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAPORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAUSER", A533ParametrosSistema_EmailSdaUser);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_PATHCRTF", A1021ParametrosSistema_PathCrtf);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_FLSEVD", A1499ParametrosSistema_FlsEvd);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_HOSTMENSURACAO", A1163ParametrosSistema_HostMensuracao);
         GxWebStd.gx_hidden_field( context, "vDIRNOME", StringUtil.RTrim( AV44DirNome));
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Count), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_ALIAS", StringUtil.RTrim( A1858ContratoServicos_Alias));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VLRUNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_INDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A1455ContratoServicos_IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTPVNC", StringUtil.RTrim( AV37TpVnc));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_TIPOVNC", StringUtil.RTrim( A868ContratoServicos_TipoVnc));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_STATUSPAGFNC", StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc));
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID3_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID2_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXC1", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40000GXC1), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID3_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID2_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEQE2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTQE2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_monitoramento.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_Monitoramento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Monitoramento do Sistema" ;
      }

      protected void WBQE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_QE2( true) ;
         }
         else
         {
            wb_table1_2_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTQE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Monitoramento do Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQE0( ) ;
      }

      protected void WSQE2( )
      {
         STARTQE2( ) ;
         EVTQE2( ) ;
      }

      protected void EVTQE2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_37_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
                              SubsflControlProps_372( ) ;
                              AV9Valido1 = cgiGet( edtavValido1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido1_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV9Valido1)) ? AV59Valido1_GXI : context.convertURL( context.PathToRelativeUrl( AV9Valido1))));
                              AV10Warning1 = cgiGet( edtavWarning1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning1_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10Warning1)) ? AV60Warning1_GXI : context.convertURL( context.PathToRelativeUrl( AV10Warning1))));
                              AV11Erro1 = cgiGet( edtavErro1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro1_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11Erro1)) ? AV61Erro1_GXI : context.convertURL( context.PathToRelativeUrl( AV11Erro1))));
                              AV6Descricao1 = cgiGet( edtavDescricao1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E11QE2 */
                                    E11QE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12QE2 */
                                    E12QE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13QE2 */
                                    E13QE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID3.LOAD") == 0 )
                           {
                              nGXsfl_55_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
                              SubsflControlProps_555( ) ;
                              AV21Valido3 = cgiGet( edtavValido3_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido3_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21Valido3)) ? AV53Valido3_GXI : context.convertURL( context.PathToRelativeUrl( AV21Valido3))));
                              AV22Warning3 = cgiGet( edtavWarning3_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning3_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV22Warning3)) ? AV54Warning3_GXI : context.convertURL( context.PathToRelativeUrl( AV22Warning3))));
                              AV23Erro3 = cgiGet( edtavErro3_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro3_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV23Erro3)) ? AV55Erro3_GXI : context.convertURL( context.PathToRelativeUrl( AV23Erro3))));
                              AV24Descricao3 = cgiGet( edtavDescricao3_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRID3.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14QE5 */
                                    E14QE5 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID2.LOAD") == 0 )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_733( ) ;
                              AV17Valido2 = cgiGet( edtavValido2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido2_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Valido2)) ? AV49Valido2_GXI : context.convertURL( context.PathToRelativeUrl( AV17Valido2))));
                              AV18Warning2 = cgiGet( edtavWarning2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning2_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Warning2)) ? AV50Warning2_GXI : context.convertURL( context.PathToRelativeUrl( AV18Warning2))));
                              AV19Erro2 = cgiGet( edtavErro2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro2_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Erro2)) ? AV51Erro2_GXI : context.convertURL( context.PathToRelativeUrl( AV19Erro2))));
                              AV20Descricao2 = cgiGet( edtavDescricao2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRID2.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15QE3 */
                                    E15QE3 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAQE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_372( ) ;
         while ( nGXsfl_37_idx <= nRC_GXsfl_37 )
         {
            sendrow_372( ) ;
            nGXsfl_37_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_733( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_733( ) ;
            nGXsfl_73_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_733( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      protected void gxnrGrid3_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_555( ) ;
         while ( nGXsfl_55_idx <= nRC_GXsfl_55 )
         {
            sendrow_555( ) ;
            nGXsfl_55_idx = (short)(((subGrid3_Islastpage==1)&&(nGXsfl_55_idx+1>subGrid3_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_555( ) ;
         }
         context.GX_webresponse.AddString(Grid3Container.ToJavascriptSource());
         /* End function gxnrGrid3_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        int A52Contratada_AreaTrabalhoCod ,
                                        wwpbaseobjects.SdtWWPContext AV5Context ,
                                        bool A43Contratada_Ativo ,
                                        String A438Contratada_Sigla ,
                                        String A71ContratadaUsuario_UsuarioPessoaNom ,
                                        bool A1394ContratadaUsuario_UsuarioAtivo ,
                                        String AV12sp1 ,
                                        int A1Usuario_Codigo ,
                                        int A69ContratadaUsuario_UsuarioCod ,
                                        String AV13sp2 ,
                                        int A828UsuarioServicos_UsuarioCod ,
                                        int A66ContratadaUsuario_ContratadaCod ,
                                        decimal A576ContratadaUsuario_CstUntPrdNrm ,
                                        decimal A577ContratadaUsuario_CstUntPrdExt ,
                                        int A39Contratada_Codigo ,
                                        bool A92Contrato_Ativo ,
                                        String A77Contrato_Numero ,
                                        int A1013Contrato_PrepostoCod ,
                                        int A74Contrato_Codigo ,
                                        decimal A116Contrato_ValorUnidadeContratacao ,
                                        DateTime A1869Contrato_DataTermino ,
                                        decimal A453Contrato_IndiceDivergencia ,
                                        bool A638ContratoServicos_Ativo ,
                                        bool A1135ContratoGestor_UsuarioEhContratante ,
                                        bool A54Usuario_Ativo ,
                                        bool AV26Printed ,
                                        String AV32Contrato_Numero )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RFQE2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void gxgrGrid3_refresh( int subGrid3_Rows ,
                                        int A75Contrato_AreaTrabalhoCod ,
                                        int A39Contratada_Codigo ,
                                        int A74Contrato_Codigo ,
                                        wwpbaseobjects.SdtWWPContext AV5Context ,
                                        bool A638ContratoServicos_Ativo ,
                                        bool A92Contrato_Ativo ,
                                        bool A43Contratada_Ativo ,
                                        int AV40Contrato_Codigo ,
                                        short AV8Count ,
                                        String A438Contratada_Sigla ,
                                        String A77Contrato_Numero ,
                                        String AV12sp1 ,
                                        String A605Servico_Sigla ,
                                        String A1858ContratoServicos_Alias ,
                                        int A160ContratoServicos_Codigo ,
                                        decimal A557Servico_VlrUnidadeContratada ,
                                        decimal A116Contrato_ValorUnidadeContratacao ,
                                        decimal A558Servico_Percentual ,
                                        decimal A1455ContratoServicos_IndiceDivergencia ,
                                        decimal A453Contrato_IndiceDivergencia ,
                                        String AV37TpVnc ,
                                        String A868ContratoServicos_TipoVnc ,
                                        int A903ContratoServicosPrazo_CntSrvCod ,
                                        decimal A907ContratoServicosPrazoRegra_Inicio ,
                                        int A157ServicoGrupo_Codigo ,
                                        bool A54Usuario_Ativo ,
                                        String A1325ContratoServicos_StatusPagFnc ,
                                        bool AV26Printed )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID3_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Rows), 6, 0, ".", "")));
         /* Execute user event: E13QE2 */
         E13QE2 ();
         GRID3_nCurrentRecord = 0;
         RFQE5( ) ;
         /* End function gxgrGrid3_refresh */
      }

      protected void gxgrGrid2_refresh( int subGrid2_Rows ,
                                        int A330ParametrosSistema_Codigo ,
                                        String A532ParametrosSistema_EmailSdaHost ,
                                        String A537ParametrosSistema_EmailSdaKey ,
                                        String A534ParametrosSistema_EmailSdaPass ,
                                        short A536ParametrosSistema_EmailSdaPort ,
                                        String A533ParametrosSistema_EmailSdaUser ,
                                        String A1021ParametrosSistema_PathCrtf ,
                                        String A1499ParametrosSistema_FlsEvd ,
                                        String A1163ParametrosSistema_HostMensuracao ,
                                        String AV44DirNome ,
                                        String AV20Descricao2 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID2_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Rows), 6, 0, ".", "")));
         /* Execute user event: E13QE2 */
         E13QE2 ();
         GRID2_nCurrentRecord = 0;
         RFQE3( ) ;
         /* End function gxgrGrid2_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQE2( ) ;
         RFQE5( ) ;
         RFQE3( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDescricao1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao1_Enabled), 5, 0)));
         edtavDescricao3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao3_Enabled), 5, 0)));
         edtavDescricao2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao2_Enabled), 5, 0)));
      }

      protected void RFQE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 37;
         /* Execute user event: E13QE2 */
         E13QE2 ();
         nGXsfl_37_idx = 1;
         sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
         SubsflControlProps_372( ) ;
         nGXsfl_37_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_372( ) ;
            /* Execute user event: E12QE2 */
            E12QE2 ();
            if ( ( GRID1_nCurrentRecord > 0 ) && ( GRID1_nGridOutOfScope == 0 ) && ( nGXsfl_37_idx == 1 ) )
            {
               GRID1_nCurrentRecord = 0;
               GRID1_nGridOutOfScope = 1;
               subgrid1_firstpage( ) ;
               /* Execute user event: E12QE2 */
               E12QE2 ();
            }
            wbEnd = 37;
            WBQE0( ) ;
         }
         nGXsfl_37_Refreshing = 0;
      }

      protected void RFQE3( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid2Container.ClearRows();
         }
         wbStart = 73;
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_733( ) ;
         nGXsfl_73_Refreshing = 1;
         Grid2Container.AddObjectProperty("GridName", "Grid2");
         Grid2Container.AddObjectProperty("CmpContext", "");
         Grid2Container.AddObjectProperty("InMasterPage", "false");
         Grid2Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
         Grid2Container.PageSize = subGrid2_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_733( ) ;
            /* Execute user event: E15QE3 */
            E15QE3 ();
            if ( ( GRID2_nCurrentRecord > 0 ) && ( GRID2_nGridOutOfScope == 0 ) && ( nGXsfl_73_idx == 1 ) )
            {
               GRID2_nCurrentRecord = 0;
               GRID2_nGridOutOfScope = 1;
               subgrid2_firstpage( ) ;
               /* Execute user event: E15QE3 */
               E15QE3 ();
            }
            wbEnd = 73;
            WBQE0( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected void RFQE5( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid3Container.ClearRows();
         }
         wbStart = 55;
         nGXsfl_55_idx = 1;
         sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
         SubsflControlProps_555( ) ;
         nGXsfl_55_Refreshing = 1;
         Grid3Container.AddObjectProperty("GridName", "Grid3");
         Grid3Container.AddObjectProperty("CmpContext", "");
         Grid3Container.AddObjectProperty("InMasterPage", "false");
         Grid3Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         Grid3Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid3Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid3Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Backcolorstyle), 1, 0, ".", "")));
         Grid3Container.PageSize = subGrid3_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_555( ) ;
            /* Execute user event: E14QE5 */
            E14QE5 ();
            if ( ( GRID3_nCurrentRecord > 0 ) && ( GRID3_nGridOutOfScope == 0 ) && ( nGXsfl_55_idx == 1 ) )
            {
               GRID3_nCurrentRecord = 0;
               GRID3_nGridOutOfScope = 1;
               subgrid3_firstpage( ) ;
               /* Execute user event: E14QE5 */
               E14QE5 ();
            }
            wbEnd = 55;
            WBQE0( ) ;
         }
         nGXsfl_55_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         if ( subGrid1_Rows > 0 )
         {
            return subGrid1_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
         }
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         if ( GRID1_nEOF == 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
         }
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
         }
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         subGrid1_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
         }
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, A52Contratada_AreaTrabalhoCod, AV5Context, A43Contratada_Ativo, A438Contratada_Sigla, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, AV12sp1, A1Usuario_Codigo, A69ContratadaUsuario_UsuarioCod, AV13sp2, A828UsuarioServicos_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A576ContratadaUsuario_CstUntPrdNrm, A577ContratadaUsuario_CstUntPrdExt, A39Contratada_Codigo, A92Contrato_Ativo, A77Contrato_Numero, A1013Contrato_PrepostoCod, A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao, A1869Contrato_DataTermino, A453Contrato_IndiceDivergencia, A638ContratoServicos_Ativo, A1135ContratoGestor_UsuarioEhContratante, A54Usuario_Ativo, AV26Printed, AV32Contrato_Numero) ;
         }
         return (int)(0) ;
      }

      protected int subGrid2_Pagecount( )
      {
         GRID2_nRecordCount = subGrid2_Recordcount( );
         if ( ((int)((GRID2_nRecordCount) % (subGrid2_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID2_nRecordCount/ (decimal)(subGrid2_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID2_nRecordCount/ (decimal)(subGrid2_Recordsperpage( ))))+1) ;
      }

      protected int subGrid2_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordsperpage( )
      {
         if ( subGrid2_Rows > 0 )
         {
            return subGrid2_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid2_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid2_firstpage( )
      {
         GRID2_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID2_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
         }
         return 0 ;
      }

      protected short subgrid2_nextpage( )
      {
         if ( GRID2_nEOF == 0 )
         {
            GRID2_nFirstRecordOnPage = (long)(GRID2_nFirstRecordOnPage+subGrid2_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID2_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
         }
         return (short)(((GRID2_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid2_previouspage( )
      {
         if ( GRID2_nFirstRecordOnPage >= subGrid2_Recordsperpage( ) )
         {
            GRID2_nFirstRecordOnPage = (long)(GRID2_nFirstRecordOnPage-subGrid2_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID2_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
         }
         return 0 ;
      }

      protected short subgrid2_lastpage( )
      {
         subGrid2_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
         }
         return 0 ;
      }

      protected int subgrid2_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID2_nFirstRecordOnPage = (long)(subGrid2_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID2_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID2_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid2_refresh( subGrid2_Rows, A330ParametrosSistema_Codigo, A532ParametrosSistema_EmailSdaHost, A537ParametrosSistema_EmailSdaKey, A534ParametrosSistema_EmailSdaPass, A536ParametrosSistema_EmailSdaPort, A533ParametrosSistema_EmailSdaUser, A1021ParametrosSistema_PathCrtf, A1499ParametrosSistema_FlsEvd, A1163ParametrosSistema_HostMensuracao, AV44DirNome, AV20Descricao2) ;
         }
         return (int)(0) ;
      }

      protected int subGrid3_Pagecount( )
      {
         GRID3_nRecordCount = subGrid3_Recordcount( );
         if ( ((int)((GRID3_nRecordCount) % (subGrid3_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID3_nRecordCount/ (decimal)(subGrid3_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID3_nRecordCount/ (decimal)(subGrid3_Recordsperpage( ))))+1) ;
      }

      protected int subGrid3_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid3_Recordsperpage( )
      {
         if ( subGrid3_Rows > 0 )
         {
            return subGrid3_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid3_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid3_firstpage( )
      {
         GRID3_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID3_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
         }
         return 0 ;
      }

      protected short subgrid3_nextpage( )
      {
         if ( GRID3_nEOF == 0 )
         {
            GRID3_nFirstRecordOnPage = (long)(GRID3_nFirstRecordOnPage+subGrid3_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID3_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
         }
         return (short)(((GRID3_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid3_previouspage( )
      {
         if ( GRID3_nFirstRecordOnPage >= subGrid3_Recordsperpage( ) )
         {
            GRID3_nFirstRecordOnPage = (long)(GRID3_nFirstRecordOnPage-subGrid3_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID3_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
         }
         return 0 ;
      }

      protected short subgrid3_lastpage( )
      {
         subGrid3_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
         }
         return 0 ;
      }

      protected int subgrid3_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID3_nFirstRecordOnPage = (long)(subGrid3_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID3_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID3_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid3_refresh( subGrid3_Rows, A75Contrato_AreaTrabalhoCod, A39Contratada_Codigo, A74Contrato_Codigo, AV5Context, A638ContratoServicos_Ativo, A92Contrato_Ativo, A43Contratada_Ativo, AV40Contrato_Codigo, AV8Count, A438Contratada_Sigla, A77Contrato_Numero, AV12sp1, A605Servico_Sigla, A1858ContratoServicos_Alias, A160ContratoServicos_Codigo, A557Servico_VlrUnidadeContratada, A116Contrato_ValorUnidadeContratacao, A558Servico_Percentual, A1455ContratoServicos_IndiceDivergencia, A453Contrato_IndiceDivergencia, AV37TpVnc, A868ContratoServicos_TipoVnc, A903ContratoServicosPrazo_CntSrvCod, A907ContratoServicosPrazoRegra_Inicio, A157ServicoGrupo_Codigo, A54Usuario_Ativo, A1325ContratoServicos_StatusPagFnc, AV26Printed) ;
         }
         return (int)(0) ;
      }

      protected void STRUPQE0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavDescricao1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao1_Enabled), 5, 0)));
         edtavDescricao3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao3_Enabled), 5, 0)));
         edtavDescricao2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao2_Enabled), 5, 0)));
         /* Using cursor H00QE3 */
         pr_default.execute(0, new Object[] {AV40Contrato_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = H00QE3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11QE2 */
         E11QE2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_37 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_37"), ",", "."));
            nRC_GXsfl_55 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_55"), ",", "."));
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ",", "."));
            GRID3_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID3_nFirstRecordOnPage"), ",", "."));
            GRID2_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID2_nFirstRecordOnPage"), ",", "."));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ",", "."));
            GRID3_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID3_nEOF"), ",", "."));
            GRID2_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID2_nEOF"), ",", "."));
            subGrid1_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID1_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
            subGrid3_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID3_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID3_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Rows), 6, 0, ".", "")));
            subGrid2_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID2_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID2_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11QE2 */
         E11QE2 ();
         if (returnInSub) return;
      }

      protected void E11QE2( )
      {
         /* Start Routine */
         subGrid2_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID2_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Rows), 6, 0, ".", "")));
         subGrid3_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID3_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Rows), 6, 0, ".", "")));
         subGrid1_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         subgrid1_gotopage( 0) ;
         subgrid2_gotopage( 0) ;
         subgrid3_gotopage( 0) ;
         AV12sp1 = StringUtil.PadL( AV12sp1, 5, "�");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12sp1", AV12sp1);
         AV13sp2 = StringUtil.PadL( AV12sp1, 10, "�");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13sp2", AV13sp2);
         AV14sp3 = StringUtil.PadL( AV12sp1, 15, "�");
         AV15sp4 = StringUtil.PadL( AV12sp1, 20, "�");
         AV16sp5 = StringUtil.PadL( AV12sp1, 25, "�");
      }

      private void E12QE2( )
      {
         /* Grid1_Load Routine */
         AV9Valido1 = context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValido1_Internalname, AV9Valido1);
         AV59Valido1_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         AV10Warning1 = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavWarning1_Internalname, AV10Warning1);
         AV60Warning1_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         AV11Erro1 = context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErro1_Internalname, AV11Erro1);
         AV61Erro1_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )));
         edtavWarning1_Linktarget = "_blank";
         edtavErro1_Linktarget = "_blank";
         AV62GXLvl229 = 0;
         /* Using cursor H00QE4 */
         pr_default.execute(1, new Object[] {AV5Context.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1595Contratada_AreaTrbSrvPdr = H00QE4_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00QE4_n1595Contratada_AreaTrbSrvPdr[0];
            A39Contratada_Codigo = H00QE4_A39Contratada_Codigo[0];
            A43Contratada_Ativo = H00QE4_A43Contratada_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00QE4_A52Contratada_AreaTrabalhoCod[0];
            A438Contratada_Sigla = H00QE4_A438Contratada_Sigla[0];
            A605Servico_Sigla = H00QE4_A605Servico_Sigla[0];
            n605Servico_Sigla = H00QE4_n605Servico_Sigla[0];
            A1595Contratada_AreaTrbSrvPdr = H00QE4_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00QE4_n1595Contratada_AreaTrbSrvPdr[0];
            A605Servico_Sigla = H00QE4_A605Servico_Sigla[0];
            n605Servico_Sigla = H00QE4_n605Servico_Sigla[0];
            AV62GXLvl229 = 1;
            AV6Descricao1 = A438Contratada_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 37;
            }
            if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
            {
               sendrow_372( ) ;
               GRID1_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
               if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
               {
                  GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
               }
            }
            if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
            {
               GRID1_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            }
            GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(37, Grid1Row);
            }
            AV63GXLvl235 = 0;
            /* Using cursor H00QE5 */
            pr_default.execute(2, new Object[] {A39Contratada_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00QE5_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00QE5_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A66ContratadaUsuario_ContratadaCod = H00QE5_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00QE5_A69ContratadaUsuario_UsuarioCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00QE5_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00QE5_n1394ContratadaUsuario_UsuarioAtivo[0];
               A577ContratadaUsuario_CstUntPrdExt = H00QE5_A577ContratadaUsuario_CstUntPrdExt[0];
               n577ContratadaUsuario_CstUntPrdExt = H00QE5_n577ContratadaUsuario_CstUntPrdExt[0];
               A576ContratadaUsuario_CstUntPrdNrm = H00QE5_A576ContratadaUsuario_CstUntPrdNrm[0];
               n576ContratadaUsuario_CstUntPrdNrm = H00QE5_n576ContratadaUsuario_CstUntPrdNrm[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00QE5_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00QE5_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00QE5_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00QE5_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00QE5_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00QE5_n1394ContratadaUsuario_UsuarioAtivo[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00QE5_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00QE5_n71ContratadaUsuario_UsuarioPessoaNom[0];
               AV63GXLvl235 = 1;
               /* Execute user subroutine: 'SEM' */
               S146 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               AV6Descricao1 = AV12sp1 + A71ContratadaUsuario_UsuarioPessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
               AV26Printed = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
               AV64GXLvl241 = 0;
               /* Using cursor H00QE6 */
               pr_default.execute(3, new Object[] {A69ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1Usuario_Codigo = H00QE6_A1Usuario_Codigo[0];
                  AV64GXLvl241 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               if ( AV64GXLvl241 == 0 )
               {
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
                  AV26Printed = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem perfis associados!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_associationusuariousuarioperfil.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               AV65GXLvl252 = 0;
               /* Using cursor H00QE7 */
               pr_default.execute(4, new Object[] {A69ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00QE7_A828UsuarioServicos_UsuarioCod[0];
                  AV65GXLvl252 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               if ( AV65GXLvl252 == 0 )
               {
                  if ( ! AV26Printed )
                  {
                     /* Load Method */
                     if ( wbStart != -1 )
                     {
                        wbStart = 37;
                     }
                     if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                     {
                        sendrow_372( ) ;
                        GRID1_nEOF = 1;
                        GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                        if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                        {
                           GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                        }
                     }
                     if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                     {
                        GRID1_nEOF = 0;
                        GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     }
                     GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                     if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                     {
                        context.DoAjaxLoad(37, Grid1Row);
                     }
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem servi�os associados!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_associationusuarioservicos.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode("" +A66ContratadaUsuario_ContratadaCod);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               if ( ( A576ContratadaUsuario_CstUntPrdNrm + A577ContratadaUsuario_CstUntPrdExt == Convert.ToDecimal( 0 )) )
               {
                  if ( ! AV26Printed )
                  {
                     /* Load Method */
                     if ( wbStart != -1 )
                     {
                        wbStart = 37;
                     }
                     if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                     {
                        sendrow_372( ) ;
                        GRID1_nEOF = 1;
                        GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                        if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                        {
                           GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                        }
                     }
                     if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                     {
                        GRID1_nEOF = 0;
                        GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     }
                     GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                     if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                     {
                        context.DoAjaxLoad(37, Grid1Row);
                     }
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem custo operacional!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( AV63GXLvl235 == 0 )
            {
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  returnInSub = true;
                  if (true) return;
               }
               AV6Descricao1 = AV12sp1 + "Sem usu�rios cadastrados!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
               edtavWarning1_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratadaUsuario"));
            }
            AV8Count = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Count), 4, 0)));
            /* Optimized group. */
            /* Using cursor H00QE8 */
            pr_default.execute(5, new Object[] {A39Contratada_Codigo});
            cV8Count = H00QE8_AV8Count[0];
            pr_default.close(5);
            AV8Count = (short)(AV8Count+cV8Count*1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Count), 4, 0)));
            /* End optimized group. */
            if ( AV8Count > 0 )
            {
               /* Using cursor H00QE9 */
               pr_default.execute(6, new Object[] {A39Contratada_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A92Contrato_Ativo = H00QE9_A92Contrato_Ativo[0];
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(6);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV12sp1 + "Existem " + StringUtil.Trim( StringUtil.Str( (decimal)(AV8Count), 4, 0)) + " contratos INATIVOS!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim("Contrato"));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(6);
               }
               pr_default.close(6);
            }
            AV8Count = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Count), 4, 0)));
            AV68GXLvl296 = 0;
            /* Using cursor H00QE12 */
            pr_default.execute(7, new Object[] {A39Contratada_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A54Usuario_Ativo = H00QE12_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00QE12_n54Usuario_Ativo[0];
               A74Contrato_Codigo = H00QE12_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00QE12_n74Contrato_Codigo[0];
               A92Contrato_Ativo = H00QE12_A92Contrato_Ativo[0];
               A1013Contrato_PrepostoCod = H00QE12_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00QE12_n1013Contrato_PrepostoCod[0];
               A116Contrato_ValorUnidadeContratacao = H00QE12_A116Contrato_ValorUnidadeContratacao[0];
               A453Contrato_IndiceDivergencia = H00QE12_A453Contrato_IndiceDivergencia[0];
               A77Contrato_Numero = H00QE12_A77Contrato_Numero[0];
               A843Contrato_DataFimTA = H00QE12_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00QE12_n843Contrato_DataFimTA[0];
               A83Contrato_DataVigenciaTermino = H00QE12_A83Contrato_DataVigenciaTermino[0];
               A843Contrato_DataFimTA = H00QE12_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00QE12_n843Contrato_DataFimTA[0];
               A54Usuario_Ativo = H00QE12_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00QE12_n54Usuario_Ativo[0];
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               AV68GXLvl296 = 1;
               AV32Contrato_Numero = A77Contrato_Numero;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Numero", AV32Contrato_Numero);
               if ( H00QE12_n1013Contrato_PrepostoCod[0] )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem preposto informado!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               if ( (Convert.ToDecimal(0)==A116Contrato_ValorUnidadeContratacao) )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem valor da unidade contratada!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               if ( A1869Contrato_DataTermino < DateTimeUtil.ServerDate( context, "DEFAULT") )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Contrato vencido!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               if ( (Convert.ToDecimal(0)==A453Contrato_IndiceDivergencia) )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem �ndice de aceita��o!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               if ( ( new prc_contratosaldo(context).executeUdp(  A74Contrato_Codigo) <= Convert.ToDecimal( 0 )) )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem saldo disponivel!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               AV69GXLvl334 = 0;
               /* Using cursor H00QE13 */
               pr_default.execute(8, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A638ContratoServicos_Ativo = H00QE13_A638ContratoServicos_Ativo[0];
                  AV69GXLvl334 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(8);
               }
               pr_default.close(8);
               if ( AV69GXLvl334 == 0 )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem servi�os assocciados!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wwcontrato.aspx") ;
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               AV70GXLvl344 = 0;
               /* Using cursor H00QE14 */
               pr_default.execute(9, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, n54Usuario_Ativo, A54Usuario_Ativo});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00QE14_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00QE14_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00QE14_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00QE14_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00QE14_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00QE14_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV70GXLvl344 = 1;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(9);
               }
               pr_default.close(9);
               if ( AV70GXLvl344 == 0 )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem gestor da contratante!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wwcontrato.aspx") ;
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               AV71GXLvl355 = 0;
               /* Using cursor H00QE15 */
               pr_default.execute(10, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, n54Usuario_Ativo, A54Usuario_Ativo});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00QE15_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00QE15_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00QE15_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00QE15_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00QE15_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00QE15_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV71GXLvl355 = 1;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(10);
               }
               pr_default.close(10);
               if ( AV71GXLvl355 == 0 )
               {
                  /* Execute user subroutine: 'TITULOCONTRATO' */
                  S1715 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(7);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV6Descricao1 = AV13sp2 + "Sem gestor da contratada!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
                  edtavWarning1_Link = formatLink("wwcontrato.aspx") ;
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 37;
                  }
                  if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
                  {
                     sendrow_372( ) ;
                     GRID1_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                     {
                        GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                     }
                  }
                  if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
                  {
                     GRID1_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  }
                  GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(37, Grid1Row);
                  }
               }
               pr_default.readNext(7);
            }
            pr_default.close(7);
            if ( AV68GXLvl296 == 0 )
            {
               /* Execute user subroutine: 'ERRO' */
               S166 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  returnInSub = true;
                  if (true) return;
               }
               AV6Descricao1 = AV12sp1 + "Sem contrato cadastrado / ativo!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
               edtavWarning1_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim("Contrato"));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 37;
               }
               if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
               {
                  sendrow_372( ) ;
                  GRID1_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
                  {
                     GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
                  }
               }
               if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
               {
                  GRID1_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
               }
               GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(37, Grid1Row);
               }
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV62GXLvl229 == 0 )
         {
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV6Descricao1 = "Sem Contratadas cadastradas!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 37;
            }
            if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
            {
               sendrow_372( ) ;
               GRID1_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
               if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
               {
                  GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
               }
            }
            if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
            {
               GRID1_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            }
            GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(37, Grid1Row);
            }
         }
      }

      protected void E13QE2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
         lblTbconfiguracoes_Caption = "Configura��es da �rea: "+AV5Context.gxTpr_Areatrabalho_descricao;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbconfiguracoes_Internalname, "Caption", lblTbconfiguracoes_Caption);
         AV37TpVnc = "CA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TpVnc", AV37TpVnc);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5Context", AV5Context);
      }

      protected void S146( )
      {
         /* 'SEM' Routine */
         edtavValido1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido1_Visible), 5, 0)));
         edtavWarning1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning1_Visible), 5, 0)));
         edtavErro1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro1_Visible), 5, 0)));
         edtavValido2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido2_Visible), 5, 0)));
         edtavWarning2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning2_Visible), 5, 0)));
         edtavErro2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro2_Visible), 5, 0)));
         edtavValido3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido3_Visible), 5, 0)));
         edtavWarning3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning3_Visible), 5, 0)));
         edtavErro3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro3_Visible), 5, 0)));
      }

      protected void S114( )
      {
         /* 'WARNING' Routine */
         edtavValido1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido1_Visible), 5, 0)));
         edtavWarning1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning1_Visible), 5, 0)));
         edtavErro1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro1_Visible), 5, 0)));
         edtavValido2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido2_Visible), 5, 0)));
         edtavWarning2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning2_Visible), 5, 0)));
         edtavErro2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro2_Visible), 5, 0)));
         edtavValido3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido3_Visible), 5, 0)));
         edtavWarning3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning3_Visible), 5, 0)));
         edtavErro3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro3_Visible), 5, 0)));
      }

      protected void S166( )
      {
         /* 'ERRO' Routine */
         edtavValido1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido1_Visible), 5, 0)));
         edtavWarning1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning1_Visible), 5, 0)));
         edtavErro1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro1_Visible), 5, 0)));
         edtavValido2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido2_Visible), 5, 0)));
         edtavWarning2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning2_Visible), 5, 0)));
         edtavErro2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro2_Visible), 5, 0)));
         edtavValido3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValido3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValido3_Visible), 5, 0)));
         edtavWarning3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning3_Visible), 5, 0)));
         edtavErro3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErro3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErro3_Visible), 5, 0)));
      }

      protected void S156( )
      {
         /* 'TITULOCNTSRV' Routine */
         if ( ! AV26Printed )
         {
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if (returnInSub) return;
            AV24Descricao3 = StringUtil.Trim( A438Contratada_Sigla) + " - Contrato: " + StringUtil.Trim( A77Contrato_Numero) + ", Servi�o: " + A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 55;
            }
            if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
            {
               sendrow_555( ) ;
               GRID3_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
               {
                  GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
               }
            }
            if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
            {
               GRID3_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
            }
            GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(55, Grid3Row);
            }
            AV26Printed = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
         }
      }

      protected void S1715( )
      {
         /* 'TITULOCONTRATO' Routine */
         if ( ! AV26Printed )
         {
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if (returnInSub) return;
            AV6Descricao1 = AV12sp1 + "Contrato " + AV32Contrato_Numero;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao1_Internalname, AV6Descricao1);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 37;
            }
            if ( ( subGrid1_Islastpage == 1 ) || ( subGrid1_Rows == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) ) ) )
            {
               sendrow_372( ) ;
               GRID1_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
               if ( ( subGrid1_Islastpage == 1 ) && ( ((int)((GRID1_nCurrentRecord) % (subGrid1_Recordsperpage( )))) == 0 ) )
               {
                  GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord;
               }
            }
            if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subGrid1_Recordsperpage( ) )
            {
               GRID1_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            }
            GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(37, Grid1Row);
            }
            AV26Printed = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
         }
      }

      protected void S136( )
      {
         /* 'SERVICOSINATIVOS' Routine */
         /* Using cursor H00QE17 */
         pr_default.execute(11, new Object[] {AV40Contrato_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A40000GXC1 = H00QE17_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(11);
         AV8Count = (short)(A40000GXC1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Count), 4, 0)));
      }

      protected void S123( )
      {
         /* 'DIRETORYCHECK' Routine */
         AV42Directory.Source = AV44DirNome;
         AV46FileNome = AV44DirNome + "\\Teste";
         AV41File.Source = AV46FileNome;
         if ( AV42Directory.Exists() )
         {
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if (returnInSub) return;
            AV20Descricao2 = "Pasta " + AV44DirNome + " existe.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
            if ( AV41File.Exists() )
            {
               /* Execute user subroutine: 'FILEDELETE' */
               S182 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'FILECREATE' */
               S192 ();
               if (returnInSub) return;
            }
            else
            {
               /* Execute user subroutine: 'FILECREATE' */
               S192 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'FILEDELETE' */
               S182 ();
               if (returnInSub) return;
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
         }
         else
         {
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV20Descricao2 = "Pasta " + AV44DirNome + " n�o achada!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
         }
      }

      protected void S192( )
      {
         /* 'FILECREATE' Routine */
         AV41File.Create();
         if ( AV41File.ErrCode == 0 )
         {
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if (returnInSub) return;
            AV20Descricao2 = " Pode criar arquivos.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
         }
         else
         {
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV20Descricao2 = "Pasta " + AV44DirNome + " sem permiss�o para criar arquivos!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
         }
      }

      protected void S182( )
      {
         /* 'FILEDELETE' Routine */
         AV41File.Delete();
         if ( AV41File.ErrCode == 0 )
         {
            /* Execute user subroutine: 'SEM' */
            S146 ();
            if (returnInSub) return;
            AV20Descricao2 = AV20Descricao2 + " Pode apagar arquivos.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
         }
         else
         {
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV20Descricao2 = "Pasta " + AV44DirNome + " sem permiss�o para apagar arquivos!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
         }
      }

      private void E15QE3( )
      {
         /* Grid2_Load Routine */
         AV17Valido2 = context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValido2_Internalname, AV17Valido2);
         AV49Valido2_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         AV18Warning2 = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavWarning2_Internalname, AV18Warning2);
         AV50Warning2_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         AV19Erro2 = context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErro2_Internalname, AV19Erro2);
         AV51Erro2_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )));
         edtavWarning2_Linktarget = "_blank";
         edtavErro2_Linktarget = "_blank";
         AV52GXLvl32 = 0;
         /* Using cursor H00QE18 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            A330ParametrosSistema_Codigo = H00QE18_A330ParametrosSistema_Codigo[0];
            A533ParametrosSistema_EmailSdaUser = H00QE18_A533ParametrosSistema_EmailSdaUser[0];
            n533ParametrosSistema_EmailSdaUser = H00QE18_n533ParametrosSistema_EmailSdaUser[0];
            A536ParametrosSistema_EmailSdaPort = H00QE18_A536ParametrosSistema_EmailSdaPort[0];
            n536ParametrosSistema_EmailSdaPort = H00QE18_n536ParametrosSistema_EmailSdaPort[0];
            A534ParametrosSistema_EmailSdaPass = H00QE18_A534ParametrosSistema_EmailSdaPass[0];
            n534ParametrosSistema_EmailSdaPass = H00QE18_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = H00QE18_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = H00QE18_n537ParametrosSistema_EmailSdaKey[0];
            A532ParametrosSistema_EmailSdaHost = H00QE18_A532ParametrosSistema_EmailSdaHost[0];
            n532ParametrosSistema_EmailSdaHost = H00QE18_n532ParametrosSistema_EmailSdaHost[0];
            A1021ParametrosSistema_PathCrtf = H00QE18_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = H00QE18_n1021ParametrosSistema_PathCrtf[0];
            A1499ParametrosSistema_FlsEvd = H00QE18_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = H00QE18_n1499ParametrosSistema_FlsEvd[0];
            A1163ParametrosSistema_HostMensuracao = H00QE18_A1163ParametrosSistema_HostMensuracao[0];
            n1163ParametrosSistema_HostMensuracao = H00QE18_n1163ParametrosSistema_HostMensuracao[0];
            AV52GXLvl32 = 1;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A532ParametrosSistema_EmailSdaHost)) || String.IsNullOrEmpty(StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey)) || String.IsNullOrEmpty(StringUtil.RTrim( A534ParametrosSistema_EmailSdaPass)) || (0==A536ParametrosSistema_EmailSdaPort) || String.IsNullOrEmpty(StringUtil.RTrim( A533ParametrosSistema_EmailSdaUser)) )
            {
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(12);
                  returnInSub = true;
                  if (true) return;
               }
               AV20Descricao2 = "Dados para envio de e-mails incompletos!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
               edtavWarning2_Link = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +1);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 73;
               }
               if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
               {
                  sendrow_733( ) ;
                  GRID2_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
                  {
                     GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
                  }
               }
               if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
               {
                  GRID2_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               }
               GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(73, Grid2Row);
               }
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) )
            {
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(12);
                  returnInSub = true;
                  if (true) return;
               }
               AV20Descricao2 = "Sem caminho da certifica��o digital!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
               edtavWarning2_Link = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +1);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 73;
               }
               if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
               {
                  sendrow_733( ) ;
                  GRID2_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
                  {
                     GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
                  }
               }
               if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
               {
                  GRID2_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               }
               GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(73, Grid2Row);
               }
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) )
            {
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(12);
                  returnInSub = true;
                  if (true) return;
               }
               AV20Descricao2 = "Sem caminho das evid�ncias!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
               edtavWarning2_Link = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +1);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 73;
               }
               if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
               {
                  sendrow_733( ) ;
                  GRID2_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
                  {
                     GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
                  }
               }
               if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
               {
                  GRID2_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               }
               GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(73, Grid2Row);
               }
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1163ParametrosSistema_HostMensuracao)) )
            {
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(12);
                  returnInSub = true;
                  if (true) return;
               }
               AV20Descricao2 = "Sem URL do APP de Mensura��o!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
               edtavWarning2_Link = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +1);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 73;
               }
               if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
               {
                  sendrow_733( ) ;
                  GRID2_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
                  {
                     GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
                  }
               }
               if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
               {
                  GRID2_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               }
               GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(73, Grid2Row);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(12);
         if ( AV52GXLvl32 == 0 )
         {
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV20Descricao2 = "Sem par�metros cadastrados!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao2_Internalname, AV20Descricao2);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 73;
            }
            if ( ( subGrid2_Islastpage == 1 ) || ( subGrid2_Rows == 0 ) || ( ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage ) && ( GRID2_nCurrentRecord < GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) ) ) )
            {
               sendrow_733( ) ;
               GRID2_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
               if ( ( subGrid2_Islastpage == 1 ) && ( ((int)((GRID2_nCurrentRecord) % (subGrid2_Recordsperpage( )))) == 0 ) )
               {
                  GRID2_nFirstRecordOnPage = GRID2_nCurrentRecord;
               }
            }
            if ( GRID2_nCurrentRecord >= GRID2_nFirstRecordOnPage + subGrid2_Recordsperpage( ) )
            {
               GRID2_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID2_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID2_nEOF), 1, 0, ".", "")));
            }
            GRID2_nCurrentRecord = (long)(GRID2_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(73, Grid2Row);
            }
         }
         AV44DirNome = "PDF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DirNome", AV44DirNome);
         /* Execute user subroutine: 'DIRETORYCHECK' */
         S123 ();
         if (returnInSub) return;
         AV44DirNome = "PublicTempStorage";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DirNome", AV44DirNome);
         /* Execute user subroutine: 'DIRETORYCHECK' */
         S123 ();
         if (returnInSub) return;
      }

      private void E14QE5( )
      {
         /* Grid3_Load Routine */
         AV21Valido3 = context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValido3_Internalname, AV21Valido3);
         AV53Valido3_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         AV22Warning3 = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavWarning3_Internalname, AV22Warning3);
         AV54Warning3_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         AV23Erro3 = context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErro3_Internalname, AV23Erro3);
         AV55Erro3_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )));
         edtavWarning3_Linktarget = "_blank";
         edtavErro3_Linktarget = "_blank";
         AV56GXLvl90 = 0;
         /* Using cursor H00QE19 */
         pr_default.execute(13, new Object[] {AV5Context.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A155Servico_Codigo = H00QE19_A155Servico_Codigo[0];
            A1013Contrato_PrepostoCod = H00QE19_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00QE19_n1013Contrato_PrepostoCod[0];
            A75Contrato_AreaTrabalhoCod = H00QE19_A75Contrato_AreaTrabalhoCod[0];
            A54Usuario_Ativo = H00QE19_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00QE19_n54Usuario_Ativo[0];
            A160ContratoServicos_Codigo = H00QE19_A160ContratoServicos_Codigo[0];
            A43Contratada_Ativo = H00QE19_A43Contratada_Ativo[0];
            A92Contrato_Ativo = H00QE19_A92Contrato_Ativo[0];
            A638ContratoServicos_Ativo = H00QE19_A638ContratoServicos_Ativo[0];
            A77Contrato_Numero = H00QE19_A77Contrato_Numero[0];
            A438Contratada_Sigla = H00QE19_A438Contratada_Sigla[0];
            A605Servico_Sigla = H00QE19_A605Servico_Sigla[0];
            n605Servico_Sigla = H00QE19_n605Servico_Sigla[0];
            A1858ContratoServicos_Alias = H00QE19_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = H00QE19_n1858ContratoServicos_Alias[0];
            A116Contrato_ValorUnidadeContratacao = H00QE19_A116Contrato_ValorUnidadeContratacao[0];
            A557Servico_VlrUnidadeContratada = H00QE19_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = H00QE19_A558Servico_Percentual[0];
            n558Servico_Percentual = H00QE19_n558Servico_Percentual[0];
            A453Contrato_IndiceDivergencia = H00QE19_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = H00QE19_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = H00QE19_n1455ContratoServicos_IndiceDivergencia[0];
            A868ContratoServicos_TipoVnc = H00QE19_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = H00QE19_n868ContratoServicos_TipoVnc[0];
            A157ServicoGrupo_Codigo = H00QE19_A157ServicoGrupo_Codigo[0];
            A1325ContratoServicos_StatusPagFnc = H00QE19_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = H00QE19_n1325ContratoServicos_StatusPagFnc[0];
            A74Contrato_Codigo = H00QE19_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00QE19_n74Contrato_Codigo[0];
            A39Contratada_Codigo = H00QE19_A39Contratada_Codigo[0];
            A605Servico_Sigla = H00QE19_A605Servico_Sigla[0];
            n605Servico_Sigla = H00QE19_n605Servico_Sigla[0];
            A157ServicoGrupo_Codigo = H00QE19_A157ServicoGrupo_Codigo[0];
            A1013Contrato_PrepostoCod = H00QE19_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00QE19_n1013Contrato_PrepostoCod[0];
            A75Contrato_AreaTrabalhoCod = H00QE19_A75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00QE19_A92Contrato_Ativo[0];
            A77Contrato_Numero = H00QE19_A77Contrato_Numero[0];
            A116Contrato_ValorUnidadeContratacao = H00QE19_A116Contrato_ValorUnidadeContratacao[0];
            A453Contrato_IndiceDivergencia = H00QE19_A453Contrato_IndiceDivergencia[0];
            A39Contratada_Codigo = H00QE19_A39Contratada_Codigo[0];
            A54Usuario_Ativo = H00QE19_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00QE19_n54Usuario_Ativo[0];
            A43Contratada_Ativo = H00QE19_A43Contratada_Ativo[0];
            A438Contratada_Sigla = H00QE19_A438Contratada_Sigla[0];
            AV56GXLvl90 = 1;
            if ( AV40Contrato_Codigo != A74Contrato_Codigo )
            {
               AV40Contrato_Codigo = A74Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo), 6, 0)));
               /* Execute user subroutine: 'SERVICOSINATIVOS' */
               S136 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV8Count > 0 )
               {
                  /* Execute user subroutine: 'SEM' */
                  S146 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV24Descricao3 = StringUtil.Trim( A438Contratada_Sigla) + " - Contrato: " + StringUtil.Trim( A77Contrato_Numero);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 55;
                  }
                  if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
                  {
                     sendrow_555( ) ;
                     GRID3_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                     {
                        GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                     }
                  }
                  if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
                  {
                     GRID3_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  }
                  GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(55, Grid3Row);
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV24Descricao3 = AV12sp1 + "Existem " + StringUtil.Trim( StringUtil.Str( (decimal)(AV8Count), 4, 0)) + " servi�os INATIVOS!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 55;
                  }
                  if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
                  {
                     sendrow_555( ) ;
                     GRID3_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                     {
                        GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                     }
                  }
                  if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
                  {
                     GRID3_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  }
                  GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(55, Grid3Row);
                  }
               }
            }
            AV31ontratada_Sigla = A438Contratada_Sigla;
            AV32Contrato_Numero = A77Contrato_Numero;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Numero", AV32Contrato_Numero);
            AV33Servico_Sigla = StringUtil.Upper( A605Servico_Sigla);
            AV26Printed = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Printed", AV26Printed);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1858ContratoServicos_Alias)) )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'ERRO' */
               S166 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem ALIAS!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( new prc_diasparaanalise(context).executeUdp( ref  A160ContratoServicos_Codigo,  (decimal)(0),  0) == 0 )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem prezo de an�lise!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( new prc_diasparaentrega(context).executeUdp( ref  A160ContratoServicos_Codigo,  (decimal)(1),  0) - new prc_diasparaanalise(context).executeUdp( ref  A160ContratoServicos_Codigo,  (decimal)(0),  0) == 0 )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem SLA informado!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( ( A557Servico_VlrUnidadeContratada + A116Contrato_ValorUnidadeContratacao == Convert.ToDecimal( 0 )) )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem valor da unidade contratada!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( (Convert.ToDecimal(0)==A558Servico_Percentual) )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem percentual de pagamento!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( ( A1455ContratoServicos_IndiceDivergencia + A453Contrato_IndiceDivergencia == Convert.ToDecimal( 0 )) )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem �ndice de diverg�ncia!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( StringUtil.StringSearch( AV33Servico_Sigla, "VAL", 1) + StringUtil.StringSearch( AV33Servico_Sigla, "COMP", 1) + StringUtil.StringSearch( StringUtil.Upper( A1858ContratoServicos_Alias), "VAL", 1) + StringUtil.StringSearch( StringUtil.Upper( A1858ContratoServicos_Alias), "COMP", 1) > 0 )
            {
               if ( H00QE19_n868ContratoServicos_TipoVnc[0] || ( StringUtil.StringSearch( AV37TpVnc, A868ContratoServicos_TipoVnc, 1) == 0 ) )
               {
                  /* Execute user subroutine: 'TITULOCNTSRV' */
                  S156 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV24Descricao3 = AV12sp1 + "Suspeita de servi�o de compara��o sem tipo de vinculo configurado!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
                  edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 55;
                  }
                  if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
                  {
                     sendrow_555( ) ;
                     GRID3_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                     {
                        GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                     }
                  }
                  if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
                  {
                     GRID3_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  }
                  GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(55, Grid3Row);
                  }
               }
            }
            else
            {
               if ( ! H00QE19_n868ContratoServicos_TipoVnc[0] && ( StringUtil.StringSearch( AV37TpVnc, A868ContratoServicos_TipoVnc, 1) > 0 ) )
               {
                  /* Execute user subroutine: 'TITULOCNTSRV' */
                  S156 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'WARNING' */
                  S114 ();
                  if ( returnInSub )
                  {
                     pr_default.close(13);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV24Descricao3 = AV12sp1 + "Suspeita de servi�o normal configurado com tipo de vinculo de Compara��o!";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
                  edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 55;
                  }
                  if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
                  {
                     sendrow_555( ) ;
                     GRID3_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                     {
                        GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                     }
                  }
                  if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
                  {
                     GRID3_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  }
                  GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(55, Grid3Row);
                  }
               }
            }
            AV35min = (decimal)(9999);
            AV57GXLvl175 = 0;
            /* Using cursor H00QE20 */
            pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A903ContratoServicosPrazo_CntSrvCod = H00QE20_A903ContratoServicosPrazo_CntSrvCod[0];
               A907ContratoServicosPrazoRegra_Inicio = H00QE20_A907ContratoServicosPrazoRegra_Inicio[0];
               AV57GXLvl175 = 1;
               if ( A907ContratoServicosPrazoRegra_Inicio < AV35min )
               {
                  AV35min = A907ContratoServicosPrazoRegra_Inicio;
               }
               pr_default.readNext(14);
            }
            pr_default.close(14);
            if ( AV57GXLvl175 == 0 )
            {
               AV35min = 0;
            }
            if ( ( AV35min > Convert.ToDecimal( 0 )) )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Valores entra 0 e " + StringUtil.Trim( StringUtil.Str( AV35min, 14, 5)) + " n�o considerados no SLA!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim("Servico"));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            AV58GXLvl190 = 0;
            /* Using cursor H00QE21 */
            pr_default.execute(15, new Object[] {n54Usuario_Ativo, A54Usuario_Ativo});
            while ( (pr_default.getStatus(15) != 101) )
            {
               AV58GXLvl190 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(15);
            }
            pr_default.close(15);
            if ( AV58GXLvl190 == 0 )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem usu�rios associados!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim("Servico"));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            if ( H00QE19_n1325ContratoServicos_StatusPagFnc[0] )
            {
               /* Execute user subroutine: 'TITULOCNTSRV' */
               S156 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'WARNING' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               AV24Descricao3 = AV12sp1 + "Sem status para liquida��o dos colaboradores!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
               edtavWarning3_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 55;
               }
               if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
               {
                  sendrow_555( ) ;
                  GRID3_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
                  {
                     GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
                  }
               }
               if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
               {
                  GRID3_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               }
               GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(55, Grid3Row);
               }
            }
            pr_default.readNext(13);
         }
         pr_default.close(13);
         if ( AV56GXLvl90 == 0 )
         {
            /* Execute user subroutine: 'ERRO' */
            S166 ();
            if (returnInSub) return;
            AV24Descricao3 = "Sem servi�os contratados!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao3_Internalname, AV24Descricao3);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 55;
            }
            if ( ( subGrid3_Islastpage == 1 ) || ( subGrid3_Rows == 0 ) || ( ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage ) && ( GRID3_nCurrentRecord < GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) ) ) )
            {
               sendrow_555( ) ;
               GRID3_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
               if ( ( subGrid3_Islastpage == 1 ) && ( ((int)((GRID3_nCurrentRecord) % (subGrid3_Recordsperpage( )))) == 0 ) )
               {
                  GRID3_nFirstRecordOnPage = GRID3_nCurrentRecord;
               }
            }
            if ( GRID3_nCurrentRecord >= GRID3_nFirstRecordOnPage + subGrid3_Recordsperpage( ) )
            {
               GRID3_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID3_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID3_nEOF), 1, 0, ".", "")));
            }
            GRID3_nCurrentRecord = (long)(GRID3_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(55, Grid3Row);
            }
         }
      }

      protected void wb_table1_2_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_QE2( true) ;
         }
         else
         {
            wb_table2_8_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_22_QE2( true) ;
         }
         else
         {
            wb_table3_22_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QE2e( true) ;
         }
         else
         {
            wb_table1_2_QE2e( false) ;
         }
      }

      protected void wb_table3_22_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_25_QE2( true) ;
         }
         else
         {
            wb_table4_25_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_43_QE2( true) ;
         }
         else
         {
            wb_table5_43_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table5_43_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_61_QE2( true) ;
         }
         else
         {
            wb_table6_61_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table6_61_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_QE2e( true) ;
         }
         else
         {
            wb_table3_22_QE2e( false) ;
         }
      }

      protected void wb_table6_61_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "TableFSHeader", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb2_Internalname, "Par�metros / Sistema", "", "", lblTb2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgValido2_Internalname, imgValido2_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWarning2_Internalname, imgWarning2_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgError2_Internalname, context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /*  Grid Control  */
            Grid2Container.SetWrapped(nGXWrapped);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid2_Internalname, subGrid2_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid2_Backcolorstyle == 0 )
               {
                  subGrid2_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid2_Class) > 0 )
                  {
                     subGrid2_Linesclass = subGrid2_Class+"Title";
                  }
               }
               else
               {
                  subGrid2_Titlebackstyle = 1;
                  if ( subGrid2_Backcolorstyle == 1 )
                  {
                     subGrid2_Titlebackcolor = subGrid2_Allbackcolor;
                     if ( StringUtil.Len( subGrid2_Class) > 0 )
                     {
                        subGrid2_Linesclass = subGrid2_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid2_Class) > 0 )
                     {
                        subGrid2_Linesclass = subGrid2_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+((edtavValido2_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+((edtavWarning2_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+((edtavErro2_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid2Container.AddObjectProperty("GridName", "Grid2");
            }
            else
            {
               Grid2Container.AddObjectProperty("GridName", "Grid2");
               Grid2Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("CmpContext", "");
               Grid2Container.AddObjectProperty("InMasterPage", "false");
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.convertURL( AV17Valido2));
               Grid2Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValido2_Visible), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.convertURL( AV18Warning2));
               Grid2Column.AddObjectProperty("Link", StringUtil.RTrim( edtavWarning2_Link));
               Grid2Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavWarning2_Linktarget));
               Grid2Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning2_Visible), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.convertURL( AV19Erro2));
               Grid2Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavErro2_Linktarget));
               Grid2Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavErro2_Visible), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", AV20Descricao2);
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricao2_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid2", Grid2Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData", Grid2Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_61_QE2e( true) ;
         }
         else
         {
            wb_table6_61_QE2e( false) ;
         }
      }

      protected void wb_table5_43_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "TableFSHeader", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb3_Internalname, "Servi�os", "", "", lblTb3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgValido3_Internalname, imgValido3_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWarning3_Internalname, imgWarning3_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgError3_Internalname, context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /*  Grid Control  */
            Grid3Container.SetWrapped(nGXWrapped);
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid3Container"+"DivS\" data-gxgridid=\"55\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid3_Internalname, subGrid3_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid3_Backcolorstyle == 0 )
               {
                  subGrid3_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid3_Class) > 0 )
                  {
                     subGrid3_Linesclass = subGrid3_Class+"Title";
                  }
               }
               else
               {
                  subGrid3_Titlebackstyle = 1;
                  if ( subGrid3_Backcolorstyle == 1 )
                  {
                     subGrid3_Titlebackcolor = subGrid3_Allbackcolor;
                     if ( StringUtil.Len( subGrid3_Class) > 0 )
                     {
                        subGrid3_Linesclass = subGrid3_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid3_Class) > 0 )
                     {
                        subGrid3_Linesclass = subGrid3_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid3_Linesclass+"\" "+" style=\""+((edtavValido3_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid3_Linesclass+"\" "+" style=\""+((edtavWarning3_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid3_Linesclass+"\" "+" style=\""+((edtavErro3_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid3_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid3Container.AddObjectProperty("GridName", "Grid3");
            }
            else
            {
               Grid3Container.AddObjectProperty("GridName", "Grid3");
               Grid3Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               Grid3Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid3Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid3Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Backcolorstyle), 1, 0, ".", "")));
               Grid3Container.AddObjectProperty("CmpContext", "");
               Grid3Container.AddObjectProperty("InMasterPage", "false");
               Grid3Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid3Column.AddObjectProperty("Value", context.convertURL( AV21Valido3));
               Grid3Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValido3_Visible), 5, 0, ".", "")));
               Grid3Container.AddColumnProperties(Grid3Column);
               Grid3Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid3Column.AddObjectProperty("Value", context.convertURL( AV22Warning3));
               Grid3Column.AddObjectProperty("Link", StringUtil.RTrim( edtavWarning3_Link));
               Grid3Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavWarning3_Linktarget));
               Grid3Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning3_Visible), 5, 0, ".", "")));
               Grid3Container.AddColumnProperties(Grid3Column);
               Grid3Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid3Column.AddObjectProperty("Value", context.convertURL( AV23Erro3));
               Grid3Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavErro3_Linktarget));
               Grid3Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavErro3_Visible), 5, 0, ".", "")));
               Grid3Container.AddColumnProperties(Grid3Column);
               Grid3Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid3Column.AddObjectProperty("Value", AV24Descricao3);
               Grid3Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricao3_Enabled), 5, 0, ".", "")));
               Grid3Container.AddColumnProperties(Grid3Column);
               Grid3Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Allowselection), 1, 0, ".", "")));
               Grid3Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Selectioncolor), 9, 0, ".", "")));
               Grid3Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Allowhovering), 1, 0, ".", "")));
               Grid3Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Hoveringcolor), 9, 0, ".", "")));
               Grid3Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Allowcollapsing), 1, 0, ".", "")));
               Grid3Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid3_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 55 )
         {
            wbEnd = 0;
            nRC_GXsfl_55 = (short)(nGXsfl_55_idx-1);
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid3Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid3", Grid3Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid3ContainerData", Grid3Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid3ContainerData"+"V", Grid3Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid3ContainerData"+"V"+"\" value='"+Grid3Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_43_QE2e( true) ;
         }
         else
         {
            wb_table5_43_QE2e( false) ;
         }
      }

      protected void wb_table4_25_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "TableFSHeader", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb1_Internalname, "Contratadas", "", "", lblTb1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgValido1_Internalname, imgValido1_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWarning1_Internalname, imgWarning1_Bitmap, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgError1_Internalname, context.GetImagePath( "b63f5646-3a80-45fa-b28f-1940d496304c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"37\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavValido1_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavWarning1_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavErro1_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV9Valido1));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValido1_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV10Warning1));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavWarning1_Link));
               Grid1Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavWarning1_Linktarget));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning1_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV11Erro1));
               Grid1Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavErro1_Linktarget));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavErro1_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", AV6Descricao1);
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricao1_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 37 )
         {
            wbEnd = 0;
            nRC_GXsfl_37 = (short)(nGXsfl_37_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_QE2e( true) ;
         }
         else
         {
            wb_table4_25_QE2e( false) ;
         }
      }

      protected void wb_table2_8_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconfiguracoes_Internalname, lblTbconfiguracoes_Caption, "", "", lblTbconfiguracoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Monitoramento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table7_13_QE2( true) ;
         }
         else
         {
            wb_table7_13_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table7_13_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table8_17_QE2( true) ;
         }
         else
         {
            wb_table8_17_QE2( false) ;
         }
         return  ;
      }

      protected void wb_table8_17_QE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_QE2e( true) ;
         }
         else
         {
            wb_table2_8_QE2e( false) ;
         }
      }

      protected void wb_table8_17_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_17_QE2e( true) ;
         }
         else
         {
            wb_table8_17_QE2e( false) ;
         }
      }

      protected void wb_table7_13_QE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_13_QE2e( true) ;
         }
         else
         {
            wb_table7_13_QE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQE2( ) ;
         WSQE2( ) ;
         WEQE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299452148");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_monitoramento.js", "?20205299452148");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_372( )
      {
         edtavValido1_Internalname = "vVALIDO1_"+sGXsfl_37_idx;
         edtavWarning1_Internalname = "vWARNING1_"+sGXsfl_37_idx;
         edtavErro1_Internalname = "vERRO1_"+sGXsfl_37_idx;
         edtavDescricao1_Internalname = "vDESCRICAO1_"+sGXsfl_37_idx;
      }

      protected void SubsflControlProps_fel_372( )
      {
         edtavValido1_Internalname = "vVALIDO1_"+sGXsfl_37_fel_idx;
         edtavWarning1_Internalname = "vWARNING1_"+sGXsfl_37_fel_idx;
         edtavErro1_Internalname = "vERRO1_"+sGXsfl_37_fel_idx;
         edtavDescricao1_Internalname = "vDESCRICAO1_"+sGXsfl_37_fel_idx;
      }

      protected void sendrow_372( )
      {
         SubsflControlProps_372( ) ;
         WBQE0( ) ;
         if ( ( subGrid1_Rows * 1 == 0 ) || ( nGXsfl_37_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)((nGXsfl_37_idx) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_37_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavValido1_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV9Valido1_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV9Valido1))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Valido1_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV9Valido1)));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavValido1_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV9Valido1)) ? AV59Valido1_GXI : context.PathToRelativeUrl( AV9Valido1)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavValido1_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV9Valido1_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning1_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV10Warning1_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10Warning1))&&String.IsNullOrEmpty(StringUtil.RTrim( AV60Warning1_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10Warning1)));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning1_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV10Warning1)) ? AV60Warning1_GXI : context.PathToRelativeUrl( AV10Warning1)),(String)edtavWarning1_Link,(String)edtavWarning1_Linktarget,(String)"",context.GetTheme( ),(int)edtavWarning1_Visible,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV10Warning1_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavErro1_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV11Erro1_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11Erro1))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Erro1_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11Erro1)));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavErro1_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11Erro1)) ? AV61Erro1_GXI : context.PathToRelativeUrl( AV11Erro1)),(String)"",(String)edtavErro1_Linktarget,(String)"",context.GetTheme( ),(int)edtavErro1_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV11Erro1_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao1_Internalname,(String)AV6Descricao1,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDescricao1_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDescricao1_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_37_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         /* End function sendrow_372 */
      }

      protected void SubsflControlProps_555( )
      {
         edtavValido3_Internalname = "vVALIDO3_"+sGXsfl_55_idx;
         edtavWarning3_Internalname = "vWARNING3_"+sGXsfl_55_idx;
         edtavErro3_Internalname = "vERRO3_"+sGXsfl_55_idx;
         edtavDescricao3_Internalname = "vDESCRICAO3_"+sGXsfl_55_idx;
      }

      protected void SubsflControlProps_fel_555( )
      {
         edtavValido3_Internalname = "vVALIDO3_"+sGXsfl_55_fel_idx;
         edtavWarning3_Internalname = "vWARNING3_"+sGXsfl_55_fel_idx;
         edtavErro3_Internalname = "vERRO3_"+sGXsfl_55_fel_idx;
         edtavDescricao3_Internalname = "vDESCRICAO3_"+sGXsfl_55_fel_idx;
      }

      protected void sendrow_555( )
      {
         SubsflControlProps_555( ) ;
         WBQE0( ) ;
         if ( ( subGrid3_Rows * 1 == 0 ) || ( nGXsfl_55_idx <= subGrid3_Recordsperpage( ) * 1 ) )
         {
            Grid3Row = GXWebRow.GetNew(context,Grid3Container);
            if ( subGrid3_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid3_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid3_Class, "") != 0 )
               {
                  subGrid3_Linesclass = subGrid3_Class+"Odd";
               }
            }
            else if ( subGrid3_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid3_Backstyle = 0;
               subGrid3_Backcolor = subGrid3_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid3_Class, "") != 0 )
               {
                  subGrid3_Linesclass = subGrid3_Class+"Uniform";
               }
            }
            else if ( subGrid3_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid3_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid3_Class, "") != 0 )
               {
                  subGrid3_Linesclass = subGrid3_Class+"Odd";
               }
               subGrid3_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid3_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid3_Backstyle = 1;
               if ( ((int)((nGXsfl_55_idx) % (2))) == 0 )
               {
                  subGrid3_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid3_Class, "") != 0 )
                  {
                     subGrid3_Linesclass = subGrid3_Class+"Even";
                  }
               }
               else
               {
                  subGrid3_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid3_Class, "") != 0 )
                  {
                     subGrid3_Linesclass = subGrid3_Class+"Odd";
                  }
               }
            }
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid3_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_55_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavValido3_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV21Valido3_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV21Valido3))&&String.IsNullOrEmpty(StringUtil.RTrim( AV53Valido3_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV21Valido3)));
            Grid3Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavValido3_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV21Valido3)) ? AV53Valido3_GXI : context.PathToRelativeUrl( AV21Valido3)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavValido3_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV21Valido3_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning3_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV22Warning3_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV22Warning3))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Warning3_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV22Warning3)));
            Grid3Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning3_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV22Warning3)) ? AV54Warning3_GXI : context.PathToRelativeUrl( AV22Warning3)),(String)edtavWarning3_Link,(String)edtavWarning3_Linktarget,(String)"",context.GetTheme( ),(int)edtavWarning3_Visible,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV22Warning3_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavErro3_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV23Erro3_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV23Erro3))&&String.IsNullOrEmpty(StringUtil.RTrim( AV55Erro3_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV23Erro3)));
            Grid3Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavErro3_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV23Erro3)) ? AV55Erro3_GXI : context.PathToRelativeUrl( AV23Erro3)),(String)"",(String)edtavErro3_Linktarget,(String)"",context.GetTheme( ),(int)edtavErro3_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV23Erro3_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid3Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Grid3Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao3_Internalname,(String)AV24Descricao3,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDescricao3_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDescricao3_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
            Grid3Container.AddRow(Grid3Row);
            nGXsfl_55_idx = (short)(((subGrid3_Islastpage==1)&&(nGXsfl_55_idx+1>subGrid3_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_555( ) ;
         }
         /* End function sendrow_555 */
      }

      protected void SubsflControlProps_733( )
      {
         edtavValido2_Internalname = "vVALIDO2_"+sGXsfl_73_idx;
         edtavWarning2_Internalname = "vWARNING2_"+sGXsfl_73_idx;
         edtavErro2_Internalname = "vERRO2_"+sGXsfl_73_idx;
         edtavDescricao2_Internalname = "vDESCRICAO2_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_733( )
      {
         edtavValido2_Internalname = "vVALIDO2_"+sGXsfl_73_fel_idx;
         edtavWarning2_Internalname = "vWARNING2_"+sGXsfl_73_fel_idx;
         edtavErro2_Internalname = "vERRO2_"+sGXsfl_73_fel_idx;
         edtavDescricao2_Internalname = "vDESCRICAO2_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_733( )
      {
         SubsflControlProps_733( ) ;
         WBQE0( ) ;
         if ( ( subGrid2_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid2_Recordsperpage( ) * 1 ) )
         {
            Grid2Row = GXWebRow.GetNew(context,Grid2Container);
            if ( subGrid2_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid2_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
            else if ( subGrid2_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid2_Backstyle = 0;
               subGrid2_Backcolor = subGrid2_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Uniform";
               }
            }
            else if ( subGrid2_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid2_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
               subGrid2_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid2_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid2_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid2_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
                  {
                     subGrid2_Linesclass = subGrid2_Class+"Even";
                  }
               }
               else
               {
                  subGrid2_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
                  {
                     subGrid2_Linesclass = subGrid2_Class+"Odd";
                  }
               }
            }
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid2_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavValido2_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV17Valido2_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Valido2))&&String.IsNullOrEmpty(StringUtil.RTrim( AV49Valido2_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Valido2)));
            Grid2Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavValido2_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Valido2)) ? AV49Valido2_GXI : context.PathToRelativeUrl( AV17Valido2)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavValido2_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV17Valido2_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning2_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV18Warning2_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18Warning2))&&String.IsNullOrEmpty(StringUtil.RTrim( AV50Warning2_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18Warning2)));
            Grid2Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning2_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV18Warning2)) ? AV50Warning2_GXI : context.PathToRelativeUrl( AV18Warning2)),(String)edtavWarning2_Link,(String)edtavWarning2_Linktarget,(String)"",context.GetTheme( ),(int)edtavWarning2_Visible,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV18Warning2_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavErro2_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV19Erro2_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Erro2))&&String.IsNullOrEmpty(StringUtil.RTrim( AV51Erro2_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Erro2)));
            Grid2Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavErro2_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Erro2)) ? AV51Erro2_GXI : context.PathToRelativeUrl( AV19Erro2)),(String)"",(String)edtavErro2_Linktarget,(String)"",context.GetTheme( ),(int)edtavErro2_Visible,(short)0,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV19Erro2_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao2_Internalname,(String)AV20Descricao2,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDescricao2_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDescricao2_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
            Grid2Container.AddRow(Grid2Row);
            nGXsfl_73_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_733( ) ;
         }
         /* End function sendrow_733 */
      }

      protected void init_default_properties( )
      {
         lblTbconfiguracoes_Internalname = "TBCONFIGURACOES";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         lblTb1_Internalname = "TB1";
         imgValido1_Internalname = "VALIDO1";
         imgWarning1_Internalname = "WARNING1";
         imgError1_Internalname = "ERROR1";
         edtavValido1_Internalname = "vVALIDO1";
         edtavWarning1_Internalname = "vWARNING1";
         edtavErro1_Internalname = "vERRO1";
         edtavDescricao1_Internalname = "vDESCRICAO1";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTb3_Internalname = "TB3";
         imgValido3_Internalname = "VALIDO3";
         imgWarning3_Internalname = "WARNING3";
         imgError3_Internalname = "ERROR3";
         edtavValido3_Internalname = "vVALIDO3";
         edtavWarning3_Internalname = "vWARNING3";
         edtavErro3_Internalname = "vERRO3";
         edtavDescricao3_Internalname = "vDESCRICAO3";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         lblTb2_Internalname = "TB2";
         imgValido2_Internalname = "VALIDO2";
         imgWarning2_Internalname = "WARNING2";
         imgError2_Internalname = "ERROR2";
         edtavValido2_Internalname = "vVALIDO2";
         edtavWarning2_Internalname = "vWARNING2";
         edtavErro2_Internalname = "vERRO2";
         edtavDescricao2_Internalname = "vDESCRICAO2";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
         subGrid3_Internalname = "GRID3";
         subGrid2_Internalname = "GRID2";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDescricao2_Jsonclick = "";
         edtavDescricao3_Jsonclick = "";
         edtavDescricao1_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavDescricao1_Enabled = 0;
         edtavErro1_Linktarget = "";
         edtavWarning1_Linktarget = "";
         edtavWarning1_Link = "";
         subGrid1_Class = "WorkWithBorder WorkWith";
         imgWarning1_Bitmap = (String)(context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         imgValido1_Bitmap = (String)(context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         subGrid3_Allowcollapsing = 0;
         subGrid3_Allowselection = 0;
         edtavDescricao3_Enabled = 0;
         edtavErro3_Linktarget = "";
         edtavWarning3_Linktarget = "";
         edtavWarning3_Link = "";
         subGrid3_Class = "WorkWithBorder WorkWith";
         imgWarning3_Bitmap = (String)(context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         imgValido3_Bitmap = (String)(context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         subGrid2_Allowcollapsing = 0;
         subGrid2_Allowselection = 0;
         edtavDescricao2_Enabled = 0;
         edtavErro2_Linktarget = "";
         edtavWarning2_Linktarget = "";
         edtavWarning2_Link = "";
         subGrid2_Class = "WorkWithBorder WorkWith";
         imgWarning2_Bitmap = (String)(context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         imgValido2_Bitmap = (String)(context.GetImagePath( "d7533b3f-bdc5-4479-8e4e-64278af316af", "", context.GetTheme( )));
         edtavErro3_Visible = -1;
         edtavWarning3_Visible = -1;
         edtavValido3_Visible = -1;
         edtavErro2_Visible = -1;
         edtavWarning2_Visible = -1;
         edtavValido2_Visible = -1;
         edtavErro1_Visible = -1;
         edtavWarning1_Visible = -1;
         edtavValido1_Visible = -1;
         lblTbconfiguracoes_Caption = "Configura��es";
         subGrid3_Backcolorstyle = 3;
         subGrid2_Backcolorstyle = 3;
         subGrid1_Backcolorstyle = 3;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Monitoramento do Sistema";
         subGrid2_Rows = 0;
         subGrid3_Rows = 0;
         subGrid1_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV13sp2',fld:'vSP2',pic:'',nv:''},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A576ContratadaUsuario_CstUntPrdNrm',fld:'CONTRATADAUSUARIO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A577ContratadaUsuario_CstUntPrdExt',fld:'CONTRATADAUSUARIO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV32Contrato_Numero',fld:'vCONTRATO_NUMERO',pic:'',nv:''},{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0},{av:'subGrid2_Rows',nv:0},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A532ParametrosSistema_EmailSdaHost',fld:'PARAMETROSSISTEMA_EMAILSDAHOST',pic:'',nv:''},{av:'A537ParametrosSistema_EmailSdaKey',fld:'PARAMETROSSISTEMA_EMAILSDAKEY',pic:'',nv:''},{av:'A534ParametrosSistema_EmailSdaPass',fld:'PARAMETROSSISTEMA_EMAILSDAPASS',pic:'',nv:''},{av:'A536ParametrosSistema_EmailSdaPort',fld:'PARAMETROSSISTEMA_EMAILSDAPORT',pic:'ZZZ9',nv:0},{av:'A533ParametrosSistema_EmailSdaUser',fld:'PARAMETROSSISTEMA_EMAILSDAUSER',pic:'',nv:''},{av:'A1021ParametrosSistema_PathCrtf',fld:'PARAMETROSSISTEMA_PATHCRTF',pic:'',nv:''},{av:'A1499ParametrosSistema_FlsEvd',fld:'PARAMETROSSISTEMA_FLSEVD',pic:'',nv:''},{av:'A1163ParametrosSistema_HostMensuracao',fld:'PARAMETROSSISTEMA_HOSTMENSURACAO',pic:'',nv:''},{av:'AV44DirNome',fld:'vDIRNOME',pic:'@!',nv:''},{av:'AV20Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'GRID3_nFirstRecordOnPage',nv:0},{av:'GRID3_nEOF',nv:0},{av:'subGrid3_Rows',nv:0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Context',fld:'vCONTEXT',pic:'',nv:null},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'AV40Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Count',fld:'vCOUNT',pic:'ZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'AV12sp1',fld:'vSP1',pic:'',nv:''},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A1858ContratoServicos_Alias',fld:'CONTRATOSERVICOS_ALIAS',pic:'@!',nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A1455ContratoServicos_IndiceDivergencia',fld:'CONTRATOSERVICOS_INDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'A453Contrato_IndiceDivergencia',fld:'CONTRATO_INDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV37TpVnc',fld:'vTPVNC',pic:'',nv:''},{av:'A868ContratoServicos_TipoVnc',fld:'CONTRATOSERVICOS_TIPOVNC',pic:'',nv:''},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A907ContratoServicosPrazoRegra_Inicio',fld:'CONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1325ContratoServicos_StatusPagFnc',fld:'CONTRATOSERVICOS_STATUSPAGFNC',pic:'',nv:''},{av:'AV26Printed',fld:'vPRINTED',pic:'',nv:false}],oparms:[{av:'AV5Context',fld:'vCONTEXT',pic:'',nv:null},{av:'lblTbconfiguracoes_Caption',ctrl:'TBCONFIGURACOES',prop:'Caption'},{av:'AV37TpVnc',fld:'vTPVNC',pic:'',nv:''}]}");
         setEventMetadata("GRID2.LOAD","{handler:'E15QE3',iparms:[{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A532ParametrosSistema_EmailSdaHost',fld:'PARAMETROSSISTEMA_EMAILSDAHOST',pic:'',nv:''},{av:'A537ParametrosSistema_EmailSdaKey',fld:'PARAMETROSSISTEMA_EMAILSDAKEY',pic:'',nv:''},{av:'A534ParametrosSistema_EmailSdaPass',fld:'PARAMETROSSISTEMA_EMAILSDAPASS',pic:'',nv:''},{av:'A536ParametrosSistema_EmailSdaPort',fld:'PARAMETROSSISTEMA_EMAILSDAPORT',pic:'ZZZ9',nv:0},{av:'A533ParametrosSistema_EmailSdaUser',fld:'PARAMETROSSISTEMA_EMAILSDAUSER',pic:'',nv:''},{av:'A1021ParametrosSistema_PathCrtf',fld:'PARAMETROSSISTEMA_PATHCRTF',pic:'',nv:''},{av:'A1499ParametrosSistema_FlsEvd',fld:'PARAMETROSSISTEMA_FLSEVD',pic:'',nv:''},{av:'A1163ParametrosSistema_HostMensuracao',fld:'PARAMETROSSISTEMA_HOSTMENSURACAO',pic:'',nv:''},{av:'AV44DirNome',fld:'vDIRNOME',pic:'@!',nv:''},{av:'AV20Descricao2',fld:'vDESCRICAO2',pic:'',nv:''}],oparms:[{av:'AV17Valido2',fld:'vVALIDO2',pic:'',nv:''},{av:'AV18Warning2',fld:'vWARNING2',pic:'',nv:''},{av:'AV19Erro2',fld:'vERRO2',pic:'',nv:''},{av:'edtavWarning2_Linktarget',ctrl:'vWARNING2',prop:'Linktarget'},{av:'edtavErro2_Linktarget',ctrl:'vERRO2',prop:'Linktarget'},{av:'AV20Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'edtavWarning2_Link',ctrl:'vWARNING2',prop:'Link'},{av:'AV44DirNome',fld:'vDIRNOME',pic:'@!',nv:''},{av:'edtavValido1_Visible',ctrl:'vVALIDO1',prop:'Visible'},{av:'edtavWarning1_Visible',ctrl:'vWARNING1',prop:'Visible'},{av:'edtavErro1_Visible',ctrl:'vERRO1',prop:'Visible'},{av:'edtavValido2_Visible',ctrl:'vVALIDO2',prop:'Visible'},{av:'edtavWarning2_Visible',ctrl:'vWARNING2',prop:'Visible'},{av:'edtavErro2_Visible',ctrl:'vERRO2',prop:'Visible'},{av:'edtavValido3_Visible',ctrl:'vVALIDO3',prop:'Visible'},{av:'edtavWarning3_Visible',ctrl:'vWARNING3',prop:'Visible'},{av:'edtavErro3_Visible',ctrl:'vERRO3',prop:'Visible'}]}");
         setEventMetadata("GRID3.LOAD","{handler:'E14QE5',iparms:[{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Context',fld:'vCONTEXT',pic:'',nv:null},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'AV40Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Count',fld:'vCOUNT',pic:'ZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'AV12sp1',fld:'vSP1',pic:'',nv:''},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A1858ContratoServicos_Alias',fld:'CONTRATOSERVICOS_ALIAS',pic:'@!',nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A1455ContratoServicos_IndiceDivergencia',fld:'CONTRATOSERVICOS_INDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'A453Contrato_IndiceDivergencia',fld:'CONTRATO_INDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV37TpVnc',fld:'vTPVNC',pic:'',nv:''},{av:'A868ContratoServicos_TipoVnc',fld:'CONTRATOSERVICOS_TIPOVNC',pic:'',nv:''},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A907ContratoServicosPrazoRegra_Inicio',fld:'CONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1325ContratoServicos_StatusPagFnc',fld:'CONTRATOSERVICOS_STATUSPAGFNC',pic:'',nv:''},{av:'AV26Printed',fld:'vPRINTED',pic:'',nv:false}],oparms:[{av:'AV21Valido3',fld:'vVALIDO3',pic:'',nv:''},{av:'AV22Warning3',fld:'vWARNING3',pic:'',nv:''},{av:'AV23Erro3',fld:'vERRO3',pic:'',nv:''},{av:'edtavWarning3_Linktarget',ctrl:'vWARNING3',prop:'Linktarget'},{av:'edtavErro3_Linktarget',ctrl:'vERRO3',prop:'Linktarget'},{av:'AV40Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV32Contrato_Numero',fld:'vCONTRATO_NUMERO',pic:'',nv:''},{av:'AV26Printed',fld:'vPRINTED',pic:'',nv:false},{av:'edtavWarning3_Link',ctrl:'vWARNING3',prop:'Link'},{av:'AV8Count',fld:'vCOUNT',pic:'ZZZ9',nv:0},{av:'edtavValido1_Visible',ctrl:'vVALIDO1',prop:'Visible'},{av:'edtavWarning1_Visible',ctrl:'vWARNING1',prop:'Visible'},{av:'edtavErro1_Visible',ctrl:'vERRO1',prop:'Visible'},{av:'edtavValido2_Visible',ctrl:'vVALIDO2',prop:'Visible'},{av:'edtavWarning2_Visible',ctrl:'vWARNING2',prop:'Visible'},{av:'edtavErro2_Visible',ctrl:'vERRO2',prop:'Visible'},{av:'edtavValido3_Visible',ctrl:'vVALIDO3',prop:'Visible'},{av:'edtavWarning3_Visible',ctrl:'vWARNING3',prop:'Visible'},{av:'edtavErro3_Visible',ctrl:'vERRO3',prop:'Visible'}]}");
         setEventMetadata("GRID1.LOAD","{handler:'E12QE2',iparms:[{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Context',fld:'vCONTEXT',pic:'',nv:null},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'AV12sp1',fld:'vSP1',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV13sp2',fld:'vSP2',pic:'',nv:''},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A576ContratadaUsuario_CstUntPrdNrm',fld:'CONTRATADAUSUARIO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A577ContratadaUsuario_CstUntPrdExt',fld:'CONTRATADAUSUARIO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A453Contrato_IndiceDivergencia',fld:'CONTRATO_INDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'AV26Printed',fld:'vPRINTED',pic:'',nv:false},{av:'AV32Contrato_Numero',fld:'vCONTRATO_NUMERO',pic:'',nv:''}],oparms:[{av:'AV9Valido1',fld:'vVALIDO1',pic:'',nv:''},{av:'AV10Warning1',fld:'vWARNING1',pic:'',nv:''},{av:'AV11Erro1',fld:'vERRO1',pic:'',nv:''},{av:'edtavWarning1_Linktarget',ctrl:'vWARNING1',prop:'Linktarget'},{av:'edtavErro1_Linktarget',ctrl:'vERRO1',prop:'Linktarget'},{av:'AV6Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV26Printed',fld:'vPRINTED',pic:'',nv:false},{av:'edtavWarning1_Link',ctrl:'vWARNING1',prop:'Link'},{av:'AV8Count',fld:'vCOUNT',pic:'ZZZ9',nv:0},{av:'AV32Contrato_Numero',fld:'vCONTRATO_NUMERO',pic:'',nv:''},{av:'edtavValido1_Visible',ctrl:'vVALIDO1',prop:'Visible'},{av:'edtavWarning1_Visible',ctrl:'vWARNING1',prop:'Visible'},{av:'edtavErro1_Visible',ctrl:'vERRO1',prop:'Visible'},{av:'edtavValido2_Visible',ctrl:'vVALIDO2',prop:'Visible'},{av:'edtavWarning2_Visible',ctrl:'vWARNING2',prop:'Visible'},{av:'edtavErro2_Visible',ctrl:'vERRO2',prop:'Visible'},{av:'edtavValido3_Visible',ctrl:'vVALIDO3',prop:'Visible'},{av:'edtavWarning3_Visible',ctrl:'vWARNING3',prop:'Visible'},{av:'edtavErro3_Visible',ctrl:'vERRO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         A438Contratada_Sigla = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         AV12sp1 = "";
         AV13sp2 = "";
         A77Contrato_Numero = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         AV32Contrato_Numero = "";
         GXKey = "";
         A605Servico_Sigla = "";
         A1858ContratoServicos_Alias = "";
         AV37TpVnc = "";
         A868ContratoServicos_TipoVnc = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A532ParametrosSistema_EmailSdaHost = "";
         A537ParametrosSistema_EmailSdaKey = "";
         A534ParametrosSistema_EmailSdaPass = "";
         A533ParametrosSistema_EmailSdaUser = "";
         A1021ParametrosSistema_PathCrtf = "";
         A1499ParametrosSistema_FlsEvd = "";
         A1163ParametrosSistema_HostMensuracao = "";
         AV44DirNome = "";
         AV20Descricao2 = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9Valido1 = "";
         AV59Valido1_GXI = "";
         AV10Warning1 = "";
         AV60Warning1_GXI = "";
         AV11Erro1 = "";
         AV61Erro1_GXI = "";
         AV6Descricao1 = "";
         AV21Valido3 = "";
         AV53Valido3_GXI = "";
         AV22Warning3 = "";
         AV54Warning3_GXI = "";
         AV23Erro3 = "";
         AV55Erro3_GXI = "";
         AV24Descricao3 = "";
         AV17Valido2 = "";
         AV49Valido2_GXI = "";
         AV18Warning2 = "";
         AV50Warning2_GXI = "";
         AV19Erro2 = "";
         AV51Erro2_GXI = "";
         Grid1Container = new GXWebGrid( context);
         Grid2Container = new GXWebGrid( context);
         Grid3Container = new GXWebGrid( context);
         scmdbuf = "";
         H00QE3_A40000GXC1 = new int[1] ;
         AV14sp3 = "";
         AV15sp4 = "";
         AV16sp5 = "";
         H00QE4_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00QE4_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00QE4_A39Contratada_Codigo = new int[1] ;
         H00QE4_A43Contratada_Ativo = new bool[] {false} ;
         H00QE4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00QE4_A438Contratada_Sigla = new String[] {""} ;
         H00QE4_A605Servico_Sigla = new String[] {""} ;
         H00QE4_n605Servico_Sigla = new bool[] {false} ;
         Grid1Row = new GXWebRow();
         H00QE5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00QE5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00QE5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00QE5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00QE5_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00QE5_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00QE5_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         H00QE5_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         H00QE5_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         H00QE5_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         H00QE5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00QE5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00QE6_A3Perfil_Codigo = new int[1] ;
         H00QE6_A1Usuario_Codigo = new int[1] ;
         H00QE7_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00QE7_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00QE8_AV8Count = new short[1] ;
         H00QE9_A74Contrato_Codigo = new int[1] ;
         H00QE9_n74Contrato_Codigo = new bool[] {false} ;
         H00QE9_A39Contratada_Codigo = new int[1] ;
         H00QE9_A92Contrato_Ativo = new bool[] {false} ;
         H00QE12_A39Contratada_Codigo = new int[1] ;
         H00QE12_A54Usuario_Ativo = new bool[] {false} ;
         H00QE12_n54Usuario_Ativo = new bool[] {false} ;
         H00QE12_A74Contrato_Codigo = new int[1] ;
         H00QE12_n74Contrato_Codigo = new bool[] {false} ;
         H00QE12_A92Contrato_Ativo = new bool[] {false} ;
         H00QE12_A1013Contrato_PrepostoCod = new int[1] ;
         H00QE12_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00QE12_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00QE12_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H00QE12_A77Contrato_Numero = new String[] {""} ;
         H00QE12_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00QE12_n843Contrato_DataFimTA = new bool[] {false} ;
         H00QE12_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         A843Contrato_DataFimTA = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         H00QE13_A160ContratoServicos_Codigo = new int[1] ;
         H00QE13_A74Contrato_Codigo = new int[1] ;
         H00QE13_n74Contrato_Codigo = new bool[] {false} ;
         H00QE13_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00QE14_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00QE14_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00QE14_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00QE14_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00QE15_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00QE15_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00QE15_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00QE15_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         Grid3Row = new GXWebRow();
         H00QE17_A40000GXC1 = new int[1] ;
         AV42Directory = new GxDirectory(context.GetPhysicalPath());
         AV46FileNome = "";
         AV41File = new GxFile(context.GetPhysicalPath());
         Grid2Row = new GXWebRow();
         H00QE18_A330ParametrosSistema_Codigo = new int[1] ;
         H00QE18_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         H00QE18_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         H00QE18_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         H00QE18_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         H00QE18_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         H00QE18_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         H00QE18_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         H00QE18_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         H00QE18_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         H00QE18_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         H00QE18_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         H00QE18_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         H00QE18_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         H00QE18_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         H00QE18_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         H00QE18_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         H00QE19_A155Servico_Codigo = new int[1] ;
         H00QE19_A1013Contrato_PrepostoCod = new int[1] ;
         H00QE19_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00QE19_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00QE19_A54Usuario_Ativo = new bool[] {false} ;
         H00QE19_n54Usuario_Ativo = new bool[] {false} ;
         H00QE19_A160ContratoServicos_Codigo = new int[1] ;
         H00QE19_A43Contratada_Ativo = new bool[] {false} ;
         H00QE19_A92Contrato_Ativo = new bool[] {false} ;
         H00QE19_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00QE19_A77Contrato_Numero = new String[] {""} ;
         H00QE19_A438Contratada_Sigla = new String[] {""} ;
         H00QE19_A605Servico_Sigla = new String[] {""} ;
         H00QE19_n605Servico_Sigla = new bool[] {false} ;
         H00QE19_A1858ContratoServicos_Alias = new String[] {""} ;
         H00QE19_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00QE19_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00QE19_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         H00QE19_A558Servico_Percentual = new decimal[1] ;
         H00QE19_n558Servico_Percentual = new bool[] {false} ;
         H00QE19_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H00QE19_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         H00QE19_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         H00QE19_A868ContratoServicos_TipoVnc = new String[] {""} ;
         H00QE19_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         H00QE19_A157ServicoGrupo_Codigo = new int[1] ;
         H00QE19_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         H00QE19_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         H00QE19_A74Contrato_Codigo = new int[1] ;
         H00QE19_n74Contrato_Codigo = new bool[] {false} ;
         H00QE19_A39Contratada_Codigo = new int[1] ;
         AV31ontratada_Sigla = "";
         AV33Servico_Sigla = "";
         H00QE20_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         H00QE20_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00QE20_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         H00QE21_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00QE21_A828UsuarioServicos_UsuarioCod = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTb2_Jsonclick = "";
         subGrid2_Linesclass = "";
         Grid2Column = new GXWebColumn();
         lblTb3_Jsonclick = "";
         subGrid3_Linesclass = "";
         Grid3Column = new GXWebColumn();
         lblTb1_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         lblTbconfiguracoes_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_monitoramento__default(),
            new Object[][] {
                new Object[] {
               H00QE3_A40000GXC1
               }
               , new Object[] {
               H00QE4_A1595Contratada_AreaTrbSrvPdr, H00QE4_n1595Contratada_AreaTrbSrvPdr, H00QE4_A39Contratada_Codigo, H00QE4_A43Contratada_Ativo, H00QE4_A52Contratada_AreaTrabalhoCod, H00QE4_A438Contratada_Sigla, H00QE4_A605Servico_Sigla, H00QE4_n605Servico_Sigla
               }
               , new Object[] {
               H00QE5_A70ContratadaUsuario_UsuarioPessoaCod, H00QE5_n70ContratadaUsuario_UsuarioPessoaCod, H00QE5_A66ContratadaUsuario_ContratadaCod, H00QE5_A69ContratadaUsuario_UsuarioCod, H00QE5_A1394ContratadaUsuario_UsuarioAtivo, H00QE5_n1394ContratadaUsuario_UsuarioAtivo, H00QE5_A577ContratadaUsuario_CstUntPrdExt, H00QE5_n577ContratadaUsuario_CstUntPrdExt, H00QE5_A576ContratadaUsuario_CstUntPrdNrm, H00QE5_n576ContratadaUsuario_CstUntPrdNrm,
               H00QE5_A71ContratadaUsuario_UsuarioPessoaNom, H00QE5_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00QE6_A3Perfil_Codigo, H00QE6_A1Usuario_Codigo
               }
               , new Object[] {
               H00QE7_A829UsuarioServicos_ServicoCod, H00QE7_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               H00QE8_AV8Count
               }
               , new Object[] {
               H00QE9_A74Contrato_Codigo, H00QE9_A39Contratada_Codigo, H00QE9_A92Contrato_Ativo
               }
               , new Object[] {
               H00QE12_A39Contratada_Codigo, H00QE12_A54Usuario_Ativo, H00QE12_n54Usuario_Ativo, H00QE12_A74Contrato_Codigo, H00QE12_A92Contrato_Ativo, H00QE12_A1013Contrato_PrepostoCod, H00QE12_n1013Contrato_PrepostoCod, H00QE12_A116Contrato_ValorUnidadeContratacao, H00QE12_A453Contrato_IndiceDivergencia, H00QE12_A77Contrato_Numero,
               H00QE12_A843Contrato_DataFimTA, H00QE12_n843Contrato_DataFimTA, H00QE12_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00QE13_A160ContratoServicos_Codigo, H00QE13_A74Contrato_Codigo, H00QE13_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00QE14_A1078ContratoGestor_ContratoCod, H00QE14_A1079ContratoGestor_UsuarioCod, H00QE14_A1446ContratoGestor_ContratadaAreaCod, H00QE14_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00QE15_A1078ContratoGestor_ContratoCod, H00QE15_A1079ContratoGestor_UsuarioCod, H00QE15_A1446ContratoGestor_ContratadaAreaCod, H00QE15_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00QE17_A40000GXC1
               }
               , new Object[] {
               H00QE18_A330ParametrosSistema_Codigo, H00QE18_A533ParametrosSistema_EmailSdaUser, H00QE18_n533ParametrosSistema_EmailSdaUser, H00QE18_A536ParametrosSistema_EmailSdaPort, H00QE18_n536ParametrosSistema_EmailSdaPort, H00QE18_A534ParametrosSistema_EmailSdaPass, H00QE18_n534ParametrosSistema_EmailSdaPass, H00QE18_A537ParametrosSistema_EmailSdaKey, H00QE18_n537ParametrosSistema_EmailSdaKey, H00QE18_A532ParametrosSistema_EmailSdaHost,
               H00QE18_n532ParametrosSistema_EmailSdaHost, H00QE18_A1021ParametrosSistema_PathCrtf, H00QE18_n1021ParametrosSistema_PathCrtf, H00QE18_A1499ParametrosSistema_FlsEvd, H00QE18_n1499ParametrosSistema_FlsEvd, H00QE18_A1163ParametrosSistema_HostMensuracao, H00QE18_n1163ParametrosSistema_HostMensuracao
               }
               , new Object[] {
               H00QE19_A155Servico_Codigo, H00QE19_A1013Contrato_PrepostoCod, H00QE19_n1013Contrato_PrepostoCod, H00QE19_A75Contrato_AreaTrabalhoCod, H00QE19_A54Usuario_Ativo, H00QE19_n54Usuario_Ativo, H00QE19_A160ContratoServicos_Codigo, H00QE19_A43Contratada_Ativo, H00QE19_A92Contrato_Ativo, H00QE19_A638ContratoServicos_Ativo,
               H00QE19_A77Contrato_Numero, H00QE19_A438Contratada_Sigla, H00QE19_A605Servico_Sigla, H00QE19_A1858ContratoServicos_Alias, H00QE19_n1858ContratoServicos_Alias, H00QE19_A116Contrato_ValorUnidadeContratacao, H00QE19_A557Servico_VlrUnidadeContratada, H00QE19_A558Servico_Percentual, H00QE19_n558Servico_Percentual, H00QE19_A453Contrato_IndiceDivergencia,
               H00QE19_A1455ContratoServicos_IndiceDivergencia, H00QE19_n1455ContratoServicos_IndiceDivergencia, H00QE19_A868ContratoServicos_TipoVnc, H00QE19_n868ContratoServicos_TipoVnc, H00QE19_A157ServicoGrupo_Codigo, H00QE19_A1325ContratoServicos_StatusPagFnc, H00QE19_n1325ContratoServicos_StatusPagFnc, H00QE19_A74Contrato_Codigo, H00QE19_A39Contratada_Codigo
               }
               , new Object[] {
               H00QE20_A906ContratoServicosPrazoRegra_Sequencial, H00QE20_A903ContratoServicosPrazo_CntSrvCod, H00QE20_A907ContratoServicosPrazoRegra_Inicio
               }
               , new Object[] {
               H00QE21_A829UsuarioServicos_ServicoCod, H00QE21_A828UsuarioServicos_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDescricao1_Enabled = 0;
         edtavDescricao3_Enabled = 0;
         edtavDescricao2_Enabled = 0;
      }

      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_15 ;
      private short nIsMod_15 ;
      private short nRcdExists_18 ;
      private short nIsMod_18 ;
      private short nRcdExists_17 ;
      private short nIsMod_17 ;
      private short nRcdExists_16 ;
      private short nIsMod_16 ;
      private short nRcdExists_14 ;
      private short nIsMod_14 ;
      private short nRcdExists_13 ;
      private short nIsMod_13 ;
      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_12 ;
      private short nIsMod_12 ;
      private short nRcdExists_11 ;
      private short nIsMod_11 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_37 ;
      private short nGXsfl_37_idx=1 ;
      private short nRC_GXsfl_55 ;
      private short nGXsfl_55_idx=1 ;
      private short AV8Count ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short A536ParametrosSistema_EmailSdaPort ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID1_nEOF ;
      private short GRID3_nEOF ;
      private short GRID2_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_37_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid2_Backcolorstyle ;
      private short nGXsfl_55_Refreshing=0 ;
      private short subGrid3_Backcolorstyle ;
      private short AV62GXLvl229 ;
      private short AV63GXLvl235 ;
      private short AV64GXLvl241 ;
      private short AV65GXLvl252 ;
      private short cV8Count ;
      private short AV68GXLvl296 ;
      private short AV69GXLvl334 ;
      private short AV70GXLvl344 ;
      private short AV71GXLvl355 ;
      private short AV52GXLvl32 ;
      private short AV56GXLvl90 ;
      private short AV57GXLvl175 ;
      private short AV58GXLvl190 ;
      private short subGrid2_Titlebackstyle ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short subGrid3_Titlebackstyle ;
      private short subGrid3_Allowselection ;
      private short subGrid3_Allowhovering ;
      private short subGrid3_Allowcollapsing ;
      private short subGrid3_Collapsed ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short subGrid1_Backstyle ;
      private short subGrid3_Backstyle ;
      private short subGrid2_Backstyle ;
      private int subGrid1_Rows ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1Usuario_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A39Contratada_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A74Contrato_Codigo ;
      private int subGrid3_Rows ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int AV40Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int A157ServicoGrupo_Codigo ;
      private int subGrid2_Rows ;
      private int A330ParametrosSistema_Codigo ;
      private int A40000GXC1 ;
      private int subGrid1_Islastpage ;
      private int subGrid2_Islastpage ;
      private int subGrid3_Islastpage ;
      private int edtavDescricao1_Enabled ;
      private int edtavDescricao3_Enabled ;
      private int edtavDescricao2_Enabled ;
      private int GRID1_nGridOutOfScope ;
      private int GRID2_nGridOutOfScope ;
      private int GRID3_nGridOutOfScope ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int edtavValido1_Visible ;
      private int edtavWarning1_Visible ;
      private int edtavErro1_Visible ;
      private int edtavValido2_Visible ;
      private int edtavWarning2_Visible ;
      private int edtavErro2_Visible ;
      private int edtavValido3_Visible ;
      private int edtavWarning3_Visible ;
      private int edtavErro3_Visible ;
      private int A155Servico_Codigo ;
      private int subGrid2_Titlebackcolor ;
      private int subGrid2_Allbackcolor ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int subGrid3_Titlebackcolor ;
      private int subGrid3_Allbackcolor ;
      private int subGrid3_Selectioncolor ;
      private int subGrid3_Hoveringcolor ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int subGrid3_Backcolor ;
      private int subGrid2_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID3_nFirstRecordOnPage ;
      private long GRID2_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID3_nCurrentRecord ;
      private long GRID2_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private long GRID2_nRecordCount ;
      private long GRID3_nRecordCount ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A558Servico_Percentual ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal AV35min ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_37_idx="0001" ;
      private String A438Contratada_Sigla ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String AV12sp1 ;
      private String AV13sp2 ;
      private String A77Contrato_Numero ;
      private String AV32Contrato_Numero ;
      private String GXKey ;
      private String sGXsfl_55_idx="0001" ;
      private String A605Servico_Sigla ;
      private String A1858ContratoServicos_Alias ;
      private String AV37TpVnc ;
      private String A868ContratoServicos_TipoVnc ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String sGXsfl_73_idx="0001" ;
      private String A537ParametrosSistema_EmailSdaKey ;
      private String AV44DirNome ;
      private String edtavDescricao2_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavValido1_Internalname ;
      private String edtavWarning1_Internalname ;
      private String edtavErro1_Internalname ;
      private String edtavDescricao1_Internalname ;
      private String edtavValido3_Internalname ;
      private String edtavWarning3_Internalname ;
      private String edtavErro3_Internalname ;
      private String edtavDescricao3_Internalname ;
      private String edtavValido2_Internalname ;
      private String edtavWarning2_Internalname ;
      private String edtavErro2_Internalname ;
      private String scmdbuf ;
      private String AV14sp3 ;
      private String AV15sp4 ;
      private String AV16sp5 ;
      private String edtavWarning1_Linktarget ;
      private String edtavErro1_Linktarget ;
      private String edtavWarning1_Link ;
      private String lblTbconfiguracoes_Caption ;
      private String lblTbconfiguracoes_Internalname ;
      private String AV46FileNome ;
      private String edtavWarning2_Linktarget ;
      private String edtavErro2_Linktarget ;
      private String edtavWarning2_Link ;
      private String edtavWarning3_Linktarget ;
      private String edtavErro3_Linktarget ;
      private String AV31ontratada_Sigla ;
      private String AV33Servico_Sigla ;
      private String edtavWarning3_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTb2_Internalname ;
      private String lblTb2_Jsonclick ;
      private String imgValido2_Internalname ;
      private String imgWarning2_Internalname ;
      private String imgError2_Internalname ;
      private String subGrid2_Internalname ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTb3_Internalname ;
      private String lblTb3_Jsonclick ;
      private String imgValido3_Internalname ;
      private String imgWarning3_Internalname ;
      private String imgError3_Internalname ;
      private String subGrid3_Internalname ;
      private String subGrid3_Class ;
      private String subGrid3_Linesclass ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTb1_Internalname ;
      private String lblTb1_Jsonclick ;
      private String imgValido1_Internalname ;
      private String imgWarning1_Internalname ;
      private String imgError1_Internalname ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTbconfiguracoes_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_37_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavDescricao1_Jsonclick ;
      private String sGXsfl_55_fel_idx="0001" ;
      private String edtavDescricao3_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String edtavDescricao2_Jsonclick ;
      private DateTime A1869Contrato_DataTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private bool entryPointCalled ;
      private bool A43Contratada_Ativo ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool A92Contrato_Ativo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n74Contrato_Codigo ;
      private bool A638ContratoServicos_Ativo ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool AV26Printed ;
      private bool n605Servico_Sigla ;
      private bool n1858ContratoServicos_Alias ;
      private bool n558Servico_Percentual ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n532ParametrosSistema_EmailSdaHost ;
      private bool n537ParametrosSistema_EmailSdaKey ;
      private bool n534ParametrosSistema_EmailSdaPass ;
      private bool n536ParametrosSistema_EmailSdaPort ;
      private bool n533ParametrosSistema_EmailSdaUser ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool n1499ParametrosSistema_FlsEvd ;
      private bool n1163ParametrosSistema_HostMensuracao ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n843Contrato_DataFimTA ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool GXt_boolean1 ;
      private bool gx_refresh_fired ;
      private bool AV9Valido1_IsBlob ;
      private bool AV10Warning1_IsBlob ;
      private bool AV11Erro1_IsBlob ;
      private bool AV21Valido3_IsBlob ;
      private bool AV22Warning3_IsBlob ;
      private bool AV23Erro3_IsBlob ;
      private bool AV17Valido2_IsBlob ;
      private bool AV18Warning2_IsBlob ;
      private bool AV19Erro2_IsBlob ;
      private String A532ParametrosSistema_EmailSdaHost ;
      private String A534ParametrosSistema_EmailSdaPass ;
      private String A533ParametrosSistema_EmailSdaUser ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String A1499ParametrosSistema_FlsEvd ;
      private String A1163ParametrosSistema_HostMensuracao ;
      private String AV20Descricao2 ;
      private String AV59Valido1_GXI ;
      private String AV60Warning1_GXI ;
      private String AV61Erro1_GXI ;
      private String AV6Descricao1 ;
      private String AV53Valido3_GXI ;
      private String AV54Warning3_GXI ;
      private String AV55Erro3_GXI ;
      private String AV24Descricao3 ;
      private String AV49Valido2_GXI ;
      private String AV50Warning2_GXI ;
      private String AV51Erro2_GXI ;
      private String AV9Valido1 ;
      private String AV10Warning1 ;
      private String AV11Erro1 ;
      private String AV21Valido3 ;
      private String AV22Warning3 ;
      private String AV23Erro3 ;
      private String AV17Valido2 ;
      private String AV18Warning2 ;
      private String AV19Erro2 ;
      private String imgValido2_Bitmap ;
      private String imgWarning2_Bitmap ;
      private String imgValido3_Bitmap ;
      private String imgWarning3_Bitmap ;
      private String imgValido1_Bitmap ;
      private String imgWarning1_Bitmap ;
      private GXWebGrid Grid1Container ;
      private GXWebGrid Grid2Container ;
      private GXWebGrid Grid3Container ;
      private GXWebRow Grid1Row ;
      private GXWebRow Grid3Row ;
      private GXWebRow Grid2Row ;
      private GXWebColumn Grid2Column ;
      private GXWebColumn Grid3Column ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00QE3_A40000GXC1 ;
      private int[] H00QE4_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00QE4_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00QE4_A39Contratada_Codigo ;
      private bool[] H00QE4_A43Contratada_Ativo ;
      private int[] H00QE4_A52Contratada_AreaTrabalhoCod ;
      private String[] H00QE4_A438Contratada_Sigla ;
      private String[] H00QE4_A605Servico_Sigla ;
      private bool[] H00QE4_n605Servico_Sigla ;
      private int[] H00QE5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00QE5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00QE5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00QE5_A69ContratadaUsuario_UsuarioCod ;
      private bool[] H00QE5_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00QE5_n1394ContratadaUsuario_UsuarioAtivo ;
      private decimal[] H00QE5_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] H00QE5_n577ContratadaUsuario_CstUntPrdExt ;
      private decimal[] H00QE5_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] H00QE5_n576ContratadaUsuario_CstUntPrdNrm ;
      private String[] H00QE5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00QE5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00QE6_A3Perfil_Codigo ;
      private int[] H00QE6_A1Usuario_Codigo ;
      private int[] H00QE7_A829UsuarioServicos_ServicoCod ;
      private int[] H00QE7_A828UsuarioServicos_UsuarioCod ;
      private short[] H00QE8_AV8Count ;
      private int[] H00QE9_A74Contrato_Codigo ;
      private bool[] H00QE9_n74Contrato_Codigo ;
      private int[] H00QE9_A39Contratada_Codigo ;
      private bool[] H00QE9_A92Contrato_Ativo ;
      private int[] H00QE12_A39Contratada_Codigo ;
      private bool[] H00QE12_A54Usuario_Ativo ;
      private bool[] H00QE12_n54Usuario_Ativo ;
      private int[] H00QE12_A74Contrato_Codigo ;
      private bool[] H00QE12_n74Contrato_Codigo ;
      private bool[] H00QE12_A92Contrato_Ativo ;
      private int[] H00QE12_A1013Contrato_PrepostoCod ;
      private bool[] H00QE12_n1013Contrato_PrepostoCod ;
      private decimal[] H00QE12_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] H00QE12_A453Contrato_IndiceDivergencia ;
      private String[] H00QE12_A77Contrato_Numero ;
      private DateTime[] H00QE12_A843Contrato_DataFimTA ;
      private bool[] H00QE12_n843Contrato_DataFimTA ;
      private DateTime[] H00QE12_A83Contrato_DataVigenciaTermino ;
      private int[] H00QE13_A160ContratoServicos_Codigo ;
      private int[] H00QE13_A74Contrato_Codigo ;
      private bool[] H00QE13_n74Contrato_Codigo ;
      private bool[] H00QE13_A638ContratoServicos_Ativo ;
      private int[] H00QE14_A1078ContratoGestor_ContratoCod ;
      private int[] H00QE14_A1079ContratoGestor_UsuarioCod ;
      private int[] H00QE14_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00QE14_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00QE15_A1078ContratoGestor_ContratoCod ;
      private int[] H00QE15_A1079ContratoGestor_UsuarioCod ;
      private int[] H00QE15_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00QE15_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00QE17_A40000GXC1 ;
      private int[] H00QE18_A330ParametrosSistema_Codigo ;
      private String[] H00QE18_A533ParametrosSistema_EmailSdaUser ;
      private bool[] H00QE18_n533ParametrosSistema_EmailSdaUser ;
      private short[] H00QE18_A536ParametrosSistema_EmailSdaPort ;
      private bool[] H00QE18_n536ParametrosSistema_EmailSdaPort ;
      private String[] H00QE18_A534ParametrosSistema_EmailSdaPass ;
      private bool[] H00QE18_n534ParametrosSistema_EmailSdaPass ;
      private String[] H00QE18_A537ParametrosSistema_EmailSdaKey ;
      private bool[] H00QE18_n537ParametrosSistema_EmailSdaKey ;
      private String[] H00QE18_A532ParametrosSistema_EmailSdaHost ;
      private bool[] H00QE18_n532ParametrosSistema_EmailSdaHost ;
      private String[] H00QE18_A1021ParametrosSistema_PathCrtf ;
      private bool[] H00QE18_n1021ParametrosSistema_PathCrtf ;
      private String[] H00QE18_A1499ParametrosSistema_FlsEvd ;
      private bool[] H00QE18_n1499ParametrosSistema_FlsEvd ;
      private String[] H00QE18_A1163ParametrosSistema_HostMensuracao ;
      private bool[] H00QE18_n1163ParametrosSistema_HostMensuracao ;
      private int[] H00QE19_A155Servico_Codigo ;
      private int[] H00QE19_A1013Contrato_PrepostoCod ;
      private bool[] H00QE19_n1013Contrato_PrepostoCod ;
      private int[] H00QE19_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00QE19_A54Usuario_Ativo ;
      private bool[] H00QE19_n54Usuario_Ativo ;
      private int[] H00QE19_A160ContratoServicos_Codigo ;
      private bool[] H00QE19_A43Contratada_Ativo ;
      private bool[] H00QE19_A92Contrato_Ativo ;
      private bool[] H00QE19_A638ContratoServicos_Ativo ;
      private String[] H00QE19_A77Contrato_Numero ;
      private String[] H00QE19_A438Contratada_Sigla ;
      private String[] H00QE19_A605Servico_Sigla ;
      private bool[] H00QE19_n605Servico_Sigla ;
      private String[] H00QE19_A1858ContratoServicos_Alias ;
      private bool[] H00QE19_n1858ContratoServicos_Alias ;
      private decimal[] H00QE19_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] H00QE19_A557Servico_VlrUnidadeContratada ;
      private decimal[] H00QE19_A558Servico_Percentual ;
      private bool[] H00QE19_n558Servico_Percentual ;
      private decimal[] H00QE19_A453Contrato_IndiceDivergencia ;
      private decimal[] H00QE19_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] H00QE19_n1455ContratoServicos_IndiceDivergencia ;
      private String[] H00QE19_A868ContratoServicos_TipoVnc ;
      private bool[] H00QE19_n868ContratoServicos_TipoVnc ;
      private int[] H00QE19_A157ServicoGrupo_Codigo ;
      private String[] H00QE19_A1325ContratoServicos_StatusPagFnc ;
      private bool[] H00QE19_n1325ContratoServicos_StatusPagFnc ;
      private int[] H00QE19_A74Contrato_Codigo ;
      private bool[] H00QE19_n74Contrato_Codigo ;
      private int[] H00QE19_A39Contratada_Codigo ;
      private short[] H00QE20_A906ContratoServicosPrazoRegra_Sequencial ;
      private int[] H00QE20_A903ContratoServicosPrazo_CntSrvCod ;
      private decimal[] H00QE20_A907ContratoServicosPrazoRegra_Inicio ;
      private int[] H00QE21_A829UsuarioServicos_ServicoCod ;
      private int[] H00QE21_A828UsuarioServicos_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxFile AV41File ;
      private GxDirectory AV42Directory ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
   }

   public class wp_monitoramento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QE3 ;
          prmH00QE3 = new Object[] {
          new Object[] {"@AV40Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE4 ;
          prmH00QE4 = new Object[] {
          new Object[] {"@AV5Conte_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE5 ;
          prmH00QE5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE6 ;
          prmH00QE6 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE7 ;
          prmH00QE7 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE8 ;
          prmH00QE8 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE9 ;
          prmH00QE9 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE12 ;
          prmH00QE12 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE13 ;
          prmH00QE13 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE14 ;
          prmH00QE14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmH00QE15 ;
          prmH00QE15 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmH00QE17 ;
          prmH00QE17 = new Object[] {
          new Object[] {"@AV40Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE18 ;
          prmH00QE18 = new Object[] {
          } ;
          Object[] prmH00QE19 ;
          prmH00QE19 = new Object[] {
          new Object[] {"@AV5Conte_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE20 ;
          prmH00QE20 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QE21 ;
          prmH00QE21 = new Object[] {
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QE3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV40Contrato_Codigo) AND ([ContratoServicos_Ativo] = 0) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE3,1,0,true,true )
             ,new CursorDef("H00QE4", "SELECT T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T1.[Contratada_Codigo], T1.[Contratada_Ativo], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Sigla], T3.[Servico_Sigla] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[AreaTrabalho_ServicoPadrao]) WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV5Conte_1Areatrabalho_codigo) AND (T1.[Contratada_Ativo] = 1) ORDER BY T1.[Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE4,100,0,true,false )
             ,new CursorDef("H00QE5", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_CstUntPrdExt], T1.[ContratadaUsuario_CstUntPrdNrm], T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @Contratada_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE5,100,0,true,false )
             ,new CursorDef("H00QE6", "SELECT TOP 1 [Perfil_Codigo], [Usuario_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE6,1,0,false,true )
             ,new CursorDef("H00QE7", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY [UsuarioServicos_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE7,1,0,false,true )
             ,new CursorDef("H00QE8", "SELECT COUNT(*) FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @Contratada_Codigo) AND (Not [Contrato_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE8,1,0,true,false )
             ,new CursorDef("H00QE9", "SELECT TOP 1 [Contrato_Codigo], [Contratada_Codigo], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @Contratada_Codigo) AND ([Contrato_Ativo] = 1) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE9,1,0,true,true )
             ,new CursorDef("H00QE12", "SELECT T1.[Contratada_Codigo], T3.[Usuario_Ativo], T1.[Contrato_Codigo], T1.[Contrato_Ativo], T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[Contrato_ValorUnidadeContratacao], T1.[Contrato_IndiceDivergencia], T1.[Contrato_Numero], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM (([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T4.[ContratoTermoAditivo_DataFim], T4.[Contrato_Codigo], T4.[ContratoTermoAditivo_Codigo], T5.[GXC6] AS GXC6 FROM ([ContratoTermoAditivo] T4 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE T4.[ContratoTermoAditivo_Codigo] = T5.[GXC6] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) WHERE (T1.[Contratada_Codigo] = @Contratada_Codigo) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE12,100,0,true,false )
             ,new CursorDef("H00QE13", "SELECT TOP 1 [ContratoServicos_Codigo], [Contrato_Codigo], [ContratoServicos_Ativo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([ContratoServicos_Ativo] = 1) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE13,1,0,false,true )
             ,new CursorDef("H00QE14", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (@Usuario_Ativo = 1) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE14,100,0,true,false )
             ,new CursorDef("H00QE15", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (@Usuario_Ativo = 1) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE15,100,0,true,false )
             ,new CursorDef("H00QE17", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV40Contrato_Codigo) AND ([ContratoServicos_Ativo] = 0) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE17,1,0,true,false )
             ,new CursorDef("H00QE18", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_PathCrtf], [ParametrosSistema_FlsEvd], [ParametrosSistema_HostMensuracao] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE18,1,0,true,true )
             ,new CursorDef("H00QE19", "SELECT T1.[Servico_Codigo], T3.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T3.[Contrato_AreaTrabalhoCod], T4.[Usuario_Ativo], T1.[ContratoServicos_Codigo], T5.[Contratada_Ativo], T3.[Contrato_Ativo], T1.[ContratoServicos_Ativo], T3.[Contrato_Numero], T5.[Contratada_Sigla], T2.[Servico_Sigla], T1.[ContratoServicos_Alias], T3.[Contrato_ValorUnidadeContratacao], T1.[Servico_VlrUnidadeContratada], T1.[Servico_Percentual], T3.[Contrato_IndiceDivergencia], T1.[ContratoServicos_IndiceDivergencia], T1.[ContratoServicos_TipoVnc], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_StatusPagFnc], T1.[Contrato_Codigo], T3.[Contratada_Codigo] FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) INNER JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T3.[Contratada_Codigo]) WHERE (T3.[Contrato_AreaTrabalhoCod] = @AV5Conte_1Areatrabalho_codigo) AND (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contrato_Ativo] = 1) AND (T5.[Contratada_Ativo] = 1) ORDER BY T3.[Contrato_AreaTrabalhoCod], T3.[Contratada_Codigo], T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE19,100,0,true,false )
             ,new CursorDef("H00QE20", "SELECT [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Inicio] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE20,100,0,false,false )
             ,new CursorDef("H00QE21", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE @Usuario_Ativo = 1 ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QE21,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 32) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.getBool(8) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
                ((String[]) buf[11])[0] = rslt.getString(10, 15) ;
                ((String[]) buf[12])[0] = rslt.getString(11, 15) ;
                ((String[]) buf[13])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(12);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(13) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[17])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(15);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(16) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(17);
                ((String[]) buf[22])[0] = rslt.getString(18, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(18);
                ((int[]) buf[24])[0] = rslt.getInt(19) ;
                ((String[]) buf[25])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(20);
                ((int[]) buf[27])[0] = rslt.getInt(21) ;
                ((int[]) buf[28])[0] = rslt.getInt(22) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                return;
       }
    }

 }

}
