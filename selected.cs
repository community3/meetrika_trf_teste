/*
               File: Selected
        Description: Linhas selecionadas numa grid
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:2:4.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class selected : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkSelected_Flag.Name = "SELECTED_FLAG";
         chkSelected_Flag.WebTags = "";
         chkSelected_Flag.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSelected_Flag_Internalname, "TitleCaption", chkSelected_Flag.Caption);
         chkSelected_Flag.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Linhas selecionadas numa grid", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtUsuario_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public selected( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public selected( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkSelected_Flag = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1W71( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1W71e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1W71( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1W71( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1W71e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Linhas selecionadas numa grid", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Selected.htm");
            wb_table3_28_1W71( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1W71e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1W71e( true) ;
         }
         else
         {
            wb_table1_2_1W71e( false) ;
         }
      }

      protected void wb_table3_28_1W71( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1W71( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1W71e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Selected.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Selected.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1W71e( true) ;
         }
         else
         {
            wb_table3_28_1W71e( false) ;
         }
      }

      protected void wb_table4_34_1W71( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_codigo_Internalname, "C�digo", "", "", lblTextblockusuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")), ((edtUsuario_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuario_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockselected_codigo_Internalname, "da linha", "", "", lblTextblockselected_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSelected_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A487Selected_Codigo), 6, 0, ",", "")), ((edtSelected_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A487Selected_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A487Selected_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSelected_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSelected_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockselected_flag_Internalname, "Flag", "", "", lblTextblockselected_flag_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Selected.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkSelected_Flag_Internalname, StringUtil.BoolToStr( A488Selected_Flag), "", "", 1, chkSelected_Flag.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(49, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1W71e( true) ;
         }
         else
         {
            wb_table4_34_1W71e( false) ;
         }
      }

      protected void wb_table2_5_1W71( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Selected.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1W71e( true) ;
         }
         else
         {
            wb_table2_5_1W71e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1Usuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               else
               {
                  A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSelected_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSelected_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SELECTED_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSelected_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A487Selected_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
               }
               else
               {
                  A487Selected_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSelected_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
               }
               A488Selected_Flag = StringUtil.StrToBool( cgiGet( chkSelected_Flag_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
               /* Read saved values. */
               Z1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1Usuario_Codigo"), ",", "."));
               Z487Selected_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z487Selected_Codigo"), ",", "."));
               Z488Selected_Flag = StringUtil.StrToBool( cgiGet( "Z488Selected_Flag"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A487Selected_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
                  A488Selected_Flag = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1W71( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1W71( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1W0( )
      {
      }

      protected void ZM1W71( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z487Selected_Codigo = A487Selected_Codigo;
            Z488Selected_Flag = A488Selected_Flag;
            Z1Usuario_Codigo = A1Usuario_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1W71( )
      {
         /* Using cursor T001W5 */
         pr_default.execute(3, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound71 = 1;
            ZM1W71( -1) ;
         }
         pr_default.close(3);
         OnLoadActions1W71( ) ;
      }

      protected void OnLoadActions1W71( )
      {
      }

      protected void CheckExtendedTable1W71( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001W4 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1W71( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A1Usuario_Codigo )
      {
         /* Using cursor T001W6 */
         pr_default.execute(4, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey1W71( )
      {
         /* Using cursor T001W7 */
         pr_default.execute(5, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound71 = 1;
         }
         else
         {
            RcdFound71 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001W3 */
         pr_default.execute(1, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1W71( 1) ;
            RcdFound71 = 1;
            A487Selected_Codigo = T001W3_A487Selected_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
            A488Selected_Flag = T001W3_A488Selected_Flag[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
            A1Usuario_Codigo = T001W3_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z487Selected_Codigo = A487Selected_Codigo;
            Z488Selected_Flag = A488Selected_Flag;
            sMode71 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1W71( ) ;
            if ( AnyError == 1 )
            {
               RcdFound71 = 0;
               InitializeNonKey1W71( ) ;
            }
            Gx_mode = sMode71;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound71 = 0;
            InitializeNonKey1W71( ) ;
            sMode71 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode71;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1W71( ) ;
         if ( RcdFound71 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound71 = 0;
         /* Using cursor T001W8 */
         pr_default.execute(6, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T001W8_A1Usuario_Codigo[0] < A1Usuario_Codigo ) || ( T001W8_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T001W8_A487Selected_Codigo[0] < A487Selected_Codigo ) || ( T001W8_A487Selected_Codigo[0] == A487Selected_Codigo ) && ( T001W8_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( String.Compare(StringUtil.BoolToStr(T001W8_A488Selected_Flag[0]), StringUtil.BoolToStr(A488Selected_Flag)) < 0 ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T001W8_A1Usuario_Codigo[0] > A1Usuario_Codigo ) || ( T001W8_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T001W8_A487Selected_Codigo[0] > A487Selected_Codigo ) || ( T001W8_A487Selected_Codigo[0] == A487Selected_Codigo ) && ( T001W8_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( String.Compare(StringUtil.BoolToStr(T001W8_A488Selected_Flag[0]), StringUtil.BoolToStr(A488Selected_Flag)) > 0 ) ) )
            {
               A1Usuario_Codigo = T001W8_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A487Selected_Codigo = T001W8_A487Selected_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
               A488Selected_Flag = T001W8_A488Selected_Flag[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
               RcdFound71 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound71 = 0;
         /* Using cursor T001W9 */
         pr_default.execute(7, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001W9_A1Usuario_Codigo[0] > A1Usuario_Codigo ) || ( T001W9_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T001W9_A487Selected_Codigo[0] > A487Selected_Codigo ) || ( T001W9_A487Selected_Codigo[0] == A487Selected_Codigo ) && ( T001W9_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( String.Compare(StringUtil.BoolToStr(T001W9_A488Selected_Flag[0]), StringUtil.BoolToStr(A488Selected_Flag)) > 0 ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001W9_A1Usuario_Codigo[0] < A1Usuario_Codigo ) || ( T001W9_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T001W9_A487Selected_Codigo[0] < A487Selected_Codigo ) || ( T001W9_A487Selected_Codigo[0] == A487Selected_Codigo ) && ( T001W9_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( String.Compare(StringUtil.BoolToStr(T001W9_A488Selected_Flag[0]), StringUtil.BoolToStr(A488Selected_Flag)) < 0 ) ) )
            {
               A1Usuario_Codigo = T001W9_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A487Selected_Codigo = T001W9_A487Selected_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
               A488Selected_Flag = T001W9_A488Selected_Flag[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
               RcdFound71 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1W71( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1W71( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound71 == 1 )
            {
               if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A487Selected_Codigo != Z487Selected_Codigo ) || ( A488Selected_Flag != Z488Selected_Flag ) )
               {
                  A1Usuario_Codigo = Z1Usuario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A487Selected_Codigo = Z487Selected_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
                  A488Selected_Flag = Z488Selected_Flag;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1W71( ) ;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A487Selected_Codigo != Z487Selected_Codigo ) || ( A488Selected_Flag != Z488Selected_Flag ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1W71( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1W71( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A487Selected_Codigo != Z487Selected_Codigo ) || ( A488Selected_Flag != Z488Selected_Flag ) )
         {
            A1Usuario_Codigo = Z1Usuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A487Selected_Codigo = Z487Selected_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
            A488Selected_Flag = Z488Selected_Flag;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound71 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1W71( ) ;
         if ( RcdFound71 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1W71( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound71 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound71 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1W71( ) ;
         if ( RcdFound71 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound71 != 0 )
            {
               ScanNext1W71( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1W71( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1W71( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001W2 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Selected"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Selected"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1W71( )
      {
         BeforeValidate1W71( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1W71( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1W71( 0) ;
            CheckOptimisticConcurrency1W71( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1W71( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1W71( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001W10 */
                     pr_default.execute(8, new Object[] {A487Selected_Codigo, A488Selected_Flag, A1Usuario_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Selected") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1W0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1W71( ) ;
            }
            EndLevel1W71( ) ;
         }
         CloseExtendedTableCursors1W71( ) ;
      }

      protected void Update1W71( )
      {
         BeforeValidate1W71( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1W71( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1W71( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1W71( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1W71( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [Selected] */
                     DeferredUpdate1W71( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1W0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1W71( ) ;
         }
         CloseExtendedTableCursors1W71( ) ;
      }

      protected void DeferredUpdate1W71( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1W71( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1W71( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1W71( ) ;
            AfterConfirm1W71( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1W71( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001W11 */
                  pr_default.execute(9, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("Selected") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound71 == 0 )
                        {
                           InitAll1W71( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1W0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode71 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1W71( ) ;
         Gx_mode = sMode71;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1W71( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1W71( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1W71( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Selected");
            if ( AnyError == 0 )
            {
               ConfirmValues1W0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Selected");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1W71( )
      {
         /* Using cursor T001W12 */
         pr_default.execute(10);
         RcdFound71 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound71 = 1;
            A1Usuario_Codigo = T001W12_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A487Selected_Codigo = T001W12_A487Selected_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
            A488Selected_Flag = T001W12_A488Selected_Flag[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1W71( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound71 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound71 = 1;
            A1Usuario_Codigo = T001W12_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A487Selected_Codigo = T001W12_A487Selected_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
            A488Selected_Flag = T001W12_A488Selected_Flag[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
         }
      }

      protected void ScanEnd1W71( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1W71( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1W71( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1W71( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1W71( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1W71( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1W71( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1W71( )
      {
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         edtSelected_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSelected_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSelected_Codigo_Enabled), 5, 0)));
         chkSelected_Flag.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSelected_Flag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkSelected_Flag.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1W0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428232518");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("selected.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z487Selected_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z487Selected_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z488Selected_Flag", Z488Selected_Flag);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("selected.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Selected" ;
      }

      public override String GetPgmdesc( )
      {
         return "Linhas selecionadas numa grid" ;
      }

      protected void InitializeNonKey1W71( )
      {
      }

      protected void InitAll1W71( )
      {
         A1Usuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A487Selected_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
         A488Selected_Flag = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A488Selected_Flag", A488Selected_Flag);
         InitializeNonKey1W71( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428232522");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("selected.js", "?2020428232522");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockusuario_codigo_Internalname = "TEXTBLOCKUSUARIO_CODIGO";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         lblTextblockselected_codigo_Internalname = "TEXTBLOCKSELECTED_CODIGO";
         edtSelected_Codigo_Internalname = "SELECTED_CODIGO";
         lblTextblockselected_flag_Internalname = "TEXTBLOCKSELECTED_FLAG";
         chkSelected_Flag_Internalname = "SELECTED_FLAG";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Linhas selecionadas numa grid";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         chkSelected_Flag.Enabled = 1;
         edtSelected_Codigo_Jsonclick = "";
         edtSelected_Codigo_Enabled = 1;
         edtUsuario_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkSelected_Flag.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001W13 */
         pr_default.execute(11, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(11);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Usuario_codigo( int GX_Parm1 )
      {
         A1Usuario_Codigo = GX_Parm1;
         /* Using cursor T001W13 */
         pr_default.execute(11, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
         }
         pr_default.close(11);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Selected_flag( int GX_Parm1 ,
                                       int GX_Parm2 ,
                                       bool GX_Parm3 )
      {
         A1Usuario_Codigo = GX_Parm1;
         A487Selected_Codigo = GX_Parm2;
         A488Selected_Flag = GX_Parm3;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z487Selected_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z488Selected_Flag);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockusuario_codigo_Jsonclick = "";
         lblTextblockselected_codigo_Jsonclick = "";
         lblTextblockselected_flag_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T001W5_A487Selected_Codigo = new int[1] ;
         T001W5_A488Selected_Flag = new bool[] {false} ;
         T001W5_A1Usuario_Codigo = new int[1] ;
         T001W4_A1Usuario_Codigo = new int[1] ;
         T001W6_A1Usuario_Codigo = new int[1] ;
         T001W7_A1Usuario_Codigo = new int[1] ;
         T001W7_A487Selected_Codigo = new int[1] ;
         T001W7_A488Selected_Flag = new bool[] {false} ;
         T001W3_A487Selected_Codigo = new int[1] ;
         T001W3_A488Selected_Flag = new bool[] {false} ;
         T001W3_A1Usuario_Codigo = new int[1] ;
         sMode71 = "";
         T001W8_A1Usuario_Codigo = new int[1] ;
         T001W8_A487Selected_Codigo = new int[1] ;
         T001W8_A488Selected_Flag = new bool[] {false} ;
         T001W9_A1Usuario_Codigo = new int[1] ;
         T001W9_A487Selected_Codigo = new int[1] ;
         T001W9_A488Selected_Flag = new bool[] {false} ;
         T001W2_A487Selected_Codigo = new int[1] ;
         T001W2_A488Selected_Flag = new bool[] {false} ;
         T001W2_A1Usuario_Codigo = new int[1] ;
         T001W12_A1Usuario_Codigo = new int[1] ;
         T001W12_A487Selected_Codigo = new int[1] ;
         T001W12_A488Selected_Flag = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T001W13_A1Usuario_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.selected__default(),
            new Object[][] {
                new Object[] {
               T001W2_A487Selected_Codigo, T001W2_A488Selected_Flag, T001W2_A1Usuario_Codigo
               }
               , new Object[] {
               T001W3_A487Selected_Codigo, T001W3_A488Selected_Flag, T001W3_A1Usuario_Codigo
               }
               , new Object[] {
               T001W4_A1Usuario_Codigo
               }
               , new Object[] {
               T001W5_A487Selected_Codigo, T001W5_A488Selected_Flag, T001W5_A1Usuario_Codigo
               }
               , new Object[] {
               T001W6_A1Usuario_Codigo
               }
               , new Object[] {
               T001W7_A1Usuario_Codigo, T001W7_A487Selected_Codigo, T001W7_A488Selected_Flag
               }
               , new Object[] {
               T001W8_A1Usuario_Codigo, T001W8_A487Selected_Codigo, T001W8_A488Selected_Flag
               }
               , new Object[] {
               T001W9_A1Usuario_Codigo, T001W9_A487Selected_Codigo, T001W9_A488Selected_Flag
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001W12_A1Usuario_Codigo, T001W12_A487Selected_Codigo, T001W12_A488Selected_Flag
               }
               , new Object[] {
               T001W13_A1Usuario_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound71 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1Usuario_Codigo ;
      private int Z487Selected_Codigo ;
      private int A1Usuario_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtUsuario_Codigo_Enabled ;
      private int A487Selected_Codigo ;
      private int edtSelected_Codigo_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkSelected_Flag_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtUsuario_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockusuario_codigo_Internalname ;
      private String lblTextblockusuario_codigo_Jsonclick ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String lblTextblockselected_codigo_Internalname ;
      private String lblTextblockselected_codigo_Jsonclick ;
      private String edtSelected_Codigo_Internalname ;
      private String edtSelected_Codigo_Jsonclick ;
      private String lblTextblockselected_flag_Internalname ;
      private String lblTextblockselected_flag_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode71 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool Z488Selected_Flag ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A488Selected_Flag ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkSelected_Flag ;
      private IDataStoreProvider pr_default ;
      private int[] T001W5_A487Selected_Codigo ;
      private bool[] T001W5_A488Selected_Flag ;
      private int[] T001W5_A1Usuario_Codigo ;
      private int[] T001W4_A1Usuario_Codigo ;
      private int[] T001W6_A1Usuario_Codigo ;
      private int[] T001W7_A1Usuario_Codigo ;
      private int[] T001W7_A487Selected_Codigo ;
      private bool[] T001W7_A488Selected_Flag ;
      private int[] T001W3_A487Selected_Codigo ;
      private bool[] T001W3_A488Selected_Flag ;
      private int[] T001W3_A1Usuario_Codigo ;
      private int[] T001W8_A1Usuario_Codigo ;
      private int[] T001W8_A487Selected_Codigo ;
      private bool[] T001W8_A488Selected_Flag ;
      private int[] T001W9_A1Usuario_Codigo ;
      private int[] T001W9_A487Selected_Codigo ;
      private bool[] T001W9_A488Selected_Flag ;
      private int[] T001W2_A487Selected_Codigo ;
      private bool[] T001W2_A488Selected_Flag ;
      private int[] T001W2_A1Usuario_Codigo ;
      private int[] T001W12_A1Usuario_Codigo ;
      private int[] T001W12_A487Selected_Codigo ;
      private bool[] T001W12_A488Selected_Flag ;
      private int[] T001W13_A1Usuario_Codigo ;
      private GXWebForm Form ;
   }

   public class selected__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001W5 ;
          prmT001W5 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W4 ;
          prmT001W4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001W6 ;
          prmT001W6 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001W7 ;
          prmT001W7 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W3 ;
          prmT001W3 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W8 ;
          prmT001W8 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W9 ;
          prmT001W9 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W2 ;
          prmT001W2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W10 ;
          prmT001W10 = new Object[] {
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001W11 ;
          prmT001W11 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT001W12 ;
          prmT001W12 = new Object[] {
          } ;
          Object[] prmT001W13 ;
          prmT001W13 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001W2", "SELECT [Selected_Codigo], [Selected_Flag], [Usuario_Codigo] FROM [Selected] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Selected_Codigo] = @Selected_Codigo AND [Selected_Flag] = @Selected_Flag ",true, GxErrorMask.GX_NOMASK, false, this,prmT001W2,1,0,true,false )
             ,new CursorDef("T001W3", "SELECT [Selected_Codigo], [Selected_Flag], [Usuario_Codigo] FROM [Selected] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Selected_Codigo] = @Selected_Codigo AND [Selected_Flag] = @Selected_Flag ",true, GxErrorMask.GX_NOMASK, false, this,prmT001W3,1,0,true,false )
             ,new CursorDef("T001W4", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001W4,1,0,true,false )
             ,new CursorDef("T001W5", "SELECT TM1.[Selected_Codigo], TM1.[Selected_Flag], TM1.[Usuario_Codigo] FROM [Selected] TM1 WITH (NOLOCK) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo and TM1.[Selected_Codigo] = @Selected_Codigo and TM1.[Selected_Flag] = @Selected_Flag ORDER BY TM1.[Usuario_Codigo], TM1.[Selected_Codigo], TM1.[Selected_Flag]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001W5,100,0,true,false )
             ,new CursorDef("T001W6", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001W6,1,0,true,false )
             ,new CursorDef("T001W7", "SELECT [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Selected_Codigo] = @Selected_Codigo AND [Selected_Flag] = @Selected_Flag  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001W7,1,0,true,false )
             ,new CursorDef("T001W8", "SELECT TOP 1 [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) WHERE ( [Usuario_Codigo] > @Usuario_Codigo or [Usuario_Codigo] = @Usuario_Codigo and [Selected_Codigo] > @Selected_Codigo or [Selected_Codigo] = @Selected_Codigo and [Usuario_Codigo] = @Usuario_Codigo and [Selected_Flag] > @Selected_Flag) ORDER BY [Usuario_Codigo], [Selected_Codigo], [Selected_Flag]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001W8,1,0,true,true )
             ,new CursorDef("T001W9", "SELECT TOP 1 [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) WHERE ( [Usuario_Codigo] < @Usuario_Codigo or [Usuario_Codigo] = @Usuario_Codigo and [Selected_Codigo] < @Selected_Codigo or [Selected_Codigo] = @Selected_Codigo and [Usuario_Codigo] = @Usuario_Codigo and [Selected_Flag] < @Selected_Flag) ORDER BY [Usuario_Codigo] DESC, [Selected_Codigo] DESC, [Selected_Flag] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001W9,1,0,true,true )
             ,new CursorDef("T001W10", "INSERT INTO [Selected]([Selected_Codigo], [Selected_Flag], [Usuario_Codigo]) VALUES(@Selected_Codigo, @Selected_Flag, @Usuario_Codigo)", GxErrorMask.GX_NOMASK,prmT001W10)
             ,new CursorDef("T001W11", "DELETE FROM [Selected]  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Selected_Codigo] = @Selected_Codigo AND [Selected_Flag] = @Selected_Flag", GxErrorMask.GX_NOMASK,prmT001W11)
             ,new CursorDef("T001W12", "SELECT [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) ORDER BY [Usuario_Codigo], [Selected_Codigo], [Selected_Flag]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001W12,100,0,true,false )
             ,new CursorDef("T001W13", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001W13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
