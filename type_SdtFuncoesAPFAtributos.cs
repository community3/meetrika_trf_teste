/*
               File: type_SdtFuncoesAPFAtributos
        Description: Atributos da Fun��o de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:48:44.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "FuncoesAPFAtributos" )]
   [XmlType(TypeName =  "FuncoesAPFAtributos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtFuncoesAPFAtributos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFuncoesAPFAtributos( )
      {
         /* Constructor for serialization */
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao = "";
         gxTv_SdtFuncoesAPFAtributos_Mode = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z = "";
      }

      public SdtFuncoesAPFAtributos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV165FuncaoAPF_Codigo ,
                        int AV364FuncaoAPFAtributos_AtributosCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV165FuncaoAPF_Codigo,(int)AV364FuncaoAPFAtributos_AtributosCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"FuncaoAPF_Codigo", typeof(int)}, new Object[]{"FuncaoAPFAtributos_AtributosCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "FuncoesAPFAtributos");
         metadata.Set("BT", "FuncoesAPFAtributos");
         metadata.Set("PK", "[ \"FuncaoAPF_Codigo\",\"FuncaoAPFAtributos_AtributosCod\" ]");
         metadata.Set("PKAssigned", "[ \"FuncaoAPFAtributos_AtributosCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Atributos_Codigo\" ],\"FKMap\":[ \"FuncaoAPFAtributos_AtributosCod-Atributos_Codigo\" ] },{ \"FK\":[ \"Atributos_Codigo\" ],\"FKMap\":[ \"FuncaoAPFAtributos_MelhoraCod-Atributos_Codigo\" ] },{ \"FK\":[ \"FuncaoAPF_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"FuncaoDados_Codigo\" ],\"FKMap\":[ \"FuncaoAPFAtributos_FuncaoDadosCod-FuncaoDados_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapf_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapf_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atributoscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atributosnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadoscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadosnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadostip_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atrtabelacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atrtabelanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_regra_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_code_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_melhoracod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atributosnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadoscod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadosnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_funcaodadostip_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atrtabelacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_atrtabelanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_sistemacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_regra_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_code_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcoesapfatributos_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_melhoracod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaoapfatributos_ativo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtFuncoesAPFAtributos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtFuncoesAPFAtributos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtFuncoesAPFAtributos obj ;
         obj = this;
         obj.gxTpr_Funcaoapf_codigo = deserialized.gxTpr_Funcaoapf_codigo;
         obj.gxTpr_Funcaoapf_nome = deserialized.gxTpr_Funcaoapf_nome;
         obj.gxTpr_Funcaoapfatributos_atributoscod = deserialized.gxTpr_Funcaoapfatributos_atributoscod;
         obj.gxTpr_Funcaoapfatributos_atributosnom = deserialized.gxTpr_Funcaoapfatributos_atributosnom;
         obj.gxTpr_Funcaoapfatributos_funcaodadoscod = deserialized.gxTpr_Funcaoapfatributos_funcaodadoscod;
         obj.gxTpr_Funcaoapfatributos_funcaodadosnom = deserialized.gxTpr_Funcaoapfatributos_funcaodadosnom;
         obj.gxTpr_Funcaoapfatributos_funcaodadostip = deserialized.gxTpr_Funcaoapfatributos_funcaodadostip;
         obj.gxTpr_Funcaoapfatributos_atrtabelacod = deserialized.gxTpr_Funcaoapfatributos_atrtabelacod;
         obj.gxTpr_Funcaoapfatributos_atrtabelanom = deserialized.gxTpr_Funcaoapfatributos_atrtabelanom;
         obj.gxTpr_Funcaoapfatributos_sistemacod = deserialized.gxTpr_Funcaoapfatributos_sistemacod;
         obj.gxTpr_Funcoesapfatributos_regra = deserialized.gxTpr_Funcoesapfatributos_regra;
         obj.gxTpr_Funcoesapfatributos_code = deserialized.gxTpr_Funcoesapfatributos_code;
         obj.gxTpr_Funcoesapfatributos_nome = deserialized.gxTpr_Funcoesapfatributos_nome;
         obj.gxTpr_Funcoesapfatributos_descricao = deserialized.gxTpr_Funcoesapfatributos_descricao;
         obj.gxTpr_Funcaoapfatributos_melhoracod = deserialized.gxTpr_Funcaoapfatributos_melhoracod;
         obj.gxTpr_Funcaoapfatributos_ativo = deserialized.gxTpr_Funcaoapfatributos_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Funcaoapf_codigo_Z = deserialized.gxTpr_Funcaoapf_codigo_Z;
         obj.gxTpr_Funcaoapf_nome_Z = deserialized.gxTpr_Funcaoapf_nome_Z;
         obj.gxTpr_Funcaoapfatributos_atributoscod_Z = deserialized.gxTpr_Funcaoapfatributos_atributoscod_Z;
         obj.gxTpr_Funcaoapfatributos_atributosnom_Z = deserialized.gxTpr_Funcaoapfatributos_atributosnom_Z;
         obj.gxTpr_Funcaoapfatributos_funcaodadoscod_Z = deserialized.gxTpr_Funcaoapfatributos_funcaodadoscod_Z;
         obj.gxTpr_Funcaoapfatributos_funcaodadosnom_Z = deserialized.gxTpr_Funcaoapfatributos_funcaodadosnom_Z;
         obj.gxTpr_Funcaoapfatributos_funcaodadostip_Z = deserialized.gxTpr_Funcaoapfatributos_funcaodadostip_Z;
         obj.gxTpr_Funcaoapfatributos_atrtabelacod_Z = deserialized.gxTpr_Funcaoapfatributos_atrtabelacod_Z;
         obj.gxTpr_Funcaoapfatributos_atrtabelanom_Z = deserialized.gxTpr_Funcaoapfatributos_atrtabelanom_Z;
         obj.gxTpr_Funcaoapfatributos_sistemacod_Z = deserialized.gxTpr_Funcaoapfatributos_sistemacod_Z;
         obj.gxTpr_Funcoesapfatributos_regra_Z = deserialized.gxTpr_Funcoesapfatributos_regra_Z;
         obj.gxTpr_Funcoesapfatributos_code_Z = deserialized.gxTpr_Funcoesapfatributos_code_Z;
         obj.gxTpr_Funcoesapfatributos_nome_Z = deserialized.gxTpr_Funcoesapfatributos_nome_Z;
         obj.gxTpr_Funcoesapfatributos_descricao_Z = deserialized.gxTpr_Funcoesapfatributos_descricao_Z;
         obj.gxTpr_Funcaoapfatributos_melhoracod_Z = deserialized.gxTpr_Funcaoapfatributos_melhoracod_Z;
         obj.gxTpr_Funcaoapfatributos_ativo_Z = deserialized.gxTpr_Funcaoapfatributos_ativo_Z;
         obj.gxTpr_Funcaoapfatributos_atributosnom_N = deserialized.gxTpr_Funcaoapfatributos_atributosnom_N;
         obj.gxTpr_Funcaoapfatributos_funcaodadoscod_N = deserialized.gxTpr_Funcaoapfatributos_funcaodadoscod_N;
         obj.gxTpr_Funcaoapfatributos_funcaodadosnom_N = deserialized.gxTpr_Funcaoapfatributos_funcaodadosnom_N;
         obj.gxTpr_Funcaoapfatributos_funcaodadostip_N = deserialized.gxTpr_Funcaoapfatributos_funcaodadostip_N;
         obj.gxTpr_Funcaoapfatributos_atrtabelacod_N = deserialized.gxTpr_Funcaoapfatributos_atrtabelacod_N;
         obj.gxTpr_Funcaoapfatributos_atrtabelanom_N = deserialized.gxTpr_Funcaoapfatributos_atrtabelanom_N;
         obj.gxTpr_Funcaoapfatributos_sistemacod_N = deserialized.gxTpr_Funcaoapfatributos_sistemacod_N;
         obj.gxTpr_Funcoesapfatributos_regra_N = deserialized.gxTpr_Funcoesapfatributos_regra_N;
         obj.gxTpr_Funcoesapfatributos_code_N = deserialized.gxTpr_Funcoesapfatributos_code_N;
         obj.gxTpr_Funcoesapfatributos_nome_N = deserialized.gxTpr_Funcoesapfatributos_nome_N;
         obj.gxTpr_Funcoesapfatributos_descricao_N = deserialized.gxTpr_Funcoesapfatributos_descricao_N;
         obj.gxTpr_Funcaoapfatributos_melhoracod_N = deserialized.gxTpr_Funcaoapfatributos_melhoracod_N;
         obj.gxTpr_Funcaoapfatributos_ativo_N = deserialized.gxTpr_Funcaoapfatributos_ativo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPF_Codigo") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPF_Nome") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtributosCod") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtributosNom") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosCod") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosNom") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosTip") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaCod") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaNom") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_SistemaCod") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Regra") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Code") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Nome") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Descricao") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_MelhoraCod") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_Ativo") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPF_Codigo_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPF_Nome_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtributosCod_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtributosNom_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosCod_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosNom_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosTip_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaCod_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaNom_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_SistemaCod_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Regra_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Code_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Nome_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Descricao_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_MelhoraCod_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_Ativo_Z") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtributosNom_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosCod_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosNom_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_FuncaoDadosTip_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaCod_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_AtrTabelaNom_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_SistemaCod_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Regra_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Code_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Nome_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncoesAPFAtributos_Descricao_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_MelhoraCod_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoAPFAtributos_Ativo_N") )
               {
                  gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "FuncoesAPFAtributos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("FuncaoAPF_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPF_Nome", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_AtributosCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_AtributosNom", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosNom", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosTip", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaNom", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncoesAPFAtributos_Regra", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncoesAPFAtributos_Code", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncoesAPFAtributos_Nome", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncoesAPFAtributos_Descricao", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_MelhoraCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("FuncaoAPFAtributos_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPF_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPF_Nome_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtributosCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtributosNom_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosNom_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosTip_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaNom_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Regra_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Code_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Nome_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Descricao_Z", StringUtil.RTrim( gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_MelhoraCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtributosNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_FuncaoDadosTip_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_AtrTabelaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_SistemaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Regra_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Code_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncoesAPFAtributos_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_MelhoraCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("FuncaoAPFAtributos_Ativo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("FuncaoAPF_Codigo", gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo, false);
         AddObjectProperty("FuncaoAPF_Nome", gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome, false);
         AddObjectProperty("FuncaoAPFAtributos_AtributosCod", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod, false);
         AddObjectProperty("FuncaoAPFAtributos_AtributosNom", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom, false);
         AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosCod", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod, false);
         AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosNom", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom, false);
         AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosTip", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip, false);
         AddObjectProperty("FuncaoAPFAtributos_AtrTabelaCod", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod, false);
         AddObjectProperty("FuncaoAPFAtributos_AtrTabelaNom", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom, false);
         AddObjectProperty("FuncaoAPFAtributos_SistemaCod", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod, false);
         AddObjectProperty("FuncoesAPFAtributos_Regra", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra, false);
         AddObjectProperty("FuncoesAPFAtributos_Code", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code, false);
         AddObjectProperty("FuncoesAPFAtributos_Nome", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome, false);
         AddObjectProperty("FuncoesAPFAtributos_Descricao", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao, false);
         AddObjectProperty("FuncaoAPFAtributos_MelhoraCod", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod, false);
         AddObjectProperty("FuncaoAPFAtributos_Ativo", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtFuncoesAPFAtributos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtFuncoesAPFAtributos_Initialized, false);
            AddObjectProperty("FuncaoAPF_Codigo_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z, false);
            AddObjectProperty("FuncaoAPF_Nome_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_AtributosCod_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_AtributosNom_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosCod_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosNom_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosTip_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_AtrTabelaCod_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_AtrTabelaNom_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_SistemaCod_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z, false);
            AddObjectProperty("FuncoesAPFAtributos_Regra_Z", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z, false);
            AddObjectProperty("FuncoesAPFAtributos_Code_Z", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z, false);
            AddObjectProperty("FuncoesAPFAtributos_Nome_Z", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z, false);
            AddObjectProperty("FuncoesAPFAtributos_Descricao_Z", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_MelhoraCod_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_Ativo_Z", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z, false);
            AddObjectProperty("FuncaoAPFAtributos_AtributosNom_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosCod_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosNom_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N, false);
            AddObjectProperty("FuncaoAPFAtributos_FuncaoDadosTip_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N, false);
            AddObjectProperty("FuncaoAPFAtributos_AtrTabelaCod_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N, false);
            AddObjectProperty("FuncaoAPFAtributos_AtrTabelaNom_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N, false);
            AddObjectProperty("FuncaoAPFAtributos_SistemaCod_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N, false);
            AddObjectProperty("FuncoesAPFAtributos_Regra_N", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N, false);
            AddObjectProperty("FuncoesAPFAtributos_Code_N", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N, false);
            AddObjectProperty("FuncoesAPFAtributos_Nome_N", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N, false);
            AddObjectProperty("FuncoesAPFAtributos_Descricao_N", gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N, false);
            AddObjectProperty("FuncaoAPFAtributos_MelhoraCod_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N, false);
            AddObjectProperty("FuncaoAPFAtributos_Ativo_N", gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "FuncaoAPF_Codigo" )]
      [  XmlElement( ElementName = "FuncaoAPF_Codigo"   )]
      public int gxTpr_Funcaoapf_codigo
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo ;
         }

         set {
            if ( gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo != value )
            {
               gxTv_SdtFuncoesAPFAtributos_Mode = "INS";
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z_SetNull( );
            }
            gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoAPF_Nome" )]
      [  XmlElement( ElementName = "FuncaoAPF_Nome"   )]
      public String gxTpr_Funcaoapf_nome
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtributosCod" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtributosCod"   )]
      public int gxTpr_Funcaoapfatributos_atributoscod
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod ;
         }

         set {
            if ( gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod != value )
            {
               gxTv_SdtFuncoesAPFAtributos_Mode = "INS";
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z_SetNull( );
               this.gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z_SetNull( );
            }
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtributosNom" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtributosNom"   )]
      public String gxTpr_Funcaoapfatributos_atributosnom
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod"   )]
      public int gxTpr_Funcaoapfatributos_funcaodadoscod
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom"   )]
      public String gxTpr_Funcaoapfatributos_funcaodadosnom
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip"   )]
      public String gxTpr_Funcaoapfatributos_funcaodadostip
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod"   )]
      public int gxTpr_Funcaoapfatributos_atrtabelacod
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom"   )]
      public String gxTpr_Funcaoapfatributos_atrtabelanom
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_SistemaCod" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_SistemaCod"   )]
      public int gxTpr_Funcaoapfatributos_sistemacod
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Regra" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Regra"   )]
      public bool gxTpr_Funcoesapfatributos_regra
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra = value;
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra = false;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Code" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Code"   )]
      public String gxTpr_Funcoesapfatributos_code
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Nome" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Nome"   )]
      public String gxTpr_Funcoesapfatributos_nome
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Descricao" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Descricao"   )]
      public String gxTpr_Funcoesapfatributos_descricao
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_MelhoraCod" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_MelhoraCod"   )]
      public int gxTpr_Funcaoapfatributos_melhoracod
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_Ativo" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_Ativo"   )]
      public bool gxTpr_Funcaoapfatributos_ativo
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N = 0;
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo = value;
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N = 1;
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo = false;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Mode ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Mode_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Initialized ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Initialized_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPF_Codigo_Z" )]
      [  XmlElement( ElementName = "FuncaoAPF_Codigo_Z"   )]
      public int gxTpr_Funcaoapf_codigo_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPF_Nome_Z" )]
      [  XmlElement( ElementName = "FuncaoAPF_Nome_Z"   )]
      public String gxTpr_Funcaoapf_nome_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtributosCod_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtributosCod_Z"   )]
      public int gxTpr_Funcaoapfatributos_atributoscod_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtributosNom_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtributosNom_Z"   )]
      public String gxTpr_Funcaoapfatributos_atributosnom_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod_Z"   )]
      public int gxTpr_Funcaoapfatributos_funcaodadoscod_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom_Z"   )]
      public String gxTpr_Funcaoapfatributos_funcaodadosnom_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip_Z"   )]
      public String gxTpr_Funcaoapfatributos_funcaodadostip_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod_Z"   )]
      public int gxTpr_Funcaoapfatributos_atrtabelacod_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom_Z"   )]
      public String gxTpr_Funcaoapfatributos_atrtabelanom_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_SistemaCod_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_SistemaCod_Z"   )]
      public int gxTpr_Funcaoapfatributos_sistemacod_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Regra_Z" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Regra_Z"   )]
      public bool gxTpr_Funcoesapfatributos_regra_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z = value;
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z = false;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Code_Z" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Code_Z"   )]
      public String gxTpr_Funcoesapfatributos_code_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Nome_Z" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Nome_Z"   )]
      public String gxTpr_Funcoesapfatributos_nome_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Descricao_Z" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Descricao_Z"   )]
      public String gxTpr_Funcoesapfatributos_descricao_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_MelhoraCod_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_MelhoraCod_Z"   )]
      public int gxTpr_Funcaoapfatributos_melhoracod_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_Ativo_Z" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_Ativo_Z"   )]
      public bool gxTpr_Funcaoapfatributos_ativo_Z
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z = value;
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtributosNom_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtributosNom_N"   )]
      public short gxTpr_Funcaoapfatributos_atributosnom_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosCod_N"   )]
      public short gxTpr_Funcaoapfatributos_funcaodadoscod_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosNom_N"   )]
      public short gxTpr_Funcaoapfatributos_funcaodadosnom_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_FuncaoDadosTip_N"   )]
      public short gxTpr_Funcaoapfatributos_funcaodadostip_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaCod_N"   )]
      public short gxTpr_Funcaoapfatributos_atrtabelacod_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_AtrTabelaNom_N"   )]
      public short gxTpr_Funcaoapfatributos_atrtabelanom_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_SistemaCod_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_SistemaCod_N"   )]
      public short gxTpr_Funcaoapfatributos_sistemacod_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Regra_N" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Regra_N"   )]
      public short gxTpr_Funcoesapfatributos_regra_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Code_N" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Code_N"   )]
      public short gxTpr_Funcoesapfatributos_code_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Nome_N" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Nome_N"   )]
      public short gxTpr_Funcoesapfatributos_nome_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncoesAPFAtributos_Descricao_N" )]
      [  XmlElement( ElementName = "FuncoesAPFAtributos_Descricao_N"   )]
      public short gxTpr_Funcoesapfatributos_descricao_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_MelhoraCod_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_MelhoraCod_N"   )]
      public short gxTpr_Funcaoapfatributos_melhoracod_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoAPFAtributos_Ativo_N" )]
      [  XmlElement( ElementName = "FuncaoAPFAtributos_Ativo_N"   )]
      public short gxTpr_Funcaoapfatributos_ativo_N
      {
         get {
            return gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N ;
         }

         set {
            gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N = (short)(value);
         }

      }

      public void gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N_SetNull( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N = 0;
         return  ;
      }

      public bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao = "";
         gxTv_SdtFuncoesAPFAtributos_Mode = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z = "";
         gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "funcoesapfatributos", "GeneXus.Programs.funcoesapfatributos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtFuncoesAPFAtributos_Initialized ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_N ;
      private short gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapf_codigo_Z ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributoscod_Z ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadoscod_Z ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelacod_Z ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_sistemacod_Z ;
      private int gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_melhoracod_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome ;
      private String gxTv_SdtFuncoesAPFAtributos_Mode ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atributosnom_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadostip_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_atrtabelanom_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_nome_Z ;
      private String sTagName ;
      private bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra ;
      private bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo ;
      private bool gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_regra_Z ;
      private bool gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_ativo_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapf_nome_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcaoapfatributos_funcaodadosnom_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_code_Z ;
      private String gxTv_SdtFuncoesAPFAtributos_Funcoesapfatributos_descricao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"FuncoesAPFAtributos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtFuncoesAPFAtributos_RESTInterface : GxGenericCollectionItem<SdtFuncoesAPFAtributos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFuncoesAPFAtributos_RESTInterface( ) : base()
      {
      }

      public SdtFuncoesAPFAtributos_RESTInterface( SdtFuncoesAPFAtributos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "FuncaoAPF_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapf_codigo
      {
         get {
            return sdt.gxTpr_Funcaoapf_codigo ;
         }

         set {
            sdt.gxTpr_Funcaoapf_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoAPF_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Funcaoapf_nome
      {
         get {
            return sdt.gxTpr_Funcaoapf_nome ;
         }

         set {
            sdt.gxTpr_Funcaoapf_nome = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_AtributosCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapfatributos_atributoscod
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_atributoscod ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_atributoscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_AtributosNom" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Funcaoapfatributos_atributosnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Funcaoapfatributos_atributosnom) ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_atributosnom = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_FuncaoDadosCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapfatributos_funcaodadoscod
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_funcaodadoscod ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_funcaodadoscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_FuncaoDadosNom" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Funcaoapfatributos_funcaodadosnom
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_funcaodadosnom ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_funcaodadosnom = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_FuncaoDadosTip" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Funcaoapfatributos_funcaodadostip
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Funcaoapfatributos_funcaodadostip) ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_funcaodadostip = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_AtrTabelaCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapfatributos_atrtabelacod
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_atrtabelacod ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_atrtabelacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_AtrTabelaNom" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Funcaoapfatributos_atrtabelanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Funcaoapfatributos_atrtabelanom) ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_atrtabelanom = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_SistemaCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapfatributos_sistemacod
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_sistemacod ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncoesAPFAtributos_Regra" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Funcoesapfatributos_regra
      {
         get {
            return sdt.gxTpr_Funcoesapfatributos_regra ;
         }

         set {
            sdt.gxTpr_Funcoesapfatributos_regra = value;
         }

      }

      [DataMember( Name = "FuncoesAPFAtributos_Code" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Funcoesapfatributos_code
      {
         get {
            return sdt.gxTpr_Funcoesapfatributos_code ;
         }

         set {
            sdt.gxTpr_Funcoesapfatributos_code = (String)(value);
         }

      }

      [DataMember( Name = "FuncoesAPFAtributos_Nome" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Funcoesapfatributos_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Funcoesapfatributos_nome) ;
         }

         set {
            sdt.gxTpr_Funcoesapfatributos_nome = (String)(value);
         }

      }

      [DataMember( Name = "FuncoesAPFAtributos_Descricao" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Funcoesapfatributos_descricao
      {
         get {
            return sdt.gxTpr_Funcoesapfatributos_descricao ;
         }

         set {
            sdt.gxTpr_Funcoesapfatributos_descricao = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_MelhoraCod" , Order = 14 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaoapfatributos_melhoracod
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_melhoracod ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_melhoracod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoAPFAtributos_Ativo" , Order = 15 )]
      [GxSeudo()]
      public bool gxTpr_Funcaoapfatributos_ativo
      {
         get {
            return sdt.gxTpr_Funcaoapfatributos_ativo ;
         }

         set {
            sdt.gxTpr_Funcaoapfatributos_ativo = value;
         }

      }

      public SdtFuncoesAPFAtributos sdt
      {
         get {
            return (SdtFuncoesAPFAtributos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtFuncoesAPFAtributos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 47 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
