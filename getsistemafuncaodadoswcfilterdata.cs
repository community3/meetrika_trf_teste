/*
               File: GetSistemaFuncaoDadosWCFilterData
        Description: Get Sistema Funcao Dados WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:33:48.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsistemafuncaodadoswcfilterdata : GXProcedure
   {
      public getsistemafuncaodadoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsistemafuncaodadoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
         return AV39OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsistemafuncaodadoswcfilterdata objgetsistemafuncaodadoswcfilterdata;
         objgetsistemafuncaodadoswcfilterdata = new getsistemafuncaodadoswcfilterdata();
         objgetsistemafuncaodadoswcfilterdata.AV30DDOName = aP0_DDOName;
         objgetsistemafuncaodadoswcfilterdata.AV28SearchTxt = aP1_SearchTxt;
         objgetsistemafuncaodadoswcfilterdata.AV29SearchTxtTo = aP2_SearchTxtTo;
         objgetsistemafuncaodadoswcfilterdata.AV34OptionsJson = "" ;
         objgetsistemafuncaodadoswcfilterdata.AV37OptionsDescJson = "" ;
         objgetsistemafuncaodadoswcfilterdata.AV39OptionIndexesJson = "" ;
         objgetsistemafuncaodadoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetsistemafuncaodadoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsistemafuncaodadoswcfilterdata);
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsistemafuncaodadoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV33Options = (IGxCollection)(new GxSimpleCollection());
         AV36OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV38OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_FUNCAODADOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_FUNCAODADOS_FUNCAODADOSSISDES") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_FUNCAODADOSSISDESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_FUNCAODADOS_SOLUCAOTECNICA") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_SOLUCAOTECNICAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV34OptionsJson = AV33Options.ToJSonString(false);
         AV37OptionsDescJson = AV36OptionsDesc.ToJSonString(false);
         AV39OptionIndexesJson = AV38OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get("SistemaFuncaoDadosWCGridState"), "") == 0 )
         {
            AV43GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SistemaFuncaoDadosWCGridState"), "");
         }
         else
         {
            AV43GridState.FromXml(AV41Session.Get("SistemaFuncaoDadosWCGridState"), "");
         }
         AV60GXV1 = 1;
         while ( AV60GXV1 <= AV43GridState.gxTpr_Filtervalues.Count )
         {
            AV44GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV43GridState.gxTpr_Filtervalues.Item(AV60GXV1));
            if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV10TFFuncaoDados_Nome = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV11TFFuncaoDados_Nome_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_TIPO_SEL") == 0 )
            {
               AV12TFFuncaoDados_Tipo_SelsJson = AV44GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoDados_Tipo_Sels.FromJSonString(AV12TFFuncaoDados_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_FUNCAODADOSSISDES") == 0 )
            {
               AV14TFFuncaoDados_FuncaoDadosSisDes = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_FUNCAODADOSSISDES_SEL") == 0 )
            {
               AV15TFFuncaoDados_FuncaoDadosSisDes_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV16TFFuncaoDados_Complexidade_SelsJson = AV44GridStateFilterValue.gxTpr_Value;
               AV17TFFuncaoDados_Complexidade_Sels.FromJSonString(AV16TFFuncaoDados_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV18TFFuncaoDados_DER = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV19TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV20TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV21TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV22TFFuncaoDados_PF = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, ".");
               AV23TFFuncaoDados_PF_To = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_ATIVO_SEL") == 0 )
            {
               AV24TFFuncaoDados_Ativo_SelsJson = AV44GridStateFilterValue.gxTpr_Value;
               AV25TFFuncaoDados_Ativo_Sels.FromJSonString(AV24TFFuncaoDados_Ativo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_SOLUCAOTECNICA") == 0 )
            {
               AV26TFFuncaoDados_SolucaoTecnica = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_SOLUCAOTECNICA_SEL") == 0 )
            {
               AV27TFFuncaoDados_SolucaoTecnica_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAODADOS_SISTEMACOD") == 0 )
            {
               AV57FuncaoDados_SistemaCod = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
            }
            AV60GXV1 = (int)(AV60GXV1+1);
         }
         if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(1));
            AV46DynamicFiltersSelector1 = AV45GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV48FuncaoDados_Nome1 = AV45GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 )
            {
               AV49FuncaoDados_Tipo1 = AV45GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 )
            {
               AV47DynamicFiltersOperator1 = AV45GridStateDynamicFilter.gxTpr_Operator;
               AV50FuncaoDados_PF1 = NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, ".");
            }
            if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV51DynamicFiltersEnabled2 = true;
               AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(2));
               AV52DynamicFiltersSelector2 = AV45GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV54FuncaoDados_Nome2 = AV45GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 )
               {
                  AV55FuncaoDados_Tipo2 = AV45GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV45GridStateDynamicFilter.gxTpr_Operator;
                  AV56FuncaoDados_PF2 = NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, ".");
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAODADOS_NOMEOPTIONS' Routine */
         AV10TFFuncaoDados_Nome = AV28SearchTxt;
         AV11TFFuncaoDados_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV17TFFuncaoDados_Complexidade_Sels ,
                                              A373FuncaoDados_Tipo ,
                                              AV13TFFuncaoDados_Tipo_Sels ,
                                              A394FuncaoDados_Ativo ,
                                              AV25TFFuncaoDados_Ativo_Sels ,
                                              AV46DynamicFiltersSelector1 ,
                                              AV48FuncaoDados_Nome1 ,
                                              AV49FuncaoDados_Tipo1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV54FuncaoDados_Nome2 ,
                                              AV55FuncaoDados_Tipo2 ,
                                              AV11TFFuncaoDados_Nome_Sel ,
                                              AV10TFFuncaoDados_Nome ,
                                              AV13TFFuncaoDados_Tipo_Sels.Count ,
                                              AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                              AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                              AV25TFFuncaoDados_Ativo_Sels.Count ,
                                              A369FuncaoDados_Nome ,
                                              A405FuncaoDados_FuncaoDadosSisDes ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV50FuncaoDados_PF1 ,
                                              A377FuncaoDados_PF ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV56FuncaoDados_PF2 ,
                                              AV17TFFuncaoDados_Complexidade_Sels.Count ,
                                              AV18TFFuncaoDados_DER ,
                                              A374FuncaoDados_DER ,
                                              AV19TFFuncaoDados_DER_To ,
                                              AV20TFFuncaoDados_RLR ,
                                              A375FuncaoDados_RLR ,
                                              AV21TFFuncaoDados_RLR_To ,
                                              AV22TFFuncaoDados_PF ,
                                              AV23TFFuncaoDados_PF_To ,
                                              AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                              AV26TFFuncaoDados_SolucaoTecnica ,
                                              A753FuncaoDados_SolucaoTecnica ,
                                              AV57FuncaoDados_SistemaCod ,
                                              A370FuncaoDados_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV48FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV48FuncaoDados_Nome1), "%", "");
         lV54FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoDados_Nome2), "%", "");
         lV10TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoDados_Nome), "%", "");
         lV14TFFuncaoDados_FuncaoDadosSisDes = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes), "%", "");
         /* Using cursor P00FV2 */
         pr_default.execute(0, new Object[] {AV57FuncaoDados_SistemaCod, AV17TFFuncaoDados_Complexidade_Sels.Count, lV48FuncaoDados_Nome1, AV49FuncaoDados_Tipo1, lV54FuncaoDados_Nome2, AV55FuncaoDados_Tipo2, lV10TFFuncaoDados_Nome, AV11TFFuncaoDados_Nome_Sel, lV14TFFuncaoDados_FuncaoDadosSisDes, AV15TFFuncaoDados_FuncaoDadosSisDes_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFV2 = false;
            A391FuncaoDados_FuncaoDadosCod = P00FV2_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P00FV2_n391FuncaoDados_FuncaoDadosCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV2_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV2_n404FuncaoDados_FuncaoDadosSisCod[0];
            A370FuncaoDados_SistemaCod = P00FV2_A370FuncaoDados_SistemaCod[0];
            A394FuncaoDados_Ativo = P00FV2_A394FuncaoDados_Ativo[0];
            A369FuncaoDados_Nome = P00FV2_A369FuncaoDados_Nome[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV2_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV2_n405FuncaoDados_FuncaoDadosSisDes[0];
            A373FuncaoDados_Tipo = P00FV2_A373FuncaoDados_Tipo[0];
            A755FuncaoDados_Contar = P00FV2_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00FV2_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00FV2_A368FuncaoDados_Codigo[0];
            A745FuncaoDados_MelhoraCod = P00FV2_A745FuncaoDados_MelhoraCod[0];
            n745FuncaoDados_MelhoraCod = P00FV2_n745FuncaoDados_MelhoraCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV2_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV2_n404FuncaoDados_FuncaoDadosSisCod[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV2_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV2_n405FuncaoDados_FuncaoDadosSisDes[0];
            GXt_char1 = A753FuncaoDados_SolucaoTecnica;
            GXt_int2 = 0;
            new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int2,  "D", out  GXt_char1) ;
            A753FuncaoDados_SolucaoTecnica = GXt_char1;
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoDados_SolucaoTecnica)) ) ) || ( StringUtil.Like( A753FuncaoDados_SolucaoTecnica , StringUtil.PadR( AV26TFFuncaoDados_SolucaoTecnica , 500 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) || ( ( StringUtil.StrCmp(A753FuncaoDados_SolucaoTecnica, AV27TFFuncaoDados_SolucaoTecnica_Sel) == 0 ) ) )
               {
                  if ( A755FuncaoDados_Contar )
                  {
                     GXt_int3 = (short)(A377FuncaoDados_PF);
                     new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
                     A377FuncaoDados_PF = (decimal)(GXt_int3);
                  }
                  else
                  {
                     A377FuncaoDados_PF = 0;
                  }
                  if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF < AV50FuncaoDados_PF1 ) ) )
                  {
                     if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF == AV50FuncaoDados_PF1 ) ) )
                     {
                        if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF > AV50FuncaoDados_PF1 ) ) )
                        {
                           if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF < AV56FuncaoDados_PF2 ) ) )
                           {
                              if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF == AV56FuncaoDados_PF2 ) ) )
                              {
                                 if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF > AV56FuncaoDados_PF2 ) ) )
                                 {
                                    if ( (Convert.ToDecimal(0)==AV22TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV22TFFuncaoDados_PF ) ) )
                                    {
                                       if ( (Convert.ToDecimal(0)==AV23TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV23TFFuncaoDados_PF_To ) ) )
                                       {
                                          GXt_char1 = A376FuncaoDados_Complexidade;
                                          new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
                                          A376FuncaoDados_Complexidade = GXt_char1;
                                          if ( ( AV17TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV17TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
                                          {
                                             GXt_int3 = A374FuncaoDados_DER;
                                             new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                             A374FuncaoDados_DER = GXt_int3;
                                             if ( (0==AV18TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV18TFFuncaoDados_DER ) ) )
                                             {
                                                if ( (0==AV19TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV19TFFuncaoDados_DER_To ) ) )
                                                {
                                                   GXt_int3 = A375FuncaoDados_RLR;
                                                   new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                                   A375FuncaoDados_RLR = GXt_int3;
                                                   if ( (0==AV20TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV20TFFuncaoDados_RLR ) ) )
                                                   {
                                                      if ( (0==AV21TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV21TFFuncaoDados_RLR_To ) ) )
                                                      {
                                                         AV40count = 0;
                                                         while ( (pr_default.getStatus(0) != 101) && ( P00FV2_A370FuncaoDados_SistemaCod[0] == A370FuncaoDados_SistemaCod ) && ( StringUtil.StrCmp(P00FV2_A369FuncaoDados_Nome[0], A369FuncaoDados_Nome) == 0 ) )
                                                         {
                                                            BRKFV2 = false;
                                                            A368FuncaoDados_Codigo = P00FV2_A368FuncaoDados_Codigo[0];
                                                            AV40count = (long)(AV40count+1);
                                                            BRKFV2 = true;
                                                            pr_default.readNext(0);
                                                         }
                                                         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
                                                         {
                                                            AV32Option = A369FuncaoDados_Nome;
                                                            AV33Options.Add(AV32Option, 0);
                                                            AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                                         }
                                                         if ( AV33Options.Count == 50 )
                                                         {
                                                            /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                            if (true) break;
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFV2 )
            {
               BRKFV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAODADOS_FUNCAODADOSSISDESOPTIONS' Routine */
         AV14TFFuncaoDados_FuncaoDadosSisDes = AV28SearchTxt;
         AV15TFFuncaoDados_FuncaoDadosSisDes_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV17TFFuncaoDados_Complexidade_Sels ,
                                              A373FuncaoDados_Tipo ,
                                              AV13TFFuncaoDados_Tipo_Sels ,
                                              A394FuncaoDados_Ativo ,
                                              AV25TFFuncaoDados_Ativo_Sels ,
                                              AV46DynamicFiltersSelector1 ,
                                              AV48FuncaoDados_Nome1 ,
                                              AV49FuncaoDados_Tipo1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV54FuncaoDados_Nome2 ,
                                              AV55FuncaoDados_Tipo2 ,
                                              AV11TFFuncaoDados_Nome_Sel ,
                                              AV10TFFuncaoDados_Nome ,
                                              AV13TFFuncaoDados_Tipo_Sels.Count ,
                                              AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                              AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                              AV25TFFuncaoDados_Ativo_Sels.Count ,
                                              A369FuncaoDados_Nome ,
                                              A405FuncaoDados_FuncaoDadosSisDes ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV50FuncaoDados_PF1 ,
                                              A377FuncaoDados_PF ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV56FuncaoDados_PF2 ,
                                              AV17TFFuncaoDados_Complexidade_Sels.Count ,
                                              AV18TFFuncaoDados_DER ,
                                              A374FuncaoDados_DER ,
                                              AV19TFFuncaoDados_DER_To ,
                                              AV20TFFuncaoDados_RLR ,
                                              A375FuncaoDados_RLR ,
                                              AV21TFFuncaoDados_RLR_To ,
                                              AV22TFFuncaoDados_PF ,
                                              AV23TFFuncaoDados_PF_To ,
                                              AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                              AV26TFFuncaoDados_SolucaoTecnica ,
                                              A753FuncaoDados_SolucaoTecnica ,
                                              AV57FuncaoDados_SistemaCod ,
                                              A370FuncaoDados_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV48FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV48FuncaoDados_Nome1), "%", "");
         lV54FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoDados_Nome2), "%", "");
         lV10TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoDados_Nome), "%", "");
         lV14TFFuncaoDados_FuncaoDadosSisDes = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes), "%", "");
         /* Using cursor P00FV3 */
         pr_default.execute(1, new Object[] {AV57FuncaoDados_SistemaCod, AV17TFFuncaoDados_Complexidade_Sels.Count, lV48FuncaoDados_Nome1, AV49FuncaoDados_Tipo1, lV54FuncaoDados_Nome2, AV55FuncaoDados_Tipo2, lV10TFFuncaoDados_Nome, AV11TFFuncaoDados_Nome_Sel, lV14TFFuncaoDados_FuncaoDadosSisDes, AV15TFFuncaoDados_FuncaoDadosSisDes_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFV4 = false;
            A391FuncaoDados_FuncaoDadosCod = P00FV3_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P00FV3_n391FuncaoDados_FuncaoDadosCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV3_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV3_n404FuncaoDados_FuncaoDadosSisCod[0];
            A370FuncaoDados_SistemaCod = P00FV3_A370FuncaoDados_SistemaCod[0];
            A394FuncaoDados_Ativo = P00FV3_A394FuncaoDados_Ativo[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV3_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV3_n405FuncaoDados_FuncaoDadosSisDes[0];
            A373FuncaoDados_Tipo = P00FV3_A373FuncaoDados_Tipo[0];
            A369FuncaoDados_Nome = P00FV3_A369FuncaoDados_Nome[0];
            A755FuncaoDados_Contar = P00FV3_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00FV3_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00FV3_A368FuncaoDados_Codigo[0];
            A745FuncaoDados_MelhoraCod = P00FV3_A745FuncaoDados_MelhoraCod[0];
            n745FuncaoDados_MelhoraCod = P00FV3_n745FuncaoDados_MelhoraCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV3_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV3_n404FuncaoDados_FuncaoDadosSisCod[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV3_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV3_n405FuncaoDados_FuncaoDadosSisDes[0];
            GXt_char1 = A753FuncaoDados_SolucaoTecnica;
            GXt_int2 = 0;
            new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int2,  "D", out  GXt_char1) ;
            A753FuncaoDados_SolucaoTecnica = GXt_char1;
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoDados_SolucaoTecnica)) ) ) || ( StringUtil.Like( A753FuncaoDados_SolucaoTecnica , StringUtil.PadR( AV26TFFuncaoDados_SolucaoTecnica , 500 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) || ( ( StringUtil.StrCmp(A753FuncaoDados_SolucaoTecnica, AV27TFFuncaoDados_SolucaoTecnica_Sel) == 0 ) ) )
               {
                  if ( A755FuncaoDados_Contar )
                  {
                     GXt_int3 = (short)(A377FuncaoDados_PF);
                     new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
                     A377FuncaoDados_PF = (decimal)(GXt_int3);
                  }
                  else
                  {
                     A377FuncaoDados_PF = 0;
                  }
                  if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF < AV50FuncaoDados_PF1 ) ) )
                  {
                     if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF == AV50FuncaoDados_PF1 ) ) )
                     {
                        if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF > AV50FuncaoDados_PF1 ) ) )
                        {
                           if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF < AV56FuncaoDados_PF2 ) ) )
                           {
                              if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF == AV56FuncaoDados_PF2 ) ) )
                              {
                                 if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF > AV56FuncaoDados_PF2 ) ) )
                                 {
                                    if ( (Convert.ToDecimal(0)==AV22TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV22TFFuncaoDados_PF ) ) )
                                    {
                                       if ( (Convert.ToDecimal(0)==AV23TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV23TFFuncaoDados_PF_To ) ) )
                                       {
                                          GXt_char1 = A376FuncaoDados_Complexidade;
                                          new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
                                          A376FuncaoDados_Complexidade = GXt_char1;
                                          if ( ( AV17TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV17TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
                                          {
                                             GXt_int3 = A374FuncaoDados_DER;
                                             new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                             A374FuncaoDados_DER = GXt_int3;
                                             if ( (0==AV18TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV18TFFuncaoDados_DER ) ) )
                                             {
                                                if ( (0==AV19TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV19TFFuncaoDados_DER_To ) ) )
                                                {
                                                   GXt_int3 = A375FuncaoDados_RLR;
                                                   new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                                   A375FuncaoDados_RLR = GXt_int3;
                                                   if ( (0==AV20TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV20TFFuncaoDados_RLR ) ) )
                                                   {
                                                      if ( (0==AV21TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV21TFFuncaoDados_RLR_To ) ) )
                                                      {
                                                         AV40count = 0;
                                                         while ( (pr_default.getStatus(1) != 101) && ( P00FV3_A370FuncaoDados_SistemaCod[0] == A370FuncaoDados_SistemaCod ) && ( StringUtil.StrCmp(P00FV3_A405FuncaoDados_FuncaoDadosSisDes[0], A405FuncaoDados_FuncaoDadosSisDes) == 0 ) )
                                                         {
                                                            BRKFV4 = false;
                                                            A391FuncaoDados_FuncaoDadosCod = P00FV3_A391FuncaoDados_FuncaoDadosCod[0];
                                                            n391FuncaoDados_FuncaoDadosCod = P00FV3_n391FuncaoDados_FuncaoDadosCod[0];
                                                            A404FuncaoDados_FuncaoDadosSisCod = P00FV3_A404FuncaoDados_FuncaoDadosSisCod[0];
                                                            n404FuncaoDados_FuncaoDadosSisCod = P00FV3_n404FuncaoDados_FuncaoDadosSisCod[0];
                                                            A368FuncaoDados_Codigo = P00FV3_A368FuncaoDados_Codigo[0];
                                                            A404FuncaoDados_FuncaoDadosSisCod = P00FV3_A404FuncaoDados_FuncaoDadosSisCod[0];
                                                            n404FuncaoDados_FuncaoDadosSisCod = P00FV3_n404FuncaoDados_FuncaoDadosSisCod[0];
                                                            AV40count = (long)(AV40count+1);
                                                            BRKFV4 = true;
                                                            pr_default.readNext(1);
                                                         }
                                                         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A405FuncaoDados_FuncaoDadosSisDes)) )
                                                         {
                                                            AV32Option = A405FuncaoDados_FuncaoDadosSisDes;
                                                            AV33Options.Add(AV32Option, 0);
                                                            AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                                         }
                                                         if ( AV33Options.Count == 50 )
                                                         {
                                                            /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                            if (true) break;
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFV4 )
            {
               BRKFV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADFUNCAODADOS_SOLUCAOTECNICAOPTIONS' Routine */
         AV26TFFuncaoDados_SolucaoTecnica = AV28SearchTxt;
         AV27TFFuncaoDados_SolucaoTecnica_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV17TFFuncaoDados_Complexidade_Sels ,
                                              A373FuncaoDados_Tipo ,
                                              AV13TFFuncaoDados_Tipo_Sels ,
                                              A394FuncaoDados_Ativo ,
                                              AV25TFFuncaoDados_Ativo_Sels ,
                                              AV46DynamicFiltersSelector1 ,
                                              AV48FuncaoDados_Nome1 ,
                                              AV49FuncaoDados_Tipo1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV54FuncaoDados_Nome2 ,
                                              AV55FuncaoDados_Tipo2 ,
                                              AV11TFFuncaoDados_Nome_Sel ,
                                              AV10TFFuncaoDados_Nome ,
                                              AV13TFFuncaoDados_Tipo_Sels.Count ,
                                              AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                              AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                              AV25TFFuncaoDados_Ativo_Sels.Count ,
                                              A369FuncaoDados_Nome ,
                                              A405FuncaoDados_FuncaoDadosSisDes ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV50FuncaoDados_PF1 ,
                                              A377FuncaoDados_PF ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV56FuncaoDados_PF2 ,
                                              AV17TFFuncaoDados_Complexidade_Sels.Count ,
                                              AV18TFFuncaoDados_DER ,
                                              A374FuncaoDados_DER ,
                                              AV19TFFuncaoDados_DER_To ,
                                              AV20TFFuncaoDados_RLR ,
                                              A375FuncaoDados_RLR ,
                                              AV21TFFuncaoDados_RLR_To ,
                                              AV22TFFuncaoDados_PF ,
                                              AV23TFFuncaoDados_PF_To ,
                                              AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                              AV26TFFuncaoDados_SolucaoTecnica ,
                                              A753FuncaoDados_SolucaoTecnica ,
                                              AV57FuncaoDados_SistemaCod ,
                                              A370FuncaoDados_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV48FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV48FuncaoDados_Nome1), "%", "");
         lV54FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoDados_Nome2), "%", "");
         lV10TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoDados_Nome), "%", "");
         lV14TFFuncaoDados_FuncaoDadosSisDes = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes), "%", "");
         /* Using cursor P00FV4 */
         pr_default.execute(2, new Object[] {AV57FuncaoDados_SistemaCod, AV17TFFuncaoDados_Complexidade_Sels.Count, lV48FuncaoDados_Nome1, AV49FuncaoDados_Tipo1, lV54FuncaoDados_Nome2, AV55FuncaoDados_Tipo2, lV10TFFuncaoDados_Nome, AV11TFFuncaoDados_Nome_Sel, lV14TFFuncaoDados_FuncaoDadosSisDes, AV15TFFuncaoDados_FuncaoDadosSisDes_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A391FuncaoDados_FuncaoDadosCod = P00FV4_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P00FV4_n391FuncaoDados_FuncaoDadosCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV4_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV4_n404FuncaoDados_FuncaoDadosSisCod[0];
            A394FuncaoDados_Ativo = P00FV4_A394FuncaoDados_Ativo[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV4_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV4_n405FuncaoDados_FuncaoDadosSisDes[0];
            A373FuncaoDados_Tipo = P00FV4_A373FuncaoDados_Tipo[0];
            A369FuncaoDados_Nome = P00FV4_A369FuncaoDados_Nome[0];
            A370FuncaoDados_SistemaCod = P00FV4_A370FuncaoDados_SistemaCod[0];
            A755FuncaoDados_Contar = P00FV4_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00FV4_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00FV4_A368FuncaoDados_Codigo[0];
            A745FuncaoDados_MelhoraCod = P00FV4_A745FuncaoDados_MelhoraCod[0];
            n745FuncaoDados_MelhoraCod = P00FV4_n745FuncaoDados_MelhoraCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = P00FV4_A404FuncaoDados_FuncaoDadosSisCod[0];
            n404FuncaoDados_FuncaoDadosSisCod = P00FV4_n404FuncaoDados_FuncaoDadosSisCod[0];
            A405FuncaoDados_FuncaoDadosSisDes = P00FV4_A405FuncaoDados_FuncaoDadosSisDes[0];
            n405FuncaoDados_FuncaoDadosSisDes = P00FV4_n405FuncaoDados_FuncaoDadosSisDes[0];
            GXt_char1 = A753FuncaoDados_SolucaoTecnica;
            GXt_int2 = 0;
            new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  GXt_int2,  "D", out  GXt_char1) ;
            A753FuncaoDados_SolucaoTecnica = GXt_char1;
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoDados_SolucaoTecnica)) ) ) || ( StringUtil.Like( A753FuncaoDados_SolucaoTecnica , StringUtil.PadR( AV26TFFuncaoDados_SolucaoTecnica , 500 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFFuncaoDados_SolucaoTecnica_Sel)) || ( ( StringUtil.StrCmp(A753FuncaoDados_SolucaoTecnica, AV27TFFuncaoDados_SolucaoTecnica_Sel) == 0 ) ) )
               {
                  if ( A755FuncaoDados_Contar )
                  {
                     GXt_int3 = (short)(A377FuncaoDados_PF);
                     new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int3) ;
                     A377FuncaoDados_PF = (decimal)(GXt_int3);
                  }
                  else
                  {
                     A377FuncaoDados_PF = 0;
                  }
                  if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF < AV50FuncaoDados_PF1 ) ) )
                  {
                     if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF == AV50FuncaoDados_PF1 ) ) )
                     {
                        if ( ! ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV47DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV50FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF > AV50FuncaoDados_PF1 ) ) )
                        {
                           if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF < AV56FuncaoDados_PF2 ) ) )
                           {
                              if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF == AV56FuncaoDados_PF2 ) ) )
                              {
                                 if ( ! ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV53DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV56FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF > AV56FuncaoDados_PF2 ) ) )
                                 {
                                    if ( (Convert.ToDecimal(0)==AV22TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV22TFFuncaoDados_PF ) ) )
                                    {
                                       if ( (Convert.ToDecimal(0)==AV23TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV23TFFuncaoDados_PF_To ) ) )
                                       {
                                          GXt_char1 = A376FuncaoDados_Complexidade;
                                          new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
                                          A376FuncaoDados_Complexidade = GXt_char1;
                                          if ( ( AV17TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV17TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
                                          {
                                             GXt_int3 = A374FuncaoDados_DER;
                                             new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                             A374FuncaoDados_DER = GXt_int3;
                                             if ( (0==AV18TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV18TFFuncaoDados_DER ) ) )
                                             {
                                                if ( (0==AV19TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV19TFFuncaoDados_DER_To ) ) )
                                                {
                                                   GXt_int3 = A375FuncaoDados_RLR;
                                                   new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int3) ;
                                                   A375FuncaoDados_RLR = GXt_int3;
                                                   if ( (0==AV20TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV20TFFuncaoDados_RLR ) ) )
                                                   {
                                                      if ( (0==AV21TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV21TFFuncaoDados_RLR_To ) ) )
                                                      {
                                                         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A753FuncaoDados_SolucaoTecnica)) )
                                                         {
                                                            AV32Option = A753FuncaoDados_SolucaoTecnica;
                                                            AV31InsertIndex = 1;
                                                            while ( ( AV31InsertIndex <= AV33Options.Count ) && ( StringUtil.StrCmp(((String)AV33Options.Item(AV31InsertIndex)), AV32Option) < 0 ) )
                                                            {
                                                               AV31InsertIndex = (int)(AV31InsertIndex+1);
                                                            }
                                                            if ( ( AV31InsertIndex <= AV33Options.Count ) && ( StringUtil.StrCmp(((String)AV33Options.Item(AV31InsertIndex)), AV32Option) == 0 ) )
                                                            {
                                                               AV40count = (long)(NumberUtil.Val( ((String)AV38OptionIndexes.Item(AV31InsertIndex)), "."));
                                                               AV40count = (long)(AV40count+1);
                                                               AV38OptionIndexes.RemoveItem(AV31InsertIndex);
                                                               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), AV31InsertIndex);
                                                            }
                                                            else
                                                            {
                                                               AV33Options.Add(AV32Option, AV31InsertIndex);
                                                               AV38OptionIndexes.Add("1", AV31InsertIndex);
                                                            }
                                                         }
                                                         if ( AV33Options.Count == 50 )
                                                         {
                                                            /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                            if (true) break;
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV33Options = new GxSimpleCollection();
         AV36OptionsDesc = new GxSimpleCollection();
         AV38OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41Session = context.GetSession();
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoDados_Nome = "";
         AV11TFFuncaoDados_Nome_Sel = "";
         AV12TFFuncaoDados_Tipo_SelsJson = "";
         AV13TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV14TFFuncaoDados_FuncaoDadosSisDes = "";
         AV15TFFuncaoDados_FuncaoDadosSisDes_Sel = "";
         AV16TFFuncaoDados_Complexidade_SelsJson = "";
         AV17TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV24TFFuncaoDados_Ativo_SelsJson = "";
         AV25TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         AV26TFFuncaoDados_SolucaoTecnica = "";
         AV27TFFuncaoDados_SolucaoTecnica_Sel = "";
         AV45GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV46DynamicFiltersSelector1 = "";
         AV48FuncaoDados_Nome1 = "";
         AV49FuncaoDados_Tipo1 = "";
         AV52DynamicFiltersSelector2 = "";
         AV54FuncaoDados_Nome2 = "";
         AV55FuncaoDados_Tipo2 = "";
         scmdbuf = "";
         lV48FuncaoDados_Nome1 = "";
         lV54FuncaoDados_Nome2 = "";
         lV10TFFuncaoDados_Nome = "";
         lV14TFFuncaoDados_FuncaoDadosSisDes = "";
         A376FuncaoDados_Complexidade = "";
         A373FuncaoDados_Tipo = "";
         A394FuncaoDados_Ativo = "";
         A369FuncaoDados_Nome = "";
         A405FuncaoDados_FuncaoDadosSisDes = "";
         A753FuncaoDados_SolucaoTecnica = "";
         P00FV2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P00FV2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P00FV2_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         P00FV2_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         P00FV2_A370FuncaoDados_SistemaCod = new int[1] ;
         P00FV2_A394FuncaoDados_Ativo = new String[] {""} ;
         P00FV2_A369FuncaoDados_Nome = new String[] {""} ;
         P00FV2_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         P00FV2_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         P00FV2_A373FuncaoDados_Tipo = new String[] {""} ;
         P00FV2_A755FuncaoDados_Contar = new bool[] {false} ;
         P00FV2_n755FuncaoDados_Contar = new bool[] {false} ;
         P00FV2_A368FuncaoDados_Codigo = new int[1] ;
         P00FV2_A745FuncaoDados_MelhoraCod = new int[1] ;
         P00FV2_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         AV32Option = "";
         P00FV3_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P00FV3_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P00FV3_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         P00FV3_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         P00FV3_A370FuncaoDados_SistemaCod = new int[1] ;
         P00FV3_A394FuncaoDados_Ativo = new String[] {""} ;
         P00FV3_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         P00FV3_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         P00FV3_A373FuncaoDados_Tipo = new String[] {""} ;
         P00FV3_A369FuncaoDados_Nome = new String[] {""} ;
         P00FV3_A755FuncaoDados_Contar = new bool[] {false} ;
         P00FV3_n755FuncaoDados_Contar = new bool[] {false} ;
         P00FV3_A368FuncaoDados_Codigo = new int[1] ;
         P00FV3_A745FuncaoDados_MelhoraCod = new int[1] ;
         P00FV3_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         P00FV4_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P00FV4_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P00FV4_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         P00FV4_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         P00FV4_A394FuncaoDados_Ativo = new String[] {""} ;
         P00FV4_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         P00FV4_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         P00FV4_A373FuncaoDados_Tipo = new String[] {""} ;
         P00FV4_A369FuncaoDados_Nome = new String[] {""} ;
         P00FV4_A370FuncaoDados_SistemaCod = new int[1] ;
         P00FV4_A755FuncaoDados_Contar = new bool[] {false} ;
         P00FV4_n755FuncaoDados_Contar = new bool[] {false} ;
         P00FV4_A368FuncaoDados_Codigo = new int[1] ;
         P00FV4_A745FuncaoDados_MelhoraCod = new int[1] ;
         P00FV4_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsistemafuncaodadoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FV2_A391FuncaoDados_FuncaoDadosCod, P00FV2_n391FuncaoDados_FuncaoDadosCod, P00FV2_A404FuncaoDados_FuncaoDadosSisCod, P00FV2_n404FuncaoDados_FuncaoDadosSisCod, P00FV2_A370FuncaoDados_SistemaCod, P00FV2_A394FuncaoDados_Ativo, P00FV2_A369FuncaoDados_Nome, P00FV2_A405FuncaoDados_FuncaoDadosSisDes, P00FV2_n405FuncaoDados_FuncaoDadosSisDes, P00FV2_A373FuncaoDados_Tipo,
               P00FV2_A755FuncaoDados_Contar, P00FV2_n755FuncaoDados_Contar, P00FV2_A368FuncaoDados_Codigo, P00FV2_A745FuncaoDados_MelhoraCod, P00FV2_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               P00FV3_A391FuncaoDados_FuncaoDadosCod, P00FV3_n391FuncaoDados_FuncaoDadosCod, P00FV3_A404FuncaoDados_FuncaoDadosSisCod, P00FV3_n404FuncaoDados_FuncaoDadosSisCod, P00FV3_A370FuncaoDados_SistemaCod, P00FV3_A394FuncaoDados_Ativo, P00FV3_A405FuncaoDados_FuncaoDadosSisDes, P00FV3_n405FuncaoDados_FuncaoDadosSisDes, P00FV3_A373FuncaoDados_Tipo, P00FV3_A369FuncaoDados_Nome,
               P00FV3_A755FuncaoDados_Contar, P00FV3_n755FuncaoDados_Contar, P00FV3_A368FuncaoDados_Codigo, P00FV3_A745FuncaoDados_MelhoraCod, P00FV3_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               P00FV4_A391FuncaoDados_FuncaoDadosCod, P00FV4_n391FuncaoDados_FuncaoDadosCod, P00FV4_A404FuncaoDados_FuncaoDadosSisCod, P00FV4_n404FuncaoDados_FuncaoDadosSisCod, P00FV4_A394FuncaoDados_Ativo, P00FV4_A405FuncaoDados_FuncaoDadosSisDes, P00FV4_n405FuncaoDados_FuncaoDadosSisDes, P00FV4_A373FuncaoDados_Tipo, P00FV4_A369FuncaoDados_Nome, P00FV4_A370FuncaoDados_SistemaCod,
               P00FV4_A755FuncaoDados_Contar, P00FV4_n755FuncaoDados_Contar, P00FV4_A368FuncaoDados_Codigo, P00FV4_A745FuncaoDados_MelhoraCod, P00FV4_n745FuncaoDados_MelhoraCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFFuncaoDados_DER ;
      private short AV19TFFuncaoDados_DER_To ;
      private short AV20TFFuncaoDados_RLR ;
      private short AV21TFFuncaoDados_RLR_To ;
      private short AV47DynamicFiltersOperator1 ;
      private short AV53DynamicFiltersOperator2 ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int3 ;
      private int AV60GXV1 ;
      private int AV57FuncaoDados_SistemaCod ;
      private int AV13TFFuncaoDados_Tipo_Sels_Count ;
      private int AV25TFFuncaoDados_Ativo_Sels_Count ;
      private int AV17TFFuncaoDados_Complexidade_Sels_Count ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A404FuncaoDados_FuncaoDadosSisCod ;
      private int A368FuncaoDados_Codigo ;
      private int A745FuncaoDados_MelhoraCod ;
      private int GXt_int2 ;
      private int AV31InsertIndex ;
      private long AV40count ;
      private decimal AV22TFFuncaoDados_PF ;
      private decimal AV23TFFuncaoDados_PF_To ;
      private decimal AV50FuncaoDados_PF1 ;
      private decimal AV56FuncaoDados_PF2 ;
      private decimal A377FuncaoDados_PF ;
      private String AV49FuncaoDados_Tipo1 ;
      private String AV55FuncaoDados_Tipo2 ;
      private String scmdbuf ;
      private String A376FuncaoDados_Complexidade ;
      private String A373FuncaoDados_Tipo ;
      private String A394FuncaoDados_Ativo ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV51DynamicFiltersEnabled2 ;
      private bool BRKFV2 ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n404FuncaoDados_FuncaoDadosSisCod ;
      private bool n405FuncaoDados_FuncaoDadosSisDes ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool BRKFV4 ;
      private String AV39OptionIndexesJson ;
      private String AV34OptionsJson ;
      private String AV37OptionsDescJson ;
      private String AV12TFFuncaoDados_Tipo_SelsJson ;
      private String AV16TFFuncaoDados_Complexidade_SelsJson ;
      private String AV24TFFuncaoDados_Ativo_SelsJson ;
      private String A753FuncaoDados_SolucaoTecnica ;
      private String AV30DDOName ;
      private String AV28SearchTxt ;
      private String AV29SearchTxtTo ;
      private String AV10TFFuncaoDados_Nome ;
      private String AV11TFFuncaoDados_Nome_Sel ;
      private String AV14TFFuncaoDados_FuncaoDadosSisDes ;
      private String AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ;
      private String AV26TFFuncaoDados_SolucaoTecnica ;
      private String AV27TFFuncaoDados_SolucaoTecnica_Sel ;
      private String AV46DynamicFiltersSelector1 ;
      private String AV48FuncaoDados_Nome1 ;
      private String AV52DynamicFiltersSelector2 ;
      private String AV54FuncaoDados_Nome2 ;
      private String lV48FuncaoDados_Nome1 ;
      private String lV54FuncaoDados_Nome2 ;
      private String lV10TFFuncaoDados_Nome ;
      private String lV14TFFuncaoDados_FuncaoDadosSisDes ;
      private String A369FuncaoDados_Nome ;
      private String A405FuncaoDados_FuncaoDadosSisDes ;
      private String AV32Option ;
      private IGxSession AV41Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FV2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P00FV2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] P00FV2_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] P00FV2_n404FuncaoDados_FuncaoDadosSisCod ;
      private int[] P00FV2_A370FuncaoDados_SistemaCod ;
      private String[] P00FV2_A394FuncaoDados_Ativo ;
      private String[] P00FV2_A369FuncaoDados_Nome ;
      private String[] P00FV2_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] P00FV2_n405FuncaoDados_FuncaoDadosSisDes ;
      private String[] P00FV2_A373FuncaoDados_Tipo ;
      private bool[] P00FV2_A755FuncaoDados_Contar ;
      private bool[] P00FV2_n755FuncaoDados_Contar ;
      private int[] P00FV2_A368FuncaoDados_Codigo ;
      private int[] P00FV2_A745FuncaoDados_MelhoraCod ;
      private bool[] P00FV2_n745FuncaoDados_MelhoraCod ;
      private int[] P00FV3_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P00FV3_n391FuncaoDados_FuncaoDadosCod ;
      private int[] P00FV3_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] P00FV3_n404FuncaoDados_FuncaoDadosSisCod ;
      private int[] P00FV3_A370FuncaoDados_SistemaCod ;
      private String[] P00FV3_A394FuncaoDados_Ativo ;
      private String[] P00FV3_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] P00FV3_n405FuncaoDados_FuncaoDadosSisDes ;
      private String[] P00FV3_A373FuncaoDados_Tipo ;
      private String[] P00FV3_A369FuncaoDados_Nome ;
      private bool[] P00FV3_A755FuncaoDados_Contar ;
      private bool[] P00FV3_n755FuncaoDados_Contar ;
      private int[] P00FV3_A368FuncaoDados_Codigo ;
      private int[] P00FV3_A745FuncaoDados_MelhoraCod ;
      private bool[] P00FV3_n745FuncaoDados_MelhoraCod ;
      private int[] P00FV4_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P00FV4_n391FuncaoDados_FuncaoDadosCod ;
      private int[] P00FV4_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] P00FV4_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] P00FV4_A394FuncaoDados_Ativo ;
      private String[] P00FV4_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] P00FV4_n405FuncaoDados_FuncaoDadosSisDes ;
      private String[] P00FV4_A373FuncaoDados_Tipo ;
      private String[] P00FV4_A369FuncaoDados_Nome ;
      private int[] P00FV4_A370FuncaoDados_SistemaCod ;
      private bool[] P00FV4_A755FuncaoDados_Contar ;
      private bool[] P00FV4_n755FuncaoDados_Contar ;
      private int[] P00FV4_A368FuncaoDados_Codigo ;
      private int[] P00FV4_A745FuncaoDados_MelhoraCod ;
      private bool[] P00FV4_n745FuncaoDados_MelhoraCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV44GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV45GridStateDynamicFilter ;
   }

   public class getsistemafuncaodadoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FV2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV17TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV13TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV25TFFuncaoDados_Ativo_Sels ,
                                             String AV46DynamicFiltersSelector1 ,
                                             String AV48FuncaoDados_Nome1 ,
                                             String AV49FuncaoDados_Tipo1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             String AV54FuncaoDados_Nome2 ,
                                             String AV55FuncaoDados_Tipo2 ,
                                             String AV11TFFuncaoDados_Nome_Sel ,
                                             String AV10TFFuncaoDados_Nome ,
                                             int AV13TFFuncaoDados_Tipo_Sels_Count ,
                                             String AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                             String AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                             int AV25TFFuncaoDados_Ativo_Sels_Count ,
                                             String A369FuncaoDados_Nome ,
                                             String A405FuncaoDados_FuncaoDadosSisDes ,
                                             short AV47DynamicFiltersOperator1 ,
                                             decimal AV50FuncaoDados_PF1 ,
                                             decimal A377FuncaoDados_PF ,
                                             short AV53DynamicFiltersOperator2 ,
                                             decimal AV56FuncaoDados_PF2 ,
                                             int AV17TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV18TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV19TFFuncaoDados_DER_To ,
                                             short AV20TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV21TFFuncaoDados_RLR_To ,
                                             decimal AV22TFFuncaoDados_PF ,
                                             decimal AV23TFFuncaoDados_PF_To ,
                                             String AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                             String AV26TFFuncaoDados_SolucaoTecnica ,
                                             String A753FuncaoDados_SolucaoTecnica ,
                                             int AV57FuncaoDados_SistemaCod ,
                                             int A370FuncaoDados_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, T2.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod, T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Ativo], T1.[FuncaoDados_Nome], T3.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo], T1.[FuncaoDados_MelhoraCod] FROM (([FuncaoDados] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[FuncaoDados_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_SistemaCod] = @AV57FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoDados_Ativo] = 'A')";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV48FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoDados_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV49FuncaoDados_Tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV54FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoDados_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV55FuncaoDados_Tipo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV13TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoDados_Tipo_Sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV14TFFuncaoDados_FuncaoDadosSisDes)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV25TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoDados_Ativo_Sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Nome]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00FV3( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV17TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV13TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV25TFFuncaoDados_Ativo_Sels ,
                                             String AV46DynamicFiltersSelector1 ,
                                             String AV48FuncaoDados_Nome1 ,
                                             String AV49FuncaoDados_Tipo1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             String AV54FuncaoDados_Nome2 ,
                                             String AV55FuncaoDados_Tipo2 ,
                                             String AV11TFFuncaoDados_Nome_Sel ,
                                             String AV10TFFuncaoDados_Nome ,
                                             int AV13TFFuncaoDados_Tipo_Sels_Count ,
                                             String AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                             String AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                             int AV25TFFuncaoDados_Ativo_Sels_Count ,
                                             String A369FuncaoDados_Nome ,
                                             String A405FuncaoDados_FuncaoDadosSisDes ,
                                             short AV47DynamicFiltersOperator1 ,
                                             decimal AV50FuncaoDados_PF1 ,
                                             decimal A377FuncaoDados_PF ,
                                             short AV53DynamicFiltersOperator2 ,
                                             decimal AV56FuncaoDados_PF2 ,
                                             int AV17TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV18TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV19TFFuncaoDados_DER_To ,
                                             short AV20TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV21TFFuncaoDados_RLR_To ,
                                             decimal AV22TFFuncaoDados_PF ,
                                             decimal AV23TFFuncaoDados_PF_To ,
                                             String AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                             String AV26TFFuncaoDados_SolucaoTecnica ,
                                             String A753FuncaoDados_SolucaoTecnica ,
                                             int AV57FuncaoDados_SistemaCod ,
                                             int A370FuncaoDados_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [10] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, T2.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod, T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Ativo], T3.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Nome], T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo], T1.[FuncaoDados_MelhoraCod] FROM (([FuncaoDados] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[FuncaoDados_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_SistemaCod] = @AV57FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoDados_Ativo] = 'A')";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV48FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoDados_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV49FuncaoDados_Tipo1)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV54FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoDados_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV55FuncaoDados_Tipo2)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV13TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoDados_Tipo_Sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV14TFFuncaoDados_FuncaoDadosSisDes)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV25TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoDados_Ativo_Sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T3.[Sistema_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P00FV4( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV17TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV13TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV25TFFuncaoDados_Ativo_Sels ,
                                             String AV46DynamicFiltersSelector1 ,
                                             String AV48FuncaoDados_Nome1 ,
                                             String AV49FuncaoDados_Tipo1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             String AV54FuncaoDados_Nome2 ,
                                             String AV55FuncaoDados_Tipo2 ,
                                             String AV11TFFuncaoDados_Nome_Sel ,
                                             String AV10TFFuncaoDados_Nome ,
                                             int AV13TFFuncaoDados_Tipo_Sels_Count ,
                                             String AV15TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                             String AV14TFFuncaoDados_FuncaoDadosSisDes ,
                                             int AV25TFFuncaoDados_Ativo_Sels_Count ,
                                             String A369FuncaoDados_Nome ,
                                             String A405FuncaoDados_FuncaoDadosSisDes ,
                                             short AV47DynamicFiltersOperator1 ,
                                             decimal AV50FuncaoDados_PF1 ,
                                             decimal A377FuncaoDados_PF ,
                                             short AV53DynamicFiltersOperator2 ,
                                             decimal AV56FuncaoDados_PF2 ,
                                             int AV17TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV18TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV19TFFuncaoDados_DER_To ,
                                             short AV20TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV21TFFuncaoDados_RLR_To ,
                                             decimal AV22TFFuncaoDados_PF ,
                                             decimal AV23TFFuncaoDados_PF_To ,
                                             String AV27TFFuncaoDados_SolucaoTecnica_Sel ,
                                             String AV26TFFuncaoDados_SolucaoTecnica ,
                                             String A753FuncaoDados_SolucaoTecnica ,
                                             int AV57FuncaoDados_SistemaCod ,
                                             int A370FuncaoDados_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [10] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, T2.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod, T1.[FuncaoDados_Ativo], T3.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Nome], T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo], T1.[FuncaoDados_MelhoraCod] FROM (([FuncaoDados] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[FuncaoDados_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_SistemaCod] = @AV57FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoDados_Ativo] = 'A')";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV48FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoDados_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV49FuncaoDados_Tipo1)";
         }
         else
         {
            GXv_int8[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV54FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int8[3] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoDados_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV55FuncaoDados_Tipo2)";
         }
         else
         {
            GXv_int8[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int8[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int8[6] = 1;
         }
         if ( AV13TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoDados_Tipo_Sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoDados_FuncaoDadosSisDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV14TFFuncaoDados_FuncaoDadosSisDes)";
         }
         else
         {
            GXv_int8[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV15TFFuncaoDados_FuncaoDadosSisDes_Sel)";
         }
         else
         {
            GXv_int8[8] = 1;
         }
         if ( AV25TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoDados_Ativo_Sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FV2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (short)dynConstraints[24] , (decimal)dynConstraints[25] , (int)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] );
               case 1 :
                     return conditional_P00FV3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (short)dynConstraints[24] , (decimal)dynConstraints[25] , (int)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] );
               case 2 :
                     return conditional_P00FV4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (short)dynConstraints[24] , (decimal)dynConstraints[25] , (int)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FV2 ;
          prmP00FV2 = new Object[] {
          new Object[] {"@AV57FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV48FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49FuncaoDados_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV54FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV55FuncaoDados_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFFuncaoDados_FuncaoDadosSisDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFFuncaoDados_FuncaoDadosSisDes_Sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00FV3 ;
          prmP00FV3 = new Object[] {
          new Object[] {"@AV57FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV48FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49FuncaoDados_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV54FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV55FuncaoDados_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFFuncaoDados_FuncaoDadosSisDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFFuncaoDados_FuncaoDadosSisDes_Sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00FV4 ;
          prmP00FV4 = new Object[] {
          new Object[] {"@AV57FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV48FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49FuncaoDados_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV54FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV55FuncaoDados_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFFuncaoDados_FuncaoDadosSisDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFFuncaoDados_FuncaoDadosSisDes_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FV2,100,0,true,false )
             ,new CursorDef("P00FV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FV3,100,0,true,false )
             ,new CursorDef("P00FV4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FV4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 3) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 3) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsistemafuncaodadoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsistemafuncaodadoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsistemafuncaodadoswcfilterdata") )
          {
             return  ;
          }
          getsistemafuncaodadoswcfilterdata worker = new getsistemafuncaodadoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
