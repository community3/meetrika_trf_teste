/*
               File: GetPromptStatusFilterData
        Description: Get Prompt Status Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:55.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptstatusfilterdata : GXProcedure
   {
      public getpromptstatusfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptstatusfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptstatusfilterdata objgetpromptstatusfilterdata;
         objgetpromptstatusfilterdata = new getpromptstatusfilterdata();
         objgetpromptstatusfilterdata.AV16DDOName = aP0_DDOName;
         objgetpromptstatusfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetpromptstatusfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptstatusfilterdata.AV20OptionsJson = "" ;
         objgetpromptstatusfilterdata.AV23OptionsDescJson = "" ;
         objgetpromptstatusfilterdata.AV25OptionIndexesJson = "" ;
         objgetpromptstatusfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptstatusfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptstatusfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptstatusfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_STATUS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSTATUS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("PromptStatusGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptStatusGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("PromptStatusGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "STATUS_AREATRABALHOCOD") == 0 )
            {
               AV32Status_AreaTrabalhoCod = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_NOME") == 0 )
            {
               AV10TFStatus_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_NOME_SEL") == 0 )
            {
               AV11TFStatus_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_TIPO_SEL") == 0 )
            {
               AV12TFStatus_Tipo_SelsJson = AV30GridStateFilterValue.gxTpr_Value;
               AV13TFStatus_Tipo_Sels.FromJSonString(AV12TFStatus_Tipo_SelsJson);
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "STATUS_NOME") == 0 )
            {
               AV34Status_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "STATUS_NOME") == 0 )
               {
                  AV37Status_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV38DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "STATUS_NOME") == 0 )
                  {
                     AV40Status_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSTATUS_NOMEOPTIONS' Routine */
         AV10TFStatus_Nome = AV14SearchTxt;
         AV11TFStatus_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A425Status_Tipo ,
                                              AV13TFStatus_Tipo_Sels ,
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Status_Nome1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37Status_Nome2 ,
                                              AV38DynamicFiltersEnabled3 ,
                                              AV39DynamicFiltersSelector3 ,
                                              AV40Status_Nome3 ,
                                              AV11TFStatus_Nome_Sel ,
                                              AV10TFStatus_Nome ,
                                              AV13TFStatus_Tipo_Sels.Count ,
                                              A424Status_Nome ,
                                              A421Status_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV34Status_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34Status_Nome1), 50, "%");
         lV37Status_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV37Status_Nome2), 50, "%");
         lV40Status_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV40Status_Nome3), 50, "%");
         lV10TFStatus_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFStatus_Nome), 50, "%");
         /* Using cursor P00MA2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV34Status_Nome1, lV37Status_Nome2, lV40Status_Nome3, lV10TFStatus_Nome, AV11TFStatus_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMA2 = false;
            A421Status_AreaTrabalhoCod = P00MA2_A421Status_AreaTrabalhoCod[0];
            A424Status_Nome = P00MA2_A424Status_Nome[0];
            A425Status_Tipo = P00MA2_A425Status_Tipo[0];
            A423Status_Codigo = P00MA2_A423Status_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MA2_A424Status_Nome[0], A424Status_Nome) == 0 ) )
            {
               BRKMA2 = false;
               A423Status_Codigo = P00MA2_A423Status_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKMA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A424Status_Nome)) )
            {
               AV18Option = A424Status_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMA2 )
            {
               BRKMA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFStatus_Nome = "";
         AV11TFStatus_Nome_Sel = "";
         AV12TFStatus_Tipo_SelsJson = "";
         AV13TFStatus_Tipo_Sels = new GxSimpleCollection();
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Status_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV37Status_Nome2 = "";
         AV39DynamicFiltersSelector3 = "";
         AV40Status_Nome3 = "";
         scmdbuf = "";
         lV34Status_Nome1 = "";
         lV37Status_Nome2 = "";
         lV40Status_Nome3 = "";
         lV10TFStatus_Nome = "";
         A424Status_Nome = "";
         P00MA2_A421Status_AreaTrabalhoCod = new int[1] ;
         P00MA2_A424Status_Nome = new String[] {""} ;
         P00MA2_A425Status_Tipo = new short[1] ;
         P00MA2_A423Status_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptstatusfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MA2_A421Status_AreaTrabalhoCod, P00MA2_A424Status_Nome, P00MA2_A425Status_Tipo, P00MA2_A423Status_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A425Status_Tipo ;
      private int AV43GXV1 ;
      private int AV32Status_AreaTrabalhoCod ;
      private int AV13TFStatus_Tipo_Sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A421Status_AreaTrabalhoCod ;
      private int A423Status_Codigo ;
      private long AV26count ;
      private String AV10TFStatus_Nome ;
      private String AV11TFStatus_Nome_Sel ;
      private String AV34Status_Nome1 ;
      private String AV37Status_Nome2 ;
      private String AV40Status_Nome3 ;
      private String scmdbuf ;
      private String lV34Status_Nome1 ;
      private String lV37Status_Nome2 ;
      private String lV40Status_Nome3 ;
      private String lV10TFStatus_Nome ;
      private String A424Status_Nome ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool BRKMA2 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV12TFStatus_Tipo_SelsJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00MA2_A421Status_AreaTrabalhoCod ;
      private String[] P00MA2_A424Status_Nome ;
      private short[] P00MA2_A425Status_Tipo ;
      private int[] P00MA2_A423Status_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13TFStatus_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getpromptstatusfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MA2( IGxContext context ,
                                             short A425Status_Tipo ,
                                             IGxCollection AV13TFStatus_Tipo_Sels ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Status_Nome1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             String AV37Status_Nome2 ,
                                             bool AV38DynamicFiltersEnabled3 ,
                                             String AV39DynamicFiltersSelector3 ,
                                             String AV40Status_Nome3 ,
                                             String AV11TFStatus_Nome_Sel ,
                                             String AV10TFStatus_Nome ,
                                             int AV13TFStatus_Tipo_Sels_Count ,
                                             String A424Status_Nome ,
                                             int A421Status_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Status_AreaTrabalhoCod], [Status_Nome], [Status_Tipo], [Status_Codigo] FROM [Status] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Status_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Status_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV34Status_Nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Status_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV37Status_Nome2)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV38DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Status_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV40Status_Nome3)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFStatus_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFStatus_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like @lV10TFStatus_Nome)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFStatus_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] = @AV11TFStatus_Nome_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV13TFStatus_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFStatus_Tipo_Sels, "[Status_Tipo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Status_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MA2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MA2 ;
          prmP00MA2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Status_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Status_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40Status_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFStatus_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFStatus_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MA2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptstatusfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptstatusfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptstatusfilterdata") )
          {
             return  ;
          }
          getpromptstatusfilterdata worker = new getpromptstatusfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
