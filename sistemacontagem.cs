/*
               File: SistemaContagem
        Description: Sistema Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:4.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemacontagem : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemacontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemacontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contagem_SistemaCod )
      {
         this.AV17Contagem_SistemaCod = aP0_Contagem_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagem_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV17Contagem_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV17Contagem_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV19TFContagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0)));
                  AV20TFContagem_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0)));
                  AV23TFContagem_DataCriacao = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagem_DataCriacao", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
                  AV24TFContagem_DataCriacao_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagem_DataCriacao_To", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
                  AV33TFContagem_PFB = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContagem_PFB", StringUtil.LTrim( StringUtil.Str( AV33TFContagem_PFB, 13, 5)));
                  AV34TFContagem_PFB_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContagem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV34TFContagem_PFB_To, 13, 5)));
                  AV37TFContagem_PFL = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContagem_PFL", StringUtil.LTrim( StringUtil.Str( AV37TFContagem_PFL, 13, 5)));
                  AV38TFContagem_PFL_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContagem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV38TFContagem_PFL_To, 13, 5)));
                  AV17Contagem_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0)));
                  AV21ddo_Contagem_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contagem_CodigoTitleControlIdToReplace", AV21ddo_Contagem_CodigoTitleControlIdToReplace);
                  AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace", AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace);
                  AV31ddo_Contagem_TipoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Contagem_TipoTitleControlIdToReplace", AV31ddo_Contagem_TipoTitleControlIdToReplace);
                  AV35ddo_Contagem_PFBTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Contagem_PFBTitleControlIdToReplace", AV35ddo_Contagem_PFBTitleControlIdToReplace);
                  AV39ddo_Contagem_PFLTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_Contagem_PFLTitleControlIdToReplace", AV39ddo_Contagem_PFLTitleControlIdToReplace);
                  AV47Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV30TFContagem_Tipo_Sels);
                  A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGI2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV47Pgmname = "SistemaContagem";
               context.Gx_err = 0;
               WSGI2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Contagem") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020521182439");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemacontagem.aspx") + "?" + UrlEncode("" +AV17Contagem_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFContagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20TFContagem_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO_TO", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_PFB", StringUtil.LTrim( StringUtil.NToC( AV33TFContagem_PFB, 13, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_PFB_TO", StringUtil.LTrim( StringUtil.NToC( AV34TFContagem_PFB_To, 13, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_PFL", StringUtil.LTrim( StringUtil.NToC( AV37TFContagem_PFL, 13, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEM_PFL_TO", StringUtil.LTrim( StringUtil.NToC( AV38TFContagem_PFL_To, 13, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEM_CODIGOTITLEFILTERDATA", AV18Contagem_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEM_CODIGOTITLEFILTERDATA", AV18Contagem_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEM_DATACRIACAOTITLEFILTERDATA", AV22Contagem_DataCriacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEM_DATACRIACAOTITLEFILTERDATA", AV22Contagem_DataCriacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEM_TIPOTITLEFILTERDATA", AV28Contagem_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEM_TIPOTITLEFILTERDATA", AV28Contagem_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEM_PFBTITLEFILTERDATA", AV32Contagem_PFBTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEM_PFBTITLEFILTERDATA", AV32Contagem_PFBTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEM_PFLTITLEFILTERDATA", AV36Contagem_PFLTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEM_PFLTITLEFILTERDATA", AV36Contagem_PFLTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV17Contagem_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEM_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contagem_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV47Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTAGEM_TIPO_SELS", AV30TFContagem_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTAGEM_TIPO_SELS", AV30TFContagem_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Caption", StringUtil.RTrim( Ddo_contagem_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagem_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Cls", StringUtil.RTrim( Ddo_contagem_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagem_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagem_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagem_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagem_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagem_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagem_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagem_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagem_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagem_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Caption", StringUtil.RTrim( Ddo_contagem_datacriacao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Tooltip", StringUtil.RTrim( Ddo_contagem_datacriacao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Cls", StringUtil.RTrim( Ddo_contagem_datacriacao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtext_set", StringUtil.RTrim( Ddo_contagem_datacriacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagem_datacriacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_datacriacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_datacriacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_datacriacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_datacriacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortedstatus", StringUtil.RTrim( Ddo_contagem_datacriacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includefilter", StringUtil.BoolToStr( Ddo_contagem_datacriacao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filtertype", StringUtil.RTrim( Ddo_contagem_datacriacao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filterisrange", StringUtil.BoolToStr( Ddo_contagem_datacriacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_datacriacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortasc", StringUtil.RTrim( Ddo_contagem_datacriacao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortdsc", StringUtil.RTrim( Ddo_contagem_datacriacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Cleanfilter", StringUtil.RTrim( Ddo_contagem_datacriacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagem_datacriacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Rangefilterto", StringUtil.RTrim( Ddo_contagem_datacriacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_datacriacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Caption", StringUtil.RTrim( Ddo_contagem_tipo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Tooltip", StringUtil.RTrim( Ddo_contagem_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Cls", StringUtil.RTrim( Ddo_contagem_tipo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_contagem_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_contagem_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_contagem_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Datalisttype", StringUtil.RTrim( Ddo_contagem_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagem_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagem_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Sortasc", StringUtil.RTrim( Ddo_contagem_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Sortdsc", StringUtil.RTrim( Ddo_contagem_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_contagem_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Caption", StringUtil.RTrim( Ddo_contagem_pfb_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Tooltip", StringUtil.RTrim( Ddo_contagem_pfb_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Cls", StringUtil.RTrim( Ddo_contagem_pfb_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filteredtext_set", StringUtil.RTrim( Ddo_contagem_pfb_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filteredtextto_set", StringUtil.RTrim( Ddo_contagem_pfb_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_pfb_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_pfb_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_pfb_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_pfb_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Sortedstatus", StringUtil.RTrim( Ddo_contagem_pfb_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Includefilter", StringUtil.BoolToStr( Ddo_contagem_pfb_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filtertype", StringUtil.RTrim( Ddo_contagem_pfb_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filterisrange", StringUtil.BoolToStr( Ddo_contagem_pfb_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_pfb_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Sortasc", StringUtil.RTrim( Ddo_contagem_pfb_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Sortdsc", StringUtil.RTrim( Ddo_contagem_pfb_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Cleanfilter", StringUtil.RTrim( Ddo_contagem_pfb_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Rangefilterfrom", StringUtil.RTrim( Ddo_contagem_pfb_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Rangefilterto", StringUtil.RTrim( Ddo_contagem_pfb_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_pfb_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Caption", StringUtil.RTrim( Ddo_contagem_pfl_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Tooltip", StringUtil.RTrim( Ddo_contagem_pfl_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Cls", StringUtil.RTrim( Ddo_contagem_pfl_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filteredtext_set", StringUtil.RTrim( Ddo_contagem_pfl_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filteredtextto_set", StringUtil.RTrim( Ddo_contagem_pfl_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_pfl_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_pfl_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_pfl_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_pfl_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Sortedstatus", StringUtil.RTrim( Ddo_contagem_pfl_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Includefilter", StringUtil.BoolToStr( Ddo_contagem_pfl_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filtertype", StringUtil.RTrim( Ddo_contagem_pfl_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filterisrange", StringUtil.BoolToStr( Ddo_contagem_pfl_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_pfl_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Sortasc", StringUtil.RTrim( Ddo_contagem_pfl_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Sortdsc", StringUtil.RTrim( Ddo_contagem_pfl_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Cleanfilter", StringUtil.RTrim( Ddo_contagem_pfl_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Rangefilterfrom", StringUtil.RTrim( Ddo_contagem_pfl_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Rangefilterto", StringUtil.RTrim( Ddo_contagem_pfl_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_pfl_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagem_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Activeeventkey", StringUtil.RTrim( Ddo_contagem_datacriacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtext_get", StringUtil.RTrim( Ddo_contagem_datacriacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagem_datacriacao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_contagem_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_contagem_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Activeeventkey", StringUtil.RTrim( Ddo_contagem_pfb_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filteredtext_get", StringUtil.RTrim( Ddo_contagem_pfb_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFB_Filteredtextto_get", StringUtil.RTrim( Ddo_contagem_pfb_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Activeeventkey", StringUtil.RTrim( Ddo_contagem_pfl_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filteredtext_get", StringUtil.RTrim( Ddo_contagem_pfl_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEM_PFL_Filteredtextto_get", StringUtil.RTrim( Ddo_contagem_pfl_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGI2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemacontagem.js", "?2020521182628");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Contagem" ;
      }

      protected void WBGI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemacontagem.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GI2( true) ;
         }
         else
         {
            wb_table1_2_GI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_SistemaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFContagem_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19TFContagem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20TFContagem_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20TFContagem_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagem_datacriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_datacriacao_Internalname, context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"), context.localUtil.Format( AV23TFContagem_DataCriacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_datacriacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_datacriacao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagem_datacriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagem_datacriacao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagem_datacriacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_datacriacao_to_Internalname, context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"), context.localUtil.Format( AV24TFContagem_DataCriacao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_datacriacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_datacriacao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagem_datacriacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagem_datacriacao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagem_datacriacaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagem_datacriacaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagem_datacriacaoauxdate_Internalname, context.localUtil.Format(AV25DDO_Contagem_DataCriacaoAuxDate, "99/99/99"), context.localUtil.Format( AV25DDO_Contagem_DataCriacaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagem_datacriacaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagem_datacriacaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagem_datacriacaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagem_datacriacaoauxdateto_Internalname, context.localUtil.Format(AV26DDO_Contagem_DataCriacaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV26DDO_Contagem_DataCriacaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagem_datacriacaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagem_datacriacaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_pfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV33TFContagem_PFB, 13, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV33TFContagem_PFB, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_pfb_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_pfb_Visible, 1, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_pfb_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV34TFContagem_PFB_To, 13, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV34TFContagem_PFB_To, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_pfb_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_pfb_to_Visible, 1, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_pfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV37TFContagem_PFL, 13, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV37TFContagem_PFL, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_pfl_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_pfl_Visible, 1, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_pfl_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV38TFContagem_PFL_To, 13, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV38TFContagem_PFL_To, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_pfl_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_pfl_to_Visible, 1, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEM_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname, AV21ddo_Contagem_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEM_DATACRIACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Internalname, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEM_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_tipotitlecontrolidtoreplace_Internalname, AV31ddo_Contagem_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, edtavDdo_contagem_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEM_PFBContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_pfbtitlecontrolidtoreplace_Internalname, AV35ddo_Contagem_PFBTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavDdo_contagem_pfbtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEM_PFLContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_pfltitlecontrolidtoreplace_Internalname, AV39ddo_Contagem_PFLTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_contagem_pfltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaContagem.htm");
         }
         wbLoad = true;
      }

      protected void STARTGI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Contagem", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGI0( ) ;
            }
         }
      }

      protected void WSGI2( )
      {
         STARTGI2( ) ;
         EVTGI2( ) ;
      }

      protected void EVTGI2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GI2 */
                                    E11GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GI2 */
                                    E12GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_DATACRIACAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GI2 */
                                    E13GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GI2 */
                                    E14GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_PFB.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15GI2 */
                                    E15GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_PFL.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16GI2 */
                                    E16GI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGI0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV15PlanoDeContagem = cgiGet( edtavPlanodecontagem_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPlanodecontagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15PlanoDeContagem)) ? AV46Planodecontagem_GXI : context.convertURL( context.PathToRelativeUrl( AV15PlanoDeContagem))));
                              A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                              A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
                              cmbContagem_Tipo.Name = cmbContagem_Tipo_Internalname;
                              cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
                              A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
                              n196Contagem_Tipo = false;
                              A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
                              n943Contagem_PFB = false;
                              A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
                              n944Contagem_PFL = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17GI2 */
                                          E17GI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18GI2 */
                                          E18GI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19GI2 */
                                          E19GI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_CODIGO"), ",", ".") != Convert.ToDecimal( AV19TFContagem_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV20TFContagem_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_datacriacao Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO"), 0) != AV23TFContagem_DataCriacao )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_datacriacao_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO_TO"), 0) != AV24TFContagem_DataCriacao_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_pfb Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFB"), ",", ".") != AV33TFContagem_PFB )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_pfb_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFB_TO"), ",", ".") != AV34TFContagem_PFB_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_pfl Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFL"), ",", ".") != AV37TFContagem_PFL )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagem_pfl_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFL_TO"), ",", ".") != AV38TFContagem_PFL_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPGI0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGI2( ) ;
            }
         }
      }

      protected void PAGI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_8_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV19TFContagem_Codigo ,
                                       int AV20TFContagem_Codigo_To ,
                                       DateTime AV23TFContagem_DataCriacao ,
                                       DateTime AV24TFContagem_DataCriacao_To ,
                                       decimal AV33TFContagem_PFB ,
                                       decimal AV34TFContagem_PFB_To ,
                                       decimal AV37TFContagem_PFL ,
                                       decimal AV38TFContagem_PFL_To ,
                                       int AV17Contagem_SistemaCod ,
                                       String AV21ddo_Contagem_CodigoTitleControlIdToReplace ,
                                       String AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace ,
                                       String AV31ddo_Contagem_TipoTitleControlIdToReplace ,
                                       String AV35ddo_Contagem_PFBTitleControlIdToReplace ,
                                       String AV39ddo_Contagem_PFLTitleControlIdToReplace ,
                                       String AV47Pgmname ,
                                       IGxCollection AV30TFContagem_Tipo_Sels ,
                                       int A192Contagem_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGI2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( sPrefix, A197Contagem_DataCriacao));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_DATACRIACAO", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_TIPO", StringUtil.RTrim( A196Contagem_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_PFB", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_PFL", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV47Pgmname = "SistemaContagem";
         context.Gx_err = 0;
      }

      protected void RFGI2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E18GI2 */
         E18GI2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A196Contagem_Tipo ,
                                                 AV30TFContagem_Tipo_Sels ,
                                                 AV19TFContagem_Codigo ,
                                                 AV20TFContagem_Codigo_To ,
                                                 AV23TFContagem_DataCriacao ,
                                                 AV24TFContagem_DataCriacao_To ,
                                                 AV30TFContagem_Tipo_Sels.Count ,
                                                 AV33TFContagem_PFB ,
                                                 AV34TFContagem_PFB_To ,
                                                 AV37TFContagem_PFL ,
                                                 AV38TFContagem_PFL_To ,
                                                 A192Contagem_Codigo ,
                                                 A197Contagem_DataCriacao ,
                                                 A943Contagem_PFB ,
                                                 A944Contagem_PFL ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A940Contagem_SistemaCod ,
                                                 AV17Contagem_SistemaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00GI2 */
            pr_default.execute(0, new Object[] {AV17Contagem_SistemaCod, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A940Contagem_SistemaCod = H00GI2_A940Contagem_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               n940Contagem_SistemaCod = H00GI2_n940Contagem_SistemaCod[0];
               A944Contagem_PFL = H00GI2_A944Contagem_PFL[0];
               n944Contagem_PFL = H00GI2_n944Contagem_PFL[0];
               A943Contagem_PFB = H00GI2_A943Contagem_PFB[0];
               n943Contagem_PFB = H00GI2_n943Contagem_PFB[0];
               A196Contagem_Tipo = H00GI2_A196Contagem_Tipo[0];
               n196Contagem_Tipo = H00GI2_n196Contagem_Tipo[0];
               A197Contagem_DataCriacao = H00GI2_A197Contagem_DataCriacao[0];
               A192Contagem_Codigo = H00GI2_A192Contagem_Codigo[0];
               /* Execute user event: E19GI2 */
               E19GI2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBGI0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A196Contagem_Tipo ,
                                              AV30TFContagem_Tipo_Sels ,
                                              AV19TFContagem_Codigo ,
                                              AV20TFContagem_Codigo_To ,
                                              AV23TFContagem_DataCriacao ,
                                              AV24TFContagem_DataCriacao_To ,
                                              AV30TFContagem_Tipo_Sels.Count ,
                                              AV33TFContagem_PFB ,
                                              AV34TFContagem_PFB_To ,
                                              AV37TFContagem_PFL ,
                                              AV38TFContagem_PFL_To ,
                                              A192Contagem_Codigo ,
                                              A197Contagem_DataCriacao ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A940Contagem_SistemaCod ,
                                              AV17Contagem_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor H00GI3 */
         pr_default.execute(1, new Object[] {AV17Contagem_SistemaCod, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To});
         GRID_nRecordCount = H00GI3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContagem_Codigo, AV20TFContagem_Codigo_To, AV23TFContagem_DataCriacao, AV24TFContagem_DataCriacao_To, AV33TFContagem_PFB, AV34TFContagem_PFB_To, AV37TFContagem_PFL, AV38TFContagem_PFL_To, AV17Contagem_SistemaCod, AV21ddo_Contagem_CodigoTitleControlIdToReplace, AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, AV31ddo_Contagem_TipoTitleControlIdToReplace, AV35ddo_Contagem_PFBTitleControlIdToReplace, AV39ddo_Contagem_PFLTitleControlIdToReplace, AV47Pgmname, AV30TFContagem_Tipo_Sels, A192Contagem_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGI0( )
      {
         /* Before Start, stand alone formulas. */
         AV47Pgmname = "SistemaContagem";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17GI2 */
         E17GI2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV40DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEM_CODIGOTITLEFILTERDATA"), AV18Contagem_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEM_DATACRIACAOTITLEFILTERDATA"), AV22Contagem_DataCriacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEM_TIPOTITLEFILTERDATA"), AV28Contagem_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEM_PFBTITLEFILTERDATA"), AV32Contagem_PFBTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEM_PFLTITLEFILTERDATA"), AV36Contagem_PFLTitleFilterData);
            /* Read variables values. */
            A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
            n940Contagem_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_CODIGO");
               GX_FocusControl = edtavTfcontagem_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19TFContagem_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0)));
            }
            else
            {
               AV19TFContagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_CODIGO_TO");
               GX_FocusControl = edtavTfcontagem_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20TFContagem_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0)));
            }
            else
            {
               AV20TFContagem_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagem_datacriacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem_Data Criacao"}), 1, "vTFCONTAGEM_DATACRIACAO");
               GX_FocusControl = edtavTfcontagem_datacriacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFContagem_DataCriacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagem_DataCriacao", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
            }
            else
            {
               AV23TFContagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagem_datacriacao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagem_DataCriacao", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagem_datacriacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem_Data Criacao_To"}), 1, "vTFCONTAGEM_DATACRIACAO_TO");
               GX_FocusControl = edtavTfcontagem_datacriacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24TFContagem_DataCriacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagem_DataCriacao_To", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
            }
            else
            {
               AV24TFContagem_DataCriacao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagem_datacriacao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagem_DataCriacao_To", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagem_datacriacaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem_Data Criacao Aux Date"}), 1, "vDDO_CONTAGEM_DATACRIACAOAUXDATE");
               GX_FocusControl = edtavDdo_contagem_datacriacaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25DDO_Contagem_DataCriacaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_Contagem_DataCriacaoAuxDate", context.localUtil.Format(AV25DDO_Contagem_DataCriacaoAuxDate, "99/99/99"));
            }
            else
            {
               AV25DDO_Contagem_DataCriacaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagem_datacriacaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_Contagem_DataCriacaoAuxDate", context.localUtil.Format(AV25DDO_Contagem_DataCriacaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagem_datacriacaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem_Data Criacao Aux Date To"}), 1, "vDDO_CONTAGEM_DATACRIACAOAUXDATETO");
               GX_FocusControl = edtavDdo_contagem_datacriacaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26DDO_Contagem_DataCriacaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_Contagem_DataCriacaoAuxDateTo", context.localUtil.Format(AV26DDO_Contagem_DataCriacaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV26DDO_Contagem_DataCriacaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagem_datacriacaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_Contagem_DataCriacaoAuxDateTo", context.localUtil.Format(AV26DDO_Contagem_DataCriacaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_Internalname), ",", ".") > 9999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_PFB");
               GX_FocusControl = edtavTfcontagem_pfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFContagem_PFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContagem_PFB", StringUtil.LTrim( StringUtil.Str( AV33TFContagem_PFB, 13, 5)));
            }
            else
            {
               AV33TFContagem_PFB = context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContagem_PFB", StringUtil.LTrim( StringUtil.Str( AV33TFContagem_PFB, 13, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_to_Internalname), ",", ".") > 9999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_PFB_TO");
               GX_FocusControl = edtavTfcontagem_pfb_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFContagem_PFB_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContagem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV34TFContagem_PFB_To, 13, 5)));
            }
            else
            {
               AV34TFContagem_PFB_To = context.localUtil.CToN( cgiGet( edtavTfcontagem_pfb_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContagem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV34TFContagem_PFB_To, 13, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_Internalname), ",", ".") > 9999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_PFL");
               GX_FocusControl = edtavTfcontagem_pfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContagem_PFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContagem_PFL", StringUtil.LTrim( StringUtil.Str( AV37TFContagem_PFL, 13, 5)));
            }
            else
            {
               AV37TFContagem_PFL = context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContagem_PFL", StringUtil.LTrim( StringUtil.Str( AV37TFContagem_PFL, 13, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_to_Internalname), ",", ".") > 9999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_PFL_TO");
               GX_FocusControl = edtavTfcontagem_pfl_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContagem_PFL_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContagem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV38TFContagem_PFL_To, 13, 5)));
            }
            else
            {
               AV38TFContagem_PFL_To = context.localUtil.CToN( cgiGet( edtavTfcontagem_pfl_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContagem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV38TFContagem_PFL_To, 13, 5)));
            }
            AV21ddo_Contagem_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contagem_CodigoTitleControlIdToReplace", AV21ddo_Contagem_CodigoTitleControlIdToReplace);
            AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace = cgiGet( edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace", AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace);
            AV31ddo_Contagem_TipoTitleControlIdToReplace = cgiGet( edtavDdo_contagem_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Contagem_TipoTitleControlIdToReplace", AV31ddo_Contagem_TipoTitleControlIdToReplace);
            AV35ddo_Contagem_PFBTitleControlIdToReplace = cgiGet( edtavDdo_contagem_pfbtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Contagem_PFBTitleControlIdToReplace", AV35ddo_Contagem_PFBTitleControlIdToReplace);
            AV39ddo_Contagem_PFLTitleControlIdToReplace = cgiGet( edtavDdo_contagem_pfltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_Contagem_PFLTitleControlIdToReplace", AV39ddo_Contagem_PFLTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV42GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV43GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV17Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17Contagem_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagem_codigo_Caption = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Caption");
            Ddo_contagem_codigo_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Tooltip");
            Ddo_contagem_codigo_Cls = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Cls");
            Ddo_contagem_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtext_set");
            Ddo_contagem_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtextto_set");
            Ddo_contagem_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Dropdownoptionstype");
            Ddo_contagem_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagem_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Includesortasc"));
            Ddo_contagem_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Includesortdsc"));
            Ddo_contagem_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Sortedstatus");
            Ddo_contagem_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Includefilter"));
            Ddo_contagem_codigo_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filtertype");
            Ddo_contagem_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filterisrange"));
            Ddo_contagem_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Includedatalist"));
            Ddo_contagem_codigo_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Sortasc");
            Ddo_contagem_codigo_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Sortdsc");
            Ddo_contagem_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Cleanfilter");
            Ddo_contagem_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Rangefilterfrom");
            Ddo_contagem_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Rangefilterto");
            Ddo_contagem_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Searchbuttontext");
            Ddo_contagem_datacriacao_Caption = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Caption");
            Ddo_contagem_datacriacao_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Tooltip");
            Ddo_contagem_datacriacao_Cls = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Cls");
            Ddo_contagem_datacriacao_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtext_set");
            Ddo_contagem_datacriacao_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtextto_set");
            Ddo_contagem_datacriacao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Dropdownoptionstype");
            Ddo_contagem_datacriacao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Titlecontrolidtoreplace");
            Ddo_contagem_datacriacao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includesortasc"));
            Ddo_contagem_datacriacao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includesortdsc"));
            Ddo_contagem_datacriacao_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortedstatus");
            Ddo_contagem_datacriacao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includefilter"));
            Ddo_contagem_datacriacao_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filtertype");
            Ddo_contagem_datacriacao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filterisrange"));
            Ddo_contagem_datacriacao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Includedatalist"));
            Ddo_contagem_datacriacao_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortasc");
            Ddo_contagem_datacriacao_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Sortdsc");
            Ddo_contagem_datacriacao_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Cleanfilter");
            Ddo_contagem_datacriacao_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Rangefilterfrom");
            Ddo_contagem_datacriacao_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Rangefilterto");
            Ddo_contagem_datacriacao_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Searchbuttontext");
            Ddo_contagem_tipo_Caption = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Caption");
            Ddo_contagem_tipo_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Tooltip");
            Ddo_contagem_tipo_Cls = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Cls");
            Ddo_contagem_tipo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Selectedvalue_set");
            Ddo_contagem_tipo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Dropdownoptionstype");
            Ddo_contagem_tipo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Titlecontrolidtoreplace");
            Ddo_contagem_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Includesortasc"));
            Ddo_contagem_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Includesortdsc"));
            Ddo_contagem_tipo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Sortedstatus");
            Ddo_contagem_tipo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Includefilter"));
            Ddo_contagem_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Includedatalist"));
            Ddo_contagem_tipo_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Datalisttype");
            Ddo_contagem_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Allowmultipleselection"));
            Ddo_contagem_tipo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Datalistfixedvalues");
            Ddo_contagem_tipo_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Sortasc");
            Ddo_contagem_tipo_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Sortdsc");
            Ddo_contagem_tipo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Cleanfilter");
            Ddo_contagem_tipo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Searchbuttontext");
            Ddo_contagem_pfb_Caption = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Caption");
            Ddo_contagem_pfb_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Tooltip");
            Ddo_contagem_pfb_Cls = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Cls");
            Ddo_contagem_pfb_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filteredtext_set");
            Ddo_contagem_pfb_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filteredtextto_set");
            Ddo_contagem_pfb_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Dropdownoptionstype");
            Ddo_contagem_pfb_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Titlecontrolidtoreplace");
            Ddo_contagem_pfb_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Includesortasc"));
            Ddo_contagem_pfb_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Includesortdsc"));
            Ddo_contagem_pfb_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Sortedstatus");
            Ddo_contagem_pfb_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Includefilter"));
            Ddo_contagem_pfb_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filtertype");
            Ddo_contagem_pfb_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filterisrange"));
            Ddo_contagem_pfb_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Includedatalist"));
            Ddo_contagem_pfb_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Sortasc");
            Ddo_contagem_pfb_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Sortdsc");
            Ddo_contagem_pfb_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Cleanfilter");
            Ddo_contagem_pfb_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Rangefilterfrom");
            Ddo_contagem_pfb_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Rangefilterto");
            Ddo_contagem_pfb_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Searchbuttontext");
            Ddo_contagem_pfl_Caption = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Caption");
            Ddo_contagem_pfl_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Tooltip");
            Ddo_contagem_pfl_Cls = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Cls");
            Ddo_contagem_pfl_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filteredtext_set");
            Ddo_contagem_pfl_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filteredtextto_set");
            Ddo_contagem_pfl_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Dropdownoptionstype");
            Ddo_contagem_pfl_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Titlecontrolidtoreplace");
            Ddo_contagem_pfl_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Includesortasc"));
            Ddo_contagem_pfl_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Includesortdsc"));
            Ddo_contagem_pfl_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Sortedstatus");
            Ddo_contagem_pfl_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Includefilter"));
            Ddo_contagem_pfl_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filtertype");
            Ddo_contagem_pfl_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filterisrange"));
            Ddo_contagem_pfl_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Includedatalist"));
            Ddo_contagem_pfl_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Sortasc");
            Ddo_contagem_pfl_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Sortdsc");
            Ddo_contagem_pfl_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Cleanfilter");
            Ddo_contagem_pfl_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Rangefilterfrom");
            Ddo_contagem_pfl_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Rangefilterto");
            Ddo_contagem_pfl_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagem_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Activeeventkey");
            Ddo_contagem_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtext_get");
            Ddo_contagem_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEM_CODIGO_Filteredtextto_get");
            Ddo_contagem_datacriacao_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Activeeventkey");
            Ddo_contagem_datacriacao_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtext_get");
            Ddo_contagem_datacriacao_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEM_DATACRIACAO_Filteredtextto_get");
            Ddo_contagem_tipo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Activeeventkey");
            Ddo_contagem_tipo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEM_TIPO_Selectedvalue_get");
            Ddo_contagem_pfb_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Activeeventkey");
            Ddo_contagem_pfb_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filteredtext_get");
            Ddo_contagem_pfb_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEM_PFB_Filteredtextto_get");
            Ddo_contagem_pfl_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Activeeventkey");
            Ddo_contagem_pfl_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filteredtext_get");
            Ddo_contagem_pfl_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEM_PFL_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_CODIGO"), ",", ".") != Convert.ToDecimal( AV19TFContagem_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV20TFContagem_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO"), 0) != AV23TFContagem_DataCriacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEM_DATACRIACAO_TO"), 0) != AV24TFContagem_DataCriacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFB"), ",", ".") != AV33TFContagem_PFB )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFB_TO"), ",", ".") != AV34TFContagem_PFB_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFL"), ",", ".") != AV37TFContagem_PFL )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEM_PFL_TO"), ",", ".") != AV38TFContagem_PFL_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17GI2 */
         E17GI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17GI2( )
      {
         /* Start Routine */
         subGrid_Rows = 50;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontagem_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_codigo_Visible), 5, 0)));
         edtavTfcontagem_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_codigo_to_Visible), 5, 0)));
         edtavTfcontagem_datacriacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_datacriacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_datacriacao_Visible), 5, 0)));
         edtavTfcontagem_datacriacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_datacriacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_datacriacao_to_Visible), 5, 0)));
         edtavTfcontagem_pfb_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_pfb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_pfb_Visible), 5, 0)));
         edtavTfcontagem_pfb_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_pfb_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_pfb_to_Visible), 5, 0)));
         edtavTfcontagem_pfl_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_pfl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_pfl_Visible), 5, 0)));
         edtavTfcontagem_pfl_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagem_pfl_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_pfl_to_Visible), 5, 0)));
         Ddo_contagem_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagem_codigo_Titlecontrolidtoreplace);
         AV21ddo_Contagem_CodigoTitleControlIdToReplace = Ddo_contagem_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contagem_CodigoTitleControlIdToReplace", AV21ddo_Contagem_CodigoTitleControlIdToReplace);
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagem_datacriacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_DataCriacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "TitleControlIdToReplace", Ddo_contagem_datacriacao_Titlecontrolidtoreplace);
         AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace = Ddo_contagem_datacriacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace", AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace);
         edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagem_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "TitleControlIdToReplace", Ddo_contagem_tipo_Titlecontrolidtoreplace);
         AV31ddo_Contagem_TipoTitleControlIdToReplace = Ddo_contagem_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Contagem_TipoTitleControlIdToReplace", AV31ddo_Contagem_TipoTitleControlIdToReplace);
         edtavDdo_contagem_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagem_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagem_pfb_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_PFB";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "TitleControlIdToReplace", Ddo_contagem_pfb_Titlecontrolidtoreplace);
         AV35ddo_Contagem_PFBTitleControlIdToReplace = Ddo_contagem_pfb_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Contagem_PFBTitleControlIdToReplace", AV35ddo_Contagem_PFBTitleControlIdToReplace);
         edtavDdo_contagem_pfbtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagem_pfbtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_pfbtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagem_pfl_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_PFL";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "TitleControlIdToReplace", Ddo_contagem_pfl_Titlecontrolidtoreplace);
         AV39ddo_Contagem_PFLTitleControlIdToReplace = Ddo_contagem_pfl_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_Contagem_PFLTitleControlIdToReplace", AV39ddo_Contagem_PFLTitleControlIdToReplace);
         edtavDdo_contagem_pfltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagem_pfltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_pfltitlecontrolidtoreplace_Visible), 5, 0)));
         edtContagem_SistemaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_SistemaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV40DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV40DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18GI2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV18Contagem_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22Contagem_DataCriacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28Contagem_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32Contagem_PFBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36Contagem_PFLTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagem_Codigo_Titleformat = 2;
         edtContagem_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Id", AV21ddo_Contagem_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_Codigo_Internalname, "Title", edtContagem_Codigo_Title);
         edtContagem_DataCriacao_Titleformat = 2;
         edtContagem_DataCriacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_DataCriacao_Internalname, "Title", edtContagem_DataCriacao_Title);
         cmbContagem_Tipo_Titleformat = 2;
         cmbContagem_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV31ddo_Contagem_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tipo_Internalname, "Title", cmbContagem_Tipo.Title.Text);
         edtContagem_PFB_Titleformat = 2;
         edtContagem_PFB_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFB", AV35ddo_Contagem_PFBTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_PFB_Internalname, "Title", edtContagem_PFB_Title);
         edtContagem_PFL_Titleformat = 2;
         edtContagem_PFL_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFL", AV39ddo_Contagem_PFLTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_PFL_Internalname, "Title", edtContagem_PFL_Title);
         AV42GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridCurrentPage), 10, 0)));
         AV43GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV18Contagem_CodigoTitleFilterData", AV18Contagem_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22Contagem_DataCriacaoTitleFilterData", AV22Contagem_DataCriacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28Contagem_TipoTitleFilterData", AV28Contagem_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32Contagem_PFBTitleFilterData", AV32Contagem_PFBTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36Contagem_PFLTitleFilterData", AV36Contagem_PFLTitleFilterData);
      }

      protected void E11GI2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV41PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV41PageToGo) ;
         }
      }

      protected void E12GI2( )
      {
         /* Ddo_contagem_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV19TFContagem_Codigo = (int)(NumberUtil.Val( Ddo_contagem_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0)));
            AV20TFContagem_Codigo_To = (int)(NumberUtil.Val( Ddo_contagem_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13GI2( )
      {
         /* Ddo_contagem_datacriacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_datacriacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_datacriacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "SortedStatus", Ddo_contagem_datacriacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_datacriacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_datacriacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "SortedStatus", Ddo_contagem_datacriacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_datacriacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFContagem_DataCriacao = context.localUtil.CToD( Ddo_contagem_datacriacao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagem_DataCriacao", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
            AV24TFContagem_DataCriacao_To = context.localUtil.CToD( Ddo_contagem_datacriacao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagem_DataCriacao_To", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14GI2( )
      {
         /* Ddo_contagem_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "SortedStatus", Ddo_contagem_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "SortedStatus", Ddo_contagem_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFContagem_Tipo_SelsJson = Ddo_contagem_tipo_Selectedvalue_get;
            AV30TFContagem_Tipo_Sels.FromJSonString(AV29TFContagem_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV30TFContagem_Tipo_Sels", AV30TFContagem_Tipo_Sels);
      }

      protected void E15GI2( )
      {
         /* Ddo_contagem_pfb_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_pfb_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_pfb_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "SortedStatus", Ddo_contagem_pfb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_pfb_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_pfb_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "SortedStatus", Ddo_contagem_pfb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_pfb_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFContagem_PFB = NumberUtil.Val( Ddo_contagem_pfb_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContagem_PFB", StringUtil.LTrim( StringUtil.Str( AV33TFContagem_PFB, 13, 5)));
            AV34TFContagem_PFB_To = NumberUtil.Val( Ddo_contagem_pfb_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContagem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV34TFContagem_PFB_To, 13, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16GI2( )
      {
         /* Ddo_contagem_pfl_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_pfl_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_pfl_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "SortedStatus", Ddo_contagem_pfl_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_pfl_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_pfl_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "SortedStatus", Ddo_contagem_pfl_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_pfl_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFContagem_PFL = NumberUtil.Val( Ddo_contagem_pfl_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContagem_PFL", StringUtil.LTrim( StringUtil.Str( AV37TFContagem_PFL, 13, 5)));
            AV38TFContagem_PFL_To = NumberUtil.Val( Ddo_contagem_pfl_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContagem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV38TFContagem_PFL_To, 13, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E19GI2( )
      {
         /* Grid_Load Routine */
         edtavPlanodecontagem_Linktarget = "_blank";
         AV15PlanoDeContagem = context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPlanodecontagem_Internalname, AV15PlanoDeContagem);
         AV46Planodecontagem_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )));
         edtavPlanodecontagem_Tooltiptext = "Plano de Contagem";
         edtavPlanodecontagem_Link = formatLink("arel_planodecontagem.aspx") + "?" + UrlEncode("" +A192Contagem_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagem_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
         Ddo_contagem_datacriacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "SortedStatus", Ddo_contagem_datacriacao_Sortedstatus);
         Ddo_contagem_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "SortedStatus", Ddo_contagem_tipo_Sortedstatus);
         Ddo_contagem_pfb_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "SortedStatus", Ddo_contagem_pfb_Sortedstatus);
         Ddo_contagem_pfl_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "SortedStatus", Ddo_contagem_pfl_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contagem_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contagem_datacriacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "SortedStatus", Ddo_contagem_datacriacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagem_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "SortedStatus", Ddo_contagem_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagem_pfb_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "SortedStatus", Ddo_contagem_pfb_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagem_pfl_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "SortedStatus", Ddo_contagem_pfl_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV47Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV47Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV16Session.Get(AV47Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_CODIGO") == 0 )
            {
               AV19TFContagem_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0)));
               AV20TFContagem_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0)));
               if ( ! (0==AV19TFContagem_Codigo) )
               {
                  Ddo_contagem_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "FilteredText_set", Ddo_contagem_codigo_Filteredtext_set);
               }
               if ( ! (0==AV20TFContagem_Codigo_To) )
               {
                  Ddo_contagem_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_codigo_Internalname, "FilteredTextTo_set", Ddo_contagem_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DATACRIACAO") == 0 )
            {
               AV23TFContagem_DataCriacao = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagem_DataCriacao", context.localUtil.Format(AV23TFContagem_DataCriacao, "99/99/99"));
               AV24TFContagem_DataCriacao_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagem_DataCriacao_To", context.localUtil.Format(AV24TFContagem_DataCriacao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV23TFContagem_DataCriacao) )
               {
                  Ddo_contagem_datacriacao_Filteredtext_set = context.localUtil.DToC( AV23TFContagem_DataCriacao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "FilteredText_set", Ddo_contagem_datacriacao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV24TFContagem_DataCriacao_To) )
               {
                  Ddo_contagem_datacriacao_Filteredtextto_set = context.localUtil.DToC( AV24TFContagem_DataCriacao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_datacriacao_Internalname, "FilteredTextTo_set", Ddo_contagem_datacriacao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_TIPO_SEL") == 0 )
            {
               AV29TFContagem_Tipo_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV30TFContagem_Tipo_Sels.FromJSonString(AV29TFContagem_Tipo_SelsJson);
               if ( ! ( AV30TFContagem_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_contagem_tipo_Selectedvalue_set = AV29TFContagem_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_tipo_Internalname, "SelectedValue_set", Ddo_contagem_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_PFB") == 0 )
            {
               AV33TFContagem_PFB = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContagem_PFB", StringUtil.LTrim( StringUtil.Str( AV33TFContagem_PFB, 13, 5)));
               AV34TFContagem_PFB_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContagem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV34TFContagem_PFB_To, 13, 5)));
               if ( ! (Convert.ToDecimal(0)==AV33TFContagem_PFB) )
               {
                  Ddo_contagem_pfb_Filteredtext_set = StringUtil.Str( AV33TFContagem_PFB, 13, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "FilteredText_set", Ddo_contagem_pfb_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV34TFContagem_PFB_To) )
               {
                  Ddo_contagem_pfb_Filteredtextto_set = StringUtil.Str( AV34TFContagem_PFB_To, 13, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfb_Internalname, "FilteredTextTo_set", Ddo_contagem_pfb_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_PFL") == 0 )
            {
               AV37TFContagem_PFL = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContagem_PFL", StringUtil.LTrim( StringUtil.Str( AV37TFContagem_PFL, 13, 5)));
               AV38TFContagem_PFL_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContagem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV38TFContagem_PFL_To, 13, 5)));
               if ( ! (Convert.ToDecimal(0)==AV37TFContagem_PFL) )
               {
                  Ddo_contagem_pfl_Filteredtext_set = StringUtil.Str( AV37TFContagem_PFL, 13, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "FilteredText_set", Ddo_contagem_pfl_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV38TFContagem_PFL_To) )
               {
                  Ddo_contagem_pfl_Filteredtextto_set = StringUtil.Str( AV38TFContagem_PFL_To, 13, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagem_pfl_Internalname, "FilteredTextTo_set", Ddo_contagem_pfl_Filteredtextto_set);
               }
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV16Session.Get(AV47Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV19TFContagem_Codigo) && (0==AV20TFContagem_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV19TFContagem_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV20TFContagem_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV23TFContagem_DataCriacao) && (DateTime.MinValue==AV24TFContagem_DataCriacao_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_DATACRIACAO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV23TFContagem_DataCriacao, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV24TFContagem_DataCriacao_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV30TFContagem_Tipo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_TIPO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV30TFContagem_Tipo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV33TFContagem_PFB) && (Convert.ToDecimal(0)==AV34TFContagem_PFB_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_PFB";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV33TFContagem_PFB, 13, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV34TFContagem_PFB_To, 13, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV37TFContagem_PFL) && (Convert.ToDecimal(0)==AV38TFContagem_PFL_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_PFL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV37TFContagem_PFL, 13, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV38TFContagem_PFL_To, 13, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV47Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV47Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Contagem";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contagem_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_GI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_GI2( true) ;
         }
         else
         {
            wb_table2_5_GI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GI2e( true) ;
         }
         else
         {
            wb_table1_2_GI2e( false) ;
         }
      }

      protected void wb_table2_5_GI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_DataCriacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_DataCriacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_DataCriacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_PFB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_PFB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_PFB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_PFL_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_PFL_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_PFL_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15PlanoDeContagem));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavPlanodecontagem_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavPlanodecontagem_Linktarget));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavPlanodecontagem_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_DataCriacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_DataCriacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A196Contagem_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_PFB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_PFB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_PFL_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_PFL_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GI2e( true) ;
         }
         else
         {
            wb_table2_5_GI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17Contagem_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGI2( ) ;
         WSGI2( ) ;
         WEGI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV17Contagem_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGI2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemacontagem");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGI2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV17Contagem_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0)));
         }
         wcpOAV17Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17Contagem_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV17Contagem_SistemaCod != wcpOAV17Contagem_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV17Contagem_SistemaCod = AV17Contagem_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV17Contagem_SistemaCod = cgiGet( sPrefix+"AV17Contagem_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV17Contagem_SistemaCod) > 0 )
         {
            AV17Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV17Contagem_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_SistemaCod), 6, 0)));
         }
         else
         {
            AV17Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV17Contagem_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGI2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGI2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV17Contagem_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contagem_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV17Contagem_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV17Contagem_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV17Contagem_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211821110");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemacontagem.js", "?20205211821110");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavPlanodecontagem_Internalname = sPrefix+"vPLANODECONTAGEM_"+sGXsfl_8_idx;
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO_"+sGXsfl_8_idx;
         edtContagem_DataCriacao_Internalname = sPrefix+"CONTAGEM_DATACRIACAO_"+sGXsfl_8_idx;
         cmbContagem_Tipo_Internalname = sPrefix+"CONTAGEM_TIPO_"+sGXsfl_8_idx;
         edtContagem_PFB_Internalname = sPrefix+"CONTAGEM_PFB_"+sGXsfl_8_idx;
         edtContagem_PFL_Internalname = sPrefix+"CONTAGEM_PFL_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavPlanodecontagem_Internalname = sPrefix+"vPLANODECONTAGEM_"+sGXsfl_8_fel_idx;
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagem_DataCriacao_Internalname = sPrefix+"CONTAGEM_DATACRIACAO_"+sGXsfl_8_fel_idx;
         cmbContagem_Tipo_Internalname = sPrefix+"CONTAGEM_TIPO_"+sGXsfl_8_fel_idx;
         edtContagem_PFB_Internalname = sPrefix+"CONTAGEM_PFB_"+sGXsfl_8_fel_idx;
         edtContagem_PFL_Internalname = sPrefix+"CONTAGEM_PFL_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBGI0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15PlanoDeContagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15PlanoDeContagem))&&String.IsNullOrEmpty(StringUtil.RTrim( AV46Planodecontagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15PlanoDeContagem)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavPlanodecontagem_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15PlanoDeContagem)) ? AV46Planodecontagem_GXI : context.PathToRelativeUrl( AV15PlanoDeContagem)),(String)edtavPlanodecontagem_Link,(String)edtavPlanodecontagem_Linktarget,(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavPlanodecontagem_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15PlanoDeContagem_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_DataCriacao_Internalname,context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"),context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_DataCriacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_8_idx;
               cmbContagem_Tipo.Name = GXCCtl;
               cmbContagem_Tipo.WebTags = "";
               cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
               cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
               cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
               cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
               cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
               if ( cmbContagem_Tipo.ItemCount > 0 )
               {
                  A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
                  n196Contagem_Tipo = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tipo,(String)cmbContagem_Tipo_Internalname,StringUtil.RTrim( A196Contagem_Tipo),(short)1,(String)cmbContagem_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")),context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")),context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DATACRIACAO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A197Contagem_DataCriacao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_TIPO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFB"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavPlanodecontagem_Internalname = sPrefix+"vPLANODECONTAGEM";
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO";
         edtContagem_DataCriacao_Internalname = sPrefix+"CONTAGEM_DATACRIACAO";
         cmbContagem_Tipo_Internalname = sPrefix+"CONTAGEM_TIPO";
         edtContagem_PFB_Internalname = sPrefix+"CONTAGEM_PFB";
         edtContagem_PFL_Internalname = sPrefix+"CONTAGEM_PFL";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContagem_SistemaCod_Internalname = sPrefix+"CONTAGEM_SISTEMACOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontagem_codigo_Internalname = sPrefix+"vTFCONTAGEM_CODIGO";
         edtavTfcontagem_codigo_to_Internalname = sPrefix+"vTFCONTAGEM_CODIGO_TO";
         edtavTfcontagem_datacriacao_Internalname = sPrefix+"vTFCONTAGEM_DATACRIACAO";
         edtavTfcontagem_datacriacao_to_Internalname = sPrefix+"vTFCONTAGEM_DATACRIACAO_TO";
         edtavDdo_contagem_datacriacaoauxdate_Internalname = sPrefix+"vDDO_CONTAGEM_DATACRIACAOAUXDATE";
         edtavDdo_contagem_datacriacaoauxdateto_Internalname = sPrefix+"vDDO_CONTAGEM_DATACRIACAOAUXDATETO";
         divDdo_contagem_datacriacaoauxdates_Internalname = sPrefix+"DDO_CONTAGEM_DATACRIACAOAUXDATES";
         edtavTfcontagem_pfb_Internalname = sPrefix+"vTFCONTAGEM_PFB";
         edtavTfcontagem_pfb_to_Internalname = sPrefix+"vTFCONTAGEM_PFB_TO";
         edtavTfcontagem_pfl_Internalname = sPrefix+"vTFCONTAGEM_PFL";
         edtavTfcontagem_pfl_to_Internalname = sPrefix+"vTFCONTAGEM_PFL_TO";
         Ddo_contagem_codigo_Internalname = sPrefix+"DDO_CONTAGEM_CODIGO";
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagem_datacriacao_Internalname = sPrefix+"DDO_CONTAGEM_DATACRIACAO";
         edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE";
         Ddo_contagem_tipo_Internalname = sPrefix+"DDO_CONTAGEM_TIPO";
         edtavDdo_contagem_tipotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_contagem_pfb_Internalname = sPrefix+"DDO_CONTAGEM_PFB";
         edtavDdo_contagem_pfbtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE";
         Ddo_contagem_pfl_Internalname = sPrefix+"DDO_CONTAGEM_PFL";
         edtavDdo_contagem_pfltitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFB_Jsonclick = "";
         cmbContagem_Tipo_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavPlanodecontagem_Tooltiptext = "Plano de Contagem";
         edtavPlanodecontagem_Linktarget = "";
         edtavPlanodecontagem_Link = "";
         edtContagem_PFL_Titleformat = 0;
         edtContagem_PFB_Titleformat = 0;
         cmbContagem_Tipo_Titleformat = 0;
         edtContagem_DataCriacao_Titleformat = 0;
         edtContagem_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContagem_PFL_Title = "PFL";
         edtContagem_PFB_Title = "PFB";
         cmbContagem_Tipo.Title.Text = "Tipo";
         edtContagem_DataCriacao_Title = "Data";
         edtContagem_Codigo_Title = "Id";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contagem_pfltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagem_pfbtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagem_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagem_pfl_to_Jsonclick = "";
         edtavTfcontagem_pfl_to_Visible = 1;
         edtavTfcontagem_pfl_Jsonclick = "";
         edtavTfcontagem_pfl_Visible = 1;
         edtavTfcontagem_pfb_to_Jsonclick = "";
         edtavTfcontagem_pfb_to_Visible = 1;
         edtavTfcontagem_pfb_Jsonclick = "";
         edtavTfcontagem_pfb_Visible = 1;
         edtavDdo_contagem_datacriacaoauxdateto_Jsonclick = "";
         edtavDdo_contagem_datacriacaoauxdate_Jsonclick = "";
         edtavTfcontagem_datacriacao_to_Jsonclick = "";
         edtavTfcontagem_datacriacao_to_Visible = 1;
         edtavTfcontagem_datacriacao_Jsonclick = "";
         edtavTfcontagem_datacriacao_Visible = 1;
         edtavTfcontagem_codigo_to_Jsonclick = "";
         edtavTfcontagem_codigo_to_Visible = 1;
         edtavTfcontagem_codigo_Jsonclick = "";
         edtavTfcontagem_codigo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_SistemaCod_Visible = 1;
         Ddo_contagem_pfl_Searchbuttontext = "Pesquisar";
         Ddo_contagem_pfl_Rangefilterto = "At�";
         Ddo_contagem_pfl_Rangefilterfrom = "Desde";
         Ddo_contagem_pfl_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_pfl_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_pfl_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_pfl_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagem_pfl_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagem_pfl_Filtertype = "Numeric";
         Ddo_contagem_pfl_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagem_pfl_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_pfl_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_pfl_Titlecontrolidtoreplace = "";
         Ddo_contagem_pfl_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_pfl_Cls = "ColumnSettings";
         Ddo_contagem_pfl_Tooltip = "Op��es";
         Ddo_contagem_pfl_Caption = "";
         Ddo_contagem_pfb_Searchbuttontext = "Pesquisar";
         Ddo_contagem_pfb_Rangefilterto = "At�";
         Ddo_contagem_pfb_Rangefilterfrom = "Desde";
         Ddo_contagem_pfb_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_pfb_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_pfb_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_pfb_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagem_pfb_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagem_pfb_Filtertype = "Numeric";
         Ddo_contagem_pfb_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagem_pfb_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_pfb_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_pfb_Titlecontrolidtoreplace = "";
         Ddo_contagem_pfb_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_pfb_Cls = "ColumnSettings";
         Ddo_contagem_pfb_Tooltip = "Op��es";
         Ddo_contagem_pfb_Caption = "";
         Ddo_contagem_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagem_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_tipo_Datalistfixedvalues = "D:Projeto de Desenvolvimento,M:Projeto de Melhor�a,C:Projeto de Contagem,A:Aplica��o";
         Ddo_contagem_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagem_tipo_Datalisttype = "FixedValues";
         Ddo_contagem_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagem_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagem_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_tipo_Titlecontrolidtoreplace = "";
         Ddo_contagem_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_tipo_Cls = "ColumnSettings";
         Ddo_contagem_tipo_Tooltip = "Op��es";
         Ddo_contagem_tipo_Caption = "";
         Ddo_contagem_datacriacao_Searchbuttontext = "Pesquisar";
         Ddo_contagem_datacriacao_Rangefilterto = "At�";
         Ddo_contagem_datacriacao_Rangefilterfrom = "Desde";
         Ddo_contagem_datacriacao_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_datacriacao_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_datacriacao_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_datacriacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagem_datacriacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagem_datacriacao_Filtertype = "Date";
         Ddo_contagem_datacriacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagem_datacriacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_datacriacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_datacriacao_Titlecontrolidtoreplace = "";
         Ddo_contagem_datacriacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_datacriacao_Cls = "ColumnSettings";
         Ddo_contagem_datacriacao_Tooltip = "Op��es";
         Ddo_contagem_datacriacao_Caption = "";
         Ddo_contagem_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagem_codigo_Rangefilterto = "At�";
         Ddo_contagem_codigo_Rangefilterfrom = "Desde";
         Ddo_contagem_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagem_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Filtertype = "Numeric";
         Ddo_contagem_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagem_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_codigo_Cls = "ColumnSettings";
         Ddo_contagem_codigo_Tooltip = "Op��es";
         Ddo_contagem_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0}],oparms:[{av:'AV18Contagem_CodigoTitleFilterData',fld:'vCONTAGEM_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV22Contagem_DataCriacaoTitleFilterData',fld:'vCONTAGEM_DATACRIACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV28Contagem_TipoTitleFilterData',fld:'vCONTAGEM_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32Contagem_PFBTitleFilterData',fld:'vCONTAGEM_PFBTITLEFILTERDATA',pic:'',nv:null},{av:'AV36Contagem_PFLTitleFilterData',fld:'vCONTAGEM_PFLTITLEFILTERDATA',pic:'',nv:null},{av:'edtContagem_Codigo_Titleformat',ctrl:'CONTAGEM_CODIGO',prop:'Titleformat'},{av:'edtContagem_Codigo_Title',ctrl:'CONTAGEM_CODIGO',prop:'Title'},{av:'edtContagem_DataCriacao_Titleformat',ctrl:'CONTAGEM_DATACRIACAO',prop:'Titleformat'},{av:'edtContagem_DataCriacao_Title',ctrl:'CONTAGEM_DATACRIACAO',prop:'Title'},{av:'cmbContagem_Tipo'},{av:'edtContagem_PFB_Titleformat',ctrl:'CONTAGEM_PFB',prop:'Titleformat'},{av:'edtContagem_PFB_Title',ctrl:'CONTAGEM_PFB',prop:'Title'},{av:'edtContagem_PFL_Titleformat',ctrl:'CONTAGEM_PFL',prop:'Titleformat'},{av:'edtContagem_PFL_Title',ctrl:'CONTAGEM_PFL',prop:'Title'},{av:'AV42GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV43GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEM_CODIGO.ONOPTIONCLICKED","{handler:'E12GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contagem_codigo_Activeeventkey',ctrl:'DDO_CONTAGEM_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagem_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagem_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagem_datacriacao_Sortedstatus',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'SortedStatus'},{av:'Ddo_contagem_tipo_Sortedstatus',ctrl:'DDO_CONTAGEM_TIPO',prop:'SortedStatus'},{av:'Ddo_contagem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEM_PFL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEM_DATACRIACAO.ONOPTIONCLICKED","{handler:'E13GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contagem_datacriacao_Activeeventkey',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'ActiveEventKey'},{av:'Ddo_contagem_datacriacao_Filteredtext_get',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'FilteredText_get'},{av:'Ddo_contagem_datacriacao_Filteredtextto_get',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_datacriacao_Sortedstatus',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'SortedStatus'},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagem_tipo_Sortedstatus',ctrl:'DDO_CONTAGEM_TIPO',prop:'SortedStatus'},{av:'Ddo_contagem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEM_PFL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEM_TIPO.ONOPTIONCLICKED","{handler:'E14GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contagem_tipo_Activeeventkey',ctrl:'DDO_CONTAGEM_TIPO',prop:'ActiveEventKey'},{av:'Ddo_contagem_tipo_Selectedvalue_get',ctrl:'DDO_CONTAGEM_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_tipo_Sortedstatus',ctrl:'DDO_CONTAGEM_TIPO',prop:'SortedStatus'},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagem_datacriacao_Sortedstatus',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'SortedStatus'},{av:'Ddo_contagem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEM_PFL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEM_PFB.ONOPTIONCLICKED","{handler:'E15GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contagem_pfb_Activeeventkey',ctrl:'DDO_CONTAGEM_PFB',prop:'ActiveEventKey'},{av:'Ddo_contagem_pfb_Filteredtext_get',ctrl:'DDO_CONTAGEM_PFB',prop:'FilteredText_get'},{av:'Ddo_contagem_pfb_Filteredtextto_get',ctrl:'DDO_CONTAGEM_PFB',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEM_PFB',prop:'SortedStatus'},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagem_datacriacao_Sortedstatus',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'SortedStatus'},{av:'Ddo_contagem_tipo_Sortedstatus',ctrl:'DDO_CONTAGEM_TIPO',prop:'SortedStatus'},{av:'Ddo_contagem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEM_PFL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEM_PFL.ONOPTIONCLICKED","{handler:'E16GI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV23TFContagem_DataCriacao',fld:'vTFCONTAGEM_DATACRIACAO',pic:'',nv:''},{av:'AV24TFContagem_DataCriacao_To',fld:'vTFCONTAGEM_DATACRIACAO_TO',pic:'',nv:''},{av:'AV33TFContagem_PFB',fld:'vTFCONTAGEM_PFB',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV34TFContagem_PFB_To',fld:'vTFCONTAGEM_PFB_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV17Contagem_SistemaCod',fld:'vCONTAGEM_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_DATACRIACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Contagem_TipoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Contagem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Contagem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFContagem_Tipo_Sels',fld:'vTFCONTAGEM_TIPO_SELS',pic:'',nv:null},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contagem_pfl_Activeeventkey',ctrl:'DDO_CONTAGEM_PFL',prop:'ActiveEventKey'},{av:'Ddo_contagem_pfl_Filteredtext_get',ctrl:'DDO_CONTAGEM_PFL',prop:'FilteredText_get'},{av:'Ddo_contagem_pfl_Filteredtextto_get',ctrl:'DDO_CONTAGEM_PFL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEM_PFL',prop:'SortedStatus'},{av:'AV37TFContagem_PFL',fld:'vTFCONTAGEM_PFL',pic:'ZZZZZZ9.99999',nv:0.0},{av:'AV38TFContagem_PFL_To',fld:'vTFCONTAGEM_PFL_TO',pic:'ZZZZZZ9.99999',nv:0.0},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagem_datacriacao_Sortedstatus',ctrl:'DDO_CONTAGEM_DATACRIACAO',prop:'SortedStatus'},{av:'Ddo_contagem_tipo_Sortedstatus',ctrl:'DDO_CONTAGEM_TIPO',prop:'SortedStatus'},{av:'Ddo_contagem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEM_PFB',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19GI2',iparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavPlanodecontagem_Linktarget',ctrl:'vPLANODECONTAGEM',prop:'Linktarget'},{av:'AV15PlanoDeContagem',fld:'vPLANODECONTAGEM',pic:'',nv:''},{av:'edtavPlanodecontagem_Tooltiptext',ctrl:'vPLANODECONTAGEM',prop:'Tooltiptext'},{av:'edtavPlanodecontagem_Link',ctrl:'vPLANODECONTAGEM',prop:'Link'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagem_codigo_Activeeventkey = "";
         Ddo_contagem_codigo_Filteredtext_get = "";
         Ddo_contagem_codigo_Filteredtextto_get = "";
         Ddo_contagem_datacriacao_Activeeventkey = "";
         Ddo_contagem_datacriacao_Filteredtext_get = "";
         Ddo_contagem_datacriacao_Filteredtextto_get = "";
         Ddo_contagem_tipo_Activeeventkey = "";
         Ddo_contagem_tipo_Selectedvalue_get = "";
         Ddo_contagem_pfb_Activeeventkey = "";
         Ddo_contagem_pfb_Filteredtext_get = "";
         Ddo_contagem_pfb_Filteredtextto_get = "";
         Ddo_contagem_pfl_Activeeventkey = "";
         Ddo_contagem_pfl_Filteredtext_get = "";
         Ddo_contagem_pfl_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23TFContagem_DataCriacao = DateTime.MinValue;
         AV24TFContagem_DataCriacao_To = DateTime.MinValue;
         AV21ddo_Contagem_CodigoTitleControlIdToReplace = "";
         AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace = "";
         AV31ddo_Contagem_TipoTitleControlIdToReplace = "";
         AV35ddo_Contagem_PFBTitleControlIdToReplace = "";
         AV39ddo_Contagem_PFLTitleControlIdToReplace = "";
         AV47Pgmname = "";
         AV30TFContagem_Tipo_Sels = new GxSimpleCollection();
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV40DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV18Contagem_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22Contagem_DataCriacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28Contagem_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32Contagem_PFBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36Contagem_PFLTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagem_codigo_Filteredtext_set = "";
         Ddo_contagem_codigo_Filteredtextto_set = "";
         Ddo_contagem_codigo_Sortedstatus = "";
         Ddo_contagem_datacriacao_Filteredtext_set = "";
         Ddo_contagem_datacriacao_Filteredtextto_set = "";
         Ddo_contagem_datacriacao_Sortedstatus = "";
         Ddo_contagem_tipo_Selectedvalue_set = "";
         Ddo_contagem_tipo_Sortedstatus = "";
         Ddo_contagem_pfb_Filteredtext_set = "";
         Ddo_contagem_pfb_Filteredtextto_set = "";
         Ddo_contagem_pfb_Sortedstatus = "";
         Ddo_contagem_pfl_Filteredtext_set = "";
         Ddo_contagem_pfl_Filteredtextto_set = "";
         Ddo_contagem_pfl_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV25DDO_Contagem_DataCriacaoAuxDate = DateTime.MinValue;
         AV26DDO_Contagem_DataCriacaoAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15PlanoDeContagem = "";
         AV46Planodecontagem_GXI = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A196Contagem_Tipo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00GI2_A940Contagem_SistemaCod = new int[1] ;
         H00GI2_n940Contagem_SistemaCod = new bool[] {false} ;
         H00GI2_A944Contagem_PFL = new decimal[1] ;
         H00GI2_n944Contagem_PFL = new bool[] {false} ;
         H00GI2_A943Contagem_PFB = new decimal[1] ;
         H00GI2_n943Contagem_PFB = new bool[] {false} ;
         H00GI2_A196Contagem_Tipo = new String[] {""} ;
         H00GI2_n196Contagem_Tipo = new bool[] {false} ;
         H00GI2_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00GI2_A192Contagem_Codigo = new int[1] ;
         H00GI3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29TFContagem_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV17Contagem_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemacontagem__default(),
            new Object[][] {
                new Object[] {
               H00GI2_A940Contagem_SistemaCod, H00GI2_n940Contagem_SistemaCod, H00GI2_A944Contagem_PFL, H00GI2_n944Contagem_PFL, H00GI2_A943Contagem_PFB, H00GI2_n943Contagem_PFB, H00GI2_A196Contagem_Tipo, H00GI2_n196Contagem_Tipo, H00GI2_A197Contagem_DataCriacao, H00GI2_A192Contagem_Codigo
               }
               , new Object[] {
               H00GI3_AGRID_nRecordCount
               }
            }
         );
         AV47Pgmname = "SistemaContagem";
         /* GeneXus formulas. */
         AV47Pgmname = "SistemaContagem";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagem_Codigo_Titleformat ;
      private short edtContagem_DataCriacao_Titleformat ;
      private short cmbContagem_Tipo_Titleformat ;
      private short edtContagem_PFB_Titleformat ;
      private short edtContagem_PFL_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV17Contagem_SistemaCod ;
      private int wcpOAV17Contagem_SistemaCod ;
      private int subGrid_Rows ;
      private int AV19TFContagem_Codigo ;
      private int AV20TFContagem_Codigo_To ;
      private int A192Contagem_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A940Contagem_SistemaCod ;
      private int edtContagem_SistemaCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontagem_codigo_Visible ;
      private int edtavTfcontagem_codigo_to_Visible ;
      private int edtavTfcontagem_datacriacao_Visible ;
      private int edtavTfcontagem_datacriacao_to_Visible ;
      private int edtavTfcontagem_pfb_Visible ;
      private int edtavTfcontagem_pfb_to_Visible ;
      private int edtavTfcontagem_pfl_Visible ;
      private int edtavTfcontagem_pfl_to_Visible ;
      private int edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagem_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagem_pfbtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagem_pfltitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV30TFContagem_Tipo_Sels_Count ;
      private int AV41PageToGo ;
      private int AV48GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV42GridCurrentPage ;
      private long AV43GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV33TFContagem_PFB ;
      private decimal AV34TFContagem_PFB_To ;
      private decimal AV37TFContagem_PFL ;
      private decimal AV38TFContagem_PFL_To ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagem_codigo_Activeeventkey ;
      private String Ddo_contagem_codigo_Filteredtext_get ;
      private String Ddo_contagem_codigo_Filteredtextto_get ;
      private String Ddo_contagem_datacriacao_Activeeventkey ;
      private String Ddo_contagem_datacriacao_Filteredtext_get ;
      private String Ddo_contagem_datacriacao_Filteredtextto_get ;
      private String Ddo_contagem_tipo_Activeeventkey ;
      private String Ddo_contagem_tipo_Selectedvalue_get ;
      private String Ddo_contagem_pfb_Activeeventkey ;
      private String Ddo_contagem_pfb_Filteredtext_get ;
      private String Ddo_contagem_pfb_Filteredtextto_get ;
      private String Ddo_contagem_pfl_Activeeventkey ;
      private String Ddo_contagem_pfl_Filteredtext_get ;
      private String Ddo_contagem_pfl_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV47Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagem_codigo_Caption ;
      private String Ddo_contagem_codigo_Tooltip ;
      private String Ddo_contagem_codigo_Cls ;
      private String Ddo_contagem_codigo_Filteredtext_set ;
      private String Ddo_contagem_codigo_Filteredtextto_set ;
      private String Ddo_contagem_codigo_Dropdownoptionstype ;
      private String Ddo_contagem_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagem_codigo_Sortedstatus ;
      private String Ddo_contagem_codigo_Filtertype ;
      private String Ddo_contagem_codigo_Sortasc ;
      private String Ddo_contagem_codigo_Sortdsc ;
      private String Ddo_contagem_codigo_Cleanfilter ;
      private String Ddo_contagem_codigo_Rangefilterfrom ;
      private String Ddo_contagem_codigo_Rangefilterto ;
      private String Ddo_contagem_codigo_Searchbuttontext ;
      private String Ddo_contagem_datacriacao_Caption ;
      private String Ddo_contagem_datacriacao_Tooltip ;
      private String Ddo_contagem_datacriacao_Cls ;
      private String Ddo_contagem_datacriacao_Filteredtext_set ;
      private String Ddo_contagem_datacriacao_Filteredtextto_set ;
      private String Ddo_contagem_datacriacao_Dropdownoptionstype ;
      private String Ddo_contagem_datacriacao_Titlecontrolidtoreplace ;
      private String Ddo_contagem_datacriacao_Sortedstatus ;
      private String Ddo_contagem_datacriacao_Filtertype ;
      private String Ddo_contagem_datacriacao_Sortasc ;
      private String Ddo_contagem_datacriacao_Sortdsc ;
      private String Ddo_contagem_datacriacao_Cleanfilter ;
      private String Ddo_contagem_datacriacao_Rangefilterfrom ;
      private String Ddo_contagem_datacriacao_Rangefilterto ;
      private String Ddo_contagem_datacriacao_Searchbuttontext ;
      private String Ddo_contagem_tipo_Caption ;
      private String Ddo_contagem_tipo_Tooltip ;
      private String Ddo_contagem_tipo_Cls ;
      private String Ddo_contagem_tipo_Selectedvalue_set ;
      private String Ddo_contagem_tipo_Dropdownoptionstype ;
      private String Ddo_contagem_tipo_Titlecontrolidtoreplace ;
      private String Ddo_contagem_tipo_Sortedstatus ;
      private String Ddo_contagem_tipo_Datalisttype ;
      private String Ddo_contagem_tipo_Datalistfixedvalues ;
      private String Ddo_contagem_tipo_Sortasc ;
      private String Ddo_contagem_tipo_Sortdsc ;
      private String Ddo_contagem_tipo_Cleanfilter ;
      private String Ddo_contagem_tipo_Searchbuttontext ;
      private String Ddo_contagem_pfb_Caption ;
      private String Ddo_contagem_pfb_Tooltip ;
      private String Ddo_contagem_pfb_Cls ;
      private String Ddo_contagem_pfb_Filteredtext_set ;
      private String Ddo_contagem_pfb_Filteredtextto_set ;
      private String Ddo_contagem_pfb_Dropdownoptionstype ;
      private String Ddo_contagem_pfb_Titlecontrolidtoreplace ;
      private String Ddo_contagem_pfb_Sortedstatus ;
      private String Ddo_contagem_pfb_Filtertype ;
      private String Ddo_contagem_pfb_Sortasc ;
      private String Ddo_contagem_pfb_Sortdsc ;
      private String Ddo_contagem_pfb_Cleanfilter ;
      private String Ddo_contagem_pfb_Rangefilterfrom ;
      private String Ddo_contagem_pfb_Rangefilterto ;
      private String Ddo_contagem_pfb_Searchbuttontext ;
      private String Ddo_contagem_pfl_Caption ;
      private String Ddo_contagem_pfl_Tooltip ;
      private String Ddo_contagem_pfl_Cls ;
      private String Ddo_contagem_pfl_Filteredtext_set ;
      private String Ddo_contagem_pfl_Filteredtextto_set ;
      private String Ddo_contagem_pfl_Dropdownoptionstype ;
      private String Ddo_contagem_pfl_Titlecontrolidtoreplace ;
      private String Ddo_contagem_pfl_Sortedstatus ;
      private String Ddo_contagem_pfl_Filtertype ;
      private String Ddo_contagem_pfl_Sortasc ;
      private String Ddo_contagem_pfl_Sortdsc ;
      private String Ddo_contagem_pfl_Cleanfilter ;
      private String Ddo_contagem_pfl_Rangefilterfrom ;
      private String Ddo_contagem_pfl_Rangefilterto ;
      private String Ddo_contagem_pfl_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContagem_SistemaCod_Internalname ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontagem_codigo_Internalname ;
      private String edtavTfcontagem_codigo_Jsonclick ;
      private String edtavTfcontagem_codigo_to_Internalname ;
      private String edtavTfcontagem_codigo_to_Jsonclick ;
      private String edtavTfcontagem_datacriacao_Internalname ;
      private String edtavTfcontagem_datacriacao_Jsonclick ;
      private String edtavTfcontagem_datacriacao_to_Internalname ;
      private String edtavTfcontagem_datacriacao_to_Jsonclick ;
      private String divDdo_contagem_datacriacaoauxdates_Internalname ;
      private String edtavDdo_contagem_datacriacaoauxdate_Internalname ;
      private String edtavDdo_contagem_datacriacaoauxdate_Jsonclick ;
      private String edtavDdo_contagem_datacriacaoauxdateto_Internalname ;
      private String edtavDdo_contagem_datacriacaoauxdateto_Jsonclick ;
      private String edtavTfcontagem_pfb_Internalname ;
      private String edtavTfcontagem_pfb_Jsonclick ;
      private String edtavTfcontagem_pfb_to_Internalname ;
      private String edtavTfcontagem_pfb_to_Jsonclick ;
      private String edtavTfcontagem_pfl_Internalname ;
      private String edtavTfcontagem_pfl_Jsonclick ;
      private String edtavTfcontagem_pfl_to_Internalname ;
      private String edtavTfcontagem_pfl_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagem_datacriacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagem_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagem_pfbtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagem_pfltitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPlanodecontagem_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String cmbContagem_Tipo_Internalname ;
      private String A196Contagem_Tipo ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFL_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_contagem_codigo_Internalname ;
      private String Ddo_contagem_datacriacao_Internalname ;
      private String Ddo_contagem_tipo_Internalname ;
      private String Ddo_contagem_pfb_Internalname ;
      private String Ddo_contagem_pfl_Internalname ;
      private String edtContagem_Codigo_Title ;
      private String edtContagem_DataCriacao_Title ;
      private String edtContagem_PFB_Title ;
      private String edtContagem_PFL_Title ;
      private String edtavPlanodecontagem_Linktarget ;
      private String edtavPlanodecontagem_Tooltiptext ;
      private String edtavPlanodecontagem_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV17Contagem_SistemaCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagem_Codigo_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String edtContagem_PFB_Jsonclick ;
      private String edtContagem_PFL_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV23TFContagem_DataCriacao ;
      private DateTime AV24TFContagem_DataCriacao_To ;
      private DateTime AV25DDO_Contagem_DataCriacaoAuxDate ;
      private DateTime AV26DDO_Contagem_DataCriacaoAuxDateTo ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagem_codigo_Includesortasc ;
      private bool Ddo_contagem_codigo_Includesortdsc ;
      private bool Ddo_contagem_codigo_Includefilter ;
      private bool Ddo_contagem_codigo_Filterisrange ;
      private bool Ddo_contagem_codigo_Includedatalist ;
      private bool Ddo_contagem_datacriacao_Includesortasc ;
      private bool Ddo_contagem_datacriacao_Includesortdsc ;
      private bool Ddo_contagem_datacriacao_Includefilter ;
      private bool Ddo_contagem_datacriacao_Filterisrange ;
      private bool Ddo_contagem_datacriacao_Includedatalist ;
      private bool Ddo_contagem_tipo_Includesortasc ;
      private bool Ddo_contagem_tipo_Includesortdsc ;
      private bool Ddo_contagem_tipo_Includefilter ;
      private bool Ddo_contagem_tipo_Includedatalist ;
      private bool Ddo_contagem_tipo_Allowmultipleselection ;
      private bool Ddo_contagem_pfb_Includesortasc ;
      private bool Ddo_contagem_pfb_Includesortdsc ;
      private bool Ddo_contagem_pfb_Includefilter ;
      private bool Ddo_contagem_pfb_Filterisrange ;
      private bool Ddo_contagem_pfb_Includedatalist ;
      private bool Ddo_contagem_pfl_Includesortasc ;
      private bool Ddo_contagem_pfl_Includesortdsc ;
      private bool Ddo_contagem_pfl_Includefilter ;
      private bool Ddo_contagem_pfl_Filterisrange ;
      private bool Ddo_contagem_pfl_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n196Contagem_Tipo ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool n940Contagem_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15PlanoDeContagem_IsBlob ;
      private String AV29TFContagem_Tipo_SelsJson ;
      private String AV21ddo_Contagem_CodigoTitleControlIdToReplace ;
      private String AV27ddo_Contagem_DataCriacaoTitleControlIdToReplace ;
      private String AV31ddo_Contagem_TipoTitleControlIdToReplace ;
      private String AV35ddo_Contagem_PFBTitleControlIdToReplace ;
      private String AV39ddo_Contagem_PFLTitleControlIdToReplace ;
      private String AV46Planodecontagem_GXI ;
      private String AV15PlanoDeContagem ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagem_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00GI2_A940Contagem_SistemaCod ;
      private bool[] H00GI2_n940Contagem_SistemaCod ;
      private decimal[] H00GI2_A944Contagem_PFL ;
      private bool[] H00GI2_n944Contagem_PFL ;
      private decimal[] H00GI2_A943Contagem_PFB ;
      private bool[] H00GI2_n943Contagem_PFB ;
      private String[] H00GI2_A196Contagem_Tipo ;
      private bool[] H00GI2_n196Contagem_Tipo ;
      private DateTime[] H00GI2_A197Contagem_DataCriacao ;
      private int[] H00GI2_A192Contagem_Codigo ;
      private long[] H00GI3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30TFContagem_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV18Contagem_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22Contagem_DataCriacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28Contagem_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32Contagem_PFBTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36Contagem_PFLTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV40DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class sistemacontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GI2( IGxContext context ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV30TFContagem_Tipo_Sels ,
                                             int AV19TFContagem_Codigo ,
                                             int AV20TFContagem_Codigo_To ,
                                             DateTime AV23TFContagem_DataCriacao ,
                                             DateTime AV24TFContagem_DataCriacao_To ,
                                             int AV30TFContagem_Tipo_Sels_Count ,
                                             decimal AV33TFContagem_PFB ,
                                             decimal AV34TFContagem_PFB_To ,
                                             decimal AV37TFContagem_PFL ,
                                             decimal AV38TFContagem_PFL_To ,
                                             int A192Contagem_Codigo ,
                                             DateTime A197Contagem_DataCriacao ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A940Contagem_SistemaCod ,
                                             int AV17Contagem_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Contagem_SistemaCod], [Contagem_PFL], [Contagem_PFB], [Contagem_Tipo], [Contagem_DataCriacao], [Contagem_Codigo]";
         sFromString = " FROM [Contagem] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Contagem_SistemaCod] = @AV17Contagem_SistemaCod)";
         if ( ! (0==AV19TFContagem_Codigo) )
         {
            sWhereString = sWhereString + " and ([Contagem_Codigo] >= @AV19TFContagem_Codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV20TFContagem_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_Codigo] <= @AV20TFContagem_Codigo_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFContagem_DataCriacao) )
         {
            sWhereString = sWhereString + " and ([Contagem_DataCriacao] >= @AV23TFContagem_DataCriacao)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContagem_DataCriacao_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_DataCriacao] <= @AV24TFContagem_DataCriacao_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV30TFContagem_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFContagem_Tipo_Sels, "[Contagem_Tipo] IN (", ")") + ")";
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFContagem_PFB) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFB] >= @AV33TFContagem_PFB)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFContagem_PFB_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFB] <= @AV34TFContagem_PFB_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFContagem_PFL) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFL] >= @AV37TFContagem_PFL)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV38TFContagem_PFL_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFL] <= @AV38TFContagem_PFL_To)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod], [Contagem_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod] DESC, [Contagem_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod], [Contagem_DataCriacao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod] DESC, [Contagem_DataCriacao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod], [Contagem_Tipo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod] DESC, [Contagem_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod], [Contagem_PFB]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod] DESC, [Contagem_PFB] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod], [Contagem_PFL]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_SistemaCod] DESC, [Contagem_PFL] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GI3( IGxContext context ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV30TFContagem_Tipo_Sels ,
                                             int AV19TFContagem_Codigo ,
                                             int AV20TFContagem_Codigo_To ,
                                             DateTime AV23TFContagem_DataCriacao ,
                                             DateTime AV24TFContagem_DataCriacao_To ,
                                             int AV30TFContagem_Tipo_Sels_Count ,
                                             decimal AV33TFContagem_PFB ,
                                             decimal AV34TFContagem_PFB_To ,
                                             decimal AV37TFContagem_PFL ,
                                             decimal AV38TFContagem_PFL_To ,
                                             int A192Contagem_Codigo ,
                                             DateTime A197Contagem_DataCriacao ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A940Contagem_SistemaCod ,
                                             int AV17Contagem_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Contagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contagem_SistemaCod] = @AV17Contagem_SistemaCod)";
         if ( ! (0==AV19TFContagem_Codigo) )
         {
            sWhereString = sWhereString + " and ([Contagem_Codigo] >= @AV19TFContagem_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV20TFContagem_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_Codigo] <= @AV20TFContagem_Codigo_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFContagem_DataCriacao) )
         {
            sWhereString = sWhereString + " and ([Contagem_DataCriacao] >= @AV23TFContagem_DataCriacao)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContagem_DataCriacao_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_DataCriacao] <= @AV24TFContagem_DataCriacao_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV30TFContagem_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFContagem_Tipo_Sels, "[Contagem_Tipo] IN (", ")") + ")";
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFContagem_PFB) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFB] >= @AV33TFContagem_PFB)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFContagem_PFB_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFB] <= @AV34TFContagem_PFB_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFContagem_PFL) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFL] >= @AV37TFContagem_PFL)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV38TFContagem_PFL_To) )
         {
            sWhereString = sWhereString + " and ([Contagem_PFL] <= @AV38TFContagem_PFL_To)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GI2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (int)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
               case 1 :
                     return conditional_H00GI3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (int)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GI2 ;
          prmH00GI2 = new Object[] {
          new Object[] {"@AV17Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFContagem_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFContagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24TFContagem_DataCriacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFContagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV34TFContagem_PFB_To",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV37TFContagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV38TFContagem_PFL_To",SqlDbType.Decimal,13,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GI3 ;
          prmH00GI3 = new Object[] {
          new Object[] {"@AV17Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFContagem_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFContagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24TFContagem_DataCriacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFContagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV34TFContagem_PFB_To",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV37TFContagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV38TFContagem_PFL_To",SqlDbType.Decimal,13,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GI2,11,0,true,false )
             ,new CursorDef("H00GI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GI3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                return;
       }
    }

 }

}
