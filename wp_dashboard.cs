/*
               File: WP_Dashboard
        Description: Dashboard
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:45:40.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_dashboard : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_dashboard( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_dashboard( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavCodigocontrato = new GXCombobox();
         cmbavStatusdemanda = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCODIGOCONTRATO") == 0 )
            {
               AV39ContratoGestor_ContratadaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoGestor_ContratadaAreaCod), 6, 0)));
               AV38ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContratoGestor_UsuarioCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvCODIGOCONTRATOSA2( AV39ContratoGestor_ContratadaAreaCod, AV38ContratoGestor_UsuarioCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813454072");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_dashboard.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDRAGANDDROPDATA", AV15DragAndDropData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDRAGANDDROPDATA", AV15DragAndDropData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMEXPANDDATA", AV16ItemExpandData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMEXPANDDATA", AV16ItemExpandData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMCOLLAPSEDATA", AV17ItemCollapseData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMCOLLAPSEDATA", AV17ItemCollapseData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFILTERCHANGEDDATA", AV18FilterChangedData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFILTERCHANGEDDATA", AV18FilterChangedData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGGREGATIONCHANGEDDATA", AV19AggregationChangedData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGGREGATIONCHANGEDDATA", AV19AggregationChangedData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMCLICKDATA", AV20ItemClickData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMCLICKDATA", AV20ItemClickData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMDOUBLECLICKDATA", AV21ItemDoubleClickData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMDOUBLECLICKDATA", AV21ItemDoubleClickData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAXES", AV22Axes);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAXES", AV22Axes);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETERS", AV23Parameters);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETERS", AV23Parameters);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vISFILTROSVALIDOS", AV26IsFiltrosValidos);
         GxWebStd.gx_hidden_field( context, "vCONTRATOGESTOR_CONTRATADAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Width", StringUtil.RTrim( Dvpanel_panelfiltros_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Cls", StringUtil.RTrim( Dvpanel_panelfiltros_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Title", StringUtil.RTrim( Dvpanel_panelfiltros_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Collapsible", StringUtil.BoolToStr( Dvpanel_panelfiltros_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Collapsed", StringUtil.BoolToStr( Dvpanel_panelfiltros_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Autowidth", StringUtil.BoolToStr( Dvpanel_panelfiltros_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Autoheight", StringUtil.BoolToStr( Dvpanel_panelfiltros_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_panelfiltros_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Iconposition", StringUtil.RTrim( Dvpanel_panelfiltros_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELFILTROS_Autoscroll", StringUtil.BoolToStr( Dvpanel_panelfiltros_Autoscroll));
         GxWebStd.gx_hidden_field( context, "USQUERY_Type", StringUtil.RTrim( Usquery_Type));
         GxWebStd.gx_hidden_field( context, "USQUERY_Objectname", StringUtil.RTrim( Usquery_Objectname));
         GxWebStd.gx_hidden_field( context, "USQUERY_Fontsize", StringUtil.LTrim( StringUtil.NToC( (decimal)(Usquery_Fontsize), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "USQUERY_Charttype", StringUtil.RTrim( Usquery_Charttype));
         GxWebStd.gx_hidden_field( context, "USQUERY_Preferredrendererforqueryviewercharts", StringUtil.RTrim( Usquery_Preferredrendererforqueryviewercharts));
         GxWebStd.gx_hidden_field( context, "USQUERY_Rememberlayout", StringUtil.BoolToStr( Usquery_Rememberlayout));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Width", StringUtil.RTrim( Dvpanel_panelgraficos_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Cls", StringUtil.RTrim( Dvpanel_panelgraficos_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Title", StringUtil.RTrim( Dvpanel_panelgraficos_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Collapsible", StringUtil.BoolToStr( Dvpanel_panelgraficos_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Collapsed", StringUtil.BoolToStr( Dvpanel_panelgraficos_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Autowidth", StringUtil.BoolToStr( Dvpanel_panelgraficos_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Autoheight", StringUtil.BoolToStr( Dvpanel_panelgraficos_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_panelgraficos_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Iconposition", StringUtil.RTrim( Dvpanel_panelgraficos_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELGRAFICOS_Autoscroll", StringUtil.BoolToStr( Dvpanel_panelgraficos_Autoscroll));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Title", StringUtil.RTrim( Ucmensagem_Title));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Confirmationtext", StringUtil.RTrim( Ucmensagem_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Yesbuttoncaption", StringUtil.RTrim( Ucmensagem_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "UCMENSAGEM_Confirmtype", StringUtil.RTrim( Ucmensagem_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_dashboard.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_Dashboard" ;
      }

      public override String GetPgmdesc( )
      {
         return "Dashboard" ;
      }

      protected void WBSA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SA2( true) ;
         }
         else
         {
            wb_table1_2_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Dashboard", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSA0( ) ;
      }

      protected void WSSA2( )
      {
         STARTSA2( ) ;
         EVTSA2( ) ;
      }

      protected void EVTSA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "UCMENSAGEM.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SA2 */
                              E11SA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SA2 */
                              E12SA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBNTCONSULTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13SA2 */
                              E13SA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14SA2 */
                              E14SA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15SA2 */
                              E15SA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavCodigocontrato.Name = "vCODIGOCONTRATO";
            dynavCodigocontrato.WebTags = "";
            cmbavStatusdemanda.Name = "vSTATUSDEMANDA";
            cmbavStatusdemanda.WebTags = "";
            cmbavStatusdemanda.addItem("", "Selecione", 0);
            cmbavStatusdemanda.addItem("B", "Stand by", 0);
            cmbavStatusdemanda.addItem("S", "Solicitada", 0);
            cmbavStatusdemanda.addItem("E", "Em An�lise", 0);
            cmbavStatusdemanda.addItem("A", "Em execu��o", 0);
            cmbavStatusdemanda.addItem("R", "Resolvida", 0);
            cmbavStatusdemanda.addItem("C", "Conferida", 0);
            cmbavStatusdemanda.addItem("D", "Retornada", 0);
            cmbavStatusdemanda.addItem("H", "Homologada", 0);
            cmbavStatusdemanda.addItem("O", "Aceite", 0);
            cmbavStatusdemanda.addItem("P", "A Pagar", 0);
            cmbavStatusdemanda.addItem("L", "Liquidada", 0);
            cmbavStatusdemanda.addItem("X", "Cancelada", 0);
            cmbavStatusdemanda.addItem("N", "N�o Faturada", 0);
            cmbavStatusdemanda.addItem("J", "Planejamento", 0);
            cmbavStatusdemanda.addItem("I", "An�lise Planejamento", 0);
            cmbavStatusdemanda.addItem("T", "Validacao T�cnica", 0);
            cmbavStatusdemanda.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatusdemanda.addItem("G", "Em Homologa��o", 0);
            cmbavStatusdemanda.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatusdemanda.addItem("U", "Rascunho", 0);
            if ( cmbavStatusdemanda.ItemCount > 0 )
            {
               AV28StatusDemanda = cmbavStatusdemanda.getValidValue(AV28StatusDemanda);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28StatusDemanda", AV28StatusDemanda);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavCodigocontrato_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDSVvCODIGOCONTRATOSA2( int AV39ContratoGestor_ContratadaAreaCod ,
                                              int AV38ContratoGestor_UsuarioCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvCODIGOCONTRATO_dataSA2( AV39ContratoGestor_ContratadaAreaCod, AV38ContratoGestor_UsuarioCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCODIGOCONTRATO_htmlSA2( int AV39ContratoGestor_ContratadaAreaCod ,
                                                 int AV38ContratoGestor_UsuarioCod )
      {
         int gxdynajaxvalue ;
         GXDSVvCODIGOCONTRATO_dataSA2( AV39ContratoGestor_ContratadaAreaCod, AV38ContratoGestor_UsuarioCod) ;
         gxdynajaxindex = 1;
         dynavCodigocontrato.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCodigocontrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavCodigocontrato.ItemCount > 0 )
         {
            AV40CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0)));
         }
      }

      protected void GXDSVvCODIGOCONTRATO_dataSA2( int AV39ContratoGestor_ContratadaAreaCod ,
                                                   int AV38ContratoGestor_UsuarioCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         IGxCollection gxcolvCODIGOCONTRATO ;
         SdtSDT_Codigos gxcolitemvCODIGOCONTRATO ;
         new dp_wp_dashboard_contrato(context ).execute(  AV39ContratoGestor_ContratadaAreaCod,  AV38ContratoGestor_UsuarioCod, out  gxcolvCODIGOCONTRATO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoGestor_ContratadaAreaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContratoGestor_UsuarioCod), 6, 0)));
         gxcolvCODIGOCONTRATO.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvCODIGOCONTRATO.Count )
         {
            gxcolitemvCODIGOCONTRATO = ((SdtSDT_Codigos)gxcolvCODIGOCONTRATO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvCODIGOCONTRATO.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvCODIGOCONTRATO.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavCodigocontrato.ItemCount > 0 )
         {
            AV40CodigoContrato = (int)(NumberUtil.Val( dynavCodigocontrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0)));
         }
         if ( cmbavStatusdemanda.ItemCount > 0 )
         {
            AV28StatusDemanda = cmbavStatusdemanda.getValidValue(AV28StatusDemanda);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28StatusDemanda", AV28StatusDemanda);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSA2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14SA2 */
         E14SA2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15SA2 */
            E15SA2 ();
            WBSA0( ) ;
         }
      }

      protected void STRUPSA0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12SA2 */
         E12SA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCODIGOCONTRATO_htmlSA2( AV39ContratoGestor_ContratadaAreaCod, AV38ContratoGestor_UsuarioCod) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDRAGANDDROPDATA"), AV15DragAndDropData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMEXPANDDATA"), AV16ItemExpandData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMCOLLAPSEDATA"), AV17ItemCollapseData);
            ajax_req_read_hidden_sdt(cgiGet( "vFILTERCHANGEDDATA"), AV18FilterChangedData);
            ajax_req_read_hidden_sdt(cgiGet( "vAGGREGATIONCHANGEDDATA"), AV19AggregationChangedData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMCLICKDATA"), AV20ItemClickData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMDOUBLECLICKDATA"), AV21ItemDoubleClickData);
            ajax_req_read_hidden_sdt(cgiGet( "vAXES"), AV22Axes);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETERS"), AV23Parameters);
            /* Read variables values. */
            dynavCodigocontrato.CurrentValue = cgiGet( dynavCodigocontrato_Internalname);
            AV40CodigoContrato = (int)(NumberUtil.Val( cgiGet( dynavCodigocontrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40CodigoContrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0)));
            cmbavStatusdemanda.CurrentValue = cgiGet( cmbavStatusdemanda_Internalname);
            AV28StatusDemanda = cgiGet( cmbavStatusdemanda_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28StatusDemanda", AV28StatusDemanda);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_in_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_In"}), 1, "vCONTAGEMRESULTADO_DATADMN_IN");
               GX_FocusControl = edtavContagemresultado_datadmn_in_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10ContagemResultado_DataDmn_In = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_DataDmn_In", context.localUtil.Format(AV10ContagemResultado_DataDmn_In, "99/99/99"));
            }
            else
            {
               AV10ContagemResultado_DataDmn_In = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_in_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_DataDmn_In", context.localUtil.Format(AV10ContagemResultado_DataDmn_In, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_fim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_Fim"}), 1, "vCONTAGEMRESULTADO_DATADMN_FIM");
               GX_FocusControl = edtavContagemresultado_datadmn_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11ContagemResultado_DataDmn_Fim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataDmn_Fim", context.localUtil.Format(AV11ContagemResultado_DataDmn_Fim, "99/99/99"));
            }
            else
            {
               AV11ContagemResultado_DataDmn_Fim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_fim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataDmn_Fim", context.localUtil.Format(AV11ContagemResultado_DataDmn_Fim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt_in_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt_In"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT_IN");
               GX_FocusControl = edtavContagemresultado_dataultcnt_in_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12ContagemResultado_DataUltCnt_In = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_DataUltCnt_In", context.localUtil.Format(AV12ContagemResultado_DataUltCnt_In, "99/99/99"));
            }
            else
            {
               AV12ContagemResultado_DataUltCnt_In = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt_in_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_DataUltCnt_In", context.localUtil.Format(AV12ContagemResultado_DataUltCnt_In, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt_fim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt_Fim"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT_FIM");
               GX_FocusControl = edtavContagemresultado_dataultcnt_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13ContagemResultado_DataUltCnt_Fim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataUltCnt_Fim", context.localUtil.Format(AV13ContagemResultado_DataUltCnt_Fim, "99/99/99"));
            }
            else
            {
               AV13ContagemResultado_DataUltCnt_Fim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt_fim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataUltCnt_Fim", context.localUtil.Format(AV13ContagemResultado_DataUltCnt_Fim, "99/99/99"));
            }
            /* Read saved values. */
            Dvpanel_panelfiltros_Width = cgiGet( "DVPANEL_PANELFILTROS_Width");
            Dvpanel_panelfiltros_Cls = cgiGet( "DVPANEL_PANELFILTROS_Cls");
            Dvpanel_panelfiltros_Title = cgiGet( "DVPANEL_PANELFILTROS_Title");
            Dvpanel_panelfiltros_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Collapsible"));
            Dvpanel_panelfiltros_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Collapsed"));
            Dvpanel_panelfiltros_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Autowidth"));
            Dvpanel_panelfiltros_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Autoheight"));
            Dvpanel_panelfiltros_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Showcollapseicon"));
            Dvpanel_panelfiltros_Iconposition = cgiGet( "DVPANEL_PANELFILTROS_Iconposition");
            Dvpanel_panelfiltros_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELFILTROS_Autoscroll"));
            Usquery_Type = cgiGet( "USQUERY_Type");
            Usquery_Objectname = cgiGet( "USQUERY_Objectname");
            Usquery_Fontsize = (int)(context.localUtil.CToN( cgiGet( "USQUERY_Fontsize"), ",", "."));
            Usquery_Charttype = cgiGet( "USQUERY_Charttype");
            Usquery_Preferredrendererforqueryviewercharts = cgiGet( "USQUERY_Preferredrendererforqueryviewercharts");
            Usquery_Rememberlayout = StringUtil.StrToBool( cgiGet( "USQUERY_Rememberlayout"));
            Dvpanel_panelgraficos_Width = cgiGet( "DVPANEL_PANELGRAFICOS_Width");
            Dvpanel_panelgraficos_Cls = cgiGet( "DVPANEL_PANELGRAFICOS_Cls");
            Dvpanel_panelgraficos_Title = cgiGet( "DVPANEL_PANELGRAFICOS_Title");
            Dvpanel_panelgraficos_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Collapsible"));
            Dvpanel_panelgraficos_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Collapsed"));
            Dvpanel_panelgraficos_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Autowidth"));
            Dvpanel_panelgraficos_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Autoheight"));
            Dvpanel_panelgraficos_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Showcollapseicon"));
            Dvpanel_panelgraficos_Iconposition = cgiGet( "DVPANEL_PANELGRAFICOS_Iconposition");
            Dvpanel_panelgraficos_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELGRAFICOS_Autoscroll"));
            Ucmensagem_Title = cgiGet( "UCMENSAGEM_Title");
            Ucmensagem_Confirmationtext = cgiGet( "UCMENSAGEM_Confirmationtext");
            Ucmensagem_Yesbuttoncaption = cgiGet( "UCMENSAGEM_Yesbuttoncaption");
            Ucmensagem_Confirmtype = cgiGet( "UCMENSAGEM_Confirmtype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCODIGOCONTRATO_htmlSA2( AV39ContratoGestor_ContratadaAreaCod, AV38ContratoGestor_UsuarioCod) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12SA2 */
         E12SA2 ();
         if (returnInSub) return;
      }

      protected void E12SA2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV14WWPContext) ;
         AV26IsFiltrosValidos = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26IsFiltrosValidos", AV26IsFiltrosValidos);
         if ( ! AV14WWPContext.gxTpr_Userehgestor )
         {
            Ucmensagem_Confirmationtext = "Usu�rio n�o possui permiss�o de acesso.";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucmensagem_Internalname, "ConfirmationText", Ucmensagem_Confirmationtext);
            this.executeUsercontrolMethod("", false, "UCMENSAGEMContainer", "Confirm", "", new Object[] {});
         }
         AV39ContratoGestor_ContratadaAreaCod = AV14WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoGestor_ContratadaAreaCod), 6, 0)));
         AV38ContratoGestor_UsuarioCod = AV14WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContratoGestor_UsuarioCod), 6, 0)));
      }

      protected void E13SA2( )
      {
         /* 'DoBntConsultar' Routine */
         /* Execute user subroutine: 'VALIDA.FILTROS' */
         S112 ();
         if (returnInSub) return;
         if ( AV26IsFiltrosValidos )
         {
            AV23Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
            AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
            AV24Parameter.gxTpr_Name = "FiltroContrato";
            AV24Parameter.gxTpr_Value = StringUtil.Trim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0));
            AV23Parameters.Add(AV24Parameter, 0);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28StatusDemanda)) )
            {
               AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV24Parameter.gxTpr_Name = "FiltroDemandaSituacao";
               AV24Parameter.gxTpr_Value = StringUtil.RTrim( context.localUtil.Format( AV28StatusDemanda, ""));
               AV23Parameters.Add(AV24Parameter, 0);
            }
            if ( ! (DateTime.MinValue==AV10ContagemResultado_DataDmn_In) )
            {
               AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV24Parameter.gxTpr_Name = "FiltroDataDemandaIn";
               AV24Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV10ContagemResultado_DataDmn_In, 2, "/"));
               AV23Parameters.Add(AV24Parameter, 0);
            }
            if ( ! (DateTime.MinValue==AV11ContagemResultado_DataDmn_Fim) )
            {
               AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV24Parameter.gxTpr_Name = "FiltroDataDemandaFim";
               AV24Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV11ContagemResultado_DataDmn_Fim, 2, "/"));
               AV23Parameters.Add(AV24Parameter, 0);
            }
            if ( ! (DateTime.MinValue==AV12ContagemResultado_DataUltCnt_In) )
            {
               AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV24Parameter.gxTpr_Name = "FiltroDataContagemIn";
               AV24Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV12ContagemResultado_DataUltCnt_In, 2, "/"));
               AV23Parameters.Add(AV24Parameter, 0);
            }
            if ( ! (DateTime.MinValue==AV13ContagemResultado_DataUltCnt_Fim) )
            {
               AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
               AV24Parameter.gxTpr_Name = "FiltroDataContagemFim";
               AV24Parameter.gxTpr_Value = StringUtil.Trim( context.localUtil.DToC( AV13ContagemResultado_DataUltCnt_Fim, 2, "/"));
               AV23Parameters.Add(AV24Parameter, 0);
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23Parameters", AV23Parameters);
      }

      protected void E14SA2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
      }

      protected void E11SA2( )
      {
         /* Ucmensagem_Close Routine */
         context.wjLoc = formatLink("wp_gestao.aspx") ;
         context.wjLocDisableFrm = 1;
         context.DoAjaxRefresh();
      }

      protected void S112( )
      {
         /* 'VALIDA.FILTROS' Routine */
         AV26IsFiltrosValidos = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26IsFiltrosValidos", AV26IsFiltrosValidos);
         if ( (0==AV40CodigoContrato) )
         {
            GX_msglist.addItem("O Filtro Contrato � obrigrat�rio. Verifique! ");
            AV26IsFiltrosValidos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26IsFiltrosValidos", AV26IsFiltrosValidos);
         }
         if ( ( (DateTime.MinValue==AV10ContagemResultado_DataDmn_In) && ! (DateTime.MinValue==AV11ContagemResultado_DataDmn_Fim) ) || ( ! (DateTime.MinValue==AV10ContagemResultado_DataDmn_In) && ! (DateTime.MinValue==AV11ContagemResultado_DataDmn_Fim) && ( AV10ContagemResultado_DataDmn_In > AV11ContagemResultado_DataDmn_Fim ) ) )
         {
            GX_msglist.addItem("O Filtro Data da Demanda n�o � v�lido. Verifique!");
            AV26IsFiltrosValidos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26IsFiltrosValidos", AV26IsFiltrosValidos);
         }
         if ( ( (DateTime.MinValue==AV12ContagemResultado_DataUltCnt_In) && ! (DateTime.MinValue==AV13ContagemResultado_DataUltCnt_Fim) ) || ( ! (DateTime.MinValue==AV12ContagemResultado_DataUltCnt_In) && ! (DateTime.MinValue==AV13ContagemResultado_DataUltCnt_Fim) && ( AV12ContagemResultado_DataUltCnt_In > AV13ContagemResultado_DataUltCnt_Fim ) ) )
         {
            GX_msglist.addItem("O Filtro Data da Contagem n�o � v�lido. Verifique!");
            AV26IsFiltrosValidos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26IsFiltrosValidos", AV26IsFiltrosValidos);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E15SA2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PANELFILTROSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PANELFILTROSContainer"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_panelfiltros_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divPanelfiltros_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-6", "left", "top", "", "", "div");
            wb_table2_15_SA2( true) ;
         }
         else
         {
            wb_table2_15_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_15_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-6 DataContentCell", "Right", "top", "", "", "div");
            wb_table3_20_SA2( true) ;
         }
         else
         {
            wb_table3_20_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "Right", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_unnamedtable1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divHtml_dvpanel_panelgraficos_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PANELGRAFICOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PANELGRAFICOSContainer"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_panelgraficos_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            wb_table4_81_SA2( true) ;
         }
         else
         {
            wb_table4_81_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_81_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "Center", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"UCMENSAGEMContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SA2e( true) ;
         }
         else
         {
            wb_table1_2_SA2e( false) ;
         }
      }

      protected void wb_table4_81_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPanelgraficos_Internalname, tblPanelgraficos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTableus1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"USQUERYContainer"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "Center", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_81_SA2e( true) ;
         }
         else
         {
            wb_table4_81_SA2e( false) ;
         }
      }

      protected void wb_table3_20_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltros_Internalname, tblTablefiltros_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecodigocontrato_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcodigocontrato_Internalname, "Contrato", "", "", lblTextblockcodigocontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, dynavCodigocontrato_Internalname, "Codigo Contrato", "col-sm-3 BootstrapAttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCodigocontrato, dynavCodigocontrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0)), 1, dynavCodigocontrato_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavCodigocontrato.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_Dashboard.htm");
            dynavCodigocontrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40CodigoContrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCodigocontrato_Internalname, "Values", (String)(dynavCodigocontrato.ToJavascriptSource()));
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablestatusdemanda_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockstatusdemanda_Internalname, "Status", "", "", lblTextblockstatusdemanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavStatusdemanda_Internalname, "Status Demanda", "col-sm-3 BootstrapAttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusdemanda, cmbavStatusdemanda_Internalname, StringUtil.RTrim( AV28StatusDemanda), 1, cmbavStatusdemanda_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavStatusdemanda.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WP_Dashboard.htm");
            cmbavStatusdemanda.CurrentValue = StringUtil.RTrim( AV28StatusDemanda);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdemanda_Internalname, "Values", (String)(cmbavStatusdemanda.ToJavascriptSource()));
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablesplittedcontagemresultado_datadmn_in_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmn_in_Internalname, "Data da Demanda", "", "", lblTextblockcontagemresultado_datadmn_in_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            wb_table5_46_SA2( true) ;
         }
         else
         {
            wb_table5_46_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_46_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablesplittedcontagemresultado_dataultcnt_in_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataultcnt_in_Internalname, "Data da Contagem", "", "", lblTextblockcontagemresultado_dataultcnt_in_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            wb_table6_61_SA2( true) ;
         }
         else
         {
            wb_table6_61_SA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_61_SA2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_SA2e( true) ;
         }
         else
         {
            wb_table3_20_SA2e( false) ;
         }
      }

      protected void wb_table6_61_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_dataultcnt_in_Internalname, tblTablemergedcontagemresultado_dataultcnt_in_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='MergeDataCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_dataultcnt_in_Internalname, "Contagem Resultado_Data Ult Cnt_In", "col-sm-3 BootstrapAttributeDateLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt_in_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt_in_Internalname, context.localUtil.Format(AV12ContagemResultado_DataUltCnt_In, "99/99/99"), context.localUtil.Format( AV12ContagemResultado_DataUltCnt_In, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt_in_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_dataultcnt_in_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt_in_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_dataultcnt_in_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Dashboard.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_dataultcnt_fim_Internalname, "At�", "col-sm-3 BootstrapAttributeDateLabel", 1, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt_fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt_fim_Internalname, context.localUtil.Format(AV13ContagemResultado_DataUltCnt_Fim, "99/99/99"), context.localUtil.Format( AV13ContagemResultado_DataUltCnt_Fim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt_fim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_dataultcnt_fim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt_fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_dataultcnt_fim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Dashboard.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_61_SA2e( true) ;
         }
         else
         {
            wb_table6_61_SA2e( false) ;
         }
      }

      protected void wb_table5_46_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_datadmn_in_Internalname, tblTablemergedcontagemresultado_datadmn_in_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='MergeDataCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_datadmn_in_Internalname, "Contagem Resultado_Data Dmn_In", "col-sm-3 BootstrapAttributeDateLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_in_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_in_Internalname, context.localUtil.Format(AV10ContagemResultado_DataDmn_In, "99/99/99"), context.localUtil.Format( AV10ContagemResultado_DataDmn_In, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_in_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmn_in_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_in_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn_in_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Dashboard.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescription'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_datadmn_fim_Internalname, "At�", "col-sm-3 BootstrapAttributeDateLabel", 1, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_fim_Internalname, context.localUtil.Format(AV11ContagemResultado_DataDmn_Fim, "99/99/99"), context.localUtil.Format( AV11ContagemResultado_DataDmn_Fim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_fim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmn_fim_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Dashboard.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn_fim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Dashboard.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_46_SA2e( true) ;
         }
         else
         {
            wb_table5_46_SA2e( false) ;
         }
      }

      protected void wb_table2_15_SA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableconsultar_Internalname, tblTableconsultar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntconsultar_Internalname, "", "Consultar", bttBtnbntconsultar_Jsonclick, 5, "Consultar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBNTCONSULTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Dashboard.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_15_SA2e( true) ;
         }
         else
         {
            wb_table2_15_SA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASA2( ) ;
         WSSA2( ) ;
         WESA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("QueryViewer/QueryViewer.css", "?10720");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813454341");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_dashboard.js", "?202051813454342");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         bttBtnbntconsultar_Internalname = "BTNBNTCONSULTAR";
         tblTableconsultar_Internalname = "TABLECONSULTAR";
         div_Internalname = "";
         lblTextblockcodigocontrato_Internalname = "TEXTBLOCKCODIGOCONTRATO";
         div_Internalname = "";
         dynavCodigocontrato_Internalname = "vCODIGOCONTRATO";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecodigocontrato_Internalname = "UNNAMEDTABLECODIGOCONTRATO";
         lblTextblockstatusdemanda_Internalname = "TEXTBLOCKSTATUSDEMANDA";
         div_Internalname = "";
         cmbavStatusdemanda_Internalname = "vSTATUSDEMANDA";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablestatusdemanda_Internalname = "UNNAMEDTABLESTATUSDEMANDA";
         lblTextblockcontagemresultado_datadmn_in_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATADMN_IN";
         div_Internalname = "";
         edtavContagemresultado_datadmn_in_Internalname = "vCONTAGEMRESULTADO_DATADMN_IN";
         div_Internalname = "";
         edtavContagemresultado_datadmn_fim_Internalname = "vCONTAGEMRESULTADO_DATADMN_FIM";
         div_Internalname = "";
         tblTablemergedcontagemresultado_datadmn_in_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_DATADMN_IN";
         div_Internalname = "";
         div_Internalname = "";
         divTablesplittedcontagemresultado_datadmn_in_Internalname = "TABLESPLITTEDCONTAGEMRESULTADO_DATADMN_IN";
         lblTextblockcontagemresultado_dataultcnt_in_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAULTCNT_IN";
         div_Internalname = "";
         edtavContagemresultado_dataultcnt_in_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT_IN";
         div_Internalname = "";
         edtavContagemresultado_dataultcnt_fim_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT_FIM";
         div_Internalname = "";
         tblTablemergedcontagemresultado_dataultcnt_in_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_DATAULTCNT_IN";
         div_Internalname = "";
         div_Internalname = "";
         divTablesplittedcontagemresultado_dataultcnt_in_Internalname = "TABLESPLITTEDCONTAGEMRESULTADO_DATAULTCNT_IN";
         tblTablefiltros_Internalname = "TABLEFILTROS";
         div_Internalname = "";
         div_Internalname = "";
         divPanelfiltros_Internalname = "PANELFILTROS";
         div_Internalname = "";
         divLayout_panelfiltros_Internalname = "LAYOUT_PANELFILTROS";
         Dvpanel_panelfiltros_Internalname = "DVPANEL_PANELFILTROS";
         Usquery_Internalname = "USQUERY";
         div_Internalname = "";
         div_Internalname = "";
         divTableus1_Internalname = "TABLEUS1";
         tblPanelgraficos_Internalname = "PANELGRAFICOS";
         div_Internalname = "";
         divLayout_panelgraficos_Internalname = "LAYOUT_PANELGRAFICOS";
         Dvpanel_panelgraficos_Internalname = "DVPANEL_PANELGRAFICOS";
         divHtml_dvpanel_panelgraficos_Internalname = "HTML_DVPANEL_PANELGRAFICOS";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         div_Internalname = "";
         divLayout_unnamedtable1_Internalname = "LAYOUT_UNNAMEDTABLE1";
         Ucmensagem_Internalname = "UCMENSAGEM";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_datadmn_fim_Jsonclick = "";
         edtavContagemresultado_datadmn_fim_Enabled = 1;
         edtavContagemresultado_datadmn_in_Jsonclick = "";
         edtavContagemresultado_datadmn_in_Enabled = 1;
         edtavContagemresultado_dataultcnt_fim_Jsonclick = "";
         edtavContagemresultado_dataultcnt_fim_Enabled = 1;
         edtavContagemresultado_dataultcnt_in_Jsonclick = "";
         edtavContagemresultado_dataultcnt_in_Enabled = 1;
         cmbavStatusdemanda_Jsonclick = "";
         cmbavStatusdemanda.Enabled = 1;
         dynavCodigocontrato_Jsonclick = "";
         dynavCodigocontrato.Enabled = 1;
         Ucmensagem_Confirmtype = "";
         Ucmensagem_Yesbuttoncaption = "Fechar";
         Ucmensagem_Confirmationtext = "";
         Ucmensagem_Title = "Aten��o";
         Dvpanel_panelgraficos_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_panelgraficos_Iconposition = "left";
         Dvpanel_panelgraficos_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_panelgraficos_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_panelgraficos_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_panelgraficos_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_panelgraficos_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_panelgraficos_Title = "Gr�fico";
         Dvpanel_panelgraficos_Cls = "GXUI-DVelop-Panel";
         Dvpanel_panelgraficos_Width = "100%";
         Usquery_Rememberlayout = Convert.ToBoolean( 0);
         Usquery_Preferredrendererforqueryviewercharts = "htmlandjs";
         Usquery_Charttype = "Pie";
         Usquery_Fontsize = 12;
         Usquery_Objectname = "QR_WP_Dashboard";
         Usquery_Type = "Chart";
         Dvpanel_panelfiltros_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_panelfiltros_Iconposition = "left";
         Dvpanel_panelfiltros_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_panelfiltros_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_panelfiltros_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_panelfiltros_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_panelfiltros_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_panelfiltros_Title = "Filtros";
         Dvpanel_panelfiltros_Cls = "GXUI-DVelop-Panel";
         Dvpanel_panelfiltros_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Dashboard";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOBNTCONSULTAR'","{handler:'E13SA2',iparms:[{av:'AV26IsFiltrosValidos',fld:'vISFILTROSVALIDOS',pic:'',nv:false},{av:'AV40CodigoContrato',fld:'vCODIGOCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV28StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''},{av:'AV10ContagemResultado_DataDmn_In',fld:'vCONTAGEMRESULTADO_DATADMN_IN',pic:'',nv:''},{av:'AV11ContagemResultado_DataDmn_Fim',fld:'vCONTAGEMRESULTADO_DATADMN_FIM',pic:'',nv:''},{av:'AV12ContagemResultado_DataUltCnt_In',fld:'vCONTAGEMRESULTADO_DATAULTCNT_IN',pic:'',nv:''},{av:'AV13ContagemResultado_DataUltCnt_Fim',fld:'vCONTAGEMRESULTADO_DATAULTCNT_FIM',pic:'',nv:''}],oparms:[{av:'AV23Parameters',fld:'vPARAMETERS',pic:'',nv:null},{av:'AV26IsFiltrosValidos',fld:'vISFILTROSVALIDOS',pic:'',nv:false}]}");
         setEventMetadata("UCMENSAGEM.CLOSE","{handler:'E11SA2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15DragAndDropData = new SdtQueryViewerDragAndDropData(context);
         AV16ItemExpandData = new SdtQueryViewerItemExpandData(context);
         AV17ItemCollapseData = new SdtQueryViewerItemCollapseData(context);
         AV18FilterChangedData = new SdtQueryViewerFilterChangedData(context);
         AV19AggregationChangedData = new SdtQueryViewerAggregationChangedData(context);
         AV20ItemClickData = new SdtQueryViewerItemClickData(context);
         AV21ItemDoubleClickData = new SdtQueryViewerItemDoubleClickData(context);
         AV22Axes = new GxObjectCollection( context, "QueryViewerAxes.Axis", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerAxes_Axis", "GeneXus.Programs");
         AV23Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_MeetrikaVs3", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28StatusDemanda = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         AV10ContagemResultado_DataDmn_In = DateTime.MinValue;
         AV11ContagemResultado_DataDmn_Fim = DateTime.MinValue;
         AV12ContagemResultado_DataUltCnt_In = DateTime.MinValue;
         AV13ContagemResultado_DataUltCnt_Fim = DateTime.MinValue;
         AV14WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV24Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblockcodigocontrato_Jsonclick = "";
         TempTags = "";
         lblTextblockstatusdemanda_Jsonclick = "";
         lblTextblockcontagemresultado_datadmn_in_Jsonclick = "";
         lblTextblockcontagemresultado_dataultcnt_in_Jsonclick = "";
         bttBtnbntconsultar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV39ContratoGestor_ContratadaAreaCod ;
      private int AV38ContratoGestor_UsuarioCod ;
      private int Usquery_Fontsize ;
      private int gxdynajaxindex ;
      private int AV40CodigoContrato ;
      private int edtavContagemresultado_dataultcnt_in_Enabled ;
      private int edtavContagemresultado_dataultcnt_fim_Enabled ;
      private int edtavContagemresultado_datadmn_in_Enabled ;
      private int edtavContagemresultado_datadmn_fim_Enabled ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_panelfiltros_Width ;
      private String Dvpanel_panelfiltros_Cls ;
      private String Dvpanel_panelfiltros_Title ;
      private String Dvpanel_panelfiltros_Iconposition ;
      private String Usquery_Type ;
      private String Usquery_Objectname ;
      private String Usquery_Charttype ;
      private String Usquery_Preferredrendererforqueryviewercharts ;
      private String Dvpanel_panelgraficos_Width ;
      private String Dvpanel_panelgraficos_Cls ;
      private String Dvpanel_panelgraficos_Title ;
      private String Dvpanel_panelgraficos_Iconposition ;
      private String Ucmensagem_Title ;
      private String Ucmensagem_Confirmationtext ;
      private String Ucmensagem_Yesbuttoncaption ;
      private String Ucmensagem_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV28StatusDemanda ;
      private String dynavCodigocontrato_Internalname ;
      private String gxwrpcisep ;
      private String cmbavStatusdemanda_Internalname ;
      private String edtavContagemresultado_datadmn_in_Internalname ;
      private String edtavContagemresultado_datadmn_fim_Internalname ;
      private String edtavContagemresultado_dataultcnt_in_Internalname ;
      private String edtavContagemresultado_dataultcnt_fim_Internalname ;
      private String Ucmensagem_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divLayout_panelfiltros_Internalname ;
      private String divPanelfiltros_Internalname ;
      private String divLayout_unnamedtable1_Internalname ;
      private String divUnnamedtable1_Internalname ;
      private String divHtml_dvpanel_panelgraficos_Internalname ;
      private String divLayout_panelgraficos_Internalname ;
      private String tblPanelgraficos_Internalname ;
      private String divTableus1_Internalname ;
      private String tblTablefiltros_Internalname ;
      private String divUnnamedtablecodigocontrato_Internalname ;
      private String lblTextblockcodigocontrato_Internalname ;
      private String lblTextblockcodigocontrato_Jsonclick ;
      private String TempTags ;
      private String dynavCodigocontrato_Jsonclick ;
      private String divUnnamedtablestatusdemanda_Internalname ;
      private String lblTextblockstatusdemanda_Internalname ;
      private String lblTextblockstatusdemanda_Jsonclick ;
      private String cmbavStatusdemanda_Jsonclick ;
      private String divTablesplittedcontagemresultado_datadmn_in_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_in_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_in_Jsonclick ;
      private String divTablesplittedcontagemresultado_dataultcnt_in_Internalname ;
      private String lblTextblockcontagemresultado_dataultcnt_in_Internalname ;
      private String lblTextblockcontagemresultado_dataultcnt_in_Jsonclick ;
      private String tblTablemergedcontagemresultado_dataultcnt_in_Internalname ;
      private String edtavContagemresultado_dataultcnt_in_Jsonclick ;
      private String edtavContagemresultado_dataultcnt_fim_Jsonclick ;
      private String tblTablemergedcontagemresultado_datadmn_in_Internalname ;
      private String edtavContagemresultado_datadmn_in_Jsonclick ;
      private String edtavContagemresultado_datadmn_fim_Jsonclick ;
      private String tblTableconsultar_Internalname ;
      private String bttBtnbntconsultar_Internalname ;
      private String bttBtnbntconsultar_Jsonclick ;
      private String div_Internalname ;
      private String Dvpanel_panelfiltros_Internalname ;
      private String Usquery_Internalname ;
      private String Dvpanel_panelgraficos_Internalname ;
      private DateTime AV10ContagemResultado_DataDmn_In ;
      private DateTime AV11ContagemResultado_DataDmn_Fim ;
      private DateTime AV12ContagemResultado_DataUltCnt_In ;
      private DateTime AV13ContagemResultado_DataUltCnt_Fim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV26IsFiltrosValidos ;
      private bool Dvpanel_panelfiltros_Collapsible ;
      private bool Dvpanel_panelfiltros_Collapsed ;
      private bool Dvpanel_panelfiltros_Autowidth ;
      private bool Dvpanel_panelfiltros_Autoheight ;
      private bool Dvpanel_panelfiltros_Showcollapseicon ;
      private bool Dvpanel_panelfiltros_Autoscroll ;
      private bool Usquery_Rememberlayout ;
      private bool Dvpanel_panelgraficos_Collapsible ;
      private bool Dvpanel_panelgraficos_Collapsed ;
      private bool Dvpanel_panelgraficos_Autowidth ;
      private bool Dvpanel_panelgraficos_Autoheight ;
      private bool Dvpanel_panelgraficos_Showcollapseicon ;
      private bool Dvpanel_panelgraficos_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavCodigocontrato ;
      private GXCombobox cmbavStatusdemanda ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerAxes_Axis ))]
      private IGxCollection AV22Axes ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerParameters_Parameter ))]
      private IGxCollection AV23Parameters ;
      private GXWebForm Form ;
      private SdtQueryViewerAggregationChangedData AV19AggregationChangedData ;
      private wwpbaseobjects.SdtWWPContext AV14WWPContext ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
      private SdtQueryViewerDragAndDropData AV15DragAndDropData ;
      private SdtQueryViewerFilterChangedData AV18FilterChangedData ;
      private SdtQueryViewerItemClickData AV20ItemClickData ;
      private SdtQueryViewerItemCollapseData AV17ItemCollapseData ;
      private SdtQueryViewerItemDoubleClickData AV21ItemDoubleClickData ;
      private SdtQueryViewerItemExpandData AV16ItemExpandData ;
      private SdtQueryViewerParameters_Parameter AV24Parameter ;
   }

}
