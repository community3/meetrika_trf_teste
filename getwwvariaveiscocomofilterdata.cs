/*
               File: GetWWVariaveisCocomoFilterData
        Description: Get WWVariaveis Cocomo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:52:33.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwvariaveiscocomofilterdata : GXProcedure
   {
      public getwwvariaveiscocomofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwvariaveiscocomofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
         return AV41OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwvariaveiscocomofilterdata objgetwwvariaveiscocomofilterdata;
         objgetwwvariaveiscocomofilterdata = new getwwvariaveiscocomofilterdata();
         objgetwwvariaveiscocomofilterdata.AV32DDOName = aP0_DDOName;
         objgetwwvariaveiscocomofilterdata.AV30SearchTxt = aP1_SearchTxt;
         objgetwwvariaveiscocomofilterdata.AV31SearchTxtTo = aP2_SearchTxtTo;
         objgetwwvariaveiscocomofilterdata.AV36OptionsJson = "" ;
         objgetwwvariaveiscocomofilterdata.AV39OptionsDescJson = "" ;
         objgetwwvariaveiscocomofilterdata.AV41OptionIndexesJson = "" ;
         objgetwwvariaveiscocomofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwvariaveiscocomofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwvariaveiscocomofilterdata);
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwvariaveiscocomofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV35Options = (IGxCollection)(new GxSimpleCollection());
         AV38OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV40OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV32DDOName), "DDO_VARIAVELCOCOMO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADVARIAVELCOCOMO_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV36OptionsJson = AV35Options.ToJSonString(false);
         AV39OptionsDescJson = AV38OptionsDesc.ToJSonString(false);
         AV41OptionIndexesJson = AV40OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV43Session.Get("WWVariaveisCocomoGridState"), "") == 0 )
         {
            AV45GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWVariaveisCocomoGridState"), "");
         }
         else
         {
            AV45GridState.FromXml(AV43Session.Get("WWVariaveisCocomoGridState"), "");
         }
         AV62GXV1 = 1;
         while ( AV62GXV1 <= AV45GridState.gxTpr_Filtervalues.Count )
         {
            AV46GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV45GridState.gxTpr_Filtervalues.Item(AV62GXV1));
            if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "VARIAVELCOCOMO_AREATRABALHOCOD") == 0 )
            {
               AV48VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA") == 0 )
            {
               AV10TFVariavelCocomo_Sigla = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA_SEL") == 0 )
            {
               AV11TFVariavelCocomo_Sigla_Sel = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_TIPO_SEL") == 0 )
            {
               AV12TFVariavelCocomo_Tipo_SelsJson = AV46GridStateFilterValue.gxTpr_Value;
               AV13TFVariavelCocomo_Tipo_Sels.FromJSonString(AV12TFVariavelCocomo_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_DATA") == 0 )
            {
               AV14TFVariavelCocomo_Data = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Value, 2);
               AV15TFVariavelCocomo_Data_To = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRABAIXO") == 0 )
            {
               AV16TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV17TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOBAIXO") == 0 )
            {
               AV18TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV19TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_BAIXO") == 0 )
            {
               AV20TFVariavelCocomo_Baixo = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV21TFVariavelCocomo_Baixo_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_NOMINAL") == 0 )
            {
               AV22TFVariavelCocomo_Nominal = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV23TFVariavelCocomo_Nominal_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_ALTO") == 0 )
            {
               AV24TFVariavelCocomo_Alto = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV25TFVariavelCocomo_Alto_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOALTO") == 0 )
            {
               AV26TFVariavelCocomo_MuitoAlto = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV27TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRAALTO") == 0 )
            {
               AV28TFVariavelCocomo_ExtraAlto = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, ".");
               AV29TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV62GXV1 = (int)(AV62GXV1+1);
         }
         if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(1));
            AV49DynamicFiltersSelector1 = AV47GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "VARIAVELCOCOMO_SIGLA") == 0 )
            {
               AV50VariavelCocomo_Sigla1 = AV47GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "VARIAVELCOCOMO_TIPO") == 0 )
            {
               AV51VariavelCocomo_Tipo1 = AV47GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV52DynamicFiltersEnabled2 = true;
               AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(2));
               AV53DynamicFiltersSelector2 = AV47GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "VARIAVELCOCOMO_SIGLA") == 0 )
               {
                  AV54VariavelCocomo_Sigla2 = AV47GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "VARIAVELCOCOMO_TIPO") == 0 )
               {
                  AV55VariavelCocomo_Tipo2 = AV47GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV56DynamicFiltersEnabled3 = true;
                  AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(3));
                  AV57DynamicFiltersSelector3 = AV47GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "VARIAVELCOCOMO_SIGLA") == 0 )
                  {
                     AV58VariavelCocomo_Sigla3 = AV47GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "VARIAVELCOCOMO_TIPO") == 0 )
                  {
                     AV59VariavelCocomo_Tipo3 = AV47GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADVARIAVELCOCOMO_SIGLAOPTIONS' Routine */
         AV10TFVariavelCocomo_Sigla = AV30SearchTxt;
         AV11TFVariavelCocomo_Sigla_Sel = "";
         AV64WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV48VariavelCocomo_AreaTrabalhoCod;
         AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
         AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV50VariavelCocomo_Sigla1;
         AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV51VariavelCocomo_Tipo1;
         AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV52DynamicFiltersEnabled2;
         AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV53DynamicFiltersSelector2;
         AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV54VariavelCocomo_Sigla2;
         AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV55VariavelCocomo_Tipo2;
         AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV56DynamicFiltersEnabled3;
         AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV57DynamicFiltersSelector3;
         AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV58VariavelCocomo_Sigla3;
         AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV59VariavelCocomo_Tipo3;
         AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV10TFVariavelCocomo_Sigla;
         AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV11TFVariavelCocomo_Sigla_Sel;
         AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV13TFVariavelCocomo_Tipo_Sels;
         AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV14TFVariavelCocomo_Data;
         AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV15TFVariavelCocomo_Data_To;
         AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV16TFVariavelCocomo_ExtraBaixo;
         AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV17TFVariavelCocomo_ExtraBaixo_To;
         AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV18TFVariavelCocomo_MuitoBaixo;
         AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV19TFVariavelCocomo_MuitoBaixo_To;
         AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV20TFVariavelCocomo_Baixo;
         AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV21TFVariavelCocomo_Baixo_To;
         AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV22TFVariavelCocomo_Nominal;
         AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV23TFVariavelCocomo_Nominal_To;
         AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV24TFVariavelCocomo_Alto;
         AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV25TFVariavelCocomo_Alto_To;
         AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV26TFVariavelCocomo_MuitoAlto;
         AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV27TFVariavelCocomo_MuitoAlto_To;
         AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV28TFVariavelCocomo_ExtraAlto;
         AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV29TFVariavelCocomo_ExtraAlto_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A964VariavelCocomo_Tipo ,
                                              AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                              AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                              AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                              AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                              AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                              AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                              AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                              AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                              AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                              AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                              AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                              AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                              AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                              AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                              AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels.Count ,
                                              AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                              AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                              AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                              AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                              AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                              AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                              AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                              AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                              AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                              AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                              AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                              AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                              AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                              AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                              AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                              AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                              A962VariavelCocomo_Sigla ,
                                              A966VariavelCocomo_Data ,
                                              A986VariavelCocomo_ExtraBaixo ,
                                              A967VariavelCocomo_MuitoBaixo ,
                                              A968VariavelCocomo_Baixo ,
                                              A969VariavelCocomo_Nominal ,
                                              A970VariavelCocomo_Alto ,
                                              A971VariavelCocomo_MuitoAlto ,
                                              A972VariavelCocomo_ExtraAlto ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A961VariavelCocomo_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1), 15, "%");
         lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2), 15, "%");
         lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3), 15, "%");
         lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = StringUtil.PadR( StringUtil.RTrim( AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla), 15, "%");
         /* Using cursor P00PH2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1, AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1, lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2, AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2, lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3, AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3, lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla, AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel, AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data, AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to, AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo, AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to, AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo, AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to, AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo, AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to, AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal, AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to, AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto, AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to, AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto, AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to, AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto, AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPH2 = false;
            A961VariavelCocomo_AreaTrabalhoCod = P00PH2_A961VariavelCocomo_AreaTrabalhoCod[0];
            A962VariavelCocomo_Sigla = P00PH2_A962VariavelCocomo_Sigla[0];
            A972VariavelCocomo_ExtraAlto = P00PH2_A972VariavelCocomo_ExtraAlto[0];
            n972VariavelCocomo_ExtraAlto = P00PH2_n972VariavelCocomo_ExtraAlto[0];
            A971VariavelCocomo_MuitoAlto = P00PH2_A971VariavelCocomo_MuitoAlto[0];
            n971VariavelCocomo_MuitoAlto = P00PH2_n971VariavelCocomo_MuitoAlto[0];
            A970VariavelCocomo_Alto = P00PH2_A970VariavelCocomo_Alto[0];
            n970VariavelCocomo_Alto = P00PH2_n970VariavelCocomo_Alto[0];
            A969VariavelCocomo_Nominal = P00PH2_A969VariavelCocomo_Nominal[0];
            n969VariavelCocomo_Nominal = P00PH2_n969VariavelCocomo_Nominal[0];
            A968VariavelCocomo_Baixo = P00PH2_A968VariavelCocomo_Baixo[0];
            n968VariavelCocomo_Baixo = P00PH2_n968VariavelCocomo_Baixo[0];
            A967VariavelCocomo_MuitoBaixo = P00PH2_A967VariavelCocomo_MuitoBaixo[0];
            n967VariavelCocomo_MuitoBaixo = P00PH2_n967VariavelCocomo_MuitoBaixo[0];
            A986VariavelCocomo_ExtraBaixo = P00PH2_A986VariavelCocomo_ExtraBaixo[0];
            n986VariavelCocomo_ExtraBaixo = P00PH2_n986VariavelCocomo_ExtraBaixo[0];
            A966VariavelCocomo_Data = P00PH2_A966VariavelCocomo_Data[0];
            n966VariavelCocomo_Data = P00PH2_n966VariavelCocomo_Data[0];
            A964VariavelCocomo_Tipo = P00PH2_A964VariavelCocomo_Tipo[0];
            A992VariavelCocomo_Sequencial = P00PH2_A992VariavelCocomo_Sequencial[0];
            AV42count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00PH2_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(P00PH2_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) )
            {
               BRKPH2 = false;
               A964VariavelCocomo_Tipo = P00PH2_A964VariavelCocomo_Tipo[0];
               A992VariavelCocomo_Sequencial = P00PH2_A992VariavelCocomo_Sequencial[0];
               AV42count = (long)(AV42count+1);
               BRKPH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A962VariavelCocomo_Sigla)) )
            {
               AV34Option = A962VariavelCocomo_Sigla;
               AV35Options.Add(AV34Option, 0);
               AV40OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV42count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV35Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPH2 )
            {
               BRKPH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV35Options = new GxSimpleCollection();
         AV38OptionsDesc = new GxSimpleCollection();
         AV40OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV43Session = context.GetSession();
         AV45GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV46GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFVariavelCocomo_Sigla = "";
         AV11TFVariavelCocomo_Sigla_Sel = "";
         AV12TFVariavelCocomo_Tipo_SelsJson = "";
         AV13TFVariavelCocomo_Tipo_Sels = new GxSimpleCollection();
         AV14TFVariavelCocomo_Data = DateTime.MinValue;
         AV15TFVariavelCocomo_Data_To = DateTime.MinValue;
         AV47GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV49DynamicFiltersSelector1 = "";
         AV50VariavelCocomo_Sigla1 = "";
         AV51VariavelCocomo_Tipo1 = "";
         AV53DynamicFiltersSelector2 = "";
         AV54VariavelCocomo_Sigla2 = "";
         AV55VariavelCocomo_Tipo2 = "";
         AV57DynamicFiltersSelector3 = "";
         AV58VariavelCocomo_Sigla3 = "";
         AV59VariavelCocomo_Tipo3 = "";
         AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = "";
         AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = "";
         AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = "";
         AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = "";
         AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = "";
         AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = "";
         AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = "";
         AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = "";
         AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = "";
         AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = "";
         AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = "";
         AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = new GxSimpleCollection();
         AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = DateTime.MinValue;
         AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = DateTime.MinValue;
         scmdbuf = "";
         lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = "";
         lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = "";
         lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = "";
         lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = "";
         A964VariavelCocomo_Tipo = "";
         A962VariavelCocomo_Sigla = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         P00PH2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         P00PH2_A962VariavelCocomo_Sigla = new String[] {""} ;
         P00PH2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         P00PH2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         P00PH2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         P00PH2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         P00PH2_A970VariavelCocomo_Alto = new decimal[1] ;
         P00PH2_n970VariavelCocomo_Alto = new bool[] {false} ;
         P00PH2_A969VariavelCocomo_Nominal = new decimal[1] ;
         P00PH2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         P00PH2_A968VariavelCocomo_Baixo = new decimal[1] ;
         P00PH2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         P00PH2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         P00PH2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         P00PH2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         P00PH2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         P00PH2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         P00PH2_n966VariavelCocomo_Data = new bool[] {false} ;
         P00PH2_A964VariavelCocomo_Tipo = new String[] {""} ;
         P00PH2_A992VariavelCocomo_Sequencial = new short[1] ;
         AV34Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwvariaveiscocomofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PH2_A961VariavelCocomo_AreaTrabalhoCod, P00PH2_A962VariavelCocomo_Sigla, P00PH2_A972VariavelCocomo_ExtraAlto, P00PH2_n972VariavelCocomo_ExtraAlto, P00PH2_A971VariavelCocomo_MuitoAlto, P00PH2_n971VariavelCocomo_MuitoAlto, P00PH2_A970VariavelCocomo_Alto, P00PH2_n970VariavelCocomo_Alto, P00PH2_A969VariavelCocomo_Nominal, P00PH2_n969VariavelCocomo_Nominal,
               P00PH2_A968VariavelCocomo_Baixo, P00PH2_n968VariavelCocomo_Baixo, P00PH2_A967VariavelCocomo_MuitoBaixo, P00PH2_n967VariavelCocomo_MuitoBaixo, P00PH2_A986VariavelCocomo_ExtraBaixo, P00PH2_n986VariavelCocomo_ExtraBaixo, P00PH2_A966VariavelCocomo_Data, P00PH2_n966VariavelCocomo_Data, P00PH2_A964VariavelCocomo_Tipo, P00PH2_A992VariavelCocomo_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A992VariavelCocomo_Sequencial ;
      private int AV62GXV1 ;
      private int AV48VariavelCocomo_AreaTrabalhoCod ;
      private int AV64WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod ;
      private int AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private long AV42count ;
      private decimal AV16TFVariavelCocomo_ExtraBaixo ;
      private decimal AV17TFVariavelCocomo_ExtraBaixo_To ;
      private decimal AV18TFVariavelCocomo_MuitoBaixo ;
      private decimal AV19TFVariavelCocomo_MuitoBaixo_To ;
      private decimal AV20TFVariavelCocomo_Baixo ;
      private decimal AV21TFVariavelCocomo_Baixo_To ;
      private decimal AV22TFVariavelCocomo_Nominal ;
      private decimal AV23TFVariavelCocomo_Nominal_To ;
      private decimal AV24TFVariavelCocomo_Alto ;
      private decimal AV25TFVariavelCocomo_Alto_To ;
      private decimal AV26TFVariavelCocomo_MuitoAlto ;
      private decimal AV27TFVariavelCocomo_MuitoAlto_To ;
      private decimal AV28TFVariavelCocomo_ExtraAlto ;
      private decimal AV29TFVariavelCocomo_ExtraAlto_To ;
      private decimal AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ;
      private decimal AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ;
      private decimal AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ;
      private decimal AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ;
      private decimal AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ;
      private decimal AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ;
      private decimal AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ;
      private decimal AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ;
      private decimal AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ;
      private decimal AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ;
      private decimal AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ;
      private decimal AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ;
      private decimal AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ;
      private decimal AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private String AV10TFVariavelCocomo_Sigla ;
      private String AV11TFVariavelCocomo_Sigla_Sel ;
      private String AV50VariavelCocomo_Sigla1 ;
      private String AV51VariavelCocomo_Tipo1 ;
      private String AV54VariavelCocomo_Sigla2 ;
      private String AV55VariavelCocomo_Tipo2 ;
      private String AV58VariavelCocomo_Sigla3 ;
      private String AV59VariavelCocomo_Tipo3 ;
      private String AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ;
      private String AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ;
      private String AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ;
      private String AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ;
      private String AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ;
      private String AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ;
      private String AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ;
      private String AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ;
      private String scmdbuf ;
      private String lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ;
      private String lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ;
      private String lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ;
      private String lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ;
      private String A964VariavelCocomo_Tipo ;
      private String A962VariavelCocomo_Sigla ;
      private DateTime AV14TFVariavelCocomo_Data ;
      private DateTime AV15TFVariavelCocomo_Data_To ;
      private DateTime AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ;
      private DateTime AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ;
      private DateTime A966VariavelCocomo_Data ;
      private bool returnInSub ;
      private bool AV52DynamicFiltersEnabled2 ;
      private bool AV56DynamicFiltersEnabled3 ;
      private bool AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ;
      private bool AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ;
      private bool BRKPH2 ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n970VariavelCocomo_Alto ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n966VariavelCocomo_Data ;
      private String AV41OptionIndexesJson ;
      private String AV36OptionsJson ;
      private String AV39OptionsDescJson ;
      private String AV12TFVariavelCocomo_Tipo_SelsJson ;
      private String AV32DDOName ;
      private String AV30SearchTxt ;
      private String AV31SearchTxtTo ;
      private String AV49DynamicFiltersSelector1 ;
      private String AV53DynamicFiltersSelector2 ;
      private String AV57DynamicFiltersSelector3 ;
      private String AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ;
      private String AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ;
      private String AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ;
      private String AV34Option ;
      private IGxSession AV43Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00PH2_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] P00PH2_A962VariavelCocomo_Sigla ;
      private decimal[] P00PH2_A972VariavelCocomo_ExtraAlto ;
      private bool[] P00PH2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] P00PH2_A971VariavelCocomo_MuitoAlto ;
      private bool[] P00PH2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] P00PH2_A970VariavelCocomo_Alto ;
      private bool[] P00PH2_n970VariavelCocomo_Alto ;
      private decimal[] P00PH2_A969VariavelCocomo_Nominal ;
      private bool[] P00PH2_n969VariavelCocomo_Nominal ;
      private decimal[] P00PH2_A968VariavelCocomo_Baixo ;
      private bool[] P00PH2_n968VariavelCocomo_Baixo ;
      private decimal[] P00PH2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] P00PH2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] P00PH2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] P00PH2_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] P00PH2_A966VariavelCocomo_Data ;
      private bool[] P00PH2_n966VariavelCocomo_Data ;
      private String[] P00PH2_A964VariavelCocomo_Tipo ;
      private short[] P00PH2_A992VariavelCocomo_Sequencial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFVariavelCocomo_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV45GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV46GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV47GridStateDynamicFilter ;
   }

   public class getwwvariaveiscocomofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PH2( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                             String AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                             String AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                             String AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                             bool AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                             String AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                             String AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                             String AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                             bool AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                             String AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                             String AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                             String AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                             String AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                             String AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                             int AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count ,
                                             DateTime AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                             DateTime AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                             decimal AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                             decimal AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                             decimal AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                             decimal AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                             decimal AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                             decimal AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                             decimal AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                             decimal AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                             decimal AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                             decimal AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                             decimal AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                             decimal AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                             decimal AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                             decimal AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                             String A962VariavelCocomo_Sigla ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A961VariavelCocomo_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV68WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV72WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV73WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV78WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PH2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PH2 ;
          prmP00PH2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWVariaveisCocomoDS_3_Variavelcocomo_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV67WWVariaveisCocomoDS_4_Variavelcocomo_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV70WWVariaveisCocomoDS_7_Variavelcocomo_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV71WWVariaveisCocomoDS_8_Variavelcocomo_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWVariaveisCocomoDS_11_Variavelcocomo_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV75WWVariaveisCocomoDS_12_Variavelcocomo_tipo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV76WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV77WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV79WWVariaveisCocomoDS_16_Tfvariavelcocomo_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV82WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV83WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV84WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV85WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV86WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV87WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV88WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV89WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV90WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV91WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV92WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV93WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV94WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to",SqlDbType.Decimal,12,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwvariaveiscocomofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwvariaveiscocomofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwvariaveiscocomofilterdata") )
          {
             return  ;
          }
          getwwvariaveiscocomofilterdata worker = new getwwvariaveiscocomofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
