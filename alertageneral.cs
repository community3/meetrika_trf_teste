/*
               File: AlertaGeneral
        Description: Alerta General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:1:45.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class alertageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public alertageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public alertageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Alerta_Codigo )
      {
         this.A1881Alerta_Codigo = aP0_Alerta_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynAlerta_ToAreaTrabalho = new GXCombobox();
         dynAlerta_ToUser = new GXCombobox();
         cmbAlerta_ToGroup = new GXCombobox();
         dynAlerta_Owner = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1881Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1881Alerta_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_TOAREATRABALHO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAALERTA_TOAREATRABALHOOQ2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_TOUSER") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAALERTA_TOUSEROQ2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_OWNER") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAALERTA_OWNEROQ2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAOQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "AlertaGeneral";
               context.Gx_err = 0;
               GXAALERTA_TOAREATRABALHO_htmlOQ2( ) ;
               GXAALERTA_TOUSER_htmlOQ2( ) ;
               GXAALERTA_OWNER_htmlOQ2( ) ;
               WSOQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Alerta General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211814586");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("alertageneral.aspx") + "?" + UrlEncode("" +A1881Alerta_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1881Alerta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1881Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ALERTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_TOUSER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_TOGROUP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_INICIO", GetSecureSignedToken( sPrefix, A1885Alerta_Inicio));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_FIM", GetSecureSignedToken( sPrefix, A1886Alerta_Fim));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_OWNER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ALERTA_CADASTRO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormOQ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("alertageneral.js", "?20205211814588");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AlertaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alerta General" ;
      }

      protected void WBOQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "alertageneral.aspx");
            }
            wb_table1_2_OQ2( true) ;
         }
         else
         {
            wb_table1_2_OQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OQ2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTOQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Alerta General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPOQ0( ) ;
            }
         }
      }

      protected void WSOQ2( )
      {
         STARTOQ2( ) ;
         EVTOQ2( ) ;
      }

      protected void EVTOQ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11OQ2 */
                                    E11OQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12OQ2 */
                                    E12OQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13OQ2 */
                                    E13OQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14OQ2 */
                                    E14OQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15OQ2 */
                                    E15OQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormOQ2( ) ;
            }
         }
      }

      protected void PAOQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynAlerta_ToAreaTrabalho.Name = "ALERTA_TOAREATRABALHO";
            dynAlerta_ToAreaTrabalho.WebTags = "";
            dynAlerta_ToUser.Name = "ALERTA_TOUSER";
            dynAlerta_ToUser.WebTags = "";
            cmbAlerta_ToGroup.Name = "ALERTA_TOGROUP";
            cmbAlerta_ToGroup.WebTags = "";
            cmbAlerta_ToGroup.addItem("0", "(Nenhum)", 0);
            cmbAlerta_ToGroup.addItem("1", "Todos os usu�rios", 0);
            cmbAlerta_ToGroup.addItem("2", "Todos os usu�rios da Contratante", 0);
            cmbAlerta_ToGroup.addItem("3", "Todos os usu�rios da Contratada", 0);
            cmbAlerta_ToGroup.addItem("4", "Todos os gestores", 0);
            cmbAlerta_ToGroup.addItem("5", "Todos os gestores da Contratante", 0);
            cmbAlerta_ToGroup.addItem("6", "Todos os gestores da Contratada", 0);
            if ( cmbAlerta_ToGroup.ItemCount > 0 )
            {
               A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
               n1884Alerta_ToGroup = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
            }
            dynAlerta_Owner.Name = "ALERTA_OWNER";
            dynAlerta_Owner.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAALERTA_TOAREATRABALHOOQ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAALERTA_TOAREATRABALHO_dataOQ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_TOAREATRABALHO_htmlOQ2( )
      {
         int gxdynajaxvalue ;
         GXDLAALERTA_TOAREATRABALHO_dataOQ2( ) ;
         gxdynajaxindex = 1;
         dynAlerta_ToAreaTrabalho.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_ToAreaTrabalho.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynAlerta_ToAreaTrabalho.ItemCount > 0 )
         {
            A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( dynAlerta_ToAreaTrabalho.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0))), "."));
            n1882Alerta_ToAreaTrabalho = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
         }
      }

      protected void GXDLAALERTA_TOAREATRABALHO_dataOQ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Indiferente)");
         /* Using cursor H00OQ2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00OQ2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00OQ2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLAALERTA_TOUSEROQ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAALERTA_TOUSER_dataOQ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_TOUSER_htmlOQ2( )
      {
         int gxdynajaxvalue ;
         GXDLAALERTA_TOUSER_dataOQ2( ) ;
         gxdynajaxindex = 1;
         dynAlerta_ToUser.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_ToUser.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynAlerta_ToUser.ItemCount > 0 )
         {
            A1883Alerta_ToUser = (int)(NumberUtil.Val( dynAlerta_ToUser.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0))), "."));
            n1883Alerta_ToUser = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOUSER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
         }
      }

      protected void GXDLAALERTA_TOUSER_dataOQ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Indiferente)");
         /* Using cursor H00OQ3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00OQ3_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00OQ3_A58Usuario_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLAALERTA_OWNEROQ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAALERTA_OWNER_dataOQ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_OWNER_htmlOQ2( )
      {
         int gxdynajaxvalue ;
         GXDLAALERTA_OWNER_dataOQ2( ) ;
         gxdynajaxindex = 1;
         dynAlerta_Owner.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_Owner.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynAlerta_Owner.ItemCount > 0 )
         {
            A1887Alerta_Owner = (int)(NumberUtil.Val( dynAlerta_Owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_OWNER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
         }
      }

      protected void GXDLAALERTA_OWNER_dataOQ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00OQ4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00OQ4_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00OQ4_A58Usuario_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynAlerta_ToAreaTrabalho.ItemCount > 0 )
         {
            A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( dynAlerta_ToAreaTrabalho.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0))), "."));
            n1882Alerta_ToAreaTrabalho = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
         }
         if ( dynAlerta_ToUser.ItemCount > 0 )
         {
            A1883Alerta_ToUser = (int)(NumberUtil.Val( dynAlerta_ToUser.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0))), "."));
            n1883Alerta_ToUser = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOUSER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
         }
         if ( cmbAlerta_ToGroup.ItemCount > 0 )
         {
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
         }
         if ( dynAlerta_Owner.ItemCount > 0 )
         {
            A1887Alerta_Owner = (int)(NumberUtil.Val( dynAlerta_Owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_OWNER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "AlertaGeneral";
         context.Gx_err = 0;
      }

      protected void RFOQ2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00OQ5 */
            pr_default.execute(3, new Object[] {A1881Alerta_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1888Alerta_Cadastro = H00OQ5_A1888Alerta_Cadastro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1888Alerta_Cadastro", context.localUtil.TToC( A1888Alerta_Cadastro, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_CADASTRO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99")));
               A1887Alerta_Owner = H00OQ5_A1887Alerta_Owner[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_OWNER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
               A1886Alerta_Fim = H00OQ5_A1886Alerta_Fim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_FIM", GetSecureSignedToken( sPrefix, A1886Alerta_Fim));
               n1886Alerta_Fim = H00OQ5_n1886Alerta_Fim[0];
               A1885Alerta_Inicio = H00OQ5_A1885Alerta_Inicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_INICIO", GetSecureSignedToken( sPrefix, A1885Alerta_Inicio));
               A1884Alerta_ToGroup = H00OQ5_A1884Alerta_ToGroup[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
               n1884Alerta_ToGroup = H00OQ5_n1884Alerta_ToGroup[0];
               A1883Alerta_ToUser = H00OQ5_A1883Alerta_ToUser[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOUSER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
               n1883Alerta_ToUser = H00OQ5_n1883Alerta_ToUser[0];
               A1882Alerta_ToAreaTrabalho = H00OQ5_A1882Alerta_ToAreaTrabalho[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
               n1882Alerta_ToAreaTrabalho = H00OQ5_n1882Alerta_ToAreaTrabalho[0];
               GXAALERTA_TOAREATRABALHO_htmlOQ2( ) ;
               GXAALERTA_TOUSER_htmlOQ2( ) ;
               GXAALERTA_OWNER_htmlOQ2( ) ;
               /* Execute user event: E12OQ2 */
               E12OQ2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            WBOQ0( ) ;
         }
      }

      protected void STRUPOQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "AlertaGeneral";
         context.Gx_err = 0;
         GXAALERTA_TOAREATRABALHO_htmlOQ2( ) ;
         GXAALERTA_TOUSER_htmlOQ2( ) ;
         GXAALERTA_OWNER_htmlOQ2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OQ2 */
         E11OQ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynAlerta_ToAreaTrabalho.CurrentValue = cgiGet( dynAlerta_ToAreaTrabalho_Internalname);
            A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( cgiGet( dynAlerta_ToAreaTrabalho_Internalname), "."));
            n1882Alerta_ToAreaTrabalho = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
            dynAlerta_ToUser.CurrentValue = cgiGet( dynAlerta_ToUser_Internalname);
            A1883Alerta_ToUser = (int)(NumberUtil.Val( cgiGet( dynAlerta_ToUser_Internalname), "."));
            n1883Alerta_ToUser = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOUSER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
            cmbAlerta_ToGroup.CurrentValue = cgiGet( cmbAlerta_ToGroup_Internalname);
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cgiGet( cmbAlerta_ToGroup_Internalname), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
            A1885Alerta_Inicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAlerta_Inicio_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_INICIO", GetSecureSignedToken( sPrefix, A1885Alerta_Inicio));
            A1886Alerta_Fim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAlerta_Fim_Internalname), 0));
            n1886Alerta_Fim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_FIM", GetSecureSignedToken( sPrefix, A1886Alerta_Fim));
            dynAlerta_Owner.CurrentValue = cgiGet( dynAlerta_Owner_Internalname);
            A1887Alerta_Owner = (int)(NumberUtil.Val( cgiGet( dynAlerta_Owner_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_OWNER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
            A1888Alerta_Cadastro = context.localUtil.CToT( cgiGet( edtAlerta_Cadastro_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1888Alerta_Cadastro", context.localUtil.TToC( A1888Alerta_Cadastro, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ALERTA_CADASTRO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99")));
            /* Read saved values. */
            wcpOA1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1881Alerta_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXAALERTA_TOAREATRABALHO_htmlOQ2( ) ;
            GXAALERTA_TOUSER_htmlOQ2( ) ;
            GXAALERTA_OWNER_htmlOQ2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OQ2 */
         E11OQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11OQ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12OQ2( )
      {
         /* Load Routine */
      }

      protected void E13OQ2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1881Alerta_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14OQ2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1881Alerta_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15OQ2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwalerta.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Alerta";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Alerta_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Alerta_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_OQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_OQ2( true) ;
         }
         else
         {
            wb_table2_8_OQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_OQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_OQ2( true) ;
         }
         else
         {
            wb_table3_46_OQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_46_OQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OQ2e( true) ;
         }
         else
         {
            wb_table1_2_OQ2e( false) ;
         }
      }

      protected void wb_table3_46_OQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_OQ2e( true) ;
         }
         else
         {
            wb_table3_46_OQ2e( false) ;
         }
      }

      protected void wb_table2_8_OQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_toareatrabalho_Internalname, "�rea de Trabalho", "", "", lblTextblockalerta_toareatrabalho_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAlerta_ToAreaTrabalho, dynAlerta_ToAreaTrabalho_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)), 1, dynAlerta_ToAreaTrabalho_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AlertaGeneral.htm");
            dynAlerta_ToAreaTrabalho.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynAlerta_ToAreaTrabalho_Internalname, "Values", (String)(dynAlerta_ToAreaTrabalho.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_touser_Internalname, "Usu�rio", "", "", lblTextblockalerta_touser_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAlerta_ToUser, dynAlerta_ToUser_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)), 1, dynAlerta_ToUser_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AlertaGeneral.htm");
            dynAlerta_ToUser.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynAlerta_ToUser_Internalname, "Values", (String)(dynAlerta_ToUser.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_togroup_Internalname, "Grupo", "", "", lblTextblockalerta_togroup_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAlerta_ToGroup, cmbAlerta_ToGroup_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)), 1, cmbAlerta_ToGroup_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AlertaGeneral.htm");
            cmbAlerta_ToGroup.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAlerta_ToGroup_Internalname, "Values", (String)(cmbAlerta_ToGroup.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_inicio_Internalname, "Inicio", "", "", lblTextblockalerta_inicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAlerta_Inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlerta_Inicio_Internalname, context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"), context.localUtil.Format( A1885Alerta_Inicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Inicio_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AlertaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtAlerta_Inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AlertaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_fim_Internalname, "Fim", "", "", lblTextblockalerta_fim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAlerta_Fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlerta_Fim_Internalname, context.localUtil.Format(A1886Alerta_Fim, "99/99/99"), context.localUtil.Format( A1886Alerta_Fim, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Fim_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AlertaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtAlerta_Fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AlertaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_owner_Internalname, "Owner", "", "", lblTextblockalerta_owner_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAlerta_Owner, dynAlerta_Owner_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)), 1, dynAlerta_Owner_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AlertaGeneral.htm");
            dynAlerta_Owner.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynAlerta_Owner_Internalname, "Values", (String)(dynAlerta_Owner.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_cadastro_Internalname, "Cadastro", "", "", lblTextblockalerta_cadastro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AlertaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAlerta_Cadastro_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlerta_Cadastro_Internalname, context.localUtil.TToC( A1888Alerta_Cadastro, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Cadastro_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AlertaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtAlerta_Cadastro_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AlertaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_OQ2e( true) ;
         }
         else
         {
            wb_table2_8_OQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1881Alerta_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOQ2( ) ;
         WSOQ2( ) ;
         WEOQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1881Alerta_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAOQ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "alertageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAOQ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1881Alerta_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         }
         wcpOA1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1881Alerta_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1881Alerta_Codigo != wcpOA1881Alerta_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1881Alerta_Codigo = A1881Alerta_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1881Alerta_Codigo = cgiGet( sPrefix+"A1881Alerta_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1881Alerta_Codigo) > 0 )
         {
            A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1881Alerta_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         }
         else
         {
            A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1881Alerta_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAOQ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSOQ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSOQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1881Alerta_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1881Alerta_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1881Alerta_Codigo_CTRL", StringUtil.RTrim( sCtrlA1881Alerta_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEOQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211814639");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("alertageneral.js", "?20205211814639");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockalerta_toareatrabalho_Internalname = sPrefix+"TEXTBLOCKALERTA_TOAREATRABALHO";
         dynAlerta_ToAreaTrabalho_Internalname = sPrefix+"ALERTA_TOAREATRABALHO";
         lblTextblockalerta_touser_Internalname = sPrefix+"TEXTBLOCKALERTA_TOUSER";
         dynAlerta_ToUser_Internalname = sPrefix+"ALERTA_TOUSER";
         lblTextblockalerta_togroup_Internalname = sPrefix+"TEXTBLOCKALERTA_TOGROUP";
         cmbAlerta_ToGroup_Internalname = sPrefix+"ALERTA_TOGROUP";
         lblTextblockalerta_inicio_Internalname = sPrefix+"TEXTBLOCKALERTA_INICIO";
         edtAlerta_Inicio_Internalname = sPrefix+"ALERTA_INICIO";
         lblTextblockalerta_fim_Internalname = sPrefix+"TEXTBLOCKALERTA_FIM";
         edtAlerta_Fim_Internalname = sPrefix+"ALERTA_FIM";
         lblTextblockalerta_owner_Internalname = sPrefix+"TEXTBLOCKALERTA_OWNER";
         dynAlerta_Owner_Internalname = sPrefix+"ALERTA_OWNER";
         lblTextblockalerta_cadastro_Internalname = sPrefix+"TEXTBLOCKALERTA_CADASTRO";
         edtAlerta_Cadastro_Internalname = sPrefix+"ALERTA_CADASTRO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAlerta_Cadastro_Jsonclick = "";
         dynAlerta_Owner_Jsonclick = "";
         edtAlerta_Fim_Jsonclick = "";
         edtAlerta_Inicio_Jsonclick = "";
         cmbAlerta_ToGroup_Jsonclick = "";
         dynAlerta_ToUser_Jsonclick = "";
         dynAlerta_ToAreaTrabalho_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13OQ2',iparms:[{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14OQ2',iparms:[{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15OQ2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1885Alerta_Inicio = DateTime.MinValue;
         A1886Alerta_Fim = DateTime.MinValue;
         A1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00OQ2_A5AreaTrabalho_Codigo = new int[1] ;
         H00OQ2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00OQ3_A57Usuario_PessoaCod = new int[1] ;
         H00OQ3_A1Usuario_Codigo = new int[1] ;
         H00OQ3_A58Usuario_PessoaNom = new String[] {""} ;
         H00OQ3_n58Usuario_PessoaNom = new bool[] {false} ;
         H00OQ4_A57Usuario_PessoaCod = new int[1] ;
         H00OQ4_A1Usuario_Codigo = new int[1] ;
         H00OQ4_A58Usuario_PessoaNom = new String[] {""} ;
         H00OQ4_n58Usuario_PessoaNom = new bool[] {false} ;
         H00OQ5_A1881Alerta_Codigo = new int[1] ;
         H00OQ5_A1888Alerta_Cadastro = new DateTime[] {DateTime.MinValue} ;
         H00OQ5_A1887Alerta_Owner = new int[1] ;
         H00OQ5_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         H00OQ5_n1886Alerta_Fim = new bool[] {false} ;
         H00OQ5_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         H00OQ5_A1884Alerta_ToGroup = new short[1] ;
         H00OQ5_n1884Alerta_ToGroup = new bool[] {false} ;
         H00OQ5_A1883Alerta_ToUser = new int[1] ;
         H00OQ5_n1883Alerta_ToUser = new bool[] {false} ;
         H00OQ5_A1882Alerta_ToAreaTrabalho = new int[1] ;
         H00OQ5_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockalerta_toareatrabalho_Jsonclick = "";
         lblTextblockalerta_touser_Jsonclick = "";
         lblTextblockalerta_togroup_Jsonclick = "";
         lblTextblockalerta_inicio_Jsonclick = "";
         lblTextblockalerta_fim_Jsonclick = "";
         lblTextblockalerta_owner_Jsonclick = "";
         lblTextblockalerta_cadastro_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1881Alerta_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.alertageneral__default(),
            new Object[][] {
                new Object[] {
               H00OQ2_A5AreaTrabalho_Codigo, H00OQ2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00OQ3_A57Usuario_PessoaCod, H00OQ3_A1Usuario_Codigo, H00OQ3_A58Usuario_PessoaNom, H00OQ3_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00OQ4_A57Usuario_PessoaCod, H00OQ4_A1Usuario_Codigo, H00OQ4_A58Usuario_PessoaNom, H00OQ4_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00OQ5_A1881Alerta_Codigo, H00OQ5_A1888Alerta_Cadastro, H00OQ5_A1887Alerta_Owner, H00OQ5_A1886Alerta_Fim, H00OQ5_n1886Alerta_Fim, H00OQ5_A1885Alerta_Inicio, H00OQ5_A1884Alerta_ToGroup, H00OQ5_n1884Alerta_ToGroup, H00OQ5_A1883Alerta_ToUser, H00OQ5_n1883Alerta_ToUser,
               H00OQ5_A1882Alerta_ToAreaTrabalho, H00OQ5_n1882Alerta_ToAreaTrabalho
               }
            }
         );
         AV14Pgmname = "AlertaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "AlertaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1884Alerta_ToGroup ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1881Alerta_Codigo ;
      private int wcpOA1881Alerta_Codigo ;
      private int A1882Alerta_ToAreaTrabalho ;
      private int A1883Alerta_ToUser ;
      private int A1887Alerta_Owner ;
      private int gxdynajaxindex ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7Alerta_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynAlerta_ToAreaTrabalho_Internalname ;
      private String dynAlerta_ToUser_Internalname ;
      private String cmbAlerta_ToGroup_Internalname ;
      private String edtAlerta_Inicio_Internalname ;
      private String edtAlerta_Fim_Internalname ;
      private String dynAlerta_Owner_Internalname ;
      private String edtAlerta_Cadastro_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockalerta_toareatrabalho_Internalname ;
      private String lblTextblockalerta_toareatrabalho_Jsonclick ;
      private String dynAlerta_ToAreaTrabalho_Jsonclick ;
      private String lblTextblockalerta_touser_Internalname ;
      private String lblTextblockalerta_touser_Jsonclick ;
      private String dynAlerta_ToUser_Jsonclick ;
      private String lblTextblockalerta_togroup_Internalname ;
      private String lblTextblockalerta_togroup_Jsonclick ;
      private String cmbAlerta_ToGroup_Jsonclick ;
      private String lblTextblockalerta_inicio_Internalname ;
      private String lblTextblockalerta_inicio_Jsonclick ;
      private String edtAlerta_Inicio_Jsonclick ;
      private String lblTextblockalerta_fim_Internalname ;
      private String lblTextblockalerta_fim_Jsonclick ;
      private String edtAlerta_Fim_Jsonclick ;
      private String lblTextblockalerta_owner_Internalname ;
      private String lblTextblockalerta_owner_Jsonclick ;
      private String dynAlerta_Owner_Jsonclick ;
      private String lblTextblockalerta_cadastro_Internalname ;
      private String lblTextblockalerta_cadastro_Jsonclick ;
      private String edtAlerta_Cadastro_Jsonclick ;
      private String sCtrlA1881Alerta_Codigo ;
      private DateTime A1888Alerta_Cadastro ;
      private DateTime A1885Alerta_Inicio ;
      private DateTime A1886Alerta_Fim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1884Alerta_ToGroup ;
      private bool n1882Alerta_ToAreaTrabalho ;
      private bool n1883Alerta_ToUser ;
      private bool n1886Alerta_Fim ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynAlerta_ToAreaTrabalho ;
      private GXCombobox dynAlerta_ToUser ;
      private GXCombobox cmbAlerta_ToGroup ;
      private GXCombobox dynAlerta_Owner ;
      private IDataStoreProvider pr_default ;
      private int[] H00OQ2_A5AreaTrabalho_Codigo ;
      private String[] H00OQ2_A6AreaTrabalho_Descricao ;
      private int[] H00OQ3_A57Usuario_PessoaCod ;
      private int[] H00OQ3_A1Usuario_Codigo ;
      private String[] H00OQ3_A58Usuario_PessoaNom ;
      private bool[] H00OQ3_n58Usuario_PessoaNom ;
      private int[] H00OQ4_A57Usuario_PessoaCod ;
      private int[] H00OQ4_A1Usuario_Codigo ;
      private String[] H00OQ4_A58Usuario_PessoaNom ;
      private bool[] H00OQ4_n58Usuario_PessoaNom ;
      private int[] H00OQ5_A1881Alerta_Codigo ;
      private DateTime[] H00OQ5_A1888Alerta_Cadastro ;
      private int[] H00OQ5_A1887Alerta_Owner ;
      private DateTime[] H00OQ5_A1886Alerta_Fim ;
      private bool[] H00OQ5_n1886Alerta_Fim ;
      private DateTime[] H00OQ5_A1885Alerta_Inicio ;
      private short[] H00OQ5_A1884Alerta_ToGroup ;
      private bool[] H00OQ5_n1884Alerta_ToGroup ;
      private int[] H00OQ5_A1883Alerta_ToUser ;
      private bool[] H00OQ5_n1883Alerta_ToUser ;
      private int[] H00OQ5_A1882Alerta_ToAreaTrabalho ;
      private bool[] H00OQ5_n1882Alerta_ToAreaTrabalho ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class alertageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OQ2 ;
          prmH00OQ2 = new Object[] {
          } ;
          Object[] prmH00OQ3 ;
          prmH00OQ3 = new Object[] {
          } ;
          Object[] prmH00OQ4 ;
          prmH00OQ4 = new Object[] {
          } ;
          Object[] prmH00OQ5 ;
          prmH00OQ5 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OQ2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OQ2,0,0,true,false )
             ,new CursorDef("H00OQ3", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OQ3,0,0,true,false )
             ,new CursorDef("H00OQ4", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OQ4,0,0,true,false )
             ,new CursorDef("H00OQ5", "SELECT [Alerta_Codigo], [Alerta_Cadastro], [Alerta_Owner], [Alerta_Fim], [Alerta_Inicio], [Alerta_ToGroup], [Alerta_ToUser], [Alerta_ToAreaTrabalho] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ORDER BY [Alerta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OQ5,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
