/*
               File: PRC_BuscaPrioridadeOS
        Description: Busca Identifica��o da Prioridade da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:17.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_buscaprioridadeos : GXProcedure
   {
      public prc_buscaprioridadeos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_buscaprioridadeos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosPrioridade_Codigo ,
                           out String aP1_ContratoServicosPrioridade_Nome )
      {
         this.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         this.AV9ContratoServicosPrioridade_Nome = "" ;
         initialize();
         executePrivate();
         aP1_ContratoServicosPrioridade_Nome=this.AV9ContratoServicosPrioridade_Nome;
      }

      public String executeUdp( int aP0_ContratoServicosPrioridade_Codigo )
      {
         this.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         this.AV9ContratoServicosPrioridade_Nome = "" ;
         initialize();
         executePrivate();
         aP1_ContratoServicosPrioridade_Nome=this.AV9ContratoServicosPrioridade_Nome;
         return AV9ContratoServicosPrioridade_Nome ;
      }

      public void executeSubmit( int aP0_ContratoServicosPrioridade_Codigo ,
                                 out String aP1_ContratoServicosPrioridade_Nome )
      {
         prc_buscaprioridadeos objprc_buscaprioridadeos;
         objprc_buscaprioridadeos = new prc_buscaprioridadeos();
         objprc_buscaprioridadeos.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         objprc_buscaprioridadeos.AV9ContratoServicosPrioridade_Nome = "" ;
         objprc_buscaprioridadeos.context.SetSubmitInitialConfig(context);
         objprc_buscaprioridadeos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_buscaprioridadeos);
         aP1_ContratoServicosPrioridade_Nome=this.AV9ContratoServicosPrioridade_Nome;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_buscaprioridadeos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9ContratoServicosPrioridade_Nome = "N�O INFORMADA";
         /* Using cursor P00XQ2 */
         pr_default.execute(0, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1337ContratoServicosPrioridade_Nome = P00XQ2_A1337ContratoServicosPrioridade_Nome[0];
            AV9ContratoServicosPrioridade_Nome = A1337ContratoServicosPrioridade_Nome;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XQ2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         P00XQ2_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         A1337ContratoServicosPrioridade_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_buscaprioridadeos__default(),
            new Object[][] {
                new Object[] {
               P00XQ2_A1336ContratoServicosPrioridade_Codigo, P00XQ2_A1337ContratoServicosPrioridade_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1336ContratoServicosPrioridade_Codigo ;
      private String AV9ContratoServicosPrioridade_Nome ;
      private String scmdbuf ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XQ2_A1336ContratoServicosPrioridade_Codigo ;
      private String[] P00XQ2_A1337ContratoServicosPrioridade_Nome ;
      private String aP1_ContratoServicosPrioridade_Nome ;
   }

   public class prc_buscaprioridadeos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XQ2 ;
          prmP00XQ2 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XQ2", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Nome] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY [ContratoServicosPrioridade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XQ2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
