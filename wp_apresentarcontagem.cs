/*
               File: WP_ApresentarContagem
        Description: Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:47:21.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_apresentarcontagem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_apresentarcontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_apresentarcontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Contagem_Demanda )
      {
         this.A945Contagem_Demanda = aP0_Contagem_Demanda;
         executePrivate();
         aP0_Contagem_Demanda=this.A945Contagem_Demanda;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContagem_ContratadaCod = new GXCombobox();
         cmbContagemItem_TipoUnidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEM_CONTRATADACOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLACONTAGEM_CONTRATADACODSK2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_79 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_79_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_79_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A945Contagem_Demanda = GetNextPar( );
               n945Contagem_Demanda = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A945Contagem_Demanda = gxfirstwebparm;
               n945Contagem_Demanda = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASK2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSK2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299472188");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_apresentarcontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(A945Contagem_Demanda))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_79", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_79), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PROPOSITO", A199Contagem_Proposito);
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PROPOSITO_Enabled", StringUtil.BoolToStr( Contagem_proposito_Enabled));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Width", StringUtil.RTrim( Dvpanel_itenscontagem_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Cls", StringUtil.RTrim( Dvpanel_itenscontagem_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Title", StringUtil.RTrim( Dvpanel_itenscontagem_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Collapsible", StringUtil.BoolToStr( Dvpanel_itenscontagem_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Collapsed", StringUtil.BoolToStr( Dvpanel_itenscontagem_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Autowidth", StringUtil.BoolToStr( Dvpanel_itenscontagem_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Autoheight", StringUtil.BoolToStr( Dvpanel_itenscontagem_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_itenscontagem_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Iconposition", StringUtil.RTrim( Dvpanel_itenscontagem_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_ITENSCONTAGEM_Autoscroll", StringUtil.BoolToStr( Dvpanel_itenscontagem_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Width", StringUtil.RTrim( Dvpanel_paneldadoscontagem_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Cls", StringUtil.RTrim( Dvpanel_paneldadoscontagem_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Title", StringUtil.RTrim( Dvpanel_paneldadoscontagem_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Collapsible", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Collapsed", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Autowidth", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Autoheight", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Iconposition", StringUtil.RTrim( Dvpanel_paneldadoscontagem_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PANELDADOSCONTAGEM_Autoscroll", StringUtil.BoolToStr( Dvpanel_paneldadoscontagem_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESK2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSK2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_apresentarcontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(A945Contagem_Demanda)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ApresentarContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem" ;
      }

      protected void WBSK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SK2( true) ;
         }
         else
         {
            wb_table1_2_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSK0( ) ;
      }

      protected void WSSK2( )
      {
         STARTSK2( ) ;
         EVTSK2( ) ;
      }

      protected void EVTSK2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SK2 */
                              E11SK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SK2 */
                              E12SK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_79_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
                              SubsflControlProps_792( ) ;
                              A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
                              A958ContagemItem_Funcao = cgiGet( edtContagemItem_Funcao_Internalname);
                              n958ContagemItem_Funcao = false;
                              cmbContagemItem_TipoUnidade.Name = cmbContagemItem_TipoUnidade_Internalname;
                              cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A955ContagemItem_CP = cgiGet( edtContagemItem_CP_Internalname);
                              n955ContagemItem_CP = false;
                              A956ContagemItem_RA = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", "."));
                              n956ContagemItem_RA = false;
                              A957ContagemItem_DER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", "."));
                              n957ContagemItem_DER = false;
                              A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
                              n950ContagemItem_PFB = false;
                              A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
                              n951ContagemItem_PFL = false;
                              A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
                              n953ContagemItem_Evidencias = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13SK2 */
                                    E13SK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14SK2 */
                                    E14SK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15SK2 */
                                    E15SK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynContagem_ContratadaCod.Name = "CONTAGEM_CONTRATADACOD";
            dynContagem_ContratadaCod.WebTags = "";
            GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_79_idx;
            cmbContagemItem_TipoUnidade.Name = GXCCtl;
            cmbContagemItem_TipoUnidade.WebTags = "";
            if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
            {
               A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEM_CONTRATADACODSK2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEM_CONTRATADACOD_dataSK2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEM_CONTRATADACOD_htmlSK2( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEM_CONTRATADACOD_dataSK2( ) ;
         gxdynajaxindex = 1;
         dynContagem_ContratadaCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagem_ContratadaCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynContagem_ContratadaCod.ItemCount > 0 )
         {
            A1118Contagem_ContratadaCod = (int)(NumberUtil.Val( dynContagem_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0))), "."));
            n1118Contagem_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLACONTAGEM_CONTRATADACOD_dataSK2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("N�o Informado");
         /* Using cursor H00SK2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SK2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00SK2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_792( ) ;
         while ( nGXsfl_79_idx <= nRC_GXsfl_79 )
         {
            sendrow_792( ) ;
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String A945Contagem_Demanda )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFSK2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_LANCAMENTO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_FUNCAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_FUNCAO", A958ContagemItem_Funcao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_TIPOUNIDADE", A952ContagemItem_TipoUnidade);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_CP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_CP", StringUtil.RTrim( A955ContagemItem_CP));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_RA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_RA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( "", context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFB", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( "", context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFL", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( "", A953ContagemItem_Evidencias));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_EVIDENCIAS", A953ContagemItem_Evidencias);
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagem_ContratadaCod.ItemCount > 0 )
         {
            A1118Contagem_ContratadaCod = (int)(NumberUtil.Val( dynContagem_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0))), "."));
            n1118Contagem_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 79;
         /* Execute user event: E14SK2 */
         E14SK2 ();
         nGXsfl_79_idx = 1;
         sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
         SubsflControlProps_792( ) ;
         nGXsfl_79_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_792( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00SK3 */
            pr_default.execute(1, new Object[] {n945Contagem_Demanda, A945Contagem_Demanda, GXPagingFrom2, GXPagingTo2});
            nGXsfl_79_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A192Contagem_Codigo = H00SK3_A192Contagem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               A197Contagem_DataCriacao = H00SK3_A197Contagem_DataCriacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               A940Contagem_SistemaCod = H00SK3_A940Contagem_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               n940Contagem_SistemaCod = H00SK3_n940Contagem_SistemaCod[0];
               A941Contagem_SistemaSigla = H00SK3_A941Contagem_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
               n941Contagem_SistemaSigla = H00SK3_n941Contagem_SistemaSigla[0];
               A199Contagem_Proposito = H00SK3_A199Contagem_Proposito[0];
               n199Contagem_Proposito = H00SK3_n199Contagem_Proposito[0];
               A213Contagem_UsuarioContadorCod = H00SK3_A213Contagem_UsuarioContadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               n213Contagem_UsuarioContadorCod = H00SK3_n213Contagem_UsuarioContadorCod[0];
               A1811Contagem_ServicoCod = H00SK3_A1811Contagem_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
               n1811Contagem_ServicoCod = H00SK3_n1811Contagem_ServicoCod[0];
               A943Contagem_PFB = H00SK3_A943Contagem_PFB[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
               n943Contagem_PFB = H00SK3_n943Contagem_PFB[0];
               A944Contagem_PFL = H00SK3_A944Contagem_PFL[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
               n944Contagem_PFL = H00SK3_n944Contagem_PFL[0];
               A953ContagemItem_Evidencias = H00SK3_A953ContagemItem_Evidencias[0];
               n953ContagemItem_Evidencias = H00SK3_n953ContagemItem_Evidencias[0];
               A951ContagemItem_PFL = H00SK3_A951ContagemItem_PFL[0];
               n951ContagemItem_PFL = H00SK3_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = H00SK3_A950ContagemItem_PFB[0];
               n950ContagemItem_PFB = H00SK3_n950ContagemItem_PFB[0];
               A957ContagemItem_DER = H00SK3_A957ContagemItem_DER[0];
               n957ContagemItem_DER = H00SK3_n957ContagemItem_DER[0];
               A956ContagemItem_RA = H00SK3_A956ContagemItem_RA[0];
               n956ContagemItem_RA = H00SK3_n956ContagemItem_RA[0];
               A955ContagemItem_CP = H00SK3_A955ContagemItem_CP[0];
               n955ContagemItem_CP = H00SK3_n955ContagemItem_CP[0];
               A952ContagemItem_TipoUnidade = H00SK3_A952ContagemItem_TipoUnidade[0];
               A958ContagemItem_Funcao = H00SK3_A958ContagemItem_Funcao[0];
               n958ContagemItem_Funcao = H00SK3_n958ContagemItem_Funcao[0];
               A224ContagemItem_Lancamento = H00SK3_A224ContagemItem_Lancamento[0];
               A197Contagem_DataCriacao = H00SK3_A197Contagem_DataCriacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               A940Contagem_SistemaCod = H00SK3_A940Contagem_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               n940Contagem_SistemaCod = H00SK3_n940Contagem_SistemaCod[0];
               A199Contagem_Proposito = H00SK3_A199Contagem_Proposito[0];
               n199Contagem_Proposito = H00SK3_n199Contagem_Proposito[0];
               A213Contagem_UsuarioContadorCod = H00SK3_A213Contagem_UsuarioContadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               n213Contagem_UsuarioContadorCod = H00SK3_n213Contagem_UsuarioContadorCod[0];
               A1811Contagem_ServicoCod = H00SK3_A1811Contagem_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
               n1811Contagem_ServicoCod = H00SK3_n1811Contagem_ServicoCod[0];
               A943Contagem_PFB = H00SK3_A943Contagem_PFB[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
               n943Contagem_PFB = H00SK3_n943Contagem_PFB[0];
               A944Contagem_PFL = H00SK3_A944Contagem_PFL[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
               n944Contagem_PFL = H00SK3_n944Contagem_PFL[0];
               A941Contagem_SistemaSigla = H00SK3_A941Contagem_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
               n941Contagem_SistemaSigla = H00SK3_n941Contagem_SistemaSigla[0];
               /* Execute user event: E15SK2 */
               E15SK2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 79;
            WBSK0( ) ;
         }
         nGXsfl_79_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00SK4 */
         pr_default.execute(2, new Object[] {n945Contagem_Demanda, A945Contagem_Demanda});
         GRID_nRecordCount = H00SK4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A945Contagem_Demanda) ;
         }
         return (int)(0) ;
      }

      protected void STRUPSK0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXACONTAGEM_CONTRATADACOD_htmlSK2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13SK2 */
         E13SK2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
            n940Contagem_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            A941Contagem_SistemaSigla = StringUtil.Upper( cgiGet( edtContagem_SistemaSigla_Internalname));
            n941Contagem_SistemaSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
            dynContagem_ContratadaCod.Name = dynContagem_ContratadaCod_Internalname;
            dynContagem_ContratadaCod.CurrentValue = cgiGet( dynContagem_ContratadaCod_Internalname);
            A1118Contagem_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynContagem_ContratadaCod_Internalname), "."));
            n1118Contagem_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            A1811Contagem_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ServicoCod_Internalname), ",", "."));
            n1811Contagem_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
            A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
            n943Contagem_PFB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
            A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
            n944Contagem_PFL = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDCURRENTPAGE");
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6GridCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
            }
            else
            {
               AV6GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_79 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_79"), ",", "."));
            A199Contagem_Proposito = cgiGet( "CONTAGEM_PROPOSITO");
            AV7GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Contagem_proposito_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Enabled"));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Dvpanel_itenscontagem_Width = cgiGet( "DVPANEL_ITENSCONTAGEM_Width");
            Dvpanel_itenscontagem_Cls = cgiGet( "DVPANEL_ITENSCONTAGEM_Cls");
            Dvpanel_itenscontagem_Title = cgiGet( "DVPANEL_ITENSCONTAGEM_Title");
            Dvpanel_itenscontagem_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Collapsible"));
            Dvpanel_itenscontagem_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Collapsed"));
            Dvpanel_itenscontagem_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Autowidth"));
            Dvpanel_itenscontagem_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Autoheight"));
            Dvpanel_itenscontagem_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Showcollapseicon"));
            Dvpanel_itenscontagem_Iconposition = cgiGet( "DVPANEL_ITENSCONTAGEM_Iconposition");
            Dvpanel_itenscontagem_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_ITENSCONTAGEM_Autoscroll"));
            Dvpanel_paneldadoscontagem_Width = cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Width");
            Dvpanel_paneldadoscontagem_Cls = cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Cls");
            Dvpanel_paneldadoscontagem_Title = cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Title");
            Dvpanel_paneldadoscontagem_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Collapsible"));
            Dvpanel_paneldadoscontagem_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Collapsed"));
            Dvpanel_paneldadoscontagem_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Autowidth"));
            Dvpanel_paneldadoscontagem_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Autoheight"));
            Dvpanel_paneldadoscontagem_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Showcollapseicon"));
            Dvpanel_paneldadoscontagem_Iconposition = cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Iconposition");
            Dvpanel_paneldadoscontagem_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PANELDADOSCONTAGEM_Autoscroll"));
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13SK2 */
         E13SK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13SK2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV6GridCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
         edtavGridcurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGridcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridcurrentpage_Visible), 5, 0)));
         AV7GridPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridPageCount), 10, 0)));
      }

      protected void E14SK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      private void E15SK2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 79;
         }
         sendrow_792( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_79_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(79, GridRow);
         }
      }

      protected void E11SK2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV6GridCurrentPage = (long)(AV6GridCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            AV6GridCurrentPage = (long)(AV6GridCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
            subgrid_nextpage( ) ;
         }
         else
         {
            AV5PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            AV6GridCurrentPage = AV5PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6GridCurrentPage), 10, 0)));
            subgrid_gotopage( AV5PageToGo) ;
         }
         context.DoAjaxRefresh();
      }

      protected void E12SK2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {(String)A945Contagem_Demanda});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void wb_table1_2_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_SK2( true) ;
         }
         else
         {
            wb_table2_8_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SK2e( true) ;
         }
         else
         {
            wb_table1_2_SK2e( false) ;
         }
      }

      protected void wb_table2_8_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PANELDADOSCONTAGEMContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PANELDADOSCONTAGEMContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table3_13_SK2( true) ;
         }
         else
         {
            wb_table3_13_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_13_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_SK2e( true) ;
         }
         else
         {
            wb_table2_8_SK2e( false) ;
         }
      }

      protected void wb_table3_13_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPaneldadoscontagem_Internalname, tblPaneldadoscontagem_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_16_SK2( true) ;
         }
         else
         {
            wb_table4_16_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_68_SK2( true) ;
         }
         else
         {
            wb_table5_68_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table5_68_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table6_95_SK2( true) ;
         }
         else
         {
            wb_table6_95_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table6_95_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_SK2e( true) ;
         }
         else
         {
            wb_table3_13_SK2e( false) ;
         }
      }

      protected void wb_table6_95_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(79), 2, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_95_SK2e( true) ;
         }
         else
         {
            wb_table6_95_SK2e( false) ;
         }
      }

      protected void wb_table5_68_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbaitens_Internalname, tblTbaitens_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_ITENSCONTAGEMContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_ITENSCONTAGEMContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table7_73_SK2( true) ;
         }
         else
         {
            wb_table7_73_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table7_73_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_68_SK2e( true) ;
         }
         else
         {
            wb_table5_68_SK2e( false) ;
         }
      }

      protected void wb_table7_73_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblItenscontagem_Internalname, tblItenscontagem_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_76_SK2( true) ;
         }
         else
         {
            wb_table8_76_SK2( false) ;
         }
         return  ;
      }

      protected void wb_table8_76_SK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_73_SK2e( true) ;
         }
         else
         {
            wb_table7_73_SK2e( false) ;
         }
      }

      protected void wb_table8_76_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"79\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Item") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Complexidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtd R/A") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtd DER") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFB") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFL") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Documenta��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A958ContagemItem_Funcao);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A952ContagemItem_TipoUnidade);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A955ContagemItem_CP));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A953ContagemItem_Evidencias);
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 79 )
         {
            wbEnd = 0;
            nRC_GXsfl_79 = (short)(nGXsfl_79_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridcurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6GridCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6GridCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridcurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridcurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_76_SK2e( true) ;
         }
         else
         {
            wb_table8_76_SK2e( false) ;
         }
      }

      protected void wb_table4_16_SK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecab_Internalname, tblTablecab_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "Contagem", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_demanda_Internalname, "Demanda", "", "", lblTextblockcontagem_demanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Demanda_Internalname, A945Contagem_Demanda, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Demanda_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datacriacao_Internalname, "Data de Contagem", "", "", lblTextblockcontagem_datacriacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagem_DataCriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataCriacao_Internalname, context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"), context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataCriacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_ApresentarContagem.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataCriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemasigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaSigla_Internalname, StringUtil.RTrim( A941Contagem_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A941Contagem_SistemaSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_proposito_Internalname, "Prop�sito", "", "", lblTextblockcontagem_proposito_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEM_PROPOSITOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_contratadacod_Internalname, "Contratada", "", "", lblTextblockcontagem_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagem_ContratadaCod, dynContagem_ContratadaCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)), 1, dynContagem_ContratadaCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_WP_ApresentarContagem.htm");
            dynContagem_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagem_ContratadaCod_Internalname, "Values", (String)(dynContagem_ContratadaCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorcod_Internalname, "Respons�vel", "", "", lblTextblockcontagem_usuariocontadorcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_servicocod_Internalname, "Servi�o", "", "", lblTextblockcontagem_servicocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1811Contagem_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ServicoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfb_Internalname, "PFB", "", "", lblTextblockcontagem_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")), context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFB_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfl_Internalname, "PFL", "", "", lblTextblockcontagem_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")), context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFL_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ApresentarContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_SK2e( true) ;
         }
         else
         {
            wb_table4_16_SK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A945Contagem_Demanda = (String)getParm(obj,0);
         n945Contagem_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASK2( ) ;
         WSSK2( ) ;
         WESK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299472377");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_apresentarcontagem.js", "?20205299472377");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
            context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_792( )
      {
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_79_idx;
         edtContagemItem_Funcao_Internalname = "CONTAGEMITEM_FUNCAO_"+sGXsfl_79_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_79_idx;
         edtContagemItem_CP_Internalname = "CONTAGEMITEM_CP_"+sGXsfl_79_idx;
         edtContagemItem_RA_Internalname = "CONTAGEMITEM_RA_"+sGXsfl_79_idx;
         edtContagemItem_DER_Internalname = "CONTAGEMITEM_DER_"+sGXsfl_79_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_79_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_79_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_79_idx;
      }

      protected void SubsflControlProps_fel_792( )
      {
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_79_fel_idx;
         edtContagemItem_Funcao_Internalname = "CONTAGEMITEM_FUNCAO_"+sGXsfl_79_fel_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_79_fel_idx;
         edtContagemItem_CP_Internalname = "CONTAGEMITEM_CP_"+sGXsfl_79_fel_idx;
         edtContagemItem_RA_Internalname = "CONTAGEMITEM_RA_"+sGXsfl_79_fel_idx;
         edtContagemItem_DER_Internalname = "CONTAGEMITEM_DER_"+sGXsfl_79_fel_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_79_fel_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_79_fel_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_79_fel_idx;
      }

      protected void sendrow_792( )
      {
         SubsflControlProps_792( ) ;
         WBSK0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_79_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_79_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_79_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Lancamento_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Lancamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Funcao_Internalname,(String)A958ContagemItem_Funcao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Funcao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_79_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_79_idx;
               cmbContagemItem_TipoUnidade.Name = GXCCtl;
               cmbContagemItem_TipoUnidade.WebTags = "";
               if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
               {
                  A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_TipoUnidade,(String)cmbContagemItem_TipoUnidade_Internalname,StringUtil.RTrim( A952ContagemItem_TipoUnidade),(short)1,(String)cmbContagemItem_TipoUnidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_CP_Internalname,StringUtil.RTrim( A955ContagemItem_CP),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_CP_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"CP",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_RA_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_RA_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_DER_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")),context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")),context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Evidencias_Internalname,(String)A953ContagemItem_Evidencias,(String)A953ContagemItem_Evidencias,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Evidencias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5000,(short)0,(short)0,(short)79,(short)1,(short)0,(short)-1,(bool)true,(String)"Evidencias",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_LANCAMENTO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_FUNCAO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_CP"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_RA"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_DER"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A953ContagemItem_Evidencias));
            GridContainer.AddRow(GridRow);
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         /* End function sendrow_792 */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagem_codigo_Internalname = "TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         lblTextblockcontagem_demanda_Internalname = "TEXTBLOCKCONTAGEM_DEMANDA";
         edtContagem_Demanda_Internalname = "CONTAGEM_DEMANDA";
         lblTextblockcontagem_datacriacao_Internalname = "TEXTBLOCKCONTAGEM_DATACRIACAO";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         lblTextblockcontagem_sistemacod_Internalname = "TEXTBLOCKCONTAGEM_SISTEMACOD";
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD";
         lblTextblockcontagem_sistemasigla_Internalname = "TEXTBLOCKCONTAGEM_SISTEMASIGLA";
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA";
         lblTextblockcontagem_proposito_Internalname = "TEXTBLOCKCONTAGEM_PROPOSITO";
         Contagem_proposito_Internalname = "CONTAGEM_PROPOSITO";
         lblTextblockcontagem_contratadacod_Internalname = "TEXTBLOCKCONTAGEM_CONTRATADACOD";
         dynContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD";
         lblTextblockcontagem_usuariocontadorcod_Internalname = "TEXTBLOCKCONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD";
         lblTextblockcontagem_servicocod_Internalname = "TEXTBLOCKCONTAGEM_SERVICOCOD";
         edtContagem_ServicoCod_Internalname = "CONTAGEM_SERVICOCOD";
         lblTextblockcontagem_pfb_Internalname = "TEXTBLOCKCONTAGEM_PFB";
         edtContagem_PFB_Internalname = "CONTAGEM_PFB";
         lblTextblockcontagem_pfl_Internalname = "TEXTBLOCKCONTAGEM_PFL";
         edtContagem_PFL_Internalname = "CONTAGEM_PFL";
         tblTablecab_Internalname = "TABLECAB";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Funcao_Internalname = "CONTAGEMITEM_FUNCAO";
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE";
         edtContagemItem_CP_Internalname = "CONTAGEMITEM_CP";
         edtContagemItem_RA_Internalname = "CONTAGEMITEM_RA";
         edtContagemItem_DER_Internalname = "CONTAGEMITEM_DER";
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB";
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL";
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         edtavGridcurrentpage_Internalname = "vGRIDCURRENTPAGE";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblItenscontagem_Internalname = "ITENSCONTAGEM";
         Dvpanel_itenscontagem_Internalname = "DVPANEL_ITENSCONTAGEM";
         tblTbaitens_Internalname = "TBAITENS";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblPaneldadoscontagem_Internalname = "PANELDADOSCONTAGEM";
         Dvpanel_paneldadoscontagem_Internalname = "DVPANEL_PANELDADOSCONTAGEM";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemItem_Evidencias_Jsonclick = "";
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFB_Jsonclick = "";
         edtContagemItem_DER_Jsonclick = "";
         edtContagemItem_RA_Jsonclick = "";
         edtContagemItem_CP_Jsonclick = "";
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         edtContagemItem_Funcao_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFB_Jsonclick = "";
         edtContagem_ServicoCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         dynContagem_ContratadaCod_Jsonclick = "";
         Contagem_proposito_Enabled = Convert.ToBoolean( 0);
         edtContagem_SistemaSigla_Jsonclick = "";
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_Demanda_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtavGridcurrentpage_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavGridcurrentpage_Visible = 1;
         subGrid_Backcolorstyle = 3;
         Dvpanel_paneldadoscontagem_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_paneldadoscontagem_Iconposition = "left";
         Dvpanel_paneldadoscontagem_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_paneldadoscontagem_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_paneldadoscontagem_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_paneldadoscontagem_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_paneldadoscontagem_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_paneldadoscontagem_Title = "Contagem";
         Dvpanel_paneldadoscontagem_Cls = "GXUI-DVelop-Panel";
         Dvpanel_paneldadoscontagem_Width = "100%";
         Dvpanel_itenscontagem_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_itenscontagem_Iconposition = "left";
         Dvpanel_itenscontagem_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_itenscontagem_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_itenscontagem_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_itenscontagem_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_itenscontagem_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_itenscontagem_Title = "Detalhes";
         Dvpanel_itenscontagem_Cls = "GXUI-DVelop-Panel";
         Dvpanel_itenscontagem_Width = "100%";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A945Contagem_Demanda',fld:'CONTAGEM_DEMANDA',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E15SK2',iparms:[],oparms:[]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11SK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A945Contagem_Demanda',fld:'CONTAGEM_DEMANDA',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'},{av:'AV6GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV6GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E12SK2',iparms:[{av:'A945Contagem_Demanda',fld:'CONTAGEM_DEMANDA',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA945Contagem_Demanda = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A199Contagem_Proposito = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A958ContagemItem_Funcao = "";
         A952ContagemItem_TipoUnidade = "";
         A955ContagemItem_CP = "";
         A953ContagemItem_Evidencias = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00SK2_A40Contratada_PessoaCod = new int[1] ;
         H00SK2_A39Contratada_Codigo = new int[1] ;
         H00SK2_A41Contratada_PessoaNom = new String[] {""} ;
         H00SK2_n41Contratada_PessoaNom = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         H00SK3_A945Contagem_Demanda = new String[] {""} ;
         H00SK3_n945Contagem_Demanda = new bool[] {false} ;
         H00SK3_A192Contagem_Codigo = new int[1] ;
         H00SK3_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00SK3_A940Contagem_SistemaCod = new int[1] ;
         H00SK3_n940Contagem_SistemaCod = new bool[] {false} ;
         H00SK3_A941Contagem_SistemaSigla = new String[] {""} ;
         H00SK3_n941Contagem_SistemaSigla = new bool[] {false} ;
         H00SK3_A199Contagem_Proposito = new String[] {""} ;
         H00SK3_n199Contagem_Proposito = new bool[] {false} ;
         H00SK3_A1118Contagem_ContratadaCod = new int[1] ;
         H00SK3_n1118Contagem_ContratadaCod = new bool[] {false} ;
         H00SK3_A213Contagem_UsuarioContadorCod = new int[1] ;
         H00SK3_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         H00SK3_A1811Contagem_ServicoCod = new int[1] ;
         H00SK3_n1811Contagem_ServicoCod = new bool[] {false} ;
         H00SK3_A943Contagem_PFB = new decimal[1] ;
         H00SK3_n943Contagem_PFB = new bool[] {false} ;
         H00SK3_A944Contagem_PFL = new decimal[1] ;
         H00SK3_n944Contagem_PFL = new bool[] {false} ;
         H00SK3_A953ContagemItem_Evidencias = new String[] {""} ;
         H00SK3_n953ContagemItem_Evidencias = new bool[] {false} ;
         H00SK3_A951ContagemItem_PFL = new decimal[1] ;
         H00SK3_n951ContagemItem_PFL = new bool[] {false} ;
         H00SK3_A950ContagemItem_PFB = new decimal[1] ;
         H00SK3_n950ContagemItem_PFB = new bool[] {false} ;
         H00SK3_A957ContagemItem_DER = new short[1] ;
         H00SK3_n957ContagemItem_DER = new bool[] {false} ;
         H00SK3_A956ContagemItem_RA = new short[1] ;
         H00SK3_n956ContagemItem_RA = new bool[] {false} ;
         H00SK3_A955ContagemItem_CP = new String[] {""} ;
         H00SK3_n955ContagemItem_CP = new bool[] {false} ;
         H00SK3_A952ContagemItem_TipoUnidade = new String[] {""} ;
         H00SK3_A958ContagemItem_Funcao = new String[] {""} ;
         H00SK3_n958ContagemItem_Funcao = new bool[] {false} ;
         H00SK3_A224ContagemItem_Lancamento = new int[1] ;
         A197Contagem_DataCriacao = DateTime.MinValue;
         A941Contagem_SistemaSigla = "";
         H00SK4_AGRID_nRecordCount = new long[1] ;
         GridRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnfechar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockcontagem_demanda_Jsonclick = "";
         lblTextblockcontagem_datacriacao_Jsonclick = "";
         lblTextblockcontagem_sistemacod_Jsonclick = "";
         lblTextblockcontagem_sistemasigla_Jsonclick = "";
         lblTextblockcontagem_proposito_Jsonclick = "";
         lblTextblockcontagem_contratadacod_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorcod_Jsonclick = "";
         lblTextblockcontagem_servicocod_Jsonclick = "";
         lblTextblockcontagem_pfb_Jsonclick = "";
         lblTextblockcontagem_pfl_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_apresentarcontagem__default(),
            new Object[][] {
                new Object[] {
               H00SK2_A40Contratada_PessoaCod, H00SK2_A39Contratada_Codigo, H00SK2_A41Contratada_PessoaNom, H00SK2_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00SK3_A945Contagem_Demanda, H00SK3_n945Contagem_Demanda, H00SK3_A192Contagem_Codigo, H00SK3_A197Contagem_DataCriacao, H00SK3_A940Contagem_SistemaCod, H00SK3_n940Contagem_SistemaCod, H00SK3_A941Contagem_SistemaSigla, H00SK3_n941Contagem_SistemaSigla, H00SK3_A199Contagem_Proposito, H00SK3_n199Contagem_Proposito,
               H00SK3_A1118Contagem_ContratadaCod, H00SK3_n1118Contagem_ContratadaCod, H00SK3_A213Contagem_UsuarioContadorCod, H00SK3_n213Contagem_UsuarioContadorCod, H00SK3_A1811Contagem_ServicoCod, H00SK3_n1811Contagem_ServicoCod, H00SK3_A943Contagem_PFB, H00SK3_n943Contagem_PFB, H00SK3_A944Contagem_PFL, H00SK3_n944Contagem_PFL,
               H00SK3_A953ContagemItem_Evidencias, H00SK3_n953ContagemItem_Evidencias, H00SK3_A951ContagemItem_PFL, H00SK3_n951ContagemItem_PFL, H00SK3_A950ContagemItem_PFB, H00SK3_n950ContagemItem_PFB, H00SK3_A957ContagemItem_DER, H00SK3_n957ContagemItem_DER, H00SK3_A956ContagemItem_RA, H00SK3_n956ContagemItem_RA,
               H00SK3_A955ContagemItem_CP, H00SK3_n955ContagemItem_CP, H00SK3_A952ContagemItem_TipoUnidade, H00SK3_A958ContagemItem_Funcao, H00SK3_n958ContagemItem_Funcao, H00SK3_A224ContagemItem_Lancamento
               }
               , new Object[] {
               H00SK4_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_79 ;
      private short nGXsfl_79_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A956ContagemItem_RA ;
      private short A957ContagemItem_DER ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_79_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A224ContagemItem_Lancamento ;
      private int gxdynajaxindex ;
      private int A1118Contagem_ContratadaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A192Contagem_Codigo ;
      private int A940Contagem_SistemaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A1811Contagem_ServicoCod ;
      private int edtavGridcurrentpage_Visible ;
      private int AV5PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV7GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV6GridCurrentPage ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_79_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Dvpanel_itenscontagem_Width ;
      private String Dvpanel_itenscontagem_Cls ;
      private String Dvpanel_itenscontagem_Title ;
      private String Dvpanel_itenscontagem_Iconposition ;
      private String Dvpanel_paneldadoscontagem_Width ;
      private String Dvpanel_paneldadoscontagem_Cls ;
      private String Dvpanel_paneldadoscontagem_Title ;
      private String Dvpanel_paneldadoscontagem_Iconposition ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItem_Funcao_Internalname ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String A955ContagemItem_CP ;
      private String edtContagemItem_CP_Internalname ;
      private String edtContagemItem_RA_Internalname ;
      private String edtContagemItem_DER_Internalname ;
      private String edtContagemItem_PFB_Internalname ;
      private String edtContagemItem_PFL_Internalname ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String GXCCtl ;
      private String edtavGridcurrentpage_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String A941Contagem_SistemaSigla ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_SistemaCod_Internalname ;
      private String edtContagem_SistemaSigla_Internalname ;
      private String dynContagem_ContratadaCod_Internalname ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_ServicoCod_Internalname ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFL_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablecontent_Internalname ;
      private String tblPaneldadoscontagem_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTbaitens_Internalname ;
      private String tblItenscontagem_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String edtavGridcurrentpage_Jsonclick ;
      private String tblTablecab_Internalname ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_demanda_Internalname ;
      private String lblTextblockcontagem_demanda_Jsonclick ;
      private String edtContagem_Demanda_Internalname ;
      private String edtContagem_Demanda_Jsonclick ;
      private String lblTextblockcontagem_datacriacao_Internalname ;
      private String lblTextblockcontagem_datacriacao_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String lblTextblockcontagem_sistemacod_Internalname ;
      private String lblTextblockcontagem_sistemacod_Jsonclick ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String lblTextblockcontagem_sistemasigla_Internalname ;
      private String lblTextblockcontagem_sistemasigla_Jsonclick ;
      private String edtContagem_SistemaSigla_Jsonclick ;
      private String lblTextblockcontagem_proposito_Internalname ;
      private String lblTextblockcontagem_proposito_Jsonclick ;
      private String lblTextblockcontagem_contratadacod_Internalname ;
      private String lblTextblockcontagem_contratadacod_Jsonclick ;
      private String dynContagem_ContratadaCod_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorcod_Internalname ;
      private String lblTextblockcontagem_usuariocontadorcod_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String lblTextblockcontagem_servicocod_Internalname ;
      private String lblTextblockcontagem_servicocod_Jsonclick ;
      private String edtContagem_ServicoCod_Jsonclick ;
      private String lblTextblockcontagem_pfb_Internalname ;
      private String lblTextblockcontagem_pfb_Jsonclick ;
      private String edtContagem_PFB_Jsonclick ;
      private String lblTextblockcontagem_pfl_Internalname ;
      private String lblTextblockcontagem_pfl_Jsonclick ;
      private String edtContagem_PFL_Jsonclick ;
      private String sGXsfl_79_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String edtContagemItem_Funcao_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String edtContagemItem_CP_Jsonclick ;
      private String edtContagemItem_RA_Jsonclick ;
      private String edtContagemItem_DER_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String edtContagemItem_Evidencias_Jsonclick ;
      private String Contagem_proposito_Internalname ;
      private String Gridpaginationbar_Internalname ;
      private String Dvpanel_itenscontagem_Internalname ;
      private String Dvpanel_paneldadoscontagem_Internalname ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool n945Contagem_Demanda ;
      private bool toggleJsOutput ;
      private bool Contagem_proposito_Enabled ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Dvpanel_itenscontagem_Collapsible ;
      private bool Dvpanel_itenscontagem_Collapsed ;
      private bool Dvpanel_itenscontagem_Autowidth ;
      private bool Dvpanel_itenscontagem_Autoheight ;
      private bool Dvpanel_itenscontagem_Showcollapseicon ;
      private bool Dvpanel_itenscontagem_Autoscroll ;
      private bool Dvpanel_paneldadoscontagem_Collapsible ;
      private bool Dvpanel_paneldadoscontagem_Collapsed ;
      private bool Dvpanel_paneldadoscontagem_Autowidth ;
      private bool Dvpanel_paneldadoscontagem_Autoheight ;
      private bool Dvpanel_paneldadoscontagem_Showcollapseicon ;
      private bool Dvpanel_paneldadoscontagem_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n958ContagemItem_Funcao ;
      private bool n955ContagemItem_CP ;
      private bool n956ContagemItem_RA ;
      private bool n957ContagemItem_DER ;
      private bool n950ContagemItem_PFB ;
      private bool n951ContagemItem_PFL ;
      private bool n953ContagemItem_Evidencias ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n940Contagem_SistemaCod ;
      private bool n941Contagem_SistemaSigla ;
      private bool n199Contagem_Proposito ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n1811Contagem_ServicoCod ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A199Contagem_Proposito ;
      private String A953ContagemItem_Evidencias ;
      private String A945Contagem_Demanda ;
      private String wcpOA945Contagem_Demanda ;
      private String A958ContagemItem_Funcao ;
      private String A952ContagemItem_TipoUnidade ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Contagem_Demanda ;
      private GXCombobox dynContagem_ContratadaCod ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private IDataStoreProvider pr_default ;
      private int[] H00SK2_A40Contratada_PessoaCod ;
      private int[] H00SK2_A39Contratada_Codigo ;
      private String[] H00SK2_A41Contratada_PessoaNom ;
      private bool[] H00SK2_n41Contratada_PessoaNom ;
      private String[] H00SK3_A945Contagem_Demanda ;
      private bool[] H00SK3_n945Contagem_Demanda ;
      private int[] H00SK3_A192Contagem_Codigo ;
      private DateTime[] H00SK3_A197Contagem_DataCriacao ;
      private int[] H00SK3_A940Contagem_SistemaCod ;
      private bool[] H00SK3_n940Contagem_SistemaCod ;
      private String[] H00SK3_A941Contagem_SistemaSigla ;
      private bool[] H00SK3_n941Contagem_SistemaSigla ;
      private String[] H00SK3_A199Contagem_Proposito ;
      private bool[] H00SK3_n199Contagem_Proposito ;
      private int[] H00SK3_A1118Contagem_ContratadaCod ;
      private bool[] H00SK3_n1118Contagem_ContratadaCod ;
      private int[] H00SK3_A213Contagem_UsuarioContadorCod ;
      private bool[] H00SK3_n213Contagem_UsuarioContadorCod ;
      private int[] H00SK3_A1811Contagem_ServicoCod ;
      private bool[] H00SK3_n1811Contagem_ServicoCod ;
      private decimal[] H00SK3_A943Contagem_PFB ;
      private bool[] H00SK3_n943Contagem_PFB ;
      private decimal[] H00SK3_A944Contagem_PFL ;
      private bool[] H00SK3_n944Contagem_PFL ;
      private String[] H00SK3_A953ContagemItem_Evidencias ;
      private bool[] H00SK3_n953ContagemItem_Evidencias ;
      private decimal[] H00SK3_A951ContagemItem_PFL ;
      private bool[] H00SK3_n951ContagemItem_PFL ;
      private decimal[] H00SK3_A950ContagemItem_PFB ;
      private bool[] H00SK3_n950ContagemItem_PFB ;
      private short[] H00SK3_A957ContagemItem_DER ;
      private bool[] H00SK3_n957ContagemItem_DER ;
      private short[] H00SK3_A956ContagemItem_RA ;
      private bool[] H00SK3_n956ContagemItem_RA ;
      private String[] H00SK3_A955ContagemItem_CP ;
      private bool[] H00SK3_n955ContagemItem_CP ;
      private String[] H00SK3_A952ContagemItem_TipoUnidade ;
      private String[] H00SK3_A958ContagemItem_Funcao ;
      private bool[] H00SK3_n958ContagemItem_Funcao ;
      private int[] H00SK3_A224ContagemItem_Lancamento ;
      private long[] H00SK4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_apresentarcontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SK2 ;
          prmH00SK2 = new Object[] {
          } ;
          Object[] prmH00SK3 ;
          prmH00SK3 = new Object[] {
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00SK4 ;
          prmH00SK4 = new Object[] {
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SK2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SK2,0,0,true,false )
             ,new CursorDef("H00SK3", "SELECT * FROM (SELECT  T2.[Contagem_Demanda], T1.[Contagem_Codigo], T2.[Contagem_DataCriacao], T2.[Contagem_SistemaCod] AS Contagem_SistemaCod, T3.[Sistema_Sigla] AS Contagem_SistemaSigla, T2.[Contagem_Proposito], T2.[Contagem_ContratadaCod], T2.[Contagem_UsuarioContadorCod], T2.[Contagem_ServicoCod], T2.[Contagem_PFB], T2.[Contagem_PFL], T1.[ContagemItem_Evidencias], T1.[ContagemItem_PFL], T1.[ContagemItem_PFB], T1.[ContagemItem_DER], T1.[ContagemItem_RA], T1.[ContagemItem_CP], T1.[ContagemItem_TipoUnidade], T1.[ContagemItem_Funcao], T1.[ContagemItem_Lancamento], ROW_NUMBER() OVER ( ORDER BY T1.[ContagemItem_Lancamento] ) AS GX_ROW_NUMBER FROM (([ContagemItem] T1 WITH (NOLOCK) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Contagem_SistemaCod]) WHERE T2.[Contagem_Demanda] = @Contagem_Demanda) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SK3,11,0,true,false )
             ,new CursorDef("H00SK4", "SELECT COUNT(*) FROM (([ContagemItem] T1 WITH (NOLOCK) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Contagem_SistemaCod]) WHERE T2.[Contagem_Demanda] = @Contagem_Demanda ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SK4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((short[]) buf[26])[0] = rslt.getShort(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((short[]) buf[28])[0] = rslt.getShort(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getVarchar(18) ;
                ((String[]) buf[33])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((int[]) buf[35])[0] = rslt.getInt(20) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                return;
       }
    }

 }

}
