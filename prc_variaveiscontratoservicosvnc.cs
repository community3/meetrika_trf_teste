/*
               File: PRC_VariaveisContratoServicosVnc
        Description: Inicializa Variaveis Contrato Servicos Vnc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:10.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_variaveiscontratoservicosvnc : GXProcedure
   {
      public prc_variaveiscontratoservicosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_variaveiscontratoservicosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoSrvVnc_Codigo ,
                           ref int aP1_SelServico_Codigo ,
                           ref int aP2_SelContrato_Codigo )
      {
         this.A917ContratoSrvVnc_Codigo = aP0_ContratoSrvVnc_Codigo;
         this.AV10SelServico_Codigo = aP1_SelServico_Codigo;
         this.AV8SelContrato_Codigo = aP2_SelContrato_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoSrvVnc_Codigo=this.A917ContratoSrvVnc_Codigo;
         aP1_SelServico_Codigo=this.AV10SelServico_Codigo;
         aP2_SelContrato_Codigo=this.AV8SelContrato_Codigo;
      }

      public int executeUdp( ref int aP0_ContratoSrvVnc_Codigo ,
                             ref int aP1_SelServico_Codigo )
      {
         this.A917ContratoSrvVnc_Codigo = aP0_ContratoSrvVnc_Codigo;
         this.AV10SelServico_Codigo = aP1_SelServico_Codigo;
         this.AV8SelContrato_Codigo = aP2_SelContrato_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoSrvVnc_Codigo=this.A917ContratoSrvVnc_Codigo;
         aP1_SelServico_Codigo=this.AV10SelServico_Codigo;
         aP2_SelContrato_Codigo=this.AV8SelContrato_Codigo;
         return AV8SelContrato_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContratoSrvVnc_Codigo ,
                                 ref int aP1_SelServico_Codigo ,
                                 ref int aP2_SelContrato_Codigo )
      {
         prc_variaveiscontratoservicosvnc objprc_variaveiscontratoservicosvnc;
         objprc_variaveiscontratoservicosvnc = new prc_variaveiscontratoservicosvnc();
         objprc_variaveiscontratoservicosvnc.A917ContratoSrvVnc_Codigo = aP0_ContratoSrvVnc_Codigo;
         objprc_variaveiscontratoservicosvnc.AV10SelServico_Codigo = aP1_SelServico_Codigo;
         objprc_variaveiscontratoservicosvnc.AV8SelContrato_Codigo = aP2_SelContrato_Codigo;
         objprc_variaveiscontratoservicosvnc.context.SetSubmitInitialConfig(context);
         objprc_variaveiscontratoservicosvnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_variaveiscontratoservicosvnc);
         aP0_ContratoSrvVnc_Codigo=this.A917ContratoSrvVnc_Codigo;
         aP1_SelServico_Codigo=this.AV10SelServico_Codigo;
         aP2_SelContrato_Codigo=this.AV8SelContrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_variaveiscontratoservicosvnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CK2 */
         pr_default.execute(0, new Object[] {A917ContratoSrvVnc_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00CK2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00CK2_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00CK2_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00CK2_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00CK2_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00CK2_n1628ContratoSrvVnc_SrvVncCntCod[0];
            AV10SelServico_Codigo = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            AV8SelContrato_Codigo = A1628ContratoSrvVnc_SrvVncCntCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CK2_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00CK2_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00CK2_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00CK2_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00CK2_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_variaveiscontratoservicosvnc__default(),
            new Object[][] {
                new Object[] {
               P00CK2_A917ContratoSrvVnc_Codigo, P00CK2_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00CK2_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00CK2_A1628ContratoSrvVnc_SrvVncCntCod, P00CK2_n1628ContratoSrvVnc_SrvVncCntCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A917ContratoSrvVnc_Codigo ;
      private int AV10SelServico_Codigo ;
      private int AV8SelContrato_Codigo ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private String scmdbuf ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoSrvVnc_Codigo ;
      private int aP1_SelServico_Codigo ;
      private int aP2_SelContrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CK2_A917ContratoSrvVnc_Codigo ;
      private int[] P00CK2_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00CK2_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00CK2_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00CK2_n1628ContratoSrvVnc_SrvVncCntCod ;
   }

   public class prc_variaveiscontratoservicosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CK2 ;
          prmP00CK2 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CK2", "SELECT TOP 1 T1.[ContratoSrvVnc_Codigo], T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod FROM ([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) WHERE T1.[ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo ORDER BY T1.[ContratoSrvVnc_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CK2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
