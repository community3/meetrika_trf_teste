/*
               File: REL_ContagemTAArqPDF
        Description: REL_Contagem TAArq PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:27.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemtaarqpdf : GXProcedure
   {
      public rel_contagemtaarqpdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_contagemtaarqpdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           short aP2_OrderedBy ,
                           bool aP3_OrderedDsc ,
                           String aP4_GridStateXML ,
                           int aP5_ContagemResultado_LoteAceite ,
                           int aP6_ContagemResultado_SS ,
                           String aP7_QrCodePath ,
                           String aP8_QrCodeUrl ,
                           String aP9_Verificador ,
                           String aP10_FileName )
      {
         this.AV103Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV95ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV145OrderedBy = aP2_OrderedBy;
         this.AV146OrderedDsc = aP3_OrderedDsc;
         this.AV132GridStateXML = aP4_GridStateXML;
         this.AV42ContagemResultado_LoteAceite = aP5_ContagemResultado_LoteAceite;
         this.AV205ContagemResultado_SS = aP6_ContagemResultado_SS;
         this.AV163QrCodePath = aP7_QrCodePath;
         this.AV164QrCodeUrl = aP8_QrCodeUrl;
         this.AV174Verificador = aP9_Verificador;
         this.AV128FileName = aP10_FileName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 short aP2_OrderedBy ,
                                 bool aP3_OrderedDsc ,
                                 String aP4_GridStateXML ,
                                 int aP5_ContagemResultado_LoteAceite ,
                                 int aP6_ContagemResultado_SS ,
                                 String aP7_QrCodePath ,
                                 String aP8_QrCodeUrl ,
                                 String aP9_Verificador ,
                                 String aP10_FileName )
      {
         rel_contagemtaarqpdf objrel_contagemtaarqpdf;
         objrel_contagemtaarqpdf = new rel_contagemtaarqpdf();
         objrel_contagemtaarqpdf.AV103Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemtaarqpdf.AV95ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objrel_contagemtaarqpdf.AV145OrderedBy = aP2_OrderedBy;
         objrel_contagemtaarqpdf.AV146OrderedDsc = aP3_OrderedDsc;
         objrel_contagemtaarqpdf.AV132GridStateXML = aP4_GridStateXML;
         objrel_contagemtaarqpdf.AV42ContagemResultado_LoteAceite = aP5_ContagemResultado_LoteAceite;
         objrel_contagemtaarqpdf.AV205ContagemResultado_SS = aP6_ContagemResultado_SS;
         objrel_contagemtaarqpdf.AV163QrCodePath = aP7_QrCodePath;
         objrel_contagemtaarqpdf.AV164QrCodeUrl = aP8_QrCodeUrl;
         objrel_contagemtaarqpdf.AV174Verificador = aP9_Verificador;
         objrel_contagemtaarqpdf.AV128FileName = aP10_FileName;
         objrel_contagemtaarqpdf.context.SetSubmitInitialConfig(context);
         objrel_contagemtaarqpdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemtaarqpdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemtaarqpdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName(AV128FileName) ;
         getPrinter().GxSetDocFormat("PDF") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV178WWPContext) ;
            AV184TextoLink = StringUtil.Trim( AV164QrCodeUrl) + ", informando o c�digo " + StringUtil.Trim( AV174Verificador);
            AV182AssinadoDtHr = context.localUtil.DToC( Gx_date, 2, "/") + " em " + StringUtil.Substring( Gx_time, 1, 5) + ", conforme hor�rio oficial de Brasilia.";
            AV183QrCode_Image = AV163QrCodePath;
            AV211Qrcode_image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV163QrCodePath);
            AV153PaginaImpar = true;
            /* Using cursor P006W4 */
            pr_default.execute(0, new Object[] {AV42ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P006W4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P006W4_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P006W4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P006W4_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P006W4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P006W4_n1604ContagemResultado_CntPrpCod[0];
               A1605ContagemResultado_CntPrpPesCod = P006W4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P006W4_n1605ContagemResultado_CntPrpPesCod[0];
               A597ContagemResultado_LoteAceiteCod = P006W4_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006W4_n597ContagemResultado_LoteAceiteCod[0];
               A1612ContagemResultado_CntNum = P006W4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P006W4_n1612ContagemResultado_CntNum[0];
               A1606ContagemResultado_CntPrpPesNom = P006W4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P006W4_n1606ContagemResultado_CntPrpPesNom[0];
               A1624ContagemResultado_DatVgnInc = P006W4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P006W4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P006W4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P006W4_n39Contratada_Codigo[0];
               A1625ContagemResultado_DatIncTA = P006W4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P006W4_n1625ContagemResultado_DatIncTA[0];
               A456ContagemResultado_Codigo = P006W4_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = P006W4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P006W4_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P006W4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P006W4_n1604ContagemResultado_CntPrpCod[0];
               A1612ContagemResultado_CntNum = P006W4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P006W4_n1612ContagemResultado_CntNum[0];
               A1624ContagemResultado_DatVgnInc = P006W4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P006W4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P006W4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P006W4_n39Contratada_Codigo[0];
               A1605ContagemResultado_CntPrpPesCod = P006W4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P006W4_n1605ContagemResultado_CntPrpPesCod[0];
               A1606ContagemResultado_CntPrpPesNom = P006W4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P006W4_n1606ContagemResultado_CntPrpPesNom[0];
               A1625ContagemResultado_DatIncTA = P006W4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P006W4_n1625ContagemResultado_DatIncTA[0];
               AV109Contrato_Numero = A1612ContagemResultado_CntNum;
               AV181Preposto = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom);
               AV107Contrato_DataInicioTA = A1625ContagemResultado_DatIncTA;
               if ( (DateTime.MinValue==AV107Contrato_DataInicioTA) )
               {
                  AV108Contrato_DataVigenciaInicio = A1624ContagemResultado_DatVgnInc;
               }
               else
               {
                  AV108Contrato_DataVigenciaInicio = AV107Contrato_DataInicioTA;
               }
               AV206Contratada_Codigo = A39Contratada_Codigo;
               /* Execute user subroutine: 'DADOSCONTRATADA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Using cursor P006W5 */
            pr_default.execute(1, new Object[] {AV42ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A596Lote_Codigo = P006W5_A596Lote_Codigo[0];
               A562Lote_Numero = P006W5_A562Lote_Numero[0];
               A844Lote_DataContrato = P006W5_A844Lote_DataContrato[0];
               n844Lote_DataContrato = P006W5_n844Lote_DataContrato[0];
               AV139Length = (short)(StringUtil.Len( A562Lote_Numero)-6);
               AV142Lote_Numero = A562Lote_Numero;
               AV140Lote = StringUtil.Substring( A562Lote_Numero, StringUtil.Len( A562Lote_Numero)-3, 4) + "/" + StringUtil.PadL( StringUtil.Substring( StringUtil.Trim( A562Lote_Numero), 1, AV139Length), 3, "0");
               AV108Contrato_DataVigenciaInicio = A844Lote_DataContrato;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Execute user subroutine: 'PRINTDATALOTE' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H6W0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DADOSCONTRATADA' Routine */
         /* Using cursor P006W6 */
         pr_default.execute(2, new Object[] {AV206Contratada_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A40Contratada_PessoaCod = P006W6_A40Contratada_PessoaCod[0];
            A503Pessoa_MunicipioCod = P006W6_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P006W6_n503Pessoa_MunicipioCod[0];
            A39Contratada_Codigo = P006W6_A39Contratada_Codigo[0];
            n39Contratada_Codigo = P006W6_n39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P006W6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P006W6_n41Contratada_PessoaNom[0];
            A524Contratada_OS = P006W6_A524Contratada_OS[0];
            n524Contratada_OS = P006W6_n524Contratada_OS[0];
            A42Contratada_PessoaCNPJ = P006W6_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P006W6_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P006W6_A518Pessoa_IE[0];
            n518Pessoa_IE = P006W6_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P006W6_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P006W6_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P006W6_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P006W6_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P006W6_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P006W6_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P006W6_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P006W6_n523Pessoa_Fax[0];
            A520Pessoa_UF = P006W6_A520Pessoa_UF[0];
            n520Pessoa_UF = P006W6_n520Pessoa_UF[0];
            A26Municipio_Nome = P006W6_A26Municipio_Nome[0];
            n26Municipio_Nome = P006W6_n26Municipio_Nome[0];
            A503Pessoa_MunicipioCod = P006W6_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P006W6_n503Pessoa_MunicipioCod[0];
            A41Contratada_PessoaNom = P006W6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P006W6_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P006W6_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P006W6_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P006W6_A518Pessoa_IE[0];
            n518Pessoa_IE = P006W6_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P006W6_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P006W6_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P006W6_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P006W6_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P006W6_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P006W6_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P006W6_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P006W6_n523Pessoa_Fax[0];
            A520Pessoa_UF = P006W6_A520Pessoa_UF[0];
            n520Pessoa_UF = P006W6_n520Pessoa_UF[0];
            A26Municipio_Nome = P006W6_A26Municipio_Nome[0];
            n26Municipio_Nome = P006W6_n26Municipio_Nome[0];
            AV106Contratada_PessoaNom = A41Contratada_PessoaNom;
            AV8Contratada_OS = A524Contratada_OS;
            if ( StringUtil.StringSearch( A42Contratada_PessoaCNPJ, ".", 1) == 0 )
            {
               AV105Contratada_PessoaCNPJ = StringUtil.Substring( A42Contratada_PessoaCNPJ, 1, 2) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 3, 3) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 6, 3) + "/" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 9, 4) + "-" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 13, 2);
            }
            else
            {
               AV105Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            }
            if ( StringUtil.StringSearch( A518Pessoa_IE, ".", 1) == 0 )
            {
               AV159Pessoa_IE = StringUtil.Substring( A518Pessoa_IE, 1, 2) + "." + StringUtil.Substring( A518Pessoa_IE, 3, 3) + "." + StringUtil.Substring( A518Pessoa_IE, 6, 3) + "/" + StringUtil.Substring( A518Pessoa_IE, 9, 3) + "-" + StringUtil.Substring( A518Pessoa_IE, 12, 2);
            }
            else
            {
               AV159Pessoa_IE = A518Pessoa_IE;
            }
            AV157Pessoa_Endereco = A519Pessoa_Endereco;
            AV156Pessoa_CEP = A521Pessoa_CEP;
            AV160Pessoa_Telefone = A522Pessoa_Telefone;
            AV158Pessoa_Fax = A523Pessoa_Fax;
            AV161Pessoa_UF = A520Pessoa_UF;
            AV143Municipio_Nome = A26Municipio_Nome;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void S121( )
      {
         /* 'PRINTDATALOTE' Routine */
         AV180x = (short)(AV180x+1);
         /* Execute user subroutine: 'PERIODO' */
         S131 ();
         if (returnInSub) return;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV205ContagemResultado_SS ,
                                              A1452ContagemResultado_SS ,
                                              AV42ContagemResultado_LoteAceite ,
                                              A597ContagemResultado_LoteAceiteCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P006W7 */
         pr_default.execute(3, new Object[] {AV42ContagemResultado_LoteAceite, AV205ContagemResultado_SS});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRK6W7 = false;
            A597ContagemResultado_LoteAceiteCod = P006W7_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006W7_n597ContagemResultado_LoteAceiteCod[0];
            A515ContagemResultado_SistemaCoord = P006W7_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P006W7_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P006W7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006W7_n509ContagemrResultado_SistemaSigla[0];
            A1452ContagemResultado_SS = P006W7_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P006W7_n1452ContagemResultado_SS[0];
            A489ContagemResultado_SistemaCod = P006W7_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P006W7_n489ContagemResultado_SistemaCod[0];
            A512ContagemResultado_ValorPF = P006W7_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P006W7_n512ContagemResultado_ValorPF[0];
            A456ContagemResultado_Codigo = P006W7_A456ContagemResultado_Codigo[0];
            A515ContagemResultado_SistemaCoord = P006W7_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P006W7_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P006W7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006W7_n509ContagemrResultado_SistemaSigla[0];
            OV204SS = AV204SS;
            AV149Os_Header = StringUtil.Substring( AV142Lote_Numero, StringUtil.Len( AV142Lote_Numero)-3, 4) + "/" + "000" + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
            AV204SS = A1452ContagemResultado_SS;
            AV167Sistema_Codigo = A489ContagemResultado_SistemaCod;
            AV169Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
            AV168Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
            while ( (pr_default.getStatus(3) != 101) && ( P006W7_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( StringUtil.StrCmp(P006W7_A515ContagemResultado_SistemaCoord[0], A515ContagemResultado_SistemaCoord) == 0 ) && ( StringUtil.StrCmp(P006W7_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRK6W7 = false;
               A489ContagemResultado_SistemaCod = P006W7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006W7_n489ContagemResultado_SistemaCod[0];
               A456ContagemResultado_Codigo = P006W7_A456ContagemResultado_Codigo[0];
               AV133i = (short)(AV133i+1);
               BRK6W7 = true;
               pr_default.readNext(3);
            }
            /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
            S147 ();
            if ( returnInSub )
            {
               pr_default.close(3);
               returnInSub = true;
               if (true) return;
            }
            AV201ValorIni = (decimal)(AV199PFInicial*A512ContagemResultado_ValorPF);
            AV173Valor = (decimal)(AV162PFFinal*A512ContagemResultado_ValorPF);
            AV189GlsPF = (decimal)(AV190GlsValor/ (decimal)(A512ContagemResultado_ValorPF));
            AV191ValorTotal = (decimal)(AV173Valor+AV201ValorIni-AV190GlsValor);
            if ( ( AV173Valor == Convert.ToDecimal( 0 )) )
            {
               AV173Valor = NumberUtil.Round( AV162PFFinal*A512ContagemResultado_ValorPF, 2);
               AV196strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
            }
            else if ( AV173Valor < 0.001m )
            {
               AV173Valor = NumberUtil.Round( AV162PFFinal*A512ContagemResultado_ValorPF, 4);
               AV196strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 4), " ", "");
            }
            else if ( AV173Valor < 0.01m )
            {
               AV173Valor = NumberUtil.Round( AV162PFFinal*A512ContagemResultado_ValorPF, 3);
               AV196strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 3), " ", "");
            }
            else
            {
               AV173Valor = NumberUtil.Round( AV162PFFinal*A512ContagemResultado_ValorPF, 2);
               AV196strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
            }
            if ( ( AV201ValorIni == Convert.ToDecimal( 0 )) )
            {
               AV201ValorIni = NumberUtil.Round( AV199PFInicial*A512ContagemResultado_ValorPF, 2);
               AV202strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV201ValorIni, 14, 2), " ", "");
            }
            else if ( AV201ValorIni < 0.001m )
            {
               AV201ValorIni = NumberUtil.Round( AV199PFInicial*A512ContagemResultado_ValorPF, 4);
               AV202strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV201ValorIni, 14, 2), " ", "");
            }
            else if ( AV201ValorIni < 0.01m )
            {
               AV201ValorIni = NumberUtil.Round( AV199PFInicial*A512ContagemResultado_ValorPF, 3);
               AV202strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV201ValorIni, 14, 3), " ", "");
            }
            else
            {
               AV201ValorIni = NumberUtil.Round( AV199PFInicial*A512ContagemResultado_ValorPF, 2);
               AV202strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV201ValorIni, 14, 2), " ", "");
            }
            if ( AV191ValorTotal < 0.001m )
            {
               AV197strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV191ValorTotal, 14, 4), " ", "");
            }
            else if ( AV191ValorTotal < 0.01m )
            {
               AV197strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV191ValorTotal, 14, 3), " ", "");
            }
            else
            {
               AV197strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV191ValorTotal, 14, 2), " ", "");
            }
            if ( AV199PFInicial + AV162PFFinal < 0.001m )
            {
               AV203strPFTotal = StringUtil.Str( AV199PFInicial+AV162PFFinal, 14, 4);
            }
            else
            {
               AV203strPFTotal = StringUtil.Str( AV199PFInicial+AV162PFFinal, 14, 3);
            }
            H6W0( false, 390) ;
            getPrinter().GxDrawLine(17, Gx_line+183, 781, Gx_line+183, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+12, 781, Gx_line+12, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+16, 359, Gx_line+35, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV169Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV113DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV112DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV160Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV161Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV143Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV159Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV175Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV134Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV135Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV136Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF", 618, Gx_line+362, 635, Gx_line+381, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203strPFTotal, "")), 496, Gx_line+361, 614, Gx_line+381, 2+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+390);
            H6W0( false, 81) ;
            getPrinter().GxDrawLine(18, Gx_line+80, 781, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+11, 781, Gx_line+11, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+11, 18, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(781, Gx_line+11, 781, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(173, Gx_line+11, 173, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(685, Gx_line+11, 685, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+36, 781, Gx_line+36, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Rela��o de Demandas Mensuradas", 285, Gx_line+14, 588, Gx_line+33, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("Demanda Final", 60, Gx_line+38, 134, Gx_line+73, 1+16, 0, 0, 1) ;
            getPrinter().GxDrawText("Descri��o", 179, Gx_line+49, 362, Gx_line+68, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Quant. de PF", 716, Gx_line+38, 766, Gx_line+75, 1+16, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+81);
            /* Execute user subroutine: 'DEMANDASDOSISTEMALOTE' */
            S157 ();
            if ( returnInSub )
            {
               pr_default.close(3);
               returnInSub = true;
               if (true) return;
            }
            H6W0( false, 21) ;
            getPrinter().GxDrawLine(173, Gx_line+20, 781, Gx_line+20, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(173, Gx_line+0, 173, Gx_line+20, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(781, Gx_line+0, 781, Gx_line+20, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Total de PF", 188, Gx_line+1, 259, Gx_line+20, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203strPFTotal, "")), 691, Gx_line+0, 777, Gx_line+20, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            H6W0( false, 116) ;
            getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 17, Gx_line+17, 117, Gx_line+100) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV182AssinadoDtHr, "")), 133, Gx_line+44, 447, Gx_line+59, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("A autenticidade deste documento pode ser conferida no site", 133, Gx_line+60, 430, Gx_line+74, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawBitMap(AV183QrCode_Image, 683, Gx_line+17, 785, Gx_line+107) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV181Preposto, "@!")), 383, Gx_line+27, 1009, Gx_line+42, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV184TextoLink, "")), 133, Gx_line+77, 708, Gx_line+92, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Documento assinado eletronicamente por", 133, Gx_line+27, 383, Gx_line+43, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+116);
            H6W0( false, 265) ;
            getPrinter().GxDrawLine(16, Gx_line+0, 780, Gx_line+0, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+210, 782, Gx_line+210, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+43, 783, Gx_line+43, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+82, 782, Gx_line+82, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+125, 782, Gx_line+125, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+168, 782, Gx_line+168, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(782, Gx_line+44, 782, Gx_line+246, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(16, Gx_line+44, 16, Gx_line+210, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(501, Gx_line+245, 782, Gx_line+245, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(192, Gx_line+45, 192, Gx_line+210, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(649, Gx_line+44, 649, Gx_line+246, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(500, Gx_line+44, 500, Gx_line+246, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(642, Gx_line+45, 642, Gx_line+246, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("3. Custo da Ordem de Servi�o", 16, Gx_line+6, 266, Gx_line+25, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV190GlsValor, "R$ Z,ZZZ,ZZ9.99")), 657, Gx_line+181, 767, Gx_line+198, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV189GlsPF, "ZZ,ZZZ,ZZ9.999")), 282, Gx_line+181, 385, Gx_line+198, 1+256, 0, 0, 1) ;
            getPrinter().GxDrawText("Glosas", 78, Gx_line+181, 120, Gx_line+199, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196strValor, "")), 671, Gx_line+140, 767, Gx_line+157, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Contagem de PF Final", 28, Gx_line+131, 170, Gx_line+149, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+149, 128, Gx_line+166, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("100%", 566, Gx_line+140, 605, Gx_line+158, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("50%", 566, Gx_line+95, 598, Gx_line+113, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("(Parcela 1)", 70, Gx_line+104, 137, Gx_line+122, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Contagem de PF Inicial", 28, Gx_line+88, 178, Gx_line+106, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os executados", 16, Gx_line+23, 361, Gx_line+41, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV195strPFFinal, "")), 282, Gx_line+140, 385, Gx_line+157, 1+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV200strPFInicial, "")), 282, Gx_line+95, 385, Gx_line+112, 1+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV202strValorIni, "")), 671, Gx_line+95, 767, Gx_line+112, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("(*)", 381, Gx_line+95, 406, Gx_line+113, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 16, Gx_line+233, 324, Gx_line+251, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("VALOR", 690, Gx_line+53, 748, Gx_line+71, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("% FASE", 550, Gx_line+53, 617, Gx_line+71, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+53, 475, Gx_line+71, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("SERVI�O", 58, Gx_line+53, 125, Gx_line+71, 1, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV197strValorTotal, "")), 650, Gx_line+217, 758, Gx_line+237, 2, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("TOTAL", 550, Gx_line+217, 595, Gx_line+237, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+265);
            if ( ( AV190GlsValor > Convert.ToDecimal( 0 )) )
            {
               AV111d = (decimal)(1);
               /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
               S167 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  returnInSub = true;
                  if (true) return;
               }
               H6W0( false, 15) ;
               getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Glosas:", 17, Gx_line+0, 100, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Descricao, "")), 125, Gx_line+0, 751, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
               while ( ( AV111d <= Convert.ToDecimal( StringUtil.Len( AV187GlsDescricao) )) )
               {
                  AV176vrLinha = (short)(AV176vrLinha+1);
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S167 ();
                  if ( returnInSub )
                  {
                     pr_default.close(3);
                     returnInSub = true;
                     if (true) return;
                  }
                  H6W0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Descricao, "")), 125, Gx_line+0, 751, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
               }
            }
            H6W0( false, 435) ;
            getPrinter().GxDrawLine(18, Gx_line+59, 782, Gx_line+59, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+11, 782, Gx_line+11, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(259, Gx_line+216, 542, Gx_line+216, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(259, Gx_line+416, 542, Gx_line+416, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(18, Gx_line+278, 782, Gx_line+278, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("4. Local de Execu��o do Servi�o", 17, Gx_line+17, 325, Gx_line+36, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("5. Aceite Provis�rio", 17, Gx_line+67, 325, Gx_line+86, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("6. Aceite Definitivo", 18, Gx_line+283, 326, Gx_line+302, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Minist�rio da Sa�de", 250, Gx_line+33, 383, Gx_line+51, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 18, Gx_line+317, 193, Gx_line+335, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 18, Gx_line+100, 193, Gx_line+118, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+33, 242, Gx_line+51, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Gestor de Ordem de Servi�o: ", 18, Gx_line+83, 188, Gx_line+101, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Gestor Contratual", 343, Gx_line+216, 454, Gx_line+234, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Gestor Contratual", 343, Gx_line+416, 454, Gx_line+434, 1+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Gestor de Ordem de Servi�o: ", 18, Gx_line+300, 188, Gx_line+318, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 268, Gx_line+116, 560, Gx_line+135, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 268, Gx_line+333, 560, Gx_line+352, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+435);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            if ( ( AV152Pagina > 2 ) && AV153PaginaImpar )
            {
               H6W0( false, 1169) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+1169);
            }
            if ( ! BRK6W7 )
            {
               BRK6W7 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S131( )
      {
         /* 'PERIODO' Routine */
         /* Using cursor P006W10 */
         pr_default.execute(4, new Object[] {AV42ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A40000GXC1 = P006W10_A40000GXC1[0];
            A40001GXC2 = P006W10_A40001GXC2[0];
         }
         else
         {
            A40000GXC1 = DateTime.MinValue;
            A40001GXC2 = DateTime.MinValue;
         }
         pr_default.close(4);
         if ( StringUtil.StrCmp(AV142Lote_Numero, "2012016") == 0 )
         {
            AV113DataInicio = context.localUtil.CToD( "04/01/2016", 2);
            AV112DataFim = context.localUtil.CToD( "08/01/2016", 2);
         }
         else
         {
            AV113DataInicio = A40000GXC1;
            AV112DataFim = A40001GXC2;
         }
      }

      protected void S147( )
      {
         /* 'CONTAGENSDOSISTEMALOTE' Routine */
         AV162PFFinal = 0;
         AV199PFInicial = 0;
         AV190GlsValor = 0;
         AV187GlsDescricao = "";
         /* Using cursor P006W11 */
         pr_default.execute(5, new Object[] {AV42ContagemResultado_LoteAceite, AV167Sistema_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P006W11_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P006W11_n1553ContagemResultado_CntSrvCod[0];
            A597ContagemResultado_LoteAceiteCod = P006W11_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006W11_n597ContagemResultado_LoteAceiteCod[0];
            A489ContagemResultado_SistemaCod = P006W11_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P006W11_n489ContagemResultado_SistemaCod[0];
            A1623ContagemResultado_CntSrvMmn = P006W11_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P006W11_n1623ContagemResultado_CntSrvMmn[0];
            A484ContagemResultado_StatusDmn = P006W11_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006W11_n484ContagemResultado_StatusDmn[0];
            A1855ContagemResultado_PFCnc = P006W11_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P006W11_n1855ContagemResultado_PFCnc[0];
            A1051ContagemResultado_GlsValor = P006W11_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P006W11_n1051ContagemResultado_GlsValor[0];
            A1050ContagemResultado_GlsDescricao = P006W11_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = P006W11_n1050ContagemResultado_GlsDescricao[0];
            A509ContagemrResultado_SistemaSigla = P006W11_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006W11_n509ContagemrResultado_SistemaSigla[0];
            A456ContagemResultado_Codigo = P006W11_A456ContagemResultado_Codigo[0];
            A1623ContagemResultado_CntSrvMmn = P006W11_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P006W11_n1623ContagemResultado_CntSrvMmn[0];
            A509ContagemrResultado_SistemaSigla = P006W11_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006W11_n509ContagemrResultado_SistemaSigla[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( StringUtil.StrCmp(A1623ContagemResultado_CntSrvMmn, "I") == 0 )
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV199PFInicial = (decimal)(AV199PFInicial+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV199PFInicial = (decimal)(AV199PFInicial+A574ContagemResultado_PFFinal);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV162PFFinal = (decimal)(AV162PFFinal+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV162PFFinal = (decimal)(AV162PFFinal+A574ContagemResultado_PFFinal);
               }
            }
            if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
            {
               AV190GlsValor = (decimal)(AV190GlsValor+A1051ContagemResultado_GlsValor);
               AV187GlsDescricao = AV187GlsDescricao + StringUtil.Trim( A1050ContagemResultado_GlsDescricao) + StringUtil.Chr( 13);
            }
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( ( AV162PFFinal > Convert.ToDecimal( 0 )) && ( AV162PFFinal < 0.001m ) )
         {
            AV195strPFFinal = StringUtil.Str( AV162PFFinal, 14, 4);
         }
         else
         {
            AV195strPFFinal = StringUtil.Str( AV162PFFinal, 14, 3);
         }
         if ( ( AV199PFInicial > Convert.ToDecimal( 0 )) && ( AV199PFInicial < 0.001m ) )
         {
            AV200strPFInicial = StringUtil.Str( AV199PFInicial, 14, 4);
         }
         else
         {
            AV200strPFInicial = StringUtil.Str( AV199PFInicial, 14, 3);
         }
      }

      protected void S157( )
      {
         /* 'DEMANDASDOSISTEMALOTE' Routine */
         /* Using cursor P006W12 */
         pr_default.execute(6, new Object[] {AV42ContagemResultado_LoteAceite, AV167Sistema_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P006W12_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006W12_n597ContagemResultado_LoteAceiteCod[0];
            A489ContagemResultado_SistemaCod = P006W12_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P006W12_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = P006W12_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P006W12_n494ContagemResultado_Descricao[0];
            A457ContagemResultado_Demanda = P006W12_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P006W12_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P006W12_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV176vrLinha = 1;
            AV114Demanda = A457ContagemResultado_Demanda;
            AV133i = 1;
            AV188DmnDescricao = A494ContagemResultado_Descricao;
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S1710 ();
            if ( returnInSub )
            {
               pr_default.close(6);
               returnInSub = true;
               if (true) return;
            }
            H6W0( false, 20) ;
            getPrinter().GxDrawLine(781, Gx_line+0, 781, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+0, 17, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(173, Gx_line+0, 173, Gx_line+19, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Descricao, "")), 178, Gx_line+0, 697, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV114Demanda, "@!")), 20, Gx_line+0, 172, Gx_line+17, 1, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            while ( AV133i <= StringUtil.Len( A494ContagemResultado_Descricao) )
            {
               AV176vrLinha = (short)(AV176vrLinha+1);
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S1710 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               H6W0( false, 21) ;
               getPrinter().GxDrawLine(18, Gx_line+0, 18, Gx_line+21, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(173, Gx_line+0, 173, Gx_line+20, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(781, Gx_line+0, 781, Gx_line+20, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV116Descricao, "")), 178, Gx_line+0, 695, Gx_line+16, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+21);
            }
            if ( A574ContagemResultado_PFFinal < 0.001m )
            {
               AV198strPFDmn = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
            }
            else
            {
               AV198strPFDmn = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
            }
            /* Noskip command */
            Gx_line = Gx_OldLine;
            H6W0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV198strPFDmn, "")), 691, Gx_line+0, 777, Gx_line+20, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H6W0( false, 2) ;
            getPrinter().GxDrawLine(18, Gx_line+0, 780, Gx_line+0, 1, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+2);
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void S1710( )
      {
         /* 'LINHADADMNDESCRICAO' Routine */
         AV194DescricaoLength = 74;
         AV192p = AV194DescricaoLength;
         AV116Descricao = StringUtil.Substring( AV188DmnDescricao, AV133i, AV192p);
         if ( ( ( AV133i < StringUtil.Len( AV188DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV192p, 1), 1) == 0 ) ) || ( ( AV133i + AV192p <= StringUtil.Len( AV188DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV192p+1, 1), 1) == 0 ) ) )
         {
            AV193p2 = AV194DescricaoLength;
            while ( AV193p2 >= 1 )
            {
               AV192p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV193p2, 1), 1));
               if ( AV192p > 0 )
               {
                  if ( AV192p == 1 )
                  {
                     AV192p = (short)(AV193p2-1);
                     AV116Descricao = StringUtil.Substring( AV188DmnDescricao, AV133i, AV192p);
                  }
                  else
                  {
                     AV192p = AV193p2;
                     AV116Descricao = StringUtil.Substring( AV188DmnDescricao, AV133i, AV192p);
                  }
                  AV133i = (short)(AV133i+AV193p2);
                  if (true) break;
               }
               else
               {
                  AV192p = AV193p2;
               }
               AV193p2 = (short)(AV193p2+-1);
            }
            if ( AV192p < 2 )
            {
               AV116Descricao = StringUtil.Substring( AV188DmnDescricao, AV133i, AV194DescricaoLength);
               AV133i = (short)(AV133i+AV194DescricaoLength);
            }
         }
         else
         {
            AV133i = (short)(AV133i+AV192p);
         }
      }

      protected void S167( )
      {
         /* 'LINHADAGLSDESCRICAO' Routine */
         AV194DescricaoLength = 120;
         AV192p = AV194DescricaoLength;
         AV116Descricao = StringUtil.Substring( AV187GlsDescricao, (int)(AV111d), AV192p);
         if ( ( ( AV111d < Convert.ToDecimal( StringUtil.Len( AV187GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV192p, 1), 1) == 0 ) ) || ( ( AV111d + AV192p <= Convert.ToDecimal( StringUtil.Len( AV187GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV192p+1, 1), 1) == 0 ) ) )
         {
            AV193p2 = AV194DescricaoLength;
            while ( AV193p2 >= 1 )
            {
               AV192p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV116Descricao, AV193p2, 1), 1));
               if ( AV192p > 0 )
               {
                  if ( AV192p == 1 )
                  {
                     AV192p = (short)(AV193p2-1);
                     AV116Descricao = StringUtil.Substring( AV187GlsDescricao, (int)(AV111d), AV192p);
                  }
                  else
                  {
                     AV192p = AV193p2;
                     AV116Descricao = StringUtil.Substring( AV187GlsDescricao, (int)(AV111d), AV192p);
                  }
                  AV111d = (decimal)(AV111d+AV193p2);
                  if (true) break;
               }
               else
               {
                  AV192p = AV193p2;
               }
               AV193p2 = (short)(AV193p2+-1);
            }
            if ( AV192p < 2 )
            {
               AV116Descricao = StringUtil.Substring( AV187GlsDescricao, (int)(AV111d), AV194DescricaoLength);
               AV111d = (decimal)(AV111d+AV194DescricaoLength);
            }
         }
         else
         {
            AV111d = (decimal)(AV111d+AV192p);
         }
      }

      protected void H6W0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 667, Gx_line+2, 711, Gx_line+29, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV147Os, "")), 41, Gx_line+2, 166, Gx_line+29, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("SS", 17, Gx_line+2, 33, Gx_line+29, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV152Pagina), "ZZZ9")), 716, Gx_line+2, 746, Gx_line+29, 1, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+32);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               AV170SubTitulo = "TERMO DE ACEITE DO PRODUTO OU SERVI�O";
               AV126Emissao = AV112DataFim;
               AV175Volume = "O Valor total de servi�o representado neste documento � de:";
               AV171TituloOS = "N� da SS de Referencia";
               AV134Identificacao1 = "Este Termo de Aceite tem como objetivo a presta��o de servi�os t�cnicos especializados para as";
               AV135Identificacao2 = "atividades de an�lise e mensura��o de software com base na t�cnica de contagem de pontos de fun��o";
               AV136Identificacao3 = "do sistema acima referenciado no �mbito do Minist�rio da Sa�de.";
               AV147Os = AV149Os_Header;
               if ( AV204SS != AV148Os_Anterior )
               {
                  AV152Pagina = 1;
                  AV148Os_Anterior = AV204SS;
                  AV153PaginaImpar = true;
               }
               else
               {
                  AV152Pagina = (short)(AV152Pagina+1);
                  AV153PaginaImpar = (bool)((((AV152Pagina/ (decimal)(2))!=Convert.ToDecimal(NumberUtil.Int( (long)(AV152Pagina/ (decimal)(2)))))));
               }
               if ( AV153PaginaImpar )
               {
                  getPrinter().GxDrawBitMap(context.GetImagePath( "6ded99f5-9d9f-4337-aeda-7b9046b912b6", "", context.GetTheme( )), 349, Gx_line+0, 429, Gx_line+80) ;
                  getPrinter().GxDrawLine(314, Gx_line+201, 314, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+201, 428, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+201, 604, Gx_line+251, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+201, 778, Gx_line+201, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(14, Gx_line+168, 778, Gx_line+251, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+201, 176, Gx_line+249, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+169, 428, Gx_line+202, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+251, 14, Gx_line+286, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+250, 14, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(15, Gx_line+287, 778, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(314, Gx_line+251, 314, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+250, 604, Gx_line+286, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+251, 428, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+251, 176, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(777, Gx_line+250, 777, Gx_line+286, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV171TituloOS, "")), 17, Gx_line+218, 174, Gx_line+235, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATASUS", 555, Gx_line+177, 664, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 139, Gx_line+177, 306, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DO CONTRATO", 616, Gx_line+218, 765, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("N� DO CONTRATO", 443, Gx_line+218, 590, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DE EMISS�O", 179, Gx_line+218, 310, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("LOTE", 322, Gx_line+217, 422, Gx_line+235, 1, 0, 0, 1) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV108Contrato_DataVigenciaInicio, "99/99/99"), 617, Gx_line+259, 766, Gx_line+276, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV109Contrato_Numero, "")), 441, Gx_line+258, 588, Gx_line+275, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV149Os_Header, "")), 17, Gx_line+256, 174, Gx_line+273, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV126Emissao, "99/99/99"), 182, Gx_line+258, 301, Gx_line+275, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 317, Gx_line+83, 462, Gx_line+102, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("SECRETARIA DE GEST�O ESTRAT�GICA E PARTICIPATIVA", 217, Gx_line+100, 562, Gx_line+119, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("DEPARTAMENTO DE INFORM�TICA DO SUS - DATASUS", 217, Gx_line+117, 556, Gx_line+136, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV170SubTitulo, "")), 217, Gx_line+138, 551, Gx_line+158, 1+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140Lote, "")), 323, Gx_line+257, 421, Gx_line+274, 1, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+290);
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
         add_metrics5( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics5( )
      {
         getPrinter().setMetrics("Courier New", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV178WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV184TextoLink = "";
         AV182AssinadoDtHr = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         AV183QrCode_Image = "";
         AV211Qrcode_image_GXI = "";
         scmdbuf = "";
         P006W4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006W4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006W4_A1603ContagemResultado_CntCod = new int[1] ;
         P006W4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P006W4_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P006W4_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P006W4_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P006W4_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P006W4_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006W4_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006W4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P006W4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P006W4_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P006W4_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P006W4_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P006W4_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P006W4_A39Contratada_Codigo = new int[1] ;
         P006W4_n39Contratada_Codigo = new bool[] {false} ;
         P006W4_A1625ContagemResultado_DatIncTA = new DateTime[] {DateTime.MinValue} ;
         P006W4_n1625ContagemResultado_DatIncTA = new bool[] {false} ;
         P006W4_A456ContagemResultado_Codigo = new int[1] ;
         A1612ContagemResultado_CntNum = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A1624ContagemResultado_DatVgnInc = DateTime.MinValue;
         A1625ContagemResultado_DatIncTA = DateTime.MinValue;
         AV109Contrato_Numero = "";
         AV181Preposto = "";
         AV107Contrato_DataInicioTA = DateTime.MinValue;
         AV108Contrato_DataVigenciaInicio = DateTime.MinValue;
         P006W5_A596Lote_Codigo = new int[1] ;
         P006W5_A562Lote_Numero = new String[] {""} ;
         P006W5_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         P006W5_n844Lote_DataContrato = new bool[] {false} ;
         A562Lote_Numero = "";
         A844Lote_DataContrato = DateTime.MinValue;
         AV142Lote_Numero = "";
         AV140Lote = "";
         P006W6_A40Contratada_PessoaCod = new int[1] ;
         P006W6_A503Pessoa_MunicipioCod = new int[1] ;
         P006W6_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P006W6_A39Contratada_Codigo = new int[1] ;
         P006W6_n39Contratada_Codigo = new bool[] {false} ;
         P006W6_A41Contratada_PessoaNom = new String[] {""} ;
         P006W6_n41Contratada_PessoaNom = new bool[] {false} ;
         P006W6_A524Contratada_OS = new int[1] ;
         P006W6_n524Contratada_OS = new bool[] {false} ;
         P006W6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P006W6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P006W6_A518Pessoa_IE = new String[] {""} ;
         P006W6_n518Pessoa_IE = new bool[] {false} ;
         P006W6_A519Pessoa_Endereco = new String[] {""} ;
         P006W6_n519Pessoa_Endereco = new bool[] {false} ;
         P006W6_A521Pessoa_CEP = new String[] {""} ;
         P006W6_n521Pessoa_CEP = new bool[] {false} ;
         P006W6_A522Pessoa_Telefone = new String[] {""} ;
         P006W6_n522Pessoa_Telefone = new bool[] {false} ;
         P006W6_A523Pessoa_Fax = new String[] {""} ;
         P006W6_n523Pessoa_Fax = new bool[] {false} ;
         P006W6_A520Pessoa_UF = new String[] {""} ;
         P006W6_n520Pessoa_UF = new bool[] {false} ;
         P006W6_A26Municipio_Nome = new String[] {""} ;
         P006W6_n26Municipio_Nome = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A520Pessoa_UF = "";
         A26Municipio_Nome = "";
         AV106Contratada_PessoaNom = "";
         AV105Contratada_PessoaCNPJ = "";
         AV159Pessoa_IE = "";
         AV157Pessoa_Endereco = "";
         AV156Pessoa_CEP = "";
         AV160Pessoa_Telefone = "";
         AV158Pessoa_Fax = "";
         AV161Pessoa_UF = "";
         AV143Municipio_Nome = "";
         P006W7_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006W7_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006W7_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P006W7_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P006W7_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006W7_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006W7_A1452ContagemResultado_SS = new int[1] ;
         P006W7_n1452ContagemResultado_SS = new bool[] {false} ;
         P006W7_A489ContagemResultado_SistemaCod = new int[1] ;
         P006W7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006W7_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006W7_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006W7_A456ContagemResultado_Codigo = new int[1] ;
         A515ContagemResultado_SistemaCoord = "";
         A509ContagemrResultado_SistemaSigla = "";
         AV204SS = 0;
         AV149Os_Header = "";
         AV169Sistema_Sigla = "";
         AV168Sistema_Coordenacao = "";
         AV196strValor = "";
         AV202strValorIni = "";
         AV197strValorTotal = "";
         AV203strPFTotal = "";
         AV113DataInicio = DateTime.MinValue;
         AV112DataFim = DateTime.MinValue;
         AV175Volume = "";
         AV134Identificacao1 = "";
         AV135Identificacao2 = "";
         AV136Identificacao3 = "";
         AV183QrCode_Image = "";
         AV195strPFFinal = "";
         AV200strPFInicial = "";
         AV116Descricao = "";
         AV187GlsDescricao = "";
         P006W10_A40000GXC1 = new DateTime[] {DateTime.MinValue} ;
         P006W10_A40001GXC2 = new DateTime[] {DateTime.MinValue} ;
         A40000GXC1 = DateTime.MinValue;
         A40001GXC2 = DateTime.MinValue;
         P006W11_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006W11_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006W11_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006W11_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006W11_A489ContagemResultado_SistemaCod = new int[1] ;
         P006W11_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006W11_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P006W11_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P006W11_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006W11_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006W11_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P006W11_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P006W11_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P006W11_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P006W11_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P006W11_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P006W11_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006W11_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006W11_A456ContagemResultado_Codigo = new int[1] ;
         A1623ContagemResultado_CntSrvMmn = "";
         A484ContagemResultado_StatusDmn = "";
         A1050ContagemResultado_GlsDescricao = "";
         P006W12_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006W12_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006W12_A489ContagemResultado_SistemaCod = new int[1] ;
         P006W12_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006W12_A494ContagemResultado_Descricao = new String[] {""} ;
         P006W12_n494ContagemResultado_Descricao = new bool[] {false} ;
         P006W12_A457ContagemResultado_Demanda = new String[] {""} ;
         P006W12_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006W12_A456ContagemResultado_Codigo = new int[1] ;
         A494ContagemResultado_Descricao = "";
         A457ContagemResultado_Demanda = "";
         AV114Demanda = "";
         AV188DmnDescricao = "";
         AV198strPFDmn = "";
         AV147Os = "";
         AV170SubTitulo = "";
         AV126Emissao = DateTime.MinValue;
         AV171TituloOS = "";
         AV148Os_Anterior = 0;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_contagemtaarqpdf__default(),
            new Object[][] {
                new Object[] {
               P006W4_A1553ContagemResultado_CntSrvCod, P006W4_n1553ContagemResultado_CntSrvCod, P006W4_A1603ContagemResultado_CntCod, P006W4_n1603ContagemResultado_CntCod, P006W4_A1604ContagemResultado_CntPrpCod, P006W4_n1604ContagemResultado_CntPrpCod, P006W4_A1605ContagemResultado_CntPrpPesCod, P006W4_n1605ContagemResultado_CntPrpPesCod, P006W4_A597ContagemResultado_LoteAceiteCod, P006W4_n597ContagemResultado_LoteAceiteCod,
               P006W4_A1612ContagemResultado_CntNum, P006W4_n1612ContagemResultado_CntNum, P006W4_A1606ContagemResultado_CntPrpPesNom, P006W4_n1606ContagemResultado_CntPrpPesNom, P006W4_A1624ContagemResultado_DatVgnInc, P006W4_n1624ContagemResultado_DatVgnInc, P006W4_A39Contratada_Codigo, P006W4_n39Contratada_Codigo, P006W4_A1625ContagemResultado_DatIncTA, P006W4_n1625ContagemResultado_DatIncTA,
               P006W4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006W5_A596Lote_Codigo, P006W5_A562Lote_Numero, P006W5_A844Lote_DataContrato, P006W5_n844Lote_DataContrato
               }
               , new Object[] {
               P006W6_A40Contratada_PessoaCod, P006W6_A503Pessoa_MunicipioCod, P006W6_n503Pessoa_MunicipioCod, P006W6_A39Contratada_Codigo, P006W6_A41Contratada_PessoaNom, P006W6_n41Contratada_PessoaNom, P006W6_A524Contratada_OS, P006W6_n524Contratada_OS, P006W6_A42Contratada_PessoaCNPJ, P006W6_n42Contratada_PessoaCNPJ,
               P006W6_A518Pessoa_IE, P006W6_n518Pessoa_IE, P006W6_A519Pessoa_Endereco, P006W6_n519Pessoa_Endereco, P006W6_A521Pessoa_CEP, P006W6_n521Pessoa_CEP, P006W6_A522Pessoa_Telefone, P006W6_n522Pessoa_Telefone, P006W6_A523Pessoa_Fax, P006W6_n523Pessoa_Fax,
               P006W6_A520Pessoa_UF, P006W6_n520Pessoa_UF, P006W6_A26Municipio_Nome, P006W6_n26Municipio_Nome
               }
               , new Object[] {
               P006W7_A597ContagemResultado_LoteAceiteCod, P006W7_n597ContagemResultado_LoteAceiteCod, P006W7_A515ContagemResultado_SistemaCoord, P006W7_n515ContagemResultado_SistemaCoord, P006W7_A509ContagemrResultado_SistemaSigla, P006W7_n509ContagemrResultado_SistemaSigla, P006W7_A1452ContagemResultado_SS, P006W7_n1452ContagemResultado_SS, P006W7_A489ContagemResultado_SistemaCod, P006W7_n489ContagemResultado_SistemaCod,
               P006W7_A512ContagemResultado_ValorPF, P006W7_n512ContagemResultado_ValorPF, P006W7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006W10_A40000GXC1, P006W10_A40001GXC2
               }
               , new Object[] {
               P006W11_A1553ContagemResultado_CntSrvCod, P006W11_n1553ContagemResultado_CntSrvCod, P006W11_A597ContagemResultado_LoteAceiteCod, P006W11_n597ContagemResultado_LoteAceiteCod, P006W11_A489ContagemResultado_SistemaCod, P006W11_n489ContagemResultado_SistemaCod, P006W11_A1623ContagemResultado_CntSrvMmn, P006W11_n1623ContagemResultado_CntSrvMmn, P006W11_A484ContagemResultado_StatusDmn, P006W11_n484ContagemResultado_StatusDmn,
               P006W11_A1855ContagemResultado_PFCnc, P006W11_n1855ContagemResultado_PFCnc, P006W11_A1051ContagemResultado_GlsValor, P006W11_n1051ContagemResultado_GlsValor, P006W11_A1050ContagemResultado_GlsDescricao, P006W11_n1050ContagemResultado_GlsDescricao, P006W11_A509ContagemrResultado_SistemaSigla, P006W11_n509ContagemrResultado_SistemaSigla, P006W11_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006W12_A597ContagemResultado_LoteAceiteCod, P006W12_n597ContagemResultado_LoteAceiteCod, P006W12_A489ContagemResultado_SistemaCod, P006W12_n489ContagemResultado_SistemaCod, P006W12_A494ContagemResultado_Descricao, P006W12_n494ContagemResultado_Descricao, P006W12_A457ContagemResultado_Demanda, P006W12_n457ContagemResultado_Demanda, P006W12_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV95ContagemResultado_StatusCnt ;
      private short AV145OrderedBy ;
      private short AV139Length ;
      private short AV180x ;
      private short AV133i ;
      private short AV176vrLinha ;
      private short AV152Pagina ;
      private short AV194DescricaoLength ;
      private short AV192p ;
      private short AV193p2 ;
      private int AV103Contratada_AreaTrabalhoCod ;
      private int AV42ContagemResultado_LoteAceite ;
      private int AV205ContagemResultado_SS ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A39Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int AV206Contratada_Codigo ;
      private int A596Lote_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A524Contratada_OS ;
      private int AV8Contratada_OS ;
      private int A1452ContagemResultado_SS ;
      private int A489ContagemResultado_SistemaCod ;
      private int OV204SS ;
      private int AV204SS ;
      private int AV167Sistema_Codigo ;
      private int Gx_OldLine ;
      private int AV148Os_Anterior ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV201ValorIni ;
      private decimal AV199PFInicial ;
      private decimal AV173Valor ;
      private decimal AV162PFFinal ;
      private decimal AV189GlsPF ;
      private decimal AV190GlsValor ;
      private decimal AV191ValorTotal ;
      private decimal AV111d ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String AV174Verificador ;
      private String AV128FileName ;
      private String AV184TextoLink ;
      private String AV182AssinadoDtHr ;
      private String Gx_time ;
      private String scmdbuf ;
      private String A1612ContagemResultado_CntNum ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String AV109Contrato_Numero ;
      private String AV181Preposto ;
      private String A562Lote_Numero ;
      private String AV142Lote_Numero ;
      private String AV140Lote ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A520Pessoa_UF ;
      private String A26Municipio_Nome ;
      private String AV106Contratada_PessoaNom ;
      private String AV159Pessoa_IE ;
      private String AV156Pessoa_CEP ;
      private String AV160Pessoa_Telefone ;
      private String AV158Pessoa_Fax ;
      private String AV161Pessoa_UF ;
      private String AV143Municipio_Nome ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV169Sistema_Sigla ;
      private String AV196strValor ;
      private String AV202strValorIni ;
      private String AV197strValorTotal ;
      private String AV203strPFTotal ;
      private String AV175Volume ;
      private String AV134Identificacao1 ;
      private String AV135Identificacao2 ;
      private String AV136Identificacao3 ;
      private String AV195strPFFinal ;
      private String AV200strPFInicial ;
      private String A1623ContagemResultado_CntSrvMmn ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV198strPFDmn ;
      private String AV171TituloOS ;
      private DateTime Gx_date ;
      private DateTime A1624ContagemResultado_DatVgnInc ;
      private DateTime A1625ContagemResultado_DatIncTA ;
      private DateTime AV107Contrato_DataInicioTA ;
      private DateTime AV108Contrato_DataVigenciaInicio ;
      private DateTime A844Lote_DataContrato ;
      private DateTime AV113DataInicio ;
      private DateTime AV112DataFim ;
      private DateTime A40000GXC1 ;
      private DateTime A40001GXC2 ;
      private DateTime AV126Emissao ;
      private bool AV146OrderedDsc ;
      private bool AV153PaginaImpar ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1624ContagemResultado_DatVgnInc ;
      private bool n39Contratada_Codigo ;
      private bool n1625ContagemResultado_DatIncTA ;
      private bool returnInSub ;
      private bool n844Lote_DataContrato ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n41Contratada_PessoaNom ;
      private bool n524Contratada_OS ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n520Pessoa_UF ;
      private bool n26Municipio_Nome ;
      private bool BRK6W7 ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1452ContagemResultado_SS ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1623ContagemResultado_CntSrvMmn ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n494ContagemResultado_Descricao ;
      private bool n457ContagemResultado_Demanda ;
      private String AV132GridStateXML ;
      private String AV163QrCodePath ;
      private String AV187GlsDescricao ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String AV164QrCodeUrl ;
      private String AV211Qrcode_image_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String AV105Contratada_PessoaCNPJ ;
      private String AV157Pessoa_Endereco ;
      private String A515ContagemResultado_SistemaCoord ;
      private String AV149Os_Header ;
      private String AV168Sistema_Coordenacao ;
      private String AV116Descricao ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String AV114Demanda ;
      private String AV188DmnDescricao ;
      private String AV147Os ;
      private String AV170SubTitulo ;
      private String AV183QrCode_Image ;
      private String Qrcode_image ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P006W4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006W4_n1553ContagemResultado_CntSrvCod ;
      private int[] P006W4_A1603ContagemResultado_CntCod ;
      private bool[] P006W4_n1603ContagemResultado_CntCod ;
      private int[] P006W4_A1604ContagemResultado_CntPrpCod ;
      private bool[] P006W4_n1604ContagemResultado_CntPrpCod ;
      private int[] P006W4_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P006W4_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P006W4_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006W4_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006W4_A1612ContagemResultado_CntNum ;
      private bool[] P006W4_n1612ContagemResultado_CntNum ;
      private String[] P006W4_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P006W4_n1606ContagemResultado_CntPrpPesNom ;
      private DateTime[] P006W4_A1624ContagemResultado_DatVgnInc ;
      private bool[] P006W4_n1624ContagemResultado_DatVgnInc ;
      private int[] P006W4_A39Contratada_Codigo ;
      private bool[] P006W4_n39Contratada_Codigo ;
      private DateTime[] P006W4_A1625ContagemResultado_DatIncTA ;
      private bool[] P006W4_n1625ContagemResultado_DatIncTA ;
      private int[] P006W4_A456ContagemResultado_Codigo ;
      private int[] P006W5_A596Lote_Codigo ;
      private String[] P006W5_A562Lote_Numero ;
      private DateTime[] P006W5_A844Lote_DataContrato ;
      private bool[] P006W5_n844Lote_DataContrato ;
      private int[] P006W6_A40Contratada_PessoaCod ;
      private int[] P006W6_A503Pessoa_MunicipioCod ;
      private bool[] P006W6_n503Pessoa_MunicipioCod ;
      private int[] P006W6_A39Contratada_Codigo ;
      private bool[] P006W6_n39Contratada_Codigo ;
      private String[] P006W6_A41Contratada_PessoaNom ;
      private bool[] P006W6_n41Contratada_PessoaNom ;
      private int[] P006W6_A524Contratada_OS ;
      private bool[] P006W6_n524Contratada_OS ;
      private String[] P006W6_A42Contratada_PessoaCNPJ ;
      private bool[] P006W6_n42Contratada_PessoaCNPJ ;
      private String[] P006W6_A518Pessoa_IE ;
      private bool[] P006W6_n518Pessoa_IE ;
      private String[] P006W6_A519Pessoa_Endereco ;
      private bool[] P006W6_n519Pessoa_Endereco ;
      private String[] P006W6_A521Pessoa_CEP ;
      private bool[] P006W6_n521Pessoa_CEP ;
      private String[] P006W6_A522Pessoa_Telefone ;
      private bool[] P006W6_n522Pessoa_Telefone ;
      private String[] P006W6_A523Pessoa_Fax ;
      private bool[] P006W6_n523Pessoa_Fax ;
      private String[] P006W6_A520Pessoa_UF ;
      private bool[] P006W6_n520Pessoa_UF ;
      private String[] P006W6_A26Municipio_Nome ;
      private bool[] P006W6_n26Municipio_Nome ;
      private int[] P006W7_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006W7_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006W7_A515ContagemResultado_SistemaCoord ;
      private bool[] P006W7_n515ContagemResultado_SistemaCoord ;
      private String[] P006W7_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006W7_n509ContagemrResultado_SistemaSigla ;
      private int[] P006W7_A1452ContagemResultado_SS ;
      private bool[] P006W7_n1452ContagemResultado_SS ;
      private int[] P006W7_A489ContagemResultado_SistemaCod ;
      private bool[] P006W7_n489ContagemResultado_SistemaCod ;
      private decimal[] P006W7_A512ContagemResultado_ValorPF ;
      private bool[] P006W7_n512ContagemResultado_ValorPF ;
      private int[] P006W7_A456ContagemResultado_Codigo ;
      private DateTime[] P006W10_A40000GXC1 ;
      private DateTime[] P006W10_A40001GXC2 ;
      private int[] P006W11_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006W11_n1553ContagemResultado_CntSrvCod ;
      private int[] P006W11_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006W11_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006W11_A489ContagemResultado_SistemaCod ;
      private bool[] P006W11_n489ContagemResultado_SistemaCod ;
      private String[] P006W11_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P006W11_n1623ContagemResultado_CntSrvMmn ;
      private String[] P006W11_A484ContagemResultado_StatusDmn ;
      private bool[] P006W11_n484ContagemResultado_StatusDmn ;
      private decimal[] P006W11_A1855ContagemResultado_PFCnc ;
      private bool[] P006W11_n1855ContagemResultado_PFCnc ;
      private decimal[] P006W11_A1051ContagemResultado_GlsValor ;
      private bool[] P006W11_n1051ContagemResultado_GlsValor ;
      private String[] P006W11_A1050ContagemResultado_GlsDescricao ;
      private bool[] P006W11_n1050ContagemResultado_GlsDescricao ;
      private String[] P006W11_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006W11_n509ContagemrResultado_SistemaSigla ;
      private int[] P006W11_A456ContagemResultado_Codigo ;
      private int[] P006W12_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006W12_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006W12_A489ContagemResultado_SistemaCod ;
      private bool[] P006W12_n489ContagemResultado_SistemaCod ;
      private String[] P006W12_A494ContagemResultado_Descricao ;
      private bool[] P006W12_n494ContagemResultado_Descricao ;
      private String[] P006W12_A457ContagemResultado_Demanda ;
      private bool[] P006W12_n457ContagemResultado_Demanda ;
      private int[] P006W12_A456ContagemResultado_Codigo ;
      private wwpbaseobjects.SdtWWPContext AV178WWPContext ;
   }

   public class rel_contagemtaarqpdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P006W7( IGxContext context ,
                                             int AV205ContagemResultado_SS ,
                                             int A1452ContagemResultado_SS ,
                                             int AV42ContagemResultado_LoteAceite ,
                                             int A597ContagemResultado_LoteAceiteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_SS], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV42ContagemResultado_LoteAceite)";
         if ( ! (0==AV205ContagemResultado_SS) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SS] = @AV205ContagemResultado_SS)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Coordenacao], T2.[Sistema_Sigla]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_P006W7(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006W4 ;
          prmP006W4 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W5 ;
          prmP006W5 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W6 ;
          prmP006W6 = new Object[] {
          new Object[] {"@AV206Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W10 ;
          prmP006W10 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W11 ;
          prmP006W11 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W12 ;
          prmP006W12 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006W7 ;
          prmP006W7 = new Object[] {
          new Object[] {"@AV42ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV205ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006W4", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_LoteAceiteCod], T3.[Contrato_Numero] AS ContagemResultado_CntNum, T5.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T3.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T3.[Contratada_Codigo], COALESCE( T6.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DatIncTA, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]),  (SELECT T7.[ContratoTermoAditivo_DataInicio], T7.[ContratoTermoAditivo_Codigo], T8.[GXC6] AS GXC6 FROM [ContratoTermoAditivo] T7 WITH (NOLOCK),  (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6 FROM [ContratoTermoAditivo] WITH (NOLOCK) ) T8 WHERE T7.[ContratoTermoAditivo_Codigo] = T8.[GXC6] ) T6 WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV42ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W4,1,0,true,true )
             ,new CursorDef("P006W5", "SELECT [Lote_Codigo], [Lote_Numero], [Lote_DataContrato] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV42ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W5,1,0,false,true )
             ,new CursorDef("P006W6", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_OS], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_IE], T2.[Pessoa_Endereco], T2.[Pessoa_CEP], T2.[Pessoa_Telefone], T2.[Pessoa_Fax], T3.[Estado_UF] AS Pessoa_UF, T3.[Municipio_Nome] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV206Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W6,1,0,false,true )
             ,new CursorDef("P006W7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W7,100,0,true,false )
             ,new CursorDef("P006W10", "SELECT COALESCE( T1.[GXC1], convert( DATETIME, '17530101', 112 )) AS GXC1, COALESCE( T1.[GXC2], convert( DATETIME, '17530101', 112 )) AS GXC2 FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC1, MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC2 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV42ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W10,1,0,true,true )
             ,new CursorDef("P006W11", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_PFCnc], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV42ContagemResultado_LoteAceite) AND (T1.[ContagemResultado_SistemaCod] = @AV167Sistema_Codigo) ORDER BY T1.[ContagemResultado_LoteAceiteCod], T3.[Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W11,100,0,true,false )
             ,new CursorDef("P006W12", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_Descricao], [ContagemResultado_Demanda], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV42ContagemResultado_LoteAceite and [ContagemResultado_SistemaCod] = @AV167Sistema_Codigo ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_SistemaCod], [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006W12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
