/*
               File: GpoObjCtrlResponsavel_BC
        Description: Gpo Obj Ctrl Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:23.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gpoobjctrlresponsavel_bc : GXHttpHandler, IGxSilentTrn
   {
      public gpoobjctrlresponsavel_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gpoobjctrlresponsavel_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4L202( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4L202( ) ;
         standaloneModal( ) ;
         AddRow4L202( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4L0( )
      {
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4L202( ) ;
            }
            else
            {
               CheckExtendedTable4L202( ) ;
               if ( AnyError == 0 )
               {
                  ZM4L202( 2) ;
                  ZM4L202( 3) ;
               }
               CloseExtendedTableCursors4L202( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4L202( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1838GpoObjCtrlResponsavel_GpoCod = A1838GpoObjCtrlResponsavel_GpoCod;
            Z1834GpoObjCtrlResponsavel_CteCteCod = A1834GpoObjCtrlResponsavel_CteCteCod;
            Z1835GpoObjCtrlResponsavel_CteUsrCod = A1835GpoObjCtrlResponsavel_CteUsrCod;
            Z1836GpoObjCtrlResponsavel_CteAreaCod = A1836GpoObjCtrlResponsavel_CteAreaCod;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1836GpoObjCtrlResponsavel_CteAreaCod = A1836GpoObjCtrlResponsavel_CteAreaCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1836GpoObjCtrlResponsavel_CteAreaCod = A1836GpoObjCtrlResponsavel_CteAreaCod;
         }
         if ( GX_JID == -1 )
         {
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
            Z1838GpoObjCtrlResponsavel_GpoCod = A1838GpoObjCtrlResponsavel_GpoCod;
            Z1834GpoObjCtrlResponsavel_CteCteCod = A1834GpoObjCtrlResponsavel_CteCteCod;
            Z1835GpoObjCtrlResponsavel_CteUsrCod = A1835GpoObjCtrlResponsavel_CteUsrCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4L202( )
      {
         /* Using cursor BC004L7 */
         pr_default.execute(4, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound202 = 1;
            A1838GpoObjCtrlResponsavel_GpoCod = BC004L7_A1838GpoObjCtrlResponsavel_GpoCod[0];
            n1838GpoObjCtrlResponsavel_GpoCod = BC004L7_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = BC004L7_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            n1834GpoObjCtrlResponsavel_CteCteCod = BC004L7_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = BC004L7_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            n1835GpoObjCtrlResponsavel_CteUsrCod = BC004L7_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
            ZM4L202( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4L202( ) ;
      }

      protected void OnLoadActions4L202( )
      {
         /* Using cursor BC004L5 */
         pr_default.execute(2, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = BC004L5_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            n1836GpoObjCtrlResponsavel_CteAreaCod = BC004L5_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable4L202( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004L5 */
         pr_default.execute(2, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = BC004L5_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            n1836GpoObjCtrlResponsavel_CteAreaCod = BC004L5_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
         }
         else
         {
            A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
            n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         }
         pr_default.close(2);
         /* Using cursor BC004L6 */
         pr_default.execute(3, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1834GpoObjCtrlResponsavel_CteCteCod) || (0==A1835GpoObjCtrlResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Gpo Obj Ctrl Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "GPOOBJCTRLRESPONSAVEL_CTECTECOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4L202( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4L202( )
      {
         /* Using cursor BC004L8 */
         pr_default.execute(5, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound202 = 1;
         }
         else
         {
            RcdFound202 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004L3 */
         pr_default.execute(1, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4L202( 1) ;
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = BC004L3_A1837GpoObjCtrlResponsavel_Codigo[0];
            A1838GpoObjCtrlResponsavel_GpoCod = BC004L3_A1838GpoObjCtrlResponsavel_GpoCod[0];
            n1838GpoObjCtrlResponsavel_GpoCod = BC004L3_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = BC004L3_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            n1834GpoObjCtrlResponsavel_CteCteCod = BC004L3_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = BC004L3_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            n1835GpoObjCtrlResponsavel_CteUsrCod = BC004L3_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
            sMode202 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4L202( ) ;
            if ( AnyError == 1 )
            {
               RcdFound202 = 0;
               InitializeNonKey4L202( ) ;
            }
            Gx_mode = sMode202;
         }
         else
         {
            RcdFound202 = 0;
            InitializeNonKey4L202( ) ;
            sMode202 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode202;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4L0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4L202( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004L2 */
            pr_default.execute(0, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1838GpoObjCtrlResponsavel_GpoCod != BC004L2_A1838GpoObjCtrlResponsavel_GpoCod[0] ) || ( Z1834GpoObjCtrlResponsavel_CteCteCod != BC004L2_A1834GpoObjCtrlResponsavel_CteCteCod[0] ) || ( Z1835GpoObjCtrlResponsavel_CteUsrCod != BC004L2_A1835GpoObjCtrlResponsavel_CteUsrCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4L202( )
      {
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4L202( 0) ;
            CheckOptimisticConcurrency4L202( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4L202( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4L202( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004L9 */
                     pr_default.execute(6, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
                     A1837GpoObjCtrlResponsavel_Codigo = BC004L9_A1837GpoObjCtrlResponsavel_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4L202( ) ;
            }
            EndLevel4L202( ) ;
         }
         CloseExtendedTableCursors4L202( ) ;
      }

      protected void Update4L202( )
      {
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4L202( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4L202( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4L202( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004L10 */
                     pr_default.execute(7, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod, A1837GpoObjCtrlResponsavel_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GpoObjCtrlResponsavel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4L202( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4L202( ) ;
         }
         CloseExtendedTableCursors4L202( ) ;
      }

      protected void DeferredUpdate4L202( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4L202( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4L202( ) ;
            AfterConfirm4L202( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4L202( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004L11 */
                  pr_default.execute(8, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode202 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4L202( ) ;
         Gx_mode = sMode202;
      }

      protected void OnDeleteControls4L202( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004L13 */
            pr_default.execute(9, new Object[] {n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod});
            if ( (pr_default.getStatus(9) != 101) )
            {
               A1836GpoObjCtrlResponsavel_CteAreaCod = BC004L13_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
               n1836GpoObjCtrlResponsavel_CteAreaCod = BC004L13_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
            }
            else
            {
               A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
               n1836GpoObjCtrlResponsavel_CteAreaCod = false;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel4L202( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4L202( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4L202( )
      {
         /* Using cursor BC004L14 */
         pr_default.execute(10, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
         RcdFound202 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = BC004L14_A1837GpoObjCtrlResponsavel_Codigo[0];
            A1838GpoObjCtrlResponsavel_GpoCod = BC004L14_A1838GpoObjCtrlResponsavel_GpoCod[0];
            n1838GpoObjCtrlResponsavel_GpoCod = BC004L14_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = BC004L14_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            n1834GpoObjCtrlResponsavel_CteCteCod = BC004L14_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = BC004L14_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            n1835GpoObjCtrlResponsavel_CteUsrCod = BC004L14_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4L202( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound202 = 0;
         ScanKeyLoad4L202( ) ;
      }

      protected void ScanKeyLoad4L202( )
      {
         sMode202 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound202 = 1;
            A1837GpoObjCtrlResponsavel_Codigo = BC004L14_A1837GpoObjCtrlResponsavel_Codigo[0];
            A1838GpoObjCtrlResponsavel_GpoCod = BC004L14_A1838GpoObjCtrlResponsavel_GpoCod[0];
            n1838GpoObjCtrlResponsavel_GpoCod = BC004L14_n1838GpoObjCtrlResponsavel_GpoCod[0];
            A1834GpoObjCtrlResponsavel_CteCteCod = BC004L14_A1834GpoObjCtrlResponsavel_CteCteCod[0];
            n1834GpoObjCtrlResponsavel_CteCteCod = BC004L14_n1834GpoObjCtrlResponsavel_CteCteCod[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = BC004L14_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            n1835GpoObjCtrlResponsavel_CteUsrCod = BC004L14_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
         }
         Gx_mode = sMode202;
      }

      protected void ScanKeyEnd4L202( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4L202( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4L202( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4L202( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4L202( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4L202( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4L202( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4L202( )
      {
      }

      protected void AddRow4L202( )
      {
         VarsToRow202( bcGpoObjCtrlResponsavel) ;
      }

      protected void ReadRow4L202( )
      {
         RowToVars202( bcGpoObjCtrlResponsavel, 1) ;
      }

      protected void InitializeNonKey4L202( )
      {
         A1838GpoObjCtrlResponsavel_GpoCod = 0;
         n1838GpoObjCtrlResponsavel_GpoCod = false;
         A1834GpoObjCtrlResponsavel_CteCteCod = 0;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         A1835GpoObjCtrlResponsavel_CteUsrCod = 0;
         n1835GpoObjCtrlResponsavel_CteUsrCod = false;
         A1836GpoObjCtrlResponsavel_CteAreaCod = 0;
         n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         Z1838GpoObjCtrlResponsavel_GpoCod = 0;
         Z1834GpoObjCtrlResponsavel_CteCteCod = 0;
         Z1835GpoObjCtrlResponsavel_CteUsrCod = 0;
      }

      protected void InitAll4L202( )
      {
         A1837GpoObjCtrlResponsavel_Codigo = 0;
         InitializeNonKey4L202( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow202( SdtGpoObjCtrlResponsavel obj202 )
      {
         obj202.gxTpr_Mode = Gx_mode;
         obj202.gxTpr_Gpoobjctrlresponsavel_gpocod = A1838GpoObjCtrlResponsavel_GpoCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod = A1834GpoObjCtrlResponsavel_CteCteCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod = A1835GpoObjCtrlResponsavel_CteUsrCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod = A1836GpoObjCtrlResponsavel_CteAreaCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_codigo = A1837GpoObjCtrlResponsavel_Codigo;
         obj202.gxTpr_Gpoobjctrlresponsavel_codigo_Z = Z1837GpoObjCtrlResponsavel_Codigo;
         obj202.gxTpr_Gpoobjctrlresponsavel_gpocod_Z = Z1838GpoObjCtrlResponsavel_GpoCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod_Z = Z1834GpoObjCtrlResponsavel_CteCteCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z = Z1835GpoObjCtrlResponsavel_CteUsrCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod_Z = Z1836GpoObjCtrlResponsavel_CteAreaCod;
         obj202.gxTpr_Gpoobjctrlresponsavel_gpocod_N = (short)(Convert.ToInt16(n1838GpoObjCtrlResponsavel_GpoCod));
         obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod_N = (short)(Convert.ToInt16(n1834GpoObjCtrlResponsavel_CteCteCod));
         obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod_N = (short)(Convert.ToInt16(n1835GpoObjCtrlResponsavel_CteUsrCod));
         obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod_N = (short)(Convert.ToInt16(n1836GpoObjCtrlResponsavel_CteAreaCod));
         obj202.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow202( SdtGpoObjCtrlResponsavel obj202 )
      {
         obj202.gxTpr_Gpoobjctrlresponsavel_codigo = A1837GpoObjCtrlResponsavel_Codigo;
         return  ;
      }

      public void RowToVars202( SdtGpoObjCtrlResponsavel obj202 ,
                                int forceLoad )
      {
         Gx_mode = obj202.gxTpr_Mode;
         A1838GpoObjCtrlResponsavel_GpoCod = obj202.gxTpr_Gpoobjctrlresponsavel_gpocod;
         n1838GpoObjCtrlResponsavel_GpoCod = false;
         A1834GpoObjCtrlResponsavel_CteCteCod = obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod;
         n1834GpoObjCtrlResponsavel_CteCteCod = false;
         A1835GpoObjCtrlResponsavel_CteUsrCod = obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod;
         n1835GpoObjCtrlResponsavel_CteUsrCod = false;
         A1836GpoObjCtrlResponsavel_CteAreaCod = obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod;
         n1836GpoObjCtrlResponsavel_CteAreaCod = false;
         A1837GpoObjCtrlResponsavel_Codigo = obj202.gxTpr_Gpoobjctrlresponsavel_codigo;
         Z1837GpoObjCtrlResponsavel_Codigo = obj202.gxTpr_Gpoobjctrlresponsavel_codigo_Z;
         Z1838GpoObjCtrlResponsavel_GpoCod = obj202.gxTpr_Gpoobjctrlresponsavel_gpocod_Z;
         Z1834GpoObjCtrlResponsavel_CteCteCod = obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod_Z;
         Z1835GpoObjCtrlResponsavel_CteUsrCod = obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z;
         Z1836GpoObjCtrlResponsavel_CteAreaCod = obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod_Z;
         n1838GpoObjCtrlResponsavel_GpoCod = (bool)(Convert.ToBoolean(obj202.gxTpr_Gpoobjctrlresponsavel_gpocod_N));
         n1834GpoObjCtrlResponsavel_CteCteCod = (bool)(Convert.ToBoolean(obj202.gxTpr_Gpoobjctrlresponsavel_ctectecod_N));
         n1835GpoObjCtrlResponsavel_CteUsrCod = (bool)(Convert.ToBoolean(obj202.gxTpr_Gpoobjctrlresponsavel_cteusrcod_N));
         n1836GpoObjCtrlResponsavel_CteAreaCod = (bool)(Convert.ToBoolean(obj202.gxTpr_Gpoobjctrlresponsavel_cteareacod_N));
         Gx_mode = obj202.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1837GpoObjCtrlResponsavel_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4L202( ) ;
         ScanKeyStart4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
         }
         ZM4L202( -1) ;
         OnLoadActions4L202( ) ;
         AddRow4L202( ) ;
         ScanKeyEnd4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars202( bcGpoObjCtrlResponsavel, 0) ;
         ScanKeyStart4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1837GpoObjCtrlResponsavel_Codigo = A1837GpoObjCtrlResponsavel_Codigo;
         }
         ZM4L202( -1) ;
         OnLoadActions4L202( ) ;
         AddRow4L202( ) ;
         ScanKeyEnd4L202( ) ;
         if ( RcdFound202 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars202( bcGpoObjCtrlResponsavel, 0) ;
         nKeyPressed = 1;
         GetKey4L202( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4L202( ) ;
         }
         else
         {
            if ( RcdFound202 == 1 )
            {
               if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
               {
                  A1837GpoObjCtrlResponsavel_Codigo = Z1837GpoObjCtrlResponsavel_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4L202( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4L202( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4L202( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow202( bcGpoObjCtrlResponsavel) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars202( bcGpoObjCtrlResponsavel, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4L202( ) ;
         if ( RcdFound202 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
            {
               A1837GpoObjCtrlResponsavel_Codigo = Z1837GpoObjCtrlResponsavel_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1837GpoObjCtrlResponsavel_Codigo != Z1837GpoObjCtrlResponsavel_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "GpoObjCtrlResponsavel_BC");
         VarsToRow202( bcGpoObjCtrlResponsavel) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcGpoObjCtrlResponsavel.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcGpoObjCtrlResponsavel.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcGpoObjCtrlResponsavel )
         {
            bcGpoObjCtrlResponsavel = (SdtGpoObjCtrlResponsavel)(sdt);
            if ( StringUtil.StrCmp(bcGpoObjCtrlResponsavel.gxTpr_Mode, "") == 0 )
            {
               bcGpoObjCtrlResponsavel.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow202( bcGpoObjCtrlResponsavel) ;
            }
            else
            {
               RowToVars202( bcGpoObjCtrlResponsavel, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcGpoObjCtrlResponsavel.gxTpr_Mode, "") == 0 )
            {
               bcGpoObjCtrlResponsavel.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars202( bcGpoObjCtrlResponsavel, 1) ;
         return  ;
      }

      public SdtGpoObjCtrlResponsavel GpoObjCtrlResponsavel_BC
      {
         get {
            return bcGpoObjCtrlResponsavel ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004L7_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L7_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         BC004L7_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         BC004L7_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         BC004L7_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         BC004L7_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         BC004L7_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         BC004L5_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         BC004L5_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         BC004L6_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         BC004L6_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         BC004L8_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L3_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L3_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         BC004L3_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         BC004L3_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         BC004L3_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         BC004L3_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         BC004L3_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         sMode202 = "";
         BC004L2_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L2_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         BC004L2_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         BC004L2_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         BC004L2_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         BC004L2_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         BC004L2_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         BC004L9_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L13_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         BC004L13_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         BC004L14_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC004L14_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         BC004L14_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         BC004L14_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         BC004L14_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         BC004L14_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         BC004L14_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gpoobjctrlresponsavel_bc__default(),
            new Object[][] {
                new Object[] {
               BC004L2_A1837GpoObjCtrlResponsavel_Codigo, BC004L2_A1838GpoObjCtrlResponsavel_GpoCod, BC004L2_n1838GpoObjCtrlResponsavel_GpoCod, BC004L2_A1834GpoObjCtrlResponsavel_CteCteCod, BC004L2_n1834GpoObjCtrlResponsavel_CteCteCod, BC004L2_A1835GpoObjCtrlResponsavel_CteUsrCod, BC004L2_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               BC004L3_A1837GpoObjCtrlResponsavel_Codigo, BC004L3_A1838GpoObjCtrlResponsavel_GpoCod, BC004L3_n1838GpoObjCtrlResponsavel_GpoCod, BC004L3_A1834GpoObjCtrlResponsavel_CteCteCod, BC004L3_n1834GpoObjCtrlResponsavel_CteCteCod, BC004L3_A1835GpoObjCtrlResponsavel_CteUsrCod, BC004L3_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               BC004L5_A1836GpoObjCtrlResponsavel_CteAreaCod, BC004L5_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
               , new Object[] {
               BC004L6_A1834GpoObjCtrlResponsavel_CteCteCod
               }
               , new Object[] {
               BC004L7_A1837GpoObjCtrlResponsavel_Codigo, BC004L7_A1838GpoObjCtrlResponsavel_GpoCod, BC004L7_n1838GpoObjCtrlResponsavel_GpoCod, BC004L7_A1834GpoObjCtrlResponsavel_CteCteCod, BC004L7_n1834GpoObjCtrlResponsavel_CteCteCod, BC004L7_A1835GpoObjCtrlResponsavel_CteUsrCod, BC004L7_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
               , new Object[] {
               BC004L8_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               BC004L9_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004L13_A1836GpoObjCtrlResponsavel_CteAreaCod, BC004L13_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
               , new Object[] {
               BC004L14_A1837GpoObjCtrlResponsavel_Codigo, BC004L14_A1838GpoObjCtrlResponsavel_GpoCod, BC004L14_n1838GpoObjCtrlResponsavel_GpoCod, BC004L14_A1834GpoObjCtrlResponsavel_CteCteCod, BC004L14_n1834GpoObjCtrlResponsavel_CteCteCod, BC004L14_A1835GpoObjCtrlResponsavel_CteUsrCod, BC004L14_n1835GpoObjCtrlResponsavel_CteUsrCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1838GpoObjCtrlResponsavel_GpoCod ;
      private short A1838GpoObjCtrlResponsavel_GpoCod ;
      private short RcdFound202 ;
      private int trnEnded ;
      private int Z1837GpoObjCtrlResponsavel_Codigo ;
      private int A1837GpoObjCtrlResponsavel_Codigo ;
      private int Z1834GpoObjCtrlResponsavel_CteCteCod ;
      private int A1834GpoObjCtrlResponsavel_CteCteCod ;
      private int Z1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int Z1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode202 ;
      private bool n1838GpoObjCtrlResponsavel_GpoCod ;
      private bool n1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private SdtGpoObjCtrlResponsavel bcGpoObjCtrlResponsavel ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004L7_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] BC004L7_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] BC004L7_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] BC004L7_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] BC004L7_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] BC004L7_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] BC004L7_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] BC004L5_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] BC004L5_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int[] BC004L6_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] BC004L6_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] BC004L8_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] BC004L3_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] BC004L3_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] BC004L3_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] BC004L3_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] BC004L3_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] BC004L3_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] BC004L3_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] BC004L2_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] BC004L2_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] BC004L2_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] BC004L2_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] BC004L2_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] BC004L2_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] BC004L2_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] BC004L9_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] BC004L13_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] BC004L13_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int[] BC004L14_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] BC004L14_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] BC004L14_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] BC004L14_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] BC004L14_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private int[] BC004L14_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] BC004L14_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class gpoobjctrlresponsavel_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004L7 ;
          prmBC004L7 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L5 ;
          prmBC004L5 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L6 ;
          prmBC004L6 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L8 ;
          prmBC004L8 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L3 ;
          prmBC004L3 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L2 ;
          prmBC004L2 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L9 ;
          prmBC004L9 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L10 ;
          prmBC004L10 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L11 ;
          prmBC004L11 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L13 ;
          prmBC004L13 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004L14 ;
          prmBC004L14 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004L2", "SELECT [GpoObjCtrlResponsavel_Codigo], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] WITH (UPDLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L2,1,0,true,false )
             ,new CursorDef("BC004L3", "SELECT [GpoObjCtrlResponsavel_Codigo], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L3,1,0,true,false )
             ,new CursorDef("BC004L5", "SELECT COALESCE( T1.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @GpoObjCtrlResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L5,1,0,true,false )
             ,new CursorDef("BC004L6", "SELECT [ContratanteUsuario_ContratanteCod] AS GpoObjCtrlResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @GpoObjCtrlResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @GpoObjCtrlResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L6,1,0,true,false )
             ,new CursorDef("BC004L7", "SELECT TM1.[GpoObjCtrlResponsavel_Codigo], TM1.[GpoObjCtrlResponsavel_GpoCod], TM1.[GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, TM1.[GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ORDER BY TM1.[GpoObjCtrlResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L7,100,0,true,false )
             ,new CursorDef("BC004L8", "SELECT [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L8,1,0,true,false )
             ,new CursorDef("BC004L9", "INSERT INTO [GpoObjCtrlResponsavel]([GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_CteUsrCod]) VALUES(@GpoObjCtrlResponsavel_GpoCod, @GpoObjCtrlResponsavel_CteCteCod, @GpoObjCtrlResponsavel_CteUsrCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004L9)
             ,new CursorDef("BC004L10", "UPDATE [GpoObjCtrlResponsavel] SET [GpoObjCtrlResponsavel_GpoCod]=@GpoObjCtrlResponsavel_GpoCod, [GpoObjCtrlResponsavel_CteCteCod]=@GpoObjCtrlResponsavel_CteCteCod, [GpoObjCtrlResponsavel_CteUsrCod]=@GpoObjCtrlResponsavel_CteUsrCod  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmBC004L10)
             ,new CursorDef("BC004L11", "DELETE FROM [GpoObjCtrlResponsavel]  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmBC004L11)
             ,new CursorDef("BC004L13", "SELECT COALESCE( T1.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @GpoObjCtrlResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L13,1,0,true,false )
             ,new CursorDef("BC004L14", "SELECT TM1.[GpoObjCtrlResponsavel_Codigo], TM1.[GpoObjCtrlResponsavel_GpoCod], TM1.[GpoObjCtrlResponsavel_CteCteCod] AS GpoObjCtrlResponsavel_CteCteCod, TM1.[GpoObjCtrlResponsavel_CteUsrCod] AS GpoObjCtrlResponsavel_CteUsrCod FROM [GpoObjCtrlResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo ORDER BY TM1.[GpoObjCtrlResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004L14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
