/*
               File: WP_ImportFromFile
        Description: Importar Arquivo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:20:17.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_importfromfile : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_importfromfile( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_importfromfile( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOqimportar = new GXCombobox();
         cmbavRequisitante = new GXCombobox();
         chkavSrvparaftr = new GXCheckbox();
         chkavNotificar = new GXCheckbox();
         cmbavContratada_codigo = new GXCombobox();
         dynavSelservico_codigo = new GXCombobox();
         cmbavContratoservicos_codigo = new GXCombobox();
         cmbavStatusdmn = new GXCombobox();
         cmbavDatasrvfat = new GXCombobox();
         dynavContratadaorigem_codigo = new GXCombobox();
         dynavSelcontratadasrv_codigo = new GXCombobox();
         dynavSelosservico_codigo = new GXCombobox();
         cmbavContratoservicosos_codigo = new GXCombobox();
         cmbavContagemresultado_ehvalidacao = new GXCombobox();
         chkavCalcularprazo = new GXCheckbox();
         dynavContagemresultado_contadorfmcod = new GXCombobox();
         dynavContagemresultado_contadorfscod = new GXCombobox();
         cmbavAcao = new GXCombobox();
         cmbavTipofiltro = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vAGRUPADOR") == 0 )
            {
               AV156AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvAGRUPADORBL0( AV156AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELSERVICO_CODIGO") == 0 )
            {
               AV127Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELSERVICO_CODIGOBL2( AV127Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADAORIGEM_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV74WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAORIGEM_CODIGOBL2( AV74WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTRATADASRV_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV74WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELCONTRATADASRV_CODIGOBL2( AV74WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELOSSERVICO_CODIGO") == 0 )
            {
               AV135SelContratadaSrv_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135SelContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELOSSERVICO_CODIGOBL2( AV135SelContratadaSrv_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV74WWPContext);
               AV127Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFMCODBL2( AV74WWPContext, AV127Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFSCOD") == 0 )
            {
               AV127Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvCONTAGEMRESULTADO_CONTADORFSCODBL2( AV127Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridsdtdemandas") == 0 )
            {
               nRC_GXsfl_373 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_373_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_373_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridsdtdemandas_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridsdtdemandas") == 0 )
            {
               AV120Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120Servico_Codigo), 6, 0)));
               AV136ContratadaSrv_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136ContratadaSrv_Codigo), 6, 0)));
               AV126OSServico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
               AV145DataSrvFatSel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145DataSrvFatSel", AV145DataSrvFatSel);
               AV97ColPFBFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ColPFBFM", AV97ColPFBFM);
               AV111ColPFBFMCAST = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ColPFBFMCAST", AV111ColPFBFMCAST);
               AV56OQImportar = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56OQImportar", AV56OQImportar);
               AV143SrvParaFtr = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV143SrvParaFtr", AV143SrvParaFtr);
               AV181RequerOrigem = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV181RequerOrigem", AV181RequerOrigem);
               AV15ColPFBFS = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ColPFBFS", AV15ColPFBFS);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV93SDT_Demandas);
               AV46Ln = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Ln", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Ln), 4, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridsdtdemandas_refresh( AV120Servico_Codigo, AV136ContratadaSrv_Codigo, AV126OSServico_Codigo, AV145DataSrvFatSel, AV97ColPFBFM, AV111ColPFBFMCAST, AV56OQImportar, AV143SrvParaFtr, AV181RequerOrigem, AV15ColPFBFS, AV93SDT_Demandas, AV46Ln) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABL2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBL2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216201883");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_importfromfile.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_demandas", AV93SDT_Demandas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_demandas", AV93SDT_Demandas);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_373", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_373), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDATASRVFATSEL", StringUtil.RTrim( AV145DataSrvFatSel));
         GxWebStd.gx_boolean_hidden_field( context, "vREQUERORIGEM", AV181RequerOrigem);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_DEMANDAS", AV93SDT_Demandas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_DEMANDAS", AV93SDT_Demandas);
         }
         GxWebStd.gx_hidden_field( context, "vLN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Ln), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_boolean_hidden_field( context, "vOSAUTOMATICA", AV95OSAutomatica);
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGESTORDACONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV180GestorDaContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATACAST", StringUtil.RTrim( AV113ColDataCAST));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "SISTEMA_ATIVO", A130Sistema_Ativo);
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_NOME", StringUtil.RTrim( A427NaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vARQUIVO", StringUtil.RTrim( AV8Arquivo));
         GxWebStd.gx_hidden_field( context, "vCOLDMNCASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108ColDmnCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATACNTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV148ColDataCntn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLCONTADORFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV102ColContadorFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV118ColPFBFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV119ColPFlFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV99ColPFBFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100ColPFlFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPARECERN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV174ColParecern), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATACASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV107ColDataCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFSCASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV103ColPFBFSCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFSCASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104ColPFLFSCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFMCASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105ColPFBFMCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFMCASTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106ColPFLFMCASTn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vARQUIVO2", StringUtil.RTrim( AV9Arquivo2));
         GxWebStd.gx_hidden_field( context, "vCOLDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ColDem), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ColPB), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ColPL), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOANOM", StringUtil.RTrim( A41Contratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_NOME", StringUtil.RTrim( AV26Contratada_Nome));
         GxWebStd.gx_hidden_field( context, "vCOLOSFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85ColOSFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLNOMESISTEMAN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86ColNomeSisteman), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLNOMEMODULON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV163ColNomeModulon), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLOSFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87ColOSFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATADMNN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88ColDataDmnn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDESCRICAON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89ColDescricaon), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLLINKN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91ColLinkn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLFILTRON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV131ColFiltron), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFSDMNN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV141ColPFBFSDmnn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFSDMNN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV140ColPFLFSDmnn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLCONTADORFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90ColContadorFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPROJETON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV167ColProjeton), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLTIPON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV169ColTipon), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLOBSERVACAON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV171ColObservacaon), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV165TipoDias));
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.DToC( A1869Contrato_DataTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADASIGLA", StringUtil.RTrim( A1223ContratoGestor_ContratadaSigla));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOATV", A2033ContratoGestor_UsuarioAtv);
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV74WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV74WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV158Blob);
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Width", StringUtil.RTrim( Innewwindow_Width));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Height", StringUtil.RTrim( Innewwindow_Height));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmationtext", StringUtil.RTrim( Confirmpanel2_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel2_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmtype", StringUtil.RTrim( Confirmpanel2_Confirmtype));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO_Text", StringUtil.RTrim( cmbavContratada_codigo.Description));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, "vCONTRATADAORIGEM_CODIGO_Text", StringUtil.RTrim( dynavContratadaorigem_codigo.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wc_anexos == null ) )
         {
            WebComp_Wc_anexos.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBL2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBL2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_importfromfile.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ImportFromFile" ;
      }

      public override String GetPgmdesc( )
      {
         return "Importar Arquivo" ;
      }

      protected void WBBL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_BL2( true) ;
         }
         else
         {
            wb_table1_2_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 394,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV156AreaTrabalho_Codigo), 6, 0, ",", "")), ((edtavAreatrabalho_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV156AreaTrabalho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV156AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,394);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_codigo_Visible, edtavAreatrabalho_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 395,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV134FileName), StringUtil.RTrim( context.localUtil.Format( AV134FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,395);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "AttributeCustom", "", "", "", edtavFilename_Visible, edtavFilename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANEL2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANEL2Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
         }
         wbLoad = true;
      }

      protected void STARTBL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Importar Arquivo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBL0( ) ;
      }

      protected void WSBL2( )
      {
         STARTBL2( ) ;
         EVTBL2( ) ;
      }

      protected void EVTBL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BL2 */
                              E11BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL2.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12BL2 */
                              E12BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSELSERVICO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13BL2 */
                              E13BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSELOSSERVICO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14BL2 */
                              E14BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VOQIMPORTAR.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15BL2 */
                              E15BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16BL2 */
                              E16BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDATAENTREGA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17BL2 */
                              E17BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'IMPORTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18BL2 */
                              E18BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VREQUISITANTE.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19BL2 */
                              E19BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATOSERVICOS_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20BL2 */
                              E20BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATADAORIGEM_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21BL2 */
                              E21BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSRVPARAFTR.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22BL2 */
                              E22BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSELCONTRATADASRV_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23BL2 */
                              E23BL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "GRIDSDTDEMANDAS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_373_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
                              SubsflControlProps_3732( ) ;
                              AV184GXV1 = nGXsfl_373_idx;
                              if ( ( AV93SDT_Demandas.Count >= AV184GXV1 ) && ( AV184GXV1 > 0 ) )
                              {
                                 AV93SDT_Demandas.CurrentItem = ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1));
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( AV158Blob)) )
                              {
                                 GXCCtl = "vBLOB_" + sGXsfl_373_idx;
                                 GXCCtlgxBlob = GXCCtl + "_gxBlob";
                                 AV158Blob = cgiGet( GXCCtlgxBlob);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24BL2 */
                                    E24BL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25BL2 */
                                    E25BL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDSDTDEMANDAS.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26BL2 */
                                    E26BL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: E16BL2 */
                                          E16BL2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 339 )
                        {
                           WebComp_Wc_anexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
                           WebComp_Wc_anexos.ComponentInit();
                           WebComp_Wc_anexos.Name = "WC_ContagemResultadoEvidencias";
                           WebComp_Wc_anexos_Component = "WC_ContagemResultadoEvidencias";
                           WebComp_Wc_anexos.componentprocess("W0339", "", sEvt);
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOqimportar.Name = "vOQIMPORTAR";
            cmbavOqimportar.WebTags = "";
            cmbavOqimportar.addItem("IDMN", "Importar Demandas", 0);
            cmbavOqimportar.addItem("ICNT", "Inserir contagens", 0);
            cmbavOqimportar.addItem("ICCT", "Importar contagens FME", 0);
            cmbavOqimportar.addItem("CNF1", "Confer�ncia planilha SIRIUS", 0);
            cmbavOqimportar.addItem("SGCD", "Confer�ncia planilha CAST", 0);
            cmbavOqimportar.addItem("PDMS", "Planilha diaria MS", 0);
            cmbavOqimportar.addItem("TA", "Tabelas e Atributos", 0);
            cmbavOqimportar.addItem("CTRF", "Confer�ncia TRF", 0);
            if ( cmbavOqimportar.ItemCount > 0 )
            {
               AV56OQImportar = cmbavOqimportar.getValidValue(AV56OQImportar);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56OQImportar", AV56OQImportar);
            }
            cmbavRequisitante.Name = "vREQUISITANTE";
            cmbavRequisitante.WebTags = "";
            cmbavRequisitante.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavRequisitante.ItemCount > 0 )
            {
               AV178Requisitante = (int)(NumberUtil.Val( cmbavRequisitante.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
            }
            chkavSrvparaftr.Name = "vSRVPARAFTR";
            chkavSrvparaftr.WebTags = "";
            chkavSrvparaftr.Caption = "Apenas para Faturar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSrvparaftr_Internalname, "TitleCaption", chkavSrvparaftr.Caption);
            chkavSrvparaftr.CheckedValue = "false";
            chkavNotificar.Name = "vNOTIFICAR";
            chkavNotificar.WebTags = "";
            chkavNotificar.Caption = "Enviar notifica��o para a Prestadora";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNotificar_Internalname, "TitleCaption", chkavNotificar.Caption);
            chkavNotificar.CheckedValue = "false";
            cmbavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            cmbavContratada_codigo.WebTags = "";
            cmbavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
            if ( cmbavContratada_codigo.ItemCount > 0 )
            {
               AV127Contratada_Codigo = (int)(NumberUtil.Val( cmbavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            }
            dynavSelservico_codigo.Name = "vSELSERVICO_CODIGO";
            dynavSelservico_codigo.WebTags = "";
            cmbavContratoservicos_codigo.Name = "vCONTRATOSERVICOS_CODIGO";
            cmbavContratoservicos_codigo.WebTags = "";
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContratoservicos_codigo.ItemCount > 0 )
            {
               AV176ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
            }
            cmbavStatusdmn.Name = "vSTATUSDMN";
            cmbavStatusdmn.WebTags = "";
            cmbavStatusdmn.addItem("", "Selecionar", 0);
            cmbavStatusdmn.addItem("R", "Resolvida", 0);
            cmbavStatusdmn.addItem("C", "Conferida", 0);
            cmbavStatusdmn.addItem("L", "Liquidada", 0);
            cmbavStatusdmn.addItem("S", "Solicitada", 0);
            if ( cmbavStatusdmn.ItemCount > 0 )
            {
               AV121StatusDmn = cmbavStatusdmn.getValidValue(AV121StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
            }
            cmbavDatasrvfat.Name = "vDATASRVFAT";
            cmbavDatasrvfat.WebTags = "";
            cmbavDatasrvfat.addItem("H", "Data de Hoje", 0);
            cmbavDatasrvfat.addItem("M", "Mesma data da OS", 0);
            if ( cmbavDatasrvfat.ItemCount > 0 )
            {
               AV142DataSrvFat = cmbavDatasrvfat.getValidValue(AV142DataSrvFat);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142DataSrvFat", AV142DataSrvFat);
            }
            dynavContratadaorigem_codigo.Name = "vCONTRATADAORIGEM_CODIGO";
            dynavContratadaorigem_codigo.WebTags = "";
            dynavSelcontratadasrv_codigo.Name = "vSELCONTRATADASRV_CODIGO";
            dynavSelcontratadasrv_codigo.WebTags = "";
            dynavSelosservico_codigo.Name = "vSELOSSERVICO_CODIGO";
            dynavSelosservico_codigo.WebTags = "";
            cmbavContratoservicosos_codigo.Name = "vCONTRATOSERVICOSOS_CODIGO";
            cmbavContratoservicosos_codigo.WebTags = "";
            cmbavContratoservicosos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContratoservicosos_codigo.ItemCount > 0 )
            {
               AV177ContratoServicosOS_Codigo = (int)(NumberUtil.Val( cmbavContratoservicosos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ContratoServicosOS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)));
            }
            cmbavContagemresultado_ehvalidacao.Name = "vCONTAGEMRESULTADO_EHVALIDACAO";
            cmbavContagemresultado_ehvalidacao.WebTags = "";
            cmbavContagemresultado_ehvalidacao.addItem(StringUtil.BoolToStr( false), "Contagem", 0);
            cmbavContagemresultado_ehvalidacao.addItem(StringUtil.BoolToStr( true), "Valida��o", 0);
            if ( cmbavContagemresultado_ehvalidacao.ItemCount > 0 )
            {
               AV20ContagemResultado_EhValidacao = StringUtil.StrToBool( cmbavContagemresultado_ehvalidacao.getValidValue(StringUtil.BoolToStr( AV20ContagemResultado_EhValidacao)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_EhValidacao", AV20ContagemResultado_EhValidacao);
            }
            chkavCalcularprazo.Name = "vCALCULARPRAZO";
            chkavCalcularprazo.WebTags = "";
            chkavCalcularprazo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCalcularprazo_Internalname, "TitleCaption", chkavCalcularprazo.Caption);
            chkavCalcularprazo.CheckedValue = "false";
            dynavContagemresultado_contadorfmcod.Name = "vCONTAGEMRESULTADO_CONTADORFMCOD";
            dynavContagemresultado_contadorfmcod.WebTags = "";
            dynavContagemresultado_contadorfscod.Name = "vCONTAGEMRESULTADO_CONTADORFSCOD";
            dynavContagemresultado_contadorfscod.WebTags = "";
            cmbavAcao.Name = "vACAO";
            cmbavAcao.WebTags = "";
            cmbavAcao.addItem("C", "Conferir", 0);
            cmbavAcao.addItem("V", "Validar", 0);
            cmbavAcao.addItem("A", "Atualizar", 0);
            cmbavAcao.addItem("Z", "Zerar PF FS", 0);
            if ( cmbavAcao.ItemCount > 0 )
            {
               AV7Acao = cmbavAcao.getValidValue(AV7Acao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Acao", AV7Acao);
            }
            cmbavTipofiltro.Name = "vTIPOFILTRO";
            cmbavTipofiltro.WebTags = "";
            cmbavTipofiltro.addItem("C", "Character", 0);
            cmbavTipofiltro.addItem("N", "Numeric", 0);
            cmbavTipofiltro.addItem("D", "Date", 0);
            if ( cmbavTipofiltro.ItemCount > 0 )
            {
               AV132TipoFiltro = cmbavTipofiltro.getValidValue(AV132TipoFiltro);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132TipoFiltro", AV132TipoFiltro);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOqimportar_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvSELSERVICO_CODIGO_htmlBL2( AV127Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlBL2( AV74WWPContext, AV127Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlBL2( AV74WWPContext, AV127Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlBL2( AV127Contratada_Codigo) ;
         GXVvSELOSSERVICO_CODIGO_htmlBL2( AV135SelContratadaSrv_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXSGVvAGRUPADORBL0( int AV156AreaTrabalho_Codigo ,
                                         String A1046ContagemResultado_Agrupador )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvAGRUPADOR_dataBL0( AV156AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvAGRUPADOR_dataBL0( int AV156AreaTrabalho_Codigo ,
                                              String A1046ContagemResultado_Agrupador )
      {
         l1046ContagemResultado_Agrupador = StringUtil.PadR( StringUtil.RTrim( A1046ContagemResultado_Agrupador), 15, "%");
         n1046ContagemResultado_Agrupador = false;
         /* Using cursor H00BL2 */
         pr_default.execute(0, new Object[] {l1046ContagemResultado_Agrupador, AV156AreaTrabalho_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00BL2_A1046ContagemResultado_Agrupador[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL2_A1046ContagemResultado_Agrupador[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvSELSERVICO_CODIGOBL2( int AV127Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELSERVICO_CODIGO_dataBL2( AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELSERVICO_CODIGO_htmlBL2( int AV127Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSELSERVICO_CODIGO_dataBL2( AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavSelservico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelservico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelservico_codigo.ItemCount > 0 )
         {
            AV125SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELSERVICO_CODIGO_dataBL2( int AV127Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00BL3 */
         pr_default.execute(1, new Object[] {AV127Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BL3_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL3_A605Servico_Sigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATADAORIGEM_CODIGOBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADAORIGEM_CODIGO_dataBL2( AV74WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADAORIGEM_CODIGO_htmlBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADAORIGEM_CODIGO_dataBL2( AV74WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratadaorigem_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratadaorigem_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratadaorigem_codigo.ItemCount > 0 )
         {
            AV150ContratadaOrigem_Codigo = (int)(NumberUtil.Val( dynavContratadaorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV150ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADAORIGEM_CODIGO_dataBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00BL4 */
         pr_default.execute(2, new Object[] {AV74WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BL4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvSELCONTRATADASRV_CODIGOBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTRATADASRV_CODIGO_dataBL2( AV74WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTRATADASRV_CODIGO_htmlBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTRATADASRV_CODIGO_dataBL2( AV74WWPContext) ;
         gxdynajaxindex = 1;
         dynavSelcontratadasrv_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontratadasrv_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelcontratadasrv_codigo.ItemCount > 0 )
         {
            AV135SelContratadaSrv_Codigo = (int)(NumberUtil.Val( dynavSelcontratadasrv_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135SelContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELCONTRATADASRV_CODIGO_dataBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00BL5 */
         pr_default.execute(3, new Object[] {AV74WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BL5_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL5_A41Contratada_PessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvSELOSSERVICO_CODIGOBL2( int AV135SelContratadaSrv_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELOSSERVICO_CODIGO_dataBL2( AV135SelContratadaSrv_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELOSSERVICO_CODIGO_htmlBL2( int AV135SelContratadaSrv_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSELOSSERVICO_CODIGO_dataBL2( AV135SelContratadaSrv_Codigo) ;
         gxdynajaxindex = 1;
         dynavSelosservico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelosservico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelosservico_codigo.ItemCount > 0 )
         {
            AV144SelOsServico_Codigo = (int)(NumberUtil.Val( dynavSelosservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV144SelOsServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELOSSERVICO_CODIGO_dataBL2( int AV135SelContratadaSrv_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00BL6 */
         pr_default.execute(4, new Object[] {AV135SelContratadaSrv_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BL6_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL6_A605Servico_Sigla[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCODBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext ,
                                                               int AV127Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataBL2( AV74WWPContext, AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext ,
                                                                  int AV127Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataBL2( AV74WWPContext, AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfmcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV18ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataBL2( wwpbaseobjects.SdtWWPContext AV74WWPContext ,
                                                                    int AV127Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00BL7 */
         pr_default.execute(5, new Object[] {AV74WWPContext.gxTpr_Areatrabalho_codigo, AV127Contratada_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BL7_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BL7_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDSVvCONTAGEMRESULTADO_CONTADORFSCODBL2( int AV127Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvCONTAGEMRESULTADO_CONTADORFSCOD_dataBL2( AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlBL2( int AV127Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDSVvCONTAGEMRESULTADO_CONTADORFSCOD_dataBL2( AV127Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfscod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfscod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV81ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)));
         }
      }

      protected void GXDSVvCONTAGEMRESULTADO_CONTADORFSCOD_dataBL2( int AV127Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         IGxCollection gxcolvCONTAGEMRESULTADO_CONTADORFSCOD ;
         SdtSDT_Usuarios_Usuario gxcolitemvCONTAGEMRESULTADO_CONTADORFSCOD ;
         new dp_contadoresfs(context ).execute(  AV127Contratada_Codigo, out  gxcolvCONTAGEMRESULTADO_CONTADORFSCOD) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
         gxcolvCONTAGEMRESULTADO_CONTADORFSCOD.Sort("Nome");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvCONTAGEMRESULTADO_CONTADORFSCOD.Count )
         {
            gxcolitemvCONTAGEMRESULTADO_CONTADORFSCOD = ((SdtSDT_Usuarios_Usuario)gxcolvCONTAGEMRESULTADO_CONTADORFSCOD.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvCONTAGEMRESULTADO_CONTADORFSCOD.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvCONTAGEMRESULTADO_CONTADORFSCOD.gxTpr_Nome);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void gxnrGridsdtdemandas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_3732( ) ;
         while ( nGXsfl_373_idx <= nRC_GXsfl_373 )
         {
            sendrow_3732( ) ;
            nGXsfl_373_idx = (short)(((subGridsdtdemandas_Islastpage==1)&&(nGXsfl_373_idx+1>subGridsdtdemandas_Recordsperpage( )) ? 1 : nGXsfl_373_idx+1));
            sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
            SubsflControlProps_3732( ) ;
         }
         context.GX_webresponse.AddString(GridsdtdemandasContainer.ToJavascriptSource());
         /* End function gxnrGridsdtdemandas_newrow */
      }

      protected void gxgrGridsdtdemandas_refresh( int AV120Servico_Codigo ,
                                                  int AV136ContratadaSrv_Codigo ,
                                                  int AV126OSServico_Codigo ,
                                                  String AV145DataSrvFatSel ,
                                                  String AV97ColPFBFM ,
                                                  String AV111ColPFBFMCAST ,
                                                  String AV56OQImportar ,
                                                  bool AV143SrvParaFtr ,
                                                  bool AV181RequerOrigem ,
                                                  String AV15ColPFBFS ,
                                                  IGxCollection AV93SDT_Demandas ,
                                                  short AV46Ln )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDSDTDEMANDAS_nCurrentRecord = 0;
         RFBL2( ) ;
         /* End function gxgrGridsdtdemandas_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOqimportar.ItemCount > 0 )
         {
            AV56OQImportar = cmbavOqimportar.getValidValue(AV56OQImportar);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56OQImportar", AV56OQImportar);
         }
         if ( cmbavRequisitante.ItemCount > 0 )
         {
            AV178Requisitante = (int)(NumberUtil.Val( cmbavRequisitante.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
         }
         if ( cmbavContratada_codigo.ItemCount > 0 )
         {
            AV127Contratada_Codigo = (int)(NumberUtil.Val( cmbavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
         }
         if ( dynavSelservico_codigo.ItemCount > 0 )
         {
            AV125SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0)));
         }
         if ( cmbavContratoservicos_codigo.ItemCount > 0 )
         {
            AV176ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
         }
         if ( cmbavStatusdmn.ItemCount > 0 )
         {
            AV121StatusDmn = cmbavStatusdmn.getValidValue(AV121StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
         }
         if ( cmbavDatasrvfat.ItemCount > 0 )
         {
            AV142DataSrvFat = cmbavDatasrvfat.getValidValue(AV142DataSrvFat);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142DataSrvFat", AV142DataSrvFat);
         }
         if ( dynavContratadaorigem_codigo.ItemCount > 0 )
         {
            AV150ContratadaOrigem_Codigo = (int)(NumberUtil.Val( dynavContratadaorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV150ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0)));
         }
         if ( dynavSelcontratadasrv_codigo.ItemCount > 0 )
         {
            AV135SelContratadaSrv_Codigo = (int)(NumberUtil.Val( dynavSelcontratadasrv_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135SelContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)));
         }
         if ( dynavSelosservico_codigo.ItemCount > 0 )
         {
            AV144SelOsServico_Codigo = (int)(NumberUtil.Val( dynavSelosservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV144SelOsServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0)));
         }
         if ( cmbavContratoservicosos_codigo.ItemCount > 0 )
         {
            AV177ContratoServicosOS_Codigo = (int)(NumberUtil.Val( cmbavContratoservicosos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ContratoServicosOS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)));
         }
         if ( cmbavContagemresultado_ehvalidacao.ItemCount > 0 )
         {
            AV20ContagemResultado_EhValidacao = StringUtil.StrToBool( cmbavContagemresultado_ehvalidacao.getValidValue(StringUtil.BoolToStr( AV20ContagemResultado_EhValidacao)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_EhValidacao", AV20ContagemResultado_EhValidacao);
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV18ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV81ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)));
         }
         if ( cmbavAcao.ItemCount > 0 )
         {
            AV7Acao = cmbavAcao.getValidValue(AV7Acao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Acao", AV7Acao);
         }
         if ( cmbavTipofiltro.ItemCount > 0 )
         {
            AV132TipoFiltro = cmbavTipofiltro.getValidValue(AV132TipoFiltro);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132TipoFiltro", AV132TipoFiltro);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlsistemanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlsistemanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsistemanom_Enabled), 5, 0)));
         edtavCtldemandafm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldemandafm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldemandafm_Enabled), 5, 0)));
         edtavCtldatadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldatadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldatadmn_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         edtavCtldataent_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldataent_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldataent_Enabled), 5, 0)));
         edtavCtldemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldemanda_Enabled), 5, 0)));
         edtavCtlcontadorfsnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcontadorfsnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontadorfsnom_Enabled), 5, 0)));
         edtavCtllink_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllink_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllink_Enabled), 5, 0)));
         edtavCtlpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpfbfm_Enabled), 5, 0)));
         edtavCtlpflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpflfm_Enabled), 5, 0)));
         edtavCtlpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpfbfs_Enabled), 5, 0)));
         edtavCtlpflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpflfs_Enabled), 5, 0)));
         edtavCtlcontadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcontadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontadorfmcod_Enabled), 5, 0)));
      }

      protected void RFBL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridsdtdemandasContainer.ClearRows();
         }
         wbStart = 373;
         /* Execute user event: E25BL2 */
         E25BL2 ();
         nGXsfl_373_idx = 1;
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
         nGXsfl_373_Refreshing = 1;
         GridsdtdemandasContainer.AddObjectProperty("GridName", "Gridsdtdemandas");
         GridsdtdemandasContainer.AddObjectProperty("CmpContext", "");
         GridsdtdemandasContainer.AddObjectProperty("InMasterPage", "false");
         GridsdtdemandasContainer.AddObjectProperty("Class", "WorkWith");
         GridsdtdemandasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridsdtdemandasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridsdtdemandasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Backcolorstyle), 1, 0, ".", "")));
         GridsdtdemandasContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Titleforecolor), 9, 0, ".", "")));
         GridsdtdemandasContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Visible), 5, 0, ".", "")));
         GridsdtdemandasContainer.PageSize = subGridsdtdemandas_Recordsperpage( );
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Wc_anexos_Component, "") == 0 )
            {
               WebComp_Wc_anexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
               WebComp_Wc_anexos.ComponentInit();
               WebComp_Wc_anexos.Name = "WC_ContagemResultadoEvidencias";
               WebComp_Wc_anexos_Component = "WC_ContagemResultadoEvidencias";
            }
            WebComp_Wc_anexos.setjustcreated();
            WebComp_Wc_anexos.componentprepare(new Object[] {(String)"W0339",(String)""});
            WebComp_Wc_anexos.componentbind(new Object[] {});
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0339"+"");
               WebComp_Wc_anexos.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               WebComp_Wc_anexos.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_3732( ) ;
            /* Execute user event: E26BL2 */
            E26BL2 ();
            wbEnd = 373;
            WBBL0( ) ;
         }
         nGXsfl_373_Refreshing = 0;
      }

      protected int subGridsdtdemandas_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridsdtdemandas_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridsdtdemandas_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridsdtdemandas_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPBL0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlsistemanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlsistemanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsistemanom_Enabled), 5, 0)));
         edtavCtldemandafm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldemandafm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldemandafm_Enabled), 5, 0)));
         edtavCtldatadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldatadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldatadmn_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         edtavCtldataent_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldataent_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldataent_Enabled), 5, 0)));
         edtavCtldemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldemanda_Enabled), 5, 0)));
         edtavCtlcontadorfsnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcontadorfsnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontadorfsnom_Enabled), 5, 0)));
         edtavCtllink_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllink_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllink_Enabled), 5, 0)));
         edtavCtlpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpfbfm_Enabled), 5, 0)));
         edtavCtlpflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpflfm_Enabled), 5, 0)));
         edtavCtlpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpfbfs_Enabled), 5, 0)));
         edtavCtlpflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpflfs_Enabled), 5, 0)));
         edtavCtlcontadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcontadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontadorfmcod_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24BL2 */
         E24BL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADAORIGEM_CODIGO_htmlBL2( AV74WWPContext) ;
         GXVvSELCONTRATADASRV_CODIGO_htmlBL2( AV74WWPContext) ;
         GXVvSELOSSERVICO_CODIGO_htmlBL2( AV135SelContratadaSrv_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_demandas"), AV93SDT_Demandas);
            /* Read variables values. */
            cmbavOqimportar.Name = cmbavOqimportar_Internalname;
            cmbavOqimportar.CurrentValue = cgiGet( cmbavOqimportar_Internalname);
            AV56OQImportar = cgiGet( cmbavOqimportar_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56OQImportar", AV56OQImportar);
            cmbavRequisitante.Name = cmbavRequisitante_Internalname;
            cmbavRequisitante.CurrentValue = cgiGet( cmbavRequisitante_Internalname);
            AV178Requisitante = (int)(NumberUtil.Val( cgiGet( cmbavRequisitante_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
            AV143SrvParaFtr = StringUtil.StrToBool( cgiGet( chkavSrvparaftr_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV143SrvParaFtr", AV143SrvParaFtr);
            AV154Notificar = StringUtil.StrToBool( cgiGet( chkavNotificar_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154Notificar", AV154Notificar);
            cmbavContratada_codigo.Name = cmbavContratada_codigo_Internalname;
            cmbavContratada_codigo.CurrentValue = cgiGet( cmbavContratada_codigo_Internalname);
            AV127Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            dynavSelservico_codigo.Name = dynavSelservico_codigo_Internalname;
            dynavSelservico_codigo.CurrentValue = cgiGet( dynavSelservico_codigo_Internalname);
            AV125SelServico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelservico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0)));
            cmbavContratoservicos_codigo.Name = cmbavContratoservicos_codigo_Internalname;
            cmbavContratoservicos_codigo.CurrentValue = cgiGet( cmbavContratoservicos_codigo_Internalname);
            AV176ContratoServicos_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
            cmbavStatusdmn.Name = cmbavStatusdmn_Internalname;
            cmbavStatusdmn.CurrentValue = cgiGet( cmbavStatusdmn_Internalname);
            AV121StatusDmn = cgiGet( cmbavStatusdmn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
            cmbavDatasrvfat.Name = cmbavDatasrvfat_Internalname;
            cmbavDatasrvfat.CurrentValue = cgiGet( cmbavDatasrvfat_Internalname);
            AV142DataSrvFat = cgiGet( cmbavDatasrvfat_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142DataSrvFat", AV142DataSrvFat);
            dynavContratadaorigem_codigo.Name = dynavContratadaorigem_codigo_Internalname;
            dynavContratadaorigem_codigo.CurrentValue = cgiGet( dynavContratadaorigem_codigo_Internalname);
            AV150ContratadaOrigem_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratadaorigem_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV150ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_CODIGO");
               GX_FocusControl = edtavServico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV120Servico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120Servico_Codigo), 6, 0)));
            }
            else
            {
               AV120Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavServico_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120Servico_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralincast_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralincast_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINCAST");
               GX_FocusControl = edtavPralincast_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV116PraLinCAST = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116PraLinCAST", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116PraLinCAST), 4, 0)));
            }
            else
            {
               AV116PraLinCAST = (short)(context.localUtil.CToN( cgiGet( edtavPralincast_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116PraLinCAST", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116PraLinCAST), 4, 0)));
            }
            AV114ColDmnCAST = StringUtil.Upper( cgiGet( edtavColdmncast_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114ColDmnCAST", AV114ColDmnCAST);
            AV109ColPFBFSCAST = StringUtil.Upper( cgiGet( edtavColpfbfscast_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109ColPFBFSCAST", AV109ColPFBFSCAST);
            AV110ColPFLFSCAST = StringUtil.Upper( cgiGet( edtavColpflfscast_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ColPFLFSCAST", AV110ColPFLFSCAST);
            AV111ColPFBFMCAST = StringUtil.Upper( cgiGet( edtavColpfbfmcast_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ColPFBFMCAST", AV111ColPFBFMCAST);
            AV112ColPFLFMCAST = StringUtil.Upper( cgiGet( edtavColpflfmcast_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112ColPFLFMCAST", AV112ColPFLFMCAST);
            dynavSelcontratadasrv_codigo.Name = dynavSelcontratadasrv_codigo_Internalname;
            dynavSelcontratadasrv_codigo.CurrentValue = cgiGet( dynavSelcontratadasrv_codigo_Internalname);
            AV135SelContratadaSrv_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontratadasrv_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135SelContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)));
            dynavSelosservico_codigo.Name = dynavSelosservico_codigo_Internalname;
            dynavSelosservico_codigo.CurrentValue = cgiGet( dynavSelosservico_codigo_Internalname);
            AV144SelOsServico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelosservico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV144SelOsServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0)));
            cmbavContratoservicosos_codigo.Name = cmbavContratoservicosos_codigo_Internalname;
            cmbavContratoservicosos_codigo.CurrentValue = cgiGet( cmbavContratoservicosos_codigo_Internalname);
            AV177ContratoServicosOS_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicosos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ContratoServicosOS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadasrv_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadasrv_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADASRV_CODIGO");
               GX_FocusControl = edtavContratadasrv_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV136ContratadaSrv_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136ContratadaSrv_Codigo), 6, 0)));
            }
            else
            {
               AV136ContratadaSrv_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratadasrv_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136ContratadaSrv_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOsservico_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOsservico_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOSSERVICO_CODIGO");
               GX_FocusControl = edtavOsservico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV126OSServico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
            }
            else
            {
               AV126OSServico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavOsservico_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHA");
               GX_FocusControl = edtavPralinha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60PraLinha = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60PraLinha), 4, 0)));
            }
            else
            {
               AV60PraLinha = (short)(context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60PraLinha), 4, 0)));
            }
            AV75ColOSFS = StringUtil.Upper( cgiGet( edtavColosfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ColOSFS", AV75ColOSFS);
            AV80ColDescricao = StringUtil.Upper( cgiGet( edtavColdescricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ColDescricao", AV80ColDescricao);
            AV166ColProjeto = StringUtil.Upper( cgiGet( edtavColprojeto_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166ColProjeto", AV166ColProjeto);
            AV97ColPFBFM = StringUtil.Upper( cgiGet( edtavColpfbfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ColPFBFM", AV97ColPFBFM);
            AV98ColPFLFM = StringUtil.Upper( cgiGet( edtavColpflfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ColPFLFM", AV98ColPFLFM);
            AV138ColPFBFSDmn = StringUtil.Upper( cgiGet( edtavColpfbfsdmn_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV138ColPFBFSDmn", AV138ColPFBFSDmn);
            AV139ColPFLFSDmn = StringUtil.Upper( cgiGet( edtavColpflfsdmn_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139ColPFLFSDmn", AV139ColPFLFSDmn);
            cmbavContagemresultado_ehvalidacao.Name = cmbavContagemresultado_ehvalidacao_Internalname;
            cmbavContagemresultado_ehvalidacao.CurrentValue = cgiGet( cmbavContagemresultado_ehvalidacao_Internalname);
            AV20ContagemResultado_EhValidacao = StringUtil.StrToBool( cgiGet( cmbavContagemresultado_ehvalidacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_EhValidacao", AV20ContagemResultado_EhValidacao);
            AV76ColNomeSistema = StringUtil.Upper( cgiGet( edtavColnomesistema_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ColNomeSistema", AV76ColNomeSistema);
            AV162ColNomeModulo = cgiGet( edtavColnomemodulo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV162ColNomeModulo", AV162ColNomeModulo);
            AV168ColTipo = StringUtil.Upper( cgiGet( edtavColtipo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168ColTipo", AV168ColTipo);
            AV57OSFM = cgiGet( edtavOsfm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57OSFM", AV57OSFM);
            AV77ColOSFM = StringUtil.Upper( cgiGet( edtavColosfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ColOSFM", AV77ColOSFM);
            AV153Agrupador = StringUtil.Upper( cgiGet( edtavAgrupador_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153Agrupador", AV153Agrupador);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataosfm_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data OSFM"}), 1, "vDATAOSFM");
               GX_FocusControl = edtavDataosfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29DataOSFM = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DataOSFM", context.localUtil.Format(AV29DataOSFM, "99/99/99"));
            }
            else
            {
               AV29DataOSFM = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDataosfm_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DataOSFM", context.localUtil.Format(AV29DataOSFM, "99/99/99"));
            }
            AV78ColDataDmn = StringUtil.Upper( cgiGet( edtavColdatadmn_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ColDataDmn", AV78ColDataDmn);
            AV175CalcularPrazo = StringUtil.StrToBool( cgiGet( chkavCalcularprazo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175CalcularPrazo", AV175CalcularPrazo);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data Entrega"}), 1, "vDATAENTREGA");
               GX_FocusControl = edtavDataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV151DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV151DataEntrega = context.localUtil.CToT( cgiGet( edtavDataentrega_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
            }
            AV79ContagemResultado_Observacao = cgiGet( edtavContagemresultado_observacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ContagemResultado_Observacao", AV79ContagemResultado_Observacao);
            AV170ColObservacao = StringUtil.Upper( cgiGet( edtavColobservacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170ColObservacao", AV170ColObservacao);
            AV83ContagemResultado_Link = cgiGet( edtavContagemresultado_link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ContagemResultado_Link", AV83ContagemResultado_Link);
            AV84ColLink = StringUtil.Upper( cgiGet( edtavCollink_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ColLink", AV84ColLink);
            dynavContagemresultado_contadorfmcod.Name = dynavContagemresultado_contadorfmcod_Internalname;
            dynavContagemresultado_contadorfmcod.CurrentValue = cgiGet( dynavContagemresultado_contadorfmcod_Internalname);
            AV18ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0)));
            AV101ColContadorFM = StringUtil.Upper( cgiGet( edtavColcontadorfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101ColContadorFM", AV101ColContadorFM);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "vDATACNT");
               GX_FocusControl = edtavDatacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV146DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV146DataCnt", context.localUtil.Format(AV146DataCnt, "99/99/99"));
            }
            else
            {
               AV146DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV146DataCnt", context.localUtil.Format(AV146DataCnt, "99/99/99"));
            }
            AV147ColDataCnt = StringUtil.Upper( cgiGet( edtavColdatacnt_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147ColDataCnt", AV147ColDataCnt);
            AV173ColParecer = StringUtil.Upper( cgiGet( edtavColparecer_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173ColParecer", AV173ColParecer);
            dynavContagemresultado_contadorfscod.Name = dynavContagemresultado_contadorfscod_Internalname;
            dynavContagemresultado_contadorfscod.CurrentValue = cgiGet( dynavContagemresultado_contadorfscod_Internalname);
            AV81ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfscod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)));
            AV82ColContadorFS = StringUtil.Upper( cgiGet( edtavColcontadorfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ColContadorFS", AV82ColContadorFS);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataosfm2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data OSFM2"}), 1, "vDATAOSFM2");
               GX_FocusControl = edtavDataosfm2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30DataOSFM2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DataOSFM2", context.localUtil.Format(AV30DataOSFM2, "99/99/99"));
            }
            else
            {
               AV30DataOSFM2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDataosfm2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DataOSFM2", context.localUtil.Format(AV30DataOSFM2, "99/99/99"));
            }
            AV58OSFM2 = cgiGet( edtavOsfm2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58OSFM2", AV58OSFM2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinutos2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinutos2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINUTOS2");
               GX_FocusControl = edtavMinutos2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49Minutos2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Minutos2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49Minutos2), 4, 0)));
            }
            else
            {
               AV49Minutos2 = (short)(context.localUtil.CToN( cgiGet( edtavMinutos2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Minutos2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49Minutos2), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_valorpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_valorpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_VALORPF");
               GX_FocusControl = edtavContagemresultado_valorpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultado_ValorPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV24ContagemResultado_ValorPF, 18, 5)));
            }
            else
            {
               AV24ContagemResultado_ValorPF = context.localUtil.CToN( cgiGet( edtavContagemresultado_valorpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV24ContagemResultado_ValorPF, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinhactrf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinhactrf_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHACTRF");
               GX_FocusControl = edtavPralinhactrf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV96PraLinhaCTRF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96PraLinhaCTRF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96PraLinhaCTRF), 4, 0)));
            }
            else
            {
               AV96PraLinhaCTRF = (short)(context.localUtil.CToN( cgiGet( edtavPralinhactrf_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96PraLinhaCTRF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96PraLinhaCTRF), 4, 0)));
            }
            AV13ColDemandas = StringUtil.Upper( cgiGet( edtavColdemandas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ColDemandas", AV13ColDemandas);
            AV159ColPFBFMCTRF = StringUtil.Upper( cgiGet( edtavColpfbfmctrf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV159ColPFBFMCTRF", AV159ColPFBFMCTRF);
            AV160ColPFLFMCTRF = StringUtil.Upper( cgiGet( edtavColpflfmctrf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV160ColPFLFMCTRF", AV160ColPFLFMCTRF);
            AV15ColPFBFS = StringUtil.Upper( cgiGet( edtavColpfbfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ColPFBFS", AV15ColPFBFS);
            AV16ColPFLFS = StringUtil.Upper( cgiGet( edtavColpflfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ColPFLFS", AV16ColPFLFS);
            AV11ColDeflator = StringUtil.Upper( cgiGet( edtavColdeflator_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ColDeflator", AV11ColDeflator);
            cmbavAcao.Name = cmbavAcao_Internalname;
            cmbavAcao.CurrentValue = cgiGet( cmbavAcao_Internalname);
            AV7Acao = cgiGet( cmbavAcao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Acao", AV7Acao);
            AV161OSFMCTRF = cgiGet( edtavOsfmctrf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV161OSFMCTRF", AV161OSFMCTRF);
            AV158Blob = cgiGet( edtavBlob_Internalname);
            AV128Aba = cgiGet( edtavAba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128Aba", AV128Aba);
            AV130ColFiltro = StringUtil.Upper( cgiGet( edtavColfiltro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ColFiltro", AV130ColFiltro);
            cmbavTipofiltro.Name = cmbavTipofiltro_Internalname;
            cmbavTipofiltro.CurrentValue = cgiGet( cmbavTipofiltro_Internalname);
            AV132TipoFiltro = cgiGet( cmbavTipofiltro_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132TipoFiltro", AV132TipoFiltro);
            AV133FiltroColuna = cgiGet( edtavFiltrocoluna_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133FiltroColuna", AV133FiltroColuna);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV156AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV156AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
            }
            AV134FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134FileName", AV134FileName);
            /* Read saved values. */
            nRC_GXsfl_373 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_373"), ",", "."));
            AV145DataSrvFatSel = cgiGet( "vDATASRVFATSEL");
            Innewwindow_Width = cgiGet( "INNEWWINDOW_Width");
            Innewwindow_Height = cgiGet( "INNEWWINDOW_Height");
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( "CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( "CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Confirmpanel2_Title = cgiGet( "CONFIRMPANEL2_Title");
            Confirmpanel2_Confirmationtext = cgiGet( "CONFIRMPANEL2_Confirmationtext");
            Confirmpanel2_Yesbuttoncaption = cgiGet( "CONFIRMPANEL2_Yesbuttoncaption");
            Confirmpanel2_Confirmtype = cgiGet( "CONFIRMPANEL2_Confirmtype");
            Confirmpanel2_Title = cgiGet( "CONFIRMPANEL2_Title");
            nRC_GXsfl_373 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_373"), ",", "."));
            nGXsfl_373_fel_idx = 0;
            while ( nGXsfl_373_fel_idx < nRC_GXsfl_373 )
            {
               nGXsfl_373_fel_idx = (short)(((subGridsdtdemandas_Islastpage==1)&&(nGXsfl_373_fel_idx+1>subGridsdtdemandas_Recordsperpage( )) ? 1 : nGXsfl_373_fel_idx+1));
               sGXsfl_373_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_3732( ) ;
               AV184GXV1 = nGXsfl_373_fel_idx;
               if ( ( AV93SDT_Demandas.Count >= AV184GXV1 ) && ( AV184GXV1 > 0 ) )
               {
                  AV93SDT_Demandas.CurrentItem = ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV158Blob)) )
               {
                  GXCCtl = "vBLOB_" + sGXsfl_373_fel_idx;
                  GXCCtlgxBlob = GXCCtl + "_gxBlob";
                  AV158Blob = cgiGet( GXCCtlgxBlob);
               }
            }
            if ( nGXsfl_373_fel_idx == 0 )
            {
               nGXsfl_373_idx = 1;
               sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
               SubsflControlProps_3732( ) ;
            }
            nGXsfl_373_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV158Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV158Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24BL2 */
         E24BL2 ();
         if (returnInSub) return;
      }

      protected void E24BL2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV74WWPContext) ;
         AV156AreaTrabalho_Codigo = AV74WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV134FileName)) )
         {
            bttCarregar_Caption = "Carregar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         tblTbldadosms_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosms_Visible), 5, 0)));
         tblTblconftrf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblconftrf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblconftrf_Visible), 5, 0)));
         tblTblcast_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
         bttCarregar_Caption = "Analisar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         tblTablegrid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablegrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablegrid_Visible), 5, 0)));
         edtavServico_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_codigo_Visible), 5, 0)));
         edtavContratadasrv_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadasrv_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadasrv_codigo_Visible), 5, 0)));
         edtavOsservico_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOsservico_codigo_Visible), 5, 0)));
         AV30DataOSFM2 = context.localUtil.CToD( "01/12/14", 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DataOSFM2", context.localUtil.Format(AV30DataOSFM2, "99/99/99"));
         AV49Minutos2 = 10;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Minutos2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49Minutos2), 4, 0)));
         AV24ContagemResultado_ValorPF = 0.5m;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV24ContagemResultado_ValorPF, 18, 5)));
         AV12ColDem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ColDem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ColDem), 4, 0)));
         AV14ColPB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ColPB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ColPB), 4, 0)));
         AV17ColPL = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ColPL", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ColPL), 4, 0)));
         AV60PraLinha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60PraLinha), 4, 0)));
         AV153Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153Agrupador", AV153Agrupador);
         AV175CalcularPrazo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175CalcularPrazo", AV175CalcularPrazo);
         edtavDataentrega_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDataentrega_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDataentrega_Enabled), 5, 0)));
         AV93SDT_Demandas.Clear();
         gx_BV373 = true;
         GX_FocusControl = cmbavContratada_codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         AV155SDT_ContagemResultadoEvidencias.Clear();
         tblTblanexos_Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblanexos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblanexos_Visible), 5, 0)));
         edtavAreatrabalho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_codigo_Visible), 5, 0)));
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"if(nome.split('\\\\').length>1) nome = nome.split('\\\\')[nome.split('\\\\').length-1]; else nome = nome.split('/')[nome.split('/').length-1];";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         GXt_boolean1 = AV181RequerOrigem;
         GXt_int2 = AV74WWPContext.gxTpr_Areatrabalho_codigo;
         new prc_requerorigem(context ).execute( ref  GXt_int2, out  GXt_boolean1) ;
         AV74WWPContext.gxTpr_Areatrabalho_codigo = GXt_int2;
         AV181RequerOrigem = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV181RequerOrigem", AV181RequerOrigem);
         /* Execute user subroutine: 'REQUISITANTE' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E25BL2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV74WWPContext) ;
         AV156AreaTrabalho_Codigo = AV74WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
         GXt_boolean1 = AV95OSAutomatica;
         new prc_osautomatica(context ).execute( ref  AV156AreaTrabalho_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
         AV95OSAutomatica = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95OSAutomatica", AV95OSAutomatica);
         if ( AV95OSAutomatica )
         {
            edtavOsfm_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOsfm_Enabled), 5, 0)));
            edtavColosfm_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColosfm_Enabled), 5, 0)));
            lblTbosfm_Caption = "N� da OS: AUTOMATICA";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbosfm_Internalname, "Caption", lblTbosfm_Caption);
            lblTbcosfm_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcosfm_Internalname, "Caption", lblTbcosfm_Caption);
         }
         AV125SelServico_Codigo = AV120Servico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0)));
         AV135SelContratadaSrv_Codigo = AV136ContratadaSrv_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135SelContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)));
         AV144SelOsServico_Codigo = AV126OSServico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV144SelOsServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0)));
         AV142DataSrvFat = AV145DataSrvFatSel;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142DataSrvFat", AV142DataSrvFat);
         chkavNotificar.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNotificar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavNotificar.Visible), 5, 0)));
         AV154Notificar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154Notificar", AV154Notificar);
         AV12ColDem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ColDem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ColDem), 4, 0)));
         AV14ColPB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ColPB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ColPB), 4, 0)));
         AV17ColPL = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ColPL", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ColPL), 4, 0)));
         tblTbldadosdmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosdmn_Visible), 5, 0)));
         tblTbldadosms_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosms_Visible), 5, 0)));
         tblTblconftrf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblconftrf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblconftrf_Visible), 5, 0)));
         tblTblcontratada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
         tblTblcontadorfm_Visible = ((!(String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111ColPFBFMCAST))))||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "CTRF")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
         tblTblcast_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
         subGridsdtdemandas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridsdtdemandasContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridsdtdemandas_Visible), 5, 0)));
         tblTblsrvvnc_Visible = (!AV143SrvParaFtr&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvvnc_Visible), 5, 0)));
         tblTblfiltrocoluna_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
         chkavSrvparaftr.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSrvparaftr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSrvparaftr.Visible), 5, 0)));
         lblTbehvalidacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbehvalidacao_Visible), 5, 0)));
         cmbavContagemresultado_ehvalidacao.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_ehvalidacao.Visible), 5, 0)));
         dynavContratadaorigem_codigo.Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem_codigo.Visible), 5, 0)));
         lblTbcontorigem_Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontorigem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontorigem_Visible), 5, 0)));
         lblTbservico_Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICCT")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbservico_Visible), 5, 0)));
         dynavSelservico_codigo.Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICCT")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Visible), 5, 0)));
         lblTbcontpres_Caption = "Contratada Origem:";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
         tblTblanexos_Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblanexos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblanexos_Visible), 5, 0)));
         lblTbcontrato_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontrato_Visible), 5, 0)));
         cmbavContratoservicos_codigo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicos_codigo.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV56OQImportar, "TA") == 0 )
         {
            bttCarregar_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCarregar_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "IDMN") == 0 )
         {
            subGridsdtdemandas_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridsdtdemandasContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridsdtdemandas_Visible), 5, 0)));
            tblTbldadosdmn_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosdmn_Visible), 5, 0)));
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            tblTblfiltrocoluna_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
            tblTblcontadorfm_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            tblTblcontadorfs_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( AV15ColPFBFS)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfs_Visible), 5, 0)));
            chkavSrvparaftr.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSrvparaftr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSrvparaftr.Visible), 5, 0)));
            tblTblsrvfat_Visible = (AV143SrvParaFtr ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvfat_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvfat_Visible), 5, 0)));
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            lblTbcontorigem_Caption = "Contratada Origem";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontorigem_Internalname, "Caption", lblTbcontorigem_Caption);
            chkavNotificar.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNotificar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavNotificar.Visible), 5, 0)));
            AV154Notificar = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154Notificar", AV154Notificar);
            lblTbcontrato_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontrato_Visible), 5, 0)));
            cmbavContratoservicos_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicos_codigo.Visible), 5, 0)));
            bttCarregar_Caption = "Analisar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
            AV93SDT_Demandas.FromXml(AV73WebSession.Get("SDTDemandas"), "");
            gx_BV373 = true;
            AV62Qtde = (short)(AV93SDT_Demandas.Count);
         }
         else if ( ( StringUtil.StrCmp(AV56OQImportar, "ICNT") == 0 ) || ( StringUtil.StrCmp(AV56OQImportar, "ICCT") == 0 ) )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            tblTblcontadorfm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            tblTblpffs_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffs_Visible), 5, 0)));
            tblTblcast_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
            bttCarregar_Caption = "Importar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            tblTblfiltrocoluna_Visible = ((StringUtil.StrCmp(AV56OQImportar, "ICCT")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
            AV93SDT_Demandas.FromXml(AV73WebSession.Get("SDTDemandas"), "");
            gx_BV373 = true;
            AV62Qtde = (short)(AV93SDT_Demandas.Count);
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "PDMS") == 0 )
         {
            tblTbldadosms_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosms_Visible), 5, 0)));
            lblTbehvalidacao_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbehvalidacao_Visible), 5, 0)));
            cmbavContagemresultado_ehvalidacao.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_ehvalidacao.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "CNF1") == 0 )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            bttCarregar_Caption = "Conferir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "SGCD") == 0 )
         {
            tblTblpffs_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffs_Visible), 5, 0)));
            tblTblcast_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            bttCarregar_Caption = "Conferir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "CTRF") == 0 )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            if ( ! (0==AV74WWPContext.gxTpr_Contratada_codigo) )
            {
               AV127Contratada_Codigo = AV74WWPContext.gxTpr_Contratada_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            }
            lblTbservico_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbservico_Visible), 5, 0)));
            edtavServico_codigo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_codigo_Visible), 5, 0)));
            tblTblconftrf_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblconftrf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblconftrf_Visible), 5, 0)));
            tblTblcontadorfm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            bttCarregar_Caption = "Conferir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         bttImportar_Visible = (AV74WWPContext.gxTpr_Insert&&((AV93SDT_Demandas.Count>0)||(AV46Ln>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttImportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttImportar_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74WWPContext", AV74WWPContext);
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Values", dynavSelservico_codigo.ToJavascriptSource());
         dynavSelcontratadasrv_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratadasrv_codigo_Internalname, "Values", dynavSelcontratadasrv_codigo.ToJavascriptSource());
         dynavSelosservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelosservico_codigo_Internalname, "Values", dynavSelosservico_codigo.ToJavascriptSource());
         cmbavDatasrvfat.CurrentValue = StringUtil.RTrim( AV142DataSrvFat);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDatasrvfat_Internalname, "Values", cmbavDatasrvfat.ToJavascriptSource());
         cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", cmbavContratada_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93SDT_Demandas", AV93SDT_Demandas);
      }

      protected void E13BL2( )
      {
         /* Selservico_codigo_Click Routine */
         AV120Servico_Codigo = AV125SelServico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120Servico_Codigo), 6, 0)));
         /* Execute user subroutine: 'CARREGACONTRATO' */
         S122 ();
         if (returnInSub) return;
         GXt_char3 = AV165TipoDias;
         new prc_tipodediasdoprazo(context ).execute( ref  AV176ContratoServicos_Codigo, out  GXt_char3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
         AV165TipoDias = GXt_char3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165TipoDias", AV165TipoDias);
         cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", cmbavContratoservicos_codigo.ToJavascriptSource());
      }

      protected void E22BL2( )
      {
         /* Srvparaftr_Click Routine */
         dynavContratadaorigem_codigo.Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem_codigo.Visible), 5, 0)));
         lblTbcontorigem_Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontorigem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontorigem_Visible), 5, 0)));
         tblTblsrvfat_Visible = (AV143SrvParaFtr ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvfat_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvfat_Visible), 5, 0)));
         tblTblsrvvnc_Visible = ((!AV143SrvParaFtr)&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvvnc_Visible), 5, 0)));
         lblTbdtcnt_Visible = (!AV143SrvParaFtr ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdtcnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbdtcnt_Visible), 5, 0)));
         lblTbcoldtcnt_Visible = (!AV143SrvParaFtr ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcoldtcnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcoldtcnt_Visible), 5, 0)));
         edtavDatacnt_Visible = (!AV143SrvParaFtr ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatacnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatacnt_Visible), 5, 0)));
         edtavColdatacnt_Visible = (!AV143SrvParaFtr ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColdatacnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColdatacnt_Visible), 5, 0)));
         if ( AV143SrvParaFtr )
         {
            lblTbservico_Caption = "Servi�o executado:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
            if ( AV95OSAutomatica )
            {
               AV121StatusDmn = "C";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
            }
            else
            {
               AV121StatusDmn = "R";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
            }
         }
         else
         {
            lblTbservico_Caption = "Servi�o para Contador FM:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
            AV121StatusDmn = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121StatusDmn", AV121StatusDmn);
            AV150ContratadaOrigem_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV150ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0)));
         }
         dynavContratadaorigem_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Values", dynavContratadaorigem_codigo.ToJavascriptSource());
         cmbavStatusdmn.CurrentValue = StringUtil.RTrim( AV121StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdmn_Internalname, "Values", cmbavStatusdmn.ToJavascriptSource());
      }

      protected void E14BL2( )
      {
         /* Selosservico_codigo_Click Routine */
         AV126OSServico_Codigo = AV144SelOsServico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
         if ( (0==AV144SelOsServico_Codigo) )
         {
            AV136ContratadaSrv_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136ContratadaSrv_Codigo), 6, 0)));
            AV126OSServico_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
         }
         else
         {
            /* Execute user subroutine: 'CARREGACONTRATOOS' */
            S132 ();
            if (returnInSub) return;
         }
         cmbavContratoservicosos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosos_codigo_Internalname, "Values", cmbavContratoservicosos_codigo.ToJavascriptSource());
      }

      protected void E15BL2( )
      {
         /* Oqimportar_Click Routine */
         AV93SDT_Demandas.Clear();
         gx_BV373 = true;
         AV12ColDem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ColDem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ColDem), 4, 0)));
         AV14ColPB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ColPB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ColPB), 4, 0)));
         AV17ColPL = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ColPL", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ColPL), 4, 0)));
         tblTbldadosdmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosdmn_Visible), 5, 0)));
         tblTbldadosms_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosms_Visible), 5, 0)));
         tblTblconftrf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblconftrf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblconftrf_Visible), 5, 0)));
         tblTbldadosdmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosdmn_Visible), 5, 0)));
         tblTblcontadorfm_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM))||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICCT")==0)||(StringUtil.StrCmp(AV56OQImportar, "CTRF")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
         tblTblcontratada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
         tblTblcast_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
         tblTblsrvvnc_Visible = (!AV143SrvParaFtr&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvvnc_Visible), 5, 0)));
         AV143SrvParaFtr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV143SrvParaFtr", AV143SrvParaFtr);
         chkavSrvparaftr.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSrvparaftr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSrvparaftr.Visible), 5, 0)));
         tblTblsrvfat_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvfat_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvfat_Visible), 5, 0)));
         tblTblfiltrocoluna_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
         lblTbehvalidacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbehvalidacao_Visible), 5, 0)));
         cmbavContagemresultado_ehvalidacao.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_ehvalidacao.Visible), 5, 0)));
         dynavContratadaorigem_codigo.Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem_codigo.Visible), 5, 0)));
         lblTbcontorigem_Visible = ((AV181RequerOrigem&&(StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontorigem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontorigem_Visible), 5, 0)));
         lblTbservico_Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICCT")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbservico_Visible), 5, 0)));
         dynavSelservico_codigo.Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICNT")==0)||(StringUtil.StrCmp(AV56OQImportar, "ICCT")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Visible), 5, 0)));
         lblTbcontpres_Caption = "Contratada Origem:";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
         tblTblanexos_Visible = (((StringUtil.StrCmp(AV56OQImportar, "IDMN")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblanexos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblanexos_Visible), 5, 0)));
         chkavNotificar.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNotificar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavNotificar.Visible), 5, 0)));
         AV154Notificar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154Notificar", AV154Notificar);
         lblTbcontrato_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontrato_Visible), 5, 0)));
         cmbavContratoservicos_codigo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicos_codigo.Visible), 5, 0)));
         subGridsdtdemandas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridsdtdemandasContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridsdtdemandas_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV56OQImportar, "TA") == 0 )
         {
            bttImportar_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttImportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttImportar_Visible), 5, 0)));
            bttCarregar_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCarregar_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "IDMN") == 0 )
         {
            subGridsdtdemandas_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridsdtdemandasContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridsdtdemandas_Visible), 5, 0)));
            tblTbldadosdmn_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosdmn_Visible), 5, 0)));
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            tblTblcontadorfm_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            tblTblcontadorfs_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( AV15ColPFBFS)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfs_Visible), 5, 0)));
            chkavSrvparaftr.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSrvparaftr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSrvparaftr.Visible), 5, 0)));
            tblTblfiltrocoluna_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            chkavNotificar.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNotificar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavNotificar.Visible), 5, 0)));
            lblTbcontrato_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontrato_Visible), 5, 0)));
            cmbavContratoservicos_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicos_codigo.Visible), 5, 0)));
            AV154Notificar = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154Notificar", AV154Notificar);
         }
         else if ( ( StringUtil.StrCmp(AV56OQImportar, "ICNT") == 0 ) || ( StringUtil.StrCmp(AV56OQImportar, "ICCT") == 0 ) )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            tblTblcontadorfm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            tblTblpffs_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffs_Visible), 5, 0)));
            tblTblcast_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
            bttCarregar_Caption = "Importar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            tblTblfiltrocoluna_Visible = ((StringUtil.StrCmp(AV56OQImportar, "ICCT")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfiltrocoluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfiltrocoluna_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "PDMS") == 0 )
         {
            tblTbldadosms_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbldadosms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbldadosms_Visible), 5, 0)));
            lblTbehvalidacao_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbehvalidacao_Visible), 5, 0)));
            cmbavContagemresultado_ehvalidacao.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_ehvalidacao.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "CNF1") == 0 )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            bttCarregar_Caption = "Conferir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "SGCD") == 0 )
         {
            tblTblpffs_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffs_Visible), 5, 0)));
            tblTblcast_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcast_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcast_Visible), 5, 0)));
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            bttCarregar_Caption = "Conferir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "CTRF") == 0 )
         {
            tblTblcontratada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontratada_Visible), 5, 0)));
            lblTbcontpres_Caption = "Prestadora:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontpres_Internalname, "Caption", lblTbcontpres_Caption);
            if ( ! (0==AV74WWPContext.gxTpr_Contratada_codigo) )
            {
               AV127Contratada_Codigo = AV74WWPContext.gxTpr_Contratada_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            }
            lblTbservico_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbservico_Visible), 5, 0)));
            edtavServico_codigo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_codigo_Visible), 5, 0)));
            tblTblcontadorfm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcontadorfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcontadorfm_Visible), 5, 0)));
            tblTblconftrf_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblconftrf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblconftrf_Visible), 5, 0)));
            bttCarregar_Caption = "Atualizar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
            GX_FocusControl = edtavPralinhactrf_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93SDT_Demandas", AV93SDT_Demandas);
         nGXsfl_373_bak_idx = nGXsfl_373_idx;
         gxgrGridsdtdemandas_refresh( AV120Servico_Codigo, AV136ContratadaSrv_Codigo, AV126OSServico_Codigo, AV145DataSrvFatSel, AV97ColPFBFM, AV111ColPFBFMCAST, AV56OQImportar, AV143SrvParaFtr, AV181RequerOrigem, AV15ColPFBFS, AV93SDT_Demandas, AV46Ln) ;
         nGXsfl_373_idx = nGXsfl_373_bak_idx;
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
         cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", cmbavContratada_codigo.ToJavascriptSource());
      }

      protected void E16BL2( )
      {
         /* 'Enter' Routine */
         AV8Arquivo = AV158Blob;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Arquivo", AV8Arquivo);
         AV134FileName = StringUtil.Substring( AV134FileName, StringUtil.StringSearchRev( AV134FileName, "\\", -1)+1, 255);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134FileName", AV134FileName);
         AV134FileName = StringUtil.Substring( AV134FileName, 1, StringUtil.StringSearchRev( AV134FileName, ".", -1)-1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134FileName", AV134FileName);
         AV55Ok = true;
         if ( AV127Contratada_Codigo > 0 )
         {
            AV180GestorDaContratada = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV180GestorDaContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV180GestorDaContratada), 6, 0)));
            /* Using cursor H00BL8 */
            pr_default.execute(6, new Object[] {AV127Contratada_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00BL8_A1078ContratoGestor_ContratoCod[0];
               A1136ContratoGestor_ContratadaCod = H00BL8_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00BL8_n1136ContratoGestor_ContratadaCod[0];
               A1079ContratoGestor_UsuarioCod = H00BL8_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00BL8_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00BL8_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = H00BL8_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00BL8_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00BL8_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00BL8_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               if ( ! A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV180GestorDaContratada = A1079ContratoGestor_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV180GestorDaContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV180GestorDaContratada), 6, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         if ( ( StringUtil.StrCmp(AV56OQImportar, "CNF1") == 0 ) || ( StringUtil.StrCmp(AV56OQImportar, "SGCD") == 0 ) )
         {
            if ( (0==AV127Contratada_Codigo) )
            {
               GX_msglist.addItem("Falta selecionar a Contratada!");
               AV55Ok = false;
            }
         }
         else if ( StringUtil.StrCmp(AV56OQImportar, "IDMN") == 0 )
         {
            AV93SDT_Demandas.Clear();
            gx_BV373 = true;
            if ( (0==AV127Contratada_Codigo) )
            {
               GX_msglist.addItem("Falta selecionar a Prestadora das OS!");
               AV55Ok = false;
            }
            else if ( (0==AV180GestorDaContratada) )
            {
               GX_msglist.addItem("A Prestadora selecionada n�o tem Gestor cadastrado!");
               AV55Ok = false;
            }
            else if ( (0==AV176ContratoServicos_Codigo) )
            {
               GX_msglist.addItem("Falta selecionar o Contrato da Prestadora!");
               AV55Ok = false;
            }
            else if ( AV181RequerOrigem && (0==AV150ContratadaOrigem_Codigo) && ( dynavContratadaorigem_codigo.ItemCount > 1 ) )
            {
               GX_msglist.addItem("Falta selecionar a Contratada Origem das OS Ref!");
               AV55Ok = false;
            }
            else if ( (0==AV60PraLinha) )
            {
               GX_msglist.addItem("Falta preencher a Primeira linha de dados!");
               AV55Ok = false;
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166ColProjeto)) && String.IsNullOrEmpty(StringUtil.RTrim( AV76ColNomeSistema)) )
            {
               GX_msglist.addItem("Prencha a coluna do Projeto ou Sistema para saber de onde importar os Sistemas!");
               AV55Ok = false;
            }
            else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166ColProjeto)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV76ColNomeSistema)) )
            {
               GX_msglist.addItem("Prencha apenas Projeto ou Sistema para saber de qual coluna importar os Sistemas!");
               AV55Ok = false;
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80ColDescricao)) )
            {
               GX_msglist.addItem("Falta preencher a coluna do Assunto!");
               AV55Ok = false;
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75ColOSFS)) )
            {
               GX_msglist.addItem("Falta preencher a coluna da OS Ref!");
               AV55Ok = false;
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76ColNomeSistema)) )
            {
               GX_msglist.addItem("Falta preencher a coluna do Nome do Sistema!");
               AV55Ok = false;
            }
            else if ( ! AV95OSAutomatica && String.IsNullOrEmpty(StringUtil.RTrim( AV57OSFM)) && String.IsNullOrEmpty(StringUtil.RTrim( AV77ColOSFM)) )
            {
               GX_msglist.addItem("Falta preencher a OS ou coluna da OS!");
               AV55Ok = false;
            }
            else if ( (DateTime.MinValue==AV29DataOSFM) && String.IsNullOrEmpty(StringUtil.RTrim( AV78ColDataDmn)) )
            {
               GX_msglist.addItem("Falta preencher a Data ou coluna da Data da OS!");
               AV55Ok = false;
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83ContagemResultado_Link)) && String.IsNullOrEmpty(StringUtil.RTrim( AV84ColLink)) )
            {
               GX_msglist.addItem("Falta preencher o Link ou a coluna do Link!");
               AV55Ok = false;
            }
            else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57OSFM)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77ColOSFM)) )
            {
               GX_msglist.addItem("Indique apenas um campo: OS ou coluna da OS!");
               AV55Ok = false;
            }
            else if ( ! (DateTime.MinValue==AV29DataOSFM) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ColDataDmn)) )
            {
               GX_msglist.addItem("Indique apenas um campo: Data ou coluna da Data da OS!");
               AV55Ok = false;
            }
            else if ( ! (DateTime.MinValue==AV146DataCnt) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ColDataCnt)) )
            {
               GX_msglist.addItem("Indique apenas um campo: Data ou coluna da Data da contagem!");
               AV55Ok = false;
            }
            else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79ContagemResultado_Observacao)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV170ColObservacao)) )
            {
               GX_msglist.addItem("Indique apenas um campo: Descri��o complementar ou coluna da Descri��o complementar!");
               AV55Ok = false;
            }
            else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ContagemResultado_Link)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV84ColLink)) )
            {
               GX_msglist.addItem("Indique apenas um campo: Link ou coluna do Link!");
               AV55Ok = false;
            }
            else if ( ( String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ColPFLFM)) ) || ( String.IsNullOrEmpty(StringUtil.RTrim( AV98ColPFLFM)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) ) )
            {
               GX_msglist.addItem("Indique ambas colunas dos valores da OS ou nenhuma!");
               AV55Ok = false;
            }
            else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) && ! (0==AV18ContagemResultado_ContadorFMCod) )
            {
               GX_msglist.addItem("Indique o Contador da OS ou a coluna do Contador da OS!");
               AV55Ok = false;
            }
            else if ( StringUtil.StringSearch( AV8Arquivo, "xlsx", 1) == 0 )
            {
               GX_msglist.addItem("Arquivo esperado para importar: Excel com extens�o .xlsx!");
               AV55Ok = false;
            }
            else if ( AV143SrvParaFtr )
            {
               if ( (0==AV125SelServico_Codigo) )
               {
                  GX_msglist.addItem("Falta selecionar o Servi�o executado para faturar!");
                  AV55Ok = false;
               }
               else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121StatusDmn)) )
               {
                  GX_msglist.addItem("Indique o Status da demanda que sera importada para Faturar!");
                  AV55Ok = false;
               }
               else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97ColPFBFM)) )
               {
                  GX_msglist.addItem("Indique a coluna dos PF FM das contagens para Faturar!");
                  AV55Ok = false;
               }
               else if ( (0==AV18ContagemResultado_ContadorFMCod) )
               {
                  GX_msglist.addItem("Indique quem executou o servi�o para Faturar!");
                  AV55Ok = false;
               }
            }
            else if ( ! AV143SrvParaFtr )
            {
               if ( (0==AV125SelServico_Codigo) )
               {
                  GX_msglist.addItem("Falta selecionar o Servi�o a ser executado nas OS!");
                  AV55Ok = false;
               }
               else if ( ! (0==AV136ContratadaSrv_Codigo) && (0==AV126OSServico_Codigo) )
               {
                  GX_msglist.addItem("Falta selecionar o Servi�o a ser criado como 2� OS vinculada!");
                  AV55Ok = false;
               }
               else if ( ( AV135SelContratadaSrv_Codigo > 0 ) && (0==AV144SelOsServico_Codigo) )
               {
                  GX_msglist.addItem("Falta selecionar o Servi�o vinculado!");
                  AV55Ok = false;
               }
               else if ( ( AV144SelOsServico_Codigo > 0 ) && (0==AV177ContratoServicosOS_Codigo) )
               {
                  GX_msglist.addItem("Falta selecionar o Contrato da Prestadora do servi�o vinculado!");
                  AV55Ok = false;
               }
               else if ( ( AV127Contratada_Codigo == AV136ContratadaSrv_Codigo ) && ( dynavSelcontratadasrv_codigo.ItemCount > 2 ) )
               {
                  GX_msglist.addItem("A Contratada Origem e a Prestadora do Servi�o vinculado s�o a mesma!");
                  AV55Ok = false;
               }
            }
         }
         if ( AV55Ok )
         {
            if ( StringUtil.StrCmp(AV56OQImportar, "CTRF") == 0 )
            {
               AV60PraLinha = AV96PraLinhaCTRF;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60PraLinha), 4, 0)));
               AV12ColDem = (short)(StringUtil.Asc( AV13ColDemandas)-64);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ColDem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ColDem), 4, 0)));
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV15ColPFBFS, 2, 1))) )
               {
                  AV14ColPB = (short)(StringUtil.Asc( AV15ColPFBFS)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ColPB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ColPB), 4, 0)));
               }
               else
               {
                  AV14ColPB = (short)(StringUtil.Asc( StringUtil.Substring( AV15ColPFBFS, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ColPB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ColPB), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV16ColPFLFS, 2, 1))) )
               {
                  AV17ColPL = (short)(StringUtil.Asc( AV16ColPFLFS)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ColPL", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ColPL), 4, 0)));
               }
               else
               {
                  AV17ColPL = (short)(StringUtil.Asc( StringUtil.Substring( AV16ColPFLFS, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ColPL", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ColPL), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV159ColPFBFMCTRF, 2, 1))) )
               {
                  AV99ColPFBFMn = (short)(StringUtil.Asc( AV159ColPFBFMCTRF)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
               }
               else
               {
                  AV99ColPFBFMn = (short)(StringUtil.Asc( StringUtil.Substring( AV159ColPFBFMCTRF, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV160ColPFLFMCTRF, 2, 1))) )
               {
                  AV100ColPFlFMn = (short)(StringUtil.Asc( AV160ColPFLFMCTRF)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
               }
               else
               {
                  AV100ColPFlFMn = (short)(StringUtil.Asc( StringUtil.Substring( AV160ColPFLFMCTRF, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
               }
            }
            AV31ErrCod = AV33ExcelDocument.Open(AV8Arquivo);
            if ( AV31ErrCod != 0 )
            {
               GX_msglist.addItem(AV33ExcelDocument.ErrDescription);
            }
            else
            {
               if ( StringUtil.StrCmp(AV56OQImportar, "ICNT") == 0 )
               {
                  AV102ColContadorFMn = (short)(StringUtil.Asc( AV101ColContadorFM)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ColContadorFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102ColContadorFMn), 4, 0)));
                  AV99ColPFBFMn = (short)(StringUtil.Asc( AV111ColPFBFMCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
                  AV100ColPFlFMn = (short)(StringUtil.Asc( AV112ColPFLFMCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
                  AV118ColPFBFSn = (short)(StringUtil.Asc( AV109ColPFBFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ColPFBFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV118ColPFBFSn), 4, 0)));
                  AV119ColPFlFSn = (short)(StringUtil.Asc( AV110ColPFLFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ColPFlFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV119ColPFlFSn), 4, 0)));
                  AV148ColDataCntn = (short)(StringUtil.Asc( AV147ColDataCnt)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ColDataCntn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ColDataCntn), 4, 0)));
                  AV108ColDmnCASTn = (short)(StringUtil.Asc( AV114ColDmnCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108ColDmnCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108ColDmnCASTn), 4, 0)));
                  AV31ErrCod = AV33ExcelDocument.Close();
                  /* Execute user subroutine: 'IMPORTARCONTAGEM' */
                  S142 ();
                  if (returnInSub) return;
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "ICCT") == 0 )
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101ColContadorFM)) )
                  {
                     AV102ColContadorFMn = (short)(StringUtil.Asc( AV101ColContadorFM)-64);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ColContadorFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102ColContadorFMn), 4, 0)));
                  }
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV111ColPFBFMCAST, 2, 1))) )
                  {
                     AV99ColPFBFMn = (short)(StringUtil.Asc( AV111ColPFBFMCAST)-64);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
                  }
                  else
                  {
                     AV99ColPFBFMn = (short)(StringUtil.Asc( StringUtil.Substring( AV111ColPFBFMCAST, 2, 1))-64+26);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
                  }
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV112ColPFLFMCAST, 2, 1))) )
                  {
                     AV100ColPFlFMn = (short)(StringUtil.Asc( AV112ColPFLFMCAST)-64);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
                  }
                  else
                  {
                     AV100ColPFlFMn = (short)(StringUtil.Asc( StringUtil.Substring( AV112ColPFLFMCAST, 2, 1))-64+26);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
                  }
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV173ColParecer, 2, 1))) )
                  {
                     AV174ColParecern = (short)(StringUtil.Asc( AV173ColParecer)-64);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174ColParecern", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174ColParecern), 4, 0)));
                  }
                  else
                  {
                     AV174ColParecern = (short)(StringUtil.Asc( StringUtil.Substring( AV173ColParecer, 2, 1))-64+26);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174ColParecern", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174ColParecern), 4, 0)));
                  }
                  AV118ColPFBFSn = (short)(StringUtil.Asc( AV109ColPFBFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ColPFBFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV118ColPFBFSn), 4, 0)));
                  AV119ColPFlFSn = (short)(StringUtil.Asc( AV110ColPFLFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ColPFlFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV119ColPFlFSn), 4, 0)));
                  AV148ColDataCntn = (short)(StringUtil.Asc( AV147ColDataCnt)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ColDataCntn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ColDataCntn), 4, 0)));
                  AV108ColDmnCASTn = (short)(StringUtil.Asc( AV114ColDmnCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108ColDmnCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108ColDmnCASTn), 4, 0)));
                  AV31ErrCod = AV33ExcelDocument.Close();
                  /* Execute user subroutine: 'IMPORTARCONTAGEM' */
                  S142 ();
                  if (returnInSub) return;
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "SGCD") == 0 )
               {
                  AV108ColDmnCASTn = (short)(StringUtil.Asc( AV114ColDmnCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108ColDmnCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108ColDmnCASTn), 4, 0)));
                  AV107ColDataCASTn = (short)(StringUtil.Asc( AV113ColDataCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ColDataCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ColDataCASTn), 4, 0)));
                  AV103ColPFBFSCASTn = (short)(StringUtil.Asc( AV109ColPFBFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ColPFBFSCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103ColPFBFSCASTn), 4, 0)));
                  AV104ColPFLFSCASTn = (short)(StringUtil.Asc( AV110ColPFLFSCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104ColPFLFSCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104ColPFLFSCASTn), 4, 0)));
                  AV105ColPFBFMCASTn = (short)(StringUtil.Asc( AV111ColPFBFMCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ColPFBFMCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ColPFBFMCASTn), 4, 0)));
                  AV106ColPFLFMCASTn = (short)(StringUtil.Asc( AV112ColPFLFMCAST)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ColPFLFMCASTn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106ColPFLFMCASTn), 4, 0)));
                  AV31ErrCod = AV33ExcelDocument.Close();
                  /* Execute user subroutine: 'CONFERENCIASGSD' */
                  S152 ();
                  if (returnInSub) return;
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "CTRF") == 0 )
               {
                  AV31ErrCod = AV33ExcelDocument.Close();
                  AV60PraLinha = AV96PraLinhaCTRF;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60PraLinha), 4, 0)));
                  /* Execute user subroutine: 'RELATORIOCONFERENCIA' */
                  S162 ();
                  if (returnInSub) return;
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "CNF1") == 0 )
               {
                  AV31ErrCod = AV33ExcelDocument.Close();
                  /* Execute user subroutine: 'RELATORIOCONFERENCIA' */
                  S162 ();
                  if (returnInSub) return;
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "TST") == 0 )
               {
                  AV31ErrCod = AV33ExcelDocument.Close();
                  new prc_sistemacoordenacaoupd(context ).execute( ref  AV8Arquivo, ref  AV156AreaTrabalho_Codigo) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Arquivo", AV8Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "TA") == 0 )
               {
                  AV46Ln = 2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Ln", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Ln), 4, 0)));
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ExcelDocument.get_Cells(AV46Ln, 1, 1, 1).Text)) && ( AV46Ln < 81 ) )
                  {
                     AV71Tmp_File = new SdtTmp_File(context);
                     AV71Tmp_File.gxTpr_File_usuariocod = AV74WWPContext.gxTpr_Userid;
                     AV71Tmp_File.gxTpr_File_row = AV46Ln-1;
                     AV71Tmp_File.gxTpr_File_col1 = StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 1, 1, 1).Text);
                     AV71Tmp_File.gxTpr_File_col2 = StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 2, 1, 1).Text);
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text)) )
                     {
                        AV71Tmp_File.gxTpr_File_col3 = StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text);
                     }
                     AV71Tmp_File.Save();
                     AV46Ln = (short)(AV46Ln+1);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Ln", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Ln), 4, 0)));
                  }
                  if ( AV46Ln > 2 )
                  {
                     context.CommitDataStores( "WP_ImportFromFile");
                     context.DoAjaxRefresh();
                  }
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "IDMN") == 0 )
               {
                  if ( AV150ContratadaOrigem_Codigo > 0 )
                  {
                     Gx_msg = "";
                     AV200GXLvl549 = 0;
                     /* Using cursor H00BL9 */
                     pr_default.execute(7, new Object[] {AV150ContratadaOrigem_Codigo});
                     while ( (pr_default.getStatus(7) != 101) )
                     {
                        A69ContratadaUsuario_UsuarioCod = H00BL9_A69ContratadaUsuario_UsuarioCod[0];
                        A66ContratadaUsuario_ContratadaCod = H00BL9_A66ContratadaUsuario_ContratadaCod[0];
                        A1228ContratadaUsuario_AreaTrabalhoCod = H00BL9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                        n1228ContratadaUsuario_AreaTrabalhoCod = H00BL9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                        A1394ContratadaUsuario_UsuarioAtivo = H00BL9_A1394ContratadaUsuario_UsuarioAtivo[0];
                        n1394ContratadaUsuario_UsuarioAtivo = H00BL9_n1394ContratadaUsuario_UsuarioAtivo[0];
                        A43Contratada_Ativo = H00BL9_A43Contratada_Ativo[0];
                        n43Contratada_Ativo = H00BL9_n43Contratada_Ativo[0];
                        A1394ContratadaUsuario_UsuarioAtivo = H00BL9_A1394ContratadaUsuario_UsuarioAtivo[0];
                        n1394ContratadaUsuario_UsuarioAtivo = H00BL9_n1394ContratadaUsuario_UsuarioAtivo[0];
                        A1228ContratadaUsuario_AreaTrabalhoCod = H00BL9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                        n1228ContratadaUsuario_AreaTrabalhoCod = H00BL9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                        A43Contratada_Ativo = H00BL9_A43Contratada_Ativo[0];
                        n43Contratada_Ativo = H00BL9_n43Contratada_Ativo[0];
                        AV200GXLvl549 = 1;
                        AV201GXLvl553 = 0;
                        /* Using cursor H00BL10 */
                        pr_default.execute(8, new Object[] {A66ContratadaUsuario_ContratadaCod});
                        while ( (pr_default.getStatus(8) != 101) )
                        {
                           A39Contratada_Codigo = H00BL10_A39Contratada_Codigo[0];
                           A74Contrato_Codigo = H00BL10_A74Contrato_Codigo[0];
                           n74Contrato_Codigo = H00BL10_n74Contrato_Codigo[0];
                           A92Contrato_Ativo = H00BL10_A92Contrato_Ativo[0];
                           n92Contrato_Ativo = H00BL10_n92Contrato_Ativo[0];
                           AV201GXLvl553 = 1;
                           AV202GXLvl556 = 0;
                           /* Using cursor H00BL11 */
                           pr_default.execute(9, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, A66ContratadaUsuario_ContratadaCod});
                           while ( (pr_default.getStatus(9) != 101) )
                           {
                              A1078ContratoGestor_ContratoCod = H00BL11_A1078ContratoGestor_ContratoCod[0];
                              A1136ContratoGestor_ContratadaCod = H00BL11_A1136ContratoGestor_ContratadaCod[0];
                              n1136ContratoGestor_ContratadaCod = H00BL11_n1136ContratoGestor_ContratadaCod[0];
                              A1079ContratoGestor_UsuarioCod = H00BL11_A1079ContratoGestor_UsuarioCod[0];
                              A1446ContratoGestor_ContratadaAreaCod = H00BL11_A1446ContratoGestor_ContratadaAreaCod[0];
                              n1446ContratoGestor_ContratadaAreaCod = H00BL11_n1446ContratoGestor_ContratadaAreaCod[0];
                              A1136ContratoGestor_ContratadaCod = H00BL11_A1136ContratoGestor_ContratadaCod[0];
                              n1136ContratoGestor_ContratadaCod = H00BL11_n1136ContratoGestor_ContratadaCod[0];
                              A1446ContratoGestor_ContratadaAreaCod = H00BL11_A1446ContratoGestor_ContratadaAreaCod[0];
                              n1446ContratoGestor_ContratadaAreaCod = H00BL11_n1446ContratoGestor_ContratadaAreaCod[0];
                              GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                              new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                              A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                              if ( ! A1135ContratoGestor_UsuarioEhContratante )
                              {
                                 AV202GXLvl556 = 1;
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                              pr_default.readNext(9);
                           }
                           pr_default.close(9);
                           if ( AV202GXLvl556 == 0 )
                           {
                              Gx_msg = "N�o existe Gestor na Contratada Origem!";
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(8);
                        }
                        pr_default.close(8);
                        if ( AV201GXLvl553 == 0 )
                        {
                           Gx_msg = "N�o existe Contrato na Contratada Origem!";
                        }
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(7);
                     }
                     pr_default.close(7);
                     if ( AV200GXLvl549 == 0 )
                     {
                        Gx_msg = "N�o existem Usu�rios cadastrados na Contratada Origem!";
                     }
                     if ( StringUtil.Len( Gx_msg) > 0 )
                     {
                        Confirmpanel_Confirmationtext = Gx_msg;
                        context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "ConfirmationText", Confirmpanel_Confirmationtext);
                        this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
                     }
                     else
                     {
                        /* Execute user subroutine: 'APOSCONFIRMPANELIDMN' */
                        S172 ();
                        if (returnInSub) return;
                     }
                  }
                  else
                  {
                     /* Execute user subroutine: 'APOSCONFIRMPANELIDMN' */
                     S172 ();
                     if (returnInSub) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV56OQImportar, "PDMS") == 0 )
               {
                  bttCarregar_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCarregar_Visible), 5, 0)));
                  /* Using cursor H00BL12 */
                  pr_default.execute(10, new Object[] {AV156AreaTrabalho_Codigo});
                  while ( (pr_default.getStatus(10) != 101) )
                  {
                     A427NaoConformidade_Nome = H00BL12_A427NaoConformidade_Nome[0];
                     A428NaoConformidade_AreaTrabalhoCod = H00BL12_A428NaoConformidade_AreaTrabalhoCod[0];
                     A426NaoConformidade_Codigo = H00BL12_A426NaoConformidade_Codigo[0];
                     AV53NaoValidada_Codigo = A426NaoConformidade_Codigo;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(10);
                  }
                  pr_default.close(10);
                  AV46Ln = 2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Ln", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Ln), 4, 0)));
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ExcelDocument.get_Cells(AV46Ln, 1, 1, 1).Text)) )
                  {
                     GX_msglist.addItem(AV33ExcelDocument.get_Cells(AV46Ln, 1, 1, 1).Text+","+AV33ExcelDocument.get_Cells(AV46Ln, 2, 1, 1).Text+","+AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text+","+AV33ExcelDocument.get_Cells(AV46Ln, 4, 1, 1).Text+","+AV33ExcelDocument.get_Cells(AV46Ln, 5, 1, 1).Text+","+AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text+","+StringUtil.Str( (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 7, 1, 1).Number), 10, 2)+","+StringUtil.Str( (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 8, 1, 1).Number), 10, 2)+","+StringUtil.Str( (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 9, 1, 1).Number), 10, 2)+","+StringUtil.Str( (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 10, 1, 1).Number), 10, 2));
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text)) )
                     {
                        AV69Sistema_Nome = "%" + StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text) + "%";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_Nome", AV69Sistema_Nome);
                        AV204GXLvl604 = 0;
                        lV69Sistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV69Sistema_Nome), "%", "");
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_Nome", AV69Sistema_Nome);
                        /* Using cursor H00BL13 */
                        pr_default.execute(11, new Object[] {lV69Sistema_Nome, AV156AreaTrabalho_Codigo});
                        while ( (pr_default.getStatus(11) != 101) )
                        {
                           A416Sistema_Nome = H00BL13_A416Sistema_Nome[0];
                           A130Sistema_Ativo = H00BL13_A130Sistema_Ativo[0];
                           A135Sistema_AreaTrabalhoCod = H00BL13_A135Sistema_AreaTrabalhoCod[0];
                           A127Sistema_Codigo = H00BL13_A127Sistema_Codigo[0];
                           A129Sistema_Sigla = H00BL13_A129Sistema_Sigla[0];
                           AV204GXLvl604 = 1;
                           AV122Sistema_Codigo = A127Sistema_Codigo;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(11);
                        }
                        pr_default.close(11);
                        if ( AV204GXLvl604 == 0 )
                        {
                           AV66Sistema = new SdtSistema(context);
                           AV66Sistema.gxTpr_Sistema_areatrabalhocod = AV156AreaTrabalho_Codigo;
                           AV66Sistema.gxTpr_Sistema_nome = StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 3, 1, 1).Text);
                           AV66Sistema.gxTpr_Sistema_sigla = StringUtil.Substring( AV66Sistema.gxTpr_Sistema_nome, 1, 15);
                           AV66Sistema.gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull();
                           AV66Sistema.gxTv_SdtSistema_Metodologia_codigo_SetNull();
                           AV66Sistema.Save();
                           context.CommitDataStores( "WP_ImportFromFile");
                           AV122Sistema_Codigo = AV66Sistema.gxTpr_Sistema_codigo;
                        }
                        AV127Contratada_Codigo = 0;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
                        AV26Contratada_Nome = "%" + StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 2, 1, 1).Text) + "%";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratada_Nome", AV26Contratada_Nome);
                        /* Execute user subroutine: 'SEARCHCONTRATADA' */
                        S182 ();
                        if (returnInSub) return;
                        if ( ! (0==AV127Contratada_Codigo) )
                        {
                           AV81ContagemResultado_ContadorFSCod = 0;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)));
                           AV42i = (short)(StringUtil.StringSearch( AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text, " ", 1)-1);
                           AV61PrimeiroNome = StringUtil.Substring( StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text), 1, AV42i) + "%";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61PrimeiroNome", AV61PrimeiroNome);
                           AV42i = (short)(StringUtil.StringSearchRev( AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text, " ", -1)+1);
                           AV72UltimoNome = "%" + StringUtil.Substring( StringUtil.Upper( AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text), AV42i, 20);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72UltimoNome", AV72UltimoNome);
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33ExcelDocument.get_Cells(AV46Ln, 6, 1, 1).Text)) )
                           {
                              AV20ContagemResultado_EhValidacao = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_EhValidacao", AV20ContagemResultado_EhValidacao);
                           }
                           else
                           {
                              lV61PrimeiroNome = StringUtil.PadR( StringUtil.RTrim( AV61PrimeiroNome), 50, "%");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61PrimeiroNome", AV61PrimeiroNome);
                              lV72UltimoNome = StringUtil.PadR( StringUtil.RTrim( AV72UltimoNome), 50, "%");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72UltimoNome", AV72UltimoNome);
                              /* Using cursor H00BL14 */
                              pr_default.execute(12, new Object[] {lV61PrimeiroNome, lV72UltimoNome, AV127Contratada_Codigo});
                              while ( (pr_default.getStatus(12) != 101) )
                              {
                                 A70ContratadaUsuario_UsuarioPessoaCod = H00BL14_A70ContratadaUsuario_UsuarioPessoaCod[0];
                                 n70ContratadaUsuario_UsuarioPessoaCod = H00BL14_n70ContratadaUsuario_UsuarioPessoaCod[0];
                                 A66ContratadaUsuario_ContratadaCod = H00BL14_A66ContratadaUsuario_ContratadaCod[0];
                                 A71ContratadaUsuario_UsuarioPessoaNom = H00BL14_A71ContratadaUsuario_UsuarioPessoaNom[0];
                                 n71ContratadaUsuario_UsuarioPessoaNom = H00BL14_n71ContratadaUsuario_UsuarioPessoaNom[0];
                                 A69ContratadaUsuario_UsuarioCod = H00BL14_A69ContratadaUsuario_UsuarioCod[0];
                                 A70ContratadaUsuario_UsuarioPessoaCod = H00BL14_A70ContratadaUsuario_UsuarioPessoaCod[0];
                                 n70ContratadaUsuario_UsuarioPessoaCod = H00BL14_n70ContratadaUsuario_UsuarioPessoaCod[0];
                                 A71ContratadaUsuario_UsuarioPessoaNom = H00BL14_A71ContratadaUsuario_UsuarioPessoaNom[0];
                                 n71ContratadaUsuario_UsuarioPessoaNom = H00BL14_n71ContratadaUsuario_UsuarioPessoaNom[0];
                                 AV81ContagemResultado_ContadorFSCod = A69ContratadaUsuario_UsuarioCod;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)));
                                 pr_default.readNext(12);
                              }
                              pr_default.close(12);
                              AV20ContagemResultado_EhValidacao = true;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_EhValidacao", AV20ContagemResultado_EhValidacao);
                           }
                           if ( ! (0==AV81ContagemResultado_ContadorFSCod) || ! AV20ContagemResultado_EhValidacao )
                           {
                              AV21ContagemResultado_NaoCnfDmnCod = 0;
                              if ( AV33ExcelDocument.get_Cells(AV46Ln, 9, 1, 1).Number + AV33ExcelDocument.get_Cells(AV46Ln, 10, 1, 1).Number == 0 )
                              {
                                 AV21ContagemResultado_NaoCnfDmnCod = AV53NaoValidada_Codigo;
                              }
                              GXt_char3 = StringUtil.Trim( AV33ExcelDocument.get_Cells(AV46Ln, 1, 1, 1).Text);
                              GXt_char4 = StringUtil.Trim( AV58OSFM2);
                              GXt_char5 = StringUtil.Substring( AV33ExcelDocument.get_Cells(AV46Ln, 4, 1, 1).Text, 1, 200);
                              GXt_decimal6 = (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 9, 1, 1).Number);
                              GXt_decimal7 = (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 10, 1, 1).Number);
                              GXt_decimal8 = (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 7, 1, 1).Number);
                              GXt_decimal9 = (decimal)(AV33ExcelDocument.get_Cells(AV46Ln, 8, 1, 1).Number);
                              GXt_int2 = (int)(AV33ExcelDocument.get_Cells(AV46Ln, 5, 1, 1).Number);
                              new prc_newrectrf(context ).execute( ref  AV156AreaTrabalho_Codigo, ref  GXt_char3, ref  GXt_char4, ref  AV127Contratada_Codigo, ref  AV120Servico_Codigo, ref  AV122Sistema_Codigo, ref  AV30DataOSFM2, ref  GXt_char5, ref  AV21ContagemResultado_NaoCnfDmnCod, ref  AV49Minutos2, ref  GXt_decimal6, ref  GXt_decimal7, ref  GXt_decimal8, ref  GXt_decimal9, ref  AV18ContagemResultado_ContadorFMCod, ref  AV81ContagemResultado_ContadorFSCod, ref  AV24ContagemResultado_ValorPF, ref  AV20ContagemResultado_EhValidacao, ref  GXt_int2, ref  AV176ContratoServicos_Codigo) ;
                              AV33ExcelDocument.get_Cells(AV46Ln, 9, 1, 1).Number = (double)(GXt_decimal6);
                              AV33ExcelDocument.get_Cells(AV46Ln, 10, 1, 1).Number = (double)(GXt_decimal7);
                              AV33ExcelDocument.get_Cells(AV46Ln, 7, 1, 1).Number = (double)(GXt_decimal8);
                              AV33ExcelDocument.get_Cells(AV46Ln, 8, 1, 1).Number = (double)(GXt_decimal9);
                              AV33ExcelDocument.get_Cells(AV46Ln, 5, 1, 1).Number = GXt_int2;
                           }
                        }
                     }
                     AV46Ln = (short)(AV46Ln+1);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Ln", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Ln), 4, 0)));
                  }
               }
               AV33ExcelDocument.Close();
            }
         }
         else
         {
            GX_FocusControl = cmbavOqimportar_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93SDT_Demandas", AV93SDT_Demandas);
         nGXsfl_373_bak_idx = nGXsfl_373_idx;
         gxgrGridsdtdemandas_refresh( AV120Servico_Codigo, AV136ContratadaSrv_Codigo, AV126OSServico_Codigo, AV145DataSrvFatSel, AV97ColPFBFM, AV111ColPFBFMCAST, AV56OQImportar, AV143SrvParaFtr, AV181RequerOrigem, AV15ColPFBFS, AV93SDT_Demandas, AV46Ln) ;
         nGXsfl_373_idx = nGXsfl_373_bak_idx;
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
         cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", cmbavContratada_codigo.ToJavascriptSource());
         dynavContagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfscod_Internalname, "Values", dynavContagemresultado_contadorfscod.ToJavascriptSource());
         cmbavContagemresultado_ehvalidacao.CurrentValue = StringUtil.BoolToStr( AV20ContagemResultado_EhValidacao);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Values", cmbavContagemresultado_ehvalidacao.ToJavascriptSource());
         cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", cmbavContratoservicos_codigo.ToJavascriptSource());
         dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", dynavContagemresultado_contadorfmcod.ToJavascriptSource());
      }

      protected void E23BL2( )
      {
         /* Selcontratadasrv_codigo_Click Routine */
         AV136ContratadaSrv_Codigo = AV135SelContratadaSrv_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ContratadaSrv_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136ContratadaSrv_Codigo), 6, 0)));
         if ( (0==AV135SelContratadaSrv_Codigo) )
         {
            AV126OSServico_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
            AV126OSServico_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126OSServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV126OSServico_Codigo), 6, 0)));
         }
      }

      protected void E17BL2( )
      {
         /* Dataentrega_Isvalid Routine */
         if ( (DateTime.MinValue==AV151DataEntrega) )
         {
            lblTextblockprazoentrega_Caption = "Prazo";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprazoentrega_Internalname, "Caption", lblTextblockprazoentrega_Caption);
         }
         else if ( ( StringUtil.StrCmp(AV165TipoDias, "U") == 0 ) && ( ( DateTimeUtil.Dow( AV151DataEntrega) == 1 ) || ( DateTimeUtil.Dow( AV151DataEntrega) == 7 ) ) )
         {
            GX_msglist.addItem("Prazo de entrega "+context.localUtil.DToC( DateTimeUtil.ResetTime( AV151DataEntrega), 2, "/")+" � "+DateTimeUtil.CDow( AV151DataEntrega, "por")+"!");
            AV151DataEntrega = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
            lblTextblockprazoentrega_Caption = "Prazo";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprazoentrega_Internalname, "Caption", lblTextblockprazoentrega_Caption);
         }
         else if ( ( StringUtil.StrCmp(AV165TipoDias, "U") == 0 ) && new prc_ehferiado(context).executeUdp( ref  AV151DataEntrega) )
         {
            GX_msglist.addItem("Prazo de entrega "+context.localUtil.DToC( DateTimeUtil.ResetTime( AV151DataEntrega), 2, "/")+" � feriado!");
            AV151DataEntrega = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
            lblTextblockprazoentrega_Caption = "Prazo";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprazoentrega_Internalname, "Caption", lblTextblockprazoentrega_Caption);
         }
         else
         {
            lblTextblockprazoentrega_Caption = "Prazo "+DateTimeUtil.CDow( AV151DataEntrega, "por");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprazoentrega_Internalname, "Caption", lblTextblockprazoentrega_Caption);
            GXt_dtime10 = AV164FimDoExpediente;
            new prc_getfimdoexpediente(context ).execute( ref  AV156AreaTrabalho_Codigo, out  GXt_dtime10) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156AreaTrabalho_Codigo), 6, 0)));
            AV164FimDoExpediente = GXt_dtime10;
            if ( ! (DateTime.MinValue==AV164FimDoExpediente) )
            {
               AV151DataEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV151DataEntrega) ) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
               AV151DataEntrega = DateTimeUtil.TAdd( AV151DataEntrega, 3600*(DateTimeUtil.Hour( AV164FimDoExpediente)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
               AV151DataEntrega = DateTimeUtil.TAdd( AV151DataEntrega, 60*(DateTimeUtil.Minute( AV164FimDoExpediente)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151DataEntrega", context.localUtil.TToC( AV151DataEntrega, 8, 5, 0, 3, "/", ":", " "));
            }
         }
      }

      protected void E18BL2( )
      {
         /* 'Importar' Routine */
         Innewwindow_Target = formatLink("aprc_inserirsdtdemandas.aspx") + "?" + UrlEncode("" +AV127Contratada_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV143SrvParaFtr)) + "," + UrlEncode("" +AV178Requisitante) + "," + UrlEncode(StringUtil.RTrim(AV121StatusDmn)) + "," + UrlEncode("" +AV136ContratadaSrv_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV142DataSrvFat)) + "," + UrlEncode("" +AV150ContratadaOrigem_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV151DataEntrega)) + "," + UrlEncode(StringUtil.RTrim(AV153Agrupador)) + "," + UrlEncode(StringUtil.BoolToStr(AV154Notificar)) + "," + UrlEncode("" +AV177ContratoServicosOS_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.DoAjaxRefreshCmp("W0339"+"");
         AV93SDT_Demandas.Clear();
         gx_BV373 = true;
         AV62Qtde = 0;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93SDT_Demandas", AV93SDT_Demandas);
         nGXsfl_373_bak_idx = nGXsfl_373_idx;
         gxgrGridsdtdemandas_refresh( AV120Servico_Codigo, AV136ContratadaSrv_Codigo, AV126OSServico_Codigo, AV145DataSrvFatSel, AV97ColPFBFM, AV111ColPFBFMCAST, AV56OQImportar, AV143SrvParaFtr, AV181RequerOrigem, AV15ColPFBFS, AV93SDT_Demandas, AV46Ln) ;
         nGXsfl_373_idx = nGXsfl_373_bak_idx;
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
      }

      protected void E19BL2( )
      {
         /* Requisitante_Click Routine */
         /* Execute user subroutine: 'CARREGACONTRATADAS' */
         S192 ();
         if (returnInSub) return;
         cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", cmbavContratada_codigo.ToJavascriptSource());
      }

      protected void E11BL2( )
      {
         /* Confirmpanel_Close Routine */
         /* Execute user subroutine: 'APOSCONFIRMPANELIDMN' */
         S172 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93SDT_Demandas", AV93SDT_Demandas);
         nGXsfl_373_bak_idx = nGXsfl_373_idx;
         gxgrGridsdtdemandas_refresh( AV120Servico_Codigo, AV136ContratadaSrv_Codigo, AV126OSServico_Codigo, AV145DataSrvFatSel, AV97ColPFBFM, AV111ColPFBFMCAST, AV56OQImportar, AV143SrvParaFtr, AV181RequerOrigem, AV15ColPFBFS, AV93SDT_Demandas, AV46Ln) ;
         nGXsfl_373_idx = nGXsfl_373_bak_idx;
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
      }

      protected void E12BL2( )
      {
         /* Confirmpanel2_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel2_Title, "Prestadora") == 0 )
         {
            AV127Contratada_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            AV127Contratada_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
         }
         cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", cmbavContratada_codigo.ToJavascriptSource());
      }

      protected void E20BL2( )
      {
         /* Contratoservicos_codigo_Click Routine */
         if ( new prc_naotempreposto(context).executeUdp(  AV176ContratoServicos_Codigo,  0) )
         {
            Confirmpanel2_Confirmationtext = "A "+cmbavContratada_codigo.Description+" n�o tem Preposto estabelecido!";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
            Confirmpanel2_Title = "Prestadora";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
            this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
         }
      }

      protected void E21BL2( )
      {
         /* Contratadaorigem_codigo_Click Routine */
         AV206GXLvl791 = 0;
         /* Using cursor H00BL15 */
         pr_default.execute(13, new Object[] {AV150ContratadaOrigem_Codigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A1013Contrato_PrepostoCod = H00BL15_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00BL15_n1013Contrato_PrepostoCod[0];
            A39Contratada_Codigo = H00BL15_A39Contratada_Codigo[0];
            AV206GXLvl791 = 1;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(13);
         }
         pr_default.close(13);
         if ( AV206GXLvl791 == 0 )
         {
            Confirmpanel2_Confirmationtext = "A "+dynavContratadaorigem_codigo.Description+" n�o tem Preposto estabelecido! A OS ser� solicitada sem esta refer�ncia.";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
            Confirmpanel2_Title = "Origem";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
            this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
         }
      }

      protected void S172( )
      {
         /* 'APOSCONFIRMPANELIDMN' Routine */
         AV85ColOSFSn = (short)(StringUtil.Asc( AV75ColOSFS)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ColOSFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85ColOSFSn), 4, 0)));
         AV86ColNomeSisteman = (short)(StringUtil.Asc( AV76ColNomeSistema)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ColNomeSisteman", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ColNomeSisteman), 4, 0)));
         AV163ColNomeModulon = (short)(StringUtil.Asc( AV162ColNomeModulo)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV163ColNomeModulon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV163ColNomeModulon), 4, 0)));
         AV87ColOSFMn = (short)(StringUtil.Asc( AV77ColOSFM)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ColOSFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87ColOSFMn), 4, 0)));
         AV88ColDataDmnn = (short)(StringUtil.Asc( AV78ColDataDmn)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ColDataDmnn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88ColDataDmnn), 4, 0)));
         AV148ColDataCntn = (short)(StringUtil.Asc( AV147ColDataCnt)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ColDataCntn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ColDataCntn), 4, 0)));
         AV89ColDescricaon = (short)(StringUtil.Asc( AV80ColDescricao)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ColDescricaon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89ColDescricaon), 4, 0)));
         AV90ColContadorFSn = (short)(StringUtil.Asc( AV82ColContadorFS)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ColContadorFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90ColContadorFSn), 4, 0)));
         AV102ColContadorFMn = (short)(StringUtil.Asc( AV101ColContadorFM)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ColContadorFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102ColContadorFMn), 4, 0)));
         AV91ColLinkn = (short)(StringUtil.Asc( AV84ColLink)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ColLinkn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91ColLinkn), 4, 0)));
         AV99ColPFBFMn = (short)(StringUtil.Asc( AV97ColPFBFM)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV99ColPFBFMn), 4, 0)));
         AV100ColPFlFMn = (short)(StringUtil.Asc( AV98ColPFLFM)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ColPFlFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100ColPFlFMn), 4, 0)));
         AV131ColFiltron = (short)(StringUtil.Asc( AV130ColFiltro)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131ColFiltron", StringUtil.LTrim( StringUtil.Str( (decimal)(AV131ColFiltron), 4, 0)));
         AV141ColPFBFSDmnn = (short)(StringUtil.Asc( AV138ColPFBFSDmn)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141ColPFBFSDmnn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV141ColPFBFSDmnn), 4, 0)));
         AV140ColPFLFSDmnn = (short)(StringUtil.Asc( AV139ColPFLFSDmn)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140ColPFLFSDmnn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV140ColPFLFSDmnn), 4, 0)));
         AV167ColProjeton = (short)(StringUtil.Asc( AV166ColProjeto)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167ColProjeton", StringUtil.LTrim( StringUtil.Str( (decimal)(AV167ColProjeton), 4, 0)));
         AV169ColTipon = (short)(StringUtil.Asc( AV168ColTipo)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV169ColTipon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV169ColTipon), 4, 0)));
         AV171ColObservacaon = (short)(StringUtil.Asc( AV170ColObservacao)-64);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171ColObservacaon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV171ColObservacaon), 4, 0)));
         tblTablesdtdemandas_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablesdtdemandas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablesdtdemandas_Visible), 5, 0)));
         AV31ErrCod = AV33ExcelDocument.Close();
         AV93SDT_Demandas.Clear();
         gx_BV373 = true;
         /* Execute user subroutine: 'CARREGASDTDEMANDAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S192( )
      {
         /* 'CARREGACONTRATADAS' Routine */
         cmbavContratada_codigo.removeAllItems();
         cmbavContratada_codigo.addItem("0", "(Nenhuma)", 0);
         pr_default.dynParam(14, new Object[]{ new Object[]{
                                              AV156AreaTrabalho_Codigo ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A1869Contrato_DataTermino ,
                                              A92Contrato_Ativo ,
                                              A43Contratada_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00BL19 */
         pr_default.execute(14, new Object[] {AV156AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A1595Contratada_AreaTrbSrvPdr = H00BL19_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00BL19_n1595Contratada_AreaTrbSrvPdr[0];
            A74Contrato_Codigo = H00BL19_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00BL19_n74Contrato_Codigo[0];
            A39Contratada_Codigo = H00BL19_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00BL19_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00BL19_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00BL19_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00BL19_n43Contratada_Ativo[0];
            A632Servico_Ativo = H00BL19_A632Servico_Ativo[0];
            n632Servico_Ativo = H00BL19_n632Servico_Ativo[0];
            A92Contrato_Ativo = H00BL19_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00BL19_n92Contrato_Ativo[0];
            A75Contrato_AreaTrabalhoCod = H00BL19_A75Contrato_AreaTrabalhoCod[0];
            A1869Contrato_DataTermino = H00BL19_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = H00BL19_n1869Contrato_DataTermino[0];
            A1869Contrato_DataTermino = H00BL19_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = H00BL19_n1869Contrato_DataTermino[0];
            A52Contratada_AreaTrabalhoCod = H00BL19_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00BL19_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00BL19_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00BL19_n43Contratada_Ativo[0];
            A1595Contratada_AreaTrbSrvPdr = H00BL19_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00BL19_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00BL19_A632Servico_Ativo[0];
            n632Servico_Ativo = H00BL19_n632Servico_Ativo[0];
            if ( A1869Contrato_DataTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") )
            {
               /* Using cursor H00BL20 */
               pr_default.execute(15, new Object[] {AV178Requisitante, n74Contrato_Codigo, A74Contrato_Codigo});
               while ( (pr_default.getStatus(15) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00BL20_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00BL20_A1079ContratoGestor_UsuarioCod[0];
                  A2033ContratoGestor_UsuarioAtv = H00BL20_A2033ContratoGestor_UsuarioAtv[0];
                  n2033ContratoGestor_UsuarioAtv = H00BL20_n2033ContratoGestor_UsuarioAtv[0];
                  A1223ContratoGestor_ContratadaSigla = H00BL20_A1223ContratoGestor_ContratadaSigla[0];
                  n1223ContratoGestor_ContratadaSigla = H00BL20_n1223ContratoGestor_ContratadaSigla[0];
                  A1136ContratoGestor_ContratadaCod = H00BL20_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00BL20_n1136ContratoGestor_ContratadaCod[0];
                  A1136ContratoGestor_ContratadaCod = H00BL20_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00BL20_n1136ContratoGestor_ContratadaCod[0];
                  A1223ContratoGestor_ContratadaSigla = H00BL20_A1223ContratoGestor_ContratadaSigla[0];
                  n1223ContratoGestor_ContratadaSigla = H00BL20_n1223ContratoGestor_ContratadaSigla[0];
                  A2033ContratoGestor_UsuarioAtv = H00BL20_A2033ContratoGestor_UsuarioAtv[0];
                  n2033ContratoGestor_UsuarioAtv = H00BL20_n2033ContratoGestor_UsuarioAtv[0];
                  cmbavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)), A1223ContratoGestor_ContratadaSigla, 0);
                  AV127Contratada_Codigo = A1136ContratoGestor_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
                  pr_default.readNext(15);
               }
               pr_default.close(15);
            }
            pr_default.readNext(14);
         }
         pr_default.close(14);
         if ( cmbavContratada_codigo.ItemCount > 2 )
         {
            AV127Contratada_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
         }
      }

      protected void S122( )
      {
         /* 'CARREGACONTRATO' Routine */
         cmbavContratoservicos_codigo.removeAllItems();
         /* Using cursor H00BL21 */
         pr_default.execute(16, new Object[] {AV120Servico_Codigo, AV127Contratada_Codigo});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A74Contrato_Codigo = H00BL21_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00BL21_n74Contrato_Codigo[0];
            A39Contratada_Codigo = H00BL21_A39Contratada_Codigo[0];
            A155Servico_Codigo = H00BL21_A155Servico_Codigo[0];
            A77Contrato_Numero = H00BL21_A77Contrato_Numero[0];
            A160ContratoServicos_Codigo = H00BL21_A160ContratoServicos_Codigo[0];
            A39Contratada_Codigo = H00BL21_A39Contratada_Codigo[0];
            A77Contrato_Numero = H00BL21_A77Contrato_Numero[0];
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A77Contrato_Numero, 0);
            AV176ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
            pr_default.readNext(16);
         }
         pr_default.close(16);
         if ( cmbavContratoservicos_codigo.ItemCount == 1 )
         {
            if ( new prc_naotempreposto(context).executeUdp(  AV176ContratoServicos_Codigo,  0) )
            {
               Confirmpanel2_Confirmationtext = "A "+cmbavContratada_codigo.Description+" n�o tem Preposto estabelecido! Esta OS n�o podera ser solicitada.";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
               Confirmpanel2_Title = "Prestadora";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
            }
         }
         else
         {
            cmbavContratoservicos_codigo.addItem("0", "(Nenhum)", 1);
            AV176ContratoServicos_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV176ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)));
         }
      }

      protected void S132( )
      {
         /* 'CARREGACONTRATOOS' Routine */
         cmbavContratoservicosos_codigo.removeAllItems();
         /* Using cursor H00BL22 */
         pr_default.execute(17, new Object[] {AV126OSServico_Codigo, AV136ContratadaSrv_Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A74Contrato_Codigo = H00BL22_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00BL22_n74Contrato_Codigo[0];
            A39Contratada_Codigo = H00BL22_A39Contratada_Codigo[0];
            A155Servico_Codigo = H00BL22_A155Servico_Codigo[0];
            A77Contrato_Numero = H00BL22_A77Contrato_Numero[0];
            A160ContratoServicos_Codigo = H00BL22_A160ContratoServicos_Codigo[0];
            A39Contratada_Codigo = H00BL22_A39Contratada_Codigo[0];
            A77Contrato_Numero = H00BL22_A77Contrato_Numero[0];
            cmbavContratoservicosos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A77Contrato_Numero, 0);
            AV177ContratoServicosOS_Codigo = A160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ContratoServicosOS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)));
            pr_default.readNext(17);
         }
         pr_default.close(17);
         if ( cmbavContratoservicosos_codigo.ItemCount > 1 )
         {
            cmbavContratoservicosos_codigo.addItem("0", "(Nenhum)", 1);
            AV177ContratoServicosOS_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ContratoServicosOS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)));
         }
      }

      protected void S202( )
      {
         /* 'CARREGASDTDEMANDAS' Routine */
         Innewwindow_Target = formatLink("aprc_carregasdtdemandas.aspx") + "?" + UrlEncode("" +AV60PraLinha) + "," + UrlEncode(StringUtil.RTrim(AV8Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV128Aba)) + "," + UrlEncode("" +AV85ColOSFSn) + "," + UrlEncode("" +AV86ColNomeSisteman) + "," + UrlEncode("" +AV163ColNomeModulon) + "," + UrlEncode("" +AV87ColOSFMn) + "," + UrlEncode("" +AV88ColDataDmnn) + "," + UrlEncode("" +AV89ColDescricaon) + "," + UrlEncode("" +AV91ColLinkn) + "," + UrlEncode("" +AV127Contratada_Codigo) + "," + UrlEncode("" +AV136ContratadaSrv_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV20ContagemResultado_EhValidacao)) + "," + UrlEncode(StringUtil.RTrim(AV57OSFM)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV29DataOSFM)) + "," + UrlEncode(StringUtil.RTrim(AV79ContagemResultado_Observacao)) + "," + UrlEncode("" +AV81ContagemResultado_ContadorFSCod) + "," + UrlEncode(StringUtil.RTrim(dynavContagemresultado_contadorfscod.Description)) + "," + UrlEncode(StringUtil.RTrim(AV83ContagemResultado_Link)) + "," + UrlEncode("" +AV99ColPFBFMn) + "," + UrlEncode("" +AV100ColPFlFMn) + "," + UrlEncode("" +AV18ContagemResultado_ContadorFMCod) + "," + UrlEncode("" +AV102ColContadorFMn) + "," + UrlEncode("" +AV176ContratoServicos_Codigo) + "," + UrlEncode("" +AV126OSServico_Codigo) + "," + UrlEncode("" +AV131ColFiltron) + "," + UrlEncode(StringUtil.RTrim(AV132TipoFiltro)) + "," + UrlEncode(StringUtil.RTrim(AV133FiltroColuna)) + "," + UrlEncode("" +AV141ColPFBFSDmnn) + "," + UrlEncode("" +AV140ColPFLFSDmnn) + "," + UrlEncode("" +AV90ColContadorFSn) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV146DataCnt)) + "," + UrlEncode("" +AV148ColDataCntn) + "," + UrlEncode(StringUtil.RTrim(AV134FileName)) + "," + UrlEncode("" +AV167ColProjeton) + "," + UrlEncode("" +AV169ColTipon) + "," + UrlEncode("" +AV171ColObservacaon) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV151DataEntrega));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void S142( )
      {
         /* 'IMPORTARCONTAGEM' Routine */
         Innewwindow_Target = formatLink("aprc_importarcontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV56OQImportar)) + "," + UrlEncode("" +AV127Contratada_Codigo) + "," + UrlEncode("" +AV120Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV128Aba)) + "," + UrlEncode("" +AV108ColDmnCASTn) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV146DataCnt)) + "," + UrlEncode("" +AV148ColDataCntn) + "," + UrlEncode("" +AV116PraLinCAST) + "," + UrlEncode("" +AV102ColContadorFMn) + "," + UrlEncode("" +AV18ContagemResultado_ContadorFMCod) + "," + UrlEncode("" +AV118ColPFBFSn) + "," + UrlEncode("" +AV119ColPFlFSn) + "," + UrlEncode("" +AV99ColPFBFMn) + "," + UrlEncode("" +AV100ColPFlFMn) + "," + UrlEncode("" +AV174ColParecern) + "," + UrlEncode(StringUtil.BoolToStr(false)) + "," + UrlEncode(StringUtil.RTrim(AV134FileName));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void S162( )
      {
         /* 'RELATORIOCONFERENCIA' Routine */
         Innewwindow_Target = formatLink("arel_conferenciams.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV9Arquivo2)) + "," + UrlEncode(StringUtil.RTrim(AV128Aba)) + "," + UrlEncode(StringUtil.RTrim(AV56OQImportar)) + "," + UrlEncode("" +AV12ColDem) + "," + UrlEncode("" +AV14ColPB) + "," + UrlEncode("" +AV17ColPL) + "," + UrlEncode("" +AV60PraLinha) + "," + UrlEncode(StringUtil.RTrim(AV7Acao)) + "," + UrlEncode("" +AV127Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV134FileName)) + "," + UrlEncode("" +AV99ColPFBFMn) + "," + UrlEncode("" +AV100ColPFlFMn) + "," + UrlEncode(StringUtil.RTrim(AV161OSFMCTRF));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void S152( )
      {
         /* 'CONFERENCIASGSD' Routine */
         Innewwindow_Target = formatLink("arel_conferenciacast.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV128Aba)) + "," + UrlEncode("" +AV127Contratada_Codigo) + "," + UrlEncode("" +AV116PraLinCAST) + "," + UrlEncode("" +AV108ColDmnCASTn) + "," + UrlEncode("" +AV107ColDataCASTn) + "," + UrlEncode("" +AV103ColPFBFSCASTn) + "," + UrlEncode("" +AV104ColPFLFSCASTn) + "," + UrlEncode("" +AV105ColPFBFMCASTn) + "," + UrlEncode("" +AV106ColPFLFMCASTn) + "," + UrlEncode(StringUtil.RTrim(AV134FileName));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void S182( )
      {
         /* 'SEARCHCONTRATADA' Routine */
         lV26Contratada_Nome = StringUtil.PadR( StringUtil.RTrim( AV26Contratada_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratada_Nome", AV26Contratada_Nome);
         /* Using cursor H00BL23 */
         pr_default.execute(18, new Object[] {lV26Contratada_Nome, AV156AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(18) != 101) )
         {
            A40Contratada_PessoaCod = H00BL23_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = H00BL23_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00BL23_n52Contratada_AreaTrabalhoCod[0];
            A41Contratada_PessoaNom = H00BL23_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00BL23_n41Contratada_PessoaNom[0];
            A43Contratada_Ativo = H00BL23_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00BL23_n43Contratada_Ativo[0];
            A39Contratada_Codigo = H00BL23_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = H00BL23_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00BL23_n41Contratada_PessoaNom[0];
            AV127Contratada_Codigo = A39Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      protected void S112( )
      {
         /* 'REQUISITANTE' Routine */
         if ( AV74WWPContext.gxTpr_Userehcontratante )
         {
            cmbavRequisitante.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV74WWPContext.gxTpr_Userid), 6, 0)), AV74WWPContext.gxTpr_Username, 0);
            AV178Requisitante = AV74WWPContext.gxTpr_Userid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
            cmbavRequisitante.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisitante.Enabled), 5, 0)));
            /* Execute user subroutine: 'CARREGACONTRATADAS' */
            S192 ();
            if (returnInSub) return;
         }
         else
         {
            /* Using cursor H00BL24 */
            pr_default.execute(19, new Object[] {AV74WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(19) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00BL24_A1078ContratoGestor_ContratoCod[0];
               A1136ContratoGestor_ContratadaCod = H00BL24_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00BL24_n1136ContratoGestor_ContratadaCod[0];
               A1080ContratoGestor_UsuarioPesCod = H00BL24_A1080ContratoGestor_UsuarioPesCod[0];
               n1080ContratoGestor_UsuarioPesCod = H00BL24_n1080ContratoGestor_UsuarioPesCod[0];
               A43Contratada_Ativo = H00BL24_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00BL24_n43Contratada_Ativo[0];
               A92Contrato_Ativo = H00BL24_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00BL24_n92Contrato_Ativo[0];
               A2033ContratoGestor_UsuarioAtv = H00BL24_A2033ContratoGestor_UsuarioAtv[0];
               n2033ContratoGestor_UsuarioAtv = H00BL24_n2033ContratoGestor_UsuarioAtv[0];
               A1081ContratoGestor_UsuarioPesNom = H00BL24_A1081ContratoGestor_UsuarioPesNom[0];
               n1081ContratoGestor_UsuarioPesNom = H00BL24_n1081ContratoGestor_UsuarioPesNom[0];
               A1079ContratoGestor_UsuarioCod = H00BL24_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00BL24_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00BL24_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = H00BL24_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00BL24_n1136ContratoGestor_ContratadaCod[0];
               A92Contrato_Ativo = H00BL24_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00BL24_n92Contrato_Ativo[0];
               A1446ContratoGestor_ContratadaAreaCod = H00BL24_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00BL24_n1446ContratoGestor_ContratadaAreaCod[0];
               A43Contratada_Ativo = H00BL24_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00BL24_n43Contratada_Ativo[0];
               A1080ContratoGestor_UsuarioPesCod = H00BL24_A1080ContratoGestor_UsuarioPesCod[0];
               n1080ContratoGestor_UsuarioPesCod = H00BL24_n1080ContratoGestor_UsuarioPesCod[0];
               A2033ContratoGestor_UsuarioAtv = H00BL24_A2033ContratoGestor_UsuarioAtv[0];
               n2033ContratoGestor_UsuarioAtv = H00BL24_n2033ContratoGestor_UsuarioAtv[0];
               A1081ContratoGestor_UsuarioPesNom = H00BL24_A1081ContratoGestor_UsuarioPesNom[0];
               n1081ContratoGestor_UsuarioPesNom = H00BL24_n1081ContratoGestor_UsuarioPesNom[0];
               GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  cmbavRequisitante.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)), A1081ContratoGestor_UsuarioPesNom, 0);
                  AV178Requisitante = A1079ContratoGestor_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
               }
               pr_default.readNext(19);
            }
            pr_default.close(19);
            if ( cmbavRequisitante.ItemCount > 2 )
            {
               AV178Requisitante = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV178Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)));
            }
            else
            {
               /* Execute user subroutine: 'CARREGACONTRATADAS' */
               S192 ();
               if (returnInSub) return;
            }
         }
      }

      private void E26BL2( )
      {
         /* Gridsdtdemandas_Load Routine */
         AV184GXV1 = 1;
         while ( AV184GXV1 <= AV93SDT_Demandas.Count )
         {
            AV93SDT_Demandas.CurrentItem = ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 373;
            }
            sendrow_3732( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_373_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(373, GridsdtdemandasRow);
            }
            AV184GXV1 = (short)(AV184GXV1+1);
         }
      }

      protected void wb_table1_2_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 3, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle;height:50px")+"\" class='Table'>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "A��O:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOqimportar, cmbavOqimportar_Internalname, StringUtil.RTrim( AV56OQImportar), 1, cmbavOqimportar_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVOQIMPORTAR.CLICK."+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavOqimportar.CurrentValue = StringUtil.RTrim( AV56OQImportar);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOqimportar_Internalname, "Values", (String)(cmbavOqimportar.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_12_BL2( true) ;
         }
         else
         {
            wb_table2_12_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table2_12_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_BL2( true) ;
         }
         else
         {
            wb_table3_63_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table3_63_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table4_95_BL2( true) ;
         }
         else
         {
            wb_table4_95_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table4_95_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            wb_table5_119_BL2( true) ;
         }
         else
         {
            wb_table5_119_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table5_119_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p>") ;
            wb_table6_223_BL2( true) ;
         }
         else
         {
            wb_table6_223_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table6_223_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table7_247_BL2( true) ;
         }
         else
         {
            wb_table7_247_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table7_247_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            wb_table8_257_BL2( true) ;
         }
         else
         {
            wb_table8_257_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table8_257_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_277_BL2( true) ;
         }
         else
         {
            wb_table9_277_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table9_277_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:14px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table10_333_BL2( true) ;
         }
         else
         {
            wb_table10_333_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table10_333_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            wb_table11_343_BL2( true) ;
         }
         else
         {
            wb_table11_343_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table11_343_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            wb_table12_367_BL2( true) ;
         }
         else
         {
            wb_table12_367_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table12_367_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table13_370_BL2( true) ;
         }
         else
         {
            wb_table13_370_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table13_370_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 391,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCarregar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(373), 3, 0)+","+"null"+");", bttCarregar_Caption, bttCarregar_Jsonclick, 5, "Carregar", "", StyleString, ClassString, bttCarregar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportFromFile.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 392,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttImportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(373), 3, 0)+","+"null"+");", "Importar", bttImportar_Jsonclick, 5, "Importar", "", StyleString, ClassString, bttImportar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'IMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BL2e( true) ;
         }
         else
         {
            wb_table1_2_BL2e( false) ;
         }
      }

      protected void wb_table13_370_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablesdtdemandas_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablesdtdemandas_Internalname, tblTablesdtdemandas_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridsdtdemandasContainer.SetWrapped(nGXWrapped);
            if ( GridsdtdemandasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridsdtdemandasContainer"+"DivS\" data-gxgridid=\"373\">") ;
               sStyleString = "";
               if ( subGridsdtdemandas_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               GxWebStd.gx_table_start( context, subGridsdtdemandas_Internalname, subGridsdtdemandas_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridsdtdemandas_Backcolorstyle == 0 )
               {
                  subGridsdtdemandas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridsdtdemandas_Class) > 0 )
                  {
                     subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Title";
                  }
               }
               else
               {
                  subGridsdtdemandas_Titlebackstyle = 1;
                  if ( subGridsdtdemandas_Backcolorstyle == 1 )
                  {
                     subGridsdtdemandas_Titlebackcolor = subGridsdtdemandas_Allbackcolor;
                     if ( StringUtil.Len( subGridsdtdemandas_Class) > 0 )
                     {
                        subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridsdtdemandas_Class) > 0 )
                     {
                        subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "OS FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Data OS FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Titulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Prazo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "N� Refer�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Link") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFB FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFL FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFB FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFL FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdtdemandas_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridsdtdemandasContainer.AddObjectProperty("GridName", "Gridsdtdemandas");
            }
            else
            {
               GridsdtdemandasContainer.AddObjectProperty("GridName", "Gridsdtdemandas");
               GridsdtdemandasContainer.AddObjectProperty("Class", "WorkWith");
               GridsdtdemandasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Backcolorstyle), 1, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Titleforecolor), 9, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Visible), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("CmpContext", "");
               GridsdtdemandasContainer.AddObjectProperty("InMasterPage", "false");
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlsistemanom_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldemandafm_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldatadmn_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldescricao_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldataent_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldemanda_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontadorfsnom_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtllink_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlpfbfm_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlpflfm_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlpfbfs_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlpflfs_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsdtdemandasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontadorfmcod_Enabled), 5, 0, ".", "")));
               GridsdtdemandasContainer.AddColumnProperties(GridsdtdemandasColumn);
               GridsdtdemandasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Allowselection), 1, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Selectioncolor), 9, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Allowhovering), 1, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Hoveringcolor), 9, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Allowcollapsing), 1, 0, ".", "")));
               GridsdtdemandasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdtdemandas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 373 )
         {
            wbEnd = 0;
            nRC_GXsfl_373 = (short)(nGXsfl_373_idx-1);
            if ( GridsdtdemandasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV184GXV1 = nGXsfl_373_idx;
               if ( subGridsdtdemandas_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+"GridsdtdemandasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridsdtdemandas", GridsdtdemandasContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridsdtdemandasContainerData", GridsdtdemandasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridsdtdemandasContainerData"+"V", GridsdtdemandasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridsdtdemandasContainerData"+"V"+"\" value='"+GridsdtdemandasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_370_BL2e( true) ;
         }
         else
         {
            wb_table13_370_BL2e( false) ;
         }
      }

      protected void wb_table12_367_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablegrid_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablegrid_Internalname, tblTablegrid_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_367_BL2e( true) ;
         }
         else
         {
            wb_table12_367_BL2e( false) ;
         }
      }

      protected void wb_table11_343_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblfiltrocoluna_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblfiltrocoluna_Internalname, tblTblfiltrocoluna_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 346,'',false,'" + sGXsfl_373_idx + "',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158Blob)) )
            {
               gxblobfileaux.Source = AV158Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV158Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV158Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV158Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV158Blob), context.PathToRelativeUrl( AV158Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV158Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 0, "px", 11, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,346);\"", "", "", "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock27_Internalname, "Nome da Aba:", "", "", lblTextblock27_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 351,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAba_Internalname, StringUtil.RTrim( AV128Aba), StringUtil.RTrim( context.localUtil.Format( AV128Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,351);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAba_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Filtrar coluna:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 356,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColfiltro_Internalname, StringUtil.RTrim( AV130ColFiltro), StringUtil.RTrim( context.localUtil.Format( AV130ColFiltro, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,356);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColfiltro_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "Tipo de dados do filtro:", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 360,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTipofiltro, cmbavTipofiltro_Internalname, StringUtil.RTrim( AV132TipoFiltro), 1, cmbavTipofiltro_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,360);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavTipofiltro.CurrentValue = StringUtil.RTrim( AV132TipoFiltro);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipofiltro_Internalname, "Values", (String)(cmbavTipofiltro.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Filtrar as linhas com valor:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 364,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltrocoluna_Internalname, StringUtil.RTrim( AV133FiltroColuna), StringUtil.RTrim( context.localUtil.Format( AV133FiltroColuna, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,364);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltrocoluna_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_343_BL2e( true) ;
         }
         else
         {
            wb_table11_343_BL2e( false) ;
         }
      }

      protected void wb_table10_333_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblanexos_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblanexos_Internalname, tblTblanexos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Anexos:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0339"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0339"+"");
               }
               WebComp_Wc_anexos.componentdraw();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_333_BL2e( true) ;
         }
         else
         {
            wb_table10_333_BL2e( false) ;
         }
      }

      protected void wb_table9_277_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblconftrf_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblconftrf_Internalname, tblTblconftrf_Internalname, "", "Table", 0, "", "", 1, 4, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock16_Internalname, "1� linha �", "", "", lblTextblock16_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 282,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralinhactrf_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96PraLinhaCTRF), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV96PraLinhaCTRF), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,282);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinhactrf_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock11_Internalname, "Coluna das Demandas �", "", "", lblTextblock11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 286,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdemandas_Internalname, StringUtil.RTrim( AV13ColDemandas), StringUtil.RTrim( context.localUtil.Format( AV13ColDemandas, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,286);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdemandas_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table14_288_BL2( true) ;
         }
         else
         {
            wb_table14_288_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table14_288_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock12_Internalname, "Coluna PFB OS Ref �", "", "", lblTextblock12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 301,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfs_Internalname, StringUtil.RTrim( AV15ColPFBFS), StringUtil.RTrim( context.localUtil.Format( AV15ColPFBFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,301);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfs_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock13_Internalname, "Coluna PFL OS Ref �", "", "", lblTextblock13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 305,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfs_Internalname, StringUtil.RTrim( AV16ColPFLFS), StringUtil.RTrim( context.localUtil.Format( AV16ColPFLFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,305);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfs_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock14_Internalname, "Coluna Deflator �", "", "", lblTextblock14_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:37px")+"\" class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 309,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdeflator_Internalname, StringUtil.RTrim( AV11ColDeflator), StringUtil.RTrim( context.localUtil.Format( AV11ColDeflator, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,309);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdeflator_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock15_Internalname, "A��o �", "", "", lblTextblock15_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 313,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAcao, cmbavAcao_Internalname, StringUtil.RTrim( AV7Acao), 1, cmbavAcao_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e27bl1_client"+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,313);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavAcao.CurrentValue = StringUtil.RTrim( AV7Acao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAcao_Internalname, "Values", (String)(cmbavAcao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock43_Internalname, "N� da OS:", "", "", lblTextblock43_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 318,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOsfmctrf_Internalname, AV161OSFMCTRF, StringUtil.RTrim( context.localUtil.Format( AV161OSFMCTRF, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,318);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOsfmctrf_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_277_BL2e( true) ;
         }
         else
         {
            wb_table9_277_BL2e( false) ;
         }
      }

      protected void wb_table14_288_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblpffm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblpffm_Internalname, tblTblpffm_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock17_Internalname, "Coluna PFB OS �", "", "", lblTextblock17_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 293,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfmctrf_Internalname, StringUtil.RTrim( AV159ColPFBFMCTRF), StringUtil.RTrim( context.localUtil.Format( AV159ColPFBFMCTRF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,293);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfmctrf_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock42_Internalname, "Coluna PFL OS �", "", "", lblTextblock42_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 297,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfmctrf_Internalname, StringUtil.RTrim( AV160ColPFLFMCTRF), StringUtil.RTrim( context.localUtil.Format( AV160ColPFLFMCTRF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,297);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfmctrf_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_288_BL2e( true) ;
         }
         else
         {
            wb_table14_288_BL2e( false) ;
         }
      }

      protected void wb_table8_257_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTbldadosms_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTbldadosms_Internalname, tblTbldadosms_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Data da OS �", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 262,'',false,'" + sGXsfl_373_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataosfm2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataosfm2_Internalname, context.localUtil.Format(AV30DataOSFM2, "99/99/99"), context.localUtil.Format( AV30DataOSFM2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,262);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataosfm2_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            GxWebStd.gx_bitmap( context, edtavDataosfm2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "N� da OS: �", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 268,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOsfm2_Internalname, AV58OSFM2, StringUtil.RTrim( context.localUtil.Format( AV58OSFM2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,268);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOsfm2_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "Esfor�o (min) �", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 272,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinutos2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49Minutos2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49Minutos2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,272);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinutos2_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Valor da Unidade �", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 276,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_valorpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV24ContagemResultado_ValorPF, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV24ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,276);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_valorpf_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_257_BL2e( true) ;
         }
         else
         {
            wb_table8_257_BL2e( false) ;
         }
      }

      protected void wb_table7_247_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcontadorfs_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcontadorfs_Internalname, tblTblcontadorfs_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:124px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontadorfs_Internalname, "Funcion�rio da Origem:", "", "", lblTbcontadorfs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:578px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 252,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfscod, dynavContagemresultado_contadorfscod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0)), 1, dynavContagemresultado_contadorfscod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,252);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavContagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfscod_Internalname, "Values", (String)(dynavContagemresultado_contadorfscod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:63px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcolcrfs_Internalname, "ou coluna:", "", "", lblTbcolcrfs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:103px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 256,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColcontadorfs_Internalname, StringUtil.RTrim( AV82ColContadorFS), StringUtil.RTrim( context.localUtil.Format( AV82ColContadorFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,256);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColcontadorfs_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_247_BL2e( true) ;
         }
         else
         {
            wb_table7_247_BL2e( false) ;
         }
      }

      protected void wb_table6_223_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcontadorfm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcontadorfm_Internalname, tblTblcontadorfm_Internalname, "", "Table", 0, "", "", 0, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:124px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Funcaion�rio da Prestadora:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 228,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfmcod, dynavContagemresultado_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0)), 1, dynavContagemresultado_contadorfmcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,228);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", (String)(dynavContagemresultado_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcolcrfs2_Internalname, "ou coluna:", "", "", lblTbcolcrfs2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 232,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColcontadorfm_Internalname, StringUtil.RTrim( AV101ColContadorFM), StringUtil.RTrim( context.localUtil.Format( AV101ColContadorFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,232);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColcontadorfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:5px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdtcnt_Internalname, "Data da Contagem:", "", "", lblTbdtcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbdtcnt_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 238,'',false,'" + sGXsfl_373_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatacnt_Internalname, context.localUtil.Format(AV146DataCnt, "99/99/99"), context.localUtil.Format( AV146DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,238);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatacnt_Jsonclick, 0, "AttributeCustom", "", "", "", edtavDatacnt_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            GxWebStd.gx_bitmap( context, edtavDatacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavDatacnt_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcoldtcnt_Internalname, "ou coluna:", "", "", lblTbcoldtcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbcoldtcnt_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 242,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdatacnt_Internalname, StringUtil.RTrim( AV147ColDataCnt), StringUtil.RTrim( context.localUtil.Format( AV147ColDataCnt, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,242);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdatacnt_Jsonclick, 0, "AttributeCustom", "", "", "", edtavColdatacnt_Visible, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcoldtcnt2_Internalname, "Parecer t�cnico:", "", "", lblTbcoldtcnt2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 246,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColparecer_Internalname, StringUtil.RTrim( AV173ColParecer), StringUtil.RTrim( context.localUtil.Format( AV173ColParecer, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,246);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColparecer_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_223_BL2e( true) ;
         }
         else
         {
            wb_table6_223_BL2e( false) ;
         }
      }

      protected void wb_table5_119_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTbldadosdmn_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTbldadosdmn_Internalname, tblTbldadosdmn_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:87px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock45_Internalname, "1� Linha de dados:", "", "", lblTextblock45_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:88px")+"\" class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60PraLinha), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60PraLinha), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinha_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 35, "px", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock18_Internalname, "COLUNAS :: OS Ref �", "", "", lblTextblock18_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColosfs_Internalname, StringUtil.RTrim( AV75ColOSFS), StringUtil.RTrim( context.localUtil.Format( AV75ColOSFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColosfs_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock48_Internalname, "Assunto (Titulo) �", "", "", lblTextblock48_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "Descri��o", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdescricao_Internalname, StringUtil.RTrim( AV80ColDescricao), StringUtil.RTrim( context.localUtil.Format( AV80ColDescricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdescricao_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock46_Internalname, "Projeto (Sistema) �", "", "", lblTextblock46_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColprojeto_Internalname, StringUtil.RTrim( AV166ColProjeto), StringUtil.RTrim( context.localUtil.Format( AV166ColProjeto, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColprojeto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock25_Internalname, "PFB OS �", "", "", lblTextblock25_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfm_Internalname, StringUtil.RTrim( AV97ColPFBFM), StringUtil.RTrim( context.localUtil.Format( AV97ColPFBFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpflfm_Internalname, "PFL OS �", "", "", lblTbpflfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfm_Internalname, StringUtil.RTrim( AV98ColPFLFM), StringUtil.RTrim( context.localUtil.Format( AV98ColPFLFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpflfm2_Internalname, "PFB OS Ref �", "", "", lblTbpflfm2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfsdmn_Internalname, StringUtil.RTrim( AV138ColPFBFSDmn), StringUtil.RTrim( context.localUtil.Format( AV138ColPFBFSDmn, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfsdmn_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpflfm4_Internalname, "PFL OS Ref �", "", "", lblTbpflfm4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfsdmn_Internalname, StringUtil.RTrim( AV139ColPFLFSDmn), StringUtil.RTrim( context.localUtil.Format( AV139ColPFLFSDmn, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,155);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfsdmn_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbehvalidacao_Internalname, "Tipo:", "", "", lblTbehvalidacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbehvalidacao_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_ehvalidacao, cmbavContagemresultado_ehvalidacao_Internalname, StringUtil.BoolToStr( AV20ContagemResultado_EhValidacao), 1, cmbavContagemresultado_ehvalidacao_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e28bl1_client"+"'", "boolean", "", cmbavContagemresultado_ehvalidacao.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavContagemresultado_ehvalidacao.CurrentValue = StringUtil.BoolToStr( AV20ContagemResultado_EhValidacao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_ehvalidacao_Internalname, "Values", (String)(cmbavContagemresultado_ehvalidacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock19_Internalname, "Sigla do Sistema �", "", "", lblTextblock19_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColnomesistema_Internalname, StringUtil.RTrim( AV76ColNomeSistema), StringUtil.RTrim( context.localUtil.Format( AV76ColNomeSistema, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,164);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColnomesistema_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock44_Internalname, "Sigla do M�dulo �", "", "", lblTextblock44_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColnomemodulo_Internalname, StringUtil.RTrim( AV162ColNomeModulo), StringUtil.RTrim( context.localUtil.Format( AV162ColNomeModulo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,168);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColnomemodulo_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock47_Internalname, "Tipo manuten��o �", "", "", lblTextblock47_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColtipo_Internalname, StringUtil.RTrim( AV168ColTipo), StringUtil.RTrim( context.localUtil.Format( AV168ColTipo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColtipo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbosfm_Internalname, lblTbosfm_Caption, "", "", lblTbosfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 178,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOsfm_Internalname, AV57OSFM, StringUtil.RTrim( context.localUtil.Format( AV57OSFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,178);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOsfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, edtavOsfm_Enabled, 1, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcosfm_Internalname, lblTbcosfm_Caption, "", "", lblTbcosfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 182,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColosfm_Internalname, StringUtil.RTrim( AV77ColOSFM), StringUtil.RTrim( context.localUtil.Format( AV77ColOSFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,182);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColosfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, edtavColosfm_Enabled, 1, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock41_Internalname, "Agrupador:", "", "", lblTextblock41_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAgrupador_Internalname, StringUtil.RTrim( AV153Agrupador), StringUtil.RTrim( context.localUtil.Format( AV153Agrupador, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,186);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgrupador_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Data da OS:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 192,'',false,'" + sGXsfl_373_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataosfm_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataosfm_Internalname, context.localUtil.Format(AV29DataOSFM, "99/99/99"), context.localUtil.Format( AV29DataOSFM, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,192);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataosfm_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            GxWebStd.gx_bitmap( context, edtavDataosfm_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock21_Internalname, "ou coluna:", "", "", lblTextblock21_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdatadmn_Internalname, StringUtil.RTrim( AV78ColDataDmn), StringUtil.RTrim( context.localUtil.Format( AV78ColDataDmn, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,196);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdatadmn_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprazoentrega_Internalname, lblTextblockprazoentrega_Caption, "", "", lblTextblockprazoentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 199,'',false,'" + sGXsfl_373_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCalcularprazo_Internalname, StringUtil.BoolToStr( AV175CalcularPrazo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(199, this, 'true', 'false');gx.ajax.executeCliEvent('e29bl1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,199);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 201,'',false,'" + sGXsfl_373_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataentrega_Internalname, context.localUtil.TToC( AV151DataEntrega, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV151DataEntrega, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,201);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataentrega_Jsonclick, 0, "AttributeCustom", "", "", "", 1, edtavDataentrega_Enabled, 1, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            GxWebStd.gx_bitmap( context, edtavDataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavDataentrega_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Descri��o complementar:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Required'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 207,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_observacao_Internalname, AV79ContagemResultado_Observacao, StringUtil.RTrim( context.localUtil.Format( AV79ContagemResultado_Observacao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,207);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_observacao_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 500, 0, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock22_Internalname, "ou coluna:", "", "", lblTextblock22_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 211,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColobservacao_Internalname, StringUtil.RTrim( AV170ColObservacao), StringUtil.RTrim( context.localUtil.Format( AV170ColObservacao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,211);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColobservacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\" >") ;
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock24_Internalname, "Link:", "", "", lblTextblock24_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Required'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 217,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_link_Internalname, AV83ContagemResultado_Link, AV83ContagemResultado_Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,217);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_link_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 400, "px", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock26_Internalname, "ou coluna:", "", "", lblTextblock26_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 221,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCollink_Internalname, StringUtil.RTrim( AV84ColLink), StringUtil.RTrim( context.localUtil.Format( AV84ColLink, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,221);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCollink_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_119_BL2e( true) ;
         }
         else
         {
            wb_table5_119_BL2e( false) ;
         }
      }

      protected void wb_table4_95_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblsrvvnc_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblsrvvnc_Internalname, tblTblsrvvnc_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroup1_Internalname, "Servi�o vinculado:", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_ImportFromFile.htm");
            wb_table15_99_BL2( true) ;
         }
         else
         {
            wb_table15_99_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table15_99_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadasrv_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV136ContratadaSrv_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV136ContratadaSrv_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadasrv_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratadasrv_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportFromFile.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOsservico_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV126OSServico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV126OSServico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOsservico_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavOsservico_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_95_BL2e( true) ;
         }
         else
         {
            wb_table4_95_BL2e( false) ;
         }
      }

      protected void wb_table15_99_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblcontratadasrv_Internalname, tblTblcontratadasrv_Internalname, "", "Table", 0, "", "", 1, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock40_Internalname, "Prestadora:", "", "", lblTextblock40_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontratadasrv_codigo, dynavSelcontratadasrv_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0)), 1, dynavSelcontratadasrv_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSELCONTRATADASRV_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavSelcontratadasrv_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV135SelContratadaSrv_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratadasrv_codigo_Internalname, "Values", (String)(dynavSelcontratadasrv_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Criar 2� demanda com Servi�o vinculado de:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelosservico_codigo, dynavSelosservico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0)), 1, dynavSelosservico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSELOSSERVICO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavSelosservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelosservico_codigo_Internalname, "Values", (String)(dynavSelosservico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontrato2_Internalname, "Contrato:", "", "", lblTbcontrato2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosos_codigo, cmbavContratoservicosos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0)), 1, cmbavContratoservicosos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavContratoservicosos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV177ContratoServicosOS_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosos_codigo_Internalname, "Values", (String)(cmbavContratoservicosos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\"  style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_99_BL2e( true) ;
         }
         else
         {
            wb_table15_99_BL2e( false) ;
         }
      }

      protected void wb_table3_63_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcast_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcast_Internalname, tblTblcast_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock28_Internalname, "1� Linha de dados:", "", "", lblTextblock28_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralincast_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV116PraLinCAST), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV116PraLinCAST), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralincast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 35, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "COLUNAS :: Demandas -", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdmncast_Internalname, StringUtil.RTrim( AV114ColDmnCAST), StringUtil.RTrim( context.localUtil.Format( AV114ColDmnCAST, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdmncast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 1, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table16_75_BL2( true) ;
         }
         else
         {
            wb_table16_75_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table16_75_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock32_Internalname, "PFB OS -", "", "", lblTextblock32_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfmcast_Internalname, StringUtil.RTrim( AV111ColPFBFMCAST), StringUtil.RTrim( context.localUtil.Format( AV111ColPFBFMCAST, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfmcast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock33_Internalname, "PFL OS -", "", "", lblTextblock33_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfmcast_Internalname, StringUtil.RTrim( AV112ColPFLFMCAST), StringUtil.RTrim( context.localUtil.Format( AV112ColPFLFMCAST, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfmcast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_BL2e( true) ;
         }
         else
         {
            wb_table3_63_BL2e( false) ;
         }
      }

      protected void wb_table16_75_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblpffs_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblpffs_Internalname, tblTblpffs_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock30_Internalname, "PFB OS Ref -", "", "", lblTextblock30_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfscast_Internalname, StringUtil.RTrim( AV109ColPFBFSCAST), StringUtil.RTrim( context.localUtil.Format( AV109ColPFBFSCAST, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfscast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock31_Internalname, "PFL OS Ref -", "", "", lblTextblock31_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfscast_Internalname, StringUtil.RTrim( AV110ColPFLFSCAST), StringUtil.RTrim( context.localUtil.Format( AV110ColPFLFSCAST, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfscast_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_75_BL2e( true) ;
         }
         else
         {
            wb_table16_75_BL2e( false) ;
         }
      }

      protected void wb_table2_12_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table17_15_BL2( true) ;
         }
         else
         {
            wb_table17_15_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table17_15_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_373_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV120Servico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV120Servico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavServico_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_12_BL2e( true) ;
         }
         else
         {
            wb_table2_12_BL2e( false) ;
         }
      }

      protected void wb_table17_15_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcontratada_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcontratada_Internalname, tblTblcontratada_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  style=\""+CSSHelper.Prettify( "height:35px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontpres2_Internalname, "Requisitante:", "", "", lblTbcontpres2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "&nbsp;") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRequisitante, cmbavRequisitante_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0)), 1, cmbavRequisitante_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVREQUISITANTE.CLICK."+"'", "int", "", 1, cmbavRequisitante.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavRequisitante.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV178Requisitante), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitante_Internalname, "Values", (String)(cmbavRequisitante.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"7\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_373_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSrvparaftr_Internalname, StringUtil.BoolToStr( AV143SrvParaFtr), "", "", chkavSrvparaftr.Visible, 1, "true", "Apenas para Faturar", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(22, this, 'true', 'false');gx.ajax.executeCliEvent('e22bl2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp; ") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_373_idx + "',0)\"";
            ClassString = "BlobContentCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavNotificar_Internalname, StringUtil.BoolToStr( AV154Notificar), "", "", chkavNotificar.Visible, 1, "true", "Enviar notifica��o para a Prestadora", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"7\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontpres_Internalname, lblTbcontpres_Caption, "", "", lblTbcontpres_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratada_codigo, cmbavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0)), 1, cmbavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV127Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratada_codigo_Internalname, "Values", (String)(cmbavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico_Internalname, lblTbservico_Caption, "", "", lblTbservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbservico_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelservico_codigo, dynavSelservico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0)), 1, dynavSelservico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSELSERVICO_CODIGO.CLICK."+"'", "int", "", dynavSelservico_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Values", (String)(dynavSelservico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontrato_Internalname, "Contrato:", "", "", lblTbcontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbcontrato_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_codigo, cmbavContratoservicos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0)), 1, cmbavContratoservicos_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATOSERVICOS_CODIGO.CLICK."+"'", "int", "", cmbavContratoservicos_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV176ContratoServicos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", (String)(cmbavContratoservicos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table18_40_BL2( true) ;
         }
         else
         {
            wb_table18_40_BL2( false) ;
         }
         return  ;
      }

      protected void wb_table18_40_BL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontorigem_Internalname, lblTbcontorigem_Caption, "", "", lblTbcontorigem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbcontorigem_Visible, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratadaorigem_codigo, dynavContratadaorigem_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0)), 1, dynavContratadaorigem_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADAORIGEM_CODIGO.CLICK."+"'", "int", "", dynavContratadaorigem_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WP_ImportFromFile.htm");
            dynavContratadaorigem_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV150ContratadaOrigem_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_codigo_Internalname, "Values", (String)(dynavContratadaorigem_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_15_BL2e( true) ;
         }
         else
         {
            wb_table17_15_BL2e( false) ;
         }
      }

      protected void wb_table18_40_BL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblsrvfat_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblsrvfat_Internalname, tblTblsrvfat_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbstatus_Internalname, "Status:", "", "", lblTbstatus_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusdmn, cmbavStatusdmn_Internalname, StringUtil.RTrim( AV121StatusDmn), 1, cmbavStatusdmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavStatusdmn.CurrentValue = StringUtil.RTrim( AV121StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdmn_Internalname, "Values", (String)(cmbavStatusdmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock50_Internalname, "Data da Contagem:", "", "", lblTextblock50_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportFromFile.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_373_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDatasrvfat, cmbavDatasrvfat_Internalname, StringUtil.RTrim( AV142DataSrvFat), 1, cmbavDatasrvfat_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e30bl1_client"+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WP_ImportFromFile.htm");
            cmbavDatasrvfat.CurrentValue = StringUtil.RTrim( AV142DataSrvFat);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDatasrvfat_Internalname, "Values", (String)(cmbavDatasrvfat.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_40_BL2e( true) ;
         }
         else
         {
            wb_table18_40_BL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABL2( ) ;
         WSBL2( ) ;
         WEBL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( StringUtil.StrCmp(WebComp_Wc_anexos_Component, "") == 0 )
         {
            WebComp_Wc_anexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wc_anexos.ComponentInit();
            WebComp_Wc_anexos.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wc_anexos_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( ! ( WebComp_Wc_anexos == null ) )
         {
            WebComp_Wc_anexos.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216205075");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_importfromfile.js", "?20206216205078");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_3732( )
      {
         edtavCtlsistemanom_Internalname = "CTLSISTEMANOM_"+sGXsfl_373_idx;
         edtavCtldemandafm_Internalname = "CTLDEMANDAFM_"+sGXsfl_373_idx;
         edtavCtldatadmn_Internalname = "CTLDATADMN_"+sGXsfl_373_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_373_idx;
         edtavCtldataent_Internalname = "CTLDATAENT_"+sGXsfl_373_idx;
         edtavCtldemanda_Internalname = "CTLDEMANDA_"+sGXsfl_373_idx;
         edtavCtlcontadorfsnom_Internalname = "CTLCONTADORFSNOM_"+sGXsfl_373_idx;
         edtavCtllink_Internalname = "CTLLINK_"+sGXsfl_373_idx;
         edtavCtlpfbfm_Internalname = "CTLPFBFM_"+sGXsfl_373_idx;
         edtavCtlpflfm_Internalname = "CTLPFLFM_"+sGXsfl_373_idx;
         edtavCtlpfbfs_Internalname = "CTLPFBFS_"+sGXsfl_373_idx;
         edtavCtlpflfs_Internalname = "CTLPFLFS_"+sGXsfl_373_idx;
         edtavCtlcontadorfmcod_Internalname = "CTLCONTADORFMCOD_"+sGXsfl_373_idx;
      }

      protected void SubsflControlProps_fel_3732( )
      {
         edtavCtlsistemanom_Internalname = "CTLSISTEMANOM_"+sGXsfl_373_fel_idx;
         edtavCtldemandafm_Internalname = "CTLDEMANDAFM_"+sGXsfl_373_fel_idx;
         edtavCtldatadmn_Internalname = "CTLDATADMN_"+sGXsfl_373_fel_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_373_fel_idx;
         edtavCtldataent_Internalname = "CTLDATAENT_"+sGXsfl_373_fel_idx;
         edtavCtldemanda_Internalname = "CTLDEMANDA_"+sGXsfl_373_fel_idx;
         edtavCtlcontadorfsnom_Internalname = "CTLCONTADORFSNOM_"+sGXsfl_373_fel_idx;
         edtavCtllink_Internalname = "CTLLINK_"+sGXsfl_373_fel_idx;
         edtavCtlpfbfm_Internalname = "CTLPFBFM_"+sGXsfl_373_fel_idx;
         edtavCtlpflfm_Internalname = "CTLPFLFM_"+sGXsfl_373_fel_idx;
         edtavCtlpfbfs_Internalname = "CTLPFBFS_"+sGXsfl_373_fel_idx;
         edtavCtlpflfs_Internalname = "CTLPFLFS_"+sGXsfl_373_fel_idx;
         edtavCtlcontadorfmcod_Internalname = "CTLCONTADORFMCOD_"+sGXsfl_373_fel_idx;
      }

      protected void sendrow_3732( )
      {
         SubsflControlProps_3732( ) ;
         WBBL0( ) ;
         GridsdtdemandasRow = GXWebRow.GetNew(context,GridsdtdemandasContainer);
         if ( subGridsdtdemandas_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridsdtdemandas_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridsdtdemandas_Class, "") != 0 )
            {
               subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Odd";
            }
         }
         else if ( subGridsdtdemandas_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridsdtdemandas_Backstyle = 0;
            subGridsdtdemandas_Backcolor = subGridsdtdemandas_Allbackcolor;
            if ( StringUtil.StrCmp(subGridsdtdemandas_Class, "") != 0 )
            {
               subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Uniform";
            }
         }
         else if ( subGridsdtdemandas_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridsdtdemandas_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridsdtdemandas_Class, "") != 0 )
            {
               subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Odd";
            }
            subGridsdtdemandas_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridsdtdemandas_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridsdtdemandas_Backstyle = 1;
            if ( ((int)((nGXsfl_373_idx) % (2))) == 0 )
            {
               subGridsdtdemandas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridsdtdemandas_Class, "") != 0 )
               {
                  subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Even";
               }
            }
            else
            {
               subGridsdtdemandas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridsdtdemandas_Class, "") != 0 )
               {
                  subGridsdtdemandas_Linesclass = subGridsdtdemandas_Class+"Odd";
               }
            }
         }
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridsdtdemandas_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_373_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsistemanom_Internalname,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Sistemanom,StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Sistemanom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlsistemanom_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlsistemanom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldemandafm_Internalname,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Demandafm,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldemandafm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldemandafm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldatadmn_Internalname,context.localUtil.Format(((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Datadmn, "99/99/99"),context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Datadmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldatadmn_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldatadmn_Enabled,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescricao_Internalname,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldescricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldescricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)1,(String)"row",(short)500,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldataent_Internalname,context.localUtil.TToC( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Dataent, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Dataent, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldataent_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldataent_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)373,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldemanda_Internalname,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Demanda,StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldemanda_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldemanda_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcontadorfsnom_Internalname,StringUtil.RTrim( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Contadorfsnom),StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Contadorfsnom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcontadorfsnom_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlcontadorfsnom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtllink_Internalname,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Link,((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Link,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtllink_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtllink_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)1,(String)"row",(int)2097152,(short)0,(short)1,(short)373,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlpfbfm_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfm, 14, 5, ",", "")),((edtavCtlpfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfm, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfm, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlpfbfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlpfbfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlpflfm_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfm, 14, 5, ",", "")),((edtavCtlpflfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfm, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfm, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlpflfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlpflfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlpfbfs_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfs, 14, 5, ",", "")),((edtavCtlpfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfs, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pfbfs, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlpfbfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlpfbfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlpflfs_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfs, 14, 5, ",", "")),((edtavCtlpflfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfs, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Pflfs, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlpflfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlpflfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridsdtdemandasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridsdtdemandasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcontadorfmcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Contadorfmcod), 6, 0, ",", "")),((edtavCtlcontadorfmcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Contadorfmcod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Demandas_Demanda)AV93SDT_Demandas.Item(AV184GXV1)).gxTpr_Contadorfmcod), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcontadorfmcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlcontadorfmcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)373,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         GXCCtl = "vBLOB_" + sGXsfl_373_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV158Blob);
         GridsdtdemandasContainer.AddRow(GridsdtdemandasRow);
         nGXsfl_373_idx = (short)(((subGridsdtdemandas_Islastpage==1)&&(nGXsfl_373_idx+1>subGridsdtdemandas_Recordsperpage( )) ? 1 : nGXsfl_373_idx+1));
         sGXsfl_373_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_373_idx), 4, 0)), 4, "0");
         SubsflControlProps_3732( ) ;
         /* End function sendrow_3732 */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavOqimportar_Internalname = "vOQIMPORTAR";
         lblTbcontpres2_Internalname = "TBCONTPRES2";
         cmbavRequisitante_Internalname = "vREQUISITANTE";
         chkavSrvparaftr_Internalname = "vSRVPARAFTR";
         chkavNotificar_Internalname = "vNOTIFICAR";
         lblTbcontpres_Internalname = "TBCONTPRES";
         cmbavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTbservico_Internalname = "TBSERVICO";
         dynavSelservico_codigo_Internalname = "vSELSERVICO_CODIGO";
         lblTbcontrato_Internalname = "TBCONTRATO";
         cmbavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         lblTbstatus_Internalname = "TBSTATUS";
         cmbavStatusdmn_Internalname = "vSTATUSDMN";
         lblTextblock50_Internalname = "TEXTBLOCK50";
         cmbavDatasrvfat_Internalname = "vDATASRVFAT";
         tblTblsrvfat_Internalname = "TBLSRVFAT";
         lblTbcontorigem_Internalname = "TBCONTORIGEM";
         dynavContratadaorigem_codigo_Internalname = "vCONTRATADAORIGEM_CODIGO";
         tblTblcontratada_Internalname = "TBLCONTRATADA";
         edtavServico_codigo_Internalname = "vSERVICO_CODIGO";
         tblTable2_Internalname = "TABLE2";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         edtavPralincast_Internalname = "vPRALINCAST";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         edtavColdmncast_Internalname = "vCOLDMNCAST";
         lblTextblock30_Internalname = "TEXTBLOCK30";
         edtavColpfbfscast_Internalname = "vCOLPFBFSCAST";
         lblTextblock31_Internalname = "TEXTBLOCK31";
         edtavColpflfscast_Internalname = "vCOLPFLFSCAST";
         tblTblpffs_Internalname = "TBLPFFS";
         lblTextblock32_Internalname = "TEXTBLOCK32";
         edtavColpfbfmcast_Internalname = "vCOLPFBFMCAST";
         lblTextblock33_Internalname = "TEXTBLOCK33";
         edtavColpflfmcast_Internalname = "vCOLPFLFMCAST";
         tblTblcast_Internalname = "TBLCAST";
         lblTextblock40_Internalname = "TEXTBLOCK40";
         dynavSelcontratadasrv_codigo_Internalname = "vSELCONTRATADASRV_CODIGO";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         dynavSelosservico_codigo_Internalname = "vSELOSSERVICO_CODIGO";
         lblTbcontrato2_Internalname = "TBCONTRATO2";
         cmbavContratoservicosos_codigo_Internalname = "vCONTRATOSERVICOSOS_CODIGO";
         tblTblcontratadasrv_Internalname = "TBLCONTRATADASRV";
         edtavContratadasrv_codigo_Internalname = "vCONTRATADASRV_CODIGO";
         edtavOsservico_codigo_Internalname = "vOSSERVICO_CODIGO";
         grpGroup1_Internalname = "GROUP1";
         tblTblsrvvnc_Internalname = "TBLSRVVNC";
         lblTextblock45_Internalname = "TEXTBLOCK45";
         edtavPralinha_Internalname = "vPRALINHA";
         lblTextblock18_Internalname = "TEXTBLOCK18";
         edtavColosfs_Internalname = "vCOLOSFS";
         lblTextblock48_Internalname = "TEXTBLOCK48";
         edtavColdescricao_Internalname = "vCOLDESCRICAO";
         lblTextblock46_Internalname = "TEXTBLOCK46";
         edtavColprojeto_Internalname = "vCOLPROJETO";
         lblTextblock25_Internalname = "TEXTBLOCK25";
         edtavColpfbfm_Internalname = "vCOLPFBFM";
         lblTbpflfm_Internalname = "TBPFLFM";
         edtavColpflfm_Internalname = "vCOLPFLFM";
         lblTbpflfm2_Internalname = "TBPFLFM2";
         edtavColpfbfsdmn_Internalname = "vCOLPFBFSDMN";
         lblTbpflfm4_Internalname = "TBPFLFM4";
         edtavColpflfsdmn_Internalname = "vCOLPFLFSDMN";
         lblTbehvalidacao_Internalname = "TBEHVALIDACAO";
         cmbavContagemresultado_ehvalidacao_Internalname = "vCONTAGEMRESULTADO_EHVALIDACAO";
         lblTextblock19_Internalname = "TEXTBLOCK19";
         edtavColnomesistema_Internalname = "vCOLNOMESISTEMA";
         lblTextblock44_Internalname = "TEXTBLOCK44";
         edtavColnomemodulo_Internalname = "vCOLNOMEMODULO";
         lblTextblock47_Internalname = "TEXTBLOCK47";
         edtavColtipo_Internalname = "vCOLTIPO";
         lblTbosfm_Internalname = "TBOSFM";
         edtavOsfm_Internalname = "vOSFM";
         lblTbcosfm_Internalname = "TBCOSFM";
         edtavColosfm_Internalname = "vCOLOSFM";
         lblTextblock41_Internalname = "TEXTBLOCK41";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDataosfm_Internalname = "vDATAOSFM";
         lblTextblock21_Internalname = "TEXTBLOCK21";
         edtavColdatadmn_Internalname = "vCOLDATADMN";
         lblTextblockprazoentrega_Internalname = "TEXTBLOCKPRAZOENTREGA";
         chkavCalcularprazo_Internalname = "vCALCULARPRAZO";
         edtavDataentrega_Internalname = "vDATAENTREGA";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavContagemresultado_observacao_Internalname = "vCONTAGEMRESULTADO_OBSERVACAO";
         lblTextblock22_Internalname = "TEXTBLOCK22";
         edtavColobservacao_Internalname = "vCOLOBSERVACAO";
         lblTextblock24_Internalname = "TEXTBLOCK24";
         edtavContagemresultado_link_Internalname = "vCONTAGEMRESULTADO_LINK";
         lblTextblock26_Internalname = "TEXTBLOCK26";
         edtavCollink_Internalname = "vCOLLINK";
         tblTbldadosdmn_Internalname = "TBLDADOSDMN";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         dynavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTbcolcrfs2_Internalname = "TBCOLCRFS2";
         edtavColcontadorfm_Internalname = "vCOLCONTADORFM";
         lblTbdtcnt_Internalname = "TBDTCNT";
         edtavDatacnt_Internalname = "vDATACNT";
         lblTbcoldtcnt_Internalname = "TBCOLDTCNT";
         edtavColdatacnt_Internalname = "vCOLDATACNT";
         lblTbcoldtcnt2_Internalname = "TBCOLDTCNT2";
         edtavColparecer_Internalname = "vCOLPARECER";
         tblTblcontadorfm_Internalname = "TBLCONTADORFM";
         lblTbcontadorfs_Internalname = "TBCONTADORFS";
         dynavContagemresultado_contadorfscod_Internalname = "vCONTAGEMRESULTADO_CONTADORFSCOD";
         lblTbcolcrfs_Internalname = "TBCOLCRFS";
         edtavColcontadorfs_Internalname = "vCOLCONTADORFS";
         tblTblcontadorfs_Internalname = "TBLCONTADORFS";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavDataosfm2_Internalname = "vDATAOSFM2";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         edtavOsfm2_Internalname = "vOSFM2";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavMinutos2_Internalname = "vMINUTOS2";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavContagemresultado_valorpf_Internalname = "vCONTAGEMRESULTADO_VALORPF";
         tblTbldadosms_Internalname = "TBLDADOSMS";
         lblTextblock16_Internalname = "TEXTBLOCK16";
         edtavPralinhactrf_Internalname = "vPRALINHACTRF";
         lblTextblock11_Internalname = "TEXTBLOCK11";
         edtavColdemandas_Internalname = "vCOLDEMANDAS";
         lblTextblock17_Internalname = "TEXTBLOCK17";
         edtavColpfbfmctrf_Internalname = "vCOLPFBFMCTRF";
         lblTextblock42_Internalname = "TEXTBLOCK42";
         edtavColpflfmctrf_Internalname = "vCOLPFLFMCTRF";
         tblTblpffm_Internalname = "TBLPFFM";
         lblTextblock12_Internalname = "TEXTBLOCK12";
         edtavColpfbfs_Internalname = "vCOLPFBFS";
         lblTextblock13_Internalname = "TEXTBLOCK13";
         edtavColpflfs_Internalname = "vCOLPFLFS";
         lblTextblock14_Internalname = "TEXTBLOCK14";
         edtavColdeflator_Internalname = "vCOLDEFLATOR";
         lblTextblock15_Internalname = "TEXTBLOCK15";
         cmbavAcao_Internalname = "vACAO";
         lblTextblock43_Internalname = "TEXTBLOCK43";
         edtavOsfmctrf_Internalname = "vOSFMCTRF";
         tblTblconftrf_Internalname = "TBLCONFTRF";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         tblTblanexos_Internalname = "TBLANEXOS";
         edtavBlob_Internalname = "vBLOB";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavAba_Internalname = "vABA";
         lblTextblock37_Internalname = "TEXTBLOCK37";
         edtavColfiltro_Internalname = "vCOLFILTRO";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         cmbavTipofiltro_Internalname = "vTIPOFILTRO";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         edtavFiltrocoluna_Internalname = "vFILTROCOLUNA";
         tblTblfiltrocoluna_Internalname = "TBLFILTROCOLUNA";
         tblTablegrid_Internalname = "TABLEGRID";
         edtavCtlsistemanom_Internalname = "CTLSISTEMANOM";
         edtavCtldemandafm_Internalname = "CTLDEMANDAFM";
         edtavCtldatadmn_Internalname = "CTLDATADMN";
         edtavCtldescricao_Internalname = "CTLDESCRICAO";
         edtavCtldataent_Internalname = "CTLDATAENT";
         edtavCtldemanda_Internalname = "CTLDEMANDA";
         edtavCtlcontadorfsnom_Internalname = "CTLCONTADORFSNOM";
         edtavCtllink_Internalname = "CTLLINK";
         edtavCtlpfbfm_Internalname = "CTLPFBFM";
         edtavCtlpflfm_Internalname = "CTLPFLFM";
         edtavCtlpfbfs_Internalname = "CTLPFBFS";
         edtavCtlpflfs_Internalname = "CTLPFLFS";
         edtavCtlcontadorfmcod_Internalname = "CTLCONTADORFMCOD";
         tblTablesdtdemandas_Internalname = "TABLESDTDEMANDAS";
         bttCarregar_Internalname = "CARREGAR";
         bttImportar_Internalname = "IMPORTAR";
         tblTable1_Internalname = "TABLE1";
         Innewwindow_Internalname = "INNEWWINDOW";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         edtavFilename_Internalname = "vFILENAME";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         Confirmpanel2_Internalname = "CONFIRMPANEL2";
         Form.Internalname = "FORM";
         subGridsdtdemandas_Internalname = "GRIDSDTDEMANDAS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlcontadorfmcod_Jsonclick = "";
         edtavCtlpflfs_Jsonclick = "";
         edtavCtlpfbfs_Jsonclick = "";
         edtavCtlpflfm_Jsonclick = "";
         edtavCtlpfbfm_Jsonclick = "";
         edtavCtllink_Jsonclick = "";
         edtavCtlcontadorfsnom_Jsonclick = "";
         edtavCtldemanda_Jsonclick = "";
         edtavCtldataent_Jsonclick = "";
         edtavCtldescricao_Jsonclick = "";
         edtavCtldatadmn_Jsonclick = "";
         edtavCtldemandafm_Jsonclick = "";
         edtavCtlsistemanom_Jsonclick = "";
         cmbavDatasrvfat_Jsonclick = "";
         cmbavStatusdmn_Jsonclick = "";
         dynavContratadaorigem_codigo_Jsonclick = "";
         lblTbcontorigem_Visible = 1;
         cmbavContratoservicos_codigo_Jsonclick = "";
         lblTbcontrato_Visible = 1;
         dynavSelservico_codigo_Jsonclick = "";
         lblTbservico_Visible = 1;
         cmbavContratada_codigo_Jsonclick = "";
         cmbavRequisitante_Jsonclick = "";
         edtavServico_codigo_Jsonclick = "";
         edtavColpflfscast_Jsonclick = "";
         edtavColpfbfscast_Jsonclick = "";
         edtavColpflfmcast_Jsonclick = "";
         edtavColpfbfmcast_Jsonclick = "";
         edtavColdmncast_Jsonclick = "";
         edtavPralincast_Jsonclick = "";
         cmbavContratoservicosos_codigo_Jsonclick = "";
         dynavSelosservico_codigo_Jsonclick = "";
         dynavSelcontratadasrv_codigo_Jsonclick = "";
         edtavOsservico_codigo_Jsonclick = "";
         edtavContratadasrv_codigo_Jsonclick = "";
         edtavCollink_Jsonclick = "";
         edtavContagemresultado_link_Jsonclick = "";
         edtavColobservacao_Jsonclick = "";
         edtavContagemresultado_observacao_Jsonclick = "";
         edtavDataentrega_Jsonclick = "";
         edtavColdatadmn_Jsonclick = "";
         edtavDataosfm_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavColosfm_Jsonclick = "";
         edtavOsfm_Jsonclick = "";
         edtavColtipo_Jsonclick = "";
         edtavColnomemodulo_Jsonclick = "";
         edtavColnomesistema_Jsonclick = "";
         cmbavContagemresultado_ehvalidacao_Jsonclick = "";
         lblTbehvalidacao_Visible = 1;
         edtavColpflfsdmn_Jsonclick = "";
         edtavColpfbfsdmn_Jsonclick = "";
         edtavColpflfm_Jsonclick = "";
         edtavColpfbfm_Jsonclick = "";
         edtavColprojeto_Jsonclick = "";
         edtavColdescricao_Jsonclick = "";
         edtavColosfs_Jsonclick = "";
         edtavPralinha_Jsonclick = "";
         edtavColparecer_Jsonclick = "";
         edtavColdatacnt_Jsonclick = "";
         lblTbcoldtcnt_Visible = 1;
         edtavDatacnt_Jsonclick = "";
         lblTbdtcnt_Visible = 1;
         edtavColcontadorfm_Jsonclick = "";
         dynavContagemresultado_contadorfmcod_Jsonclick = "";
         edtavColcontadorfs_Jsonclick = "";
         dynavContagemresultado_contadorfscod_Jsonclick = "";
         edtavContagemresultado_valorpf_Jsonclick = "";
         edtavMinutos2_Jsonclick = "";
         edtavOsfm2_Jsonclick = "";
         edtavDataosfm2_Jsonclick = "";
         edtavColpflfmctrf_Jsonclick = "";
         edtavColpfbfmctrf_Jsonclick = "";
         tblTblpffm_Visible = 1;
         edtavOsfmctrf_Jsonclick = "";
         cmbavAcao_Jsonclick = "";
         edtavColdeflator_Jsonclick = "";
         edtavColpflfs_Jsonclick = "";
         edtavColpfbfs_Jsonclick = "";
         edtavColdemandas_Jsonclick = "";
         edtavPralinhactrf_Jsonclick = "";
         edtavFiltrocoluna_Jsonclick = "";
         cmbavTipofiltro_Jsonclick = "";
         edtavColfiltro_Jsonclick = "";
         edtavAba_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         subGridsdtdemandas_Allowcollapsing = 0;
         subGridsdtdemandas_Allowselection = 0;
         edtavCtlcontadorfmcod_Enabled = 0;
         edtavCtlpflfs_Enabled = 0;
         edtavCtlpfbfs_Enabled = 0;
         edtavCtlpflfm_Enabled = 0;
         edtavCtlpfbfm_Enabled = 0;
         edtavCtllink_Enabled = 0;
         edtavCtlcontadorfsnom_Enabled = 0;
         edtavCtldemanda_Enabled = 0;
         edtavCtldataent_Enabled = 0;
         edtavCtldescricao_Enabled = 0;
         edtavCtldatadmn_Enabled = 0;
         edtavCtldemandafm_Enabled = 0;
         edtavCtlsistemanom_Enabled = 0;
         subGridsdtdemandas_Class = "WorkWith";
         subGridsdtdemandas_Visible = 1;
         bttImportar_Visible = 1;
         bttCarregar_Visible = 1;
         cmbavOqimportar_Jsonclick = "";
         cmbavRequisitante.Enabled = 1;
         dynavContagemresultado_contadorfscod.Description = "";
         tblTablesdtdemandas_Visible = 1;
         lblTextblockprazoentrega_Caption = "Calcular prazo:";
         lblTbservico_Caption = "Servi�o:";
         edtavColdatacnt_Visible = 1;
         edtavDatacnt_Visible = 1;
         tblTblpffs_Visible = 1;
         lblTbcontorigem_Caption = "Contratada Origem:";
         tblTblsrvfat_Visible = 1;
         tblTblcontadorfs_Visible = 1;
         cmbavContratoservicos_codigo.Visible = 1;
         lblTbcontpres_Caption = "Prestadora:";
         dynavSelservico_codigo.Visible = 1;
         dynavContratadaorigem_codigo.Visible = 1;
         cmbavContagemresultado_ehvalidacao.Visible = 1;
         chkavSrvparaftr.Visible = 1;
         tblTblfiltrocoluna_Visible = 1;
         tblTblsrvvnc_Visible = 1;
         tblTblcontadorfm_Visible = 1;
         tblTblcontratada_Visible = 1;
         tblTbldadosdmn_Visible = 1;
         chkavNotificar.Visible = 1;
         lblTbcosfm_Caption = "ou coluna:";
         lblTbosfm_Caption = "N� OS:";
         edtavColosfm_Enabled = 1;
         edtavOsfm_Enabled = 1;
         tblTblanexos_Visible = 1;
         edtavDataentrega_Enabled = 1;
         edtavOsservico_codigo_Visible = 1;
         edtavContratadasrv_codigo_Visible = 1;
         edtavServico_codigo_Visible = 1;
         tblTablegrid_Visible = 1;
         tblTblcast_Visible = 1;
         tblTblconftrf_Visible = 1;
         tblTbldadosms_Visible = 1;
         bttCarregar_Caption = "Carregar";
         subGridsdtdemandas_Titleforecolor = (int)(0x000000);
         subGridsdtdemandas_Backcolorstyle = 3;
         edtavCtlcontadorfmcod_Enabled = -1;
         edtavCtlpflfs_Enabled = -1;
         edtavCtlpfbfs_Enabled = -1;
         edtavCtlpflfm_Enabled = -1;
         edtavCtlpfbfm_Enabled = -1;
         edtavCtllink_Enabled = -1;
         edtavCtlcontadorfsnom_Enabled = -1;
         edtavCtldemanda_Enabled = -1;
         edtavCtldataent_Enabled = -1;
         edtavCtldescricao_Enabled = -1;
         edtavCtldatadmn_Enabled = -1;
         edtavCtldemandafm_Enabled = -1;
         edtavCtlsistemanom_Enabled = -1;
         chkavCalcularprazo.Caption = "";
         chkavNotificar.Caption = "";
         chkavSrvparaftr.Caption = "";
         edtavFilename_Jsonclick = "";
         edtavFilename_Enabled = 1;
         edtavFilename_Visible = 1;
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Enabled = 1;
         edtavAreatrabalho_codigo_Visible = 1;
         dynavContratadaorigem_codigo.Description = "";
         cmbavContratada_codigo.Description = "";
         Confirmpanel2_Confirmtype = "0";
         Confirmpanel2_Yesbuttoncaption = "Entendi";
         Confirmpanel2_Confirmationtext = "A Prestadora n�o tem Preposto!";
         Confirmpanel2_Title = "Prestadora";
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Yesbuttoncaption = "Entendi";
         Confirmpanel_Confirmationtext = "";
         Confirmpanel_Title = "Aten��o";
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Importar Arquivo";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( GXCombobox cmbGX_Parm1 ,
                                            wwpbaseobjects.SdtWWPContext GX_Parm2 ,
                                            GXCombobox dynGX_Parm3 ,
                                            GXCombobox dynGX_Parm4 ,
                                            GXCombobox dynGX_Parm5 )
      {
         cmbavContratada_codigo = cmbGX_Parm1;
         AV127Contratada_Codigo = (int)(NumberUtil.Val( cmbavContratada_codigo.CurrentValue, "."));
         AV74WWPContext = GX_Parm2;
         dynavSelservico_codigo = dynGX_Parm3;
         AV125SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.CurrentValue, "."));
         dynavContagemresultado_contadorfmcod = dynGX_Parm4;
         AV18ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.CurrentValue, "."));
         dynavContagemresultado_contadorfscod = dynGX_Parm5;
         AV81ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.CurrentValue, "."));
         GXVvSELSERVICO_CODIGO_htmlBL2( AV127Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlBL2( AV74WWPContext, AV127Contratada_Codigo) ;
         GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlBL2( AV127Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavSelservico_codigo.ItemCount > 0 )
         {
            AV125SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0))), "."));
         }
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelServico_Codigo), 6, 0));
         isValidOutput.Add(dynavSelservico_codigo);
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV18ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0))), "."));
         }
         dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contadorfmcod);
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV81ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0))), "."));
         }
         dynavContagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV81ContagemResultado_ContadorFSCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contadorfscod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Selcontratadasrv_codigo( GXCombobox dynGX_Parm1 ,
                                                  GXCombobox dynGX_Parm2 )
      {
         dynavSelcontratadasrv_codigo = dynGX_Parm1;
         AV135SelContratadaSrv_Codigo = (int)(NumberUtil.Val( dynavSelcontratadasrv_codigo.CurrentValue, "."));
         dynavSelosservico_codigo = dynGX_Parm2;
         AV144SelOsServico_Codigo = (int)(NumberUtil.Val( dynavSelosservico_codigo.CurrentValue, "."));
         GXVvSELOSSERVICO_CODIGO_htmlBL2( AV135SelContratadaSrv_Codigo) ;
         dynload_actions( ) ;
         if ( dynavSelosservico_codigo.ItemCount > 0 )
         {
            AV144SelOsServico_Codigo = (int)(NumberUtil.Val( dynavSelosservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0))), "."));
         }
         dynavSelosservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV144SelOsServico_Codigo), 6, 0));
         isValidOutput.Add(dynavSelosservico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDSDTDEMANDAS_nFirstRecordOnPage',nv:0},{av:'GRIDSDTDEMANDAS_nEOF',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''},{av:'AV97ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV111ColPFBFMCAST',fld:'vCOLPFBFMCAST',pic:'@!',nv:''},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV15ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0}],oparms:[{av:'AV74WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV156AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV95OSAutomatica',fld:'vOSAUTOMATICA',pic:'',nv:false},{av:'edtavOsfm_Enabled',ctrl:'vOSFM',prop:'Enabled'},{av:'edtavColosfm_Enabled',ctrl:'vCOLOSFM',prop:'Enabled'},{av:'lblTbosfm_Caption',ctrl:'TBOSFM',prop:'Caption'},{av:'lblTbcosfm_Caption',ctrl:'TBCOSFM',prop:'Caption'},{av:'AV125SelServico_Codigo',fld:'vSELSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV135SelContratadaSrv_Codigo',fld:'vSELCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144SelOsServico_Codigo',fld:'vSELOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV142DataSrvFat',fld:'vDATASRVFAT',pic:'@!',nv:''},{av:'chkavNotificar.Visible',ctrl:'vNOTIFICAR',prop:'Visible'},{av:'AV154Notificar',fld:'vNOTIFICAR',pic:'',nv:false},{av:'AV12ColDem',fld:'vCOLDEM',pic:'ZZZ9',nv:0},{av:'AV14ColPB',fld:'vCOLPB',pic:'ZZZ9',nv:0},{av:'AV17ColPL',fld:'vCOLPL',pic:'ZZZ9',nv:0},{av:'tblTbldadosdmn_Visible',ctrl:'TBLDADOSDMN',prop:'Visible'},{av:'tblTbldadosms_Visible',ctrl:'TBLDADOSMS',prop:'Visible'},{av:'tblTblconftrf_Visible',ctrl:'TBLCONFTRF',prop:'Visible'},{av:'tblTblcontratada_Visible',ctrl:'TBLCONTRATADA',prop:'Visible'},{av:'tblTblcontadorfm_Visible',ctrl:'TBLCONTADORFM',prop:'Visible'},{av:'tblTblcast_Visible',ctrl:'TBLCAST',prop:'Visible'},{av:'subGridsdtdemandas_Visible',ctrl:'GRIDSDTDEMANDAS',prop:'Visible'},{av:'tblTblsrvvnc_Visible',ctrl:'TBLSRVVNC',prop:'Visible'},{av:'tblTblfiltrocoluna_Visible',ctrl:'TBLFILTROCOLUNA',prop:'Visible'},{av:'chkavSrvparaftr.Visible',ctrl:'vSRVPARAFTR',prop:'Visible'},{av:'lblTbehvalidacao_Visible',ctrl:'TBEHVALIDACAO',prop:'Visible'},{av:'cmbavContagemresultado_ehvalidacao'},{av:'dynavContratadaorigem_codigo'},{av:'lblTbcontorigem_Visible',ctrl:'TBCONTORIGEM',prop:'Visible'},{av:'lblTbservico_Visible',ctrl:'TBSERVICO',prop:'Visible'},{av:'dynavSelservico_codigo'},{av:'lblTbcontpres_Caption',ctrl:'TBCONTPRES',prop:'Caption'},{av:'tblTblanexos_Visible',ctrl:'TBLANEXOS',prop:'Visible'},{av:'lblTbcontrato_Visible',ctrl:'TBCONTRATO',prop:'Visible'},{av:'cmbavContratoservicos_codigo'},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavServico_codigo_Visible',ctrl:'vSERVICO_CODIGO',prop:'Visible'},{ctrl:'CARREGAR',prop:'Caption'},{av:'tblTblpffs_Visible',ctrl:'TBLPFFS',prop:'Visible'},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'tblTblcontadorfs_Visible',ctrl:'TBLCONTADORFS',prop:'Visible'},{av:'tblTblsrvfat_Visible',ctrl:'TBLSRVFAT',prop:'Visible'},{av:'lblTbcontorigem_Caption',ctrl:'TBCONTORIGEM',prop:'Caption'},{ctrl:'CARREGAR',prop:'Visible'},{ctrl:'IMPORTAR',prop:'Visible'}]}");
         setEventMetadata("VSELSERVICO_CODIGO.CLICK","{handler:'E13BL2',iparms:[{av:'AV125SelServico_Codigo',fld:'vSELSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'cmbavContratada_codigo'}],oparms:[{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}]}");
         setEventMetadata("VSRVPARAFTR.CLICK","{handler:'E22BL2',iparms:[{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV95OSAutomatica',fld:'vOSAUTOMATICA',pic:'',nv:false}],oparms:[{av:'dynavContratadaorigem_codigo'},{av:'lblTbcontorigem_Visible',ctrl:'TBCONTORIGEM',prop:'Visible'},{av:'tblTblsrvfat_Visible',ctrl:'TBLSRVFAT',prop:'Visible'},{av:'tblTblsrvvnc_Visible',ctrl:'TBLSRVVNC',prop:'Visible'},{av:'lblTbdtcnt_Visible',ctrl:'TBDTCNT',prop:'Visible'},{av:'lblTbcoldtcnt_Visible',ctrl:'TBCOLDTCNT',prop:'Visible'},{av:'edtavDatacnt_Visible',ctrl:'vDATACNT',prop:'Visible'},{av:'edtavColdatacnt_Visible',ctrl:'vCOLDATACNT',prop:'Visible'},{av:'AV150ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTbservico_Caption',ctrl:'TBSERVICO',prop:'Caption'},{av:'AV121StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''}]}");
         setEventMetadata("VDATASRVFAT.CLICK","{handler:'E30BL1',iparms:[{av:'AV142DataSrvFat',fld:'vDATASRVFAT',pic:'@!',nv:''}],oparms:[{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''}]}");
         setEventMetadata("VSELOSSERVICO_CODIGO.CLICK","{handler:'E14BL2',iparms:[{av:'AV144SelOsServico_Codigo',fld:'vSELOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''}],oparms:[{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV177ContratoServicosOS_Codigo',fld:'vCONTRATOSERVICOSOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VOQIMPORTAR.CLICK","{handler:'E15BL2',iparms:[{av:'AV97ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV15ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV96PraLinhaCTRF',fld:'vPRALINHACTRF',pic:'ZZZ9',nv:0},{av:'AV74WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'GRIDSDTDEMANDAS_nFirstRecordOnPage',nv:0},{av:'GRIDSDTDEMANDAS_nEOF',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''},{av:'AV111ColPFBFMCAST',fld:'vCOLPFBFMCAST',pic:'@!',nv:''},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0}],oparms:[{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV12ColDem',fld:'vCOLDEM',pic:'ZZZ9',nv:0},{av:'AV14ColPB',fld:'vCOLPB',pic:'ZZZ9',nv:0},{av:'AV17ColPL',fld:'vCOLPL',pic:'ZZZ9',nv:0},{av:'tblTbldadosdmn_Visible',ctrl:'TBLDADOSDMN',prop:'Visible'},{av:'tblTbldadosms_Visible',ctrl:'TBLDADOSMS',prop:'Visible'},{av:'tblTblconftrf_Visible',ctrl:'TBLCONFTRF',prop:'Visible'},{av:'tblTblcontadorfm_Visible',ctrl:'TBLCONTADORFM',prop:'Visible'},{av:'tblTblcontratada_Visible',ctrl:'TBLCONTRATADA',prop:'Visible'},{av:'tblTblcast_Visible',ctrl:'TBLCAST',prop:'Visible'},{av:'tblTblsrvvnc_Visible',ctrl:'TBLSRVVNC',prop:'Visible'},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'chkavSrvparaftr.Visible',ctrl:'vSRVPARAFTR',prop:'Visible'},{av:'tblTblsrvfat_Visible',ctrl:'TBLSRVFAT',prop:'Visible'},{av:'tblTblfiltrocoluna_Visible',ctrl:'TBLFILTROCOLUNA',prop:'Visible'},{av:'lblTbehvalidacao_Visible',ctrl:'TBEHVALIDACAO',prop:'Visible'},{av:'cmbavContagemresultado_ehvalidacao'},{av:'dynavContratadaorigem_codigo'},{av:'lblTbcontorigem_Visible',ctrl:'TBCONTORIGEM',prop:'Visible'},{av:'lblTbservico_Visible',ctrl:'TBSERVICO',prop:'Visible'},{av:'dynavSelservico_codigo'},{av:'lblTbcontpres_Caption',ctrl:'TBCONTPRES',prop:'Caption'},{av:'tblTblanexos_Visible',ctrl:'TBLANEXOS',prop:'Visible'},{av:'chkavNotificar.Visible',ctrl:'vNOTIFICAR',prop:'Visible'},{av:'AV154Notificar',fld:'vNOTIFICAR',pic:'',nv:false},{av:'lblTbcontrato_Visible',ctrl:'TBCONTRATO',prop:'Visible'},{av:'cmbavContratoservicos_codigo'},{av:'subGridsdtdemandas_Visible',ctrl:'GRIDSDTDEMANDAS',prop:'Visible'},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavServico_codigo_Visible',ctrl:'vSERVICO_CODIGO',prop:'Visible'},{ctrl:'CARREGAR',prop:'Caption'},{av:'tblTblpffs_Visible',ctrl:'TBLPFFS',prop:'Visible'},{av:'tblTblcontadorfs_Visible',ctrl:'TBLCONTADORFS',prop:'Visible'},{ctrl:'IMPORTAR',prop:'Visible'},{ctrl:'CARREGAR',prop:'Visible'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_EHVALIDACAO.CLICK","{handler:'E28BL1',iparms:[{av:'AV20ContagemResultado_EhValidacao',fld:'vCONTAGEMRESULTADO_EHVALIDACAO',pic:'',nv:false}],oparms:[{av:'tblTblcontadorfs_Visible',ctrl:'TBLCONTADORFS',prop:'Visible'}]}");
         setEventMetadata("'ENTER'","{handler:'E16BL2',iparms:[{av:'GRIDSDTDEMANDAS_nFirstRecordOnPage',nv:0},{av:'GRIDSDTDEMANDAS_nEOF',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''},{av:'AV97ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV111ColPFBFMCAST',fld:'vCOLPFBFMCAST',pic:'@!',nv:''},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV15ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0},{av:'AV158Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV134FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV180GestorDaContratada',fld:'vGESTORDACONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV150ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV166ColProjeto',fld:'vCOLPROJETO',pic:'@!',nv:''},{av:'AV80ColDescricao',fld:'vCOLDESCRICAO',pic:'@!',nv:''},{av:'AV75ColOSFS',fld:'vCOLOSFS',pic:'@!',nv:''},{av:'AV76ColNomeSistema',fld:'vCOLNOMESISTEMA',pic:'@!',nv:''},{av:'AV95OSAutomatica',fld:'vOSAUTOMATICA',pic:'',nv:false},{av:'AV77ColOSFM',fld:'vCOLOSFM',pic:'@!',nv:''},{av:'AV57OSFM',fld:'vOSFM',pic:'',nv:''},{av:'AV78ColDataDmn',fld:'vCOLDATADMN',pic:'@!',nv:''},{av:'AV29DataOSFM',fld:'vDATAOSFM',pic:'',nv:''},{av:'AV147ColDataCnt',fld:'vCOLDATACNT',pic:'@!',nv:''},{av:'AV146DataCnt',fld:'vDATACNT',pic:'',nv:''},{av:'AV170ColObservacao',fld:'vCOLOBSERVACAO',pic:'@!',nv:''},{av:'AV79ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV84ColLink',fld:'vCOLLINK',pic:'@!',nv:''},{av:'AV83ContagemResultado_Link',fld:'vCONTAGEMRESULTADO_LINK',pic:'',nv:''},{av:'AV98ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''},{av:'AV125SelServico_Codigo',fld:'vSELSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV121StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''},{av:'AV18ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV135SelContratadaSrv_Codigo',fld:'vSELCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV177ContratoServicosOS_Codigo',fld:'vCONTRATOSERVICOSOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144SelOsServico_Codigo',fld:'vSELOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV96PraLinhaCTRF',fld:'vPRALINHACTRF',pic:'ZZZ9',nv:0},{av:'AV13ColDemandas',fld:'vCOLDEMANDAS',pic:'@!',nv:''},{av:'AV16ColPFLFS',fld:'vCOLPFLFS',pic:'@!',nv:''},{av:'AV159ColPFBFMCTRF',fld:'vCOLPFBFMCTRF',pic:'@!',nv:''},{av:'AV160ColPFLFMCTRF',fld:'vCOLPFLFMCTRF',pic:'@!',nv:''},{av:'AV173ColParecer',fld:'vCOLPARECER',pic:'@!',nv:''},{av:'AV101ColContadorFM',fld:'vCOLCONTADORFM',pic:'@!',nv:''},{av:'AV112ColPFLFMCAST',fld:'vCOLPFLFMCAST',pic:'@!',nv:''},{av:'AV110ColPFLFSCAST',fld:'vCOLPFLFSCAST',pic:'@!',nv:''},{av:'AV109ColPFBFSCAST',fld:'vCOLPFBFSCAST',pic:'@!',nv:''},{av:'AV113ColDataCAST',fld:'vCOLDATACAST',pic:'@!',nv:''},{av:'AV114ColDmnCAST',fld:'vCOLDMNCAST',pic:'@!',nv:''},{av:'AV74WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'AV24ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV49Minutos2',fld:'vMINUTOS2',pic:'ZZZ9',nv:0},{av:'AV30DataOSFM2',fld:'vDATAOSFM2',pic:'',nv:''},{av:'AV58OSFM2',fld:'vOSFM2',pic:'',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A416Sistema_Nome',fld:'SISTEMA_NOME',pic:'@!',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV156AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A428NaoConformidade_AreaTrabalhoCod',fld:'NAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV8Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV128Aba',fld:'vABA',pic:'',nv:''},{av:'AV108ColDmnCASTn',fld:'vCOLDMNCASTN',pic:'ZZZ9',nv:0},{av:'AV148ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV116PraLinCAST',fld:'vPRALINCAST',pic:'ZZZ9',nv:0},{av:'AV102ColContadorFMn',fld:'vCOLCONTADORFMN',pic:'ZZZ9',nv:0},{av:'AV118ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV119ColPFlFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV99ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV100ColPFlFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV174ColParecern',fld:'vCOLPARECERN',pic:'ZZZ9',nv:0},{av:'AV107ColDataCASTn',fld:'vCOLDATACASTN',pic:'ZZZ9',nv:0},{av:'AV103ColPFBFSCASTn',fld:'vCOLPFBFSCASTN',pic:'ZZZ9',nv:0},{av:'AV104ColPFLFSCASTn',fld:'vCOLPFLFSCASTN',pic:'ZZZ9',nv:0},{av:'AV105ColPFBFMCASTn',fld:'vCOLPFBFMCASTN',pic:'ZZZ9',nv:0},{av:'AV106ColPFLFMCASTn',fld:'vCOLPFLFMCASTN',pic:'ZZZ9',nv:0},{av:'AV9Arquivo2',fld:'vARQUIVO2',pic:'',nv:''},{av:'AV12ColDem',fld:'vCOLDEM',pic:'ZZZ9',nv:0},{av:'AV14ColPB',fld:'vCOLPB',pic:'ZZZ9',nv:0},{av:'AV17ColPL',fld:'vCOLPL',pic:'ZZZ9',nv:0},{av:'AV7Acao',fld:'vACAO',pic:'@!',nv:''},{av:'AV161OSFMCTRF',fld:'vOSFMCTRF',pic:'',nv:''},{av:'AV162ColNomeModulo',fld:'vCOLNOMEMODULO',pic:'',nv:''},{av:'AV82ColContadorFS',fld:'vCOLCONTADORFS',pic:'@!',nv:''},{av:'AV130ColFiltro',fld:'vCOLFILTRO',pic:'@!',nv:''},{av:'AV138ColPFBFSDmn',fld:'vCOLPFBFSDMN',pic:'@!',nv:''},{av:'AV139ColPFLFSDmn',fld:'vCOLPFLFSDMN',pic:'@!',nv:''},{av:'AV168ColTipo',fld:'vCOLTIPO',pic:'@!',nv:''},{av:'A41Contratada_PessoaNom',fld:'CONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26Contratada_Nome',fld:'vCONTRATADA_NOME',pic:'@!',nv:''},{av:'AV85ColOSFSn',fld:'vCOLOSFSN',pic:'ZZZ9',nv:0},{av:'AV86ColNomeSisteman',fld:'vCOLNOMESISTEMAN',pic:'ZZZ9',nv:0},{av:'AV163ColNomeModulon',fld:'vCOLNOMEMODULON',pic:'ZZZ9',nv:0},{av:'AV87ColOSFMn',fld:'vCOLOSFMN',pic:'ZZZ9',nv:0},{av:'AV88ColDataDmnn',fld:'vCOLDATADMNN',pic:'ZZZ9',nv:0},{av:'AV89ColDescricaon',fld:'vCOLDESCRICAON',pic:'ZZZ9',nv:0},{av:'AV91ColLinkn',fld:'vCOLLINKN',pic:'ZZZ9',nv:0},{av:'AV20ContagemResultado_EhValidacao',fld:'vCONTAGEMRESULTADO_EHVALIDACAO',pic:'',nv:false},{av:'AV81ContagemResultado_ContadorFSCod',fld:'vCONTAGEMRESULTADO_CONTADORFSCOD',pic:'ZZZZZ9',nv:0},{av:'AV131ColFiltron',fld:'vCOLFILTRON',pic:'ZZZ9',nv:0},{av:'AV132TipoFiltro',fld:'vTIPOFILTRO',pic:'@!',nv:''},{av:'AV133FiltroColuna',fld:'vFILTROCOLUNA',pic:'',nv:''},{av:'AV141ColPFBFSDmnn',fld:'vCOLPFBFSDMNN',pic:'ZZZ9',nv:0},{av:'AV140ColPFLFSDmnn',fld:'vCOLPFLFSDMNN',pic:'ZZZ9',nv:0},{av:'AV90ColContadorFSn',fld:'vCOLCONTADORFSN',pic:'ZZZ9',nv:0},{av:'AV167ColProjeton',fld:'vCOLPROJETON',pic:'ZZZ9',nv:0},{av:'AV169ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV171ColObservacaon',fld:'vCOLOBSERVACAON',pic:'ZZZ9',nv:0},{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV8Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV134FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV180GestorDaContratada',fld:'vGESTORDACONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV60PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV12ColDem',fld:'vCOLDEM',pic:'ZZZ9',nv:0},{av:'AV14ColPB',fld:'vCOLPB',pic:'ZZZ9',nv:0},{av:'AV17ColPL',fld:'vCOLPL',pic:'ZZZ9',nv:0},{av:'AV99ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV100ColPFlFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{ctrl:'CARREGAR',prop:'Visible'},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0},{av:'AV69Sistema_Nome',fld:'vSISTEMA_NOME',pic:'@!',nv:''},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26Contratada_Nome',fld:'vCONTRATADA_NOME',pic:'@!',nv:''},{av:'AV81ContagemResultado_ContadorFSCod',fld:'vCONTAGEMRESULTADO_CONTADORFSCOD',pic:'ZZZZZ9',nv:0},{av:'AV61PrimeiroNome',fld:'vPRIMEIRONOME',pic:'@!',nv:''},{av:'AV72UltimoNome',fld:'vULTIMONOME',pic:'@!',nv:''},{av:'AV20ContagemResultado_EhValidacao',fld:'vCONTAGEMRESULTADO_EHVALIDACAO',pic:'',nv:false},{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV49Minutos2',fld:'vMINUTOS2',pic:'ZZZ9',nv:0},{av:'AV30DataOSFM2',fld:'vDATAOSFM2',pic:'',nv:''},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58OSFM2',fld:'vOSFM2',pic:'',nv:''},{av:'AV156AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'},{av:'AV108ColDmnCASTn',fld:'vCOLDMNCASTN',pic:'ZZZ9',nv:0},{av:'AV107ColDataCASTn',fld:'vCOLDATACASTN',pic:'ZZZ9',nv:0},{av:'AV103ColPFBFSCASTn',fld:'vCOLPFBFSCASTN',pic:'ZZZ9',nv:0},{av:'AV104ColPFLFSCASTn',fld:'vCOLPFLFSCASTN',pic:'ZZZ9',nv:0},{av:'AV105ColPFBFMCASTn',fld:'vCOLPFBFMCASTN',pic:'ZZZ9',nv:0},{av:'AV106ColPFLFMCASTn',fld:'vCOLPFLFMCASTN',pic:'ZZZ9',nv:0},{av:'AV102ColContadorFMn',fld:'vCOLCONTADORFMN',pic:'ZZZ9',nv:0},{av:'AV174ColParecern',fld:'vCOLPARECERN',pic:'ZZZ9',nv:0},{av:'AV118ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV119ColPFlFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV148ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'},{av:'AV85ColOSFSn',fld:'vCOLOSFSN',pic:'ZZZ9',nv:0},{av:'AV86ColNomeSisteman',fld:'vCOLNOMESISTEMAN',pic:'ZZZ9',nv:0},{av:'AV163ColNomeModulon',fld:'vCOLNOMEMODULON',pic:'ZZZ9',nv:0},{av:'AV87ColOSFMn',fld:'vCOLOSFMN',pic:'ZZZ9',nv:0},{av:'AV88ColDataDmnn',fld:'vCOLDATADMNN',pic:'ZZZ9',nv:0},{av:'AV89ColDescricaon',fld:'vCOLDESCRICAON',pic:'ZZZ9',nv:0},{av:'AV90ColContadorFSn',fld:'vCOLCONTADORFSN',pic:'ZZZ9',nv:0},{av:'AV91ColLinkn',fld:'vCOLLINKN',pic:'ZZZ9',nv:0},{av:'AV131ColFiltron',fld:'vCOLFILTRON',pic:'ZZZ9',nv:0},{av:'AV141ColPFBFSDmnn',fld:'vCOLPFBFSDMNN',pic:'ZZZ9',nv:0},{av:'AV140ColPFLFSDmnn',fld:'vCOLPFLFSDMNN',pic:'ZZZ9',nv:0},{av:'AV167ColProjeton',fld:'vCOLPROJETON',pic:'ZZZ9',nv:0},{av:'AV169ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV171ColObservacaon',fld:'vCOLOBSERVACAON',pic:'ZZZ9',nv:0},{av:'tblTablesdtdemandas_Visible',ctrl:'TABLESDTDEMANDAS',prop:'Visible'}]}");
         setEventMetadata("VACAO.CLICK","{handler:'E27BL1',iparms:[{av:'AV7Acao',fld:'vACAO',pic:'@!',nv:''}],oparms:[{av:'tblTblpffm_Visible',ctrl:'TBLPFFM',prop:'Visible'},{ctrl:'CARREGAR',prop:'Caption'}]}");
         setEventMetadata("VSELCONTRATADASRV_CODIGO.CLICK","{handler:'E23BL2',iparms:[{av:'AV135SelContratadaSrv_Codigo',fld:'vSELCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDATAENTREGA.ISVALID","{handler:'E17BL2',iparms:[{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV165TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV156AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''},{av:'lblTextblockprazoentrega_Caption',ctrl:'TEXTBLOCKPRAZOENTREGA',prop:'Caption'}]}");
         setEventMetadata("'IMPORTAR'","{handler:'E18BL2',iparms:[{av:'GRIDSDTDEMANDAS_nFirstRecordOnPage',nv:0},{av:'GRIDSDTDEMANDAS_nEOF',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''},{av:'AV97ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV111ColPFBFMCAST',fld:'vCOLPFBFMCAST',pic:'@!',nv:''},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV15ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV178Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'AV121StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''},{av:'AV142DataSrvFat',fld:'vDATASRVFAT',pic:'@!',nv:''},{av:'AV150ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV153Agrupador',fld:'vAGRUPADOR',pic:'@!',nv:''},{av:'AV154Notificar',fld:'vNOTIFICAR',pic:'',nv:false},{av:'AV177ContratoServicosOS_Codigo',fld:'vCONTRATOSERVICOSOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null}]}");
         setEventMetadata("VCALCULARPRAZO.CLICK","{handler:'E29BL1',iparms:[{av:'AV175CalcularPrazo',fld:'vCALCULARPRAZO',pic:'',nv:false}],oparms:[{av:'edtavDataentrega_Enabled',ctrl:'vDATAENTREGA',prop:'Enabled'},{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VREQUISITANTE.CLICK","{handler:'E19BL2',iparms:[{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV156AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1223ContratoGestor_ContratadaSigla',fld:'CONTRATOGESTOR_CONTRATADASIGLA',pic:'@!',nv:''},{av:'AV178Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2033ContratoGestor_UsuarioAtv',fld:'CONTRATOGESTOR_USUARIOATV',pic:'',nv:false}],oparms:[{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11BL2',iparms:[{av:'AV75ColOSFS',fld:'vCOLOSFS',pic:'@!',nv:''},{av:'AV76ColNomeSistema',fld:'vCOLNOMESISTEMA',pic:'@!',nv:''},{av:'AV162ColNomeModulo',fld:'vCOLNOMEMODULO',pic:'',nv:''},{av:'AV77ColOSFM',fld:'vCOLOSFM',pic:'@!',nv:''},{av:'AV78ColDataDmn',fld:'vCOLDATADMN',pic:'@!',nv:''},{av:'AV147ColDataCnt',fld:'vCOLDATACNT',pic:'@!',nv:''},{av:'AV80ColDescricao',fld:'vCOLDESCRICAO',pic:'@!',nv:''},{av:'AV82ColContadorFS',fld:'vCOLCONTADORFS',pic:'@!',nv:''},{av:'AV101ColContadorFM',fld:'vCOLCONTADORFM',pic:'@!',nv:''},{av:'AV84ColLink',fld:'vCOLLINK',pic:'@!',nv:''},{av:'AV97ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV98ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''},{av:'AV130ColFiltro',fld:'vCOLFILTRO',pic:'@!',nv:''},{av:'AV138ColPFBFSDmn',fld:'vCOLPFBFSDMN',pic:'@!',nv:''},{av:'AV139ColPFLFSDmn',fld:'vCOLPFLFSDMN',pic:'@!',nv:''},{av:'AV166ColProjeto',fld:'vCOLPROJETO',pic:'@!',nv:''},{av:'AV168ColTipo',fld:'vCOLTIPO',pic:'@!',nv:''},{av:'AV170ColObservacao',fld:'vCOLOBSERVACAO',pic:'@!',nv:''},{av:'GRIDSDTDEMANDAS_nFirstRecordOnPage',nv:0},{av:'GRIDSDTDEMANDAS_nEOF',nv:0},{av:'AV120Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136ContratadaSrv_Codigo',fld:'vCONTRATADASRV_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV126OSServico_Codigo',fld:'vOSSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV145DataSrvFatSel',fld:'vDATASRVFATSEL',pic:'',nv:''},{av:'AV111ColPFBFMCAST',fld:'vCOLPFBFMCAST',pic:'@!',nv:''},{av:'AV56OQImportar',fld:'vOQIMPORTAR',pic:'',nv:''},{av:'AV143SrvParaFtr',fld:'vSRVPARAFTR',pic:'',nv:false},{av:'AV181RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV15ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'AV46Ln',fld:'vLN',pic:'ZZZ9',nv:0},{av:'AV60PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV8Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV128Aba',fld:'vABA',pic:'',nv:''},{av:'AV85ColOSFSn',fld:'vCOLOSFSN',pic:'ZZZ9',nv:0},{av:'AV86ColNomeSisteman',fld:'vCOLNOMESISTEMAN',pic:'ZZZ9',nv:0},{av:'AV163ColNomeModulon',fld:'vCOLNOMEMODULON',pic:'ZZZ9',nv:0},{av:'AV87ColOSFMn',fld:'vCOLOSFMN',pic:'ZZZ9',nv:0},{av:'AV88ColDataDmnn',fld:'vCOLDATADMNN',pic:'ZZZ9',nv:0},{av:'AV89ColDescricaon',fld:'vCOLDESCRICAON',pic:'ZZZ9',nv:0},{av:'AV91ColLinkn',fld:'vCOLLINKN',pic:'ZZZ9',nv:0},{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_EhValidacao',fld:'vCONTAGEMRESULTADO_EHVALIDACAO',pic:'',nv:false},{av:'AV57OSFM',fld:'vOSFM',pic:'',nv:''},{av:'AV29DataOSFM',fld:'vDATAOSFM',pic:'',nv:''},{av:'AV79ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV81ContagemResultado_ContadorFSCod',fld:'vCONTAGEMRESULTADO_CONTADORFSCOD',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultado_Link',fld:'vCONTAGEMRESULTADO_LINK',pic:'',nv:''},{av:'AV99ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV100ColPFlFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ColContadorFMn',fld:'vCOLCONTADORFMN',pic:'ZZZ9',nv:0},{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV131ColFiltron',fld:'vCOLFILTRON',pic:'ZZZ9',nv:0},{av:'AV132TipoFiltro',fld:'vTIPOFILTRO',pic:'@!',nv:''},{av:'AV133FiltroColuna',fld:'vFILTROCOLUNA',pic:'',nv:''},{av:'AV141ColPFBFSDmnn',fld:'vCOLPFBFSDMNN',pic:'ZZZ9',nv:0},{av:'AV140ColPFLFSDmnn',fld:'vCOLPFLFSDMNN',pic:'ZZZ9',nv:0},{av:'AV90ColContadorFSn',fld:'vCOLCONTADORFSN',pic:'ZZZ9',nv:0},{av:'AV146DataCnt',fld:'vDATACNT',pic:'',nv:''},{av:'AV148ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV134FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV167ColProjeton',fld:'vCOLPROJETON',pic:'ZZZ9',nv:0},{av:'AV169ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV171ColObservacaon',fld:'vCOLOBSERVACAON',pic:'ZZZ9',nv:0},{av:'AV151DataEntrega',fld:'vDATAENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV85ColOSFSn',fld:'vCOLOSFSN',pic:'ZZZ9',nv:0},{av:'AV86ColNomeSisteman',fld:'vCOLNOMESISTEMAN',pic:'ZZZ9',nv:0},{av:'AV163ColNomeModulon',fld:'vCOLNOMEMODULON',pic:'ZZZ9',nv:0},{av:'AV87ColOSFMn',fld:'vCOLOSFMN',pic:'ZZZ9',nv:0},{av:'AV88ColDataDmnn',fld:'vCOLDATADMNN',pic:'ZZZ9',nv:0},{av:'AV148ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV89ColDescricaon',fld:'vCOLDESCRICAON',pic:'ZZZ9',nv:0},{av:'AV90ColContadorFSn',fld:'vCOLCONTADORFSN',pic:'ZZZ9',nv:0},{av:'AV102ColContadorFMn',fld:'vCOLCONTADORFMN',pic:'ZZZ9',nv:0},{av:'AV91ColLinkn',fld:'vCOLLINKN',pic:'ZZZ9',nv:0},{av:'AV99ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV100ColPFlFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV131ColFiltron',fld:'vCOLFILTRON',pic:'ZZZ9',nv:0},{av:'AV141ColPFBFSDmnn',fld:'vCOLPFBFSDMNN',pic:'ZZZ9',nv:0},{av:'AV140ColPFLFSDmnn',fld:'vCOLPFLFSDMNN',pic:'ZZZ9',nv:0},{av:'AV167ColProjeton',fld:'vCOLPROJETON',pic:'ZZZ9',nv:0},{av:'AV169ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV171ColObservacaon',fld:'vCOLOBSERVACAON',pic:'ZZZ9',nv:0},{av:'tblTablesdtdemandas_Visible',ctrl:'TABLESDTDEMANDAS',prop:'Visible'},{av:'AV93SDT_Demandas',fld:'vSDT_DEMANDAS',grid:373,pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("CONFIRMPANEL2.CLOSE","{handler:'E12BL2',iparms:[{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}],oparms:[{av:'AV127Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATOSERVICOS_CODIGO.CLICK","{handler:'E20BL2',iparms:[{av:'AV176ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'cmbavContratada_codigo'}],oparms:[{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}]}");
         setEventMetadata("VCONTRATADAORIGEM_CODIGO.CLICK","{handler:'E21BL2',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV150ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'dynavContratadaorigem_codigo'}],oparms:[{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1046ContagemResultado_Agrupador = "";
         AV74WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV145DataSrvFatSel = "";
         AV97ColPFBFM = "";
         AV111ColPFBFMCAST = "";
         AV56OQImportar = "";
         AV181RequerOrigem = true;
         AV15ColPFBFS = "";
         AV93SDT_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A77Contrato_Numero = "";
         AV113ColDataCAST = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A427NaoConformidade_Nome = "";
         AV8Arquivo = "";
         AV9Arquivo2 = "";
         A41Contratada_PessoaNom = "";
         AV26Contratada_Nome = "";
         AV165TipoDias = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         A1223ContratoGestor_ContratadaSigla = "";
         GXCCtlgxBlob = "";
         AV158Blob = "";
         Innewwindow_Width = "";
         Innewwindow_Height = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV134FileName = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         WebComp_Wc_anexos_Component = "";
         AV121StatusDmn = "";
         AV142DataSrvFat = "";
         AV20ContagemResultado_EhValidacao = false;
         AV7Acao = "";
         AV132TipoFiltro = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l1046ContagemResultado_Agrupador = "";
         H00BL2_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00BL2_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00BL3_A74Contrato_Codigo = new int[1] ;
         H00BL3_n74Contrato_Codigo = new bool[] {false} ;
         H00BL3_A160ContratoServicos_Codigo = new int[1] ;
         H00BL3_A155Servico_Codigo = new int[1] ;
         H00BL3_A605Servico_Sigla = new String[] {""} ;
         H00BL3_A39Contratada_Codigo = new int[1] ;
         H00BL3_A632Servico_Ativo = new bool[] {false} ;
         H00BL3_n632Servico_Ativo = new bool[] {false} ;
         H00BL3_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00BL4_A40Contratada_PessoaCod = new int[1] ;
         H00BL4_A39Contratada_Codigo = new int[1] ;
         H00BL4_A41Contratada_PessoaNom = new String[] {""} ;
         H00BL4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00BL4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00BL4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00BL4_A43Contratada_Ativo = new bool[] {false} ;
         H00BL4_n43Contratada_Ativo = new bool[] {false} ;
         H00BL5_A40Contratada_PessoaCod = new int[1] ;
         H00BL5_A39Contratada_Codigo = new int[1] ;
         H00BL5_A41Contratada_PessoaNom = new String[] {""} ;
         H00BL5_n41Contratada_PessoaNom = new bool[] {false} ;
         H00BL5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00BL5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00BL5_A43Contratada_Ativo = new bool[] {false} ;
         H00BL5_n43Contratada_Ativo = new bool[] {false} ;
         H00BL6_A74Contrato_Codigo = new int[1] ;
         H00BL6_n74Contrato_Codigo = new bool[] {false} ;
         H00BL6_A160ContratoServicos_Codigo = new int[1] ;
         H00BL6_A155Servico_Codigo = new int[1] ;
         H00BL6_A605Servico_Sigla = new String[] {""} ;
         H00BL6_A39Contratada_Codigo = new int[1] ;
         H00BL6_A632Servico_Ativo = new bool[] {false} ;
         H00BL6_n632Servico_Ativo = new bool[] {false} ;
         H00BL6_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00BL7_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00BL7_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BL7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00BL7_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00BL7_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00BL7_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00BL7_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00BL7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00BL7_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00BL7_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         GridsdtdemandasContainer = new GXWebGrid( context);
         AV114ColDmnCAST = "";
         AV109ColPFBFSCAST = "";
         AV110ColPFLFSCAST = "";
         AV112ColPFLFMCAST = "";
         AV75ColOSFS = "";
         AV80ColDescricao = "";
         AV166ColProjeto = "";
         AV98ColPFLFM = "";
         AV138ColPFBFSDmn = "";
         AV139ColPFLFSDmn = "";
         AV76ColNomeSistema = "";
         AV162ColNomeModulo = "";
         AV168ColTipo = "";
         AV57OSFM = "";
         AV77ColOSFM = "";
         AV153Agrupador = "";
         AV29DataOSFM = DateTime.MinValue;
         AV78ColDataDmn = "";
         AV151DataEntrega = (DateTime)(DateTime.MinValue);
         AV79ContagemResultado_Observacao = "";
         AV170ColObservacao = "";
         AV83ContagemResultado_Link = "";
         AV84ColLink = "";
         AV101ColContadorFM = "";
         AV146DataCnt = DateTime.MinValue;
         AV147ColDataCnt = "";
         AV173ColParecer = "";
         AV82ColContadorFS = "";
         AV30DataOSFM2 = DateTime.MinValue;
         AV58OSFM2 = "";
         AV13ColDemandas = "";
         AV159ColPFBFMCTRF = "";
         AV160ColPFLFMCTRF = "";
         AV16ColPFLFS = "";
         AV11ColDeflator = "";
         AV161OSFMCTRF = "";
         AV128Aba = "";
         AV130ColFiltro = "";
         AV133FiltroColuna = "";
         AV155SDT_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV73WebSession = context.GetSession();
         H00BL8_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00BL8_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00BL8_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00BL8_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00BL8_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00BL8_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV33ExcelDocument = new ExcelDocumentI();
         AV71Tmp_File = new SdtTmp_File(context);
         Gx_msg = "";
         H00BL9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00BL9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00BL9_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00BL9_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00BL9_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00BL9_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00BL9_A43Contratada_Ativo = new bool[] {false} ;
         H00BL9_n43Contratada_Ativo = new bool[] {false} ;
         H00BL10_A39Contratada_Codigo = new int[1] ;
         H00BL10_A74Contrato_Codigo = new int[1] ;
         H00BL10_n74Contrato_Codigo = new bool[] {false} ;
         H00BL10_A92Contrato_Ativo = new bool[] {false} ;
         H00BL10_n92Contrato_Ativo = new bool[] {false} ;
         H00BL11_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00BL11_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00BL11_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00BL11_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00BL11_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00BL11_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00BL12_A427NaoConformidade_Nome = new String[] {""} ;
         H00BL12_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00BL12_A426NaoConformidade_Codigo = new int[1] ;
         AV69Sistema_Nome = "";
         lV69Sistema_Nome = "";
         H00BL13_A416Sistema_Nome = new String[] {""} ;
         H00BL13_A130Sistema_Ativo = new bool[] {false} ;
         H00BL13_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00BL13_A127Sistema_Codigo = new int[1] ;
         H00BL13_A129Sistema_Sigla = new String[] {""} ;
         AV66Sistema = new SdtSistema(context);
         AV61PrimeiroNome = "";
         AV72UltimoNome = "";
         lV72UltimoNome = "";
         lV61PrimeiroNome = "";
         H00BL14_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00BL14_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BL14_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00BL14_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00BL14_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00BL14_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         GXt_char3 = "";
         GXt_char4 = "";
         GXt_char5 = "";
         AV164FimDoExpediente = (DateTime)(DateTime.MinValue);
         GXt_dtime10 = (DateTime)(DateTime.MinValue);
         H00BL15_A74Contrato_Codigo = new int[1] ;
         H00BL15_n74Contrato_Codigo = new bool[] {false} ;
         H00BL15_A1013Contrato_PrepostoCod = new int[1] ;
         H00BL15_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00BL15_A39Contratada_Codigo = new int[1] ;
         H00BL19_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00BL19_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00BL19_A74Contrato_Codigo = new int[1] ;
         H00BL19_n74Contrato_Codigo = new bool[] {false} ;
         H00BL19_A39Contratada_Codigo = new int[1] ;
         H00BL19_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00BL19_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00BL19_A43Contratada_Ativo = new bool[] {false} ;
         H00BL19_n43Contratada_Ativo = new bool[] {false} ;
         H00BL19_A632Servico_Ativo = new bool[] {false} ;
         H00BL19_n632Servico_Ativo = new bool[] {false} ;
         H00BL19_A92Contrato_Ativo = new bool[] {false} ;
         H00BL19_n92Contrato_Ativo = new bool[] {false} ;
         H00BL19_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00BL19_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         H00BL19_n1869Contrato_DataTermino = new bool[] {false} ;
         H00BL20_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00BL20_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00BL20_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00BL20_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00BL20_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         H00BL20_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         H00BL20_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00BL20_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00BL21_A74Contrato_Codigo = new int[1] ;
         H00BL21_n74Contrato_Codigo = new bool[] {false} ;
         H00BL21_A39Contratada_Codigo = new int[1] ;
         H00BL21_A155Servico_Codigo = new int[1] ;
         H00BL21_A77Contrato_Numero = new String[] {""} ;
         H00BL21_A160ContratoServicos_Codigo = new int[1] ;
         H00BL22_A74Contrato_Codigo = new int[1] ;
         H00BL22_n74Contrato_Codigo = new bool[] {false} ;
         H00BL22_A39Contratada_Codigo = new int[1] ;
         H00BL22_A155Servico_Codigo = new int[1] ;
         H00BL22_A77Contrato_Numero = new String[] {""} ;
         H00BL22_A160ContratoServicos_Codigo = new int[1] ;
         lV26Contratada_Nome = "";
         H00BL23_A40Contratada_PessoaCod = new int[1] ;
         H00BL23_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00BL23_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00BL23_A41Contratada_PessoaNom = new String[] {""} ;
         H00BL23_n41Contratada_PessoaNom = new bool[] {false} ;
         H00BL23_A43Contratada_Ativo = new bool[] {false} ;
         H00BL23_n43Contratada_Ativo = new bool[] {false} ;
         H00BL23_A39Contratada_Codigo = new int[1] ;
         H00BL24_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00BL24_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00BL24_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00BL24_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         H00BL24_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         H00BL24_A43Contratada_Ativo = new bool[] {false} ;
         H00BL24_n43Contratada_Ativo = new bool[] {false} ;
         H00BL24_A92Contrato_Ativo = new bool[] {false} ;
         H00BL24_n92Contrato_Ativo = new bool[] {false} ;
         H00BL24_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00BL24_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00BL24_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         H00BL24_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         H00BL24_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00BL24_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00BL24_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         A1081ContratoGestor_UsuarioPesNom = "";
         GridsdtdemandasRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         bttCarregar_Jsonclick = "";
         bttImportar_Jsonclick = "";
         subGridsdtdemandas_Linesclass = "";
         GridsdtdemandasColumn = new GXWebColumn();
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock27_Jsonclick = "";
         lblTextblock37_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock16_Jsonclick = "";
         lblTextblock11_Jsonclick = "";
         lblTextblock12_Jsonclick = "";
         lblTextblock13_Jsonclick = "";
         lblTextblock14_Jsonclick = "";
         lblTextblock15_Jsonclick = "";
         lblTextblock43_Jsonclick = "";
         lblTextblock17_Jsonclick = "";
         lblTextblock42_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         lblTbcontadorfs_Jsonclick = "";
         lblTbcolcrfs_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTbcolcrfs2_Jsonclick = "";
         lblTbdtcnt_Jsonclick = "";
         lblTbcoldtcnt_Jsonclick = "";
         lblTbcoldtcnt2_Jsonclick = "";
         lblTextblock45_Jsonclick = "";
         lblTextblock18_Jsonclick = "";
         lblTextblock48_Jsonclick = "";
         lblTextblock46_Jsonclick = "";
         lblTextblock25_Jsonclick = "";
         lblTbpflfm_Jsonclick = "";
         lblTbpflfm2_Jsonclick = "";
         lblTbpflfm4_Jsonclick = "";
         lblTbehvalidacao_Jsonclick = "";
         lblTextblock19_Jsonclick = "";
         lblTextblock44_Jsonclick = "";
         lblTextblock47_Jsonclick = "";
         lblTbosfm_Jsonclick = "";
         lblTbcosfm_Jsonclick = "";
         lblTextblock41_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock21_Jsonclick = "";
         lblTextblockprazoentrega_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock22_Jsonclick = "";
         lblTextblock24_Jsonclick = "";
         lblTextblock26_Jsonclick = "";
         lblTextblock40_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         lblTbcontrato2_Jsonclick = "";
         lblTextblock28_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock32_Jsonclick = "";
         lblTextblock33_Jsonclick = "";
         lblTextblock30_Jsonclick = "";
         lblTextblock31_Jsonclick = "";
         lblTbcontpres2_Jsonclick = "";
         lblTbcontpres_Jsonclick = "";
         lblTbservico_Jsonclick = "";
         lblTbcontrato_Jsonclick = "";
         lblTbcontorigem_Jsonclick = "";
         lblTbstatus_Jsonclick = "";
         lblTextblock50_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_importfromfile__default(),
            new Object[][] {
                new Object[] {
               H00BL2_A1046ContagemResultado_Agrupador, H00BL2_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               H00BL3_A74Contrato_Codigo, H00BL3_A160ContratoServicos_Codigo, H00BL3_A155Servico_Codigo, H00BL3_A605Servico_Sigla, H00BL3_A39Contratada_Codigo, H00BL3_A632Servico_Ativo, H00BL3_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00BL4_A40Contratada_PessoaCod, H00BL4_A39Contratada_Codigo, H00BL4_A41Contratada_PessoaNom, H00BL4_n41Contratada_PessoaNom, H00BL4_A52Contratada_AreaTrabalhoCod, H00BL4_A43Contratada_Ativo
               }
               , new Object[] {
               H00BL5_A40Contratada_PessoaCod, H00BL5_A39Contratada_Codigo, H00BL5_A41Contratada_PessoaNom, H00BL5_n41Contratada_PessoaNom, H00BL5_A52Contratada_AreaTrabalhoCod, H00BL5_A43Contratada_Ativo
               }
               , new Object[] {
               H00BL6_A74Contrato_Codigo, H00BL6_A160ContratoServicos_Codigo, H00BL6_A155Servico_Codigo, H00BL6_A605Servico_Sigla, H00BL6_A39Contratada_Codigo, H00BL6_A632Servico_Ativo, H00BL6_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00BL7_A70ContratadaUsuario_UsuarioPessoaCod, H00BL7_n70ContratadaUsuario_UsuarioPessoaCod, H00BL7_A69ContratadaUsuario_UsuarioCod, H00BL7_A71ContratadaUsuario_UsuarioPessoaNom, H00BL7_n71ContratadaUsuario_UsuarioPessoaNom, H00BL7_A1228ContratadaUsuario_AreaTrabalhoCod, H00BL7_n1228ContratadaUsuario_AreaTrabalhoCod, H00BL7_A66ContratadaUsuario_ContratadaCod, H00BL7_A1394ContratadaUsuario_UsuarioAtivo, H00BL7_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               H00BL8_A1078ContratoGestor_ContratoCod, H00BL8_A1136ContratoGestor_ContratadaCod, H00BL8_n1136ContratoGestor_ContratadaCod, H00BL8_A1079ContratoGestor_UsuarioCod, H00BL8_A1446ContratoGestor_ContratadaAreaCod, H00BL8_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00BL9_A69ContratadaUsuario_UsuarioCod, H00BL9_A66ContratadaUsuario_ContratadaCod, H00BL9_A1228ContratadaUsuario_AreaTrabalhoCod, H00BL9_n1228ContratadaUsuario_AreaTrabalhoCod, H00BL9_A1394ContratadaUsuario_UsuarioAtivo, H00BL9_n1394ContratadaUsuario_UsuarioAtivo, H00BL9_A43Contratada_Ativo, H00BL9_n43Contratada_Ativo
               }
               , new Object[] {
               H00BL10_A39Contratada_Codigo, H00BL10_A74Contrato_Codigo, H00BL10_A92Contrato_Ativo
               }
               , new Object[] {
               H00BL11_A1078ContratoGestor_ContratoCod, H00BL11_A1136ContratoGestor_ContratadaCod, H00BL11_n1136ContratoGestor_ContratadaCod, H00BL11_A1079ContratoGestor_UsuarioCod, H00BL11_A1446ContratoGestor_ContratadaAreaCod, H00BL11_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00BL12_A427NaoConformidade_Nome, H00BL12_A428NaoConformidade_AreaTrabalhoCod, H00BL12_A426NaoConformidade_Codigo
               }
               , new Object[] {
               H00BL13_A416Sistema_Nome, H00BL13_A130Sistema_Ativo, H00BL13_A135Sistema_AreaTrabalhoCod, H00BL13_A127Sistema_Codigo, H00BL13_A129Sistema_Sigla
               }
               , new Object[] {
               H00BL14_A70ContratadaUsuario_UsuarioPessoaCod, H00BL14_n70ContratadaUsuario_UsuarioPessoaCod, H00BL14_A66ContratadaUsuario_ContratadaCod, H00BL14_A71ContratadaUsuario_UsuarioPessoaNom, H00BL14_n71ContratadaUsuario_UsuarioPessoaNom, H00BL14_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H00BL15_A74Contrato_Codigo, H00BL15_A1013Contrato_PrepostoCod, H00BL15_n1013Contrato_PrepostoCod, H00BL15_A39Contratada_Codigo
               }
               , new Object[] {
               H00BL19_A1595Contratada_AreaTrbSrvPdr, H00BL19_n1595Contratada_AreaTrbSrvPdr, H00BL19_A74Contrato_Codigo, H00BL19_A39Contratada_Codigo, H00BL19_A52Contratada_AreaTrabalhoCod, H00BL19_A43Contratada_Ativo, H00BL19_A632Servico_Ativo, H00BL19_n632Servico_Ativo, H00BL19_A92Contrato_Ativo, H00BL19_A75Contrato_AreaTrabalhoCod,
               H00BL19_A1869Contrato_DataTermino, H00BL19_n1869Contrato_DataTermino
               }
               , new Object[] {
               H00BL20_A1078ContratoGestor_ContratoCod, H00BL20_A1079ContratoGestor_UsuarioCod, H00BL20_A2033ContratoGestor_UsuarioAtv, H00BL20_n2033ContratoGestor_UsuarioAtv, H00BL20_A1223ContratoGestor_ContratadaSigla, H00BL20_n1223ContratoGestor_ContratadaSigla, H00BL20_A1136ContratoGestor_ContratadaCod, H00BL20_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               H00BL21_A74Contrato_Codigo, H00BL21_A39Contratada_Codigo, H00BL21_A155Servico_Codigo, H00BL21_A77Contrato_Numero, H00BL21_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00BL22_A74Contrato_Codigo, H00BL22_A39Contratada_Codigo, H00BL22_A155Servico_Codigo, H00BL22_A77Contrato_Numero, H00BL22_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00BL23_A40Contratada_PessoaCod, H00BL23_A52Contratada_AreaTrabalhoCod, H00BL23_A41Contratada_PessoaNom, H00BL23_n41Contratada_PessoaNom, H00BL23_A43Contratada_Ativo, H00BL23_A39Contratada_Codigo
               }
               , new Object[] {
               H00BL24_A1078ContratoGestor_ContratoCod, H00BL24_A1136ContratoGestor_ContratadaCod, H00BL24_n1136ContratoGestor_ContratadaCod, H00BL24_A1080ContratoGestor_UsuarioPesCod, H00BL24_n1080ContratoGestor_UsuarioPesCod, H00BL24_A43Contratada_Ativo, H00BL24_n43Contratada_Ativo, H00BL24_A92Contrato_Ativo, H00BL24_n92Contrato_Ativo, H00BL24_A2033ContratoGestor_UsuarioAtv,
               H00BL24_n2033ContratoGestor_UsuarioAtv, H00BL24_A1081ContratoGestor_UsuarioPesNom, H00BL24_n1081ContratoGestor_UsuarioPesNom, H00BL24_A1079ContratoGestor_UsuarioCod, H00BL24_A1446ContratoGestor_ContratadaAreaCod, H00BL24_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         WebComp_Wc_anexos = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlsistemanom_Enabled = 0;
         edtavCtldemandafm_Enabled = 0;
         edtavCtldatadmn_Enabled = 0;
         edtavCtldescricao_Enabled = 0;
         edtavCtldataent_Enabled = 0;
         edtavCtldemanda_Enabled = 0;
         edtavCtlcontadorfsnom_Enabled = 0;
         edtavCtllink_Enabled = 0;
         edtavCtlpfbfm_Enabled = 0;
         edtavCtlpflfm_Enabled = 0;
         edtavCtlpfbfs_Enabled = 0;
         edtavCtlpflfs_Enabled = 0;
         edtavCtlcontadorfmcod_Enabled = 0;
      }

      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_373 ;
      private short nGXsfl_373_idx=1 ;
      private short AV46Ln ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV108ColDmnCASTn ;
      private short AV148ColDataCntn ;
      private short AV102ColContadorFMn ;
      private short AV118ColPFBFSn ;
      private short AV119ColPFlFSn ;
      private short AV99ColPFBFMn ;
      private short AV100ColPFlFMn ;
      private short AV174ColParecern ;
      private short AV107ColDataCASTn ;
      private short AV103ColPFBFSCASTn ;
      private short AV104ColPFLFSCASTn ;
      private short AV105ColPFBFMCASTn ;
      private short AV106ColPFLFMCASTn ;
      private short AV12ColDem ;
      private short AV14ColPB ;
      private short AV17ColPL ;
      private short AV85ColOSFSn ;
      private short AV86ColNomeSisteman ;
      private short AV163ColNomeModulon ;
      private short AV87ColOSFMn ;
      private short AV88ColDataDmnn ;
      private short AV89ColDescricaon ;
      private short AV91ColLinkn ;
      private short AV131ColFiltron ;
      private short AV141ColPFBFSDmnn ;
      private short AV140ColPFLFSDmnn ;
      private short AV90ColContadorFSn ;
      private short AV167ColProjeton ;
      private short AV169ColTipon ;
      private short AV171ColObservacaon ;
      private short wbEnd ;
      private short wbStart ;
      private short AV184GXV1 ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_373_Refreshing=0 ;
      private short subGridsdtdemandas_Backcolorstyle ;
      private short AV116PraLinCAST ;
      private short AV60PraLinha ;
      private short AV49Minutos2 ;
      private short AV96PraLinhaCTRF ;
      private short nGXsfl_373_fel_idx=1 ;
      private short AV62Qtde ;
      private short GRIDSDTDEMANDAS_nEOF ;
      private short nGXsfl_373_bak_idx=1 ;
      private short AV31ErrCod ;
      private short AV200GXLvl549 ;
      private short AV201GXLvl553 ;
      private short AV202GXLvl556 ;
      private short AV204GXLvl604 ;
      private short AV42i ;
      private short AV206GXLvl791 ;
      private short subGridsdtdemandas_Titlebackstyle ;
      private short subGridsdtdemandas_Allowselection ;
      private short subGridsdtdemandas_Allowhovering ;
      private short subGridsdtdemandas_Allowcollapsing ;
      private short subGridsdtdemandas_Collapsed ;
      private short nGXWrapped ;
      private short subGridsdtdemandas_Backstyle ;
      private short wbTemp ;
      private int AV156AreaTrabalho_Codigo ;
      private int AV127Contratada_Codigo ;
      private int AV135SelContratadaSrv_Codigo ;
      private int AV120Servico_Codigo ;
      private int AV136ContratadaSrv_Codigo ;
      private int AV126OSServico_Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int AV180GestorDaContratada ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A426NaoConformidade_Codigo ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int edtavAreatrabalho_codigo_Enabled ;
      private int edtavAreatrabalho_codigo_Visible ;
      private int edtavFilename_Visible ;
      private int edtavFilename_Enabled ;
      private int AV178Requisitante ;
      private int AV176ContratoServicos_Codigo ;
      private int AV177ContratoServicosOS_Codigo ;
      private int gxdynajaxindex ;
      private int AV125SelServico_Codigo ;
      private int AV150ContratadaOrigem_Codigo ;
      private int AV144SelOsServico_Codigo ;
      private int AV18ContagemResultado_ContadorFMCod ;
      private int AV81ContagemResultado_ContadorFSCod ;
      private int subGridsdtdemandas_Islastpage ;
      private int edtavCtlsistemanom_Enabled ;
      private int edtavCtldemandafm_Enabled ;
      private int edtavCtldatadmn_Enabled ;
      private int edtavCtldescricao_Enabled ;
      private int edtavCtldataent_Enabled ;
      private int edtavCtldemanda_Enabled ;
      private int edtavCtlcontadorfsnom_Enabled ;
      private int edtavCtllink_Enabled ;
      private int edtavCtlpfbfm_Enabled ;
      private int edtavCtlpflfm_Enabled ;
      private int edtavCtlpfbfs_Enabled ;
      private int edtavCtlpflfs_Enabled ;
      private int edtavCtlcontadorfmcod_Enabled ;
      private int subGridsdtdemandas_Titleforecolor ;
      private int subGridsdtdemandas_Visible ;
      private int tblTbldadosms_Visible ;
      private int tblTblconftrf_Visible ;
      private int tblTblcast_Visible ;
      private int tblTablegrid_Visible ;
      private int edtavServico_codigo_Visible ;
      private int edtavContratadasrv_codigo_Visible ;
      private int edtavOsservico_codigo_Visible ;
      private int edtavDataentrega_Enabled ;
      private int tblTblanexos_Visible ;
      private int edtavOsfm_Enabled ;
      private int edtavColosfm_Enabled ;
      private int tblTbldadosdmn_Visible ;
      private int tblTblcontratada_Visible ;
      private int tblTblcontadorfm_Visible ;
      private int tblTblsrvvnc_Visible ;
      private int tblTblfiltrocoluna_Visible ;
      private int lblTbehvalidacao_Visible ;
      private int lblTbcontorigem_Visible ;
      private int lblTbservico_Visible ;
      private int lblTbcontrato_Visible ;
      private int bttCarregar_Visible ;
      private int tblTblcontadorfs_Visible ;
      private int tblTblsrvfat_Visible ;
      private int tblTblpffs_Visible ;
      private int bttImportar_Visible ;
      private int lblTbdtcnt_Visible ;
      private int lblTbcoldtcnt_Visible ;
      private int edtavDatacnt_Visible ;
      private int edtavColdatacnt_Visible ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int AV53NaoValidada_Codigo ;
      private int AV122Sistema_Codigo ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int AV21ContagemResultado_NaoCnfDmnCod ;
      private int GXt_int2 ;
      private int tblTablesdtdemandas_Visible ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A40Contratada_PessoaCod ;
      private int A1080ContratoGestor_UsuarioPesCod ;
      private int subGridsdtdemandas_Titlebackcolor ;
      private int subGridsdtdemandas_Allbackcolor ;
      private int subGridsdtdemandas_Selectioncolor ;
      private int subGridsdtdemandas_Hoveringcolor ;
      private int tblTblpffm_Visible ;
      private int idxLst ;
      private int subGridsdtdemandas_Backcolor ;
      private long GRIDSDTDEMANDAS_nCurrentRecord ;
      private long GRIDSDTDEMANDAS_nFirstRecordOnPage ;
      private decimal AV24ContagemResultado_ValorPF ;
      private decimal GXt_decimal6 ;
      private decimal GXt_decimal7 ;
      private decimal GXt_decimal8 ;
      private decimal GXt_decimal9 ;
      private String Confirmpanel2_Title ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A1046ContagemResultado_Agrupador ;
      private String sGXsfl_373_idx="0001" ;
      private String AV145DataSrvFatSel ;
      private String AV97ColPFBFM ;
      private String AV111ColPFBFMCAST ;
      private String AV56OQImportar ;
      private String AV15ColPFBFS ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String AV113ColDataCAST ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A129Sistema_Sigla ;
      private String A427NaoConformidade_Nome ;
      private String AV8Arquivo ;
      private String AV9Arquivo2 ;
      private String A41Contratada_PessoaNom ;
      private String AV26Contratada_Nome ;
      private String AV165TipoDias ;
      private String A1223ContratoGestor_ContratadaSigla ;
      private String GXCCtlgxBlob ;
      private String Innewwindow_Width ;
      private String Innewwindow_Height ;
      private String Innewwindow_Target ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Confirmtype ;
      private String Confirmpanel2_Confirmationtext ;
      private String Confirmpanel2_Yesbuttoncaption ;
      private String Confirmpanel2_Confirmtype ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String edtavFilename_Internalname ;
      private String AV134FileName ;
      private String edtavFilename_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String WebComp_Wc_anexos_Component ;
      private String chkavSrvparaftr_Internalname ;
      private String chkavNotificar_Internalname ;
      private String AV121StatusDmn ;
      private String AV142DataSrvFat ;
      private String chkavCalcularprazo_Internalname ;
      private String AV7Acao ;
      private String AV132TipoFiltro ;
      private String cmbavOqimportar_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String l1046ContagemResultado_Agrupador ;
      private String edtavCtlsistemanom_Internalname ;
      private String edtavCtldemandafm_Internalname ;
      private String edtavCtldatadmn_Internalname ;
      private String edtavCtldescricao_Internalname ;
      private String edtavCtldataent_Internalname ;
      private String edtavCtldemanda_Internalname ;
      private String edtavCtlcontadorfsnom_Internalname ;
      private String edtavCtllink_Internalname ;
      private String edtavCtlpfbfm_Internalname ;
      private String edtavCtlpflfm_Internalname ;
      private String edtavCtlpfbfs_Internalname ;
      private String edtavCtlpflfs_Internalname ;
      private String edtavCtlcontadorfmcod_Internalname ;
      private String cmbavRequisitante_Internalname ;
      private String cmbavContratada_codigo_Internalname ;
      private String dynavSelservico_codigo_Internalname ;
      private String cmbavContratoservicos_codigo_Internalname ;
      private String cmbavStatusdmn_Internalname ;
      private String cmbavDatasrvfat_Internalname ;
      private String dynavContratadaorigem_codigo_Internalname ;
      private String edtavServico_codigo_Internalname ;
      private String edtavPralincast_Internalname ;
      private String AV114ColDmnCAST ;
      private String edtavColdmncast_Internalname ;
      private String AV109ColPFBFSCAST ;
      private String edtavColpfbfscast_Internalname ;
      private String AV110ColPFLFSCAST ;
      private String edtavColpflfscast_Internalname ;
      private String edtavColpfbfmcast_Internalname ;
      private String AV112ColPFLFMCAST ;
      private String edtavColpflfmcast_Internalname ;
      private String dynavSelcontratadasrv_codigo_Internalname ;
      private String dynavSelosservico_codigo_Internalname ;
      private String cmbavContratoservicosos_codigo_Internalname ;
      private String edtavContratadasrv_codigo_Internalname ;
      private String edtavOsservico_codigo_Internalname ;
      private String edtavPralinha_Internalname ;
      private String AV75ColOSFS ;
      private String edtavColosfs_Internalname ;
      private String AV80ColDescricao ;
      private String edtavColdescricao_Internalname ;
      private String AV166ColProjeto ;
      private String edtavColprojeto_Internalname ;
      private String edtavColpfbfm_Internalname ;
      private String AV98ColPFLFM ;
      private String edtavColpflfm_Internalname ;
      private String AV138ColPFBFSDmn ;
      private String edtavColpfbfsdmn_Internalname ;
      private String AV139ColPFLFSDmn ;
      private String edtavColpflfsdmn_Internalname ;
      private String cmbavContagemresultado_ehvalidacao_Internalname ;
      private String AV76ColNomeSistema ;
      private String edtavColnomesistema_Internalname ;
      private String AV162ColNomeModulo ;
      private String edtavColnomemodulo_Internalname ;
      private String AV168ColTipo ;
      private String edtavColtipo_Internalname ;
      private String edtavOsfm_Internalname ;
      private String AV77ColOSFM ;
      private String edtavColosfm_Internalname ;
      private String AV153Agrupador ;
      private String edtavAgrupador_Internalname ;
      private String edtavDataosfm_Internalname ;
      private String AV78ColDataDmn ;
      private String edtavColdatadmn_Internalname ;
      private String edtavDataentrega_Internalname ;
      private String edtavContagemresultado_observacao_Internalname ;
      private String AV170ColObservacao ;
      private String edtavColobservacao_Internalname ;
      private String edtavContagemresultado_link_Internalname ;
      private String AV84ColLink ;
      private String edtavCollink_Internalname ;
      private String dynavContagemresultado_contadorfmcod_Internalname ;
      private String AV101ColContadorFM ;
      private String edtavColcontadorfm_Internalname ;
      private String edtavDatacnt_Internalname ;
      private String AV147ColDataCnt ;
      private String edtavColdatacnt_Internalname ;
      private String AV173ColParecer ;
      private String edtavColparecer_Internalname ;
      private String dynavContagemresultado_contadorfscod_Internalname ;
      private String AV82ColContadorFS ;
      private String edtavColcontadorfs_Internalname ;
      private String edtavDataosfm2_Internalname ;
      private String edtavOsfm2_Internalname ;
      private String edtavMinutos2_Internalname ;
      private String edtavContagemresultado_valorpf_Internalname ;
      private String edtavPralinhactrf_Internalname ;
      private String AV13ColDemandas ;
      private String edtavColdemandas_Internalname ;
      private String AV159ColPFBFMCTRF ;
      private String edtavColpfbfmctrf_Internalname ;
      private String AV160ColPFLFMCTRF ;
      private String edtavColpflfmctrf_Internalname ;
      private String edtavColpfbfs_Internalname ;
      private String AV16ColPFLFS ;
      private String edtavColpflfs_Internalname ;
      private String AV11ColDeflator ;
      private String edtavColdeflator_Internalname ;
      private String cmbavAcao_Internalname ;
      private String edtavOsfmctrf_Internalname ;
      private String edtavBlob_Internalname ;
      private String AV128Aba ;
      private String edtavAba_Internalname ;
      private String AV130ColFiltro ;
      private String edtavColfiltro_Internalname ;
      private String cmbavTipofiltro_Internalname ;
      private String AV133FiltroColuna ;
      private String edtavFiltrocoluna_Internalname ;
      private String sGXsfl_373_fel_idx="0001" ;
      private String bttCarregar_Caption ;
      private String bttCarregar_Internalname ;
      private String tblTbldadosms_Internalname ;
      private String tblTblconftrf_Internalname ;
      private String tblTblcast_Internalname ;
      private String tblTablegrid_Internalname ;
      private String tblTblanexos_Internalname ;
      private String lblTbosfm_Caption ;
      private String lblTbosfm_Internalname ;
      private String lblTbcosfm_Caption ;
      private String lblTbcosfm_Internalname ;
      private String tblTbldadosdmn_Internalname ;
      private String tblTblcontratada_Internalname ;
      private String tblTblcontadorfm_Internalname ;
      private String tblTblsrvvnc_Internalname ;
      private String tblTblfiltrocoluna_Internalname ;
      private String lblTbehvalidacao_Internalname ;
      private String lblTbcontorigem_Internalname ;
      private String lblTbservico_Internalname ;
      private String lblTbcontpres_Caption ;
      private String lblTbcontpres_Internalname ;
      private String lblTbcontrato_Internalname ;
      private String tblTblcontadorfs_Internalname ;
      private String tblTblsrvfat_Internalname ;
      private String lblTbcontorigem_Caption ;
      private String tblTblpffs_Internalname ;
      private String bttImportar_Internalname ;
      private String lblTbdtcnt_Internalname ;
      private String lblTbcoldtcnt_Internalname ;
      private String lblTbservico_Caption ;
      private String Gx_msg ;
      private String Confirmpanel_Internalname ;
      private String AV61PrimeiroNome ;
      private String AV72UltimoNome ;
      private String lV72UltimoNome ;
      private String lV61PrimeiroNome ;
      private String GXt_char3 ;
      private String GXt_char4 ;
      private String GXt_char5 ;
      private String lblTextblockprazoentrega_Caption ;
      private String lblTextblockprazoentrega_Internalname ;
      private String Innewwindow_Internalname ;
      private String Confirmpanel2_Internalname ;
      private String tblTablesdtdemandas_Internalname ;
      private String lV26Contratada_Nome ;
      private String A1081ContratoGestor_UsuarioPesNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavOqimportar_Jsonclick ;
      private String bttCarregar_Jsonclick ;
      private String bttImportar_Jsonclick ;
      private String subGridsdtdemandas_Internalname ;
      private String subGridsdtdemandas_Class ;
      private String subGridsdtdemandas_Linesclass ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock27_Jsonclick ;
      private String edtavAba_Jsonclick ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String edtavColfiltro_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String cmbavTipofiltro_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String edtavFiltrocoluna_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String lblTextblock16_Internalname ;
      private String lblTextblock16_Jsonclick ;
      private String edtavPralinhactrf_Jsonclick ;
      private String lblTextblock11_Internalname ;
      private String lblTextblock11_Jsonclick ;
      private String edtavColdemandas_Jsonclick ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String edtavColpfbfs_Jsonclick ;
      private String lblTextblock13_Internalname ;
      private String lblTextblock13_Jsonclick ;
      private String edtavColpflfs_Jsonclick ;
      private String lblTextblock14_Internalname ;
      private String lblTextblock14_Jsonclick ;
      private String edtavColdeflator_Jsonclick ;
      private String lblTextblock15_Internalname ;
      private String lblTextblock15_Jsonclick ;
      private String cmbavAcao_Jsonclick ;
      private String lblTextblock43_Internalname ;
      private String lblTextblock43_Jsonclick ;
      private String edtavOsfmctrf_Jsonclick ;
      private String tblTblpffm_Internalname ;
      private String lblTextblock17_Internalname ;
      private String lblTextblock17_Jsonclick ;
      private String edtavColpfbfmctrf_Jsonclick ;
      private String lblTextblock42_Internalname ;
      private String lblTextblock42_Jsonclick ;
      private String edtavColpflfmctrf_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavDataosfm2_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String edtavOsfm2_Jsonclick ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String edtavMinutos2_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String edtavContagemresultado_valorpf_Jsonclick ;
      private String lblTbcontadorfs_Internalname ;
      private String lblTbcontadorfs_Jsonclick ;
      private String dynavContagemresultado_contadorfscod_Jsonclick ;
      private String lblTbcolcrfs_Internalname ;
      private String lblTbcolcrfs_Jsonclick ;
      private String edtavColcontadorfs_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String dynavContagemresultado_contadorfmcod_Jsonclick ;
      private String lblTbcolcrfs2_Internalname ;
      private String lblTbcolcrfs2_Jsonclick ;
      private String edtavColcontadorfm_Jsonclick ;
      private String lblTbdtcnt_Jsonclick ;
      private String edtavDatacnt_Jsonclick ;
      private String lblTbcoldtcnt_Jsonclick ;
      private String edtavColdatacnt_Jsonclick ;
      private String lblTbcoldtcnt2_Internalname ;
      private String lblTbcoldtcnt2_Jsonclick ;
      private String edtavColparecer_Jsonclick ;
      private String lblTextblock45_Internalname ;
      private String lblTextblock45_Jsonclick ;
      private String edtavPralinha_Jsonclick ;
      private String lblTextblock18_Internalname ;
      private String lblTextblock18_Jsonclick ;
      private String edtavColosfs_Jsonclick ;
      private String lblTextblock48_Internalname ;
      private String lblTextblock48_Jsonclick ;
      private String edtavColdescricao_Jsonclick ;
      private String lblTextblock46_Internalname ;
      private String lblTextblock46_Jsonclick ;
      private String edtavColprojeto_Jsonclick ;
      private String lblTextblock25_Internalname ;
      private String lblTextblock25_Jsonclick ;
      private String edtavColpfbfm_Jsonclick ;
      private String lblTbpflfm_Internalname ;
      private String lblTbpflfm_Jsonclick ;
      private String edtavColpflfm_Jsonclick ;
      private String lblTbpflfm2_Internalname ;
      private String lblTbpflfm2_Jsonclick ;
      private String edtavColpfbfsdmn_Jsonclick ;
      private String lblTbpflfm4_Internalname ;
      private String lblTbpflfm4_Jsonclick ;
      private String edtavColpflfsdmn_Jsonclick ;
      private String lblTbehvalidacao_Jsonclick ;
      private String cmbavContagemresultado_ehvalidacao_Jsonclick ;
      private String lblTextblock19_Internalname ;
      private String lblTextblock19_Jsonclick ;
      private String edtavColnomesistema_Jsonclick ;
      private String lblTextblock44_Internalname ;
      private String lblTextblock44_Jsonclick ;
      private String edtavColnomemodulo_Jsonclick ;
      private String lblTextblock47_Internalname ;
      private String lblTextblock47_Jsonclick ;
      private String edtavColtipo_Jsonclick ;
      private String lblTbosfm_Jsonclick ;
      private String edtavOsfm_Jsonclick ;
      private String lblTbcosfm_Jsonclick ;
      private String edtavColosfm_Jsonclick ;
      private String lblTextblock41_Internalname ;
      private String lblTextblock41_Jsonclick ;
      private String edtavAgrupador_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavDataosfm_Jsonclick ;
      private String lblTextblock21_Internalname ;
      private String lblTextblock21_Jsonclick ;
      private String edtavColdatadmn_Jsonclick ;
      private String lblTextblockprazoentrega_Jsonclick ;
      private String edtavDataentrega_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavContagemresultado_observacao_Jsonclick ;
      private String lblTextblock22_Internalname ;
      private String lblTextblock22_Jsonclick ;
      private String edtavColobservacao_Jsonclick ;
      private String lblTextblock24_Internalname ;
      private String lblTextblock24_Jsonclick ;
      private String edtavContagemresultado_link_Jsonclick ;
      private String lblTextblock26_Internalname ;
      private String lblTextblock26_Jsonclick ;
      private String edtavCollink_Jsonclick ;
      private String grpGroup1_Internalname ;
      private String edtavContratadasrv_codigo_Jsonclick ;
      private String edtavOsservico_codigo_Jsonclick ;
      private String tblTblcontratadasrv_Internalname ;
      private String lblTextblock40_Internalname ;
      private String lblTextblock40_Jsonclick ;
      private String dynavSelcontratadasrv_codigo_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String dynavSelosservico_codigo_Jsonclick ;
      private String lblTbcontrato2_Internalname ;
      private String lblTbcontrato2_Jsonclick ;
      private String cmbavContratoservicosos_codigo_Jsonclick ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock28_Jsonclick ;
      private String edtavPralincast_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String edtavColdmncast_Jsonclick ;
      private String lblTextblock32_Internalname ;
      private String lblTextblock32_Jsonclick ;
      private String edtavColpfbfmcast_Jsonclick ;
      private String lblTextblock33_Internalname ;
      private String lblTextblock33_Jsonclick ;
      private String edtavColpflfmcast_Jsonclick ;
      private String lblTextblock30_Internalname ;
      private String lblTextblock30_Jsonclick ;
      private String edtavColpfbfscast_Jsonclick ;
      private String lblTextblock31_Internalname ;
      private String lblTextblock31_Jsonclick ;
      private String edtavColpflfscast_Jsonclick ;
      private String tblTable2_Internalname ;
      private String edtavServico_codigo_Jsonclick ;
      private String lblTbcontpres2_Internalname ;
      private String lblTbcontpres2_Jsonclick ;
      private String cmbavRequisitante_Jsonclick ;
      private String lblTbcontpres_Jsonclick ;
      private String cmbavContratada_codigo_Jsonclick ;
      private String lblTbservico_Jsonclick ;
      private String dynavSelservico_codigo_Jsonclick ;
      private String lblTbcontrato_Jsonclick ;
      private String cmbavContratoservicos_codigo_Jsonclick ;
      private String lblTbcontorigem_Jsonclick ;
      private String dynavContratadaorigem_codigo_Jsonclick ;
      private String lblTbstatus_Internalname ;
      private String lblTbstatus_Jsonclick ;
      private String cmbavStatusdmn_Jsonclick ;
      private String lblTextblock50_Internalname ;
      private String lblTextblock50_Jsonclick ;
      private String cmbavDatasrvfat_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlsistemanom_Jsonclick ;
      private String edtavCtldemandafm_Jsonclick ;
      private String edtavCtldatadmn_Jsonclick ;
      private String edtavCtldescricao_Jsonclick ;
      private String edtavCtldataent_Jsonclick ;
      private String edtavCtldemanda_Jsonclick ;
      private String edtavCtlcontadorfsnom_Jsonclick ;
      private String edtavCtllink_Jsonclick ;
      private String edtavCtlpfbfm_Jsonclick ;
      private String edtavCtlpflfm_Jsonclick ;
      private String edtavCtlpfbfs_Jsonclick ;
      private String edtavCtlpflfs_Jsonclick ;
      private String edtavCtlcontadorfmcod_Jsonclick ;
      private DateTime AV151DataEntrega ;
      private DateTime AV164FimDoExpediente ;
      private DateTime GXt_dtime10 ;
      private DateTime A1869Contrato_DataTermino ;
      private DateTime AV29DataOSFM ;
      private DateTime AV146DataCnt ;
      private DateTime AV30DataOSFM2 ;
      private bool entryPointCalled ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool AV143SrvParaFtr ;
      private bool AV181RequerOrigem ;
      private bool toggleJsOutput ;
      private bool AV95OSAutomatica ;
      private bool A92Contrato_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool A130Sistema_Ativo ;
      private bool A2033ContratoGestor_UsuarioAtv ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV20ContagemResultado_EhValidacao ;
      private bool AV154Notificar ;
      private bool AV175CalcularPrazo ;
      private bool returnInSub ;
      private bool gx_BV373 ;
      private bool gx_refresh_fired ;
      private bool AV55Ok ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n43Contratada_Ativo ;
      private bool n74Contrato_Codigo ;
      private bool n92Contrato_Ativo ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool A632Servico_Ativo ;
      private bool n632Servico_Ativo ;
      private bool n1869Contrato_DataTermino ;
      private bool n2033ContratoGestor_UsuarioAtv ;
      private bool n1223ContratoGestor_ContratadaSigla ;
      private bool n41Contratada_PessoaNom ;
      private bool n1080ContratoGestor_UsuarioPesCod ;
      private bool n1081ContratoGestor_UsuarioPesNom ;
      private bool GXt_boolean1 ;
      private String AV83ContagemResultado_Link ;
      private String A416Sistema_Nome ;
      private String AV57OSFM ;
      private String AV79ContagemResultado_Observacao ;
      private String AV58OSFM2 ;
      private String AV161OSFMCTRF ;
      private String AV69Sistema_Nome ;
      private String lV69Sistema_Nome ;
      private String AV158Blob ;
      private IGxSession AV73WebSession ;
      private GXWebComponent WebComp_Wc_anexos ;
      private ExcelDocumentI AV33ExcelDocument ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridsdtdemandasContainer ;
      private GXWebRow GridsdtdemandasRow ;
      private GXWebColumn GridsdtdemandasColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOqimportar ;
      private GXCombobox cmbavRequisitante ;
      private GXCheckbox chkavSrvparaftr ;
      private GXCheckbox chkavNotificar ;
      private GXCombobox cmbavContratada_codigo ;
      private GXCombobox dynavSelservico_codigo ;
      private GXCombobox cmbavContratoservicos_codigo ;
      private GXCombobox cmbavStatusdmn ;
      private GXCombobox cmbavDatasrvfat ;
      private GXCombobox dynavContratadaorigem_codigo ;
      private GXCombobox dynavSelcontratadasrv_codigo ;
      private GXCombobox dynavSelosservico_codigo ;
      private GXCombobox cmbavContratoservicosos_codigo ;
      private GXCombobox cmbavContagemresultado_ehvalidacao ;
      private GXCheckbox chkavCalcularprazo ;
      private GXCombobox dynavContagemresultado_contadorfmcod ;
      private GXCombobox dynavContagemresultado_contadorfscod ;
      private GXCombobox cmbavAcao ;
      private GXCombobox cmbavTipofiltro ;
      private IDataStoreProvider pr_default ;
      private String[] H00BL2_A1046ContagemResultado_Agrupador ;
      private bool[] H00BL2_n1046ContagemResultado_Agrupador ;
      private int[] H00BL3_A74Contrato_Codigo ;
      private bool[] H00BL3_n74Contrato_Codigo ;
      private int[] H00BL3_A160ContratoServicos_Codigo ;
      private int[] H00BL3_A155Servico_Codigo ;
      private String[] H00BL3_A605Servico_Sigla ;
      private int[] H00BL3_A39Contratada_Codigo ;
      private bool[] H00BL3_A632Servico_Ativo ;
      private bool[] H00BL3_n632Servico_Ativo ;
      private bool[] H00BL3_A638ContratoServicos_Ativo ;
      private int[] H00BL4_A40Contratada_PessoaCod ;
      private int[] H00BL4_A39Contratada_Codigo ;
      private String[] H00BL4_A41Contratada_PessoaNom ;
      private bool[] H00BL4_n41Contratada_PessoaNom ;
      private int[] H00BL4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL4_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL4_A43Contratada_Ativo ;
      private bool[] H00BL4_n43Contratada_Ativo ;
      private int[] H00BL5_A40Contratada_PessoaCod ;
      private int[] H00BL5_A39Contratada_Codigo ;
      private String[] H00BL5_A41Contratada_PessoaNom ;
      private bool[] H00BL5_n41Contratada_PessoaNom ;
      private int[] H00BL5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL5_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL5_A43Contratada_Ativo ;
      private bool[] H00BL5_n43Contratada_Ativo ;
      private int[] H00BL6_A74Contrato_Codigo ;
      private bool[] H00BL6_n74Contrato_Codigo ;
      private int[] H00BL6_A160ContratoServicos_Codigo ;
      private int[] H00BL6_A155Servico_Codigo ;
      private String[] H00BL6_A605Servico_Sigla ;
      private int[] H00BL6_A39Contratada_Codigo ;
      private bool[] H00BL6_A632Servico_Ativo ;
      private bool[] H00BL6_n632Servico_Ativo ;
      private bool[] H00BL6_A638ContratoServicos_Ativo ;
      private int[] H00BL7_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00BL7_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00BL7_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00BL7_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00BL7_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00BL7_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00BL7_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00BL7_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00BL7_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00BL7_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00BL8_A1078ContratoGestor_ContratoCod ;
      private int[] H00BL8_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00BL8_n1136ContratoGestor_ContratadaCod ;
      private int[] H00BL8_A1079ContratoGestor_UsuarioCod ;
      private int[] H00BL8_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00BL8_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00BL9_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00BL9_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00BL9_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00BL9_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00BL9_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00BL9_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00BL9_A43Contratada_Ativo ;
      private bool[] H00BL9_n43Contratada_Ativo ;
      private int[] H00BL10_A39Contratada_Codigo ;
      private int[] H00BL10_A74Contrato_Codigo ;
      private bool[] H00BL10_n74Contrato_Codigo ;
      private bool[] H00BL10_A92Contrato_Ativo ;
      private bool[] H00BL10_n92Contrato_Ativo ;
      private int[] H00BL11_A1078ContratoGestor_ContratoCod ;
      private int[] H00BL11_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00BL11_n1136ContratoGestor_ContratadaCod ;
      private int[] H00BL11_A1079ContratoGestor_UsuarioCod ;
      private int[] H00BL11_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00BL11_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] H00BL12_A427NaoConformidade_Nome ;
      private int[] H00BL12_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00BL12_A426NaoConformidade_Codigo ;
      private String[] H00BL13_A416Sistema_Nome ;
      private bool[] H00BL13_A130Sistema_Ativo ;
      private int[] H00BL13_A135Sistema_AreaTrabalhoCod ;
      private int[] H00BL13_A127Sistema_Codigo ;
      private String[] H00BL13_A129Sistema_Sigla ;
      private int[] H00BL14_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00BL14_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00BL14_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00BL14_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00BL14_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00BL14_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00BL15_A74Contrato_Codigo ;
      private bool[] H00BL15_n74Contrato_Codigo ;
      private int[] H00BL15_A1013Contrato_PrepostoCod ;
      private bool[] H00BL15_n1013Contrato_PrepostoCod ;
      private int[] H00BL15_A39Contratada_Codigo ;
      private int[] H00BL19_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00BL19_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00BL19_A74Contrato_Codigo ;
      private bool[] H00BL19_n74Contrato_Codigo ;
      private int[] H00BL19_A39Contratada_Codigo ;
      private int[] H00BL19_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL19_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL19_A43Contratada_Ativo ;
      private bool[] H00BL19_n43Contratada_Ativo ;
      private bool[] H00BL19_A632Servico_Ativo ;
      private bool[] H00BL19_n632Servico_Ativo ;
      private bool[] H00BL19_A92Contrato_Ativo ;
      private bool[] H00BL19_n92Contrato_Ativo ;
      private int[] H00BL19_A75Contrato_AreaTrabalhoCod ;
      private DateTime[] H00BL19_A1869Contrato_DataTermino ;
      private bool[] H00BL19_n1869Contrato_DataTermino ;
      private int[] H00BL20_A1078ContratoGestor_ContratoCod ;
      private int[] H00BL20_A1079ContratoGestor_UsuarioCod ;
      private bool[] H00BL20_A2033ContratoGestor_UsuarioAtv ;
      private bool[] H00BL20_n2033ContratoGestor_UsuarioAtv ;
      private String[] H00BL20_A1223ContratoGestor_ContratadaSigla ;
      private bool[] H00BL20_n1223ContratoGestor_ContratadaSigla ;
      private int[] H00BL20_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00BL20_n1136ContratoGestor_ContratadaCod ;
      private int[] H00BL21_A74Contrato_Codigo ;
      private bool[] H00BL21_n74Contrato_Codigo ;
      private int[] H00BL21_A39Contratada_Codigo ;
      private int[] H00BL21_A155Servico_Codigo ;
      private String[] H00BL21_A77Contrato_Numero ;
      private int[] H00BL21_A160ContratoServicos_Codigo ;
      private int[] H00BL22_A74Contrato_Codigo ;
      private bool[] H00BL22_n74Contrato_Codigo ;
      private int[] H00BL22_A39Contratada_Codigo ;
      private int[] H00BL22_A155Servico_Codigo ;
      private String[] H00BL22_A77Contrato_Numero ;
      private int[] H00BL22_A160ContratoServicos_Codigo ;
      private int[] H00BL23_A40Contratada_PessoaCod ;
      private int[] H00BL23_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00BL23_n52Contratada_AreaTrabalhoCod ;
      private String[] H00BL23_A41Contratada_PessoaNom ;
      private bool[] H00BL23_n41Contratada_PessoaNom ;
      private bool[] H00BL23_A43Contratada_Ativo ;
      private bool[] H00BL23_n43Contratada_Ativo ;
      private int[] H00BL23_A39Contratada_Codigo ;
      private int[] H00BL24_A1078ContratoGestor_ContratoCod ;
      private int[] H00BL24_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00BL24_n1136ContratoGestor_ContratadaCod ;
      private int[] H00BL24_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] H00BL24_n1080ContratoGestor_UsuarioPesCod ;
      private bool[] H00BL24_A43Contratada_Ativo ;
      private bool[] H00BL24_n43Contratada_Ativo ;
      private bool[] H00BL24_A92Contrato_Ativo ;
      private bool[] H00BL24_n92Contrato_Ativo ;
      private bool[] H00BL24_A2033ContratoGestor_UsuarioAtv ;
      private bool[] H00BL24_n2033ContratoGestor_UsuarioAtv ;
      private String[] H00BL24_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] H00BL24_n1081ContratoGestor_UsuarioPesNom ;
      private int[] H00BL24_A1079ContratoGestor_UsuarioCod ;
      private int[] H00BL24_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00BL24_n1446ContratoGestor_ContratadaAreaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV155SDT_ContagemResultadoEvidencias ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV93SDT_Demandas ;
      private GXWebForm Form ;
      private SdtSistema AV66Sistema ;
      private SdtTmp_File AV71Tmp_File ;
      private wwpbaseobjects.SdtWWPContext AV74WWPContext ;
   }

   public class wp_importfromfile__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BL19( IGxContext context ,
                                              int AV156AreaTrabalho_Codigo ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              DateTime A1869Contrato_DataTermino ,
                                              bool A92Contrato_Ativo ,
                                              bool A43Contratada_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [1] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T1.[Contrato_Codigo], T1.[Contratada_Codigo], T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratada_Ativo], T5.[Servico_Ativo], T1.[Contrato_Ativo], T1.[Contrato_AreaTrabalhoCod], COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT CASE  WHEN T6.[Contrato_DataVigenciaTermino] > COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T6.[Contrato_DataVigenciaTermino] ELSE COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T6.[Contrato_Codigo] FROM ([Contrato] T6 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC4] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T3.[Contratada_Ativo] = 1)";
         if ( AV156AreaTrabalho_Codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV156AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int11[0] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 14 :
                     return conditional_H00BL19(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (bool)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BL2 ;
          prmH00BL2 = new Object[] {
          new Object[] {"@l1046ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV156AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL3 ;
          prmH00BL3 = new Object[] {
          new Object[] {"@AV127Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL4 ;
          prmH00BL4 = new Object[] {
          new Object[] {"@AV74WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL5 ;
          prmH00BL5 = new Object[] {
          new Object[] {"@AV74WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL6 ;
          prmH00BL6 = new Object[] {
          new Object[] {"@AV135SelContratadaSrv_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL7 ;
          prmH00BL7 = new Object[] {
          new Object[] {"@AV74WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL8 ;
          prmH00BL8 = new Object[] {
          new Object[] {"@AV127Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL9 ;
          prmH00BL9 = new Object[] {
          new Object[] {"@AV150ContratadaOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL10 ;
          prmH00BL10 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL11 ;
          prmH00BL11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL12 ;
          prmH00BL12 = new Object[] {
          new Object[] {"@AV156AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL13 ;
          prmH00BL13 = new Object[] {
          new Object[] {"@lV69Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV156AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL14 ;
          prmH00BL14 = new Object[] {
          new Object[] {"@lV61PrimeiroNome",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72UltimoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV127Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL15 ;
          prmH00BL15 = new Object[] {
          new Object[] {"@AV150ContratadaOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL20 ;
          prmH00BL20 = new Object[] {
          new Object[] {"@AV178Requisitante",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL21 ;
          prmH00BL21 = new Object[] {
          new Object[] {"@AV120Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL22 ;
          prmH00BL22 = new Object[] {
          new Object[] {"@AV126OSServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136ContratadaSrv_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL23 ;
          prmH00BL23 = new Object[] {
          new Object[] {"@lV26Contratada_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV156AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL24 ;
          prmH00BL24 = new Object[] {
          new Object[] {"@AV74WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BL19 ;
          prmH00BL19 = new Object[] {
          new Object[] {"@AV156AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BL2", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Agrupador] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Agrupador]) like '%' + UPPER(@l1046ContagemResultado_Agrupador)) AND (T2.[Contratada_AreaTrabalhoCod] = @AV156AreaTrabalho_Codigo) ORDER BY T1.[ContagemResultado_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL2,0,0,true,false )
             ,new CursorDef("H00BL3", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[Servico_Ativo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T3.[Contratada_Codigo] = @AV127Contratada_Codigo) AND (T2.[Servico_Ativo] = 1) AND (T1.[ContratoServicos_Ativo] = 1) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL3,0,0,true,false )
             ,new CursorDef("H00BL4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV74WWPC_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL4,0,0,true,false )
             ,new CursorDef("H00BL5", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV74WWPC_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL5,0,0,true,false )
             ,new CursorDef("H00BL6", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[Servico_Ativo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T3.[Contratada_Codigo] = @AV135SelContratadaSrv_Codigo) AND (T2.[Servico_Ativo] = 1) AND (T1.[ContratoServicos_Ativo] = 1) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL6,0,0,true,false )
             ,new CursorDef("H00BL7", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV74WWPC_1Areatrabalho_codigo) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV127Contratada_Codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL7,0,0,true,false )
             ,new CursorDef("H00BL8", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T2.[Contratada_Codigo] = @AV127Contratada_Codigo ORDER BY T1.[ContratoGestor_ContratoCod], T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL8,100,0,true,false )
             ,new CursorDef("H00BL9", "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T3.[Contratada_Ativo] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV150ContratadaOrigem_Codigo) AND (T2.[Usuario_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL9,100,0,true,false )
             ,new CursorDef("H00BL10", "SELECT TOP 1 [Contratada_Codigo], [Contrato_Codigo], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @ContratadaUsuario_ContratadaCod) AND ([Contrato_Ativo] = 1) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL10,1,0,true,true )
             ,new CursorDef("H00BL11", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (T2.[Contratada_Codigo] = @ContratadaUsuario_ContratadaCod) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL11,100,0,true,false )
             ,new CursorDef("H00BL12", "SELECT TOP 1 [NaoConformidade_Nome], [NaoConformidade_AreaTrabalhoCod], [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV156AreaTrabalho_Codigo) AND ([NaoConformidade_Nome] like '%N�O VALIDADA%') ORDER BY [NaoConformidade_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL12,1,0,true,true )
             ,new CursorDef("H00BL13", "SELECT TOP 1 [Sistema_Nome], [Sistema_Ativo], [Sistema_AreaTrabalhoCod], [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_Nome] like @lV69Sistema_Nome) AND ([Sistema_AreaTrabalhoCod] = @AV156AreaTrabalho_Codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL13,1,0,true,true )
             ,new CursorDef("H00BL14", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T3.[Pessoa_Nome] like @lV61PrimeiroNome and T3.[Pessoa_Nome] like @lV72UltimoNome) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV127Contratada_Codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL14,100,0,true,false )
             ,new CursorDef("H00BL15", "SELECT TOP 1 [Contrato_Codigo], [Contrato_PrepostoCod], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @AV150ContratadaOrigem_Codigo) AND ([Contrato_PrepostoCod] > 0) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL15,1,0,true,true )
             ,new CursorDef("H00BL19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL19,100,0,true,false )
             ,new CursorDef("H00BL20", "SELECT DISTINCT NULL AS [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod], NULL AS [ContratoGestor_UsuarioAtv], [ContratoGestor_ContratadaSigla], [ContratoGestor_ContratadaCod] FROM ( SELECT TOP(100) PERCENT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T4.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratoGestor_UsuarioCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV178Requisitante and T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (T4.[Usuario_Ativo] = 1) ORDER BY T1.[ContratoGestor_UsuarioCod], T1.[ContratoGestor_ContratoCod], T2.[Contratada_Codigo], T3.[Contratada_Sigla]) DistinctT ORDER BY [ContratoGestor_UsuarioCod], [ContratoGestor_ContratadaCod], [ContratoGestor_ContratadaSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL20,100,0,true,false )
             ,new CursorDef("H00BL21", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[Servico_Codigo], T2.[Contrato_Numero], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV120Servico_Codigo) AND (T2.[Contratada_Codigo] = @AV127Contratada_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL21,100,0,true,false )
             ,new CursorDef("H00BL22", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[Servico_Codigo], T2.[Contrato_Numero], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV126OSServico_Codigo) AND (T2.[Contratada_Codigo] = @AV136ContratadaSrv_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL22,100,0,true,false )
             ,new CursorDef("H00BL23", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Ativo], T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T2.[Pessoa_Nome] like @lV26Contratada_Nome) AND (T1.[Contratada_AreaTrabalhoCod] = @AV156AreaTrabalho_Codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL23,1,0,true,true )
             ,new CursorDef("H00BL24", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T4.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod, T3.[Contratada_Ativo], T2.[Contrato_Ativo], T4.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, T5.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE (T4.[Usuario_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) AND (T2.[Contrato_AreaTrabalhoCod] = @AV74WWPC_1Areatrabalho_codigo) ORDER BY T5.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BL24,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 25) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
