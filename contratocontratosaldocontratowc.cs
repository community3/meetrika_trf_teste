/*
               File: ContratoContratoSaldoContratoWC
        Description: Contrato Contrato Saldo Contrato WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 1:57:24.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratocontratosaldocontratowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratocontratosaldocontratowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratocontratosaldocontratowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV7Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_19_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV28TFSaldoContrato_VigenciaInicio = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
                  AV29TFSaldoContrato_VigenciaInicio_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
                  AV34TFSaldoContrato_VigenciaFim = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
                  AV35TFSaldoContrato_VigenciaFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
                  AV40TFSaldoContrato_UnidadeMedicao_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
                  AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel", AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel);
                  AV44TFSaldoContrato_Credito = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5)));
                  AV45TFSaldoContrato_Credito_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5)));
                  AV48TFSaldoContrato_Reservado = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5)));
                  AV49TFSaldoContrato_Reservado_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5)));
                  AV52TFSaldoContrato_Executado = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5)));
                  AV53TFSaldoContrato_Executado_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5)));
                  AV56TFSaldoContrato_Saldo = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5)));
                  AV57TFSaldoContrato_Saldo_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5)));
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
                  AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
                  AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace", AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace);
                  AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace);
                  AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
                  AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
                  AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace);
                  AV71Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAMI2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV71Pgmname = "ContratoContratoSaldoContratoWC";
               context.Gx_err = 0;
               /* Using cursor H00MI3 */
               pr_default.execute(0, new Object[] {AV7Contrato_Codigo});
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A40000GXC1 = H00MI3_A40000GXC1[0];
                  n40000GXC1 = H00MI3_n40000GXC1[0];
               }
               else
               {
                  A40000GXC1 = 0;
                  n40000GXC1 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40000GXC1", StringUtil.LTrim( StringUtil.Str( (decimal)(A40000GXC1), 9, 0)));
               }
               pr_default.close(0);
               WSMI2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Contrato Saldo Contrato WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204291572494");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratocontratosaldocontratowc.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME", StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL", StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO", StringUtil.LTrim( StringUtil.NToC( AV44TFSaldoContrato_Credito, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO_TO", StringUtil.LTrim( StringUtil.NToC( AV45TFSaldoContrato_Credito_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO", StringUtil.LTrim( StringUtil.NToC( AV48TFSaldoContrato_Reservado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO_TO", StringUtil.LTrim( StringUtil.NToC( AV49TFSaldoContrato_Reservado_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO", StringUtil.LTrim( StringUtil.NToC( AV52TFSaldoContrato_Executado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO_TO", StringUtil.LTrim( StringUtil.NToC( AV53TFSaldoContrato_Executado_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_SALDO", StringUtil.LTrim( StringUtil.NToC( AV56TFSaldoContrato_Saldo, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_SALDO_TO", StringUtil.LTrim( StringUtil.NToC( AV57TFSaldoContrato_Saldo_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA", AV27SaldoContrato_VigenciaInicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA", AV27SaldoContrato_VigenciaInicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA", AV33SaldoContrato_VigenciaFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA", AV33SaldoContrato_VigenciaFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_NOMETITLEFILTERDATA", AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_NOMETITLEFILTERDATA", AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_CREDITOTITLEFILTERDATA", AV43SaldoContrato_CreditoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_CREDITOTITLEFILTERDATA", AV43SaldoContrato_CreditoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_RESERVADOTITLEFILTERDATA", AV47SaldoContrato_ReservadoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_RESERVADOTITLEFILTERDATA", AV47SaldoContrato_ReservadoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA", AV51SaldoContrato_ExecutadoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA", AV51SaldoContrato_ExecutadoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_SALDOTITLEFILTERDATA", AV55SaldoContrato_SaldoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_SALDOTITLEFILTERDATA", AV55SaldoContrato_SaldoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV71Pgmname));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vISERRO", AV16IsErro);
         GxWebStd.gx_hidden_field( context, sPrefix+"vNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSALDOCONTRATO_CREDITO", StringUtil.LTrim( StringUtil.NToC( AV22SaldoContrato_Credito, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Caption", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Cls", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciainicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_vigenciainicio_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Caption", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Cls", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_vigenciafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_vigenciafim_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Caption", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Cls", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_unidademedicao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Caption", StringUtil.RTrim( Ddo_saldocontrato_credito_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_credito_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Cls", StringUtil.RTrim( Ddo_saldocontrato_credito_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_credito_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_credito_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_credito_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_credito_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_credito_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_credito_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_credito_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_credito_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_credito_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_credito_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_credito_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_credito_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_credito_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Caption", StringUtil.RTrim( Ddo_saldocontrato_reservado_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_reservado_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Cls", StringUtil.RTrim( Ddo_saldocontrato_reservado_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_reservado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_reservado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_reservado_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_reservado_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_reservado_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_reservado_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_reservado_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_reservado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_reservado_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_reservado_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_reservado_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_reservado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Caption", StringUtil.RTrim( Ddo_saldocontrato_executado_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_executado_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Cls", StringUtil.RTrim( Ddo_saldocontrato_executado_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_executado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_executado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_executado_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_executado_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_executado_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_executado_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_executado_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_executado_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_executado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_executado_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_executado_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_executado_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_executado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Caption", StringUtil.RTrim( Ddo_saldocontrato_saldo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_saldo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Cls", StringUtil.RTrim( Ddo_saldocontrato_saldo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_saldo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_saldo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_saldo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Datalistfixedvalues", StringUtil.RTrim( Ddo_saldocontrato_saldo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_saldo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_saldo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_saldo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_saldo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_saldo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_saldo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_saldo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_saldo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciainicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_vigenciafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_credito_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_credito_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_reservado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_reservado_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_executado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_executado_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_saldo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_saldo_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMI2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratocontratosaldocontratowc.js", "?2020429157273");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoContratoSaldoContratoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Contrato Saldo Contrato WC" ;
      }

      protected void WBMI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratocontratosaldocontratowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_MI2( true) ;
         }
         else
         {
            wb_table1_2_MI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciainicio_Internalname, context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"), context.localUtil.Format( AV28TFSaldoContrato_VigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciainicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciainicio_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciainicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciainicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciainicio_to_Internalname, context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"), context.localUtil.Format( AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciainicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciainicio_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciainicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciainicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_saldocontrato_vigenciainicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname, context.localUtil.Format(AV30DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"), context.localUtil.Format( AV30DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname, context.localUtil.Format(AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciafim_Internalname, context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"), context.localUtil.Format( AV34TFSaldoContrato_VigenciaFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciafim_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsaldocontrato_vigenciafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_vigenciafim_to_Internalname, context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"), context.localUtil.Format( AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_vigenciafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_vigenciafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsaldocontrato_vigenciafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsaldocontrato_vigenciafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_saldocontrato_vigenciafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciafimauxdate_Internalname, context.localUtil.Format(AV36DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"), context.localUtil.Format( AV36DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname, context.localUtil.Format(AV37DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV37DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_unidademedicao_nome_Internalname, StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome), StringUtil.RTrim( context.localUtil.Format( AV40TFSaldoContrato_UnidadeMedicao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_unidademedicao_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_unidademedicao_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_unidademedicao_nome_sel_Internalname, StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_unidademedicao_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_unidademedicao_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_credito_Internalname, StringUtil.LTrim( StringUtil.NToC( AV44TFSaldoContrato_Credito, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV44TFSaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_credito_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_credito_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_credito_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV45TFSaldoContrato_Credito_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV45TFSaldoContrato_Credito_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_credito_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_credito_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV48TFSaldoContrato_Reservado, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV48TFSaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_reservado_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_reservado_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_reservado_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV49TFSaldoContrato_Reservado_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV49TFSaldoContrato_Reservado_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_reservado_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_reservado_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_executado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV52TFSaldoContrato_Executado, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV52TFSaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_executado_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_executado_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_executado_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV53TFSaldoContrato_Executado_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV53TFSaldoContrato_Executado_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,55);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_executado_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_executado_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV56TFSaldoContrato_Saldo, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV56TFSaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_saldo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_saldo_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_saldo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV57TFSaldoContrato_Saldo_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV57TFSaldoContrato_Saldo_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_saldo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_saldo_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Internalname, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_CREDITOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_RESERVADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_EXECUTADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_SALDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoSaldoContratoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTMI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Contrato Saldo Contrato WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPMI0( ) ;
            }
         }
      }

      protected void WSMI2( )
      {
         STARTMI2( ) ;
         EVTMI2( ) ;
      }

      protected void EVTMI2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11MI2 */
                                    E11MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12MI2 */
                                    E12MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13MI2 */
                                    E13MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MI2 */
                                    E14MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15MI2 */
                                    E15MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16MI2 */
                                    E16MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17MI2 */
                                    E17MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18MI2 */
                                    E18MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINICIALIZARSALDO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19MI2 */
                                    E19MI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "VRESERVARSALDO.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 25), "VDEBITOSALDORESERVA.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "VDEBITOSALDO.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 25), "VDEBITOSALDORESERVA.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "VRESERVARSALDO.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "VDEBITOSALDO.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMI0( ) ;
                              }
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
                              A1571SaldoContrato_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaInicio_Internalname), 0));
                              A1572SaldoContrato_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaFim_Internalname), 0));
                              A1784SaldoContrato_UnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtSaldoContrato_UnidadeMedicao_Nome_Internalname));
                              n1784SaldoContrato_UnidadeMedicao_Nome = false;
                              A1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtSaldoContrato_Credito_Internalname), ",", ".");
                              A1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Reservado_Internalname), ",", ".");
                              AV24DebitoSaldoReserva = cgiGet( edtavDebitosaldoreserva_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDebitosaldoreserva_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24DebitoSaldoReserva)) ? AV68Debitosaldoreserva_GXI : context.convertURL( context.PathToRelativeUrl( AV24DebitoSaldoReserva))));
                              A1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Executado_Internalname), ",", ".");
                              A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtSaldoContrato_Saldo_Internalname), ",", ".");
                              AV25ReservarSaldo = cgiGet( edtavReservarsaldo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavReservarsaldo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReservarSaldo)) ? AV69Reservarsaldo_GXI : context.convertURL( context.PathToRelativeUrl( AV25ReservarSaldo))));
                              AV26DebitoSaldo = cgiGet( edtavDebitosaldo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDebitosaldo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26DebitoSaldo)) ? AV70Debitosaldo_GXI : context.convertURL( context.PathToRelativeUrl( AV26DebitoSaldo))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20MI2 */
                                          E20MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21MI2 */
                                          E21MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22MI2 */
                                          E22MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VRESERVARSALDO.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23MI2 */
                                          E23MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDEBITOSALDORESERVA.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24MI2 */
                                          E24MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDEBITOSALDO.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25MI2 */
                                          E25MI2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_vigenciainicio Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO"), 0) != AV28TFSaldoContrato_VigenciaInicio )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_vigenciainicio_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO"), 0) != AV29TFSaldoContrato_VigenciaInicio_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_vigenciafim Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM"), 0) != AV34TFSaldoContrato_VigenciaFim )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_vigenciafim_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO"), 0) != AV35TFSaldoContrato_VigenciaFim_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_unidademedicao_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME"), AV40TFSaldoContrato_UnidadeMedicao_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_unidademedicao_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL"), AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_credito Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO"), ",", ".") != AV44TFSaldoContrato_Credito )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_credito_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO_TO"), ",", ".") != AV45TFSaldoContrato_Credito_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_reservado Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO"), ",", ".") != AV48TFSaldoContrato_Reservado )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_reservado_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO_TO"), ",", ".") != AV49TFSaldoContrato_Reservado_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_executado Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO"), ",", ".") != AV52TFSaldoContrato_Executado )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_executado_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO_TO"), ",", ".") != AV53TFSaldoContrato_Executado_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_saldo Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_SALDO"), ",", ".") != AV56TFSaldoContrato_Saldo )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_saldo_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_SALDO_TO"), ",", ".") != AV57TFSaldoContrato_Saldo_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPMI0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMI2( ) ;
            }
         }
      }

      protected void PAMI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       DateTime AV28TFSaldoContrato_VigenciaInicio ,
                                       DateTime AV29TFSaldoContrato_VigenciaInicio_To ,
                                       DateTime AV34TFSaldoContrato_VigenciaFim ,
                                       DateTime AV35TFSaldoContrato_VigenciaFim_To ,
                                       String AV40TFSaldoContrato_UnidadeMedicao_Nome ,
                                       String AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                       decimal AV44TFSaldoContrato_Credito ,
                                       decimal AV45TFSaldoContrato_Credito_To ,
                                       decimal AV48TFSaldoContrato_Reservado ,
                                       decimal AV49TFSaldoContrato_Reservado_To ,
                                       decimal AV52TFSaldoContrato_Executado ,
                                       decimal AV53TFSaldoContrato_Executado_To ,
                                       decimal AV56TFSaldoContrato_Saldo ,
                                       decimal AV57TFSaldoContrato_Saldo_To ,
                                       int AV7Contrato_Codigo ,
                                       String AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace ,
                                       String AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace ,
                                       String AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace ,
                                       String AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace ,
                                       String AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace ,
                                       String AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace ,
                                       String AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace ,
                                       String AV71Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFMI2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( sPrefix, A1571SaldoContrato_VigenciaInicio));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_VIGENCIAINICIO", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( sPrefix, A1572SaldoContrato_VigenciaFim));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_VIGENCIAFIM", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_CREDITO", StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_RESERVADO", StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_EXECUTADO", StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_SALDO", StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV71Pgmname = "ContratoContratoSaldoContratoWC";
         context.Gx_err = 0;
      }

      protected void RFMI2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E21MI2 */
         E21MI2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV28TFSaldoContrato_VigenciaInicio ,
                                                 AV29TFSaldoContrato_VigenciaInicio_To ,
                                                 AV34TFSaldoContrato_VigenciaFim ,
                                                 AV35TFSaldoContrato_VigenciaFim_To ,
                                                 AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                                 AV40TFSaldoContrato_UnidadeMedicao_Nome ,
                                                 AV44TFSaldoContrato_Credito ,
                                                 AV45TFSaldoContrato_Credito_To ,
                                                 AV48TFSaldoContrato_Reservado ,
                                                 AV49TFSaldoContrato_Reservado_To ,
                                                 AV52TFSaldoContrato_Executado ,
                                                 AV53TFSaldoContrato_Executado_To ,
                                                 AV56TFSaldoContrato_Saldo ,
                                                 AV57TFSaldoContrato_Saldo_To ,
                                                 A1571SaldoContrato_VigenciaInicio ,
                                                 A1572SaldoContrato_VigenciaFim ,
                                                 A1784SaldoContrato_UnidadeMedicao_Nome ,
                                                 A1573SaldoContrato_Credito ,
                                                 A1574SaldoContrato_Reservado ,
                                                 A1575SaldoContrato_Executado ,
                                                 A1576SaldoContrato_Saldo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A74Contrato_Codigo ,
                                                 AV7Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV40TFSaldoContrato_UnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
            /* Using cursor H00MI5 */
            pr_default.execute(1, new Object[] {AV7Contrato_Codigo, AV7Contrato_Codigo, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, lV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_19_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1783SaldoContrato_UnidadeMedicao_Codigo = H00MI5_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
               A1576SaldoContrato_Saldo = H00MI5_A1576SaldoContrato_Saldo[0];
               A1575SaldoContrato_Executado = H00MI5_A1575SaldoContrato_Executado[0];
               A1574SaldoContrato_Reservado = H00MI5_A1574SaldoContrato_Reservado[0];
               A1573SaldoContrato_Credito = H00MI5_A1573SaldoContrato_Credito[0];
               A1784SaldoContrato_UnidadeMedicao_Nome = H00MI5_A1784SaldoContrato_UnidadeMedicao_Nome[0];
               n1784SaldoContrato_UnidadeMedicao_Nome = H00MI5_n1784SaldoContrato_UnidadeMedicao_Nome[0];
               A1572SaldoContrato_VigenciaFim = H00MI5_A1572SaldoContrato_VigenciaFim[0];
               A1571SaldoContrato_VigenciaInicio = H00MI5_A1571SaldoContrato_VigenciaInicio[0];
               A1561SaldoContrato_Codigo = H00MI5_A1561SaldoContrato_Codigo[0];
               A74Contrato_Codigo = H00MI5_A74Contrato_Codigo[0];
               A40000GXC1 = H00MI5_A40000GXC1[0];
               n40000GXC1 = H00MI5_n40000GXC1[0];
               A1784SaldoContrato_UnidadeMedicao_Nome = H00MI5_A1784SaldoContrato_UnidadeMedicao_Nome[0];
               n1784SaldoContrato_UnidadeMedicao_Nome = H00MI5_n1784SaldoContrato_UnidadeMedicao_Nome[0];
               A40000GXC1 = H00MI5_A40000GXC1[0];
               n40000GXC1 = H00MI5_n40000GXC1[0];
               /* Execute user event: E22MI2 */
               E22MI2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 19;
            WBMI0( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV28TFSaldoContrato_VigenciaInicio ,
                                              AV29TFSaldoContrato_VigenciaInicio_To ,
                                              AV34TFSaldoContrato_VigenciaFim ,
                                              AV35TFSaldoContrato_VigenciaFim_To ,
                                              AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                              AV40TFSaldoContrato_UnidadeMedicao_Nome ,
                                              AV44TFSaldoContrato_Credito ,
                                              AV45TFSaldoContrato_Credito_To ,
                                              AV48TFSaldoContrato_Reservado ,
                                              AV49TFSaldoContrato_Reservado_To ,
                                              AV52TFSaldoContrato_Executado ,
                                              AV53TFSaldoContrato_Executado_To ,
                                              AV56TFSaldoContrato_Saldo ,
                                              AV57TFSaldoContrato_Saldo_To ,
                                              A1571SaldoContrato_VigenciaInicio ,
                                              A1572SaldoContrato_VigenciaFim ,
                                              A1784SaldoContrato_UnidadeMedicao_Nome ,
                                              A1573SaldoContrato_Credito ,
                                              A1574SaldoContrato_Reservado ,
                                              A1575SaldoContrato_Executado ,
                                              A1576SaldoContrato_Saldo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A74Contrato_Codigo ,
                                              AV7Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40TFSaldoContrato_UnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
         /* Using cursor H00MI7 */
         pr_default.execute(2, new Object[] {AV7Contrato_Codigo, AV7Contrato_Codigo, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, lV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To});
         GRID_nRecordCount = H00MI7_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV28TFSaldoContrato_VigenciaInicio, AV29TFSaldoContrato_VigenciaInicio_To, AV34TFSaldoContrato_VigenciaFim, AV35TFSaldoContrato_VigenciaFim_To, AV40TFSaldoContrato_UnidadeMedicao_Nome, AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV44TFSaldoContrato_Credito, AV45TFSaldoContrato_Credito_To, AV48TFSaldoContrato_Reservado, AV49TFSaldoContrato_Reservado_To, AV52TFSaldoContrato_Executado, AV53TFSaldoContrato_Executado_To, AV56TFSaldoContrato_Saldo, AV57TFSaldoContrato_Saldo_To, AV7Contrato_Codigo, AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, AV71Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMI0( )
      {
         /* Before Start, stand alone formulas. */
         AV71Pgmname = "ContratoContratoSaldoContratoWC";
         context.Gx_err = 0;
         /* Using cursor H00MI9 */
         pr_default.execute(3, new Object[] {AV7Contrato_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40000GXC1 = H00MI9_A40000GXC1[0];
            n40000GXC1 = H00MI9_n40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
            n40000GXC1 = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40000GXC1", StringUtil.LTrim( StringUtil.Str( (decimal)(A40000GXC1), 9, 0)));
         }
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E20MI2 */
         E20MI2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV59DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA"), AV27SaldoContrato_VigenciaInicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA"), AV33SaldoContrato_VigenciaFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_NOMETITLEFILTERDATA"), AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_CREDITOTITLEFILTERDATA"), AV43SaldoContrato_CreditoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_RESERVADOTITLEFILTERDATA"), AV47SaldoContrato_ReservadoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA"), AV51SaldoContrato_ExecutadoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_SALDOTITLEFILTERDATA"), AV55SaldoContrato_SaldoTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Inicio"}), 1, "vTFSALDOCONTRATO_VIGENCIAINICIO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFSaldoContrato_VigenciaInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
            }
            else
            {
               AV28TFSaldoContrato_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciainicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Inicio_To"}), 1, "vTFSALDOCONTRATO_VIGENCIAINICIO_TO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciainicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFSaldoContrato_VigenciaInicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            }
            else
            {
               AV29TFSaldoContrato_VigenciaInicio_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciainicio_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Inicio Aux Date"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30DDO_SaldoContrato_VigenciaInicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_SaldoContrato_VigenciaInicioAuxDate", context.localUtil.Format(AV30DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"));
            }
            else
            {
               AV30DDO_SaldoContrato_VigenciaInicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_SaldoContrato_VigenciaInicioAuxDate", context.localUtil.Format(AV30DDO_SaldoContrato_VigenciaInicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Inicio Aux Date To"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo", context.localUtil.Format(AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo", context.localUtil.Format(AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Fim"}), 1, "vTFSALDOCONTRATO_VIGENCIAFIM");
               GX_FocusControl = edtavTfsaldocontrato_vigenciafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFSaldoContrato_VigenciaFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
            }
            else
            {
               AV34TFSaldoContrato_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsaldocontrato_vigenciafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSaldo Contrato_Vigencia Fim_To"}), 1, "vTFSALDOCONTRATO_VIGENCIAFIM_TO");
               GX_FocusControl = edtavTfsaldocontrato_vigenciafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFSaldoContrato_VigenciaFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            }
            else
            {
               AV35TFSaldoContrato_VigenciaFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsaldocontrato_vigenciafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Fim Aux Date"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36DDO_SaldoContrato_VigenciaFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_SaldoContrato_VigenciaFimAuxDate", context.localUtil.Format(AV36DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"));
            }
            else
            {
               AV36DDO_SaldoContrato_VigenciaFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_SaldoContrato_VigenciaFimAuxDate", context.localUtil.Format(AV36DDO_SaldoContrato_VigenciaFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Saldo Contrato_Vigencia Fim Aux Date To"}), 1, "vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DDO_SaldoContrato_VigenciaFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DDO_SaldoContrato_VigenciaFimAuxDateTo", context.localUtil.Format(AV37DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV37DDO_SaldoContrato_VigenciaFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DDO_SaldoContrato_VigenciaFimAuxDateTo", context.localUtil.Format(AV37DDO_SaldoContrato_VigenciaFimAuxDateTo, "99/99/99"));
            }
            AV40TFSaldoContrato_UnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtavTfsaldocontrato_unidademedicao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
            AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfsaldocontrato_unidademedicao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel", AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CREDITO");
               GX_FocusControl = edtavTfsaldocontrato_credito_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFSaldoContrato_Credito = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5)));
            }
            else
            {
               AV44TFSaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CREDITO_TO");
               GX_FocusControl = edtavTfsaldocontrato_credito_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFSaldoContrato_Credito_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5)));
            }
            else
            {
               AV45TFSaldoContrato_Credito_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_credito_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_RESERVADO");
               GX_FocusControl = edtavTfsaldocontrato_reservado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFSaldoContrato_Reservado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5)));
            }
            else
            {
               AV48TFSaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_RESERVADO_TO");
               GX_FocusControl = edtavTfsaldocontrato_reservado_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFSaldoContrato_Reservado_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5)));
            }
            else
            {
               AV49TFSaldoContrato_Reservado_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_reservado_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_EXECUTADO");
               GX_FocusControl = edtavTfsaldocontrato_executado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFSaldoContrato_Executado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5)));
            }
            else
            {
               AV52TFSaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_EXECUTADO_TO");
               GX_FocusControl = edtavTfsaldocontrato_executado_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFSaldoContrato_Executado_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5)));
            }
            else
            {
               AV53TFSaldoContrato_Executado_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_executado_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_SALDO");
               GX_FocusControl = edtavTfsaldocontrato_saldo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFSaldoContrato_Saldo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5)));
            }
            else
            {
               AV56TFSaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_SALDO_TO");
               GX_FocusControl = edtavTfsaldocontrato_saldo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFSaldoContrato_Saldo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5)));
            }
            else
            {
               AV57TFSaldoContrato_Saldo_To = context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_saldo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5)));
            }
            AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
            AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
            AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace", AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace);
            AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace);
            AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
            AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
            AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_19"), ",", "."));
            AV61GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV62GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_saldocontrato_vigenciainicio_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Caption");
            Ddo_saldocontrato_vigenciainicio_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Tooltip");
            Ddo_saldocontrato_vigenciainicio_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Cls");
            Ddo_saldocontrato_vigenciainicio_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_set");
            Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_set");
            Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Dropdownoptionstype");
            Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_vigenciainicio_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortasc"));
            Ddo_saldocontrato_vigenciainicio_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includesortdsc"));
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortedstatus");
            Ddo_saldocontrato_vigenciainicio_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includefilter"));
            Ddo_saldocontrato_vigenciainicio_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filtertype");
            Ddo_saldocontrato_vigenciainicio_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filterisrange"));
            Ddo_saldocontrato_vigenciainicio_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Includedatalist"));
            Ddo_saldocontrato_vigenciainicio_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Datalistfixedvalues");
            Ddo_saldocontrato_vigenciainicio_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_vigenciainicio_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortasc");
            Ddo_saldocontrato_vigenciainicio_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Sortdsc");
            Ddo_saldocontrato_vigenciainicio_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Loadingdata");
            Ddo_saldocontrato_vigenciainicio_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Cleanfilter");
            Ddo_saldocontrato_vigenciainicio_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterfrom");
            Ddo_saldocontrato_vigenciainicio_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Rangefilterto");
            Ddo_saldocontrato_vigenciainicio_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Noresultsfound");
            Ddo_saldocontrato_vigenciainicio_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Searchbuttontext");
            Ddo_saldocontrato_vigenciafim_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Caption");
            Ddo_saldocontrato_vigenciafim_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Tooltip");
            Ddo_saldocontrato_vigenciafim_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Cls");
            Ddo_saldocontrato_vigenciafim_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_set");
            Ddo_saldocontrato_vigenciafim_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_set");
            Ddo_saldocontrato_vigenciafim_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Dropdownoptionstype");
            Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Titlecontrolidtoreplace");
            Ddo_saldocontrato_vigenciafim_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortasc"));
            Ddo_saldocontrato_vigenciafim_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includesortdsc"));
            Ddo_saldocontrato_vigenciafim_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortedstatus");
            Ddo_saldocontrato_vigenciafim_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includefilter"));
            Ddo_saldocontrato_vigenciafim_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filtertype");
            Ddo_saldocontrato_vigenciafim_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filterisrange"));
            Ddo_saldocontrato_vigenciafim_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Includedatalist"));
            Ddo_saldocontrato_vigenciafim_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Datalistfixedvalues");
            Ddo_saldocontrato_vigenciafim_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_vigenciafim_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortasc");
            Ddo_saldocontrato_vigenciafim_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Sortdsc");
            Ddo_saldocontrato_vigenciafim_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Loadingdata");
            Ddo_saldocontrato_vigenciafim_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Cleanfilter");
            Ddo_saldocontrato_vigenciafim_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterfrom");
            Ddo_saldocontrato_vigenciafim_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Rangefilterto");
            Ddo_saldocontrato_vigenciafim_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Noresultsfound");
            Ddo_saldocontrato_vigenciafim_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Searchbuttontext");
            Ddo_saldocontrato_unidademedicao_nome_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Caption");
            Ddo_saldocontrato_unidademedicao_nome_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Tooltip");
            Ddo_saldocontrato_unidademedicao_nome_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Cls");
            Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filteredtext_set");
            Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Selectedvalue_set");
            Ddo_saldocontrato_unidademedicao_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Dropdownoptionstype");
            Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Titlecontrolidtoreplace");
            Ddo_saldocontrato_unidademedicao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includesortasc"));
            Ddo_saldocontrato_unidademedicao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includesortdsc"));
            Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortedstatus");
            Ddo_saldocontrato_unidademedicao_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includefilter"));
            Ddo_saldocontrato_unidademedicao_nome_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filtertype");
            Ddo_saldocontrato_unidademedicao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filterisrange"));
            Ddo_saldocontrato_unidademedicao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Includedatalist"));
            Ddo_saldocontrato_unidademedicao_nome_Datalisttype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalisttype");
            Ddo_saldocontrato_unidademedicao_nome_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistfixedvalues");
            Ddo_saldocontrato_unidademedicao_nome_Datalistproc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistproc");
            Ddo_saldocontrato_unidademedicao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_unidademedicao_nome_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortasc");
            Ddo_saldocontrato_unidademedicao_nome_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Sortdsc");
            Ddo_saldocontrato_unidademedicao_nome_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Loadingdata");
            Ddo_saldocontrato_unidademedicao_nome_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Cleanfilter");
            Ddo_saldocontrato_unidademedicao_nome_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Rangefilterfrom");
            Ddo_saldocontrato_unidademedicao_nome_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Rangefilterto");
            Ddo_saldocontrato_unidademedicao_nome_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Noresultsfound");
            Ddo_saldocontrato_unidademedicao_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Searchbuttontext");
            Ddo_saldocontrato_credito_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Caption");
            Ddo_saldocontrato_credito_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Tooltip");
            Ddo_saldocontrato_credito_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Cls");
            Ddo_saldocontrato_credito_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtext_set");
            Ddo_saldocontrato_credito_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtextto_set");
            Ddo_saldocontrato_credito_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Dropdownoptionstype");
            Ddo_saldocontrato_credito_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_credito_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includesortasc"));
            Ddo_saldocontrato_credito_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includesortdsc"));
            Ddo_saldocontrato_credito_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortedstatus");
            Ddo_saldocontrato_credito_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includefilter"));
            Ddo_saldocontrato_credito_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filtertype");
            Ddo_saldocontrato_credito_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filterisrange"));
            Ddo_saldocontrato_credito_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Includedatalist"));
            Ddo_saldocontrato_credito_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Datalistfixedvalues");
            Ddo_saldocontrato_credito_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_credito_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortasc");
            Ddo_saldocontrato_credito_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Sortdsc");
            Ddo_saldocontrato_credito_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Loadingdata");
            Ddo_saldocontrato_credito_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Cleanfilter");
            Ddo_saldocontrato_credito_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Rangefilterfrom");
            Ddo_saldocontrato_credito_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Rangefilterto");
            Ddo_saldocontrato_credito_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Noresultsfound");
            Ddo_saldocontrato_credito_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Searchbuttontext");
            Ddo_saldocontrato_reservado_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Caption");
            Ddo_saldocontrato_reservado_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Tooltip");
            Ddo_saldocontrato_reservado_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Cls");
            Ddo_saldocontrato_reservado_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtext_set");
            Ddo_saldocontrato_reservado_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_set");
            Ddo_saldocontrato_reservado_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Dropdownoptionstype");
            Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_reservado_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includesortasc"));
            Ddo_saldocontrato_reservado_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includesortdsc"));
            Ddo_saldocontrato_reservado_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortedstatus");
            Ddo_saldocontrato_reservado_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includefilter"));
            Ddo_saldocontrato_reservado_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filtertype");
            Ddo_saldocontrato_reservado_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filterisrange"));
            Ddo_saldocontrato_reservado_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Includedatalist"));
            Ddo_saldocontrato_reservado_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Datalistfixedvalues");
            Ddo_saldocontrato_reservado_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_reservado_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortasc");
            Ddo_saldocontrato_reservado_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Sortdsc");
            Ddo_saldocontrato_reservado_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Loadingdata");
            Ddo_saldocontrato_reservado_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Cleanfilter");
            Ddo_saldocontrato_reservado_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Rangefilterfrom");
            Ddo_saldocontrato_reservado_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Rangefilterto");
            Ddo_saldocontrato_reservado_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Noresultsfound");
            Ddo_saldocontrato_reservado_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Searchbuttontext");
            Ddo_saldocontrato_executado_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Caption");
            Ddo_saldocontrato_executado_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Tooltip");
            Ddo_saldocontrato_executado_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Cls");
            Ddo_saldocontrato_executado_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_set");
            Ddo_saldocontrato_executado_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_set");
            Ddo_saldocontrato_executado_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Dropdownoptionstype");
            Ddo_saldocontrato_executado_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_executado_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includesortasc"));
            Ddo_saldocontrato_executado_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includesortdsc"));
            Ddo_saldocontrato_executado_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortedstatus");
            Ddo_saldocontrato_executado_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includefilter"));
            Ddo_saldocontrato_executado_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filtertype");
            Ddo_saldocontrato_executado_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filterisrange"));
            Ddo_saldocontrato_executado_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Includedatalist"));
            Ddo_saldocontrato_executado_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Datalistfixedvalues");
            Ddo_saldocontrato_executado_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_executado_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortasc");
            Ddo_saldocontrato_executado_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Sortdsc");
            Ddo_saldocontrato_executado_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Loadingdata");
            Ddo_saldocontrato_executado_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Cleanfilter");
            Ddo_saldocontrato_executado_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Rangefilterfrom");
            Ddo_saldocontrato_executado_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Rangefilterto");
            Ddo_saldocontrato_executado_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Noresultsfound");
            Ddo_saldocontrato_executado_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Searchbuttontext");
            Ddo_saldocontrato_saldo_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Caption");
            Ddo_saldocontrato_saldo_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Tooltip");
            Ddo_saldocontrato_saldo_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Cls");
            Ddo_saldocontrato_saldo_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtext_set");
            Ddo_saldocontrato_saldo_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtextto_set");
            Ddo_saldocontrato_saldo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Dropdownoptionstype");
            Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_saldo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Includesortasc"));
            Ddo_saldocontrato_saldo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Includesortdsc"));
            Ddo_saldocontrato_saldo_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortedstatus");
            Ddo_saldocontrato_saldo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Includefilter"));
            Ddo_saldocontrato_saldo_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filtertype");
            Ddo_saldocontrato_saldo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filterisrange"));
            Ddo_saldocontrato_saldo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Includedatalist"));
            Ddo_saldocontrato_saldo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Datalistfixedvalues");
            Ddo_saldocontrato_saldo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_saldo_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortasc");
            Ddo_saldocontrato_saldo_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Sortdsc");
            Ddo_saldocontrato_saldo_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Loadingdata");
            Ddo_saldocontrato_saldo_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Cleanfilter");
            Ddo_saldocontrato_saldo_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Rangefilterfrom");
            Ddo_saldocontrato_saldo_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Rangefilterto");
            Ddo_saldocontrato_saldo_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Noresultsfound");
            Ddo_saldocontrato_saldo_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_saldocontrato_vigenciainicio_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Activeeventkey");
            Ddo_saldocontrato_vigenciainicio_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtext_get");
            Ddo_saldocontrato_vigenciainicio_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO_Filteredtextto_get");
            Ddo_saldocontrato_vigenciafim_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Activeeventkey");
            Ddo_saldocontrato_vigenciafim_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtext_get");
            Ddo_saldocontrato_vigenciafim_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM_Filteredtextto_get");
            Ddo_saldocontrato_unidademedicao_nome_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Activeeventkey");
            Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Filteredtext_get");
            Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME_Selectedvalue_get");
            Ddo_saldocontrato_credito_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Activeeventkey");
            Ddo_saldocontrato_credito_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtext_get");
            Ddo_saldocontrato_credito_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CREDITO_Filteredtextto_get");
            Ddo_saldocontrato_reservado_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Activeeventkey");
            Ddo_saldocontrato_reservado_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtext_get");
            Ddo_saldocontrato_reservado_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_RESERVADO_Filteredtextto_get");
            Ddo_saldocontrato_executado_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Activeeventkey");
            Ddo_saldocontrato_executado_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtext_get");
            Ddo_saldocontrato_executado_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_EXECUTADO_Filteredtextto_get");
            Ddo_saldocontrato_saldo_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Activeeventkey");
            Ddo_saldocontrato_saldo_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtext_get");
            Ddo_saldocontrato_saldo_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_SALDO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO"), 0) != AV28TFSaldoContrato_VigenciaInicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAINICIO_TO"), 0) != AV29TFSaldoContrato_VigenciaInicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM"), 0) != AV34TFSaldoContrato_VigenciaFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_VIGENCIAFIM_TO"), 0) != AV35TFSaldoContrato_VigenciaFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME"), AV40TFSaldoContrato_UnidadeMedicao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL"), AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO"), ",", ".") != AV44TFSaldoContrato_Credito )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CREDITO_TO"), ",", ".") != AV45TFSaldoContrato_Credito_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO"), ",", ".") != AV48TFSaldoContrato_Reservado )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_RESERVADO_TO"), ",", ".") != AV49TFSaldoContrato_Reservado_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO"), ",", ".") != AV52TFSaldoContrato_Executado )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_EXECUTADO_TO"), ",", ".") != AV53TFSaldoContrato_Executado_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_SALDO"), ",", ".") != AV56TFSaldoContrato_Saldo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_SALDO_TO"), ",", ".") != AV57TFSaldoContrato_Saldo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E20MI2 */
         E20MI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20MI2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfsaldocontrato_vigenciainicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_vigenciainicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciainicio_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciainicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_vigenciainicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciainicio_to_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_vigenciafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciafim_Visible), 5, 0)));
         edtavTfsaldocontrato_vigenciafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_vigenciafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_vigenciafim_to_Visible), 5, 0)));
         edtavTfsaldocontrato_unidademedicao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_unidademedicao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_unidademedicao_nome_Visible), 5, 0)));
         edtavTfsaldocontrato_unidademedicao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_unidademedicao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_unidademedicao_nome_sel_Visible), 5, 0)));
         edtavTfsaldocontrato_credito_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_credito_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_credito_Visible), 5, 0)));
         edtavTfsaldocontrato_credito_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_credito_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_credito_to_Visible), 5, 0)));
         edtavTfsaldocontrato_reservado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_reservado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_reservado_Visible), 5, 0)));
         edtavTfsaldocontrato_reservado_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_reservado_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_reservado_to_Visible), 5, 0)));
         edtavTfsaldocontrato_executado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_executado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_executado_Visible), 5, 0)));
         edtavTfsaldocontrato_executado_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_executado_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_executado_to_Visible), 5, 0)));
         edtavTfsaldocontrato_saldo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_saldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_saldo_Visible), 5, 0)));
         edtavTfsaldocontrato_saldo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_saldo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_saldo_to_Visible), 5, 0)));
         Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_VigenciaInicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace);
         AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace", AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace);
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_VigenciaFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace);
         AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace", AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace);
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_UnidadeMedicao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace);
         AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace = Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace", AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace);
         edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_credito_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Credito";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_credito_Titlecontrolidtoreplace);
         AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace = Ddo_saldocontrato_credito_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace", AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace);
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Reservado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_reservado_Titlecontrolidtoreplace);
         AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace = Ddo_saldocontrato_reservado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace", AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace);
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_executado_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Executado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_executado_Titlecontrolidtoreplace);
         AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = Ddo_saldocontrato_executado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace", AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace);
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Saldo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_saldo_Titlecontrolidtoreplace);
         AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace = Ddo_saldocontrato_saldo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace", AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace);
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV59DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV59DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         bttBtninicializarsaldo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtninicializarsaldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtninicializarsaldo_Visible), 5, 0)));
         if ( A40000GXC1 <= 0 )
         {
            bttBtninicializarsaldo_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtninicializarsaldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtninicializarsaldo_Visible), 5, 0)));
         }
      }

      protected void E21MI2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV27SaldoContrato_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33SaldoContrato_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43SaldoContrato_CreditoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47SaldoContrato_ReservadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51SaldoContrato_ExecutadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55SaldoContrato_SaldoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSaldoContrato_VigenciaInicio_Titleformat = 2;
         edtSaldoContrato_VigenciaInicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicio", AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_VigenciaInicio_Internalname, "Title", edtSaldoContrato_VigenciaInicio_Title);
         edtSaldoContrato_VigenciaFim_Titleformat = 2;
         edtSaldoContrato_VigenciaFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_VigenciaFim_Internalname, "Title", edtSaldoContrato_VigenciaFim_Title);
         edtSaldoContrato_UnidadeMedicao_Nome_Titleformat = 2;
         edtSaldoContrato_UnidadeMedicao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade Medida", AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_UnidadeMedicao_Nome_Internalname, "Title", edtSaldoContrato_UnidadeMedicao_Nome_Title);
         edtSaldoContrato_Credito_Titleformat = 2;
         edtSaldoContrato_Credito_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cr�dito", AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_Credito_Internalname, "Title", edtSaldoContrato_Credito_Title);
         edtSaldoContrato_Reservado_Titleformat = 2;
         edtSaldoContrato_Reservado_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Reservado", AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_Reservado_Internalname, "Title", edtSaldoContrato_Reservado_Title);
         edtSaldoContrato_Executado_Titleformat = 2;
         edtSaldoContrato_Executado_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Executado", AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_Executado_Internalname, "Title", edtSaldoContrato_Executado_Title);
         edtSaldoContrato_Saldo_Titleformat = 2;
         edtSaldoContrato_Saldo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo", AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_Saldo_Internalname, "Title", edtSaldoContrato_Saldo_Title);
         AV61GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GridCurrentPage), 10, 0)));
         AV62GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridPageCount), 10, 0)));
         edtavDebitosaldoreserva_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDebitosaldoreserva_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDebitosaldoreserva_Visible), 5, 0)));
         edtavReservarsaldo_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavReservarsaldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReservarsaldo_Visible), 5, 0)));
         edtavDebitosaldo_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDebitosaldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDebitosaldo_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV27SaldoContrato_VigenciaInicioTitleFilterData", AV27SaldoContrato_VigenciaInicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV33SaldoContrato_VigenciaFimTitleFilterData", AV33SaldoContrato_VigenciaFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData", AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV43SaldoContrato_CreditoTitleFilterData", AV43SaldoContrato_CreditoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47SaldoContrato_ReservadoTitleFilterData", AV47SaldoContrato_ReservadoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV51SaldoContrato_ExecutadoTitleFilterData", AV51SaldoContrato_ExecutadoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55SaldoContrato_SaldoTitleFilterData", AV55SaldoContrato_SaldoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11MI2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV60PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV60PageToGo) ;
         }
      }

      protected void E12MI2( )
      {
         /* Ddo_saldocontrato_vigenciainicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciainicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFSaldoContrato_VigenciaInicio = context.localUtil.CToD( Ddo_saldocontrato_vigenciainicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
            AV29TFSaldoContrato_VigenciaInicio_To = context.localUtil.CToD( Ddo_saldocontrato_vigenciainicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13MI2( )
      {
         /* Ddo_saldocontrato_vigenciafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciafim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_vigenciafim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_vigenciafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFSaldoContrato_VigenciaFim = context.localUtil.CToD( Ddo_saldocontrato_vigenciafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
            AV35TFSaldoContrato_VigenciaFim_To = context.localUtil.CToD( Ddo_saldocontrato_vigenciafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14MI2( )
      {
         /* Ddo_saldocontrato_unidademedicao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFSaldoContrato_UnidadeMedicao_Nome = Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
            AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel = Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel", AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15MI2( )
      {
         /* Ddo_saldocontrato_credito_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_credito_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_credito_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_credito_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFSaldoContrato_Credito = NumberUtil.Val( Ddo_saldocontrato_credito_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5)));
            AV45TFSaldoContrato_Credito_To = NumberUtil.Val( Ddo_saldocontrato_credito_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16MI2( )
      {
         /* Ddo_saldocontrato_reservado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_reservado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_reservado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_reservado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFSaldoContrato_Reservado = NumberUtil.Val( Ddo_saldocontrato_reservado_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5)));
            AV49TFSaldoContrato_Reservado_To = NumberUtil.Val( Ddo_saldocontrato_reservado_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E17MI2( )
      {
         /* Ddo_saldocontrato_executado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_executado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_executado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_executado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFSaldoContrato_Executado = NumberUtil.Val( Ddo_saldocontrato_executado_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5)));
            AV53TFSaldoContrato_Executado_To = NumberUtil.Val( Ddo_saldocontrato_executado_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18MI2( )
      {
         /* Ddo_saldocontrato_saldo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_saldo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_saldo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_saldo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFSaldoContrato_Saldo = NumberUtil.Val( Ddo_saldocontrato_saldo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5)));
            AV57TFSaldoContrato_Saldo_To = NumberUtil.Val( Ddo_saldocontrato_saldo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E22MI2( )
      {
         /* Grid_Load Routine */
         AV17imgReservar = context.GetImagePath( "31aaf43c-cf6e-4175-8456-c36ead79f7ec", "", context.GetTheme( ));
         AV65Imgreservar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "31aaf43c-cf6e-4175-8456-c36ead79f7ec", "", context.GetTheme( )));
         AV19imgDebitoSaldoReserva = context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( ));
         AV66Imgdebitosaldoreserva_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( )));
         AV18imgDebitoSaldo = context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( ));
         AV67Imgdebitosaldo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( )));
         AV24DebitoSaldoReserva = context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDebitosaldoreserva_Internalname, AV24DebitoSaldoReserva);
         AV68Debitosaldoreserva_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( )));
         edtavDebitosaldoreserva_Tooltiptext = "Debitar Saldo com Reserva";
         AV25ReservarSaldo = context.GetImagePath( "cb35c955-00c3-4bc3-8d68-0fb15a35abe1", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavReservarsaldo_Internalname, AV25ReservarSaldo);
         AV69Reservarsaldo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "cb35c955-00c3-4bc3-8d68-0fb15a35abe1", "", context.GetTheme( )));
         edtavReservarsaldo_Tooltiptext = "Reservar Saldo";
         AV26DebitoSaldo = context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDebitosaldo_Internalname, AV26DebitoSaldo);
         AV70Debitosaldo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2e5d06bc-c841-46b0-b183-ab5b3c360219", "", context.GetTheme( )));
         edtavDebitosaldo_Tooltiptext = "Debitar Saldo sem Reserva";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 19;
         }
         sendrow_192( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(19, GridRow);
         }
      }

      protected void E19MI2( )
      {
         /* 'DoInicializarSaldo' Routine */
         if ( AV16IsErro )
         {
            GX_msglist.addItem("Contrato n�o possui vig�ncia v�lida para gera��o de Saldo, atualize as datas de vig�ncia do contrato ou do termo aditivo mais recente!");
         }
         else
         {
            bttBtninicializarsaldo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtninicializarsaldo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtninicializarsaldo_Visible), 5, 0)));
            context.DoAjaxRefreshCmp(sPrefix);
         }
         new prc_saldocontratoinicializar(context ).execute(  AV7Contrato_Codigo, out  AV16IsErro) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16IsErro", AV16IsErro);
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_saldocontrato_vigenciainicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
         Ddo_saldocontrato_vigenciafim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
         Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_nome_Sortedstatus);
         Ddo_saldocontrato_credito_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
         Ddo_saldocontrato_reservado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
         Ddo_saldocontrato_executado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
         Ddo_saldocontrato_saldo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_saldocontrato_vigenciainicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciainicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_saldocontrato_vigenciafim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "SortedStatus", Ddo_saldocontrato_vigenciafim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_saldocontrato_credito_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "SortedStatus", Ddo_saldocontrato_credito_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_saldocontrato_reservado_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "SortedStatus", Ddo_saldocontrato_reservado_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_saldocontrato_executado_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "SortedStatus", Ddo_saldocontrato_executado_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_saldocontrato_saldo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "SortedStatus", Ddo_saldocontrato_saldo_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( A40000GXC1 <= 0 ) ) )
         {
            bttBtninicializarsaldo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtninicializarsaldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtninicializarsaldo_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV71Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV71Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV71Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV72GXV1 = 1;
         while ( AV72GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV72GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAINICIO") == 0 )
            {
               AV28TFSaldoContrato_VigenciaInicio = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSaldoContrato_VigenciaInicio", context.localUtil.Format(AV28TFSaldoContrato_VigenciaInicio, "99/99/99"));
               AV29TFSaldoContrato_VigenciaInicio_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFSaldoContrato_VigenciaInicio_To", context.localUtil.Format(AV29TFSaldoContrato_VigenciaInicio_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV28TFSaldoContrato_VigenciaInicio) )
               {
                  Ddo_saldocontrato_vigenciainicio_Filteredtext_set = context.localUtil.DToC( AV28TFSaldoContrato_VigenciaInicio, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciainicio_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV29TFSaldoContrato_VigenciaInicio_To) )
               {
                  Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = context.localUtil.DToC( AV29TFSaldoContrato_VigenciaInicio_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciainicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAFIM") == 0 )
            {
               AV34TFSaldoContrato_VigenciaFim = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFSaldoContrato_VigenciaFim", context.localUtil.Format(AV34TFSaldoContrato_VigenciaFim, "99/99/99"));
               AV35TFSaldoContrato_VigenciaFim_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSaldoContrato_VigenciaFim_To", context.localUtil.Format(AV35TFSaldoContrato_VigenciaFim_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV34TFSaldoContrato_VigenciaFim) )
               {
                  Ddo_saldocontrato_vigenciafim_Filteredtext_set = context.localUtil.DToC( AV34TFSaldoContrato_VigenciaFim, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredText_set", Ddo_saldocontrato_vigenciafim_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV35TFSaldoContrato_VigenciaFim_To) )
               {
                  Ddo_saldocontrato_vigenciafim_Filteredtextto_set = context.localUtil.DToC( AV35TFSaldoContrato_VigenciaFim_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_vigenciafim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME") == 0 )
            {
               AV40TFSaldoContrato_UnidadeMedicao_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSaldoContrato_UnidadeMedicao_Nome", AV40TFSaldoContrato_UnidadeMedicao_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome)) )
               {
                  Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set = AV40TFSaldoContrato_UnidadeMedicao_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "FilteredText_set", Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL") == 0 )
            {
               AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel", AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) )
               {
                  Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set = AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_nome_Internalname, "SelectedValue_set", Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CREDITO") == 0 )
            {
               AV44TFSaldoContrato_Credito = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFSaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5)));
               AV45TFSaldoContrato_Credito_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFSaldoContrato_Credito_To", StringUtil.LTrim( StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV44TFSaldoContrato_Credito) )
               {
                  Ddo_saldocontrato_credito_Filteredtext_set = StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "FilteredText_set", Ddo_saldocontrato_credito_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV45TFSaldoContrato_Credito_To) )
               {
                  Ddo_saldocontrato_credito_Filteredtextto_set = StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_credito_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_credito_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_RESERVADO") == 0 )
            {
               AV48TFSaldoContrato_Reservado = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5)));
               AV49TFSaldoContrato_Reservado_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFSaldoContrato_Reservado_To", StringUtil.LTrim( StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV48TFSaldoContrato_Reservado) )
               {
                  Ddo_saldocontrato_reservado_Filteredtext_set = StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "FilteredText_set", Ddo_saldocontrato_reservado_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV49TFSaldoContrato_Reservado_To) )
               {
                  Ddo_saldocontrato_reservado_Filteredtextto_set = StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_reservado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_reservado_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_EXECUTADO") == 0 )
            {
               AV52TFSaldoContrato_Executado = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFSaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5)));
               AV53TFSaldoContrato_Executado_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFSaldoContrato_Executado_To", StringUtil.LTrim( StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV52TFSaldoContrato_Executado) )
               {
                  Ddo_saldocontrato_executado_Filteredtext_set = StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "FilteredText_set", Ddo_saldocontrato_executado_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV53TFSaldoContrato_Executado_To) )
               {
                  Ddo_saldocontrato_executado_Filteredtextto_set = StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_executado_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_executado_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_SALDO") == 0 )
            {
               AV56TFSaldoContrato_Saldo = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFSaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5)));
               AV57TFSaldoContrato_Saldo_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFSaldoContrato_Saldo_To", StringUtil.LTrim( StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV56TFSaldoContrato_Saldo) )
               {
                  Ddo_saldocontrato_saldo_Filteredtext_set = StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "FilteredText_set", Ddo_saldocontrato_saldo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV57TFSaldoContrato_Saldo_To) )
               {
                  Ddo_saldocontrato_saldo_Filteredtextto_set = StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_saldo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_saldo_Filteredtextto_set);
               }
            }
            AV72GXV1 = (int)(AV72GXV1+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV71Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV28TFSaldoContrato_VigenciaInicio) && (DateTime.MinValue==AV29TFSaldoContrato_VigenciaInicio_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_VIGENCIAINICIO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV28TFSaldoContrato_VigenciaInicio, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV29TFSaldoContrato_VigenciaInicio_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV34TFSaldoContrato_VigenciaFim) && (DateTime.MinValue==AV35TFSaldoContrato_VigenciaFim_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_VIGENCIAFIM";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV34TFSaldoContrato_VigenciaFim, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV35TFSaldoContrato_VigenciaFim_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFSaldoContrato_UnidadeMedicao_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV44TFSaldoContrato_Credito) && (Convert.ToDecimal(0)==AV45TFSaldoContrato_Credito_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_CREDITO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV44TFSaldoContrato_Credito, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV45TFSaldoContrato_Credito_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV48TFSaldoContrato_Reservado) && (Convert.ToDecimal(0)==AV49TFSaldoContrato_Reservado_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_RESERVADO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV48TFSaldoContrato_Reservado, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV49TFSaldoContrato_Reservado_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV52TFSaldoContrato_Executado) && (Convert.ToDecimal(0)==AV53TFSaldoContrato_Executado_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_EXECUTADO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV52TFSaldoContrato_Executado, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV53TFSaldoContrato_Executado_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV56TFSaldoContrato_Saldo) && (Convert.ToDecimal(0)==AV57TFSaldoContrato_Saldo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_SALDO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV56TFSaldoContrato_Saldo, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV57TFSaldoContrato_Saldo_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Contrato_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV71Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV71Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "SaldoContrato";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E23MI2( )
      {
         /* Reservarsaldo_Click Routine */
         context.wjLoc = formatLink("wp_saldocontratoreservar.aspx") + "?" + UrlEncode("" +A1561SaldoContrato_Codigo) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV20NotaEmpenho_Codigo) + "," + UrlEncode("" +AV21Contagem_Codigo) + "," + UrlEncode(StringUtil.Str(AV22SaldoContrato_Credito,18,5)) + "," + UrlEncode(StringUtil.RTrim("RSV"));
         context.wjLocDisableFrm = 1;
      }

      protected void E24MI2( )
      {
         /* Debitosaldoreserva_Click Routine */
         context.wjLoc = formatLink("wp_saldocontratoreservar.aspx") + "?" + UrlEncode("" +A1561SaldoContrato_Codigo) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV20NotaEmpenho_Codigo) + "," + UrlEncode("" +AV21Contagem_Codigo) + "," + UrlEncode(StringUtil.Str(AV22SaldoContrato_Credito,18,5)) + "," + UrlEncode(StringUtil.RTrim("DSR"));
         context.wjLocDisableFrm = 1;
      }

      protected void E25MI2( )
      {
         /* Debitosaldo_Click Routine */
         context.wjLoc = formatLink("wp_saldocontratoreservar.aspx") + "?" + UrlEncode("" +A1561SaldoContrato_Codigo) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV20NotaEmpenho_Codigo) + "," + UrlEncode("" +AV21Contagem_Codigo) + "," + UrlEncode(StringUtil.Str(AV22SaldoContrato_Credito,18,5)) + "," + UrlEncode(StringUtil.RTrim("SLD"));
         context.wjLocDisableFrm = 1;
      }

      protected void wb_table1_2_MI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_MI2( true) ;
         }
         else
         {
            wb_table2_8_MI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtninicializarsaldo_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", "Inicializar Saldo", bttBtninicializarsaldo_Jsonclick, 5, "Inicializar Saldo", "", StyleString, ClassString, bttBtninicializarsaldo_Visible, bttBtninicializarsaldo_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINICIALIZARSALDO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoContratoSaldoContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_MI2( true) ;
         }
         else
         {
            wb_table3_16_MI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_MI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MI2e( true) ;
         }
         else
         {
            wb_table1_2_MI2e( false) ;
         }
      }

      protected void wb_table3_16_MI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Saldo Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_VigenciaInicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_VigenciaInicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_VigenciaInicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_VigenciaFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_VigenciaFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_VigenciaFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_UnidadeMedicao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_UnidadeMedicao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_UnidadeMedicao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Credito_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Credito_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Credito_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Reservado_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Reservado_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Reservado_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDebitosaldoreserva_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Executado_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Executado_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Executado_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Saldo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Saldo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Saldo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavReservarsaldo_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDebitosaldo_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_VigenciaInicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_VigenciaInicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_VigenciaFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_VigenciaFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1784SaldoContrato_UnidadeMedicao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_UnidadeMedicao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_UnidadeMedicao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Credito_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Credito_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Reservado_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Reservado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV24DebitoSaldoReserva));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDebitosaldoreserva_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDebitosaldoreserva_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Executado_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Executado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Saldo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Saldo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25ReservarSaldo));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavReservarsaldo_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavReservarsaldo_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26DebitoSaldo));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDebitosaldo_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDebitosaldo_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_MI2e( true) ;
         }
         else
         {
            wb_table3_16_MI2e( false) ;
         }
      }

      protected void wb_table2_8_MI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MI2e( true) ;
         }
         else
         {
            wb_table2_8_MI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMI2( ) ;
         WSMI2( ) ;
         WEMI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAMI2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratocontratosaldocontratowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAMI2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contrato_Codigo != wcpOAV7Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contrato_Codigo = AV7Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contrato_Codigo = cgiGet( sPrefix+"AV7Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contrato_Codigo) > 0 )
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         else
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAMI2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSMI2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSMI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEMI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204291573257");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratocontratosaldocontratowc.js", "?20204291573257");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO_"+sGXsfl_19_idx;
         edtSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO_"+sGXsfl_19_idx;
         edtSaldoContrato_VigenciaInicio_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAINICIO_"+sGXsfl_19_idx;
         edtSaldoContrato_VigenciaFim_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAFIM_"+sGXsfl_19_idx;
         edtSaldoContrato_UnidadeMedicao_Nome_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_NOME_"+sGXsfl_19_idx;
         edtSaldoContrato_Credito_Internalname = sPrefix+"SALDOCONTRATO_CREDITO_"+sGXsfl_19_idx;
         edtSaldoContrato_Reservado_Internalname = sPrefix+"SALDOCONTRATO_RESERVADO_"+sGXsfl_19_idx;
         edtavDebitosaldoreserva_Internalname = sPrefix+"vDEBITOSALDORESERVA_"+sGXsfl_19_idx;
         edtSaldoContrato_Executado_Internalname = sPrefix+"SALDOCONTRATO_EXECUTADO_"+sGXsfl_19_idx;
         edtSaldoContrato_Saldo_Internalname = sPrefix+"SALDOCONTRATO_SALDO_"+sGXsfl_19_idx;
         edtavReservarsaldo_Internalname = sPrefix+"vRESERVARSALDO_"+sGXsfl_19_idx;
         edtavDebitosaldo_Internalname = sPrefix+"vDEBITOSALDO_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_VigenciaInicio_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAINICIO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_VigenciaFim_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAFIM_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_UnidadeMedicao_Nome_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_NOME_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_Credito_Internalname = sPrefix+"SALDOCONTRATO_CREDITO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_Reservado_Internalname = sPrefix+"SALDOCONTRATO_RESERVADO_"+sGXsfl_19_fel_idx;
         edtavDebitosaldoreserva_Internalname = sPrefix+"vDEBITOSALDORESERVA_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_Executado_Internalname = sPrefix+"SALDOCONTRATO_EXECUTADO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_Saldo_Internalname = sPrefix+"SALDOCONTRATO_SALDO_"+sGXsfl_19_fel_idx;
         edtavReservarsaldo_Internalname = sPrefix+"vRESERVARSALDO_"+sGXsfl_19_fel_idx;
         edtavDebitosaldo_Internalname = sPrefix+"vDEBITOSALDO_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBMI0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_19_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_VigenciaInicio_Internalname,context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"),context.localUtil.Format( A1571SaldoContrato_VigenciaInicio, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_VigenciaInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_VigenciaFim_Internalname,context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"),context.localUtil.Format( A1572SaldoContrato_VigenciaFim, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_VigenciaFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_UnidadeMedicao_Nome_Internalname,StringUtil.RTrim( A1784SaldoContrato_UnidadeMedicao_Nome),StringUtil.RTrim( context.localUtil.Format( A1784SaldoContrato_UnidadeMedicao_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_UnidadeMedicao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Credito_Internalname,StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ",", "")),context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Credito_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Reservado_Internalname,StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ",", "")),context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Reservado_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDebitosaldoreserva_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDebitosaldoreserva_Enabled!=0)&&(edtavDebitosaldoreserva_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV24DebitoSaldoReserva_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV24DebitoSaldoReserva))&&String.IsNullOrEmpty(StringUtil.RTrim( AV68Debitosaldoreserva_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV24DebitoSaldoReserva)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDebitosaldoreserva_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV24DebitoSaldoReserva)) ? AV68Debitosaldoreserva_GXI : context.PathToRelativeUrl( AV24DebitoSaldoReserva)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDebitosaldoreserva_Visible,(short)1,(String)"",(String)edtavDebitosaldoreserva_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDebitosaldoreserva_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDEBITOSALDORESERVA.CLICK."+sGXsfl_19_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV24DebitoSaldoReserva_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Executado_Internalname,StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ",", "")),context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Executado_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Saldo_Internalname,StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ",", "")),context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Saldo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavReservarsaldo_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavReservarsaldo_Enabled!=0)&&(edtavReservarsaldo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 30,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV25ReservarSaldo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25ReservarSaldo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV69Reservarsaldo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25ReservarSaldo)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavReservarsaldo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25ReservarSaldo)) ? AV69Reservarsaldo_GXI : context.PathToRelativeUrl( AV25ReservarSaldo)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavReservarsaldo_Visible,(short)1,(String)"",(String)edtavReservarsaldo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavReservarsaldo_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVRESERVARSALDO.CLICK."+sGXsfl_19_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV25ReservarSaldo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDebitosaldo_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDebitosaldo_Enabled!=0)&&(edtavDebitosaldo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 31,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV26DebitoSaldo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26DebitoSaldo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Debitosaldo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26DebitoSaldo)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDebitosaldo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26DebitoSaldo)) ? AV70Debitosaldo_GXI : context.PathToRelativeUrl( AV26DebitoSaldo)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDebitosaldo_Visible,(short)1,(String)"",(String)edtavDebitosaldo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDebitosaldo_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDEBITOSALDO.CLICK."+sGXsfl_19_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV26DebitoSaldo_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_VIGENCIAINICIO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1571SaldoContrato_VigenciaInicio));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_VIGENCIAFIM"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1572SaldoContrato_VigenciaFim));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_CREDITO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_RESERVADO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_EXECUTADO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_SALDO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         /* End function sendrow_192 */
      }

      protected void init_default_properties( )
      {
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         bttBtninicializarsaldo_Internalname = sPrefix+"BTNINICIALIZARSALDO";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         edtSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO";
         edtSaldoContrato_VigenciaInicio_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAINICIO";
         edtSaldoContrato_VigenciaFim_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAFIM";
         edtSaldoContrato_UnidadeMedicao_Nome_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_NOME";
         edtSaldoContrato_Credito_Internalname = sPrefix+"SALDOCONTRATO_CREDITO";
         edtSaldoContrato_Reservado_Internalname = sPrefix+"SALDOCONTRATO_RESERVADO";
         edtavDebitosaldoreserva_Internalname = sPrefix+"vDEBITOSALDORESERVA";
         edtSaldoContrato_Executado_Internalname = sPrefix+"SALDOCONTRATO_EXECUTADO";
         edtSaldoContrato_Saldo_Internalname = sPrefix+"SALDOCONTRATO_SALDO";
         edtavReservarsaldo_Internalname = sPrefix+"vRESERVARSALDO";
         edtavDebitosaldo_Internalname = sPrefix+"vDEBITOSALDO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfsaldocontrato_vigenciainicio_Internalname = sPrefix+"vTFSALDOCONTRATO_VIGENCIAINICIO";
         edtavTfsaldocontrato_vigenciainicio_to_Internalname = sPrefix+"vTFSALDOCONTRATO_VIGENCIAINICIO_TO";
         edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE";
         edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO";
         divDdo_saldocontrato_vigenciainicioauxdates_Internalname = sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATES";
         edtavTfsaldocontrato_vigenciafim_Internalname = sPrefix+"vTFSALDOCONTRATO_VIGENCIAFIM";
         edtavTfsaldocontrato_vigenciafim_to_Internalname = sPrefix+"vTFSALDOCONTRATO_VIGENCIAFIM_TO";
         edtavDdo_saldocontrato_vigenciafimauxdate_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE";
         edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO";
         divDdo_saldocontrato_vigenciafimauxdates_Internalname = sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIMAUXDATES";
         edtavTfsaldocontrato_unidademedicao_nome_Internalname = sPrefix+"vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME";
         edtavTfsaldocontrato_unidademedicao_nome_sel_Internalname = sPrefix+"vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL";
         edtavTfsaldocontrato_credito_Internalname = sPrefix+"vTFSALDOCONTRATO_CREDITO";
         edtavTfsaldocontrato_credito_to_Internalname = sPrefix+"vTFSALDOCONTRATO_CREDITO_TO";
         edtavTfsaldocontrato_reservado_Internalname = sPrefix+"vTFSALDOCONTRATO_RESERVADO";
         edtavTfsaldocontrato_reservado_to_Internalname = sPrefix+"vTFSALDOCONTRATO_RESERVADO_TO";
         edtavTfsaldocontrato_executado_Internalname = sPrefix+"vTFSALDOCONTRATO_EXECUTADO";
         edtavTfsaldocontrato_executado_to_Internalname = sPrefix+"vTFSALDOCONTRATO_EXECUTADO_TO";
         edtavTfsaldocontrato_saldo_Internalname = sPrefix+"vTFSALDOCONTRATO_SALDO";
         edtavTfsaldocontrato_saldo_to_Internalname = sPrefix+"vTFSALDOCONTRATO_SALDO_TO";
         Ddo_saldocontrato_vigenciainicio_Internalname = sPrefix+"DDO_SALDOCONTRATO_VIGENCIAINICIO";
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_vigenciafim_Internalname = sPrefix+"DDO_SALDOCONTRATO_VIGENCIAFIM";
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_unidademedicao_nome_Internalname = sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME";
         edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_credito_Internalname = sPrefix+"DDO_SALDOCONTRATO_CREDITO";
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_reservado_Internalname = sPrefix+"DDO_SALDOCONTRATO_RESERVADO";
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_executado_Internalname = sPrefix+"DDO_SALDOCONTRATO_EXECUTADO";
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_saldo_Internalname = sPrefix+"DDO_SALDOCONTRATO_SALDO";
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavDebitosaldo_Jsonclick = "";
         edtavDebitosaldo_Enabled = 1;
         edtavReservarsaldo_Jsonclick = "";
         edtavReservarsaldo_Enabled = 1;
         edtSaldoContrato_Saldo_Jsonclick = "";
         edtSaldoContrato_Executado_Jsonclick = "";
         edtavDebitosaldoreserva_Jsonclick = "";
         edtavDebitosaldoreserva_Enabled = 1;
         edtSaldoContrato_Reservado_Jsonclick = "";
         edtSaldoContrato_Credito_Jsonclick = "";
         edtSaldoContrato_UnidadeMedicao_Nome_Jsonclick = "";
         edtSaldoContrato_VigenciaFim_Jsonclick = "";
         edtSaldoContrato_VigenciaInicio_Jsonclick = "";
         edtSaldoContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDebitosaldo_Tooltiptext = "Debitar Saldo sem Reserva";
         edtavReservarsaldo_Tooltiptext = "Reservar Saldo";
         edtavDebitosaldoreserva_Tooltiptext = "Debitar Saldo com Reserva";
         edtSaldoContrato_Saldo_Titleformat = 0;
         edtSaldoContrato_Executado_Titleformat = 0;
         edtSaldoContrato_Reservado_Titleformat = 0;
         edtSaldoContrato_Credito_Titleformat = 0;
         edtSaldoContrato_UnidadeMedicao_Nome_Titleformat = 0;
         edtSaldoContrato_VigenciaFim_Titleformat = 0;
         edtSaldoContrato_VigenciaInicio_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         bttBtninicializarsaldo_Enabled = 1;
         bttBtninicializarsaldo_Visible = 1;
         edtavDebitosaldo_Visible = -1;
         edtavReservarsaldo_Visible = -1;
         edtavDebitosaldoreserva_Visible = -1;
         edtSaldoContrato_Saldo_Title = "Saldo";
         edtSaldoContrato_Executado_Title = "Executado";
         edtSaldoContrato_Reservado_Title = "Reservado";
         edtSaldoContrato_Credito_Title = "Cr�dito";
         edtSaldoContrato_UnidadeMedicao_Nome_Title = "Unidade Medida";
         edtSaldoContrato_VigenciaFim_Title = "Fim";
         edtSaldoContrato_VigenciaInicio_Title = "Inicio";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible = 1;
         edtavTfsaldocontrato_saldo_to_Jsonclick = "";
         edtavTfsaldocontrato_saldo_to_Visible = 1;
         edtavTfsaldocontrato_saldo_Jsonclick = "";
         edtavTfsaldocontrato_saldo_Visible = 1;
         edtavTfsaldocontrato_executado_to_Jsonclick = "";
         edtavTfsaldocontrato_executado_to_Visible = 1;
         edtavTfsaldocontrato_executado_Jsonclick = "";
         edtavTfsaldocontrato_executado_Visible = 1;
         edtavTfsaldocontrato_reservado_to_Jsonclick = "";
         edtavTfsaldocontrato_reservado_to_Visible = 1;
         edtavTfsaldocontrato_reservado_Jsonclick = "";
         edtavTfsaldocontrato_reservado_Visible = 1;
         edtavTfsaldocontrato_credito_to_Jsonclick = "";
         edtavTfsaldocontrato_credito_to_Visible = 1;
         edtavTfsaldocontrato_credito_Jsonclick = "";
         edtavTfsaldocontrato_credito_Visible = 1;
         edtavTfsaldocontrato_unidademedicao_nome_sel_Jsonclick = "";
         edtavTfsaldocontrato_unidademedicao_nome_sel_Visible = 1;
         edtavTfsaldocontrato_unidademedicao_nome_Jsonclick = "";
         edtavTfsaldocontrato_unidademedicao_nome_Visible = 1;
         edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick = "";
         edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_to_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_to_Visible = 1;
         edtavTfsaldocontrato_vigenciafim_Jsonclick = "";
         edtavTfsaldocontrato_vigenciafim_Visible = 1;
         edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick = "";
         edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_to_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_to_Visible = 1;
         edtavTfsaldocontrato_vigenciainicio_Jsonclick = "";
         edtavTfsaldocontrato_vigenciainicio_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_saldocontrato_saldo_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_saldo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_saldo_Rangefilterto = "At�";
         Ddo_saldocontrato_saldo_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_saldo_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_saldo_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_saldo_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_saldo_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_saldo_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_saldo_Datalistfixedvalues = "";
         Ddo_saldocontrato_saldo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_saldo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Filtertype = "Numeric";
         Ddo_saldocontrato_saldo_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_saldo_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_saldo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_saldo_Cls = "ColumnSettings";
         Ddo_saldocontrato_saldo_Tooltip = "Op��es";
         Ddo_saldocontrato_saldo_Caption = "";
         Ddo_saldocontrato_executado_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_executado_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_executado_Rangefilterto = "At�";
         Ddo_saldocontrato_executado_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_executado_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_executado_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_executado_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_executado_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_executado_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_executado_Datalistfixedvalues = "";
         Ddo_saldocontrato_executado_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_executado_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Filtertype = "Numeric";
         Ddo_saldocontrato_executado_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_executado_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_executado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_executado_Cls = "ColumnSettings";
         Ddo_saldocontrato_executado_Tooltip = "Op��es";
         Ddo_saldocontrato_executado_Caption = "";
         Ddo_saldocontrato_reservado_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_reservado_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_reservado_Rangefilterto = "At�";
         Ddo_saldocontrato_reservado_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_reservado_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_reservado_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_reservado_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_reservado_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_reservado_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_reservado_Datalistfixedvalues = "";
         Ddo_saldocontrato_reservado_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_reservado_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Filtertype = "Numeric";
         Ddo_saldocontrato_reservado_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_reservado_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_reservado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_reservado_Cls = "ColumnSettings";
         Ddo_saldocontrato_reservado_Tooltip = "Op��es";
         Ddo_saldocontrato_reservado_Caption = "";
         Ddo_saldocontrato_credito_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_credito_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_credito_Rangefilterto = "At�";
         Ddo_saldocontrato_credito_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_credito_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_credito_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_credito_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_credito_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_credito_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_credito_Datalistfixedvalues = "";
         Ddo_saldocontrato_credito_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_credito_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Filtertype = "Numeric";
         Ddo_saldocontrato_credito_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_credito_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_credito_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_credito_Cls = "ColumnSettings";
         Ddo_saldocontrato_credito_Tooltip = "Op��es";
         Ddo_saldocontrato_credito_Caption = "";
         Ddo_saldocontrato_unidademedicao_nome_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_unidademedicao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_unidademedicao_nome_Rangefilterto = "At�";
         Ddo_saldocontrato_unidademedicao_nome_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_unidademedicao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_unidademedicao_nome_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_unidademedicao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_unidademedicao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_unidademedicao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_unidademedicao_nome_Datalistproc = "GetContratoContratoSaldoContratoWCFilterData";
         Ddo_saldocontrato_unidademedicao_nome_Datalistfixedvalues = "";
         Ddo_saldocontrato_unidademedicao_nome_Datalisttype = "Dynamic";
         Ddo_saldocontrato_unidademedicao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_saldocontrato_unidademedicao_nome_Filtertype = "Character";
         Ddo_saldocontrato_unidademedicao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_unidademedicao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_unidademedicao_nome_Cls = "ColumnSettings";
         Ddo_saldocontrato_unidademedicao_nome_Tooltip = "Op��es";
         Ddo_saldocontrato_unidademedicao_nome_Caption = "";
         Ddo_saldocontrato_vigenciafim_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_vigenciafim_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_vigenciafim_Rangefilterto = "At�";
         Ddo_saldocontrato_vigenciafim_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_vigenciafim_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_vigenciafim_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_vigenciafim_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_vigenciafim_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_vigenciafim_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_vigenciafim_Datalistfixedvalues = "";
         Ddo_saldocontrato_vigenciafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_vigenciafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Filtertype = "Date";
         Ddo_saldocontrato_vigenciafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_vigenciafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_vigenciafim_Cls = "ColumnSettings";
         Ddo_saldocontrato_vigenciafim_Tooltip = "Op��es";
         Ddo_saldocontrato_vigenciafim_Caption = "";
         Ddo_saldocontrato_vigenciainicio_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_vigenciainicio_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_vigenciainicio_Rangefilterto = "At�";
         Ddo_saldocontrato_vigenciainicio_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_vigenciainicio_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_vigenciainicio_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_vigenciainicio_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_vigenciainicio_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_vigenciainicio_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_vigenciainicio_Datalistfixedvalues = "";
         Ddo_saldocontrato_vigenciainicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_vigenciainicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Filtertype = "Date";
         Ddo_saldocontrato_vigenciainicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_vigenciainicio_Cls = "ColumnSettings";
         Ddo_saldocontrato_vigenciainicio_Tooltip = "Op��es";
         Ddo_saldocontrato_vigenciainicio_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV27SaldoContrato_VigenciaInicioTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV33SaldoContrato_VigenciaFimTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData',fld:'vSALDOCONTRATO_UNIDADEMEDICAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV43SaldoContrato_CreditoTitleFilterData',fld:'vSALDOCONTRATO_CREDITOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47SaldoContrato_ReservadoTitleFilterData',fld:'vSALDOCONTRATO_RESERVADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV51SaldoContrato_ExecutadoTitleFilterData',fld:'vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV55SaldoContrato_SaldoTitleFilterData',fld:'vSALDOCONTRATO_SALDOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtSaldoContrato_VigenciaInicio_Titleformat',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Titleformat'},{av:'edtSaldoContrato_VigenciaInicio_Title',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Title'},{av:'edtSaldoContrato_VigenciaFim_Titleformat',ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Titleformat'},{av:'edtSaldoContrato_VigenciaFim_Title',ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Title'},{av:'edtSaldoContrato_UnidadeMedicao_Nome_Titleformat',ctrl:'SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'Titleformat'},{av:'edtSaldoContrato_UnidadeMedicao_Nome_Title',ctrl:'SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'Title'},{av:'edtSaldoContrato_Credito_Titleformat',ctrl:'SALDOCONTRATO_CREDITO',prop:'Titleformat'},{av:'edtSaldoContrato_Credito_Title',ctrl:'SALDOCONTRATO_CREDITO',prop:'Title'},{av:'edtSaldoContrato_Reservado_Titleformat',ctrl:'SALDOCONTRATO_RESERVADO',prop:'Titleformat'},{av:'edtSaldoContrato_Reservado_Title',ctrl:'SALDOCONTRATO_RESERVADO',prop:'Title'},{av:'edtSaldoContrato_Executado_Titleformat',ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Titleformat'},{av:'edtSaldoContrato_Executado_Title',ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Title'},{av:'edtSaldoContrato_Saldo_Titleformat',ctrl:'SALDOCONTRATO_SALDO',prop:'Titleformat'},{av:'edtSaldoContrato_Saldo_Title',ctrl:'SALDOCONTRATO_SALDO',prop:'Title'},{av:'AV61GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV62GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavDebitosaldoreserva_Visible',ctrl:'vDEBITOSALDORESERVA',prop:'Visible'},{av:'edtavReservarsaldo_Visible',ctrl:'vRESERVARSALDO',prop:'Visible'},{av:'edtavDebitosaldo_Visible',ctrl:'vDEBITOSALDO',prop:'Visible'},{ctrl:'BTNINICIALIZARSALDO',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED","{handler:'E12MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_vigenciainicio_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_vigenciainicio_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED","{handler:'E13MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_vigenciafim_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_vigenciafim_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_vigenciafim_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME.ONOPTIONCLICKED","{handler:'E14MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_unidademedicao_nome_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED","{handler:'E15MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_credito_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_credito_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_credito_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED","{handler:'E16MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_reservado_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_reservado_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_reservado_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED","{handler:'E17MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_executado_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_executado_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_executado_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED","{handler:'E18MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_saldo_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_saldo_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_saldo_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_saldo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_saldocontrato_vigenciainicio_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_vigenciafim_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_nome_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_saldocontrato_credito_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_reservado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_executado_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E22MI2',iparms:[],oparms:[{av:'AV24DebitoSaldoReserva',fld:'vDEBITOSALDORESERVA',pic:'',nv:''},{av:'edtavDebitosaldoreserva_Tooltiptext',ctrl:'vDEBITOSALDORESERVA',prop:'Tooltiptext'},{av:'AV25ReservarSaldo',fld:'vRESERVARSALDO',pic:'',nv:''},{av:'edtavReservarsaldo_Tooltiptext',ctrl:'vRESERVARSALDO',prop:'Tooltiptext'},{av:'AV26DebitoSaldo',fld:'vDEBITOSALDO',pic:'',nv:''},{av:'edtavDebitosaldo_Tooltiptext',ctrl:'vDEBITOSALDO',prop:'Tooltiptext'}]}");
         setEventMetadata("'DOINICIALIZARSALDO'","{handler:'E19MI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV29TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV34TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV35TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV40TFSaldoContrato_UnidadeMedicao_Nome',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV16IsErro',fld:'vISERRO',pic:'',nv:false}],oparms:[{ctrl:'BTNINICIALIZARSALDO',prop:'Visible'},{av:'AV16IsErro',fld:'vISERRO',pic:'',nv:false}]}");
         setEventMetadata("VRESERVARSALDO.CLICK","{handler:'E23MI2',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDEBITOSALDORESERVA.CLICK","{handler:'E24MI2',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDEBITOSALDO.CLICK","{handler:'E25MI2',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV22SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV21Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_saldocontrato_vigenciainicio_Activeeventkey = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtext_get = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtextto_get = "";
         Ddo_saldocontrato_vigenciafim_Activeeventkey = "";
         Ddo_saldocontrato_vigenciafim_Filteredtext_get = "";
         Ddo_saldocontrato_vigenciafim_Filteredtextto_get = "";
         Ddo_saldocontrato_unidademedicao_nome_Activeeventkey = "";
         Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get = "";
         Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get = "";
         Ddo_saldocontrato_credito_Activeeventkey = "";
         Ddo_saldocontrato_credito_Filteredtext_get = "";
         Ddo_saldocontrato_credito_Filteredtextto_get = "";
         Ddo_saldocontrato_reservado_Activeeventkey = "";
         Ddo_saldocontrato_reservado_Filteredtext_get = "";
         Ddo_saldocontrato_reservado_Filteredtextto_get = "";
         Ddo_saldocontrato_executado_Activeeventkey = "";
         Ddo_saldocontrato_executado_Filteredtext_get = "";
         Ddo_saldocontrato_executado_Filteredtextto_get = "";
         Ddo_saldocontrato_saldo_Activeeventkey = "";
         Ddo_saldocontrato_saldo_Filteredtext_get = "";
         Ddo_saldocontrato_saldo_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV28TFSaldoContrato_VigenciaInicio = DateTime.MinValue;
         AV29TFSaldoContrato_VigenciaInicio_To = DateTime.MinValue;
         AV34TFSaldoContrato_VigenciaFim = DateTime.MinValue;
         AV35TFSaldoContrato_VigenciaFim_To = DateTime.MinValue;
         AV40TFSaldoContrato_UnidadeMedicao_Nome = "";
         AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel = "";
         AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "";
         AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "";
         AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace = "";
         AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace = "";
         AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "";
         AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "";
         AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace = "";
         AV71Pgmname = "";
         GXKey = "";
         scmdbuf = "";
         H00MI3_A40000GXC1 = new int[1] ;
         H00MI3_n40000GXC1 = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV59DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV27SaldoContrato_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33SaldoContrato_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43SaldoContrato_CreditoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47SaldoContrato_ReservadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51SaldoContrato_ExecutadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55SaldoContrato_SaldoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_saldocontrato_vigenciainicio_Filteredtext_set = "";
         Ddo_saldocontrato_vigenciainicio_Filteredtextto_set = "";
         Ddo_saldocontrato_vigenciainicio_Sortedstatus = "";
         Ddo_saldocontrato_vigenciafim_Filteredtext_set = "";
         Ddo_saldocontrato_vigenciafim_Filteredtextto_set = "";
         Ddo_saldocontrato_vigenciafim_Sortedstatus = "";
         Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set = "";
         Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set = "";
         Ddo_saldocontrato_unidademedicao_nome_Sortedstatus = "";
         Ddo_saldocontrato_credito_Filteredtext_set = "";
         Ddo_saldocontrato_credito_Filteredtextto_set = "";
         Ddo_saldocontrato_credito_Sortedstatus = "";
         Ddo_saldocontrato_reservado_Filteredtext_set = "";
         Ddo_saldocontrato_reservado_Filteredtextto_set = "";
         Ddo_saldocontrato_reservado_Sortedstatus = "";
         Ddo_saldocontrato_executado_Filteredtext_set = "";
         Ddo_saldocontrato_executado_Filteredtextto_set = "";
         Ddo_saldocontrato_executado_Sortedstatus = "";
         Ddo_saldocontrato_saldo_Filteredtext_set = "";
         Ddo_saldocontrato_saldo_Filteredtextto_set = "";
         Ddo_saldocontrato_saldo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV30DDO_SaldoContrato_VigenciaInicioAuxDate = DateTime.MinValue;
         AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo = DateTime.MinValue;
         AV36DDO_SaldoContrato_VigenciaFimAuxDate = DateTime.MinValue;
         AV37DDO_SaldoContrato_VigenciaFimAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         A1784SaldoContrato_UnidadeMedicao_Nome = "";
         AV24DebitoSaldoReserva = "";
         AV68Debitosaldoreserva_GXI = "";
         AV25ReservarSaldo = "";
         AV69Reservarsaldo_GXI = "";
         AV26DebitoSaldo = "";
         AV70Debitosaldo_GXI = "";
         GridContainer = new GXWebGrid( context);
         lV40TFSaldoContrato_UnidadeMedicao_Nome = "";
         H00MI5_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         H00MI5_A1576SaldoContrato_Saldo = new decimal[1] ;
         H00MI5_A1575SaldoContrato_Executado = new decimal[1] ;
         H00MI5_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00MI5_A1573SaldoContrato_Credito = new decimal[1] ;
         H00MI5_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         H00MI5_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         H00MI5_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00MI5_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00MI5_A1561SaldoContrato_Codigo = new int[1] ;
         H00MI5_A74Contrato_Codigo = new int[1] ;
         H00MI5_A40000GXC1 = new int[1] ;
         H00MI5_n40000GXC1 = new bool[] {false} ;
         H00MI7_AGRID_nRecordCount = new long[1] ;
         H00MI9_A40000GXC1 = new int[1] ;
         H00MI9_n40000GXC1 = new bool[] {false} ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV17imgReservar = "";
         AV65Imgreservar_GXI = "";
         AV19imgDebitoSaldoReserva = "";
         AV66Imgdebitosaldoreserva_GXI = "";
         AV18imgDebitoSaldo = "";
         AV67Imgdebitosaldo_GXI = "";
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         bttBtninicializarsaldo_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contrato_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratocontratosaldocontratowc__default(),
            new Object[][] {
                new Object[] {
               H00MI3_A40000GXC1, H00MI3_n40000GXC1
               }
               , new Object[] {
               H00MI5_A1783SaldoContrato_UnidadeMedicao_Codigo, H00MI5_A1576SaldoContrato_Saldo, H00MI5_A1575SaldoContrato_Executado, H00MI5_A1574SaldoContrato_Reservado, H00MI5_A1573SaldoContrato_Credito, H00MI5_A1784SaldoContrato_UnidadeMedicao_Nome, H00MI5_n1784SaldoContrato_UnidadeMedicao_Nome, H00MI5_A1572SaldoContrato_VigenciaFim, H00MI5_A1571SaldoContrato_VigenciaInicio, H00MI5_A1561SaldoContrato_Codigo,
               H00MI5_A74Contrato_Codigo, H00MI5_A40000GXC1, H00MI5_n40000GXC1
               }
               , new Object[] {
               H00MI7_AGRID_nRecordCount
               }
               , new Object[] {
               H00MI9_A40000GXC1, H00MI9_n40000GXC1
               }
            }
         );
         AV71Pgmname = "ContratoContratoSaldoContratoWC";
         /* GeneXus formulas. */
         AV71Pgmname = "ContratoContratoSaldoContratoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSaldoContrato_VigenciaInicio_Titleformat ;
      private short edtSaldoContrato_VigenciaFim_Titleformat ;
      private short edtSaldoContrato_UnidadeMedicao_Nome_Titleformat ;
      private short edtSaldoContrato_Credito_Titleformat ;
      private short edtSaldoContrato_Reservado_Titleformat ;
      private short edtSaldoContrato_Executado_Titleformat ;
      private short edtSaldoContrato_Saldo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contrato_Codigo ;
      private int wcpOAV7Contrato_Codigo ;
      private int subGrid_Rows ;
      private int A40000GXC1 ;
      private int AV20NotaEmpenho_Codigo ;
      private int AV21Contagem_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_saldocontrato_vigenciainicio_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_vigenciafim_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_unidademedicao_nome_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_credito_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_reservado_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_executado_Datalistupdateminimumcharacters ;
      private int Ddo_saldocontrato_saldo_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfsaldocontrato_vigenciainicio_Visible ;
      private int edtavTfsaldocontrato_vigenciainicio_to_Visible ;
      private int edtavTfsaldocontrato_vigenciafim_Visible ;
      private int edtavTfsaldocontrato_vigenciafim_to_Visible ;
      private int edtavTfsaldocontrato_unidademedicao_nome_Visible ;
      private int edtavTfsaldocontrato_unidademedicao_nome_sel_Visible ;
      private int edtavTfsaldocontrato_credito_Visible ;
      private int edtavTfsaldocontrato_credito_to_Visible ;
      private int edtavTfsaldocontrato_reservado_Visible ;
      private int edtavTfsaldocontrato_reservado_to_Visible ;
      private int edtavTfsaldocontrato_executado_Visible ;
      private int edtavTfsaldocontrato_executado_to_Visible ;
      private int edtavTfsaldocontrato_saldo_Visible ;
      private int edtavTfsaldocontrato_saldo_to_Visible ;
      private int edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Visible ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int bttBtninicializarsaldo_Visible ;
      private int edtavDebitosaldoreserva_Visible ;
      private int edtavReservarsaldo_Visible ;
      private int edtavDebitosaldo_Visible ;
      private int AV60PageToGo ;
      private int bttBtninicializarsaldo_Enabled ;
      private int AV72GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDebitosaldoreserva_Enabled ;
      private int edtavReservarsaldo_Enabled ;
      private int edtavDebitosaldo_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV61GridCurrentPage ;
      private long AV62GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV44TFSaldoContrato_Credito ;
      private decimal AV45TFSaldoContrato_Credito_To ;
      private decimal AV48TFSaldoContrato_Reservado ;
      private decimal AV49TFSaldoContrato_Reservado_To ;
      private decimal AV52TFSaldoContrato_Executado ;
      private decimal AV53TFSaldoContrato_Executado_To ;
      private decimal AV56TFSaldoContrato_Saldo ;
      private decimal AV57TFSaldoContrato_Saldo_To ;
      private decimal AV22SaldoContrato_Credito ;
      private decimal A1573SaldoContrato_Credito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_saldocontrato_vigenciainicio_Activeeventkey ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtext_get ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtextto_get ;
      private String Ddo_saldocontrato_vigenciafim_Activeeventkey ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtext_get ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtextto_get ;
      private String Ddo_saldocontrato_unidademedicao_nome_Activeeventkey ;
      private String Ddo_saldocontrato_unidademedicao_nome_Filteredtext_get ;
      private String Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_get ;
      private String Ddo_saldocontrato_credito_Activeeventkey ;
      private String Ddo_saldocontrato_credito_Filteredtext_get ;
      private String Ddo_saldocontrato_credito_Filteredtextto_get ;
      private String Ddo_saldocontrato_reservado_Activeeventkey ;
      private String Ddo_saldocontrato_reservado_Filteredtext_get ;
      private String Ddo_saldocontrato_reservado_Filteredtextto_get ;
      private String Ddo_saldocontrato_executado_Activeeventkey ;
      private String Ddo_saldocontrato_executado_Filteredtext_get ;
      private String Ddo_saldocontrato_executado_Filteredtextto_get ;
      private String Ddo_saldocontrato_saldo_Activeeventkey ;
      private String Ddo_saldocontrato_saldo_Filteredtext_get ;
      private String Ddo_saldocontrato_saldo_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_19_idx="0001" ;
      private String AV40TFSaldoContrato_UnidadeMedicao_Nome ;
      private String AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ;
      private String AV71Pgmname ;
      private String GXKey ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_saldocontrato_vigenciainicio_Caption ;
      private String Ddo_saldocontrato_vigenciainicio_Tooltip ;
      private String Ddo_saldocontrato_vigenciainicio_Cls ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtext_set ;
      private String Ddo_saldocontrato_vigenciainicio_Filteredtextto_set ;
      private String Ddo_saldocontrato_vigenciainicio_Dropdownoptionstype ;
      private String Ddo_saldocontrato_vigenciainicio_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_vigenciainicio_Sortedstatus ;
      private String Ddo_saldocontrato_vigenciainicio_Filtertype ;
      private String Ddo_saldocontrato_vigenciainicio_Datalistfixedvalues ;
      private String Ddo_saldocontrato_vigenciainicio_Sortasc ;
      private String Ddo_saldocontrato_vigenciainicio_Sortdsc ;
      private String Ddo_saldocontrato_vigenciainicio_Loadingdata ;
      private String Ddo_saldocontrato_vigenciainicio_Cleanfilter ;
      private String Ddo_saldocontrato_vigenciainicio_Rangefilterfrom ;
      private String Ddo_saldocontrato_vigenciainicio_Rangefilterto ;
      private String Ddo_saldocontrato_vigenciainicio_Noresultsfound ;
      private String Ddo_saldocontrato_vigenciainicio_Searchbuttontext ;
      private String Ddo_saldocontrato_vigenciafim_Caption ;
      private String Ddo_saldocontrato_vigenciafim_Tooltip ;
      private String Ddo_saldocontrato_vigenciafim_Cls ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtext_set ;
      private String Ddo_saldocontrato_vigenciafim_Filteredtextto_set ;
      private String Ddo_saldocontrato_vigenciafim_Dropdownoptionstype ;
      private String Ddo_saldocontrato_vigenciafim_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_vigenciafim_Sortedstatus ;
      private String Ddo_saldocontrato_vigenciafim_Filtertype ;
      private String Ddo_saldocontrato_vigenciafim_Datalistfixedvalues ;
      private String Ddo_saldocontrato_vigenciafim_Sortasc ;
      private String Ddo_saldocontrato_vigenciafim_Sortdsc ;
      private String Ddo_saldocontrato_vigenciafim_Loadingdata ;
      private String Ddo_saldocontrato_vigenciafim_Cleanfilter ;
      private String Ddo_saldocontrato_vigenciafim_Rangefilterfrom ;
      private String Ddo_saldocontrato_vigenciafim_Rangefilterto ;
      private String Ddo_saldocontrato_vigenciafim_Noresultsfound ;
      private String Ddo_saldocontrato_vigenciafim_Searchbuttontext ;
      private String Ddo_saldocontrato_unidademedicao_nome_Caption ;
      private String Ddo_saldocontrato_unidademedicao_nome_Tooltip ;
      private String Ddo_saldocontrato_unidademedicao_nome_Cls ;
      private String Ddo_saldocontrato_unidademedicao_nome_Filteredtext_set ;
      private String Ddo_saldocontrato_unidademedicao_nome_Selectedvalue_set ;
      private String Ddo_saldocontrato_unidademedicao_nome_Dropdownoptionstype ;
      private String Ddo_saldocontrato_unidademedicao_nome_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_unidademedicao_nome_Sortedstatus ;
      private String Ddo_saldocontrato_unidademedicao_nome_Filtertype ;
      private String Ddo_saldocontrato_unidademedicao_nome_Datalisttype ;
      private String Ddo_saldocontrato_unidademedicao_nome_Datalistfixedvalues ;
      private String Ddo_saldocontrato_unidademedicao_nome_Datalistproc ;
      private String Ddo_saldocontrato_unidademedicao_nome_Sortasc ;
      private String Ddo_saldocontrato_unidademedicao_nome_Sortdsc ;
      private String Ddo_saldocontrato_unidademedicao_nome_Loadingdata ;
      private String Ddo_saldocontrato_unidademedicao_nome_Cleanfilter ;
      private String Ddo_saldocontrato_unidademedicao_nome_Rangefilterfrom ;
      private String Ddo_saldocontrato_unidademedicao_nome_Rangefilterto ;
      private String Ddo_saldocontrato_unidademedicao_nome_Noresultsfound ;
      private String Ddo_saldocontrato_unidademedicao_nome_Searchbuttontext ;
      private String Ddo_saldocontrato_credito_Caption ;
      private String Ddo_saldocontrato_credito_Tooltip ;
      private String Ddo_saldocontrato_credito_Cls ;
      private String Ddo_saldocontrato_credito_Filteredtext_set ;
      private String Ddo_saldocontrato_credito_Filteredtextto_set ;
      private String Ddo_saldocontrato_credito_Dropdownoptionstype ;
      private String Ddo_saldocontrato_credito_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_credito_Sortedstatus ;
      private String Ddo_saldocontrato_credito_Filtertype ;
      private String Ddo_saldocontrato_credito_Datalistfixedvalues ;
      private String Ddo_saldocontrato_credito_Sortasc ;
      private String Ddo_saldocontrato_credito_Sortdsc ;
      private String Ddo_saldocontrato_credito_Loadingdata ;
      private String Ddo_saldocontrato_credito_Cleanfilter ;
      private String Ddo_saldocontrato_credito_Rangefilterfrom ;
      private String Ddo_saldocontrato_credito_Rangefilterto ;
      private String Ddo_saldocontrato_credito_Noresultsfound ;
      private String Ddo_saldocontrato_credito_Searchbuttontext ;
      private String Ddo_saldocontrato_reservado_Caption ;
      private String Ddo_saldocontrato_reservado_Tooltip ;
      private String Ddo_saldocontrato_reservado_Cls ;
      private String Ddo_saldocontrato_reservado_Filteredtext_set ;
      private String Ddo_saldocontrato_reservado_Filteredtextto_set ;
      private String Ddo_saldocontrato_reservado_Dropdownoptionstype ;
      private String Ddo_saldocontrato_reservado_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_reservado_Sortedstatus ;
      private String Ddo_saldocontrato_reservado_Filtertype ;
      private String Ddo_saldocontrato_reservado_Datalistfixedvalues ;
      private String Ddo_saldocontrato_reservado_Sortasc ;
      private String Ddo_saldocontrato_reservado_Sortdsc ;
      private String Ddo_saldocontrato_reservado_Loadingdata ;
      private String Ddo_saldocontrato_reservado_Cleanfilter ;
      private String Ddo_saldocontrato_reservado_Rangefilterfrom ;
      private String Ddo_saldocontrato_reservado_Rangefilterto ;
      private String Ddo_saldocontrato_reservado_Noresultsfound ;
      private String Ddo_saldocontrato_reservado_Searchbuttontext ;
      private String Ddo_saldocontrato_executado_Caption ;
      private String Ddo_saldocontrato_executado_Tooltip ;
      private String Ddo_saldocontrato_executado_Cls ;
      private String Ddo_saldocontrato_executado_Filteredtext_set ;
      private String Ddo_saldocontrato_executado_Filteredtextto_set ;
      private String Ddo_saldocontrato_executado_Dropdownoptionstype ;
      private String Ddo_saldocontrato_executado_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_executado_Sortedstatus ;
      private String Ddo_saldocontrato_executado_Filtertype ;
      private String Ddo_saldocontrato_executado_Datalistfixedvalues ;
      private String Ddo_saldocontrato_executado_Sortasc ;
      private String Ddo_saldocontrato_executado_Sortdsc ;
      private String Ddo_saldocontrato_executado_Loadingdata ;
      private String Ddo_saldocontrato_executado_Cleanfilter ;
      private String Ddo_saldocontrato_executado_Rangefilterfrom ;
      private String Ddo_saldocontrato_executado_Rangefilterto ;
      private String Ddo_saldocontrato_executado_Noresultsfound ;
      private String Ddo_saldocontrato_executado_Searchbuttontext ;
      private String Ddo_saldocontrato_saldo_Caption ;
      private String Ddo_saldocontrato_saldo_Tooltip ;
      private String Ddo_saldocontrato_saldo_Cls ;
      private String Ddo_saldocontrato_saldo_Filteredtext_set ;
      private String Ddo_saldocontrato_saldo_Filteredtextto_set ;
      private String Ddo_saldocontrato_saldo_Dropdownoptionstype ;
      private String Ddo_saldocontrato_saldo_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_saldo_Sortedstatus ;
      private String Ddo_saldocontrato_saldo_Filtertype ;
      private String Ddo_saldocontrato_saldo_Datalistfixedvalues ;
      private String Ddo_saldocontrato_saldo_Sortasc ;
      private String Ddo_saldocontrato_saldo_Sortdsc ;
      private String Ddo_saldocontrato_saldo_Loadingdata ;
      private String Ddo_saldocontrato_saldo_Cleanfilter ;
      private String Ddo_saldocontrato_saldo_Rangefilterfrom ;
      private String Ddo_saldocontrato_saldo_Rangefilterto ;
      private String Ddo_saldocontrato_saldo_Noresultsfound ;
      private String Ddo_saldocontrato_saldo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciainicio_Internalname ;
      private String edtavTfsaldocontrato_vigenciainicio_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciainicio_to_Internalname ;
      private String edtavTfsaldocontrato_vigenciainicio_to_Jsonclick ;
      private String divDdo_saldocontrato_vigenciainicioauxdates_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdate_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdate_Jsonclick ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdateto_Internalname ;
      private String edtavDdo_saldocontrato_vigenciainicioauxdateto_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciafim_Internalname ;
      private String edtavTfsaldocontrato_vigenciafim_Jsonclick ;
      private String edtavTfsaldocontrato_vigenciafim_to_Internalname ;
      private String edtavTfsaldocontrato_vigenciafim_to_Jsonclick ;
      private String divDdo_saldocontrato_vigenciafimauxdates_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdate_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdate_Jsonclick ;
      private String edtavDdo_saldocontrato_vigenciafimauxdateto_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimauxdateto_Jsonclick ;
      private String edtavTfsaldocontrato_unidademedicao_nome_Internalname ;
      private String edtavTfsaldocontrato_unidademedicao_nome_Jsonclick ;
      private String edtavTfsaldocontrato_unidademedicao_nome_sel_Internalname ;
      private String edtavTfsaldocontrato_unidademedicao_nome_sel_Jsonclick ;
      private String edtavTfsaldocontrato_credito_Internalname ;
      private String edtavTfsaldocontrato_credito_Jsonclick ;
      private String edtavTfsaldocontrato_credito_to_Internalname ;
      private String edtavTfsaldocontrato_credito_to_Jsonclick ;
      private String edtavTfsaldocontrato_reservado_Internalname ;
      private String edtavTfsaldocontrato_reservado_Jsonclick ;
      private String edtavTfsaldocontrato_reservado_to_Internalname ;
      private String edtavTfsaldocontrato_reservado_to_Jsonclick ;
      private String edtavTfsaldocontrato_executado_Internalname ;
      private String edtavTfsaldocontrato_executado_Jsonclick ;
      private String edtavTfsaldocontrato_executado_to_Internalname ;
      private String edtavTfsaldocontrato_executado_to_Jsonclick ;
      private String edtavTfsaldocontrato_saldo_Internalname ;
      private String edtavTfsaldocontrato_saldo_Jsonclick ;
      private String edtavTfsaldocontrato_saldo_to_Internalname ;
      private String edtavTfsaldocontrato_saldo_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_saldocontrato_vigenciainiciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_vigenciafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_unidademedicao_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_creditotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_reservadotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_executadotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_saldotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContrato_Codigo_Internalname ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String edtSaldoContrato_VigenciaInicio_Internalname ;
      private String edtSaldoContrato_VigenciaFim_Internalname ;
      private String A1784SaldoContrato_UnidadeMedicao_Nome ;
      private String edtSaldoContrato_UnidadeMedicao_Nome_Internalname ;
      private String edtSaldoContrato_Credito_Internalname ;
      private String edtSaldoContrato_Reservado_Internalname ;
      private String edtavDebitosaldoreserva_Internalname ;
      private String edtSaldoContrato_Executado_Internalname ;
      private String edtSaldoContrato_Saldo_Internalname ;
      private String edtavReservarsaldo_Internalname ;
      private String edtavDebitosaldo_Internalname ;
      private String lV40TFSaldoContrato_UnidadeMedicao_Nome ;
      private String subGrid_Internalname ;
      private String Ddo_saldocontrato_vigenciainicio_Internalname ;
      private String Ddo_saldocontrato_vigenciafim_Internalname ;
      private String Ddo_saldocontrato_unidademedicao_nome_Internalname ;
      private String Ddo_saldocontrato_credito_Internalname ;
      private String Ddo_saldocontrato_reservado_Internalname ;
      private String Ddo_saldocontrato_executado_Internalname ;
      private String Ddo_saldocontrato_saldo_Internalname ;
      private String bttBtninicializarsaldo_Internalname ;
      private String edtSaldoContrato_VigenciaInicio_Title ;
      private String edtSaldoContrato_VigenciaFim_Title ;
      private String edtSaldoContrato_UnidadeMedicao_Nome_Title ;
      private String edtSaldoContrato_Credito_Title ;
      private String edtSaldoContrato_Reservado_Title ;
      private String edtSaldoContrato_Executado_Title ;
      private String edtSaldoContrato_Saldo_Title ;
      private String edtavDebitosaldoreserva_Tooltiptext ;
      private String edtavReservarsaldo_Tooltiptext ;
      private String edtavDebitosaldo_Tooltiptext ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String bttBtninicializarsaldo_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String sCtrlAV7Contrato_Codigo ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String edtSaldoContrato_VigenciaInicio_Jsonclick ;
      private String edtSaldoContrato_VigenciaFim_Jsonclick ;
      private String edtSaldoContrato_UnidadeMedicao_Nome_Jsonclick ;
      private String edtSaldoContrato_Credito_Jsonclick ;
      private String edtSaldoContrato_Reservado_Jsonclick ;
      private String edtavDebitosaldoreserva_Jsonclick ;
      private String edtSaldoContrato_Executado_Jsonclick ;
      private String edtSaldoContrato_Saldo_Jsonclick ;
      private String edtavReservarsaldo_Jsonclick ;
      private String edtavDebitosaldo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV28TFSaldoContrato_VigenciaInicio ;
      private DateTime AV29TFSaldoContrato_VigenciaInicio_To ;
      private DateTime AV34TFSaldoContrato_VigenciaFim ;
      private DateTime AV35TFSaldoContrato_VigenciaFim_To ;
      private DateTime AV30DDO_SaldoContrato_VigenciaInicioAuxDate ;
      private DateTime AV31DDO_SaldoContrato_VigenciaInicioAuxDateTo ;
      private DateTime AV36DDO_SaldoContrato_VigenciaFimAuxDate ;
      private DateTime AV37DDO_SaldoContrato_VigenciaFimAuxDateTo ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n40000GXC1 ;
      private bool toggleJsOutput ;
      private bool AV16IsErro ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_saldocontrato_vigenciainicio_Includesortasc ;
      private bool Ddo_saldocontrato_vigenciainicio_Includesortdsc ;
      private bool Ddo_saldocontrato_vigenciainicio_Includefilter ;
      private bool Ddo_saldocontrato_vigenciainicio_Filterisrange ;
      private bool Ddo_saldocontrato_vigenciainicio_Includedatalist ;
      private bool Ddo_saldocontrato_vigenciafim_Includesortasc ;
      private bool Ddo_saldocontrato_vigenciafim_Includesortdsc ;
      private bool Ddo_saldocontrato_vigenciafim_Includefilter ;
      private bool Ddo_saldocontrato_vigenciafim_Filterisrange ;
      private bool Ddo_saldocontrato_vigenciafim_Includedatalist ;
      private bool Ddo_saldocontrato_unidademedicao_nome_Includesortasc ;
      private bool Ddo_saldocontrato_unidademedicao_nome_Includesortdsc ;
      private bool Ddo_saldocontrato_unidademedicao_nome_Includefilter ;
      private bool Ddo_saldocontrato_unidademedicao_nome_Filterisrange ;
      private bool Ddo_saldocontrato_unidademedicao_nome_Includedatalist ;
      private bool Ddo_saldocontrato_credito_Includesortasc ;
      private bool Ddo_saldocontrato_credito_Includesortdsc ;
      private bool Ddo_saldocontrato_credito_Includefilter ;
      private bool Ddo_saldocontrato_credito_Filterisrange ;
      private bool Ddo_saldocontrato_credito_Includedatalist ;
      private bool Ddo_saldocontrato_reservado_Includesortasc ;
      private bool Ddo_saldocontrato_reservado_Includesortdsc ;
      private bool Ddo_saldocontrato_reservado_Includefilter ;
      private bool Ddo_saldocontrato_reservado_Filterisrange ;
      private bool Ddo_saldocontrato_reservado_Includedatalist ;
      private bool Ddo_saldocontrato_executado_Includesortasc ;
      private bool Ddo_saldocontrato_executado_Includesortdsc ;
      private bool Ddo_saldocontrato_executado_Includefilter ;
      private bool Ddo_saldocontrato_executado_Filterisrange ;
      private bool Ddo_saldocontrato_executado_Includedatalist ;
      private bool Ddo_saldocontrato_saldo_Includesortasc ;
      private bool Ddo_saldocontrato_saldo_Includesortdsc ;
      private bool Ddo_saldocontrato_saldo_Includefilter ;
      private bool Ddo_saldocontrato_saldo_Filterisrange ;
      private bool Ddo_saldocontrato_saldo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV24DebitoSaldoReserva_IsBlob ;
      private bool AV25ReservarSaldo_IsBlob ;
      private bool AV26DebitoSaldo_IsBlob ;
      private String AV32ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace ;
      private String AV38ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace ;
      private String AV42ddo_SaldoContrato_UnidadeMedicao_NomeTitleControlIdToReplace ;
      private String AV46ddo_SaldoContrato_CreditoTitleControlIdToReplace ;
      private String AV50ddo_SaldoContrato_ReservadoTitleControlIdToReplace ;
      private String AV54ddo_SaldoContrato_ExecutadoTitleControlIdToReplace ;
      private String AV58ddo_SaldoContrato_SaldoTitleControlIdToReplace ;
      private String AV68Debitosaldoreserva_GXI ;
      private String AV69Reservarsaldo_GXI ;
      private String AV70Debitosaldo_GXI ;
      private String AV65Imgreservar_GXI ;
      private String AV66Imgdebitosaldoreserva_GXI ;
      private String AV67Imgdebitosaldo_GXI ;
      private String AV24DebitoSaldoReserva ;
      private String AV25ReservarSaldo ;
      private String AV26DebitoSaldo ;
      private String AV17imgReservar ;
      private String AV19imgDebitoSaldoReserva ;
      private String AV18imgDebitoSaldo ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00MI3_A40000GXC1 ;
      private bool[] H00MI3_n40000GXC1 ;
      private int[] H00MI5_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private decimal[] H00MI5_A1576SaldoContrato_Saldo ;
      private decimal[] H00MI5_A1575SaldoContrato_Executado ;
      private decimal[] H00MI5_A1574SaldoContrato_Reservado ;
      private decimal[] H00MI5_A1573SaldoContrato_Credito ;
      private String[] H00MI5_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] H00MI5_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private DateTime[] H00MI5_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] H00MI5_A1571SaldoContrato_VigenciaInicio ;
      private int[] H00MI5_A1561SaldoContrato_Codigo ;
      private int[] H00MI5_A74Contrato_Codigo ;
      private int[] H00MI5_A40000GXC1 ;
      private bool[] H00MI5_n40000GXC1 ;
      private long[] H00MI7_AGRID_nRecordCount ;
      private int[] H00MI9_A40000GXC1 ;
      private bool[] H00MI9_n40000GXC1 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27SaldoContrato_VigenciaInicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33SaldoContrato_VigenciaFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39SaldoContrato_UnidadeMedicao_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43SaldoContrato_CreditoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47SaldoContrato_ReservadoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51SaldoContrato_ExecutadoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55SaldoContrato_SaldoTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV59DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratocontratosaldocontratowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MI5( IGxContext context ,
                                             DateTime AV28TFSaldoContrato_VigenciaInicio ,
                                             DateTime AV29TFSaldoContrato_VigenciaInicio_To ,
                                             DateTime AV34TFSaldoContrato_VigenciaFim ,
                                             DateTime AV35TFSaldoContrato_VigenciaFim_To ,
                                             String AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                             String AV40TFSaldoContrato_UnidadeMedicao_Nome ,
                                             decimal AV44TFSaldoContrato_Credito ,
                                             decimal AV45TFSaldoContrato_Credito_To ,
                                             decimal AV48TFSaldoContrato_Reservado ,
                                             decimal AV49TFSaldoContrato_Reservado_To ,
                                             decimal AV52TFSaldoContrato_Executado ,
                                             decimal AV53TFSaldoContrato_Executado_To ,
                                             decimal AV56TFSaldoContrato_Saldo ,
                                             decimal AV57TFSaldoContrato_Saldo_To ,
                                             DateTime A1571SaldoContrato_VigenciaInicio ,
                                             DateTime A1572SaldoContrato_VigenciaFim ,
                                             String A1784SaldoContrato_UnidadeMedicao_Nome ,
                                             decimal A1573SaldoContrato_Credito ,
                                             decimal A1574SaldoContrato_Reservado ,
                                             decimal A1575SaldoContrato_Executado ,
                                             decimal A1576SaldoContrato_Saldo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T1.[SaldoContrato_Saldo], T1.[SaldoContrato_Executado], T1.[SaldoContrato_Reservado], T1.[SaldoContrato_Credito], T2.[UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, T1.[SaldoContrato_VigenciaFim], T1.[SaldoContrato_VigenciaInicio], T1.[SaldoContrato_Codigo], T1.[Contrato_Codigo], COALESCE( T3.[GXC1], 0) AS GXC1";
         sFromString = " FROM ([SaldoContrato] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[SaldoContrato_UnidadeMedicao_Codigo]),  (SELECT COUNT(*) AS GXC1 FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7Contrato_Codigo ) T3";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (DateTime.MinValue==AV28TFSaldoContrato_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] >= @AV28TFSaldoContrato_VigenciaInicio)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFSaldoContrato_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] <= @AV29TFSaldoContrato_VigenciaInicio_To)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFSaldoContrato_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] >= @AV34TFSaldoContrato_VigenciaFim)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFSaldoContrato_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] <= @AV35TFSaldoContrato_VigenciaFim_To)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV40TFSaldoContrato_UnidadeMedicao_Nome)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFSaldoContrato_Credito) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] >= @AV44TFSaldoContrato_Credito)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV45TFSaldoContrato_Credito_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] <= @AV45TFSaldoContrato_Credito_To)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFSaldoContrato_Reservado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] >= @AV48TFSaldoContrato_Reservado)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFSaldoContrato_Reservado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] <= @AV49TFSaldoContrato_Reservado_To)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV52TFSaldoContrato_Executado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] >= @AV52TFSaldoContrato_Executado)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFSaldoContrato_Executado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] <= @AV53TFSaldoContrato_Executado_To)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV56TFSaldoContrato_Saldo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] >= @AV56TFSaldoContrato_Saldo)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV57TFSaldoContrato_Saldo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] <= @AV57TFSaldoContrato_Saldo_To)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_VigenciaInicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_VigenciaInicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_VigenciaFim]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_VigenciaFim] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_Credito]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_Credito] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_Reservado]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_Reservado] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_Executado]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_Executado] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_Saldo]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[SaldoContrato_Saldo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SaldoContrato_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00MI7( IGxContext context ,
                                             DateTime AV28TFSaldoContrato_VigenciaInicio ,
                                             DateTime AV29TFSaldoContrato_VigenciaInicio_To ,
                                             DateTime AV34TFSaldoContrato_VigenciaFim ,
                                             DateTime AV35TFSaldoContrato_VigenciaFim_To ,
                                             String AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                             String AV40TFSaldoContrato_UnidadeMedicao_Nome ,
                                             decimal AV44TFSaldoContrato_Credito ,
                                             decimal AV45TFSaldoContrato_Credito_To ,
                                             decimal AV48TFSaldoContrato_Reservado ,
                                             decimal AV49TFSaldoContrato_Reservado_To ,
                                             decimal AV52TFSaldoContrato_Executado ,
                                             decimal AV53TFSaldoContrato_Executado_To ,
                                             decimal AV56TFSaldoContrato_Saldo ,
                                             decimal AV57TFSaldoContrato_Saldo_To ,
                                             DateTime A1571SaldoContrato_VigenciaInicio ,
                                             DateTime A1572SaldoContrato_VigenciaFim ,
                                             String A1784SaldoContrato_UnidadeMedicao_Nome ,
                                             decimal A1573SaldoContrato_Credito ,
                                             decimal A1574SaldoContrato_Reservado ,
                                             decimal A1575SaldoContrato_Executado ,
                                             decimal A1576SaldoContrato_Saldo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([SaldoContrato] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[SaldoContrato_UnidadeMedicao_Codigo]),  (SELECT COUNT(*) AS GXC1 FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7Contrato_Codigo ) T3";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (DateTime.MinValue==AV28TFSaldoContrato_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] >= @AV28TFSaldoContrato_VigenciaInicio)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFSaldoContrato_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] <= @AV29TFSaldoContrato_VigenciaInicio_To)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFSaldoContrato_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] >= @AV34TFSaldoContrato_VigenciaFim)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFSaldoContrato_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] <= @AV35TFSaldoContrato_VigenciaFim_To)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSaldoContrato_UnidadeMedicao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV40TFSaldoContrato_UnidadeMedicao_Nome)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFSaldoContrato_Credito) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] >= @AV44TFSaldoContrato_Credito)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV45TFSaldoContrato_Credito_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] <= @AV45TFSaldoContrato_Credito_To)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFSaldoContrato_Reservado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] >= @AV48TFSaldoContrato_Reservado)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFSaldoContrato_Reservado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] <= @AV49TFSaldoContrato_Reservado_To)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV52TFSaldoContrato_Executado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] >= @AV52TFSaldoContrato_Executado)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFSaldoContrato_Executado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] <= @AV53TFSaldoContrato_Executado_To)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV56TFSaldoContrato_Saldo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] >= @AV56TFSaldoContrato_Saldo)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV57TFSaldoContrato_Saldo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] <= @AV57TFSaldoContrato_Saldo_To)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00MI5(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] );
               case 2 :
                     return conditional_H00MI7(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MI3 ;
          prmH00MI3 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MI9 ;
          prmH00MI9 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MI5 ;
          prmH00MI5 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFSaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFSaldoContrato_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFSaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35TFSaldoContrato_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV40TFSaldoContrato_UnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFSaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV45TFSaldoContrato_Credito_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFSaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV49TFSaldoContrato_Reservado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV52TFSaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFSaldoContrato_Executado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV56TFSaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV57TFSaldoContrato_Saldo_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00MI7 ;
          prmH00MI7 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFSaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFSaldoContrato_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFSaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35TFSaldoContrato_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV40TFSaldoContrato_UnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFSaldoContrato_UnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFSaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV45TFSaldoContrato_Credito_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFSaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV49TFSaldoContrato_Reservado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV52TFSaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFSaldoContrato_Executado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV56TFSaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV57TFSaldoContrato_Saldo_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MI3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7Contrato_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MI3,1,0,true,false )
             ,new CursorDef("H00MI5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MI5,11,0,true,false )
             ,new CursorDef("H00MI7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MI7,1,0,true,false )
             ,new CursorDef("H00MI9", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7Contrato_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MI9,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
