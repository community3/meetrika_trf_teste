/*
               File: WP_AssociarContrato_Gestor
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:42.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcontrato_gestor : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcontrato_gestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcontrato_gestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoGestor_ContratoCod )
      {
         this.AV7ContratoGestor_ContratoCod = aP0_ContratoGestor_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7ContratoGestor_ContratoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoGestor_ContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOGESTOR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGestor_ContratoCod), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAHN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSHN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEHN2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299394251");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcontrato_gestor.aspx") + "?" + UrlEncode("" +AV7ContratoGestor_ContratoCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV37jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV37jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV35jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV35jqAssociados);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2009ContratoGestor_Tipo), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV39MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV39MenuPerfil);
         }
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOGESTOR_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43ContratoGestor_Tipo), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV34All);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOGESTOR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGestor_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOGESTOR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGestor_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormHN2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarContrato_Gestor" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WBHN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_HN2( true) ;
         }
         else
         {
            wb_table1_2_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTHN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPHN0( ) ;
      }

      protected void WSHN2( )
      {
         STARTHN2( ) ;
         EVTHN2( ) ;
      }

      protected void EVTHN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11HN2 */
                           E11HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12HN2 */
                           E12HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13HN2 */
                                 E13HN2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14HN2 */
                           E14HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15HN2 */
                           E15HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16HN2 */
                           E16HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17HN2 */
                           E17HN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18HN2 */
                           E18HN2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEHN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormHN2( ) ;
            }
         }
      }

      protected void PAHN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFHN2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12HN2 */
         E12HN2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00HN2 */
            pr_default.execute(0, new Object[] {AV7ContratoGestor_ContratoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = H00HN2_A74Contrato_Codigo[0];
               A77Contrato_Numero = H00HN2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
               /* Execute user event: E18HN2 */
               E18HN2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBHN0( ) ;
         }
      }

      protected void STRUPHN0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11HN2 */
         E11HN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV37jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV35jqAssociados);
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11HN2 */
         E11HN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11HN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV46GXLvl5 = 0;
            /* Using cursor H00HN3 */
            pr_default.execute(1, new Object[] {AV7ContratoGestor_ContratoCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = H00HN3_A74Contrato_Codigo[0];
               A92Contrato_Ativo = H00HN3_A92Contrato_Ativo[0];
               AV46GXLvl5 = 1;
               lblTbinativo_Visible = (!A92Contrato_Ativo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinativo_Visible), 5, 0)));
               bttBtn_confirm_Visible = (A92Contrato_Ativo&&(AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV46GXLvl5 == 0 )
            {
               GX_msglist.addItem("Contrato n�o encontrado.");
            }
            /* Execute user subroutine: 'TITULARSUBSTITUTO' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehcontratante )
            {
               /* Using cursor H00HN4 */
               pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A5AreaTrabalho_Codigo = H00HN4_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = H00HN4_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H00HN4_n29Contratante_Codigo[0];
                  AV27Contratante_Codigo = A29Contratante_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratante_Codigo), 6, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /* Using cursor H00HN5 */
               pr_default.execute(3, new Object[] {AV27Contratante_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A61ContratanteUsuario_UsuarioPessoaCod = H00HN5_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00HN5_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00HN5_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = H00HN5_A60ContratanteUsuario_UsuarioCod[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00HN5_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00HN5_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  A61ContratanteUsuario_UsuarioPessoaCod = H00HN5_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00HN5_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00HN5_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00HN5_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  AV8ContratoGestor_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoGestor_UsuarioCod), 6, 0)));
                  AV10Exist = false;
                  AV40Tipo = "";
                  /* Using cursor H00HN6 */
                  pr_default.execute(4, new Object[] {AV7ContratoGestor_ContratoCod, AV8ContratoGestor_UsuarioCod});
                  while ( (pr_default.getStatus(4) != 101) )
                  {
                     A1079ContratoGestor_UsuarioCod = H00HN6_A1079ContratoGestor_UsuarioCod[0];
                     A1078ContratoGestor_ContratoCod = H00HN6_A1078ContratoGestor_ContratoCod[0];
                     A2009ContratoGestor_Tipo = H00HN6_A2009ContratoGestor_Tipo[0];
                     n2009ContratoGestor_Tipo = H00HN6_n2009ContratoGestor_Tipo[0];
                     AV40Tipo = " (" + gxdomaintipogestor.getDescription(context,A2009ContratoGestor_Tipo) + ")";
                     AV10Exist = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(4);
                  AV36jqItem = new SdtjqSelectData_Item(context);
                  AV36jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0);
                  AV36jqItem.gxTpr_Descr = "��"+StringUtil.Trim( A62ContratanteUsuario_UsuarioPessoaNom)+AV40Tipo;
                  AV36jqItem.gxTpr_Selected = false;
                  if ( AV10Exist )
                  {
                     AV35jqAssociados.Add(AV36jqItem, 0);
                  }
                  else
                  {
                     AV37jqNaoAssociados.Add(AV36jqItem, 0);
                  }
                  AV20Codigos.Add(A60ContratanteUsuario_UsuarioCod, 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            if ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehcontratada )
            {
               /* Using cursor H00HN7 */
               pr_default.execute(5, new Object[] {AV7ContratoGestor_ContratoCod});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A74Contrato_Codigo = H00HN7_A74Contrato_Codigo[0];
                  A39Contratada_Codigo = H00HN7_A39Contratada_Codigo[0];
                  pr_default.dynParam(6, new Object[]{ new Object[]{
                                                       A69ContratadaUsuario_UsuarioCod ,
                                                       AV20Codigos ,
                                                       A66ContratadaUsuario_ContratadaCod ,
                                                       A39Contratada_Codigo },
                                                       new int[] {
                                                       TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                       }
                  });
                  /* Using cursor H00HN8 */
                  pr_default.execute(6, new Object[] {A39Contratada_Codigo});
                  while ( (pr_default.getStatus(6) != 101) )
                  {
                     A70ContratadaUsuario_UsuarioPessoaCod = H00HN8_A70ContratadaUsuario_UsuarioPessoaCod[0];
                     n70ContratadaUsuario_UsuarioPessoaCod = H00HN8_n70ContratadaUsuario_UsuarioPessoaCod[0];
                     A66ContratadaUsuario_ContratadaCod = H00HN8_A66ContratadaUsuario_ContratadaCod[0];
                     A69ContratadaUsuario_UsuarioCod = H00HN8_A69ContratadaUsuario_UsuarioCod[0];
                     A71ContratadaUsuario_UsuarioPessoaNom = H00HN8_A71ContratadaUsuario_UsuarioPessoaNom[0];
                     n71ContratadaUsuario_UsuarioPessoaNom = H00HN8_n71ContratadaUsuario_UsuarioPessoaNom[0];
                     A70ContratadaUsuario_UsuarioPessoaCod = H00HN8_A70ContratadaUsuario_UsuarioPessoaCod[0];
                     n70ContratadaUsuario_UsuarioPessoaCod = H00HN8_n70ContratadaUsuario_UsuarioPessoaCod[0];
                     A71ContratadaUsuario_UsuarioPessoaNom = H00HN8_A71ContratadaUsuario_UsuarioPessoaNom[0];
                     n71ContratadaUsuario_UsuarioPessoaNom = H00HN8_n71ContratadaUsuario_UsuarioPessoaNom[0];
                     AV8ContratoGestor_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoGestor_UsuarioCod), 6, 0)));
                     AV10Exist = false;
                     AV40Tipo = "";
                     /* Using cursor H00HN9 */
                     pr_default.execute(7, new Object[] {AV7ContratoGestor_ContratoCod, AV8ContratoGestor_UsuarioCod});
                     while ( (pr_default.getStatus(7) != 101) )
                     {
                        A1079ContratoGestor_UsuarioCod = H00HN9_A1079ContratoGestor_UsuarioCod[0];
                        A1078ContratoGestor_ContratoCod = H00HN9_A1078ContratoGestor_ContratoCod[0];
                        A2009ContratoGestor_Tipo = H00HN9_A2009ContratoGestor_Tipo[0];
                        n2009ContratoGestor_Tipo = H00HN9_n2009ContratoGestor_Tipo[0];
                        AV40Tipo = " (" + gxdomaintipogestor.getDescription(context,A2009ContratoGestor_Tipo) + ")";
                        AV10Exist = true;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(7);
                     AV36jqItem = new SdtjqSelectData_Item(context);
                     AV36jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0);
                     AV36jqItem.gxTpr_Descr = "��"+StringUtil.Trim( A71ContratadaUsuario_UsuarioPessoaNom)+AV40Tipo;
                     AV36jqItem.gxTpr_Selected = false;
                     if ( AV10Exist )
                     {
                        AV35jqAssociados.Add(AV36jqItem, 0);
                     }
                     else
                     {
                        AV37jqNaoAssociados.Add(AV36jqItem, 0);
                     }
                     pr_default.readNext(6);
                  }
                  pr_default.close(6);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
            }
         }
      }

      protected void E12HN2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13HN2 */
         E13HN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13HN2( )
      {
         /* Enter Routine */
         AV12Success = true;
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV35jqAssociados.Count )
         {
            AV36jqItem = ((SdtjqSelectData_Item)AV35jqAssociados.Item(AV53GXV1));
            if ( AV12Success )
            {
               AV8ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( AV36jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoGestor_UsuarioCod), 6, 0)));
               AV54GXLvl98 = 0;
               /* Using cursor H00HN10 */
               pr_default.execute(8, new Object[] {AV7ContratoGestor_ContratoCod, AV8ContratoGestor_UsuarioCod});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A1079ContratoGestor_UsuarioCod = H00HN10_A1079ContratoGestor_UsuarioCod[0];
                  A1078ContratoGestor_ContratoCod = H00HN10_A1078ContratoGestor_ContratoCod[0];
                  AV54GXLvl98 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(8);
               if ( AV54GXLvl98 == 0 )
               {
                  AV11ContratoGestor = new SdtContratoGestor(context);
                  AV11ContratoGestor.gxTpr_Contratogestor_contratocod = AV7ContratoGestor_ContratoCod;
                  AV11ContratoGestor.gxTpr_Contratogestor_usuariocod = AV8ContratoGestor_UsuarioCod;
                  AV11ContratoGestor.Save();
                  if ( ! AV11ContratoGestor.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         AV55GXV2 = 1;
         while ( AV55GXV2 <= AV37jqNaoAssociados.Count )
         {
            AV36jqItem = ((SdtjqSelectData_Item)AV37jqNaoAssociados.Item(AV55GXV2));
            if ( AV12Success )
            {
               AV8ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( AV36jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoGestor_UsuarioCod), 6, 0)));
               /* Using cursor H00HN11 */
               pr_default.execute(9, new Object[] {AV7ContratoGestor_ContratoCod, AV8ContratoGestor_UsuarioCod});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A1079ContratoGestor_UsuarioCod = H00HN11_A1079ContratoGestor_UsuarioCod[0];
                  A1078ContratoGestor_ContratoCod = H00HN11_A1078ContratoGestor_ContratoCod[0];
                  AV11ContratoGestor = new SdtContratoGestor(context);
                  AV11ContratoGestor.Load(AV7ContratoGestor_ContratoCod, AV8ContratoGestor_UsuarioCod);
                  if ( AV11ContratoGestor.Success() )
                  {
                     AV11ContratoGestor.Delete();
                  }
                  if ( ! AV11ContratoGestor.Success() )
                  {
                     AV12Success = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(9);
            }
            AV55GXV2 = (int)(AV55GXV2+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociarContrato_Gestor");
            /* Execute user subroutine: 'TITULARSUBSTITUTO' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociarContrato_Gestor");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E14HN2( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV34All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34All", AV34All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35jqAssociados", AV35jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37jqNaoAssociados", AV37jqNaoAssociados);
      }

      protected void E15HN2( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV34All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34All", AV34All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37jqNaoAssociados", AV37jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35jqAssociados", AV35jqAssociados);
      }

      protected void E16HN2( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV34All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34All", AV34All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37jqNaoAssociados", AV37jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35jqAssociados", AV35jqAssociados);
      }

      protected void E17HN2( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV34All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34All", AV34All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35jqAssociados", AV35jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37jqNaoAssociados", AV37jqNaoAssociados);
      }

      protected void S122( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV58GXV4 = 1;
         AV57GXV3 = AV39MenuPerfil.GetMessages();
         while ( AV58GXV4 <= AV57GXV3.Count )
         {
            AV15Message = ((SdtMessages_Message)AV57GXV3.Item(AV58GXV4));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV58GXV4 = (int)(AV58GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV37jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV37jqNaoAssociados.Item(AV14i)).gxTpr_Selected || AV34All )
            {
               ((SdtjqSelectData_Item)AV37jqNaoAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV35jqAssociados.Add(((SdtjqSelectData_Item)AV37jqNaoAssociados.Item(AV14i)), 0);
               AV37jqNaoAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV35jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV35jqAssociados.Item(AV14i)).gxTpr_Selected || AV34All )
            {
               ((SdtjqSelectData_Item)AV35jqAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV37jqNaoAssociados.Add(((SdtjqSelectData_Item)AV35jqAssociados.Item(AV14i)), 0);
               AV35jqAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV37jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV35jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void S112( )
      {
         /* 'TITULARSUBSTITUTO' Routine */
         AV14i = 0;
         /* Using cursor H00HN12 */
         pr_default.execute(10, new Object[] {AV7ContratoGestor_ContratoCod});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1078ContratoGestor_ContratoCod = H00HN12_A1078ContratoGestor_ContratoCod[0];
            A2009ContratoGestor_Tipo = H00HN12_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = H00HN12_n2009ContratoGestor_Tipo[0];
            A1079ContratoGestor_UsuarioCod = H00HN12_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00HN12_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00HN12_n1446ContratoGestor_ContratadaAreaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00HN12_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00HN12_n1446ContratoGestor_ContratadaAreaCod[0];
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
            if ( A1135ContratoGestor_UsuarioEhContratante )
            {
               AV42Codigo = A1079ContratoGestor_UsuarioCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Codigo), 6, 0)));
               AV43ContratoGestor_Tipo = A2009ContratoGestor_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGestor_Tipo", StringUtil.Str( (decimal)(AV43ContratoGestor_Tipo), 1, 0));
               AV14i = (int)(AV14i+1);
            }
            pr_default.readNext(10);
         }
         pr_default.close(10);
         if ( AV14i > 1 )
         {
            AV14i = 0;
            /* Using cursor H00HN13 */
            pr_default.execute(11, new Object[] {AV7ContratoGestor_ContratoCod});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A2009ContratoGestor_Tipo = H00HN13_A2009ContratoGestor_Tipo[0];
               n2009ContratoGestor_Tipo = H00HN13_n2009ContratoGestor_Tipo[0];
               A1078ContratoGestor_ContratoCod = H00HN13_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00HN13_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00HN13_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00HN13_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00HN13_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00HN13_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV42Codigo = A1079ContratoGestor_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Codigo), 6, 0)));
                  AV14i = (int)(AV14i+1);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(11);
            }
            pr_default.close(11);
            if ( AV14i == 1 )
            {
               AV14i = 0;
               /* Using cursor H00HN14 */
               pr_default.execute(12, new Object[] {AV7ContratoGestor_ContratoCod});
               while ( (pr_default.getStatus(12) != 101) )
               {
                  A2009ContratoGestor_Tipo = H00HN14_A2009ContratoGestor_Tipo[0];
                  n2009ContratoGestor_Tipo = H00HN14_n2009ContratoGestor_Tipo[0];
                  A1078ContratoGestor_ContratoCod = H00HN14_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00HN14_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00HN14_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00HN14_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00HN14_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00HN14_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV42Codigo = A1079ContratoGestor_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Codigo), 6, 0)));
                     AV14i = (int)(AV14i+1);
                  }
                  pr_default.readNext(12);
               }
               pr_default.close(12);
               if ( AV14i == 1 )
               {
                  AV43ContratoGestor_Tipo = 2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGestor_Tipo", StringUtil.Str( (decimal)(AV43ContratoGestor_Tipo), 1, 0));
                  /* Execute user subroutine: 'SETTIPO' */
                  S162 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
         }
         else
         {
            if ( (0==AV43ContratoGestor_Tipo) )
            {
               AV43ContratoGestor_Tipo = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGestor_Tipo", StringUtil.Str( (decimal)(AV43ContratoGestor_Tipo), 1, 0));
               /* Execute user subroutine: 'SETTIPO' */
               S162 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
      }

      protected void S162( )
      {
         /* 'SETTIPO' Routine */
         AV11ContratoGestor.Load(AV7ContratoGestor_ContratoCod, AV42Codigo);
         AV11ContratoGestor.gxTpr_Contratogestor_tipo = AV43ContratoGestor_Tipo;
         AV11ContratoGestor.Save();
         context.CommitDataStores( "WP_AssociarContrato_Gestor");
      }

      protected void nextLoad( )
      {
      }

      protected void E18HN2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_HN2( true) ;
         }
         else
         {
            wb_table2_8_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_18_HN2( true) ;
         }
         else
         {
            wb_table3_18_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_50_HN2( true) ;
         }
         else
         {
            wb_table4_50_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_50_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HN2e( true) ;
         }
         else
         {
            wb_table1_2_HN2e( false) ;
         }
      }

      protected void wb_table4_50_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_50_HN2e( true) ;
         }
         else
         {
            wb_table4_50_HN2e( false) ;
         }
      }

      protected void wb_table3_18_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_21_HN2( true) ;
         }
         else
         {
            wb_table5_21_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_HN2e( true) ;
         }
         else
         {
            wb_table3_18_HN2e( false) ;
         }
      }

      protected void wb_table5_21_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_33_HN2( true) ;
         }
         else
         {
            wb_table6_33_HN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_33_HN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_HN2e( true) ;
         }
         else
         {
            wb_table5_21_HN2e( false) ;
         }
      }

      protected void wb_table6_33_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_33_HN2e( true) ;
         }
         else
         {
            wb_table6_33_HN2e( false) ;
         }
      }

      protected void wb_table2_8_HN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Gestores ao Contrato :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinativo_Internalname, " (Inativo)", "", "", lblTbinativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", lblTbinativo_Visible, 1, 0, "HLP_WP_AssociarContrato_Gestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_HN2e( true) ;
         }
         else
         {
            wb_table2_8_HN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoGestor_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoGestor_ContratoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOGESTOR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGestor_ContratoCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHN2( ) ;
         WSHN2( ) ;
         WEHN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299394346");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcontrato_gestor.js", "?20205299394347");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTbinativo_Internalname = "TBINATIVO";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbinativo_Visible = 1;
         edtContrato_Numero_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         lblAssociatedrecordstitle_Caption = "Usu�rios Associados";
         lblNotassociatedrecordstitle_Caption = "Usu�rios N�o Associados";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 300;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E13HN2',iparms:[{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7ContratoGestor_ContratoCod',fld:'vCONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A2009ContratoGestor_Tipo',fld:'CONTRATOGESTOR_TIPO',pic:'9',nv:0},{av:'AV39MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null},{av:'AV42Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ContratoGestor_Tipo',fld:'vCONTRATOGESTOR_TIPO',pic:'9',nv:0},{av:'AV8ContratoGestor_UsuarioCod',fld:'vCONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8ContratoGestor_UsuarioCod',fld:'vCONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV42Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ContratoGestor_Tipo',fld:'vCONTRATOGESTOR_TIPO',pic:'9',nv:0}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14HN2',iparms:[{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15HN2',iparms:[{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16HN2',iparms:[{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17HN2',iparms:[{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV34All',fld:'vALL',pic:'',nv:false},{av:'AV35jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV37jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV37jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV35jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV39MenuPerfil = new SdtMenuPerfil(context);
         A77Contrato_Numero = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00HN2_A74Contrato_Codigo = new int[1] ;
         H00HN2_A77Contrato_Numero = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         H00HN3_A74Contrato_Codigo = new int[1] ;
         H00HN3_A92Contrato_Ativo = new bool[] {false} ;
         H00HN4_A5AreaTrabalho_Codigo = new int[1] ;
         H00HN4_A29Contratante_Codigo = new int[1] ;
         H00HN4_n29Contratante_Codigo = new bool[] {false} ;
         H00HN5_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00HN5_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00HN5_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00HN5_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00HN5_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00HN5_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         AV40Tipo = "";
         H00HN6_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN6_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00HN6_A2009ContratoGestor_Tipo = new short[1] ;
         H00HN6_n2009ContratoGestor_Tipo = new bool[] {false} ;
         AV36jqItem = new SdtjqSelectData_Item(context);
         AV20Codigos = new GxSimpleCollection();
         H00HN7_A74Contrato_Codigo = new int[1] ;
         H00HN7_A39Contratada_Codigo = new int[1] ;
         H00HN8_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00HN8_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00HN8_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00HN8_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00HN8_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00HN8_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00HN9_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN9_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00HN9_A2009ContratoGestor_Tipo = new short[1] ;
         H00HN9_n2009ContratoGestor_Tipo = new bool[] {false} ;
         H00HN10_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN10_A1078ContratoGestor_ContratoCod = new int[1] ;
         AV11ContratoGestor = new SdtContratoGestor(context);
         H00HN11_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN11_A1078ContratoGestor_ContratoCod = new int[1] ;
         AV57GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         H00HN12_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00HN12_A2009ContratoGestor_Tipo = new short[1] ;
         H00HN12_n2009ContratoGestor_Tipo = new bool[] {false} ;
         H00HN12_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN12_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00HN12_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00HN13_A2009ContratoGestor_Tipo = new short[1] ;
         H00HN13_n2009ContratoGestor_Tipo = new bool[] {false} ;
         H00HN13_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00HN13_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN13_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00HN13_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00HN14_A2009ContratoGestor_Tipo = new short[1] ;
         H00HN14_n2009ContratoGestor_Tipo = new bool[] {false} ;
         H00HN14_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00HN14_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00HN14_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00HN14_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         lblTbinativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcontrato_gestor__default(),
            new Object[][] {
                new Object[] {
               H00HN2_A74Contrato_Codigo, H00HN2_A77Contrato_Numero
               }
               , new Object[] {
               H00HN3_A74Contrato_Codigo, H00HN3_A92Contrato_Ativo
               }
               , new Object[] {
               H00HN4_A5AreaTrabalho_Codigo, H00HN4_A29Contratante_Codigo, H00HN4_n29Contratante_Codigo
               }
               , new Object[] {
               H00HN5_A61ContratanteUsuario_UsuarioPessoaCod, H00HN5_n61ContratanteUsuario_UsuarioPessoaCod, H00HN5_A63ContratanteUsuario_ContratanteCod, H00HN5_A60ContratanteUsuario_UsuarioCod, H00HN5_A62ContratanteUsuario_UsuarioPessoaNom, H00HN5_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00HN6_A1079ContratoGestor_UsuarioCod, H00HN6_A1078ContratoGestor_ContratoCod, H00HN6_A2009ContratoGestor_Tipo, H00HN6_n2009ContratoGestor_Tipo
               }
               , new Object[] {
               H00HN7_A74Contrato_Codigo, H00HN7_A39Contratada_Codigo
               }
               , new Object[] {
               H00HN8_A70ContratadaUsuario_UsuarioPessoaCod, H00HN8_n70ContratadaUsuario_UsuarioPessoaCod, H00HN8_A66ContratadaUsuario_ContratadaCod, H00HN8_A69ContratadaUsuario_UsuarioCod, H00HN8_A71ContratadaUsuario_UsuarioPessoaNom, H00HN8_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00HN9_A1079ContratoGestor_UsuarioCod, H00HN9_A1078ContratoGestor_ContratoCod, H00HN9_A2009ContratoGestor_Tipo, H00HN9_n2009ContratoGestor_Tipo
               }
               , new Object[] {
               H00HN10_A1079ContratoGestor_UsuarioCod, H00HN10_A1078ContratoGestor_ContratoCod
               }
               , new Object[] {
               H00HN11_A1079ContratoGestor_UsuarioCod, H00HN11_A1078ContratoGestor_ContratoCod
               }
               , new Object[] {
               H00HN12_A1078ContratoGestor_ContratoCod, H00HN12_A2009ContratoGestor_Tipo, H00HN12_n2009ContratoGestor_Tipo, H00HN12_A1079ContratoGestor_UsuarioCod, H00HN12_A1446ContratoGestor_ContratadaAreaCod, H00HN12_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00HN13_A2009ContratoGestor_Tipo, H00HN13_n2009ContratoGestor_Tipo, H00HN13_A1078ContratoGestor_ContratoCod, H00HN13_A1079ContratoGestor_UsuarioCod, H00HN13_A1446ContratoGestor_ContratadaAreaCod, H00HN13_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00HN14_A2009ContratoGestor_Tipo, H00HN14_n2009ContratoGestor_Tipo, H00HN14_A1078ContratoGestor_ContratoCod, H00HN14_A1079ContratoGestor_UsuarioCod, H00HN14_A1446ContratoGestor_ContratadaAreaCod, H00HN14_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_11 ;
      private short nIsMod_11 ;
      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short A2009ContratoGestor_Tipo ;
      private short AV43ContratoGestor_Tipo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV46GXLvl5 ;
      private short AV54GXLvl98 ;
      private short nGXWrapped ;
      private int AV7ContratoGestor_ContratoCod ;
      private int wcpOAV7ContratoGestor_ContratoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV42Codigo ;
      private int AV8ContratoGestor_UsuarioCod ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int A74Contrato_Codigo ;
      private int lblTbinativo_Visible ;
      private int bttBtn_confirm_Visible ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int AV27Contratante_Codigo ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A39Contratada_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int AV53GXV1 ;
      private int AV55GXV2 ;
      private int AV58GXV4 ;
      private int AV14i ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContrato_Numero_Internalname ;
      private String lblTbinativo_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String AV40Tipo ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTbinativo_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool AV34All ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A92Contrato_Ativo ;
      private bool n29Contratante_Codigo ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool AV10Exist ;
      private bool n2009ContratoGestor_Tipo ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool AV12Success ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool GXt_boolean1 ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00HN2_A74Contrato_Codigo ;
      private String[] H00HN2_A77Contrato_Numero ;
      private int[] H00HN3_A74Contrato_Codigo ;
      private bool[] H00HN3_A92Contrato_Ativo ;
      private int[] H00HN4_A5AreaTrabalho_Codigo ;
      private int[] H00HN4_A29Contratante_Codigo ;
      private bool[] H00HN4_n29Contratante_Codigo ;
      private int[] H00HN5_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00HN5_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00HN5_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00HN5_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00HN5_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00HN5_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00HN6_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN6_A1078ContratoGestor_ContratoCod ;
      private short[] H00HN6_A2009ContratoGestor_Tipo ;
      private bool[] H00HN6_n2009ContratoGestor_Tipo ;
      private int[] H00HN7_A74Contrato_Codigo ;
      private int[] H00HN7_A39Contratada_Codigo ;
      private int[] H00HN8_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00HN8_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00HN8_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00HN8_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00HN8_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00HN8_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00HN9_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN9_A1078ContratoGestor_ContratoCod ;
      private short[] H00HN9_A2009ContratoGestor_Tipo ;
      private bool[] H00HN9_n2009ContratoGestor_Tipo ;
      private int[] H00HN10_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN10_A1078ContratoGestor_ContratoCod ;
      private int[] H00HN11_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN11_A1078ContratoGestor_ContratoCod ;
      private int[] H00HN12_A1078ContratoGestor_ContratoCod ;
      private short[] H00HN12_A2009ContratoGestor_Tipo ;
      private bool[] H00HN12_n2009ContratoGestor_Tipo ;
      private int[] H00HN12_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN12_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00HN12_n1446ContratoGestor_ContratadaAreaCod ;
      private short[] H00HN13_A2009ContratoGestor_Tipo ;
      private bool[] H00HN13_n2009ContratoGestor_Tipo ;
      private int[] H00HN13_A1078ContratoGestor_ContratoCod ;
      private int[] H00HN13_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN13_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00HN13_n1446ContratoGestor_ContratadaAreaCod ;
      private short[] H00HN14_A2009ContratoGestor_Tipo ;
      private bool[] H00HN14_n2009ContratoGestor_Tipo ;
      private int[] H00HN14_A1078ContratoGestor_ContratoCod ;
      private int[] H00HN14_A1079ContratoGestor_UsuarioCod ;
      private int[] H00HN14_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00HN14_n1446ContratoGestor_ContratadaAreaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Codigos ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV57GXV3 ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV37jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV35jqAssociados ;
      private SdtMenuPerfil AV39MenuPerfil ;
      private SdtMessages_Message AV15Message ;
      private SdtContratoGestor AV11ContratoGestor ;
      private SdtjqSelectData_Item AV36jqItem ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_associarcontrato_gestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00HN8( IGxContext context ,
                                             int A69ContratadaUsuario_UsuarioCod ,
                                             IGxCollection AV20Codigos ,
                                             int A66ContratadaUsuario_ContratadaCod ,
                                             int A39Contratada_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @Contratada_Codigo)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV20Codigos, "T1.[ContratadaUsuario_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H00HN8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HN2 ;
          prmH00HN2 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN3 ;
          prmH00HN3 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN4 ;
          prmH00HN4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN5 ;
          prmH00HN5 = new Object[] {
          new Object[] {"@AV27Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN6 ;
          prmH00HN6 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN7 ;
          prmH00HN7 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN9 ;
          prmH00HN9 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN10 ;
          prmH00HN10 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN11 ;
          prmH00HN11 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN12 ;
          prmH00HN12 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN13 ;
          prmH00HN13 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN14 ;
          prmH00HN14 = new Object[] {
          new Object[] {"@AV7ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HN8 ;
          prmH00HN8 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HN2", "SELECT [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7ContratoGestor_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN2,1,0,true,true )
             ,new CursorDef("H00HN3", "SELECT TOP 1 [Contrato_Codigo], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7ContratoGestor_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN3,1,0,false,true )
             ,new CursorDef("H00HN4", "SELECT TOP 1 [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN4,1,0,false,true )
             ,new CursorDef("H00HN5", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV27Contratante_Codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN5,100,0,true,false )
             ,new CursorDef("H00HN6", "SELECT TOP 1 [ContratoGestor_UsuarioCod], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_Tipo] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV8ContratoGestor_UsuarioCod ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN6,1,0,false,true )
             ,new CursorDef("H00HN7", "SELECT [Contrato_Codigo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV7ContratoGestor_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN7,1,0,true,true )
             ,new CursorDef("H00HN8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN8,100,0,true,false )
             ,new CursorDef("H00HN9", "SELECT TOP 1 [ContratoGestor_UsuarioCod], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_Tipo] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV8ContratoGestor_UsuarioCod ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN9,1,0,false,true )
             ,new CursorDef("H00HN10", "SELECT TOP 1 [ContratoGestor_UsuarioCod], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV8ContratoGestor_UsuarioCod ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN10,1,0,false,true )
             ,new CursorDef("H00HN11", "SELECT TOP 1 [ContratoGestor_UsuarioCod], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV8ContratoGestor_UsuarioCod ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN11,1,0,true,true )
             ,new CursorDef("H00HN12", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_Tipo], T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN12,100,0,true,false )
             ,new CursorDef("H00HN13", "SELECT T1.[ContratoGestor_Tipo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod) AND (T1.[ContratoGestor_Tipo] = 1) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN13,100,0,true,false )
             ,new CursorDef("H00HN14", "SELECT T1.[ContratoGestor_Tipo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @AV7ContratoGestor_ContratoCod) AND (T1.[ContratoGestor_Tipo] IS NULL or (T1.[ContratoGestor_Tipo] = convert(int, 0))) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HN14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
