/*
               File: LogErroBC
        Description: Log Erro BC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:53.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class logerrobc : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Log Erro BC", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtLogErroBCID_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public logerrobc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public logerrobc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_47186( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_47186e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_47186( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_47186( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_47186e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Log Erro BC", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_LogErroBC.htm");
            wb_table3_28_47186( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_47186e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_47186e( true) ;
         }
         else
         {
            wb_table1_2_47186e( false) ;
         }
      }

      protected void wb_table3_28_47186( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_47186( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_47186e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogErroBC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogErroBC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_47186e( true) ;
         }
         else
         {
            wb_table3_28_47186e( false) ;
         }
      }

      protected void wb_table4_34_47186( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcid_Internalname, "Erro BCID", "", "", lblTextblocklogerrobcid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogErroBCID_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1668LogErroBCID), 18, 0, ",", "")), ((edtLogErroBCID_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1668LogErroBCID), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1668LogErroBCID), "ZZZZZZZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogErroBCID_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogErroBCID_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcmodulo_Internalname, "Erro BCModulo", "", "", lblTextblocklogerrobcmodulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogErroBCModulo_Internalname, A1680LogErroBCModulo, StringUtil.RTrim( context.localUtil.Format( A1680LogErroBCModulo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogErroBCModulo_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogErroBCModulo_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcdata_Internalname, "Erro BCData", "", "", lblTextblocklogerrobcdata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLogErroBCData_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLogErroBCData_Internalname, context.localUtil.Format(A1681LogErroBCData, "99/99/99"), context.localUtil.Format( A1681LogErroBCData, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogErroBCData_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogErroBCData_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_LogErroBC.htm");
            GxWebStd.gx_bitmap( context, edtLogErroBCData_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLogErroBCData_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LogErroBC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcnomebc_Internalname, "BCNome BC", "", "", lblTextblocklogerrobcnomebc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogErroBCNomeBC_Internalname, A1682LogErroBCNomeBC, StringUtil.RTrim( context.localUtil.Format( A1682LogErroBCNomeBC, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogErroBCNomeBC_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogErroBCNomeBC_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcjsonbc_Internalname, "BCJson BC", "", "", lblTextblocklogerrobcjsonbc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLogErroBCJsonBC_Internalname, A1683LogErroBCJsonBC, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtLogErroBCJsonBC_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogerrobcjasonmessage_Internalname, "BCJason Message", "", "", lblTextblocklogerrobcjasonmessage_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLogErroBCJasonMessage_Internalname, A1684LogErroBCJasonMessage, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, 1, edtLogErroBCJasonMessage_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_LogErroBC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_47186e( true) ;
         }
         else
         {
            wb_table4_34_47186e( false) ;
         }
      }

      protected void wb_table2_5_47186( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogErroBC.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_47186e( true) ;
         }
         else
         {
            wb_table2_5_47186e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtLogErroBCID_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLogErroBCID_Internalname), ",", ".") > Convert.ToDecimal( 999999999999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOGERROBCID");
                  AnyError = 1;
                  GX_FocusControl = edtLogErroBCID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1668LogErroBCID = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
               }
               else
               {
                  A1668LogErroBCID = (long)(context.localUtil.CToN( cgiGet( edtLogErroBCID_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
               }
               A1680LogErroBCModulo = cgiGet( edtLogErroBCModulo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1680LogErroBCModulo", A1680LogErroBCModulo);
               if ( context.localUtil.VCDate( cgiGet( edtLogErroBCData_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Log Erro BCData"}), 1, "LOGERROBCDATA");
                  AnyError = 1;
                  GX_FocusControl = edtLogErroBCData_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1681LogErroBCData = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1681LogErroBCData", context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
               }
               else
               {
                  A1681LogErroBCData = context.localUtil.CToD( cgiGet( edtLogErroBCData_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1681LogErroBCData", context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
               }
               A1682LogErroBCNomeBC = cgiGet( edtLogErroBCNomeBC_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1682LogErroBCNomeBC", A1682LogErroBCNomeBC);
               A1683LogErroBCJsonBC = cgiGet( edtLogErroBCJsonBC_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1683LogErroBCJsonBC", A1683LogErroBCJsonBC);
               A1684LogErroBCJasonMessage = cgiGet( edtLogErroBCJasonMessage_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1684LogErroBCJasonMessage", A1684LogErroBCJasonMessage);
               /* Read saved values. */
               Z1668LogErroBCID = (long)(context.localUtil.CToN( cgiGet( "Z1668LogErroBCID"), ",", "."));
               Z1680LogErroBCModulo = cgiGet( "Z1680LogErroBCModulo");
               Z1681LogErroBCData = context.localUtil.CToD( cgiGet( "Z1681LogErroBCData"), 0);
               Z1682LogErroBCNomeBC = cgiGet( "Z1682LogErroBCNomeBC");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1668LogErroBCID = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll47186( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes47186( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption470( )
      {
      }

      protected void ZM47186( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1680LogErroBCModulo = T00473_A1680LogErroBCModulo[0];
               Z1681LogErroBCData = T00473_A1681LogErroBCData[0];
               Z1682LogErroBCNomeBC = T00473_A1682LogErroBCNomeBC[0];
            }
            else
            {
               Z1680LogErroBCModulo = A1680LogErroBCModulo;
               Z1681LogErroBCData = A1681LogErroBCData;
               Z1682LogErroBCNomeBC = A1682LogErroBCNomeBC;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1668LogErroBCID = A1668LogErroBCID;
            Z1680LogErroBCModulo = A1680LogErroBCModulo;
            Z1681LogErroBCData = A1681LogErroBCData;
            Z1682LogErroBCNomeBC = A1682LogErroBCNomeBC;
            Z1683LogErroBCJsonBC = A1683LogErroBCJsonBC;
            Z1684LogErroBCJasonMessage = A1684LogErroBCJasonMessage;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load47186( )
      {
         /* Using cursor T00474 */
         pr_default.execute(2, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound186 = 1;
            A1680LogErroBCModulo = T00474_A1680LogErroBCModulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1680LogErroBCModulo", A1680LogErroBCModulo);
            A1681LogErroBCData = T00474_A1681LogErroBCData[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1681LogErroBCData", context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
            A1682LogErroBCNomeBC = T00474_A1682LogErroBCNomeBC[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1682LogErroBCNomeBC", A1682LogErroBCNomeBC);
            A1683LogErroBCJsonBC = T00474_A1683LogErroBCJsonBC[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1683LogErroBCJsonBC", A1683LogErroBCJsonBC);
            A1684LogErroBCJasonMessage = T00474_A1684LogErroBCJasonMessage[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1684LogErroBCJasonMessage", A1684LogErroBCJasonMessage);
            ZM47186( -2) ;
         }
         pr_default.close(2);
         OnLoadActions47186( ) ;
      }

      protected void OnLoadActions47186( )
      {
      }

      protected void CheckExtendedTable47186( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1681LogErroBCData) || ( A1681LogErroBCData >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Log Erro BCData fora do intervalo", "OutOfRange", 1, "LOGERROBCDATA");
            AnyError = 1;
            GX_FocusControl = edtLogErroBCData_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors47186( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey47186( )
      {
         /* Using cursor T00475 */
         pr_default.execute(3, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound186 = 1;
         }
         else
         {
            RcdFound186 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00473 */
         pr_default.execute(1, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM47186( 2) ;
            RcdFound186 = 1;
            A1668LogErroBCID = T00473_A1668LogErroBCID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
            A1680LogErroBCModulo = T00473_A1680LogErroBCModulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1680LogErroBCModulo", A1680LogErroBCModulo);
            A1681LogErroBCData = T00473_A1681LogErroBCData[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1681LogErroBCData", context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
            A1682LogErroBCNomeBC = T00473_A1682LogErroBCNomeBC[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1682LogErroBCNomeBC", A1682LogErroBCNomeBC);
            A1683LogErroBCJsonBC = T00473_A1683LogErroBCJsonBC[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1683LogErroBCJsonBC", A1683LogErroBCJsonBC);
            A1684LogErroBCJasonMessage = T00473_A1684LogErroBCJasonMessage[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1684LogErroBCJasonMessage", A1684LogErroBCJasonMessage);
            Z1668LogErroBCID = A1668LogErroBCID;
            sMode186 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load47186( ) ;
            if ( AnyError == 1 )
            {
               RcdFound186 = 0;
               InitializeNonKey47186( ) ;
            }
            Gx_mode = sMode186;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound186 = 0;
            InitializeNonKey47186( ) ;
            sMode186 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode186;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey47186( ) ;
         if ( RcdFound186 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound186 = 0;
         /* Using cursor T00476 */
         pr_default.execute(4, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00476_A1668LogErroBCID[0] < A1668LogErroBCID ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00476_A1668LogErroBCID[0] > A1668LogErroBCID ) ) )
            {
               A1668LogErroBCID = T00476_A1668LogErroBCID[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
               RcdFound186 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound186 = 0;
         /* Using cursor T00477 */
         pr_default.execute(5, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00477_A1668LogErroBCID[0] > A1668LogErroBCID ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00477_A1668LogErroBCID[0] < A1668LogErroBCID ) ) )
            {
               A1668LogErroBCID = T00477_A1668LogErroBCID[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
               RcdFound186 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey47186( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtLogErroBCID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert47186( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound186 == 1 )
            {
               if ( A1668LogErroBCID != Z1668LogErroBCID )
               {
                  A1668LogErroBCID = Z1668LogErroBCID;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "LOGERROBCID");
                  AnyError = 1;
                  GX_FocusControl = edtLogErroBCID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtLogErroBCID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update47186( ) ;
                  GX_FocusControl = edtLogErroBCID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1668LogErroBCID != Z1668LogErroBCID )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtLogErroBCID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert47186( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "LOGERROBCID");
                     AnyError = 1;
                     GX_FocusControl = edtLogErroBCID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtLogErroBCID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert47186( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1668LogErroBCID != Z1668LogErroBCID )
         {
            A1668LogErroBCID = Z1668LogErroBCID;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "LOGERROBCID");
            AnyError = 1;
            GX_FocusControl = edtLogErroBCID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtLogErroBCID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "LOGERROBCID");
            AnyError = 1;
            GX_FocusControl = edtLogErroBCID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart47186( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd47186( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart47186( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound186 != 0 )
            {
               ScanNext47186( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd47186( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency47186( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00472 */
            pr_default.execute(0, new Object[] {A1668LogErroBCID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogErroBC"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1680LogErroBCModulo, T00472_A1680LogErroBCModulo[0]) != 0 ) || ( Z1681LogErroBCData != T00472_A1681LogErroBCData[0] ) || ( StringUtil.StrCmp(Z1682LogErroBCNomeBC, T00472_A1682LogErroBCNomeBC[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1680LogErroBCModulo, T00472_A1680LogErroBCModulo[0]) != 0 )
               {
                  GXUtil.WriteLog("logerrobc:[seudo value changed for attri]"+"LogErroBCModulo");
                  GXUtil.WriteLogRaw("Old: ",Z1680LogErroBCModulo);
                  GXUtil.WriteLogRaw("Current: ",T00472_A1680LogErroBCModulo[0]);
               }
               if ( Z1681LogErroBCData != T00472_A1681LogErroBCData[0] )
               {
                  GXUtil.WriteLog("logerrobc:[seudo value changed for attri]"+"LogErroBCData");
                  GXUtil.WriteLogRaw("Old: ",Z1681LogErroBCData);
                  GXUtil.WriteLogRaw("Current: ",T00472_A1681LogErroBCData[0]);
               }
               if ( StringUtil.StrCmp(Z1682LogErroBCNomeBC, T00472_A1682LogErroBCNomeBC[0]) != 0 )
               {
                  GXUtil.WriteLog("logerrobc:[seudo value changed for attri]"+"LogErroBCNomeBC");
                  GXUtil.WriteLogRaw("Old: ",Z1682LogErroBCNomeBC);
                  GXUtil.WriteLogRaw("Current: ",T00472_A1682LogErroBCNomeBC[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LogErroBC"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert47186( )
      {
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable47186( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM47186( 0) ;
            CheckOptimisticConcurrency47186( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm47186( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert47186( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00478 */
                     pr_default.execute(6, new Object[] {A1668LogErroBCID, A1680LogErroBCModulo, A1681LogErroBCData, A1682LogErroBCNomeBC, A1683LogErroBCJsonBC, A1684LogErroBCJasonMessage});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption470( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load47186( ) ;
            }
            EndLevel47186( ) ;
         }
         CloseExtendedTableCursors47186( ) ;
      }

      protected void Update47186( )
      {
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable47186( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency47186( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm47186( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate47186( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00479 */
                     pr_default.execute(7, new Object[] {A1680LogErroBCModulo, A1681LogErroBCData, A1682LogErroBCNomeBC, A1683LogErroBCJsonBC, A1684LogErroBCJasonMessage, A1668LogErroBCID});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogErroBC"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate47186( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption470( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel47186( ) ;
         }
         CloseExtendedTableCursors47186( ) ;
      }

      protected void DeferredUpdate47186( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency47186( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls47186( ) ;
            AfterConfirm47186( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete47186( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004710 */
                  pr_default.execute(8, new Object[] {A1668LogErroBCID});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound186 == 0 )
                        {
                           InitAll47186( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption470( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode186 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel47186( ) ;
         Gx_mode = sMode186;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls47186( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel47186( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete47186( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "LogErroBC");
            if ( AnyError == 0 )
            {
               ConfirmValues470( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "LogErroBC");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart47186( )
      {
         /* Using cursor T004711 */
         pr_default.execute(9);
         RcdFound186 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound186 = 1;
            A1668LogErroBCID = T004711_A1668LogErroBCID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext47186( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound186 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound186 = 1;
            A1668LogErroBCID = T004711_A1668LogErroBCID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
         }
      }

      protected void ScanEnd47186( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm47186( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert47186( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate47186( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete47186( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete47186( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate47186( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes47186( )
      {
         edtLogErroBCID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCID_Enabled), 5, 0)));
         edtLogErroBCModulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCModulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCModulo_Enabled), 5, 0)));
         edtLogErroBCData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCData_Enabled), 5, 0)));
         edtLogErroBCNomeBC_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCNomeBC_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCNomeBC_Enabled), 5, 0)));
         edtLogErroBCJsonBC_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCJsonBC_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCJsonBC_Enabled), 5, 0)));
         edtLogErroBCJasonMessage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogErroBCJasonMessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogErroBCJasonMessage_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues470( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282385444");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("logerrobc.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1668LogErroBCID", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1668LogErroBCID), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1680LogErroBCModulo", Z1680LogErroBCModulo);
         GxWebStd.gx_hidden_field( context, "Z1681LogErroBCData", context.localUtil.DToC( Z1681LogErroBCData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1682LogErroBCNomeBC", Z1682LogErroBCNomeBC);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("logerrobc.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "LogErroBC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Log Erro BC" ;
      }

      protected void InitializeNonKey47186( )
      {
         A1680LogErroBCModulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1680LogErroBCModulo", A1680LogErroBCModulo);
         A1681LogErroBCData = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1681LogErroBCData", context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
         A1682LogErroBCNomeBC = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1682LogErroBCNomeBC", A1682LogErroBCNomeBC);
         A1683LogErroBCJsonBC = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1683LogErroBCJsonBC", A1683LogErroBCJsonBC);
         A1684LogErroBCJasonMessage = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1684LogErroBCJasonMessage", A1684LogErroBCJasonMessage);
         Z1680LogErroBCModulo = "";
         Z1681LogErroBCData = DateTime.MinValue;
         Z1682LogErroBCNomeBC = "";
      }

      protected void InitAll47186( )
      {
         A1668LogErroBCID = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1668LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(A1668LogErroBCID), 18, 0)));
         InitializeNonKey47186( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282385448");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("logerrobc.js", "?20204282385448");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocklogerrobcid_Internalname = "TEXTBLOCKLOGERROBCID";
         edtLogErroBCID_Internalname = "LOGERROBCID";
         lblTextblocklogerrobcmodulo_Internalname = "TEXTBLOCKLOGERROBCMODULO";
         edtLogErroBCModulo_Internalname = "LOGERROBCMODULO";
         lblTextblocklogerrobcdata_Internalname = "TEXTBLOCKLOGERROBCDATA";
         edtLogErroBCData_Internalname = "LOGERROBCDATA";
         lblTextblocklogerrobcnomebc_Internalname = "TEXTBLOCKLOGERROBCNOMEBC";
         edtLogErroBCNomeBC_Internalname = "LOGERROBCNOMEBC";
         lblTextblocklogerrobcjsonbc_Internalname = "TEXTBLOCKLOGERROBCJSONBC";
         edtLogErroBCJsonBC_Internalname = "LOGERROBCJSONBC";
         lblTextblocklogerrobcjasonmessage_Internalname = "TEXTBLOCKLOGERROBCJASONMESSAGE";
         edtLogErroBCJasonMessage_Internalname = "LOGERROBCJASONMESSAGE";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Log Erro BC";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtLogErroBCJasonMessage_Enabled = 1;
         edtLogErroBCJsonBC_Enabled = 1;
         edtLogErroBCNomeBC_Jsonclick = "";
         edtLogErroBCNomeBC_Enabled = 1;
         edtLogErroBCData_Jsonclick = "";
         edtLogErroBCData_Enabled = 1;
         edtLogErroBCModulo_Jsonclick = "";
         edtLogErroBCModulo_Enabled = 1;
         edtLogErroBCID_Jsonclick = "";
         edtLogErroBCID_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtLogErroBCModulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Logerrobcid( long GX_Parm1 ,
                                     String GX_Parm2 ,
                                     DateTime GX_Parm3 ,
                                     String GX_Parm4 ,
                                     String GX_Parm5 ,
                                     String GX_Parm6 )
      {
         A1668LogErroBCID = GX_Parm1;
         A1680LogErroBCModulo = GX_Parm2;
         A1681LogErroBCData = GX_Parm3;
         A1682LogErroBCNomeBC = GX_Parm4;
         A1683LogErroBCJsonBC = GX_Parm5;
         A1684LogErroBCJasonMessage = GX_Parm6;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(A1680LogErroBCModulo);
         isValidOutput.Add(context.localUtil.Format(A1681LogErroBCData, "99/99/99"));
         isValidOutput.Add(A1682LogErroBCNomeBC);
         isValidOutput.Add(A1683LogErroBCJsonBC);
         isValidOutput.Add(A1684LogErroBCJasonMessage);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1668LogErroBCID), 18, 0, ",", "")));
         isValidOutput.Add(Z1680LogErroBCModulo);
         isValidOutput.Add(context.localUtil.DToC( Z1681LogErroBCData, 0, "/"));
         isValidOutput.Add(Z1682LogErroBCNomeBC);
         isValidOutput.Add(Z1683LogErroBCJsonBC);
         isValidOutput.Add(Z1684LogErroBCJasonMessage);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1680LogErroBCModulo = "";
         Z1681LogErroBCData = DateTime.MinValue;
         Z1682LogErroBCNomeBC = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocklogerrobcid_Jsonclick = "";
         lblTextblocklogerrobcmodulo_Jsonclick = "";
         A1680LogErroBCModulo = "";
         lblTextblocklogerrobcdata_Jsonclick = "";
         A1681LogErroBCData = DateTime.MinValue;
         lblTextblocklogerrobcnomebc_Jsonclick = "";
         A1682LogErroBCNomeBC = "";
         lblTextblocklogerrobcjsonbc_Jsonclick = "";
         A1683LogErroBCJsonBC = "";
         lblTextblocklogerrobcjasonmessage_Jsonclick = "";
         A1684LogErroBCJasonMessage = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1683LogErroBCJsonBC = "";
         Z1684LogErroBCJasonMessage = "";
         T00474_A1668LogErroBCID = new long[1] ;
         T00474_A1680LogErroBCModulo = new String[] {""} ;
         T00474_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         T00474_A1682LogErroBCNomeBC = new String[] {""} ;
         T00474_A1683LogErroBCJsonBC = new String[] {""} ;
         T00474_A1684LogErroBCJasonMessage = new String[] {""} ;
         T00475_A1668LogErroBCID = new long[1] ;
         T00473_A1668LogErroBCID = new long[1] ;
         T00473_A1680LogErroBCModulo = new String[] {""} ;
         T00473_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         T00473_A1682LogErroBCNomeBC = new String[] {""} ;
         T00473_A1683LogErroBCJsonBC = new String[] {""} ;
         T00473_A1684LogErroBCJasonMessage = new String[] {""} ;
         sMode186 = "";
         T00476_A1668LogErroBCID = new long[1] ;
         T00477_A1668LogErroBCID = new long[1] ;
         T00472_A1668LogErroBCID = new long[1] ;
         T00472_A1680LogErroBCModulo = new String[] {""} ;
         T00472_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         T00472_A1682LogErroBCNomeBC = new String[] {""} ;
         T00472_A1683LogErroBCJsonBC = new String[] {""} ;
         T00472_A1684LogErroBCJasonMessage = new String[] {""} ;
         T004711_A1668LogErroBCID = new long[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.logerrobc__default(),
            new Object[][] {
                new Object[] {
               T00472_A1668LogErroBCID, T00472_A1680LogErroBCModulo, T00472_A1681LogErroBCData, T00472_A1682LogErroBCNomeBC, T00472_A1683LogErroBCJsonBC, T00472_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               T00473_A1668LogErroBCID, T00473_A1680LogErroBCModulo, T00473_A1681LogErroBCData, T00473_A1682LogErroBCNomeBC, T00473_A1683LogErroBCJsonBC, T00473_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               T00474_A1668LogErroBCID, T00474_A1680LogErroBCModulo, T00474_A1681LogErroBCData, T00474_A1682LogErroBCNomeBC, T00474_A1683LogErroBCJsonBC, T00474_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               T00475_A1668LogErroBCID
               }
               , new Object[] {
               T00476_A1668LogErroBCID
               }
               , new Object[] {
               T00477_A1668LogErroBCID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004711_A1668LogErroBCID
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound186 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtLogErroBCID_Enabled ;
      private int edtLogErroBCModulo_Enabled ;
      private int edtLogErroBCData_Enabled ;
      private int edtLogErroBCNomeBC_Enabled ;
      private int edtLogErroBCJsonBC_Enabled ;
      private int edtLogErroBCJasonMessage_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private long Z1668LogErroBCID ;
      private long A1668LogErroBCID ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtLogErroBCID_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocklogerrobcid_Internalname ;
      private String lblTextblocklogerrobcid_Jsonclick ;
      private String edtLogErroBCID_Jsonclick ;
      private String lblTextblocklogerrobcmodulo_Internalname ;
      private String lblTextblocklogerrobcmodulo_Jsonclick ;
      private String edtLogErroBCModulo_Internalname ;
      private String edtLogErroBCModulo_Jsonclick ;
      private String lblTextblocklogerrobcdata_Internalname ;
      private String lblTextblocklogerrobcdata_Jsonclick ;
      private String edtLogErroBCData_Internalname ;
      private String edtLogErroBCData_Jsonclick ;
      private String lblTextblocklogerrobcnomebc_Internalname ;
      private String lblTextblocklogerrobcnomebc_Jsonclick ;
      private String edtLogErroBCNomeBC_Internalname ;
      private String edtLogErroBCNomeBC_Jsonclick ;
      private String lblTextblocklogerrobcjsonbc_Internalname ;
      private String lblTextblocklogerrobcjsonbc_Jsonclick ;
      private String edtLogErroBCJsonBC_Internalname ;
      private String lblTextblocklogerrobcjasonmessage_Internalname ;
      private String lblTextblocklogerrobcjasonmessage_Jsonclick ;
      private String edtLogErroBCJasonMessage_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode186 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1681LogErroBCData ;
      private DateTime A1681LogErroBCData ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private String A1683LogErroBCJsonBC ;
      private String A1684LogErroBCJasonMessage ;
      private String Z1683LogErroBCJsonBC ;
      private String Z1684LogErroBCJasonMessage ;
      private String Z1680LogErroBCModulo ;
      private String Z1682LogErroBCNomeBC ;
      private String A1680LogErroBCModulo ;
      private String A1682LogErroBCNomeBC ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] T00474_A1668LogErroBCID ;
      private String[] T00474_A1680LogErroBCModulo ;
      private DateTime[] T00474_A1681LogErroBCData ;
      private String[] T00474_A1682LogErroBCNomeBC ;
      private String[] T00474_A1683LogErroBCJsonBC ;
      private String[] T00474_A1684LogErroBCJasonMessage ;
      private long[] T00475_A1668LogErroBCID ;
      private long[] T00473_A1668LogErroBCID ;
      private String[] T00473_A1680LogErroBCModulo ;
      private DateTime[] T00473_A1681LogErroBCData ;
      private String[] T00473_A1682LogErroBCNomeBC ;
      private String[] T00473_A1683LogErroBCJsonBC ;
      private String[] T00473_A1684LogErroBCJasonMessage ;
      private long[] T00476_A1668LogErroBCID ;
      private long[] T00477_A1668LogErroBCID ;
      private long[] T00472_A1668LogErroBCID ;
      private String[] T00472_A1680LogErroBCModulo ;
      private DateTime[] T00472_A1681LogErroBCData ;
      private String[] T00472_A1682LogErroBCNomeBC ;
      private String[] T00472_A1683LogErroBCJsonBC ;
      private String[] T00472_A1684LogErroBCJasonMessage ;
      private long[] T004711_A1668LogErroBCID ;
      private GXWebForm Form ;
   }

   public class logerrobc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00474 ;
          prmT00474 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00475 ;
          prmT00475 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00473 ;
          prmT00473 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00476 ;
          prmT00476 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00477 ;
          prmT00477 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00472 ;
          prmT00472 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT00478 ;
          prmT00478 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0} ,
          new Object[] {"@LogErroBCModulo",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogErroBCNomeBC",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCJsonBC",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCJasonMessage",SqlDbType.VarChar,2097152,0}
          } ;
          Object[] prmT00479 ;
          prmT00479 = new Object[] {
          new Object[] {"@LogErroBCModulo",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogErroBCNomeBC",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCJsonBC",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCJasonMessage",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT004710 ;
          prmT004710 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT004711 ;
          prmT004711 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00472", "SELECT [LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage] FROM [LogErroBC] WITH (UPDLOCK) WHERE [LogErroBCID] = @LogErroBCID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00472,1,0,true,false )
             ,new CursorDef("T00473", "SELECT [LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage] FROM [LogErroBC] WITH (NOLOCK) WHERE [LogErroBCID] = @LogErroBCID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00473,1,0,true,false )
             ,new CursorDef("T00474", "SELECT TM1.[LogErroBCID], TM1.[LogErroBCModulo], TM1.[LogErroBCData], TM1.[LogErroBCNomeBC], TM1.[LogErroBCJsonBC], TM1.[LogErroBCJasonMessage] FROM [LogErroBC] TM1 WITH (NOLOCK) WHERE TM1.[LogErroBCID] = @LogErroBCID ORDER BY TM1.[LogErroBCID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00474,100,0,true,false )
             ,new CursorDef("T00475", "SELECT [LogErroBCID] FROM [LogErroBC] WITH (NOLOCK) WHERE [LogErroBCID] = @LogErroBCID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00475,1,0,true,false )
             ,new CursorDef("T00476", "SELECT TOP 1 [LogErroBCID] FROM [LogErroBC] WITH (NOLOCK) WHERE ( [LogErroBCID] > @LogErroBCID) ORDER BY [LogErroBCID]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00476,1,0,true,true )
             ,new CursorDef("T00477", "SELECT TOP 1 [LogErroBCID] FROM [LogErroBC] WITH (NOLOCK) WHERE ( [LogErroBCID] < @LogErroBCID) ORDER BY [LogErroBCID] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00477,1,0,true,true )
             ,new CursorDef("T00478", "INSERT INTO [LogErroBC]([LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage]) VALUES(@LogErroBCID, @LogErroBCModulo, @LogErroBCData, @LogErroBCNomeBC, @LogErroBCJsonBC, @LogErroBCJasonMessage)", GxErrorMask.GX_NOMASK,prmT00478)
             ,new CursorDef("T00479", "UPDATE [LogErroBC] SET [LogErroBCModulo]=@LogErroBCModulo, [LogErroBCData]=@LogErroBCData, [LogErroBCNomeBC]=@LogErroBCNomeBC, [LogErroBCJsonBC]=@LogErroBCJsonBC, [LogErroBCJasonMessage]=@LogErroBCJasonMessage  WHERE [LogErroBCID] = @LogErroBCID", GxErrorMask.GX_NOMASK,prmT00479)
             ,new CursorDef("T004710", "DELETE FROM [LogErroBC]  WHERE [LogErroBCID] = @LogErroBCID", GxErrorMask.GX_NOMASK,prmT004710)
             ,new CursorDef("T004711", "SELECT [LogErroBCID] FROM [LogErroBC] WITH (NOLOCK) ORDER BY [LogErroBCID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004711,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (long)parms[5]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
