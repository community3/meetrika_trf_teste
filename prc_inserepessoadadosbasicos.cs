/*
               File: PRC_InserePessoaDadosBasicos
        Description: PRC_InserePessoaDadosBasicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:25.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inserepessoadadosbasicos : GXProcedure
   {
      public prc_inserepessoadadosbasicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_inserepessoadadosbasicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Pessoa_Codigo ,
                           String aP1_Pessoa_Nome ,
                           String aP2_Pessoa_TipoPessoa ,
                           String aP3_Pessoa_Docto )
      {
         this.AV13Pessoa_Codigo = aP0_Pessoa_Codigo;
         this.AV12Pessoa_Nome = aP1_Pessoa_Nome;
         this.AV9Pessoa_TipoPessoa = aP2_Pessoa_TipoPessoa;
         this.AV10Pessoa_Docto = aP3_Pessoa_Docto;
         initialize();
         executePrivate();
         aP0_Pessoa_Codigo=this.AV13Pessoa_Codigo;
      }

      public void executeSubmit( ref int aP0_Pessoa_Codigo ,
                                 String aP1_Pessoa_Nome ,
                                 String aP2_Pessoa_TipoPessoa ,
                                 String aP3_Pessoa_Docto )
      {
         prc_inserepessoadadosbasicos objprc_inserepessoadadosbasicos;
         objprc_inserepessoadadosbasicos = new prc_inserepessoadadosbasicos();
         objprc_inserepessoadadosbasicos.AV13Pessoa_Codigo = aP0_Pessoa_Codigo;
         objprc_inserepessoadadosbasicos.AV12Pessoa_Nome = aP1_Pessoa_Nome;
         objprc_inserepessoadadosbasicos.AV9Pessoa_TipoPessoa = aP2_Pessoa_TipoPessoa;
         objprc_inserepessoadadosbasicos.AV10Pessoa_Docto = aP3_Pessoa_Docto;
         objprc_inserepessoadadosbasicos.context.SetSubmitInitialConfig(context);
         objprc_inserepessoadadosbasicos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inserepessoadadosbasicos);
         aP0_Pessoa_Codigo=this.AV13Pessoa_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inserepessoadadosbasicos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Pessoa = new SdtPessoa(context);
         AV8Pessoa.gxTpr_Pessoa_nome = AV12Pessoa_Nome;
         AV8Pessoa.gxTpr_Pessoa_tipopessoa = AV9Pessoa_TipoPessoa;
         AV8Pessoa.gxTpr_Pessoa_docto = AV10Pessoa_Docto;
         AV8Pessoa.Save();
         if ( AV8Pessoa.Success() )
         {
            AV13Pessoa_Codigo = AV8Pessoa.gxTpr_Pessoa_codigo;
            context.CommitDataStores( "PRC_InserePessoaDadosBasicos");
         }
         else
         {
            GX_msglist.addItem(AV8Pessoa.GetMessages().ToXml(false, true, "Messages", "Genexus"));
            context.RollbackDataStores( "PRC_InserePessoaDadosBasicos");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Pessoa = new SdtPessoa(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_inserepessoadadosbasicos__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV13Pessoa_Codigo ;
      private String AV12Pessoa_Nome ;
      private String AV9Pessoa_TipoPessoa ;
      private String AV10Pessoa_Docto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Pessoa_Codigo ;
      private IDataStoreProvider pr_default ;
      private SdtPessoa AV8Pessoa ;
   }

   public class prc_inserepessoadadosbasicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
