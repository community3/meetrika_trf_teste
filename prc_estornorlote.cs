/*
               File: PRC_EstornorLote
        Description: Realiza o Estorno do Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:33.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_estornorlote : GXProcedure
   {
      public prc_estornorlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_estornorlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_Codigo ,
                           String aP1_Lote_Numero ,
                           out String aP2_Retorno )
      {
         this.AV8Lote_Codigo = aP0_Lote_Codigo;
         this.AV10Lote_Numero = aP1_Lote_Numero;
         this.AV9Retorno = "" ;
         initialize();
         executePrivate();
         aP2_Retorno=this.AV9Retorno;
      }

      public String executeUdp( int aP0_Lote_Codigo ,
                                String aP1_Lote_Numero )
      {
         this.AV8Lote_Codigo = aP0_Lote_Codigo;
         this.AV10Lote_Numero = aP1_Lote_Numero;
         this.AV9Retorno = "" ;
         initialize();
         executePrivate();
         aP2_Retorno=this.AV9Retorno;
         return AV9Retorno ;
      }

      public void executeSubmit( int aP0_Lote_Codigo ,
                                 String aP1_Lote_Numero ,
                                 out String aP2_Retorno )
      {
         prc_estornorlote objprc_estornorlote;
         objprc_estornorlote = new prc_estornorlote();
         objprc_estornorlote.AV8Lote_Codigo = aP0_Lote_Codigo;
         objprc_estornorlote.AV10Lote_Numero = aP1_Lote_Numero;
         objprc_estornorlote.AV9Retorno = "" ;
         objprc_estornorlote.context.SetSubmitInitialConfig(context);
         objprc_estornorlote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_estornorlote);
         aP2_Retorno=this.AV9Retorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_estornorlote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         AV17PrazoEntrega = DateTimeUtil.ServerNow( context, "DEFAULT");
         /* Using cursor P00Y22 */
         pr_default.execute(0, new Object[] {AV8Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00Y22_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00Y22_n597ContagemResultado_LoteAceiteCod[0];
            A484ContagemResultado_StatusDmn = P00Y22_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00Y22_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00Y22_A456ContagemResultado_Codigo[0];
            AV15StatusAnterior = A484ContagemResultado_StatusDmn;
            A484ContagemResultado_StatusDmn = "H";
            n484ContagemResultado_StatusDmn = false;
            A597ContagemResultado_LoteAceiteCod = 0;
            n597ContagemResultado_LoteAceiteCod = false;
            n597ContagemResultado_LoteAceiteCod = true;
            /* Execute user subroutine: 'REGISTRA.ESTORNO.HISTORICO' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P00Y23 */
            pr_default.execute(1, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Optimized DELETE. */
         /* Using cursor P00Y24 */
         pr_default.execute(2, new Object[] {AV8Lote_Codigo});
         pr_default.close(2);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
         /* End optimized DELETE. */
         /* Optimized DELETE. */
         /* Using cursor P00Y25 */
         pr_default.execute(3, new Object[] {AV8Lote_Codigo});
         pr_default.close(3);
         dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
         /* End optimized DELETE. */
         /* Optimized DELETE. */
         /* Using cursor P00Y26 */
         pr_default.execute(4, new Object[] {AV8Lote_Codigo});
         pr_default.close(4);
         dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
         /* End optimized DELETE. */
         context.CommitDataStores( "PRC_EstornorLote");
         AV9Retorno = StringUtil.Format( "Lote %1 Estornado.", StringUtil.Trim( AV10Lote_Numero), "", "", "", "", "", "", "", "");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'REGISTRA.ESTORNO.HISTORICO' Routine */
         AV16ContagemResultado_Observacao = StringUtil.Format( "Estorno do Lote %1", StringUtil.Trim( AV10Lote_Numero), "", "", "", "", "", "", "", "");
         AV11Acao = "BK";
         new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV13WWPContext.gxTpr_Userid,  AV11Acao,  "D",  AV13WWPContext.gxTpr_Userid,  0,  "O",  "H",  AV16ContagemResultado_Observacao,  AV17PrazoEntrega,  false) ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV17PrazoEntrega = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         P00Y22_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00Y22_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00Y22_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00Y22_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00Y22_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         AV15StatusAnterior = "";
         AV16ContagemResultado_Observacao = "";
         AV11Acao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_estornorlote__default(),
            new Object[][] {
                new Object[] {
               P00Y22_A597ContagemResultado_LoteAceiteCod, P00Y22_n597ContagemResultado_LoteAceiteCod, P00Y22_A484ContagemResultado_StatusDmn, P00Y22_n484ContagemResultado_StatusDmn, P00Y22_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Lote_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private String AV10Lote_Numero ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV15StatusAnterior ;
      private String AV11Acao ;
      private DateTime AV17PrazoEntrega ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool returnInSub ;
      private String AV16ContagemResultado_Observacao ;
      private String AV9Retorno ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Y22_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00Y22_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00Y22_A484ContagemResultado_StatusDmn ;
      private bool[] P00Y22_n484ContagemResultado_StatusDmn ;
      private int[] P00Y22_A456ContagemResultado_Codigo ;
      private String aP2_Retorno ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class prc_estornorlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y22 ;
          prmP00Y22 = new Object[] {
          new Object[] {"@AV8Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y23 ;
          prmP00Y23 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y24 ;
          prmP00Y24 = new Object[] {
          new Object[] {"@AV8Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y25 ;
          prmP00Y25 = new Object[] {
          new Object[] {"@AV8Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y26 ;
          prmP00Y26 = new Object[] {
          new Object[] {"@AV8Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y22", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV8Lote_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y22,1,0,true,false )
             ,new CursorDef("P00Y23", "UPDATE [ContagemResultado] SET [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y23)
             ,new CursorDef("P00Y24", "DELETE FROM [ContagemResultadoIndicadores]  WHERE [ContagemResultadoIndicadores_LoteCod] = @AV8Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y24)
             ,new CursorDef("P00Y25", "DELETE FROM [LoteArquivoAnexo]  WHERE [LoteArquivoAnexo_LoteCod] = @AV8Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y25)
             ,new CursorDef("P00Y26", "DELETE FROM [Lote]  WHERE [Lote_Codigo] = @AV8Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y26)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
