/*
               File: GetWWPessoaCertificadoFilterData
        Description: Get WWPessoa Certificado Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:2.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwpessoacertificadofilterdata : GXProcedure
   {
      public getwwpessoacertificadofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwpessoacertificadofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwpessoacertificadofilterdata objgetwwpessoacertificadofilterdata;
         objgetwwpessoacertificadofilterdata = new getwwpessoacertificadofilterdata();
         objgetwwpessoacertificadofilterdata.AV22DDOName = aP0_DDOName;
         objgetwwpessoacertificadofilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetwwpessoacertificadofilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetwwpessoacertificadofilterdata.AV26OptionsJson = "" ;
         objgetwwpessoacertificadofilterdata.AV29OptionsDescJson = "" ;
         objgetwwpessoacertificadofilterdata.AV31OptionIndexesJson = "" ;
         objgetwwpessoacertificadofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwpessoacertificadofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwpessoacertificadofilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwpessoacertificadofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_PESSOACERTIFICADO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOACERTIFICADO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("WWPessoaCertificadoGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWPessoaCertificadoGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("WWPessoaCertificadoGridState"), "");
         }
         AV63GXV1 = 1;
         while ( AV63GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV63GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME") == 0 )
            {
               AV16TFPessoaCertificado_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME_SEL") == 0 )
            {
               AV17TFPessoaCertificado_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_EMISSAO") == 0 )
            {
               AV14TFPessoaCertificado_Emissao = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV15TFPessoaCertificado_Emissao_To = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_VALIDADE") == 0 )
            {
               AV18TFPessoaCertificado_Validade = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV19TFPessoaCertificado_Validade_To = context.localUtil.CToD( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV63GXV1 = (int)(AV63GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "PESSOACERTIFICADO_EMISSAO") == 0 )
            {
               AV49PessoaCertificado_Emissao1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
               AV50PessoaCertificado_Emissao_To1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "PESSOACERTIFICADO_VALIDADE") == 0 )
            {
               AV51PessoaCertificado_Validade1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
               AV52PessoaCertificado_Validade_To1 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV41DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV42DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "PESSOACERTIFICADO_EMISSAO") == 0 )
               {
                  AV53PessoaCertificado_Emissao2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                  AV54PessoaCertificado_Emissao_To2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "PESSOACERTIFICADO_VALIDADE") == 0 )
               {
                  AV55PessoaCertificado_Validade2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                  AV56PessoaCertificado_Validade_To2 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PESSOACERTIFICADO_EMISSAO") == 0 )
                  {
                     AV57PessoaCertificado_Emissao3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                     AV58PessoaCertificado_Emissao_To3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PESSOACERTIFICADO_VALIDADE") == 0 )
                  {
                     AV59PessoaCertificado_Validade3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                     AV60PessoaCertificado_Validade_To3 = context.localUtil.CToD( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPESSOACERTIFICADO_NOMEOPTIONS' Routine */
         AV16TFPessoaCertificado_Nome = AV20SearchTxt;
         AV17TFPessoaCertificado_Nome_Sel = "";
         AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV49PessoaCertificado_Emissao1;
         AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV50PessoaCertificado_Emissao_To1;
         AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV51PessoaCertificado_Validade1;
         AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV52PessoaCertificado_Validade_To1;
         AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV41DynamicFiltersEnabled2;
         AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV42DynamicFiltersSelector2;
         AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV53PessoaCertificado_Emissao2;
         AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV54PessoaCertificado_Emissao_To2;
         AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV55PessoaCertificado_Validade2;
         AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV56PessoaCertificado_Validade_To2;
         AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV57PessoaCertificado_Emissao3;
         AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV58PessoaCertificado_Emissao_To3;
         AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV59PessoaCertificado_Validade3;
         AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV60PessoaCertificado_Validade_To3;
         AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV16TFPessoaCertificado_Nome;
         AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV17TFPessoaCertificado_Nome_Sel;
         AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV14TFPessoaCertificado_Emissao;
         AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV15TFPessoaCertificado_Emissao_To;
         AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV18TFPessoaCertificado_Validade;
         AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV19TFPessoaCertificado_Validade_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                              AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                              AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                              AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                              AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                              AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                              AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                              AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                              AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                              AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                              AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                              AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                              AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                              AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                              AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                              AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                              AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                              AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                              AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                              AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                              AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                              AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                              AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                              A2012PessoaCertificado_Emissao ,
                                              A2014PessoaCertificado_Validade ,
                                              A2013PessoaCertificado_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = StringUtil.PadR( StringUtil.RTrim( AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome), 50, "%");
         /* Using cursor P00VY2 */
         pr_default.execute(0, new Object[] {AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1, AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1, AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1, AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1, AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2, AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2, AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2, AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2, AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3, AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3, AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3, AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3, lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome, AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel, AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao, AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to, AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade, AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKVY2 = false;
            A2013PessoaCertificado_Nome = P00VY2_A2013PessoaCertificado_Nome[0];
            A2014PessoaCertificado_Validade = P00VY2_A2014PessoaCertificado_Validade[0];
            n2014PessoaCertificado_Validade = P00VY2_n2014PessoaCertificado_Validade[0];
            A2012PessoaCertificado_Emissao = P00VY2_A2012PessoaCertificado_Emissao[0];
            A2010PessoaCertificado_Codigo = P00VY2_A2010PessoaCertificado_Codigo[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00VY2_A2013PessoaCertificado_Nome[0], A2013PessoaCertificado_Nome) == 0 ) )
            {
               BRKVY2 = false;
               A2010PessoaCertificado_Codigo = P00VY2_A2010PessoaCertificado_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKVY2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2013PessoaCertificado_Nome)) )
            {
               AV24Option = A2013PessoaCertificado_Nome;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVY2 )
            {
               BRKVY2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV16TFPessoaCertificado_Nome = "";
         AV17TFPessoaCertificado_Nome_Sel = "";
         AV14TFPessoaCertificado_Emissao = DateTime.MinValue;
         AV15TFPessoaCertificado_Emissao_To = DateTime.MinValue;
         AV18TFPessoaCertificado_Validade = DateTime.MinValue;
         AV19TFPessoaCertificado_Validade_To = DateTime.MinValue;
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV49PessoaCertificado_Emissao1 = DateTime.MinValue;
         AV50PessoaCertificado_Emissao_To1 = DateTime.MinValue;
         AV51PessoaCertificado_Validade1 = DateTime.MinValue;
         AV52PessoaCertificado_Validade_To1 = DateTime.MinValue;
         AV42DynamicFiltersSelector2 = "";
         AV53PessoaCertificado_Emissao2 = DateTime.MinValue;
         AV54PessoaCertificado_Emissao_To2 = DateTime.MinValue;
         AV55PessoaCertificado_Validade2 = DateTime.MinValue;
         AV56PessoaCertificado_Validade_To2 = DateTime.MinValue;
         AV46DynamicFiltersSelector3 = "";
         AV57PessoaCertificado_Emissao3 = DateTime.MinValue;
         AV58PessoaCertificado_Emissao_To3 = DateTime.MinValue;
         AV59PessoaCertificado_Validade3 = DateTime.MinValue;
         AV60PessoaCertificado_Validade_To3 = DateTime.MinValue;
         AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = "";
         AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = DateTime.MinValue;
         AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = DateTime.MinValue;
         AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = DateTime.MinValue;
         AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = DateTime.MinValue;
         AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = "";
         AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = DateTime.MinValue;
         AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = DateTime.MinValue;
         AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = DateTime.MinValue;
         AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = DateTime.MinValue;
         AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = "";
         AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = DateTime.MinValue;
         AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = DateTime.MinValue;
         AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = DateTime.MinValue;
         AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = DateTime.MinValue;
         AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = "";
         AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = "";
         AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = DateTime.MinValue;
         AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = DateTime.MinValue;
         AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = DateTime.MinValue;
         AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = DateTime.MinValue;
         scmdbuf = "";
         lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = "";
         A2012PessoaCertificado_Emissao = DateTime.MinValue;
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         A2013PessoaCertificado_Nome = "";
         P00VY2_A2013PessoaCertificado_Nome = new String[] {""} ;
         P00VY2_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         P00VY2_n2014PessoaCertificado_Validade = new bool[] {false} ;
         P00VY2_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00VY2_A2010PessoaCertificado_Codigo = new int[1] ;
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwpessoacertificadofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00VY2_A2013PessoaCertificado_Nome, P00VY2_A2014PessoaCertificado_Validade, P00VY2_n2014PessoaCertificado_Validade, P00VY2_A2012PessoaCertificado_Emissao, P00VY2_A2010PessoaCertificado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV63GXV1 ;
      private int A2010PessoaCertificado_Codigo ;
      private long AV32count ;
      private String AV16TFPessoaCertificado_Nome ;
      private String AV17TFPessoaCertificado_Nome_Sel ;
      private String AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ;
      private String AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ;
      private String scmdbuf ;
      private String lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ;
      private String A2013PessoaCertificado_Nome ;
      private DateTime AV14TFPessoaCertificado_Emissao ;
      private DateTime AV15TFPessoaCertificado_Emissao_To ;
      private DateTime AV18TFPessoaCertificado_Validade ;
      private DateTime AV19TFPessoaCertificado_Validade_To ;
      private DateTime AV49PessoaCertificado_Emissao1 ;
      private DateTime AV50PessoaCertificado_Emissao_To1 ;
      private DateTime AV51PessoaCertificado_Validade1 ;
      private DateTime AV52PessoaCertificado_Validade_To1 ;
      private DateTime AV53PessoaCertificado_Emissao2 ;
      private DateTime AV54PessoaCertificado_Emissao_To2 ;
      private DateTime AV55PessoaCertificado_Validade2 ;
      private DateTime AV56PessoaCertificado_Validade_To2 ;
      private DateTime AV57PessoaCertificado_Emissao3 ;
      private DateTime AV58PessoaCertificado_Emissao_To3 ;
      private DateTime AV59PessoaCertificado_Validade3 ;
      private DateTime AV60PessoaCertificado_Validade_To3 ;
      private DateTime AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ;
      private DateTime AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ;
      private DateTime AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ;
      private DateTime AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ;
      private DateTime AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ;
      private DateTime AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ;
      private DateTime AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ;
      private DateTime AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ;
      private DateTime AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ;
      private DateTime AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ;
      private DateTime AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ;
      private DateTime AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ;
      private DateTime AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ;
      private DateTime AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ;
      private DateTime AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ;
      private DateTime AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ;
      private DateTime A2012PessoaCertificado_Emissao ;
      private DateTime A2014PessoaCertificado_Validade ;
      private bool returnInSub ;
      private bool AV41DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ;
      private bool AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ;
      private bool BRKVY2 ;
      private bool n2014PessoaCertificado_Validade ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV42DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ;
      private String AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ;
      private String AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00VY2_A2013PessoaCertificado_Nome ;
      private DateTime[] P00VY2_A2014PessoaCertificado_Validade ;
      private bool[] P00VY2_n2014PessoaCertificado_Validade ;
      private DateTime[] P00VY2_A2012PessoaCertificado_Emissao ;
      private int[] P00VY2_A2010PessoaCertificado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getwwpessoacertificadofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VY2( IGxContext context ,
                                             String AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                             DateTime AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                             DateTime AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                             DateTime AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                             bool AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                             String AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                             DateTime AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                             DateTime AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                             DateTime AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                             bool AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                             String AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                             DateTime AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                             DateTime AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                             DateTime AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                             DateTime AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                             String AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                             String AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                             DateTime AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                             DateTime AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                             DateTime AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                             DateTime AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                             DateTime A2012PessoaCertificado_Emissao ,
                                             DateTime A2014PessoaCertificado_Validade ,
                                             String A2013PessoaCertificado_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [PessoaCertificado_Nome], [PessoaCertificado_Validade], [PessoaCertificado_Emissao], [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV70WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV76WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV77WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] like @lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] like @lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] = @AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] = @AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [PessoaCertificado_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00VY2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VY2 ;
          prmP00VY2 = new Object[] {
          new Object[] {"@AV66WWPessoaCertificadoDS_2_Pessoacertificado_emissao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWPessoaCertificadoDS_4_Pessoacertificado_validade1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWPessoaCertificadoDS_8_Pessoacertificado_emissao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWPessoaCertificadoDS_10_Pessoacertificado_validade2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWPessoaCertificadoDS_14_Pessoacertificado_emissao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWPessoaCertificadoDS_16_Pessoacertificado_validade3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV82WWPessoaCertificadoDS_18_Tfpessoacertificado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWPessoaCertificadoDS_22_Tfpessoacertificado_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VY2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwpessoacertificadofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwpessoacertificadofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwpessoacertificadofilterdata") )
          {
             return  ;
          }
          getwwpessoacertificadofilterdata worker = new getwwpessoacertificadofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
