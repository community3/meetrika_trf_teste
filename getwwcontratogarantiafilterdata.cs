/*
               File: GetWWContratoGarantiaFilterData
        Description: Get WWContrato Garantia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:24.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratogarantiafilterdata : GXProcedure
   {
      public getwwcontratogarantiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratogarantiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
         return AV39OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratogarantiafilterdata objgetwwcontratogarantiafilterdata;
         objgetwwcontratogarantiafilterdata = new getwwcontratogarantiafilterdata();
         objgetwwcontratogarantiafilterdata.AV30DDOName = aP0_DDOName;
         objgetwwcontratogarantiafilterdata.AV28SearchTxt = aP1_SearchTxt;
         objgetwwcontratogarantiafilterdata.AV29SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratogarantiafilterdata.AV34OptionsJson = "" ;
         objgetwwcontratogarantiafilterdata.AV37OptionsDescJson = "" ;
         objgetwwcontratogarantiafilterdata.AV39OptionIndexesJson = "" ;
         objgetwwcontratogarantiafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratogarantiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratogarantiafilterdata);
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratogarantiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV33Options = (IGxCollection)(new GxSimpleCollection());
         AV36OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV38OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV34OptionsJson = AV33Options.ToJSonString(false);
         AV37OptionsDescJson = AV36OptionsDesc.ToJSonString(false);
         AV39OptionIndexesJson = AV38OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get("WWContratoGarantiaGridState"), "") == 0 )
         {
            AV43GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoGarantiaGridState"), "");
         }
         else
         {
            AV43GridState.FromXml(AV41Session.Get("WWContratoGarantiaGridState"), "");
         }
         AV81GXV1 = 1;
         while ( AV81GXV1 <= AV43GridState.gxTpr_Filtervalues.Count )
         {
            AV44GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV43GridState.gxTpr_Filtervalues.Item(AV81GXV1));
            if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV12TFContratada_PessoaNom = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV13TFContratada_PessoaNom_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV14TFContratada_PessoaCNPJ = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV15TFContratada_PessoaCNPJ_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV16TFContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_PERCENTUAL") == 0 )
            {
               AV18TFContratoGarantia_Percentual = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, ".");
               AV19TFContratoGarantia_Percentual_To = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATACAUCAO") == 0 )
            {
               AV20TFContratoGarantia_DataCaucao = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV21TFContratoGarantia_DataCaucao_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORGARANTIA") == 0 )
            {
               AV22TFContratoGarantia_ValorGarantia = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, ".");
               AV23TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
            {
               AV24TFContratoGarantia_DataEncerramento = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV25TFContratoGarantia_DataEncerramento_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORENCERRAMENTO") == 0 )
            {
               AV26TFContratoGarantia_ValorEncerramento = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, ".");
               AV27TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV81GXV1 = (int)(AV81GXV1+1);
         }
         if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(1));
            AV46DynamicFiltersSelector1 = AV45GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV47ContratoGarantia_DataPagtoGarantia1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV48ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
            {
               AV49ContratoGarantia_DataCaucao1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV50ContratoGarantia_DataCaucao_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
            {
               AV51ContratoGarantia_DataEncerramento1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV52ContratoGarantia_DataEncerramento_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV53DynamicFiltersEnabled2 = true;
               AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(2));
               AV54DynamicFiltersSelector2 = AV45GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV54DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
               {
                  AV55ContratoGarantia_DataPagtoGarantia2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV56ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
               {
                  AV57ContratoGarantia_DataCaucao2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV58ContratoGarantia_DataCaucao_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
               {
                  AV59ContratoGarantia_DataEncerramento2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV60ContratoGarantia_DataEncerramento_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV61DynamicFiltersEnabled3 = true;
                  AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(3));
                  AV62DynamicFiltersSelector3 = AV45GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
                  {
                     AV63ContratoGarantia_DataPagtoGarantia3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV64ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
                  {
                     AV65ContratoGarantia_DataCaucao3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV66ContratoGarantia_DataCaucao_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
                  {
                     AV67ContratoGarantia_DataEncerramento3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV68ContratoGarantia_DataEncerramento_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV28SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV46DynamicFiltersSelector1;
         AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV47ContratoGarantia_DataPagtoGarantia1;
         AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV48ContratoGarantia_DataPagtoGarantia_To1;
         AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV49ContratoGarantia_DataCaucao1;
         AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV50ContratoGarantia_DataCaucao_To1;
         AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV51ContratoGarantia_DataEncerramento1;
         AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV52ContratoGarantia_DataEncerramento_To1;
         AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV55ContratoGarantia_DataPagtoGarantia2;
         AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV56ContratoGarantia_DataPagtoGarantia_To2;
         AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV57ContratoGarantia_DataCaucao2;
         AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV58ContratoGarantia_DataCaucao_To2;
         AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV59ContratoGarantia_DataEncerramento2;
         AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV60ContratoGarantia_DataEncerramento_To2;
         AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV61DynamicFiltersEnabled3;
         AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV62DynamicFiltersSelector3;
         AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV63ContratoGarantia_DataPagtoGarantia3;
         AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV64ContratoGarantia_DataPagtoGarantia_To3;
         AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV65ContratoGarantia_DataCaucao3;
         AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV66ContratoGarantia_DataCaucao_To3;
         AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV67ContratoGarantia_DataEncerramento3;
         AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV68ContratoGarantia_DataEncerramento_To3;
         AV106WWContratoGarantiaDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV16TFContratoGarantia_DataPagtoGarantia;
         AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV17TFContratoGarantia_DataPagtoGarantia_To;
         AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV18TFContratoGarantia_Percentual;
         AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV19TFContratoGarantia_Percentual_To;
         AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV20TFContratoGarantia_DataCaucao;
         AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV21TFContratoGarantia_DataCaucao_To;
         AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV22TFContratoGarantia_ValorGarantia;
         AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV23TFContratoGarantia_ValorGarantia_To;
         AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV24TFContratoGarantia_DataEncerramento;
         AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV25TFContratoGarantia_DataEncerramento_To;
         AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV26TFContratoGarantia_ValorEncerramento;
         AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV27TFContratoGarantia_ValorEncerramento_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                              AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                              AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                              AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                              AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                              AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                              AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                              AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                              AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                              AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                              AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                              AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                              AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                              AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                              AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                              AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                              AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                              AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                              AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                              AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                              AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                              AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                              AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                              AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                              AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                              AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                              AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                              AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                              AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                              AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                              AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                              AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                              AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                              AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                              AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                              AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                              AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                              AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                              AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                              AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                              AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV106WWContratoGarantiaDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero), 20, "%");
         lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom), 100, "%");
         lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00J32 */
         pr_default.execute(0, new Object[] {AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1, AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1, AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1, AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1, AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1, AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1, AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2, AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2, AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2, AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2, AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2, AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2, AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3, AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3, AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3, AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3, AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3, AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3, lV106WWContratoGarantiaDS_24_Tfcontrato_numero, AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel, lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom, AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel, lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj, AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel, AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia, AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to, AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual, AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to, AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao, AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to, AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia, AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to, AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento, AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to, AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento, AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJ32 = false;
            A74Contrato_Codigo = P00J32_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J32_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J32_A40Contratada_PessoaCod[0];
            A77Contrato_Numero = P00J32_A77Contrato_Numero[0];
            A107ContratoGarantia_ValorEncerramento = P00J32_A107ContratoGarantia_ValorEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00J32_A105ContratoGarantia_ValorGarantia[0];
            A103ContratoGarantia_Percentual = P00J32_A103ContratoGarantia_Percentual[0];
            A42Contratada_PessoaCNPJ = P00J32_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J32_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J32_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J32_n41Contratada_PessoaNom[0];
            A106ContratoGarantia_DataEncerramento = P00J32_A106ContratoGarantia_DataEncerramento[0];
            A104ContratoGarantia_DataCaucao = P00J32_A104ContratoGarantia_DataCaucao[0];
            A102ContratoGarantia_DataPagtoGarantia = P00J32_A102ContratoGarantia_DataPagtoGarantia[0];
            A101ContratoGarantia_Codigo = P00J32_A101ContratoGarantia_Codigo[0];
            A39Contratada_Codigo = P00J32_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J32_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J32_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J32_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J32_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J32_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J32_n41Contratada_PessoaNom[0];
            AV40count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00J32_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKJ32 = false;
               A74Contrato_Codigo = P00J32_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = P00J32_A101ContratoGarantia_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKJ32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV32Option = A77Contrato_Numero;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ32 )
            {
               BRKJ32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV12TFContratada_PessoaNom = AV28SearchTxt;
         AV13TFContratada_PessoaNom_Sel = "";
         AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV46DynamicFiltersSelector1;
         AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV47ContratoGarantia_DataPagtoGarantia1;
         AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV48ContratoGarantia_DataPagtoGarantia_To1;
         AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV49ContratoGarantia_DataCaucao1;
         AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV50ContratoGarantia_DataCaucao_To1;
         AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV51ContratoGarantia_DataEncerramento1;
         AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV52ContratoGarantia_DataEncerramento_To1;
         AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV55ContratoGarantia_DataPagtoGarantia2;
         AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV56ContratoGarantia_DataPagtoGarantia_To2;
         AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV57ContratoGarantia_DataCaucao2;
         AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV58ContratoGarantia_DataCaucao_To2;
         AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV59ContratoGarantia_DataEncerramento2;
         AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV60ContratoGarantia_DataEncerramento_To2;
         AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV61DynamicFiltersEnabled3;
         AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV62DynamicFiltersSelector3;
         AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV63ContratoGarantia_DataPagtoGarantia3;
         AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV64ContratoGarantia_DataPagtoGarantia_To3;
         AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV65ContratoGarantia_DataCaucao3;
         AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV66ContratoGarantia_DataCaucao_To3;
         AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV67ContratoGarantia_DataEncerramento3;
         AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV68ContratoGarantia_DataEncerramento_To3;
         AV106WWContratoGarantiaDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV16TFContratoGarantia_DataPagtoGarantia;
         AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV17TFContratoGarantia_DataPagtoGarantia_To;
         AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV18TFContratoGarantia_Percentual;
         AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV19TFContratoGarantia_Percentual_To;
         AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV20TFContratoGarantia_DataCaucao;
         AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV21TFContratoGarantia_DataCaucao_To;
         AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV22TFContratoGarantia_ValorGarantia;
         AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV23TFContratoGarantia_ValorGarantia_To;
         AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV24TFContratoGarantia_DataEncerramento;
         AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV25TFContratoGarantia_DataEncerramento_To;
         AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV26TFContratoGarantia_ValorEncerramento;
         AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV27TFContratoGarantia_ValorEncerramento_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                              AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                              AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                              AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                              AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                              AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                              AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                              AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                              AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                              AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                              AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                              AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                              AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                              AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                              AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                              AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                              AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                              AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                              AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                              AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                              AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                              AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                              AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                              AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                              AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                              AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                              AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                              AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                              AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                              AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                              AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                              AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                              AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                              AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                              AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                              AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                              AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                              AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                              AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                              AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                              AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV106WWContratoGarantiaDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero), 20, "%");
         lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom), 100, "%");
         lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00J33 */
         pr_default.execute(1, new Object[] {AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1, AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1, AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1, AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1, AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1, AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1, AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2, AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2, AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2, AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2, AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2, AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2, AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3, AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3, AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3, AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3, AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3, AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3, lV106WWContratoGarantiaDS_24_Tfcontrato_numero, AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel, lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom, AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel, lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj, AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel, AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia, AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to, AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual, AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to, AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao, AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to, AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia, AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to, AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento, AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to, AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento, AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJ34 = false;
            A74Contrato_Codigo = P00J33_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J33_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J33_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00J33_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J33_n41Contratada_PessoaNom[0];
            A107ContratoGarantia_ValorEncerramento = P00J33_A107ContratoGarantia_ValorEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00J33_A105ContratoGarantia_ValorGarantia[0];
            A103ContratoGarantia_Percentual = P00J33_A103ContratoGarantia_Percentual[0];
            A42Contratada_PessoaCNPJ = P00J33_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J33_n42Contratada_PessoaCNPJ[0];
            A77Contrato_Numero = P00J33_A77Contrato_Numero[0];
            A106ContratoGarantia_DataEncerramento = P00J33_A106ContratoGarantia_DataEncerramento[0];
            A104ContratoGarantia_DataCaucao = P00J33_A104ContratoGarantia_DataCaucao[0];
            A102ContratoGarantia_DataPagtoGarantia = P00J33_A102ContratoGarantia_DataPagtoGarantia[0];
            A101ContratoGarantia_Codigo = P00J33_A101ContratoGarantia_Codigo[0];
            A39Contratada_Codigo = P00J33_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J33_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J33_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00J33_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J33_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00J33_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J33_n42Contratada_PessoaCNPJ[0];
            AV40count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00J33_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKJ34 = false;
               A74Contrato_Codigo = P00J33_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00J33_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J33_A40Contratada_PessoaCod[0];
               A101ContratoGarantia_Codigo = P00J33_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = P00J33_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J33_A40Contratada_PessoaCod[0];
               AV40count = (long)(AV40count+1);
               BRKJ34 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV32Option = A41Contratada_PessoaNom;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ34 )
            {
               BRKJ34 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV14TFContratada_PessoaCNPJ = AV28SearchTxt;
         AV15TFContratada_PessoaCNPJ_Sel = "";
         AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV46DynamicFiltersSelector1;
         AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV47ContratoGarantia_DataPagtoGarantia1;
         AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV48ContratoGarantia_DataPagtoGarantia_To1;
         AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV49ContratoGarantia_DataCaucao1;
         AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV50ContratoGarantia_DataCaucao_To1;
         AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV51ContratoGarantia_DataEncerramento1;
         AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV52ContratoGarantia_DataEncerramento_To1;
         AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV53DynamicFiltersEnabled2;
         AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV54DynamicFiltersSelector2;
         AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV55ContratoGarantia_DataPagtoGarantia2;
         AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV56ContratoGarantia_DataPagtoGarantia_To2;
         AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV57ContratoGarantia_DataCaucao2;
         AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV58ContratoGarantia_DataCaucao_To2;
         AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV59ContratoGarantia_DataEncerramento2;
         AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV60ContratoGarantia_DataEncerramento_To2;
         AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV61DynamicFiltersEnabled3;
         AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV62DynamicFiltersSelector3;
         AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV63ContratoGarantia_DataPagtoGarantia3;
         AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV64ContratoGarantia_DataPagtoGarantia_To3;
         AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV65ContratoGarantia_DataCaucao3;
         AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV66ContratoGarantia_DataCaucao_To3;
         AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV67ContratoGarantia_DataEncerramento3;
         AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV68ContratoGarantia_DataEncerramento_To3;
         AV106WWContratoGarantiaDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV16TFContratoGarantia_DataPagtoGarantia;
         AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV17TFContratoGarantia_DataPagtoGarantia_To;
         AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV18TFContratoGarantia_Percentual;
         AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV19TFContratoGarantia_Percentual_To;
         AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV20TFContratoGarantia_DataCaucao;
         AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV21TFContratoGarantia_DataCaucao_To;
         AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV22TFContratoGarantia_ValorGarantia;
         AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV23TFContratoGarantia_ValorGarantia_To;
         AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV24TFContratoGarantia_DataEncerramento;
         AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV25TFContratoGarantia_DataEncerramento_To;
         AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV26TFContratoGarantia_ValorEncerramento;
         AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV27TFContratoGarantia_ValorEncerramento_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                              AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                              AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                              AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                              AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                              AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                              AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                              AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                              AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                              AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                              AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                              AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                              AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                              AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                              AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                              AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                              AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                              AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                              AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                              AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                              AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                              AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                              AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                              AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                              AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                              AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                              AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                              AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                              AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                              AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                              AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                              AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                              AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                              AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                              AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                              AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                              AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                              AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                              AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                              AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                              AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV106WWContratoGarantiaDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero), 20, "%");
         lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom), 100, "%");
         lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00J34 */
         pr_default.execute(2, new Object[] {AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1, AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1, AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1, AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1, AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1, AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1, AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2, AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2, AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2, AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2, AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2, AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2, AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3, AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3, AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3, AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3, AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3, AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3, lV106WWContratoGarantiaDS_24_Tfcontrato_numero, AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel, lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom, AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel, lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj, AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel, AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia, AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to, AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual, AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to, AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao, AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to, AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia, AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to, AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento, AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to, AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento, AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJ36 = false;
            A74Contrato_Codigo = P00J34_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J34_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J34_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J34_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J34_n42Contratada_PessoaCNPJ[0];
            A107ContratoGarantia_ValorEncerramento = P00J34_A107ContratoGarantia_ValorEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00J34_A105ContratoGarantia_ValorGarantia[0];
            A103ContratoGarantia_Percentual = P00J34_A103ContratoGarantia_Percentual[0];
            A41Contratada_PessoaNom = P00J34_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J34_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00J34_A77Contrato_Numero[0];
            A106ContratoGarantia_DataEncerramento = P00J34_A106ContratoGarantia_DataEncerramento[0];
            A104ContratoGarantia_DataCaucao = P00J34_A104ContratoGarantia_DataCaucao[0];
            A102ContratoGarantia_DataPagtoGarantia = P00J34_A102ContratoGarantia_DataPagtoGarantia[0];
            A101ContratoGarantia_Codigo = P00J34_A101ContratoGarantia_Codigo[0];
            A39Contratada_Codigo = P00J34_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J34_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J34_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J34_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J34_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J34_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J34_n41Contratada_PessoaNom[0];
            AV40count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00J34_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKJ36 = false;
               A74Contrato_Codigo = P00J34_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00J34_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J34_A40Contratada_PessoaCod[0];
               A101ContratoGarantia_Codigo = P00J34_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = P00J34_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J34_A40Contratada_PessoaCod[0];
               AV40count = (long)(AV40count+1);
               BRKJ36 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV32Option = A42Contratada_PessoaCNPJ;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ36 )
            {
               BRKJ36 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV33Options = new GxSimpleCollection();
         AV36OptionsDesc = new GxSimpleCollection();
         AV38OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41Session = context.GetSession();
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratada_PessoaNom = "";
         AV13TFContratada_PessoaNom_Sel = "";
         AV14TFContratada_PessoaCNPJ = "";
         AV15TFContratada_PessoaCNPJ_Sel = "";
         AV16TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         AV17TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         AV20TFContratoGarantia_DataCaucao = DateTime.MinValue;
         AV21TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         AV24TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         AV25TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         AV45GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV46DynamicFiltersSelector1 = "";
         AV47ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         AV48ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         AV49ContratoGarantia_DataCaucao1 = DateTime.MinValue;
         AV50ContratoGarantia_DataCaucao_To1 = DateTime.MinValue;
         AV51ContratoGarantia_DataEncerramento1 = DateTime.MinValue;
         AV52ContratoGarantia_DataEncerramento_To1 = DateTime.MinValue;
         AV54DynamicFiltersSelector2 = "";
         AV55ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         AV56ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         AV57ContratoGarantia_DataCaucao2 = DateTime.MinValue;
         AV58ContratoGarantia_DataCaucao_To2 = DateTime.MinValue;
         AV59ContratoGarantia_DataEncerramento2 = DateTime.MinValue;
         AV60ContratoGarantia_DataEncerramento_To2 = DateTime.MinValue;
         AV62DynamicFiltersSelector3 = "";
         AV63ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         AV64ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         AV65ContratoGarantia_DataCaucao3 = DateTime.MinValue;
         AV66ContratoGarantia_DataCaucao_To3 = DateTime.MinValue;
         AV67ContratoGarantia_DataEncerramento3 = DateTime.MinValue;
         AV68ContratoGarantia_DataEncerramento_To3 = DateTime.MinValue;
         AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 = "";
         AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = DateTime.MinValue;
         AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = DateTime.MinValue;
         AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = DateTime.MinValue;
         AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = DateTime.MinValue;
         AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = DateTime.MinValue;
         AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = DateTime.MinValue;
         AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 = "";
         AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = DateTime.MinValue;
         AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = DateTime.MinValue;
         AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = DateTime.MinValue;
         AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = DateTime.MinValue;
         AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = DateTime.MinValue;
         AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = DateTime.MinValue;
         AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 = "";
         AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = DateTime.MinValue;
         AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = DateTime.MinValue;
         AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = DateTime.MinValue;
         AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = DateTime.MinValue;
         AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = DateTime.MinValue;
         AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = DateTime.MinValue;
         AV106WWContratoGarantiaDS_24_Tfcontrato_numero = "";
         AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel = "";
         AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = "";
         AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = "";
         AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = "";
         AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = "";
         AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = DateTime.MinValue;
         AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = DateTime.MinValue;
         AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = DateTime.MinValue;
         AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = DateTime.MinValue;
         AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = DateTime.MinValue;
         AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = DateTime.MinValue;
         scmdbuf = "";
         lV106WWContratoGarantiaDS_24_Tfcontrato_numero = "";
         lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom = "";
         lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = "";
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         P00J32_A74Contrato_Codigo = new int[1] ;
         P00J32_A39Contratada_Codigo = new int[1] ;
         P00J32_A40Contratada_PessoaCod = new int[1] ;
         P00J32_A77Contrato_Numero = new String[] {""} ;
         P00J32_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00J32_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00J32_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00J32_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J32_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J32_A41Contratada_PessoaNom = new String[] {""} ;
         P00J32_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J32_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00J32_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00J32_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         P00J32_A101ContratoGarantia_Codigo = new int[1] ;
         AV32Option = "";
         P00J33_A74Contrato_Codigo = new int[1] ;
         P00J33_A39Contratada_Codigo = new int[1] ;
         P00J33_A40Contratada_PessoaCod = new int[1] ;
         P00J33_A41Contratada_PessoaNom = new String[] {""} ;
         P00J33_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J33_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00J33_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00J33_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00J33_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J33_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J33_A77Contrato_Numero = new String[] {""} ;
         P00J33_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00J33_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00J33_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         P00J33_A101ContratoGarantia_Codigo = new int[1] ;
         P00J34_A74Contrato_Codigo = new int[1] ;
         P00J34_A39Contratada_Codigo = new int[1] ;
         P00J34_A40Contratada_PessoaCod = new int[1] ;
         P00J34_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J34_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J34_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00J34_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00J34_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00J34_A41Contratada_PessoaNom = new String[] {""} ;
         P00J34_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J34_A77Contrato_Numero = new String[] {""} ;
         P00J34_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00J34_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00J34_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         P00J34_A101ContratoGarantia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratogarantiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00J32_A74Contrato_Codigo, P00J32_A39Contratada_Codigo, P00J32_A40Contratada_PessoaCod, P00J32_A77Contrato_Numero, P00J32_A107ContratoGarantia_ValorEncerramento, P00J32_A105ContratoGarantia_ValorGarantia, P00J32_A103ContratoGarantia_Percentual, P00J32_A42Contratada_PessoaCNPJ, P00J32_n42Contratada_PessoaCNPJ, P00J32_A41Contratada_PessoaNom,
               P00J32_n41Contratada_PessoaNom, P00J32_A106ContratoGarantia_DataEncerramento, P00J32_A104ContratoGarantia_DataCaucao, P00J32_A102ContratoGarantia_DataPagtoGarantia, P00J32_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               P00J33_A74Contrato_Codigo, P00J33_A39Contratada_Codigo, P00J33_A40Contratada_PessoaCod, P00J33_A41Contratada_PessoaNom, P00J33_n41Contratada_PessoaNom, P00J33_A107ContratoGarantia_ValorEncerramento, P00J33_A105ContratoGarantia_ValorGarantia, P00J33_A103ContratoGarantia_Percentual, P00J33_A42Contratada_PessoaCNPJ, P00J33_n42Contratada_PessoaCNPJ,
               P00J33_A77Contrato_Numero, P00J33_A106ContratoGarantia_DataEncerramento, P00J33_A104ContratoGarantia_DataCaucao, P00J33_A102ContratoGarantia_DataPagtoGarantia, P00J33_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               P00J34_A74Contrato_Codigo, P00J34_A39Contratada_Codigo, P00J34_A40Contratada_PessoaCod, P00J34_A42Contratada_PessoaCNPJ, P00J34_n42Contratada_PessoaCNPJ, P00J34_A107ContratoGarantia_ValorEncerramento, P00J34_A105ContratoGarantia_ValorGarantia, P00J34_A103ContratoGarantia_Percentual, P00J34_A41Contratada_PessoaNom, P00J34_n41Contratada_PessoaNom,
               P00J34_A77Contrato_Numero, P00J34_A106ContratoGarantia_DataEncerramento, P00J34_A104ContratoGarantia_DataCaucao, P00J34_A102ContratoGarantia_DataPagtoGarantia, P00J34_A101ContratoGarantia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV81GXV1 ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A101ContratoGarantia_Codigo ;
      private long AV40count ;
      private decimal AV18TFContratoGarantia_Percentual ;
      private decimal AV19TFContratoGarantia_Percentual_To ;
      private decimal AV22TFContratoGarantia_ValorGarantia ;
      private decimal AV23TFContratoGarantia_ValorGarantia_To ;
      private decimal AV26TFContratoGarantia_ValorEncerramento ;
      private decimal AV27TFContratoGarantia_ValorEncerramento_To ;
      private decimal AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ;
      private decimal AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ;
      private decimal AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ;
      private decimal AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ;
      private decimal AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ;
      private decimal AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContratada_PessoaNom ;
      private String AV13TFContratada_PessoaNom_Sel ;
      private String AV106WWContratoGarantiaDS_24_Tfcontrato_numero ;
      private String AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ;
      private String AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ;
      private String AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ;
      private String scmdbuf ;
      private String lV106WWContratoGarantiaDS_24_Tfcontrato_numero ;
      private String lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ;
      private String A77Contrato_Numero ;
      private String A41Contratada_PessoaNom ;
      private DateTime AV16TFContratoGarantia_DataPagtoGarantia ;
      private DateTime AV17TFContratoGarantia_DataPagtoGarantia_To ;
      private DateTime AV20TFContratoGarantia_DataCaucao ;
      private DateTime AV21TFContratoGarantia_DataCaucao_To ;
      private DateTime AV24TFContratoGarantia_DataEncerramento ;
      private DateTime AV25TFContratoGarantia_DataEncerramento_To ;
      private DateTime AV47ContratoGarantia_DataPagtoGarantia1 ;
      private DateTime AV48ContratoGarantia_DataPagtoGarantia_To1 ;
      private DateTime AV49ContratoGarantia_DataCaucao1 ;
      private DateTime AV50ContratoGarantia_DataCaucao_To1 ;
      private DateTime AV51ContratoGarantia_DataEncerramento1 ;
      private DateTime AV52ContratoGarantia_DataEncerramento_To1 ;
      private DateTime AV55ContratoGarantia_DataPagtoGarantia2 ;
      private DateTime AV56ContratoGarantia_DataPagtoGarantia_To2 ;
      private DateTime AV57ContratoGarantia_DataCaucao2 ;
      private DateTime AV58ContratoGarantia_DataCaucao_To2 ;
      private DateTime AV59ContratoGarantia_DataEncerramento2 ;
      private DateTime AV60ContratoGarantia_DataEncerramento_To2 ;
      private DateTime AV63ContratoGarantia_DataPagtoGarantia3 ;
      private DateTime AV64ContratoGarantia_DataPagtoGarantia_To3 ;
      private DateTime AV65ContratoGarantia_DataCaucao3 ;
      private DateTime AV66ContratoGarantia_DataCaucao_To3 ;
      private DateTime AV67ContratoGarantia_DataEncerramento3 ;
      private DateTime AV68ContratoGarantia_DataEncerramento_To3 ;
      private DateTime AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ;
      private DateTime AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ;
      private DateTime AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ;
      private DateTime AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ;
      private DateTime AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ;
      private DateTime AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ;
      private DateTime AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ;
      private DateTime AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ;
      private DateTime AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ;
      private DateTime AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ;
      private DateTime AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ;
      private DateTime AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ;
      private DateTime AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ;
      private DateTime AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ;
      private DateTime AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ;
      private DateTime AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ;
      private DateTime AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ;
      private DateTime AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ;
      private DateTime AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ;
      private DateTime AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ;
      private DateTime AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ;
      private DateTime AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ;
      private DateTime AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ;
      private DateTime AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private bool returnInSub ;
      private bool AV53DynamicFiltersEnabled2 ;
      private bool AV61DynamicFiltersEnabled3 ;
      private bool AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ;
      private bool AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ;
      private bool BRKJ32 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKJ34 ;
      private bool BRKJ36 ;
      private String AV39OptionIndexesJson ;
      private String AV34OptionsJson ;
      private String AV37OptionsDescJson ;
      private String AV30DDOName ;
      private String AV28SearchTxt ;
      private String AV29SearchTxtTo ;
      private String AV14TFContratada_PessoaCNPJ ;
      private String AV15TFContratada_PessoaCNPJ_Sel ;
      private String AV46DynamicFiltersSelector1 ;
      private String AV54DynamicFiltersSelector2 ;
      private String AV62DynamicFiltersSelector3 ;
      private String AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ;
      private String AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ;
      private String AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ;
      private String AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ;
      private String AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ;
      private String lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV32Option ;
      private IGxSession AV41Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00J32_A74Contrato_Codigo ;
      private int[] P00J32_A39Contratada_Codigo ;
      private int[] P00J32_A40Contratada_PessoaCod ;
      private String[] P00J32_A77Contrato_Numero ;
      private decimal[] P00J32_A107ContratoGarantia_ValorEncerramento ;
      private decimal[] P00J32_A105ContratoGarantia_ValorGarantia ;
      private decimal[] P00J32_A103ContratoGarantia_Percentual ;
      private String[] P00J32_A42Contratada_PessoaCNPJ ;
      private bool[] P00J32_n42Contratada_PessoaCNPJ ;
      private String[] P00J32_A41Contratada_PessoaNom ;
      private bool[] P00J32_n41Contratada_PessoaNom ;
      private DateTime[] P00J32_A106ContratoGarantia_DataEncerramento ;
      private DateTime[] P00J32_A104ContratoGarantia_DataCaucao ;
      private DateTime[] P00J32_A102ContratoGarantia_DataPagtoGarantia ;
      private int[] P00J32_A101ContratoGarantia_Codigo ;
      private int[] P00J33_A74Contrato_Codigo ;
      private int[] P00J33_A39Contratada_Codigo ;
      private int[] P00J33_A40Contratada_PessoaCod ;
      private String[] P00J33_A41Contratada_PessoaNom ;
      private bool[] P00J33_n41Contratada_PessoaNom ;
      private decimal[] P00J33_A107ContratoGarantia_ValorEncerramento ;
      private decimal[] P00J33_A105ContratoGarantia_ValorGarantia ;
      private decimal[] P00J33_A103ContratoGarantia_Percentual ;
      private String[] P00J33_A42Contratada_PessoaCNPJ ;
      private bool[] P00J33_n42Contratada_PessoaCNPJ ;
      private String[] P00J33_A77Contrato_Numero ;
      private DateTime[] P00J33_A106ContratoGarantia_DataEncerramento ;
      private DateTime[] P00J33_A104ContratoGarantia_DataCaucao ;
      private DateTime[] P00J33_A102ContratoGarantia_DataPagtoGarantia ;
      private int[] P00J33_A101ContratoGarantia_Codigo ;
      private int[] P00J34_A74Contrato_Codigo ;
      private int[] P00J34_A39Contratada_Codigo ;
      private int[] P00J34_A40Contratada_PessoaCod ;
      private String[] P00J34_A42Contratada_PessoaCNPJ ;
      private bool[] P00J34_n42Contratada_PessoaCNPJ ;
      private decimal[] P00J34_A107ContratoGarantia_ValorEncerramento ;
      private decimal[] P00J34_A105ContratoGarantia_ValorGarantia ;
      private decimal[] P00J34_A103ContratoGarantia_Percentual ;
      private String[] P00J34_A41Contratada_PessoaNom ;
      private bool[] P00J34_n41Contratada_PessoaNom ;
      private String[] P00J34_A77Contrato_Numero ;
      private DateTime[] P00J34_A106ContratoGarantia_DataEncerramento ;
      private DateTime[] P00J34_A104ContratoGarantia_DataCaucao ;
      private DateTime[] P00J34_A102ContratoGarantia_DataPagtoGarantia ;
      private int[] P00J34_A101ContratoGarantia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV44GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV45GridStateDynamicFilter ;
   }

   public class getwwcontratogarantiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00J32( IGxContext context ,
                                             String AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                             DateTime AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                             DateTime AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                             DateTime AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                             DateTime AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                             DateTime AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                             bool AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                             String AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                             DateTime AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                             DateTime AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                             DateTime AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                             DateTime AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                             DateTime AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                             bool AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                             String AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                             DateTime AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                             DateTime AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                             DateTime AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                             DateTime AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                             DateTime AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                             String AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                             String AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                             String AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                             String AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                             String AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                             String AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                             DateTime AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                             DateTime AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                             decimal AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                             decimal AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                             DateTime AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                             DateTime AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                             decimal AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                             decimal AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                             DateTime AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                             DateTime AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                             decimal AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                             decimal AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [36] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_Numero], T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_DataPagtoGarantia], T1.[ContratoGarantia_Codigo] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00J33( IGxContext context ,
                                             String AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                             DateTime AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                             DateTime AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                             DateTime AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                             DateTime AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                             DateTime AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                             bool AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                             String AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                             DateTime AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                             DateTime AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                             DateTime AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                             DateTime AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                             DateTime AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                             bool AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                             String AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                             DateTime AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                             DateTime AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                             DateTime AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                             DateTime AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                             DateTime AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                             String AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                             String AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                             String AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                             String AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                             String AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                             String AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                             DateTime AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                             DateTime AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                             decimal AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                             decimal AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                             DateTime AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                             DateTime AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                             decimal AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                             decimal AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                             DateTime AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                             DateTime AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                             decimal AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                             decimal AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [36] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Contrato_Numero], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_DataPagtoGarantia], T1.[ContratoGarantia_Codigo] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00J34( IGxContext context ,
                                             String AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                             DateTime AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                             DateTime AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                             DateTime AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                             DateTime AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                             DateTime AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                             bool AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                             String AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                             DateTime AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                             DateTime AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                             DateTime AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                             DateTime AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                             DateTime AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                             bool AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                             String AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                             DateTime AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                             DateTime AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                             DateTime AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                             DateTime AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                             DateTime AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                             String AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                             String AV106WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                             String AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                             String AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                             String AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                             String AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                             DateTime AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                             DateTime AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                             decimal AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                             decimal AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                             DateTime AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                             DateTime AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                             decimal AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                             decimal AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                             DateTime AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                             DateTime AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                             decimal AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                             decimal AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [36] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_DataPagtoGarantia], T1.[ContratoGarantia_Codigo] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV90WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV98WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoGarantiaDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV106WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00J32(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] );
               case 1 :
                     return conditional_P00J33(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] );
               case 2 :
                     return conditional_P00J34(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00J32 ;
          prmP00J32 = new Object[] {
          new Object[] {"@AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV106WWContratoGarantiaDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00J33 ;
          prmP00J33 = new Object[] {
          new Object[] {"@AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV106WWContratoGarantiaDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00J34 ;
          prmP00J34 = new Object[] {
          new Object[] {"@AV84WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWContratoGarantiaDS_4_Contratogarantia_datacaucao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWContratoGarantiaDS_12_Contratogarantia_datacaucao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoGarantiaDS_20_Contratogarantia_datacaucao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV104WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV105WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV106WWContratoGarantiaDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV107WWContratoGarantiaDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV108WWContratoGarantiaDS_26_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV109WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV110WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV111WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV112WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoGarantiaDS_32_Tfcontratogarantia_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV115WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV116WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV117WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV119WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV120WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV122WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV123WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00J32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J32,100,0,true,false )
             ,new CursorDef("P00J33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J33,100,0,true,false )
             ,new CursorDef("P00J34", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J34,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((String[]) buf[9])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[71]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[71]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[71]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratogarantiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratogarantiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratogarantiafilterdata") )
          {
             return  ;
          }
          getwwcontratogarantiafilterdata worker = new getwwcontratogarantiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
